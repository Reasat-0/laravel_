<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />

  <!-- Validator Jquery plugin css -->

  <!-- <link href="{{asset('jquery-validation-1.19.2/demo/css/screen.css')}}" rel="stylesheet"> -->

 <!--  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('assets/img/favicon.png')}}"> -->
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Oilfield Management
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
   <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{asset('assets/css/material-dashboard.css?v=2.1.0')}}" rel="stylesheet" />

  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet" />
  <link href="{{asset('drag-drop-image-uploader/dist/image-uploader.min.css')}}" rel="stylesheet">
  <link href="{{asset('year_picker/yearpicker.css')}}" rel="stylesheet">
  <link href="{{asset('css/custom.css')}}" rel="stylesheet" />


<!--   <link rel="stylesheet" href="css/screen.css">
  <script src="../lib/jquery.js"></script> -->




  <script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
  <!-- <script src="{{asset('drag-drop-image-uploader/dist/image-uploader.min.js')}}"></script> -->
  <!-- <script src="{{asset('drag-drop-image-uploader/src/image-uploader.js')}}"></script> -->
  <script src="{{asset('drag-drop-image-uploader/src/image-uploader2.js')}}"></script>

  
  <!-- <script src="{{asset('multiple-file-upload-validation/jquery.MultiFile.min.js')}}"></script> -->
  <script src="{{asset('drag-drop-image-uploader/src/file_uploader.js')}}"></script>

  
  <script src="{{asset('year_picker/yearpicker.js')}}"></script>

    <!--  Select 2  -->

  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

  <!-- Editable Select -->
  <script src="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script>
  <link href="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet">

  <!-- Validator Jquery Plugin js-->
  <!-- <script src="{{asset('jquery-validation-1.19.2/dist/jquery.validate.js')}}"></script> -->


<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>


<!-- Signature LInks -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="{{asset('js/signature.js')}}"></script>


<link rel="stylesheet" type="text/css" href="{{asset('css/signature.css')}}">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="rose" data-background-color="black" data-image="{{asset('assets/img/sidebar-1.jpg')}}">

      <div class="logo">
        <a href="{{route('home')}}" class="simple-text logo-normal">
          <img src="{{ asset('assets/img/oil-trucks.jpg') }}"/>
        </a>
      </div>

      <div class="sidebar-wrapper">
        <ul class="nav">
          <!-- <li class="nav-item menu_li">
            <a class="nav-link" href="{{ route('home') }}">
              <i class="material-icons">dashboard</i>
              <p> Dashboard </p>
            </a>
          </li> -->
          <li class="nav-item menu_li">
            <a class="nav-link" data-toggle="collapse" href="#user_page">
              <i class="material-icons">people_outline</i>
              <p> Customers
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="user_page">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('customers.create') }}">
                    <span class="sidebar-mini"> AC </span>
                    <span class="sidebar-normal"> Add Customer </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('customers.index') }}">
                    <span class="sidebar-mini"> MC </span>
                    <span class="sidebar-normal"> Manage Customer </span>
                  </a>
                </li>
              </ul>
            </div>
          </li>

          <li class="nav-item menu_li">
            <a class="nav-link" data-toggle="collapse" href="#inventory_page">
              <i class="material-icons">ballot</i>
              <p> Inventory
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="inventory_page">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('inventory.create') }}">
                    <span class="sidebar-mini"> AI </span>
                    <span class="sidebar-normal"> Add Inventory </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('inventory.index') }}">
                    <span class="sidebar-mini"> MI </span>
                    <span class="sidebar-normal"> Manage Inventory </span>
                  </a>
                </li>
              </ul>
            </div>
          </li>

          <li class="nav-item menu_li">
            <a class="nav-link" data-toggle="collapse" href="#products_service_page">
              <i class="material-icons">store</i>
              <p> Services
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="products_service_page">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('products.create') }}">
                    <span class="sidebar-mini"> AS </span>
                    <span class="sidebar-normal"> Add Service </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('products.index') }}">
                    <span class="sidebar-mini"> MS </span>
                    <span class="sidebar-normal"> Manage Service </span>
                  </a>
                </li>
              </ul>
            </div>
          </li>

          <!-- <li class="nav-item menu_li">
            <a class="nav-link" href="{{ route('jobChecklist.index') }}">
              <i class="material-icons">ballot</i>
              <p> Job Checklist </p>
            </a>
          </li> -->

          

          <li class="nav-item menu_li">
            <a class="nav-link" data-toggle="collapse" href="#vendor_page">
              <i class="material-icons">supervised_user_circle</i>
              <p> Vendors
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="vendor_page">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('vendors.create') }}">
                    <span class="sidebar-mini"> AV </span>
                    <span class="sidebar-normal"> Add Vendor </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('vendors.index') }}">
                    <span class="sidebar-mini"> MV </span>
                    <span class="sidebar-normal"> Manage Vendor </span>
                  </a>
                </li>
              </ul>
            </div>
          </li>


          <li class="nav-item menu_li">
            <a class="nav-link" href="{{ route('insurance.index') }}">
              <i class="material-icons">ballot</i>
              <p> Manage Insurance </p>
            </a>
          </li>


          <li class="nav-item menu_li">
            <a class="nav-link" data-toggle="collapse" href="#users_page">
              <i class="material-icons">account_circle</i>
              <p> Users
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="users_page">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('users.create') }}">
                    <span class="sidebar-mini"> AU </span>
                    <span class="sidebar-normal"> Add User </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('users.index') }}">
                    <span class="sidebar-mini"> MU </span>
                    <span class="sidebar-normal"> Manage User </span>
                  </a>
                </li>
              </ul>
            </div>
          </li>


          <li class="nav-item menu_li">
            <a class="nav-link" data-toggle="collapse" href="#quotation_page">
              <i class="material-icons">monetization_on</i>
              <p> Quotation
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="quotation_page">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('quotation.create') }}">
                    <span class="sidebar-mini"> AQ </span>
                    <span class="sidebar-normal"> Add Quotation </span>
                  </a>
                </li>
                <!-- <li class="nav-item ">
                  <a class="nav-link" href="{{ route('quotation.index') }}">
                    <span class="sidebar-mini"> MQ </span>
                    <span class="sidebar-normal"> Manage Quotation </span>
                  </a>
                </li> -->
              </ul>
            </div>
          </li>

          <!-- <li class="nav-item menu_li">
            <a class="nav-link" data-toggle="collapse" href="#job_ticket_page">
              <i class="material-icons">money</i>
              <p> Job Tickets
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="job_ticket_page">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="add_service.html">
                    <span class="sidebar-mini"> AJT </span>
                    <span class="sidebar-normal"> Add Job Ticket </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="manage_service.html">
                    <span class="sidebar-mini"> MJT </span>
                    <span class="sidebar-normal"> Manage Job Ticket </span>
                  </a>
                </li>
              </ul>
            </div>
          </li> -->

          <!-- <li class="nav-item menu_li">
            <a class="nav-link" data-toggle="collapse" href="#invoice_page">
              <i class="material-icons">ballot</i>
              <p> Invoice
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="invoice_page">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="add_service.html">
                    <span class="sidebar-mini"> AI </span>
                    <span class="sidebar-normal"> Add Invoice </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="manage_service.html">
                    <span class="sidebar-mini"> MI </span>
                    <span class="sidebar-normal"> Manage Invoice </span>
                  </a>
                </li>
              </ul>
            </div>
          </li> -->



          <!-- <li class="nav-item menu_li">
            <a class="nav-link" href="{{ route('settings.index') }}">
              <i class="material-icons">build</i>
              <p> Settings</p>
            </a>
          </li> -->

        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize">
              <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
              </button>
            </div>
            <a class="navbar-brand page_title" href="{{ route('home') }}">Oil Field Service Management</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Mike John responded to your email</a>
                  <a class="dropdown-item" href="#">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                  <a class="dropdown-item" href="#">Another Notification</a>
                  <a class="dropdown-item" href="#">Another One</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="{{ route('myProfile') }}">Profile</a>
                  <a class="dropdown-item" href="#">Settings</a>
                  <div class="dropdown-divider"></div>
                  <!-- <a class="dropdown-item" href="#">Log out</a> -->
                  

                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                

                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
    
      
      @yield('content')

      

  </div>
</div>

<!--   Core JS Files   -->

<script src="{{asset('assets/js/core/popper.min.js')}}"></script>
<script src="{{asset('assets/js/core/bootstrap-material-design.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
<!-- Plugin for the momentJs  -->
<script src="{{asset('assets/js/plugins/moment.min.js')}}"></script>
<!--  Plugin for Sweet Alert -->
<script src="{{asset('assets/js/plugins/sweetalert2.js')}}"></script>
<!-- Forms Validations Plugin -->
<script src="{{asset('assets/js/plugins/jquery.validate.min.js')}}"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="{{asset('assets/js/plugins/jquery.bootstrap-wizard.js')}}"></script>
<!--  Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="{{asset('assets/js/plugins/bootstrap-selectpicker.js')}}"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="{{asset('assets/js/plugins/bootstrap-datetimepicker.min.js')}}"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="{{asset('assets/js/plugins/jquery.dataTables.min.js')}}"></script>
<!--  Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="{{asset('assets/js/plugins/bootstrap-tagsinput.js')}}"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="{{asset('assets/js/plugins/jasny-bootstrap.min.js')}}"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="{{asset('assets/js/plugins/fullcalendar.min.js')}}"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="{{asset('assets/js/plugins/jquery-jvectormap.js')}}"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{asset('assets/js/plugins/nouislider.min.js')}}"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="{{asset('assets/js/plugins/arrive.min.js')}}"></script>
<!--  Google Maps Plugin    -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->
<!-- Chartist JS -->
<script src="{{asset('assets/js/plugins/chartist.min.js')}}"></script>
<!--  Notifications Plugin    -->
<script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{asset('assets/js/material-dashboard.js?v=2.1.0')}}" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{asset('assets/demo/demo.js')}}"></script>

<script src="{{asset('assets/js/fontawesome.js')}}"></script>

<!-- <script src="{{asset('assets/js/jquery-input-mask-phone-number.js')}}"></script> -->

<script src="{{asset('js/custom.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.8/handlebars.min.js"></script>



<script>
  $(document).ready(function() {

    $().ready(function() {
      $sidebar = $('.sidebar');

      $sidebar_img_container = $sidebar.find('.sidebar-background');

      $full_page = $('.full-page');

      $sidebar_responsive = $('body > .navbar-collapse');

      window_width = $(window).width();

      fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

      if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
        if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
          $('.fixed-plugin .dropdown').addClass('open');
        }

      }

      $('.fixed-plugin a').click(function(event) {
        // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
        if ($(this).hasClass('switch-trigger')) {
          if (event.stopPropagation) {
            event.stopPropagation();
          } else if (window.event) {
            window.event.cancelBubble = true;
          }
        }
      });

      $('.fixed-plugin .active-color span').click(function() {
        $full_page_background = $('.full-page-background');

        $(this).siblings().removeClass('active');
        $(this).addClass('active');

        var new_color = $(this).data('color');

        if ($sidebar.length != 0) {
          $sidebar.attr('data-color', new_color);
        }

        if ($full_page.length != 0) {
          $full_page.attr('filter-color', new_color);
        }

        if ($sidebar_responsive.length != 0) {
          $sidebar_responsive.attr('data-color', new_color);
        }
      });

      $('.fixed-plugin .background-color .badge').click(function() {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');

        var new_color = $(this).data('background-color');

        if ($sidebar.length != 0) {
          $sidebar.attr('data-background-color', new_color);
        }
      });

      $('.fixed-plugin .img-holder').click(function() {
        $full_page_background = $('.full-page-background');

        $(this).parent('li').siblings().removeClass('active');
        $(this).parent('li').addClass('active');


        var new_image = $(this).find("img").attr('src');

        if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
          $sidebar_img_container.fadeOut('fast', function() {
            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $sidebar_img_container.fadeIn('fast');
          });
        }

        if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
          var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

          $full_page_background.fadeOut('fast', function() {
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
            $full_page_background.fadeIn('fast');
          });
        }

        if ($('.switch-sidebar-image input:checked').length == 0) {
          var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
          var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

          $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
          $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
        }

        if ($sidebar_responsive.length != 0) {
          $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
        }
      });

      $('.switch-sidebar-image input').change(function() {
        $full_page_background = $('.full-page-background');

        $input = $(this);

        if ($input.is(':checked')) {
          if ($sidebar_img_container.length != 0) {
            $sidebar_img_container.fadeIn('fast');
            $sidebar.attr('data-image', '#');
          }

          if ($full_page_background.length != 0) {
            $full_page_background.fadeIn('fast');
            $full_page.attr('data-image', '#');
          }

          background_image = true;
        } else {
          if ($sidebar_img_container.length != 0) {
            $sidebar.removeAttr('data-image');
            $sidebar_img_container.fadeOut('fast');
          }

          if ($full_page_background.length != 0) {
            $full_page.removeAttr('data-image', '#');
            $full_page_background.fadeOut('fast');
          }

          background_image = false;
        }
      });

      $('.switch-sidebar-mini input').change(function() {
        $body = $('body');

        $input = $(this);

        if (md.misc.sidebar_mini_active == true) {
          $('body').removeClass('sidebar-mini');
          md.misc.sidebar_mini_active = false;

          $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

        } else {

          $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

          setTimeout(function() {
            $('body').addClass('sidebar-mini');

            md.misc.sidebar_mini_active = true;
          }, 300);
        }

        // we simulate the window Resize so the charts will get updated in realtime.
        var simulateWindowResize = setInterval(function() {
          window.dispatchEvent(new Event('resize'));
        }, 180);

        // we stop the simulation of Window Resize after the animations are completed
        setTimeout(function() {
          clearInterval(simulateWindowResize);
        }, 1000);

      });
    });
  });
</script>
<script>
  $(document).ready(function() {
    // Javascript method's body can be found in assets/js/demos.js
    md.initDashboardPageCharts();

    md.initVectorMap();

  });
  $(document).ready(function() {
      // initialise Datetimepicker and Sliders
      md.initFormExtendedDatetimepickers();
      if ($('.slider').length != 0) {
        md.initSliders();
      }

    });


  //  Select 2 dw activation
  $(document).ready(function() {
    $('.custom_select2_col select').select2();

  });



</script>



</body>

</html>
