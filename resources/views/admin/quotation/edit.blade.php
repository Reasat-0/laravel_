@extends('layouts.master')

@section('content')

        <div class="content">
          <div class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12 mx-auto card_holder_box">
                  
                  
                  <div class="card">
                    <div class="card-header card-header-tabs card-header-rose">
                      <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                          
                          <ul class="nav nav-tabs" data-tabs="tabs">
                            <li class="nav-item">
                              <a class="nav-link active" href="#profile" data-toggle="tab">
                                <i class="material-icons">ballot</i>Quotation
                                <div class="ripple-container"></div>
                              </a>
                            </li>
                            
                          </ul>
                        </div>
                      </div>
                    </div>

                    <div style="margin-top: 15px;">
                        @include('partials.messages')
                    </div>

                    <div class="card-body">
                      <form method="post" action="{{ route('quotation.update', $quotation->id) }}" class="form-horizontal">
                                    
                       @csrf
                       @method('PUT')

                      <div class="card">
                        
                       
                        <div class="card-body">
                          <div class="row justify-content-between align-items-center">
                            <div class="col-md-4 input_wrapper quote_customer_selection_div custom_select2_col">
                              <div class="form-group custom-form-select">
                                <select class="custom-select hasvalue select2" id="pro-sub-category-custom-select" name="customer_id" required="true" aria-required="true">
                                  <option class="form-select-placeholder"></option>
                                  @foreach($customers as $rowdata)
                                      <option c_name="{{$rowdata->companyName}}" d_name="{{$rowdata->displayName}}" email="{{$rowdata->primaryEmailAddr_addess}}" phone="{{$rowdata->primaryPhone_freeFormNumber}}" mobile="{{$rowdata->mobile}}" value="{{$rowdata->id}}" @if($quotation->customer_id == $rowdata->id) selected @endif>{{$rowdata->displayName}}</option>
                                  @endforeach
                                </select>
                                <div class="form-element-bar">
                                </div>
                                <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Customers list</label>
                              </div>
                            </div>
                            <div class="col-md-4 input_wrapper quote_date_ticked_div">
                              <div class="form-group bmd-form-group is-filled">
                                <label for="quote_copon" class="bmd-label-floating input_label">Quote Ticket</label>
                                <input type="text" class="form-control form_input_field" id="quote_ticket1" name="quote_ticket" readonly="" value="{{$quotation->quote_ticket}}">
                              </div>
                              <div class="form-group bmd-form-group is-filled">
                                <label for="quote_copon" class="bmd-label-floating input_label">Date</label>
                                <input type="text" class="form-control datepicker" name="date" value="{{$quotation->date}}">
                              </div>
                            </div>
                          </div>


                          <div class="customers_details">
                            <div class="row">

                              <div class="card customer-card">
                                
                                <div class="card-body">

                                  <div class="alert alert-success alert-dismissible update_customer_msg_wrapper" role="alert" style="display: none;">
                                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                      <strong class="update_customer_msg"></strong>
                                  </div>

                                  <div class="col-md-12">
                                    <div class="row">
                                      <div class="col-md-12">
                                        <h3 class="title">Company name : <br class="name_br" style="display: none;"><span id="companyName">@if($quotation->customers){{$quotation->customers->companyName}}@endif</span></h3>
                                        <hr class="company_name_hr">
                                      </div>
                                      <div class="customer-desc">
                                        <div class="col">
                                          <p>Contact</p>
                                          <div class="form-group bmd-form-group">
                                            <input type="text" class="form-control form_input_field" id="displayName" name="displayName"  value="@if($quotation->customers){{$quotation->customers->displayName}}@endif">
                                          </div>
                                        </div>
                                        <div class="col">
                                          <p>Email</p>

                                          <div class="form-group bmd-form-group">
                                            <input type="text" class="form-control form_input_field" id="email" name="email" value="@if($quotation->customers){{$quotation->customers->primaryEmailAddr_addess}}@endif">
                                          </div>

                                        </div>
                                        <div class="col">
                                          <p>Office Phone</p>
                                
                                          <div class="form-group bmd-form-group">
                                            <input type="text" class="form-control form_input_field" id="phone" name="phone" value="@if($quotation->customers){{$quotation->customers->primaryPhone_freeFormNumber}}@endif">
                                          </div>

                                        </div>
                                        <div class="col">
                                          <p>Cell No</p>

                                          <div class="form-group bmd-form-group">
                                            <input type="text" class="form-control form_input_field" id="mobile" name="mobile" value="@if($quotation->customers){{$quotation->customers->mobile}}@endif">
                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                  </div>

                                </div>
                                <div class="card-footer text-right ml-auto save_btn_wrapper">
                                    <button type="button" class="btn btn-xs custom-btn-one customer_update">Update</button>
                                </div>
                              </div>
                            </div>
                          </div>


                        </div>
                      </div>
                      
                      <div class="card edit_category_inner_card service_rental_card">
                        <div class="card-header card-header-rose card-header-text sub_card">
                          <div class="card-text card_text_title">
                            <h4 class="card-title">Product Quotation</h4>
                          </div>
                        </div>

                        <div class="card-body col-md-12 mx-auto increased_margin_t_cardbody">     

                          <div class="row resource_row">
                            <div class="col-md-3 justify-content-center d-flex align-items-center resource_toggle_div quote_resource_toggle_div">
                                <div class="togglebutton admin-role-toggle resource_toggle_section ">
                                    <label>
                                        <input class="service_rental" type="checkbox" name="" checked="">
                                        Service <span class="toggle"></span> Rental
                                    </label>
                                </div>
                            </div>
                            <input type="hidden" id="service_id">
                            <input type="hidden" id="rental_id">

                            <div class="col-md-9 resource_toggle_content_div quote_resource_toggle_content_div">
                                <div class="row" id="service">
                                  <div class="col-md-4 input_wrapper top_input_wrapper custom_select2_col">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select cat_service select2 blank_it">
                                          <option class="form-select-placeholder"></option>
                                        @foreach($cat->where('type', 'product') as $rowdata)
                                          <option value="{{$rowdata->id}}">{{$rowdata->category_name}}</option>
                                        @endforeach
                                      </select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label" id="vendor_resource_name_existing" for="resource_name">Category Name</label>
                                    </div>
                                  </div>
                                  <div class="col-md-4 input_wrapper custom_select2_col">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select subcat_service select2 blank_it"></select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label" for="resource_name">SubCategory Name</label>
                                    </div>
                                  </div>
                                  <div class="col-md-4 input_wrapper custom_select2_col">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select service select2 blank_it"></select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label" for="resource_name">Service Name</label>
                                    </div>
                                  </div>
                                </div>

                                <div class="row" id="rental" style="display: none">
                                  <div class="col-md-4 input_wrapper top_input_wrapper custom_select2_col">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select cat_rental select2 blank_it">
                                          <option class="form-select-placeholder"></option>
                                        @foreach($cat->where('type', 'inventory') as $rowdata)
                                          <option value="{{$rowdata->id}}">{{$rowdata->category_name}}</option>
                                        @endforeach
                                      </select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label" id="vendor_resource_name_existing" for="resource_name">Category Name</label>
                                    </div>
                                  </div>
                                  <div class="col-md-4 input_wrapper custom_select2_col">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select subcat_rental select2 blank_it" id="vendor_resource_name_select">
                                      </select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label" for="resource_name">SubCategory Name</label>
                                    </div>
                                  </div>
                                  <div class="col-md-4 input_wrapper">
                                    <div class="form-group custom-form-select">
                                      <label for="resource_code_exist" class="bmd-label-floating input_label">Resource</label>
                                      <input type="text" class="form-control form_input_field rental" value="">
                                    </div>
                                  </div>
                                </div>
                            </div>
                          </div>  

                          <div class="row">
                            <div class="col-md-12 text-right add_vendor_resource_btn_wrapper">
                              <button class="btn btn-warning btn-round btn-sm add_quote_tbl" type="button">
                                Add
                                <div class="ripple-container"></div>
                              </button>
                            </div>
                          </div>
                          
                          <!-- <div class="row">
                            <button class="btn btn-round btn-sm">Address</button>
                          </div> -->
    

                          <div class="card product_quotation_table_card">
                            <div class=" card-body table_card_wrapper">
                              <div class="card quotation_table_header card_to_scroll2">
                                <div class="quotation_table_section">
                                  <div class="quotation_table_row">
                                    <div class="quote_col">
                                      <p>Item no</p>
                                    </div>
                                    <div class="quote_col">
                                      <p>Item name
                                    </div>
                                    <div class="quote_col single_resource_main">
                                      <div class="all_single_resounce">
                                          <div class="single_resource_col">
                                            <p>Item details</p>
                                          </div>
                                          <div class="single_resource_col">
                                           <p>Price</p>
                                          </div>
                                          <div class="single_resource_col">
                                            Total Unit
                                          </div>
                                          <div class="single_resource_col">
                                           Total duration
                                          </div>
                                          <div class="single_resource_col">
                                           Total <br> ( Unit Cost )
                                          </div>
                                        </div>
                                      </div>

                                    </div>

                                    
                                  </div>
                              </div>

                            <div class="ajaxAppend">
                            @if(!$quote_item->isEMpty())
                              <input type="hidden" class="counter_inc" value="{{$quote_item->count()}}">
                            @else
                              <input type="hidden" class="counter_inc" value="0">
                            @endif

                            <?php $i=1; 
                                  $counter = 0;
                              
                            ?>
                            @foreach($quote_item as $rowdata)
                            @if($rowdata->item_type == 'service')
                              <div class="card single_card_for_single_service">
                                <input type="hidden" name="quote_item_key_for_dlt[]" value="{{$rowdata->id}}">
                                <div class="quotation_table_main_section card_to_scroll">
                                  <div class="quotation_table_section">
                                    <div class="quotation_table_row">
                                      <div class="quote_col">
                                        <p class="item_no">{{$i}}</p>
                                      </div>
                                      <div class="quote_col">
                                        <input type="hidden" name="quote_item_key[{{$counter}}][]" value="{{$rowdata->id}}">
                                        <input type="hidden" name="item_type[{{$counter}}][]" value="service">
                                        
                                        <p>{{$rowdata->service->product_name}}</p>
                                      </div>
                                      <div class="quote_col single_resource_main">
                                      <?php
                                        $quote_item_details = $rowdata->quote_item_details($rowdata->id);
                                      ?>
                                      <?php $resource_var_counter = 0; ?>
                                      @foreach($quote_item_details as $quote_item_detail)
                                        <input type="hidden" name="quote_item_detail_key[{{$counter}}][]" value="{{$quote_item_detail->id}}">
                                        <input type="hidden" name="resource_id[{{$counter}}][]" value="{{$quote_item_detail->resource_id}}">
                                        <div class="all_single_resounce">
                                        <div class="sub_row">
                                          <div class="single_resource_col">
                                            <p>@if($quote_item_detail->quoteItem){{$quote_item_detail->quoteItem->resource_name}}@endif</p>
                                          </div>
                                          <div class="single_resource_col">
                                            <div class="form-group custom-form-select">
                                              <select class="custom-select cat_service hasvalue colInput" name="resource_price[{{$counter}}][]">
                                                  <option class="form-select-placeholder"></option>
                                                  <option value="{{$quote_item_detail->quoteItem->price_per_hour_service}}" @if($quote_item_detail->resource_price == $quote_item_detail->quoteItem->price_per_hour_service) selected @endif>{{$quote_item_detail->quoteItem->price_per_hour_service}} /Per Hour</option>
                                                  <option value="{{$quote_item_detail->quoteItem->price_per_day_service}}" @if($quote_item_detail->resource_price == $quote_item_detail->quoteItem->price_per_day_service) selected @endif>{{$quote_item_detail->quoteItem->price_per_day_service}} /Per Day</option>
                                                  <option value="{{$quote_item_detail->quoteItem->price_per_week_service}}" @if($quote_item_detail->resource_price == $quote_item_detail->quoteItem->price_per_week_service) selected @endif>{{$quote_item_detail->quoteItem->price_per_week_service}} /Per Week</option>
                                                  <option value="{{$quote_item_detail->quoteItem->price_per_month_service}}" @if($quote_item_detail->resource_price == $quote_item_detail->quoteItem->price_per_month_service) selected @endif>{{$quote_item_detail->quoteItem->price_per_month_service}} /Per Month</option>
                                              </select>
                                              <div class="form-element-bar">
                                              </div>
                                            </div>
                                          </div>
                                          <div class="single_resource_col">
                                            <div class="form-group custom-form-select bmd-form-group">      
                                              <input type="number" class="form-control form_input_field colInput" name="resource_unit[{{$counter}}][]" value="{{$quote_item_detail->resource_unit}}">
                                            </div>
                                          </div>
                                          <div class="single_resource_col">
                                            <div class="form-group custom-form-select bmd-form-group">      
                                              <input type="number" class="form-control form_input_field colInput" name="resource_duration[{{$counter}}][]" value="{{$quote_item_detail->resource_duration}}">
                                            </div>
                                          </div>
                                          <div class="single_resource_col colTotal">
                                            ${{$quote_item_detail->item_detail_total}}
                                          </div>
                                          <input type="hidden" class="colTotal_hidden_input" name="item_detail_total[{{$counter}}][]" value="{{$quote_item_detail->item_detail_total}}">
                                        </div>


                                        <div class="quote_col variable_main_section">
                                            <div class="all_single_resounce_header">
                                              <div class="single_resource_col varaible_name">
                                                <p>Name of variable</p>
                                              </div>
                                              <div class="single_resource_col single_resource_paramenters">
                                                <div class="single_resource_paramenters_row">
                                                  <div class="single_resource_col">
                                                    <p>Parameter</p>
                                                  </div>
                                                  <div class="single_resource_col">
                                                    <p>Data type</p>
                                                  </div>
                                                  <div class="single_resource_col">
                                                    <p>Value</p>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="single_resource_col total_variable">
                                                <p>Total</p>
                                              </div>
                                            </div>
                                            <?php 
                                            $variables=App\Models\QuoteVariable::where('quote_item_type', 'inventory')->where('quote_item_id', $quote_item_detail->id)->get();
                                            $resource_var_param_counter = 0;
                                            ?>

                                            @foreach($variables as $variable)
                                            <div class="all_single_resounce">
                                              <div class="single_resource_col varaible_name">
                                                <input type="hidden" name="resource_var_key[{{$counter}}][{{$resource_var_counter}}][]" value="{{$variable->id}}">
                                                <input type="hidden" name="resource_var_name[{{$counter}}][{{$resource_var_counter}}][]" value="{{$variable->var_name}}">
                                                <p>{{$variable->var_name}}</p>
                                              </div>
                                              <div class="single_resource_col single_resource_paramenters">
                                              <?php
                                                $var_params = App\Models\QuoteVariableParameter::where('quote_item_var_id', $variable->id)->get();
                                              ?>
                                              @foreach($var_params as $var_param)
                                                <div class="single_resource_paramenters_row">
                                                  <div class="single_resource_col">
                                                    <input type="hidden" name="resource_var_param_key[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="{{$var_param->id}}">
                                                    <input type="hidden" name="resource_var_param_name[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_name}}">
                                                    <p>{{$var_param->var_param_name}}</p>
                                                  </div>
                                                  <div class="single_resource_col">
                                                    <input type="hidden" name="resource_var_param_data_type[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_data_type}}">
                                                    <p>{{$var_param->var_param_data_type}}</p>
                                                  </div>
                                                  <div class="single_resource_col">

                                                  @if($var_param->var_param_value_type == 0)
                                                    @if($var_param->var_param_data_type == 'decimal' || $var_param->var_param_data_type == 'integer')
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="0">

                                                      <input type="hidden" class="multiply" name="resource_var_param_value[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">

                                                    @elseif($var_param->var_param_data_type == 'percentage')
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="0">

                                                      <input type="hidden" class="percentage" name="resource_var_param_value[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">

                                                    @else
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="0">

                                                      <input type="hidden" class="" name="resource_var_param_value[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">

                                                    @endif

                                                    <p>{{$var_param->var_param_value}}</p>
                                                  
                                                  @else
                                                    @if($var_param->var_param_data_type == 'decimal')
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="1">

                                                      <input type="number" class="form-control form_input_field multiply" step="0.01" name="resource_var_param_value[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @elseif($var_param->var_param_data_type == 'integer')
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="1">

                                                      <input type="number" class="form-control form_input_field multiply" name="resource_var_param_value[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @elseif($var_param->var_param_data_type == 'datetime')
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="1">

                                                      <input type="text" class="form-control form_input_field datepicker" name="resource_var_param_value[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @elseif($var_param->var_param_data_type == 'pecentage')
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="1">

                                                      <input type="text" class="form-control form_input_field percentage" name="resource_var_param_value[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @else
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="1">

                                                      <input type="text" class="form-control form_input_field" name="resource_var_param_value[{{$counter}}][{{$resource_var_counter}}][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @endif

                                                  @endif
                                                  </div>
                                                </div>
                                              
                                              @endforeach

                                              </div>
                                              <div class="single_resource_col total_variable total_variable_count">
                                                <p>
                                                  @if($variable->item_var_total == NULL) 0 @else{{$variable->item_var_total}}
                                                  @endif
                                                </p>
                                                <input type="hidden" name="item_var_total[{{$counter}}][{{$resource_var_counter}}][]" class="total_variable_hidden" value="{{$variable->item_var_total}}">
                                              </div>
                                            </div>
                                            <?php $resource_var_param_counter++; ?>
                                            @endforeach

                                            <!-- <div class="all_single_resounce">
                                              <div class="single_resource_col varaible_name">
                                                <p>Schedule</p>
                                              </div>
                                              <div class="single_resource_col single_resource_paramenters">
                                                <div class="single_resource_paramenters_row">
                                                  <div class="single_resource_col">
                                                    <p>scehdule</p>
                                                  </div>
                                                  <div class="single_resource_col">
                                                    <p>date time</p>
                                                  </div>
                                                  <div class="single_resource_col">
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                      <input type="text" class="form-control form_input_field rental" value="">
                                                    </div>
                                                  </div>
                                                </div>
                                              
                                              </div>
                                              <div class="single_resource_col total_variable">
                                                <p>N/A</p>
                                              </div>
                                            </div> -->
                                          </div>
                                          


                                        </div>
                                        <?php $resource_var_counter++; ?>
                                      @endforeach
                                        
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card service_variable_details_section">
                                  <div class="card-body service_variable_details_table">
                                    <div class="serve_row">
                                      <div class="serve_col">
                                        <p>Service variable Details</p>
                                      </div>
                                      <div class="serve_col service_variable_single">
                                      <?php
                                        $service_vars = $rowdata->service_vars($rowdata->id);
                                      ?>
                                       <div class="quote_col variable_main_section">
                                            <div class="all_single_resounce_header">
                                              <div class="single_resource_col varaible_name">
                                                <p>Name of variable</p>
                                              </div>
                                              <div class="single_resource_col single_resource_paramenters">
                                                <div class="single_resource_paramenters_row">
                                                  <div class="single_resource_col">
                                                    <p>Parameter</p>
                                                  </div>
                                                  <div class="single_resource_col">
                                                    <p>Data type</p>
                                                  </div>
                                                  <div class="single_resource_col">
                                                    <p>Value</p>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="single_resource_col total_variable">
                                                <p>Total</p>
                                              </div>
                                            </div>

                                            <?php 
                                            $variables=App\Models\QuoteVariable::where('quote_item_type', 'service')->where('quote_item_id', $rowdata->id)->get();
                                            $service_var_param_counter = 0 ;
                                            ?>
                                            @foreach($variables as $variable)
                                            <div class="all_single_resounce">
                                              <div class="single_resource_col varaible_name">
                                                <input type="hidden" name="service_var_key[{{$counter}}][]" value="{{$variable->id}}">
                                                <input type="hidden" name="service_var_name[{{$counter}}][]" value="{{$variable->var_name}}">
                                                <p>{{$variable->var_name}}</p>
                                              </div>
                                              <div class="single_resource_col single_resource_paramenters">
                                              <?php
                                                $var_params = App\Models\QuoteVariableParameter::where('quote_item_var_id', $variable->id)->get();
                                              ?>
                                              @foreach($var_params as $var_param)

                                                <div class="single_resource_paramenters_row">
                                                  <div class="single_resource_col">
                                                    <input type="hidden" name="service_var_param_key[{{$counter}}][{{$service_var_param_counter}}][]" value="{{$var_param->id}}">
                                                    <input type="hidden" name="service_var_param_name[{{$counter}}][{{$service_var_param_counter}}][]" value="{{$var_param->var_param_name}}">
                                                    <p>{{$var_param->var_param_name}}</p>
                                                  </div>
                                                  <div class="single_resource_col">
                                                    <input type="hidden" name="service_var_param_data_type[{{$counter}}][{{$service_var_param_counter}}][]" value="{{$var_param->var_param_data_type}}">
                                                    <p>{{$var_param->var_param_data_type}}</p>
                                                  </div>
                                                  <div class="single_resource_col">
                                                  @if($var_param->var_param_value_type == 0)
                                                    @if($var_param->var_param_data_type == 'decimal' || $var_param->var_param_data_type == 'integer')
                                                      <input type="hidden" name="service_var_param_value_type[{{$counter}}][{{$service_var_param_counter}}][]" value="0">

                                                      <input type="hidden" class="multiply" name="service_var_param_value[{{$counter}}][{{$service_var_param_counter}}][]" value="{{$var_param->var_param_value}}">

                                                    @elseif($var_param->var_param_data_type == 'percentage')
                                                      <input type="hidden" name="service_var_param_value_type[{{$counter}}][{{$service_var_param_counter}}][]" value="0">

                                                      <input type="hidden" class="percentage" name="service_var_param_value[{{$counter}}][{{$service_var_param_counter}}][]" value="{{$var_param->var_param_value}}">

                                                    @else
                                                      <input type="hidden" name="service_var_param_value_type[{{$counter}}][{{$service_var_param_counter}}][]" value="0">

                                                      <input type="hidden" class="" name="service_var_param_value[{{$counter}}][{{$service_var_param_counter}}][]" value="{{$var_param->var_param_value}}">

                                                    @endif

                                                    <p>{{$var_param->var_param_value}}</p>
                                                  
                                                  @else
                                                    @if($var_param->var_param_data_type == 'decimal')
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                      <input type="hidden" name="service_var_param_value_type[{{$counter}}][{{$service_var_param_counter}}][]" value="1">

                                                      <input type="number" class="form-control form_input_field multiply" step="0.01" name="service_var_param_value[{{$counter}}][{{$service_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @elseif($var_param->var_param_data_type == 'integer')
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                       <input type="hidden" name="service_var_param_value_type[{{$counter}}][{{$service_var_param_counter}}][]" value="1">

                                                      <input type="number" class="form-control form_input_field multiply" name="service_var_param_value[{{$counter}}][{{$service_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @elseif($var_param->var_param_data_type == 'datetime')
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                       <input type="hidden" name="service_var_param_value_type[{{$counter}}][{{$service_var_param_counter}}][]" value="1">

                                                      <input type="text" class="form-control form_input_field datepicker"  name="service_var_param_value[{{$counter}}][{{$service_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @elseif($var_param->var_param_data_type == 'pecentage')
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                       <input type="hidden" name="service_var_param_value_type[{{$counter}}][{{$service_var_param_counter}}][]" value="1">

                                                      <input type="text" class="form-control form_input_field percentage" name="service_var_param_value[{{$counter}}][{{$service_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @else
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                       <input type="hidden" name="service_var_param_value_type[{{$counter}}][{{$service_var_param_counter}}][]" value="1">

                                                      <input type="text" class="form-control form_input_field" name="service_var_param_value[{{$counter}}][{{$service_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @endif

                                                  @endif
                                                  </div>
                                                </div>
                                              @endforeach

                                              </div>
                                              <div class="single_resource_col total_variable total_variable_count">
                                                <p>
                                                  @if($variable->item_var_total == NULL) 0 @else{{$variable->item_var_total}}
                                                  @endif
                                                </p>
                                                <input type="hidden" name="service_var_total[{{$counter}}][]" class="total_variable_hidden" value="{{$variable->item_var_total}}">
                                              </div>
                                            </div>
                                            <?php $service_var_param_counter++; ?>
                                            @endforeach

                                          </div>

                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card service_loaction_net_total_section">
                                  <div class="card-body service_loaction_net_tota">
                                    <div class="service_location">
                                      <input type="hidden" class="address_selected" name="address_selected[{{$counter}}][]" value="{{$rowdata->service_location_address_id}}">
                                      <input type="hidden" class="new_address_hidden_field line1" name="line1[{{$counter}}][]" value="">
                                      <input type="hidden" class="new_address_hidden_field line2" name="line2[{{$counter}}][]" value="">
                                      <input type="hidden" class="new_address_hidden_field city" name="city[{{$counter}}][]" value="">
                                      <input type="hidden" class="new_address_hidden_field country" name="country[{{$counter}}][]" value="">
                                      <input type="hidden" class="new_address_hidden_field subdivisioncode" name="subdivisioncode[{{$counter}}][]" value="">
                                      <input type="hidden" class="new_address_hidden_field postalcode" name="postalcode[{{$counter}}][]" value="">
                                      <button class="btn btn-info btn-lg btn-round quote_address_btn" type="button" counter="{{$counter}}">Service Location</button>
                                      <div id="myModal{{$counter}}" class="modal addressModal" style="display: none;"></div>
                                    </div>

                                    @if($rowdata->service_location_address_id  != null)
                                    <div class="card selected_address_card">
                                      <div class="card-body">
                                        <div id="address_viewer" class="">
                                          <h6> Selected Address </h6>
                                          <hr class="head-hr">
                                          @if($rowdata->service_location_address_id == 'default')
                                          <?php 
                                          $addr = App\Models\Customer::where('id', $quotation->customer_id)->first();
                                          ?>
                                          @else
                                          <?php 
                                          $addr = App\Models\ServiceLocationAddress::where('id', $rowdata->service_location_address_id)->first();
                                          ?>
                                          @endif
                                          
                                          <p> {{$addr->shipAddr_line1}} </p>
                                          <p> {{$addr->shipAddr_line2}} </p>
                                          <p> {{$addr->shipAddr_city}} </p>
                                          <p> {{$addr->shipAddr_country}} </p>
                                          <p> {{$addr->shipAddr_countrySubDivisionCode}} </p>
                                          <p> {{$addr->shipAddr_postalCode}} </p>
                                          
                                        </div>
                                      </div>
                                    </div>
                                    @endif

                                    <div class="net_total">
                                      <div class="net_total_single">
                                        <p class="tr_outside_total_unit_cost">Total Unit Cost<span>${{$rowdata->item_total}}</span></p>
                                      <input type="hidden" class="tr_outside_total_unit_cost_input_hidden" name="item_total[{{$counter}}][]" value="{{$rowdata->item_total}}">
                                      </div>
                                      <div class="net_total_single">
                                        <p class="tr_outside_var_cost_1">Total Variable Cost <span>${{$rowdata->var_total}}</span></p>
                                        <input type="hidden" name="var_total[{{$counter}}][]" class="final_total_variable_hidden" value="{{$rowdata->var_total}}">
                                      </div>
                                      <hr style="width:40%;margin-right:0;">
                                      <div class="net_total_single tr_outside_gross_cost">
                                        <p>Gross Total <span>${{$rowdata->gross_total}}</span></p>
                                        <input type="hidden" name="gross_total[{{$counter}}][]" class="final_gross_total_hidden" value="{{$rowdata->gross_total}}">
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="cross">
                                  <a href="#" class="btn btn-danger btn-round">
                                    <i class="material-icons">close</i>
                                  </a>
                                </div>
                              </div>
                            @endif

                            @if($rowdata->item_type == 'rental')
                              <div class="card single_card_for_single_service">
                                <input type="hidden" name="quote_item_key_for_dlt[]" value="{{$rowdata->id}}">
                                <input type="hidden" name="quote_item_key[{{$counter}}][]" value="{{$rowdata->id}}">
                                <div class="quotation_table_main_section card_to_scroll">
                                  <div class="quotation_table_section">
                                    <div class="quotation_table_row">
                                      <div class="quote_col">
                                        <p class="item_no">{{$i}}</p>
                                      </div>
                                      <div class="quote_col">
                                        <input type="hidden" name="quote_item_key[{{$counter}}][]" value="{{$rowdata->id}}">
                                        <input type="hidden" name="item_type[{{$counter}}][]" value="rental">
                                        
                                        <p>@if($rowdata->rental){{$rowdata->rental->resource_name}}@endif</p>
                                      </div>
                                      <div class="quote_col single_resource_main">

                                      <?php
                                        $quote_item_details = $rowdata->quote_item_details($rowdata->id);
                                      ?>

                                      @foreach($quote_item_details as $quote_item_detail)
                                      <input type="hidden" name="quote_item_detail_key[{{$counter}}][]" value="{{$quote_item_detail->id}}">
                                      <input type="hidden" name="resource_id[{{$counter}}][]" value="{{$quote_item_detail->resource_id}}">
                                        <div class="all_single_resounce">
                                        <div class="sub_row">
                                          <div class="single_resource_col">
                                            <p>{{$quote_item_detail->quoteItem->resource_name}}</p>
                                          </div>
                                          <div class="single_resource_col">
                                            <div class="form-group custom-form-select">
                                              <select class="custom-select cat_service hasvalue colInput" name="resource_price[{{$counter}}][]">
                                                  <option class="form-select-placeholder"></option>
                                                  <option value="{{$quote_item_detail->quoteItem->price_per_hour_rental}}" @if($quote_item_detail->resource_price == $quote_item_detail->quoteItem->price_per_hour_rental) selected @endif>{{$quote_item_detail->quoteItem->price_per_hour_rental}} /Per Hour</option>
                                                  <option value="{{$quote_item_detail->quoteItem->price_per_day_rental}}" @if($quote_item_detail->resource_price == $quote_item_detail->quoteItem->price_per_day_rental) selected @endif>{{$quote_item_detail->quoteItem->price_per_day_rental}} /Per Day</option>
                                                  <option value="{{$quote_item_detail->quoteItem->price_per_week_rental}}" @if($quote_item_detail->resource_price == $quote_item_detail->quoteItem->price_per_week_rental) selected @endif>{{$quote_item_detail->quoteItem->price_per_week_rental}} /Per Week</option>
                                                  <option value="{{$quote_item_detail->quoteItem->price_per_month_rental}}" @if($quote_item_detail->resource_price == $quote_item_detail->quoteItem->price_per_month_rental) selected @endif>{{$quote_item_detail->quoteItem->price_per_month_rental}} /Per Month</option>
                                              </select>
                                              <div class="form-element-bar">
                                              </div>
                                            </div>
                                          </div>
                                          <div class="single_resource_col">
                                            <div class="form-group custom-form-select bmd-form-group">      
                                              <input type="number" class="form-control form_input_field colInput" name="resource_unit[{{$counter}}][]" value="{{$quote_item_detail->resource_unit}}">
                                            </div>
                                          </div>
                                          <div class="single_resource_col">
                                            <div class="form-group custom-form-select bmd-form-group">      
                                              <input type="number" class="form-control form_input_field colInput" name="resource_duration[{{$counter}}][]" value="{{$quote_item_detail->resource_duration}}">
                                            </div>
                                          </div>
                                          <div class="single_resource_col colTotal">
                                            ${{$quote_item_detail->item_detail_total}}
                                          </div>
                                          <input type="hidden" class="colTotal_hidden_input" name="item_detail_total[{{$counter}}][]" value="{{$quote_item_detail->item_detail_total}}">
                                        </div>

                                        <div class="quote_col variable_main_section">
                                            <div class="all_single_resounce_header">
                                              <div class="single_resource_col varaible_name">
                                                <p>Name of variable</p>
                                              </div>
                                              <div class="single_resource_col single_resource_paramenters">
                                                <div class="single_resource_paramenters_row">
                                                  <div class="single_resource_col">
                                                    <p>Parameter</p>
                                                  </div>
                                                  <div class="single_resource_col">
                                                    <p>Data type</p>
                                                  </div>
                                                  <div class="single_resource_col">
                                                    <p>Value</p>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="single_resource_col total_variable">
                                                <p>Total</p>
                                              </div>
                                            </div>
                                            
                                          <?php 
                                            $variables=App\Models\QuoteVariable::where('quote_item_type', 'inventory')->where('quote_item_id', $quote_item_detail->id)->get();
                                            $resource_var_param_counter = 0;
                                            ?>

                                            @foreach($variables as $variable)
                                            <div class="all_single_resounce">
                                              <div class="single_resource_col varaible_name">
                                                <input type="hidden" name="resource_var_key[{{$counter}}][0][]" value="{{$variable->id}}">
                                                <input type="hidden" name="resource_var_name[{{$counter}}][0][]" value="{{$variable->var_name}}">
                                                <p>{{$variable->var_name}}</p>
                                              </div>
                                              <div class="single_resource_col single_resource_paramenters">
                                              <?php
                                                $var_params = App\Models\QuoteVariableParameter::where('quote_item_var_id', $variable->id)->get();
                                              ?>
                                              @foreach($var_params as $var_param)
                                                <div class="single_resource_paramenters_row">
                                                  <div class="single_resource_col">
                                                    <input type="hidden" name="resource_var_param_key[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="{{$var_param->id}}">
                                                    <input type="hidden" name="resource_var_param_name[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_name}}">
                                                    <p>{{$var_param->var_param_name}}</p>
                                                  </div>
                                                  <div class="single_resource_col">
                                                    <input type="hidden" name="resource_var_param_data_type[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_data_type}}">
                                                    <p>{{$var_param->var_param_data_type}}</p>
                                                  </div>
                                                  <div class="single_resource_col">

                                                  @if($var_param->var_param_value_type == 0)
                                                    @if($var_param->var_param_data_type == 'decimal' || $var_param->var_param_data_type == 'integer')
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="0">

                                                      <input type="hidden" class="multiply" name="resource_var_param_value[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">

                                                    @elseif($var_param->var_param_data_type == 'percentage')
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="0">

                                                      <input type="hidden" class="percentage" name="resource_var_param_value[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">

                                                    @else
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="0">

                                                      <input type="hidden" class="" name="resource_var_param_value[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">

                                                    @endif

                                                    <p>{{$var_param->var_param_value}}</p>
                                                  
                                                  @else
                                                    @if($var_param->var_param_data_type == 'decimal')
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="1">

                                                      <input type="number" class="form-control form_input_field multiply" step="0.01" name="resource_var_param_value[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @elseif($var_param->var_param_data_type == 'integer')
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="1">

                                                      <input type="number" class="form-control form_input_field multiply" name="resource_var_param_value[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @elseif($var_param->var_param_data_type == 'datetime')
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="1">

                                                      <input type="text" class="form-control form_input_field datepicker" name="resource_var_param_value[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @elseif($var_param->var_param_data_type == 'pecentage')
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="1">

                                                      <input type="text" class="form-control form_input_field percentage" name="resource_var_param_value[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @else
                                                    <div class="form-group custom-form-select bmd-form-group">
                                                      <input type="hidden" name="resource_var_param_value_type[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="1">

                                                      <input type="text" class="form-control form_input_field" name="resource_var_param_value[{{$counter}}][0][{{$resource_var_param_counter}}][]" value="{{$var_param->var_param_value}}">
                                                    </div>
                                                    @endif

                                                  @endif
                                                  </div>
                                                </div>
                                              
                                              @endforeach

                                              </div>
                                              <div class="single_resource_col total_variable total_variable_count">
                                                <p>
                                                  @if($variable->item_var_total == NULL) 0 @else{{$variable->item_var_total}}
                                                  @endif
                                                </p>
                                                <input type="hidden" name="item_var_total[{{$counter}}][0][]" class="total_variable_hidden" value="{{$variable->item_var_total}}">
                                              </div>
                                              </div>
                                                <?php $resource_var_param_counter++; ?>
                                            @endforeach
                                            </div>
                                            

                                          </div>
                                          @endforeach
                                        </div>
                                      

                                      </div>
                                    </div>
                                  </div>
                                

                                <div class="card service_loaction_net_total_section">
                                  <div class="card-body service_loaction_net_tota">
                                    <div class="service_location">

                                      <input type="hidden" class="address_selected" name="address_selected[{{$counter}}][]" value="{{$rowdata->service_location_address_id}}">
                                      <input type="hidden" class="new_address_hidden_field line1" name="line1[{{$counter}}][]" value="">
                                      <input type="hidden" class="new_address_hidden_field line2" name="line2[{{$counter}}][]" value="">
                                      <input type="hidden" class="new_address_hidden_field city" name="city[{{$counter}}][]" value="">
                                      <input type="hidden" class="new_address_hidden_field country" name="country[{{$counter}}][]" value="">
                                      <input type="hidden" class="new_address_hidden_field subdivisioncode" name="subdivisioncode[{{$counter}}][]" value="">
                                      <input type="hidden" class="new_address_hidden_field postalcode" name="postalcode[{{$counter}}][]" value="">

                                      <button class="btn btn-info btn-lg btn-round quote_address_btn" type="button" counter="{{$counter}}">Service Location</button>
                                      <div id="myModal{{$counter}}" class="modal addressModal" style="display: none;">
                                      </div>
                                    </div>

                                    <div class="card selected_address_card">
                                      <div class="card-body">
                                        <div id="address_viewer" class="">
                                          <h6> Selected Address </h6>
                                          <hr class="head-hr">
                                          @if($rowdata->service_location_address_id == 'default')
                                          <?php 
                                          $addr = App\Models\Customer::where('id', $quotation->customer_id)->first();
                                          ?>
                                          @else
                                          <?php 
                                          $addr = App\Models\ServiceLocationAddress::where('id', $rowdata->service_location_address_id)->first();
                                          ?>
                                          @endif

                                          <p> {{$addr->shipAddr_line1}} </p>
                                          <p> {{$addr->shipAddr_line2}} </p>
                                          <p> {{$addr->shipAddr_city}} </p>
                                          <p> {{$addr->shipAddr_country}} </p>
                                          <p> {{$addr->shipAddr_countrySubDivisionCode}} </p>
                                          <p> {{$addr->shipAddr_postalCode}} </p>
                                          
                                        </div>
                                      </div>
                                    </div>

                                    <div class="net_total">
                                      <div class="net_total_single">
                                        <p class="tr_outside_total_unit_cost">Total Unit Cost<span>${{$rowdata->item_total}}</span></p>
                                        <input type="hidden" class="tr_outside_total_unit_cost_input_hidden" name="item_total[{{$counter}}][]" value="{{$rowdata->item_total}}">
                                      </div>
                                       <div class="net_total_single">
                                        <p class="tr_outside_var_cost_1">Total Variable Cost <span>${{$rowdata->var_total}}</span></p>
                                        <input type="hidden" name="var_total[{{$counter}}][]" class="final_total_variable_hidden" value="{{$rowdata->var_total}}">
                                      </div>
                                      <hr style="width:40%;margin-right:0;">
                                      <div class="net_total_single tr_outside_gross_cost">
                                        <p>Gross Total <span>${{$rowdata->gross_total}}</span></p>
                                        <input type="hidden" name="gross_total[{{$counter}}][]" class="final_gross_total_hidden" value="{{$rowdata->gross_total}}">
                                      </div>

                                    </div>
                                  </div>
                                </div>

                                <div class="cross">
                                  <a href="#" class="btn btn-danger btn-round">
                                    <i class="material-icons">close</i>
                                  </a>
                                </div>
                              </div>
                            @endif
                            <?php $i++; 
                                  $counter++;
                            ?>
                            @endforeach

                            </div>
                                                          

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="card final_result">
                        
                        <div class="card-body service_loaction_net_tota">
                          <div class="net_total ml-auto">
                              <div class="net_total_single total_gross_count">
                                <p>Total Gross Cost<span>$@if($quotation->total_gross_cost == Null)0 @else{{$quotation->total_gross_cost}}@endif</span></p>
                                <input type="hidden" name="total_gross_cost" class="total_gross_count_hidden_input" value="{{$quotation->total_gross_cost}}">
                              </div>
                              <div class="net_total_single">
                                <p>Discount Coupon <span>$0</span></p>
                              </div>
                              <div class="net_total_single">
                                <p>Tax <span>$0</span></p>
                              </div>
                              <hr style="width:40%;margin-right:0;">
                              <div class="net_total_single">
                                <p>Net Total<span>$0</span></p>
                              </div>
                            </div>  
                        </div>

                      </div>
                      
                      <div class="form-group bmd-form-group save_btn_wrapper text-right">
                        <button type="submit" class="btn custom-btn-one save_btn">Update<div class="ripple-container"></div></button>
                      </div>
                    </form>

                  </div>
  
                    
                    
                </div>
              </div>



              <div class="card-body">
                
              </div>
          </div>
        </div>
      </div>

<script>
    $('.service_rental').change(function() {
      if (this.checked) {
          $('#service').show();
          $('#rental').hide();
      }
      else{
          $('#rental').show();
          $('#service').hide();
      }        
    });


////Cat//////
  $('.cat_service').change(function(){
    if($(this).val() != ''){
          let value = $(this).val();
          let _token = $('input[name="_token"]').val();

           $.ajax({
              url:"{{ route('getSubcategory') }}",
              method:"POST",
              data:{value:value, _token:_token, type:'product'},
              success:function(result)
              {
               $('.subcat_service').html(result);
              }
           })
        }
    });
    $('.cat_rental').change(function(){
    if($(this).val() != ''){
          let value = $(this).val();
          let _token = $('input[name="_token"]').val();

           $.ajax({
              url:"{{ route('getSubcategory') }}",
              method:"POST",
              data:{value:value, _token:_token, type:'inventory'},
              success:function(result)
              {
               $('.subcat_rental').html(result);
              }
           })
        }
    });
////Cat//////


////SubCat//////
  $('.subcat_service').change(function(){
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getService') }}",
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result)
            {
             $('.service').html(result);
            }
         })
      }
     });
  $('.subcat_rental').change(function(){
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getResourceInfoBySubcat') }}",
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result)
            {
             var obj = JSON.parse(result);
             if(obj[0].resource_name != null){
              $('.rental').parent().addClass('is-filled'); 
              $('.rental').val(obj[0].resource_name);
              
              $('#rental_id').val(obj[0].inventory_id);
             }

            }
         })
      }
     });
////SubCat//////
  

////Service/Resource//////
  $(".add_quote_tbl").click(function(){
      if($('#pro-sub-category-custom-select').val() === ''){
        alert('Please select a customer first');
      }
      else{
        if ($('.service_rental').is(":checked")){
          if($('#service_id').val() != ''){
              let found = false;
              $(".service_id_input").each(function(){
                 var texttocheck = $(this).val();
                 //alert(texttocheck);
                 if(texttocheck == $('#service_id').val()){
                    found = true
                 }
              });
              if(found == true){
                alert('You cannot add same service twice');
              }
              else{
                let value = $('#service_id').val();
                let _token = $('input[name="_token"]').val();
                let item_no = $('.single_card_for_single_service').length;

                let counter = $('.counter_inc').val();

                 $.ajax({
                    url:"{{ route('getServiceResourceInfo') }}",
                    method:"POST",
                    data:{value:value, _token:_token, counter:counter, item_no:item_no},
                    success:function(result)
                    {
                     $('.counter_inc').val(+ $('.counter_inc').val() + 1 );

                     $('.ajaxAppend').append(result);
                     $('.product_quotation_table_card').show();
                     ScrollEventListener($(".card_to_scroll"));
                    }
                 })
                 $('.cat_service').val('').removeClass('hasvalue').parent().addClass('is-filled');
                 $('.subcat_service').val('').removeClass('hasvalue').parent().addClass('is-filled');
                 $('.service').val('').removeClass('hasvalue').parent().addClass('is-filled');
                 $('.custom_select2_col select.blank_it').val(null).trigger('change');              
              }
            }
          else{
            alert('Please select a service first');
          }
        }
        else{
          if($('#rental_id').val() != ''){
                let value = $('#rental_id').val();
                let _token = $('input[name="_token"]').val();
                let item_no = $('.single_card_for_single_service').length;

                let counter = $('.counter_inc').val();

                 $.ajax({
                    url:"{{ route('getRentalResourceInfo') }}",
                    method:"POST",
                    data:{value:value, _token:_token, counter:counter, item_no:item_no},
                    success:function(result)
                    {
                    $('.counter_inc').val(+ $('.counter_inc').val() + 1 );

                     $('.ajaxAppend').append(result);
                     $('.product_quotation_table_card').show();
                     ScrollEventListener($(".card_to_scroll"));
                    }
                 })
               $('.cat_rental').val('').removeClass('hasvalue').parent().addClass('is-filled');
               $('.subcat_rental').val('').removeClass('hasvalue').parent().addClass('is-filled');
               $('.rental').val('').parent().removeClass('is-filled');
               $('.custom_select2_col select.blank_it').val(null).trigger('change'); 
            }
            else{
              alert('Please select a resource first');
            }     
          }
      }
  });

  $('.service').change(function(){
    $('#service_id').val($(this).val());
  });

////Service/Resource//////



$( ".custom-select" ).change(function() {
  if(this.value.length > 0){
      $( this ).addClass( "hasvalue" );
  }
})


$(document).on('input', '.colInput', function () {    
    $('.trinside').each(function(){

    var total = 1;
    $(this).find('.colInput').each(function(){
      var input = $(this).val();
      if(input.length !==0){
        total *= parseFloat(input);

      }
    });
    $(this).find('#colTotal').html('$ ' + total);
    
    });

    $('.trinside').each(function(){

    var rowTotal = 0;
    $(this).find('.colTotal').each(function(){
      var input = $(this).text().replace("$ ", "");
      if(!isNaN(input)){
        rowTotal += parseFloat(input);

      }
    });
    $(this).find('#rowTotal').html('$ ' + rowTotal);

    });


  $('.troutside').each(function(){
    var rowTotal = 0;
    $(this).find('.colTotal').each(function(){
      var input = $(this).text().replace("$ ", "");
      if(!isNaN(input)){
        rowTotal += parseFloat(input);

      }
    });
    $(this).find('#rowTotal').html('$ ' + rowTotal);  
  });


});



$( "#pro-sub-category-custom-select" ).change(function() {
    $('#companyName').html($('option:selected', this).attr('c_name'));
    $('#displayName').val($('option:selected', this).attr('d_name'));
    $('#email').val($('option:selected', this).attr('email'));
    $('#phone').val($('option:selected', this).attr('phone'));
    $('#mobile').val($('option:selected', this).attr('mobile'));

    $('.customers_details').show();
});

function rand_str() {
      const list = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
      var res = "";
      for(var i = 0; i < 4; i++) {
          var rnd = Math.floor(Math.random() * list.length);
          res = res + list.charAt(rnd);
      }
      return res;
}

let quote_ticket_1st = Math.floor(1000 + Math.random() * 9000);
let quote_ticket_2nd = rand_str();

$('#quote_ticket').val(quote_ticket_1st + '-' + quote_ticket_2nd);

$(document).on('focus',".datepicker", function(){
    $(this).datetimepicker({
      format: 'MM/DD/YYYY',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });

});


$(document).on('click','.quote_address_btn',function(){
        let value = $('#pro-sub-category-custom-select').val();
        let _token = $('input[name="_token"]').val();
        let counter = $(this).attr('counter');
        let service_location_id = $(this).parent().find('.address_selected').val();
        let line1 = $(this).parent().find('.line1').val();
        let line2 = $(this).parent().find('.line2').val();
        let city = $(this).parent().find('.city').val();
        let country = $(this).parent().find('.country').val();
        let subdivisioncode = $(this).parent().find('.subdivisioncode').val();
        let postalcode = $(this).parent().find('.postalcode').val();

        //alert(service_location_id);

         $.ajax({
            url:"{{ route('getAddress') }}",
            method:"POST",
            data:{value:value, _token:_token, counter:counter, service_location_id:service_location_id, line1:line1, line2:line2, city:city, country:country, subdivisioncode:subdivisioncode, postalcode:postalcode},
            success:function(result)
            {
              //alert(result);
              $('#myModal'+counter+'.addressModal').html(result);
              $('#myModal'+counter).show();
            }
          })

         $(document).on('click','.close',function(){
              $('#myModal'+counter+'.address_card').addClass("out");
              setTimeout(function(){
                $('#myModal'+counter).hide();
                $('#myModal'+counter+'.address_card').removeClass("out");
              },400);


         })

})

$(document).on('change', '.modalRadio', function (e) {
    alert($(this).parent()); 
});



$(document).on('click', '.customer_update', function () {
      let customer_id = $('#pro-sub-category-custom-select').val();
      let displayName = $('#displayName').val();
      let email = $('#email').val();
      let phone = $('#phone').val();
      let mobile = $('#mobile').val();
      let _token = $('input[name="_token"]').val();

      $.ajax({
          url:"{{ route('updateCustomerQuoteInfo') }}",
          method:"POST",
          data:{customer_id:customer_id,displayName:displayName,email:email,phone:phone,mobile:mobile, _token:_token},
          success:function(result)
          {
           var obj = JSON.parse(result);
           $('#displayName').val(obj[0].displayName);
           $('#email').val(obj[0].email);
           $('#phone').val(obj[0].phone);
           $('#mobile').val(obj[0].mobile);

           $('.update_customer_msg').html('Customer data has been updated');
           $('.update_customer_msg_wrapper').show();
          }
       })
})

</script>
@endsection