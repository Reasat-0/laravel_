@extends('layouts.master')

@section('content')      


      <div class="content">
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">

                  

                  <div class="card">
                    <div class="card-header card-header-rose card-header-icon user_sub_card">
                      <div class="card-icon">
                        <i class="material-icons">face</i>
                      </div>
                      <h4 class="card-title">All Quotation</h4>
                    </div>

                    <div style="margin-top: 15px;">
                        @include('partials.messages')
                    </div>

                    <div class="card-body">
                      <div class="toolbar">
                      </div>
                      <div class="material-datatables user_datatable">
                        <table id="all_users_table" class="table table-striped table-no-bordered table-hover all_table" cellspacing="0" width="100%" style="width:100%;">
                          <thead>
                            <tr>
                              <th>Quote Ticket</th>
                              <th>Date</th>
                              <th>Customer</th>
                              <th class="disabled-sorting text-right">Actions</th>
                            </tr>
                          </thead>

                          <tbody>

                          @foreach($quotations as $rowdata)
                            <tr>
                              <td>{{$rowdata->quote_ticket}}</td>
                              <td>{{$rowdata->date}}</td>
                              <td>@if($rowdata->customers){{$rowdata->customers->displayName}}@endif</td>
                              <td class="text-right">
                                
                                <form action="{{ route('quotation.destroy',$rowdata->id) }}" method="POST">
                                    <a href="{{ route('quotation.edit',$rowdata->id) }}" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a>
                                 
                                    @csrf
                                    @method('DELETE')
                    
                                    <button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm('Are you sure you want to delete?');"><i class="material-icons">close</i></button>
                                </form>
                              
                              </td>
                            </tr>
                          @endforeach
                          
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!-- end content-->
                  </div>


                
              </div>
            </div>
        </div>
      </div>
    </div>

@endsection

<script>
  $(window).on('load', function(){
    setTimeout(function() {
       $('.material-datatables').css('overflow-x','unset');
       $('.user_datatable table thead th:last-child').css('width','100px');
    }, 100);
  });
</script>