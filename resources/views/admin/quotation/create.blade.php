@extends('layouts.master')

@section('content')

        <div class="content">
          <div class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12 mx-auto card_holder_box">
                  
                  
                  <div class="card">
                    <div class="card-header card-header-tabs card-header-rose">
                      <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                          
                          <ul class="nav nav-tabs" data-tabs="tabs">
                            <li class="nav-item">
                              <a class="nav-link active" href="#profile" data-toggle="tab">
                                <i class="material-icons">ballot</i>Quotation
                                <div class="ripple-container"></div>
                              </a>
                            </li>
                            
                          </ul>
                        </div>
                      </div>
                    </div>

                    <div style="margin-top: 15px;">
                        @include('partials.messages')
                    </div>

                    <div class="card-body">
                      <form method="post" action="{{ route('quotation.store') }}" class="form-horizontal">
                                    
                       @csrf

                      <div class="card">
                        
                       
                        <div class="card-body">
                          <div class="row justify-content-between align-items-center">
                            <div class="col-md-4 input_wrapper quote_customer_selection_div custom_select2_col">
                              <div class="form-group custom-form-select">
                                <select class="custom-select select2 customer_dropdown" id="pro-sub-category-custom-select" name="customer_id" required="true" aria-required="true">
                                  <option class="form-select-placeholder"></option>
                                  @foreach($customers as $rowdata)
                                      <option value="{{$rowdata->id}}">{{$rowdata->displayName}}</option>
                                  @endforeach
                                </select>
                                <div class="form-element-bar">
                                </div>
                                <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Customers list</label>
                              </div>
                            </div>
                            <div class="col-md-4 input_wrapper quote_date_ticked_div">
                              <div class="form-group bmd-form-group is-filled">
                                <label for="quote_copon" class="bmd-label-floating input_label">Quote Ticket</label>
                                <input type="text" class="form-control form_input_field" id="quote_ticket" name="quote_ticket" readonly="">
                              </div>
                              <div class="form-group bmd-form-group is-filled">
                                <label for="quote_copon" class="bmd-label-floating input_label">Date</label>
                                <input type="text" class="form-control datepicker" name="date" value="{{date('m/d/Y')}}">
                              </div>
                            </div>
                          </div>


                          <div class="customers_details" style="display: none">
                            <div class="row">

                              <div class="card customer-card">
                                
                                <div class="card-body">

                                  <div class="alert alert-success alert-dismissible update_customer_msg_wrapper" role="alert" style="display: none;">
                                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                      <strong class="update_customer_msg"></strong>
                                  </div>

                                  <div class="col-md-12">
                                    <div class="row">
                                      <div class="col-md-12">
                                        <h3 class="title">Company name : <br class="name_br" style="display: none;"><span id="companyName"></span></h3>
                                        <hr class="company_name_hr">
                                      </div>

                                      <div class="customer-desc input_row">
                                        <div class="col input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="category_name" class="bmd-label-floating input_label">Line 1</label>
                                            <input type="text" class="form-control form_input_field" id="billAddr_line1" name="billAddr_line1">
                                          </div>
                                        </div>
                                        <div class="col input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="category_name" class="bmd-label-floating input_label">City</label>
                                            <input type="text" class="form-control form_input_field" id="billAddr_city" name="billAddr_city">
                                          </div>

                                        </div>
                                        <div class="col input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="category_name" class="bmd-label-floating input_label">Country</label>
                                            <input type="text" class="form-control form_input_field" id="billAddr_country" name="billAddr_country">
                                          </div>

                                        </div>
                                      </div>

                                      <div class="customer-desc input_row">
                                        <div class="col input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="category_name" class="bmd-label-floating input_label">Country Subdivision Code</label>
                                            <input type="text" class="form-control form_input_field" id="billAddr_countrySubDivisionCode" name="billAddr_countrySubDivisionCode">
                                          </div>
                                        </div>
                                        <div class="col input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="category_name" class="bmd-label-floating input_label">Postal Code</label>
                                            <input type="text" class="form-control form_input_field" id="billAddr_postalCode" name="billAddr_postalCode">
                                          </div>
                                        </div>
                                        <div class="col input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="category_name" class="bmd-label-floating input_label">Account No</label>
                                            <input type="text" class="form-control form_input_field" id="account_no" name="account_no">
                                          </div>
                                        </div>
                                      </div>

                                      <div class="customer-desc input_row">
                                        <div class="col input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="category_name" class="bmd-label-floating input_label">Billing Phone</label>
                                            <input type="text" class="form-control form_input_field" id="billing_phone" name="billing_phone">
                                          </div>
                                        </div>
                                        <div class="col input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="category_name" class="bmd-label-floating input_label">Billing Email</label>
                                            <input type="text" class="form-control form_input_field" id="billing_email" name="billing_email">
                                          </div>
                                        </div>
                                        <div class="col input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="category_name" class="bmd-label-floating input_label">Billing Contact Name</label>
                                            <input type="text" class="form-control form_input_field" id="billing_contact_name" name="billing_contact_name">
                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                  </div>

                                </div>
                                <div class="text-right my-3 mr-3 save_btn_wrapper">
                                    <button type="button" class="btn btn-xs btn-success btn-round customer_update">Update</button>
                                </div>
                              </div>
                            </div>
                          </div>


                        </div>
                      </div>
                      
                      <div class="card edit_category_inner_card service_rental_card">
                        <div class="card-header card-header-rose card-header-text sub_card">
                          <div class="card-text card_text_title">
                            <h4 class="card-title">Product Quotation</h4>
                          </div>
                        </div>

                        <div class="card-body col-md-12 mx-auto increased_margin_t_cardbody">     

                          <div class="row resource_row">
                            <div class="col-md-3 justify-content-center d-flex align-items-center resource_toggle_div quote_resource_toggle_div">
                                <div class="togglebutton admin-role-toggle resource_toggle_section ">
                                    <label>
                                        <input class="service_rental" type="checkbox" name="" checked="">
                                        Service <span class="toggle"></span> Rental
                                    </label>
                                </div>
                            </div>
                            <input type="hidden" id="service_id">
                            <input type="hidden" id="rental_id">

                            <div class="col-md-9 resource_toggle_content_div quote_resource_toggle_content_div">
                                <div class="row" id="service">
                                  <div class="col-md-4 input_wrapper top_input_wrapper custom_select2_col">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select cat_service select2 blank_it">
                                          <option class="form-select-placeholder"></option>
                                        @foreach($cat->where('type', 'product') as $rowdata)
                                          <option value="{{$rowdata->id}}">{{$rowdata->category_name}}</option>
                                        @endforeach
                                      </select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label" id="vendor_resource_name_existing" for="resource_name">Category Name</label>
                                    </div>
                                  </div>
                                  <div class="col-md-4 input_wrapper custom_select2_col">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select subcat_service select2 blank_it"></select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label" for="resource_name">SubCategory Name</label>
                                    </div>
                                  </div>
                                  <div class="col-md-4 input_wrapper custom_select2_col">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select service select2 blank_it"></select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label" for="resource_name">Service Name</label>
                                    </div>
                                  </div>
                                </div>

                                <div class="row" id="rental" style="display: none">
                                  <div class="col-md-4 input_wrapper top_input_wrapper custom_select2_col">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select cat_rental select2 blank_it">
                                          <option class="form-select-placeholder"></option>
                                        @foreach($cat->where('type', 'inventory') as $rowdata)
                                          <option value="{{$rowdata->id}}">{{$rowdata->category_name}}</option>
                                        @endforeach
                                      </select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label" id="vendor_resource_name_existing" for="resource_name">Category Name</label>
                                    </div>
                                  </div>
                                  <div class="col-md-4 input_wrapper custom_select2_col">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select subcat_rental select2 blank_it" id="vendor_resource_name_select">
                                      </select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label" for="resource_name">SubCategory Name</label>
                                    </div>
                                  </div>
                                  <div class="col-md-4 input_wrapper">
                                    <div class="form-group custom-form-select">
                                      <label for="resource_code_exist" class="bmd-label-floating input_label">Resource</label>
                                      <input type="text" class="form-control form_input_field rental" value="">
                                    </div>
                                  </div>
                                </div>
                            </div>
                          </div>  

                          <div class="row quotation_product_selection_row">
                            <div class="col-md-12 text-right add_vendor_resource_btn_wrapper">
                              <button class="btn btn-warning btn-round btn-sm add_quote_tbl" type="button">
                                Add
                                <div class="ripple-container"></div>
                              </button>
                            </div>
                          </div>
                          
                          <!-- <div class="row">
                            <button class="btn btn-round btn-sm">Address</button>
                          </div> -->
                          <div id="myModal" class="modal addressModal" style="display: none;">
                          </div>    


                        </div>
                      </div>
                      <div class="card quotation_mainsection">

                        <input type="hidden" class="counter_inc" value="0">
                        
                        <!-- <div class="card single_service_rental_card">

                          <div class="card-body">
                            <div class="single_service_rental_badge service_badge">
                              <p>Item No: <span class="item_no">1 </span> - <span>Service</span></p>
                            </div>

                            <div class="card main_service_card_section">
                              <div class="service_name_main_variable_section">
                                <div class="service_name_main_variable_table">
                                  <div class="service_name_main_variable_row service_name_main_variable_header">
                                    <div class="service_name_main_variable_col">
                                      <p>Service Name</p>
                                    </div>
                                    
                                    <div class="service_name_main_variable_col">
                                      <p>Service Variable Section</p>
                                    </div>
                                  </div>
                                  <div class="service_name_main_variable_row">
                                    <div class="service_name_main_variable_col">
                                      <p>Pit Cleaning</p>
                                    </div>
                                   
                                    <div class="service_name_main_variable_col">
                                      <div class="card single_service_under_variables_section">
                                        <div class="card-body variable_card_table">
                                          <div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <p>Name</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <p>Parameter</p>
                                            </div>
                                            
                                            <div class="variable_card_section_col">
                                              <p>Total</p>
                                            </div>
                                          </div>
                                          <div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <p>Fresh Water</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <div class="Parameter_with_value">
                                                <p>cost per barrel</p>
                                                <p>$120</p>
                                              </div>
                                              <div class="Parameter_with_value">
                                                <p>Service Operator hours</p>
                                                <p class="form-group bmd-form-group">
                                                  <input type="text" class="form-control form_input_field" name="">
                                                </p>
                                              </div>
                                              <div class="Parameter_with_value">
                                                <p>Amount Per barrel</p>
                                                <p class="form-group bmd-form-group">
                                                  <input type="number" class="form-control form_input_field" name="">
                                                </p>
                                              </div>
                                              <div class="Parameter_with_value">
                                                <p>Amount Per barrel</p>
                                                <p class="form-group bmd-form-group">
                                                  <input type="number" class="form-control form_input_field" name="">
                                                </p>
                                              </div>
                                              <div class="Parameter_with_value">
                                                <p>Amount Per barrel</p>
                                                <p class="form-group bmd-form-group">
                                                  <input type="number" class="form-control form_input_field" name="">
                                                </p>
                                              </div>
                                            </div>
                                            
                                            <div class="variable_card_section_col">
                                              <p>$200</p>
                                            </div>
                                          </div>
                                          <div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <p>Acid Water</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <div class="Parameter_with_value">
                                                <p>Price per barrel</p>
                                                <p>$120</p>
                                              </div>
                                              <div class="Parameter_with_value">
                                                <p>Amount Per barrel</p>
                                                <p class="form-group bmd-form-group">
                                                  <input type="number" class="form-control form_input_field" name="">
                                                </p>
                                              </div>
                                            </div>
                                           
                                            <div class="variable_card_section_col">
                                              $400
                                            </div>
                                          </div>

                                        </div>
                                      </div>
                                    </div>

                                  </div>
                                </div>
                              </div>

                              <div class="service_item_card_main_section_table_wrapper">
                                <div class="service_item_card_main_section">
                                  <div class="service_item_card_table_wrapper">
                                    <div class="service_item_card_table">
                                      <div class="service_item_card_row service_item_card_header">
                                        <div class="service_item_card_col">
                                          <p>Item Name</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>Item Details</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>Price</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>Total Unit</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>Total Duration</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>Total Unit Cost</p>
                                        </div>
                                      </div>
                                      <div class="service_item_card_row">
                                        <div class="service_item_card_col">
                                          <p>Pit Cleaning</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>Vacume Truck</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>140</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>2 barel</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>3 hours</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>120</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="card single_service_under_variables_section item_variables">
                                    <div class="card-body">
                                      <div class="section-title text-center">
                                        <h4>Resource Variable section</h4>
                                      </div>
                                      
                                      <div class="card single_service_under_variables_section">
                                        <div class="card-body variable_card_table">
                                          <div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <p>Name</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <p>Parameter</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <p>Total</p>
                                            </div>
                                          </div>
                                          <div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <p>Fresh Water</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <div class="Parameter_with_value">
                                                <p>Price per barrel</p>
                                                <p>$120</p>
                                              </div>
                                              <div class="Parameter_with_value">
                                                <p>Amount Per barrel</p>
                                                <p class="form-group bmd-form-group">
                                                  <input type="number" class="form-control form_input_field" name="">
                                                </p>
                                              </div>
                                            </div>
                                            
                                            <div class="variable_card_section_col">
                                              <p>$200</p>
                                            </div>
                                          </div>
                                          <div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <p>Acid Water</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <div class="Parameter_with_value">
                                                <p>Price per barrel</p>
                                                <p>$120</p>
                                              </div>
                                              <div class="Parameter_with_value">
                                                <p>Amount Per barrel</p>
                                                <p class="form-group bmd-form-group">
                                                  <input type="number" class="form-control form_input_field" name="">
                                                </p>
                                              </div>
                                            </div>
                                            
                                            <div class="variable_card_section_col">
                                              $400
                                            </div>
                                          </div>

                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="service_item_card_main_section_table_wrapper">
                                <div class="service_item_card_main_section">
                                  <div class="service_item_card_table_wrapper">
                                    <div class="service_item_card_table">
                                      <div class="service_item_card_row service_item_card_header">
                                        <div class="service_item_card_col">
                                          <p>Item Name</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>Item Details</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>Price</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>Total Unit</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>Total Duration</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>Total Unit Cost</p>
                                        </div>
                                      </div>
                                      <div class="service_item_card_row">
                                        <div class="service_item_card_col">
                                          <p>Pit Cleaning</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>Vacume Truck</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>140</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>2 barrel</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>3 hours</p>
                                        </div>
                                        <div class="service_item_card_col">
                                          <p>120</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="card single_service_under_variables_section item_variables">
                                    <div class="card-body">
                                      <div class="section-title text-center">
                                        <h4>Resource Variable section</h4>
                                      </div>
                                      
                                      <div class="card single_service_under_variables_section">
                                        <div class="card-body variable_card_table">
                                          <div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <p>Name</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <p>Parameter</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <p>Total</p>
                                            </div>
                                          </div>
                                          <div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <p>Fresh Water</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <div class="Parameter_with_value">
                                                <p>Price per barrel</p>
                                                <p>$120</p>
                                              </div>
                                              <div class="Parameter_with_value">
                                                <p>Amount Per barrel</p>
                                                <p class="form-group bmd-form-group"><input type="number" class="form-control form_input_field" name=""></p>
                                              </div>
                                              <div class="Parameter_with_value">
                                                <p>Amount Per barrel</p>
                                                <p class="form-group bmd-form-group"><input type="number" class="form-control form_input_field" name=""></p>
                                              </div>
                                            </div>
                                            
                                            <div class="variable_card_section_col">
                                              <p>$200</p>
                                            </div>
                                          </div>
                                          <div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <p>Acid Water</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <div class="Parameter_with_value">
                                                <p>Amount Per barrel</p>
                                                <p>30</p>
                                              </div>
                                              <div class="Parameter_with_value">
                                                <p>Amount Per barrel</p>
                                                <p class="form-group bmd-form-group"><input type="number" class="form-control form_input_field" name=""></p>
                                              </div>
                                              <div class="Parameter_with_value">
                                                <p>Amount Per barrel</p>
                                                <p class="form-group bmd-form-group"><input type="number" class="form-control form_input_field" name=""></p>
                                              </div>
                                            </div>
                                            
                                            <div class="variable_card_section_col">
                                              $400
                                            </div>
                                          </div>

                                          <div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <p>Schedule</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <div class="Parameter_with_value">
                                                <p>Date/Time</p>
                                                <p class="form-group bmd-form-group">
                                                  <input type="text" class="form-control datepicker" name="date" value="06/14/2020">
                                                </p>
                                              </div>
                                              
                                            </div>
                                            
                                            <div class="variable_card_section_col">
                                              $400
                                            </div>
                                          </div>

                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="card service_loaction_net_total_section">
                              <div class="card-body service_loaction_net_tota">
                                <div class="service_location">
                                  <button class="btn btn-info btn-lg btn-round quote_address_btn" type="button">Service Location</button>
                                </div>
                                <div class="net_total">
                                  <div class="net_total_single">
                                    <p>Total Unit Cost<span>$200</span></p>
                                  </div>
                                  <div class="net_total_single">
                                    <p>Disposer Fee <span>$200</span></p>
                                  </div>
                                  <div class="net_total_single">
                                    <p>Fresh Water <span>$200</span></p>
                                  </div>
                                  <hr style="width:40%;margin-right:0;">
                                  <div class="net_total_single">
                                    <p>Gross Total<span>$200</span></p>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="cross">
                              <a href="#" class="btn btn-danger btn-round">
                                <i class="material-icons">close</i>
                              </a>
                            </div>
                          </div>
                        </div>

                        <div class="card single_service_rental_card">

                          <div class="card-body">

                            <div class="single_service_rental_badge service_badge">
                              <p>Item No: <span class="item_no">2 </span> - <span>Rental</span></p>
                            </div>

                            <div class="card rental_card_body">
                              <div class="rental_card_body_table">
                                <div class="rental_card_body_row rental_card_body_header">
                                  <div class="rental_column">
                                    <p>Item Name</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>Item Details</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>Price</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>Total Unit</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>Total Duration</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>Total Unit Cost</p>
                                  </div>
                                </div>
                                <div class="rental_card_body_row">
                                  <div class="rental_column">
                                    <p>itename</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>item details</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>230</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>5</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>5 hours</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>349</p>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="card single_service_under_variables_section item_variables rental_variable_card_section">
                              <div class="card-body">
                                <div class="section-title text-center">
                                  <h4>Resource Variable section</h4>
                                </div>
                                
                                <div class="card single_service_under_variables_section">
                                  <div class="card-body variable_card_table">
                                    <div class="variable_card_section_row">
                                      <div class="variable_card_section_col">
                                        <p>Name</p>
                                      </div>
                                      <div class="variable_card_section_col">
                                        <p>Parameter</p>
                                      </div>
                                      <div class="variable_card_section_col">
                                        <p>Total</p>
                                      </div>
                                    </div>
                                    <div class="variable_card_section_row">
                                      <div class="variable_card_section_col">
                                        <p>Fresh Water</p>
                                      </div>
                                      <div class="variable_card_section_col">
                                        <div class="Parameter_with_value">
                                          <p>Price per barrel</p>
                                          <p>$120</p>
                                        </div>
                                        <div class="Parameter_with_value">
                                          <p>Amount Per barrel</p>
                                          <p class="form-group bmd-form-group">
                                            <input type="number" class="form-control form_input_field" name="">
                                          </p>
                                        </div>
                                      </div>
                                      
                                      <div class="variable_card_section_col">
                                        <p>$200</p>
                                      </div>
                                    </div>
                                    <div class="variable_card_section_row">
                                      <div class="variable_card_section_col">
                                        <p>brine water</p>
                                      </div>
                                      <div class="variable_card_section_col">
                                        <div class="Parameter_with_value">
                                          <p>Price per barrel</p>
                                          <p>$120</p>
                                        </div>
                                        <div class="Parameter_with_value">
                                          <p>Amount Per barrel</p>
                                          <p class="form-group bmd-form-group">
                                            <input type="number" class="form-control form_input_field" name="">
                                          </p>
                                        </div>
                                      </div>
                                      
                                      <div class="variable_card_section_col">
                                        $400
                                      </div>
                                    </div>

                                  </div>
                                </div>

                              </div>
                            </div>

                            <div class="card service_loaction_net_total_section">
                              <div class="card-body service_loaction_net_tota">
                                <div class="service_location">
                                  <button class="btn btn-info btn-lg btn-round quote_address_btn" type="button">Service Location</button>
                                </div>
                                <div class="net_total">
                                  <div class="net_total_single">
                                    <p>Total Unit Cost<span>$200</span></p>
                                  </div>
                                  <div class="net_total_single">
                                    <p>Disposer Fee <span>$200</span></p>
                                  </div>
                                  <div class="net_total_single">
                                    <p>Fresh Water <span>$200</span></p>
                                  </div>
                                  <hr style="width:40%;margin-right:0;">
                                  <div class="net_total_single">
                                    <p>Gross Total<span>$200</span></p>
                                  </div>
                                </div>
                              </div>
                            </div>

                          <div class="cross">
                            <a href="#" class="btn btn-danger btn-round">
                              <i class="material-icons">close</i>
                            </a>
                          </div>

                          </div>
                        </div> -->

                      </div>

                      <div class="card final_result" style="display: none;">
                        
                        <div class="card-body service_loaction_net_tota">

                          <div class="card evo_sp_man_card">
                            <div class="card-body">
                              <div id="address_viewer" class="">
                                <h6> EVO Representative's Contact Name :  </h6>
                                <hr class="selected-head-hr">
                                <p> @if (!Auth::guest()) 
                                      {{ auth()->user()->name }} - {{ auth()->user()->email }} 
                                    @endif
                                </p>
                              </div>
                            </div>
                          </div>



                          <div class="net_total ml-auto">
                              <div class="net_total_single total_gross_count">
                                <p class="final_gross_count">Total Gross Cost<span>$0</span></p>
                                <input type="hidden" name="total_gross_cost" class="final_gross_count_hidden_input">
                              </div>
                              <div class="net_total_single">
                                <p>Discount Coupon <span>$0</span></p>
                              </div>
                              <div class="net_total_single">
                                <p>Tax <span>$0</span></p>
                              </div>
                              <hr style="width:40%;margin-right:0;">
                              <div class="net_total_single">
                                <p class="final_net_count">Net Total<span>$0</span></p>
                                <input type="hidden" name="total_net_cost" class="final_net_count_hidden_input">
                              </div>
                            </div>  
                        </div>

                      </div>

                      <div class="form-group bmd-form-group save_btn_wrapper text-right">
                        <button type="submit" class="btn custom-btn-one save_btn">Save<div class="ripple-container"></div></button>
                      </div>
                    </form>

                    </div>
                      
                      

                  </div>
  
                    
                    
                </div>
              </div>
          </div>
        </div>
      </div>

<script>
    $('.service_rental').change(function() {
      if (this.checked) {
          $('#service').show();
          $('#rental').hide();
      }
      else{
          $('#rental').show();
          $('#service').hide();
      }        
    });


////Cat//////
  $('.cat_service').change(function(){
    if($(this).val() != ''){
          let value = $(this).val();
          let _token = $('input[name="_token"]').val();

           $.ajax({
              url:"{{ route('getSubcategory') }}",
              method:"POST",
              data:{value:value, _token:_token, type:'product'},
              success:function(result)
              {
               $('.subcat_service').html(result);
              }
           })
        }
    });
    $('.cat_rental').change(function(){
    if($(this).val() != ''){
          let value = $(this).val();
          let _token = $('input[name="_token"]').val();

           $.ajax({
              url:"{{ route('getSubcategory') }}",
              method:"POST",
              data:{value:value, _token:_token, type:'inventory'},
              success:function(result)
              {
               $('.subcat_rental').html(result);
              }
           })
        }
    });
////Cat//////


////SubCat//////
  $('.subcat_service').change(function(){
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getService') }}",
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result)
            {
             $('.service').html(result);
            }
         })
      }
     });
  $('.subcat_rental').change(function(){
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getResourceInfoBySubcat') }}",
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result)
            {
             var obj = JSON.parse(result);
             if(obj[0].resource_name != null){
              $('.rental').parent().addClass('is-filled'); 
              $('.rental').val(obj[0].resource_name);
              
              $('#rental_id').val(obj[0].inventory_id);
             }

            }
         })
      }
     });
////SubCat//////
  

////Service/Resource//////
  $(".add_quote_tbl").click(function(){
      if($('#pro-sub-category-custom-select').val() === ''){
        alert('Please select a customer first');
      }
      else{
        if ($('.service_rental').is(":checked")){
          if($('#service_id').val() != ''){
              let found = false;
              $(".service_id_input").each(function(){
                 var texttocheck = $(this).val();
                 //alert(texttocheck);
                 if(texttocheck == $('#service_id').val()){
                    found = true
                 }
              });
              if(found == true){
                alert('You cannot add same service twice');
              }
              else{
                let value = $('#service_id').val();
                let _token = $('input[name="_token"]').val();
                let item_no = $('.single_service_rental_card').length;
                
                let counter = $('.counter_inc').val();

                 $.ajax({
                    url:"{{ route('getServiceResourceInfo') }}",
                    method:"POST",
                    data:{value:value, _token:_token, counter:counter, item_no:item_no},
                    success:function(result)
                    {
                       $('.editable-select').editableSelect();
                     $('.counter_inc').val(+ $('.counter_inc').val() + 1 );

                     $('.quotation_mainsection').append(result);
                     //$('.product_quotation_table_card').show();
                     $('.final_result').show();
                     $(".select2Price").select2({
                        placeholder: "Select or input price",
                        tags: true
                      });
                     //ScrollEventListener($(".card_to_scroll"));
                    }
                 })
                 $('.cat_service').val('').removeClass('hasvalue').parent().addClass('is-filled');
                 $('.subcat_service').val('').removeClass('hasvalue').parent().addClass('is-filled');
                 $('.service').val('').removeClass('hasvalue').parent().addClass('is-filled');
                 $('.custom_select2_col select.blank_it').val(null).trigger('change');              
              }
            }
          else{
            alert('Please select a service first');
          }
        }
        else{
          if($('#rental_id').val() != ''){
                let value = $('#rental_id').val();
                let _token = $('input[name="_token"]').val();
                let item_no = $('.single_service_rental_card').length;

                let counter = $('.counter_inc').val();

                 $.ajax({
                    url:"{{ route('getRentalResourceInfo') }}",
                    method:"POST",
                    data:{value:value, _token:_token, counter:counter, item_no:item_no},
                    success:function(result)
                    {
                     $('.counter_inc').val(+ $('.counter_inc').val() + 1 );

                     $('.quotation_mainsection').append(result);
                     //$('.product_quotation_table_card').show();
                     $('.final_result').show();
                     $(".select2Price").select2({
                        placeholder: "Select or input price",
                        tags: true
                      });
                     ScrollEventListener($(".card_to_scroll"));
                    }
                 })
                $('.cat_rental').val('').removeClass('hasvalue').parent().addClass('is-filled');
                $('.subcat_rental').val('').removeClass('hasvalue').parent().addClass('is-filled');
                $('.custom_select2_col select.blank_it').val(null).trigger('change');                
                $('.rental').val('').parent().removeClass('is-filled');
            }
            else{
              alert('Please select a resource first');
            }     
          }
      }
  });

  $('.service').change(function(){
    $('#service_id').val($(this).val());
  });

////Service/Resource//////



$( ".custom-select" ).change(function() {
  if(this.value.length > 0){
      $( this ).addClass( "hasvalue" );
  }
})


$(document).on('input', '.colInput', function () {
    $('.trinside').each(function(){

    var total = 1;
    $(this).find('.colInput').each(function(){
      var input = $(this).val();
      if(input.length !==0){
        total *= parseFloat(input);

      }
    });
    $(this).find('#colTotal').html('$ ' + total);
    
    });

    $('.trinside').each(function(){

    var rowTotal = 0;
    $(this).find('.colTotal').each(function(){
      var input = $(this).text().replace("$ ", "");
      if(!isNaN(input)){
        rowTotal += parseFloat(input);

      }
    });
    $(this).find('#rowTotal').html('$ ' + rowTotal);

    });


  $('.troutside').each(function(){
    var rowTotal = 0;
    $(this).find('.colTotal').each(function(){
      var input = $(this).text().replace("$ ", "");
      if(!isNaN(input)){
        rowTotal += parseFloat(input);

      }
    });
    $(this).find('#rowTotal').html('$ ' + rowTotal);  
  });


});



/*$( "#pro-sub-category-custom-select" ).change(function() {
    $('#companyName').html($('option:selected', this).attr('c_name'));
    $('#displayName').val($('option:selected', this).attr('d_name'));
    $('#email').val($('option:selected', this).attr('email'));
    $('#phone').val($('option:selected', this).attr('phone'));
    $('#mobile').val($('option:selected', this).attr('mobile'));

    $('.customers_details').show();
});*/

function rand_str() {
      const list = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
      var res = "";
      for(var i = 0; i < 4; i++) {
          var rnd = Math.floor(Math.random() * list.length);
          res = res + list.charAt(rnd);
      }
      return res;
}

let quote_ticket_1st = Math.floor(1000 + Math.random() * 9000);
let quote_ticket_2nd = rand_str();

$('#quote_ticket').val(quote_ticket_1st + '-' + quote_ticket_2nd);

$(document).on('focus',".datepicker", function(){
    $(this).datetimepicker({
      format: 'MM/DD/YYYY',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });

});


$(document).on('click','.quote_address_btn',function(){
        let value = $('#pro-sub-category-custom-select').val();
        let _token = $('input[name="_token"]').val();
        let counter = $(this).attr('counter');
        let service_location_id = $(this).parent().find('.address_selected').val();
        let line1 = $(this).parent().find('.line1').val();
        let line2 = $(this).parent().find('.line2').val();
        let city = $(this).parent().find('.city').val();
        let country = $(this).parent().find('.country').val();
        let subdivisioncode = $(this).parent().find('.subdivisioncode').val();
        let postalcode = $(this).parent().find('.postalcode').val();

        let lease_name = $(this).parent().find('.lease_name').val();
        let rig_number = $(this).parent().find('.rig_number').val();
        let afe_number = $(this).parent().find('.afe_number').val();
        let site_contact_no = $(this).parent().find('.site_contact_no').val();
        let rig_contact_email = $(this).parent().find('.rig_contact_email').val();
        let company_man_contact_name = $(this).parent().find('.company_man_contact_name').val();
        
         $.ajax({
            url:"{{ route('getAddress') }}",
            method:"POST",
            data:{value:value, _token:_token, counter:counter, service_location_id:service_location_id, line1:line1, line2:line2, city:city, country:country, subdivisioncode:subdivisioncode, postalcode:postalcode, lease_name:lease_name, rig_number:rig_number, afe_number:afe_number, site_contact_no:site_contact_no, rig_contact_email:rig_contact_email, company_man_contact_name:company_man_contact_name},
            success:function(result)
            {
              $('#myModal' +counter+ '.addressModal').html(result);
              $('#myModal'+counter).show();
             //$('.ajaxAppend').append(result);
             //$('.product_quotation_table_card').show();
             //ScrollEventListener($(".card_to_scroll"));
            }
          })

         $(document).on('click','.close',function(){
              $('.address_card').addClass("out");
              setTimeout(function(){
                $('#myModal'+counter).hide();
                $('.address_card').removeClass("out");
              },400);


         })

})

/*$(document).on('change', '.modalRadio', function (e) {
    alert($(this).parent()); 
});*/

$(document).on('change', '.custom-select', function () {
//$( ".custom-select" ).change(function() {
  //alert('okkk');
      if(this.value.length > 0){
          $( this ).addClass( "hasvalue" );
      }
})

$('.customer_dropdown').change(function(){
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getCustomerDetails') }}",
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result)
            { 
              $('#companyName').html(result.companyName);
              $('#account_no').val(result.account_no).parent().addClass('is-filled'); ;
              $('#billAddr_line1').val(result.billAddr_line1).parent().addClass('is-filled');
              $('#billAddr_city').val(result.billAddr_city).parent().addClass('is-filled');
              $('#billAddr_postalCode').val(result.billAddr_postalCode).parent().addClass('is-filled');
              $('#billAddr_country').val(result.billAddr_country).parent().addClass('is-filled');
              $('#billAddr_countrySubDivisionCode').val(result.billAddr_countrySubDivisionCode).parent().addClass('is-filled'); 
              $('#billing_phone').val(result.billing_phone).parent().addClass('is-filled');
              $('#billing_email').val(result.billing_email).parent().addClass('is-filled');
              $('#billing_contact_name').val(result.billing_contact_name).parent().addClass('is-filled');
              $('.customers_details').show();
            },
            dataType: "json"
         })
      }
});

$(document).on('click', '.customer_update', function () {
      let customer_id = $('#pro-sub-category-custom-select').val();
      let account_no = $('#account_no').val();
      let billAddr_line1 = $('#billAddr_line1').val();
      let billAddr_city = $('#billAddr_city').val();
      let billAddr_country = $('#billAddr_country').val();
      let billAddr_countrySubDivisionCode = $('#billAddr_countrySubDivisionCode').val();
      let billAddr_postalCode = $('#billAddr_postalCode').val();
      let billing_phone = $('#billing_phone').val();
      let billing_email = $('#billing_email').val();
      let billing_contact_name = $('#billing_contact_name').val();
      let _token = $('input[name="_token"]').val();

      $.ajax({
          url:"{{ route('updateCustomerQuoteInfo') }}",
          method:"POST",
          data:{customer_id:customer_id,account_no:account_no,billAddr_line1:billAddr_line1,billAddr_city:billAddr_city,billAddr_country:billAddr_country,billAddr_countrySubDivisionCode:billAddr_countrySubDivisionCode,billAddr_postalCode:billAddr_postalCode,billing_phone:billing_phone,billing_email:billing_email,billing_contact_name:billing_contact_name, _token:_token},
          success:function(result)
          {
           var obj = JSON.parse(result);
           $('#account_no').val(obj[0].account_no);
           $('#billAddr_line1').val(obj[0].billAddr_line1);
           $('#billAddr_city').val(obj[0].billAddr_city);
           $('#billAddr_country').val(obj[0].billAddr_country);
           $('#billAddr_countrySubDivisionCode').val(obj[0].billAddr_countrySubDivisionCode);
           $('#billAddr_postalCode').val(obj[0].billAddr_postalCode);
           $('#billing_phone').val(obj[0].billing_phone);
           $('#billing_email').val(obj[0].billing_email);
           $('#billing_contact_name').val(obj[0].billing_contact_name);

           $('.update_customer_msg').html('Customer data has been updated');
           $('.update_customer_msg_wrapper').show();
          }
       })
})

$(document).on('select2:selecting', '.select2Price', function (e) {
    
    let data = e.params.args.data.id;
    let numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
    
    if(!numberRegex.test(data)){
      alert('Please put numeric value only for price');
      e.preventDefault();
    }
})

</script>
@endsection