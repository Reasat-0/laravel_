@extends('layouts.master')

@section('content')      


      <div class="content">
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">

                  

                  <div class="card">
                    <div class="card-header card-header-rose card-header-icon user_sub_card">
                      <div class="card-icon">
                        <i class="material-icons">face</i>
                      </div>
                      <h4 class="card-title">All Users</h4>
                    </div>

                    <div style="margin-top: 15px;">
                        @include('partials.messages')
                    </div>

                    <div class="card-body">
                      <div class="toolbar">
                      </div>
                      <div class="material-datatables user_datatable">
                        <table id="all_users_table" class="table table-striped table-no-bordered table-hover all_table" cellspacing="0" width="100%" style="width:100%;">
                          <thead>
                            <tr>
                              <th>User Name</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Role</th>
                              <th>Status</th>
                              <th class="text-right">Change Status</th>
                              <th class="disabled-sorting text-right">Actions</th>
                            </tr>
                          </thead>

                          <tbody>

                          @foreach($users as $rowdata)
                            <tr>
                              <td>{{$rowdata->name}}</td>
                              <td>{{$rowdata->email}}</td>
                              <td>{{$rowdata->phone_no}}</td>
                              <td>{{$rowdata->status}}</td>
                              <td>{{$rowdata->role}}</td>
                              @if($rowdata->role == 'admin')
                              <td class="text-left"> <a href=" {{ route('make.agent', ['id'=> $rowdata->id]) }} " class="btn btn-sm btn-link btn-warning">Make Agent</a></td>
                              @else
                              <td class="text-left"> <a href="{{ route('make.admin', ['id'=> $rowdata->id]) }} " class="btn btn-sm btn-link btn-success">Make Admin</a></td>
                              @endif

                              <td class="text-right">
                                
                                <form action="{{ route('users.destroy',$rowdata->id) }}" method="POST">
                                    <a href="{{ route('users.edit',$rowdata->name) }}" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a>
                                 
                                    @csrf
                                    @method('DELETE')
                    
                                    <button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm('Are you sure you want to delete?');"><i class="material-icons">close</i></button>
                                </form>
                              
                              </td>
                            </tr>
                          @endforeach
                          
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!-- end content-->
                  </div>


                
              </div>
            </div>
        </div>
      </div>
    </div>

@endsection

<script>
  $(window).on('load', function(){
    setTimeout(function() {
       $('.material-datatables').css('overflow-x','unset');
       $('.user_datatable table thead th:last-child').css('width','100px');
    }, 100);
  });
</script>