@extends('layouts.master')

@section('content')


<div class="content">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <form id="add_user_form" action="{{ route('users.store') }}" method="post">

                        @csrf

                        <div class="card ">
                            <div class="card-header card-header-rose card-header-icon user_sub_card">
                                <div class="card-icon">
                                    <i class="material-icons">face</i>
                                </div>
                                <h4 class="card-title box_label_title">Add User</h4>
                            </div>

                            <div style="margin-top: 15px;">
                                @include('partials.messages')
                            </div>

                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group bmd-form-group">
                                            <label for="user_name" class="bmd-label-floating input_label"> User Name *</label>
                                            <input type="text" class="form-control form_input_field" id="user_name" name="name" required aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group bmd-form-group">
                                            <label for="user_email" class="bmd-label-floating input_label"> Email Address *</label>
                                            <input type="email" class="form-control form_input_field" id="user_email" name="email" required="true" aria-required="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row input_row">
                                    <div class="col-md-6">
                                        <div class="form-group bmd-form-group">
                                            <label for="user_password" class="bmd-label-floating input_label"> Password *</label>
                                            <input type="password" class="form-control form_input_field" id="user_password" name="password" required="true" name="password" aria-required="true" autocomplete="new-password">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group bmd-form-group">
                                            <label for="user_phone" class="bmd-label-floating input_label"> Phone number</label>
                                            <input type="text" class="form-control form_input_field" id="user_phone" name="phone_no">
                                        </div>
                                    </div>
                                </div>
                                <div class="row user_stat_role">
                                    <div class="col-md-6">
                                        <div class="form-group bmd-form-group">
                                            <h5 class="title">Status</h5>
                                            <div class="togglebutton user_stat_toggle">
                                                <label>
                                                    <input type="checkbox" name="status" checked=""> Inactive &nbsp; <span class="toggle"></span> Active
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group bmd-form-group">
                                      <h5 class="title">Role</h5>
                                      <div class="togglebutton admin-role-toggle">
                                        <label>
                                            <input type="checkbox" name="role" checked="">
                                            Agent <span class="toggle"></span> Admin
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="card-footer text-right ml-auto save_btn_wrapper">
                                <button type="submit" class="btn custom-btn-one save_btn">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection