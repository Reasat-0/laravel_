@extends('layouts.master')

@section('content')

        <div class="content manage-inventory-content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12 mx-auto">
                <div class="card insurance_root_card">
                  <div class="card-header card-header-tabs card-header-rose custom_card_header mang_inventory_card_header" id="insurance_header_section">
                    <div class="nav-tabs-navigation">
                      <div class="nav-tabs-wrapper">
                        <p class="nav-tabs-title">Checklist</p>
                        <ul class="nav nav-tabs" data-tabs="tabs" id="tabMenu">

                          <li class="nav-item">
                            <a class="nav-link active" href="#pre_checklist_tab" data-toggle="tab">
                              <i class="material-icons">code</i>Generate Checklist
                              <div class="ripple-container"></div>
                            </a>
                          </li>

                          <li class="nav-item">
                            <a class="nav-link" href="#post_checklist_tab" data-toggle="tab">
                              <i class="material-icons">source</i>View Checklist
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                        

                        </ul>
                      </div>
                    </div>
                  </div>


                  <div class="card-body manage-inventory-content-card-body">
                    <div class="tab-content insurance_parent_tab_content">
                      <div class="tab-pane active" id="pre_checklist_tab">
                        <div class="card">
                          <div class="card-header card-header-tabs card-header-rose custom_card_header mng_view_card_header checklist_first_card_header" id="insurance_tab_header">
                              <!-- <span class="stepChecklist"></span>
                              <span class="stepChecklist"></span>
                              <span class="stepChecklist"></span>
                              <span class="stepChecklist"></span> -->


                            <div class="nav-tabs-navigation">
                              <div class="nav-tabs-wrapper">
                                <ul class="nav nav-tabs sub-insurance-nav" data-tabs="tabs">
                                  <li class="nav-item stepChecklist">
                                    <a class="nav-link active" href="#" data-toggle="tab">
                                      <i class="material-icons">code</i>Pre Trip Report
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li>
                                  <li class="nav-item stepChecklist">
                                    <a class="nav-link" href="#" data-toggle="tab">
                                      <i class="material-icons">code</i>Vacuum Checklist
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li>
                                  <li class="nav-item stepChecklist">
                                    <a class="nav-link" href="#" data-toggle="tab">
                                      <i class="material-icons">code</i>BBS Observation Form
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li>

                                   <li class="nav-item stepChecklist">
                                    <a class="nav-link" href="#" data-toggle="tab">
                                      <i class="material-icons">code</i> Job Analysis
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li>

                                   <li class="nav-item stepChecklist">
                                    <a class="nav-link" href="#" data-toggle="tab">
                                      <i class="material-icons">code</i>NLI Notification
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                            </div>

                            </div>
                          <!-- <div class="card-header card-header-tabs card-header-rose custom_card_header mng_view_card_header checklist_first_card_header" id="insurance_tab_header">

                          </div> -->

                          <div class="tab-content">
                            <div id="checklistForm" action="">
                              <!-- One "tab" for each step in the form: -->
                              <div class="tabChecklist">
                                <form class="pre_trip_form">
                                  <div class="card pre_trip_report_card">
                                    <div class="card-header card-header-rose card-header-text lead_card_header user_sub_card">
                                      <div class="card-icon">
                                          <i class="material-icons">supervised_user_circle</i>
                                      </div>
                                        <h4 class="card-title">Pre-Trip Report</h4>
                                    </div>
                                    <div class="card-body">

                                      <div class="row">
                                        <div class="col-md-8 ml-auto mr-auto pre_trip_report_header">
                                          <div class="row">
                                            <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="pre_trip_date_report" class="bmd-label-floating input_label">Date</label>
                                                  <input type="text" class="form-control form_input_field datepicker" id="pre_trip_date_report" name="pre_trip_date_report">
                                                </div>
                                            </div>
                                            <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="tructor_truck_no" class="bmd-label-floating input_label">Tructor/Truck No</label>
                                                  <input type="text" class="form-control form_input_field" id="tructor_truck_no" name="tructor_truck_no">
                                                </div>
                                            </div>
                                          </div>
                                          <div class="row pre_trip_report_remark">
                                            <label class="col-md-12 text-center">
                                              <span>Note</span>
                                              CHECK ANY DEFECTIVE ITEM AND GIVE DETAILS UNDER "REMARKS"
                                            </label>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="card odometer_card">
                                            <div class="card-header card-header-rose card-header-text sub_card">
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">Odometer</h4>
                                              </div>
                                            </div>
                                            <div class="card-body">
                                              <div class="row">
                                                <div class="col-md-6 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="odometer_start" class="bmd-label-floating input_label">Odometer Start</label>
                                                    <input type="text" class="form-control form_input_field " id="odometer_start" name="odometer_start">
                                                  </div>
                                                </div>
                                                <div class="col-md-6 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="odometer_end" class="bmd-label-floating input_label">Odometer Ending</label>
                                                    <input type="text" class="form-control form_input_field " id="odometer_end" name="odometer_end">
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="card odometer_card">
                                            <div class="card-header card-header-rose card-header-text sub_card">
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">Basic Equipments</h4>
                                              </div>
                                            </div>
                                            <div class="card-body">

                                              <div class="row">
                                                <div class="col-md-4">
                                                  <ul class="equipments_list">
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Air Compressor
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Air Lines
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Battery    
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Body       
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Brake Accessories           
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Brakes, Parking        
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Brakes, Service     
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Clutch
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Coupling Devices   
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Defroster/Heater     
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Drive Line   
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Engine
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Exhaust
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                  </ul>
                                                </div>
                                                <div class="col-md-4">
                                                  <ul class="equipments_list">
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Fifth Wheel
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Frame and Assembly
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Front Axle
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Fuel Tanks
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Generator
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Horn
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Lights
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                      <ul class="equipments_sublist">
                                                        <li>
                                                          <div class="form-check">
                                                            <label class="form-check-label">
                                                              <input class="form-check-input" type="checkbox" value="">Head - Stop
                                                              <span class="form-check-sign">
                                                                <span class="check"></span>
                                                              </span>
                                                            </label>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="form-check">
                                                            <label class="form-check-label">
                                                              <input class="form-check-input" type="checkbox" value="">Tail - Dash
                                                              <span class="form-check-sign">
                                                                <span class="check"></span>
                                                              </span>
                                                            </label>
                                                          </div>
                                                        </li>
                                                        <li>
                                                          <div class="form-check">
                                                            <label class="form-check-label">
                                                              <input class="form-check-input" type="checkbox" value="">Turn Indicators
                                                              <span class="form-check-sign">
                                                                <span class="check"></span>
                                                              </span>
                                                            </label>
                                                          </div>
                                                        </li>
                                                      </ul>
                                                    </li>
                                                    
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Mirros
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Muffler
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Oil Pressure
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                  </ul>
                                                </div>
                                                <div class="col-md-4">
                                                  <ul class="equipments_list">
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Radiator
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Rear End
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Reflectors
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Suspension System  
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Starter
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Steering
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Tachograph
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Tires
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Tire Chains
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Trasnsmission
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Wheels and Rims
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Windows
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Windshield Wipers
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                  </ul>
                                                </div>
                                              </div>
                                              <div class="row safety-row">
                                                <div class="col-md-4">
                                                  <h4 class="equipments_sublist_title">Safety Equipment</h4>
                                                  <ul class="equipments_list">
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Fire Extinguisher (2)
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Reflective Triangles
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Flags - Flares - First Aid Kit
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Spare Bulbs & Fuses  
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Spare Seal Beam
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                  </ul>
                                                </div>
                                                <div class="col-md-4">
                                                  <h4 class="equipments_sublist_title">Safety PPE</h4>
                                                  <ul class="equipments_list">
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Hard Hat & Gloves
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Safety Glasses / Goggles
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Boots & FRC's
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Hearing Protection
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">H2S and 4/5 Gas Monitor
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Job / Task Specific PPE
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                  </ul>
                                                </div>
                                              </div>

                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="card odometer_card">
                                            <div class="card-header card-header-rose card-header-text sub_card">
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">Other Equipments</h4>
                                              </div>
                                            </div>
                                            <div class="card-body">

                                              <div class="row input_row" style="margin-bottom: 10px;">
                                                <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="trailer_no" class="bmd-label-floating input_label">Trailer's No(S)</label>
                                                      <input type="text" class="form-control form_input_field" id="trailer_no" name="trailer_no">
                                                    </div>
                                                </div>
                                              </div>

                                              <div class="row input_row">
                                                <div class="col-md-4">
                                                  <ul class="equipments_list">
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Brake Connections
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Brakes
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Coupling Devices    
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Coupling (King) Pin    
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Doors           
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                  </ul>
                                                </div>

                                                <div class="col-md-4">
                                                  <ul class="equipments_list">
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Hitch
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Landing Gear
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Lights- All
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Roof 
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Suspension System    
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                  </ul>
                                                </div>

                                                <div class="col-md-4">
                                                  <ul class="equipments_list">
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Tarpaulin
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Tires
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Wheels and Rims
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Paperwork - Permits
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <li>
                                                      <div class="form-check">
                                                        <label class="form-check-label">
                                                          <input class="form-check-input" type="checkbox" value="">Other  
                                                          <span class="form-check-sign">
                                                            <span class="check"></span>
                                                          </span>
                                                        </label>
                                                      </div>
                                                    </li>
                                                  </ul>
                                                </div>

                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="card odometer_card">
                                            <div class="card-header card-header-rose card-header-text sub_card">
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">Others</h4>
                                              </div>
                                            </div>
                                            <div class="card-body">

                                              <div class="row">
                                                <div class="col-md-12 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="report_remarks" class="bmd-label-floating input_label">Remarks</label>
                                                    <textarea class="form-control form_input_field form_fields" rows="5" name="report_remarks" id="report_remarks"></textarea>
                                                  </div>
                                                </div>
                                              </div>

                                              <fieldset class="custom_fieldset">
                                                <legend>Time count</legend>
                                                <div class="row">
                                                  <div class="col-md-4 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="time_in" class="bmd-label-floating input_label">Time In</label>
                                                      <input type="text" class="form-control form_input_field timepicker" id="time_in" name="time_in">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-4 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="time_out" class="bmd-label-floating input_label">Time Out</label>
                                                      <input type="text" class="form-control form_input_field timepicker" id="time_out" name="time_out">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-4 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="total_hour" class="bmd-label-floating input_label">Total Hours</label>
                                                      <input type="text" class="form-control form_input_field" id="total_hour" name="total_hour">
                                                    </div>
                                                  </div>
                                                </div>
                                              </fieldset>

                                              <div class="row condition_row_pri_trip input_row">
                                                <div class="col-md-6 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="">Condition of The Above Vehicle is Satisfactory
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="driver_signature1" class="bmd-label-floating input_label">Driver's Signature</label>
                                                      <input type="text" class="form-control form_input_field" id="driver_signature1" name="driver_signature1">
                                                    </div>
                                                </div>
                                              </div>

                                              <div class="row defect_correction_row input_row">
                                                <div class="col-md-6 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="">Above Defects Corrected  
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-6 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="">Above Defects Need Not Be Corrected For Safe Operation Of Vehicle
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                              </div>

                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="card odometer_card">
                                            <div class="card-header card-header-rose card-header-text sub_card">
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">Signature</h4>
                                              </div>
                                            </div>
                                            <div class="card-body">

                                              <fieldset class="custom_fieldset_2 signature_section_fieldset">
                                                <legend>Driver's Signature</legend> 
                                                <div class="row input_row mt-2" style="margin-bottom: 20px;">
                                                  <div class="col-md-12 draw_signature_col text-center">
                                                    <label class="" for="">Signature (Drag Your cursor to sign below)</label>
                                                    <label class="sign_error_owner_msg" style="display: none;" for="">Please Put Your Signature</label>
                                                    <br/>
                                                    <div id="sig" ></div>
                                                    <input type="hidden" class="sign_input_validation_owner" value="0">
                                                    <br/>
                                                    <button id="clear" class="btn btn-sm btn-round btn-danger my-3">Clear Signature</button>
                                                    <textarea id="signature64" name="signed" style="display: none"></textarea>
                                                  </div>
                                                  <!-- <canvas id='blank' style='display:none'></canvas> -->
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-5 input_wrapper sign_date_section mx-auto">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="customer_sign_date" class="bmd-label-floating input_label">Date</label>
                                                      <input type="text" class="form-control form_input_field form_fields field_to_fill datepicker" id="pre_trip_driver_sign_date" name="pre_trip_driver_sign_date" value="">
                                                    </div>
                                                  </div>
                                                </div>
                                              </fieldset>

                                              <div class="row input_row" style="margin-bottom: 10px;">
                                                <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="mechanic_signature" class="bmd-label-floating input_label">Mechanic's Signature</label>
                                                      <input type="text" class="form-control form_input_field" id="mechanic_signature" name="mechanic_signature">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="machanic_signature_date" class="bmd-label-floating input_label">Date</label>
                                                    <input type="text" class="form-control form_input_field datepicker" id="machanic_signature_date" name="machanic_signature_date">
                                                  </div>
                                                </div>
                                              </div>

                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                </form>
                              </div>
                              <div class="tabChecklist">
                                <form id="vacume_truck_checklist_form">
                                  <div class="card vacume_truck_checklist_card" id="top_vacuum_card">
                                    <div class="card-header card-header-rose card-header-icon user_sub_card">
                                      <div class="card-icon">
                                        <i class="material-icons">assignment</i>
                                      </div>
                                      <h4 class="card-title">Vacuum Truck Operator Checklist</h4>
                                    </div>
                                    <div class="card-body">
                                      <div class="row">
                                        <div class="col-md-1 progress_container vacume_truck_checklist_arrow" style="text-align:center;margin-bottom:20px;">
                                          <span class="stepVehOpeCheck step-first active">
                                            <i class="material-icons">done</i>
                                          </span>
                                          <span class="stepVehOpeCheck step-inner">
                                            <i class="material-icons">done</i>
                                          </span>
                                          <span class="stepVehOpeCheck step-inner">
                                            <i class="material-icons">done</i>
                                          </span>
                                          <span class="stepVehOpeCheck step-inner">
                                            <i class="material-icons">done</i>
                                          </span>
                                          <span class="stepVehOpeCheck step-inner">
                                            <i class="material-icons">done</i>
                                          </span>
                                        </div>

                                        <div class="col-md-11 vacume_truck_checklist_content">
                                          <div class="tabVehOpeCheck">
                                            <div class="card">
                                              <div class="card-body">
                                                <h4 class="vehicle-checklist-title">Section 1 <span>Truck Requirements</span></h4>
                                                <div class="row vehicle-check-row">
                                                  <div class="col-md-12">
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">The cargo tank displays a name plate/specification plate confirming current DOT compliance.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="speci_plate" value="yes" checked>
                                                          <input label="No" type="radio" name="speci_plate" value="no">
                                                          <input label="N/A" type="radio" name="speci_plate" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">The cargo tank is designed to ASME Boiler and Pressure Vessel Code, Section VIII, Division 1 (or Canadian National Board) standards.  (Minimum 25 psi design and 40 psi test pressures)</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="pressure_vessel_code" value="yes" checked>
                                                          <input label="No" type="radio" name="pressure_vessel_code" value="no">
                                                          <input label="N/A" type="radio" name="pressure_vessel_code" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Truck inspection stickers for over-the-road transport and operation are current.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="truck_ins_sticker" value="yes" checked>
                                                          <input label="No" type="radio" name="truck_ins_sticker" value="no">
                                                          <input label="N/A" type="radio" name="truck_ins_sticker" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">The truck is equipped with two 20 lb. or larger dry chemical fire extinguisher rated for Class B fires.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="truck_equip" value="yes" checked>
                                                          <input label="No" type="radio" name="truck_equip" value="no">
                                                          <input label="N/A" type="radio" name="truck_equip" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Cargo tank is clean, door and dome gaskets in good condition and seal tightly.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="cargo_tank_clean" value="yes" checked>
                                                          <input label="No" type="radio" name="cargo_tank_clean" value="no">
                                                          <input label="N/A" type="radio" name="cargo_tank_clean" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Pressure valves, pressure relief (safety) valves, gaskets, and shutoff valves have been inspected, are leak tight, working properly, and, where appropriate, can be capped while in transit.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="press_valves_relief" value="yes" checked>
                                                          <input label="No" type="radio" name="press_valves_relief" value="no">
                                                          <input label="N/A" type="radio" name="press_valves_relief" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Floats for liquid level indicators move freely and are working properly.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="float_liq_level_ind" value="yes" checked>
                                                          <input label="No" type="radio" name="float_liq_level_ind" value="no">
                                                          <input label="N/A" type="radio" name="float_liq_level_ind" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row note-row">
                                                      <label>
                                                        <span>NOTE</span> If the answer to any of the statement above is “No” or “N/A”, a comment must be entered to explain.</label>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="tabVehOpeCheck">
                                            <div class="card">
                                              <div class="card-body">
                                                <h4 class="vehicle-checklist-title">Section 1 <span>Truck Requirements</span></h4>
                                                <div class="row vehicle-check-row">
                                                  <div class="col-md-12">
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">The vacuum truck is equipped with at least 50 ft. (15 meters) of loading hose and at least 50 ft. of vacuum pump vent (exhaust) hose.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="vac_equi_50" value="yes" checked>
                                                          <input label="No" type="radio" name="vac_equi_50" value="no">
                                                          <input label="N/A" type="radio" name="vac_equi_50" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Hoses and accessories are conductive and periodic inspection results are available.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="hoses_access" value="yes" checked>
                                                          <input label="No" type="radio" name="hoses_access" value="no">
                                                          <input label="N/A" type="radio" name="hoses_access" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Connections are clean and free of rust and paint.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="conn_clean" value="yes" checked>
                                                          <input label="No" type="radio" name="conn_clean" value="no">
                                                          <input label="N/A" type="radio" name="conn_clean" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Hoses, connections, and fittings are in good condition, suitable for the material being loaded/unloaded, and appropriately sized.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="hoses_conn_fitt" value="yes" checked>
                                                          <input label="No" type="radio" name="hoses_conn_fitt" value="no">
                                                          <input label="N/A" type="radio" name="hoses_conn_fitt" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">If hose > 4" (100mm), hose has protector in place.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="hoses_100mm" value="yes" checked>
                                                          <input label="No" type="radio" name="hoses_100mm" value="no">
                                                          <input label="N/A" type="radio" name="hoses_100mm" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">The truck is equipped with cables suitable for bonding the hoses and grounding the truck.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="bonding_hoss_ground" value="yes" checked>
                                                          <input label="No" type="radio" name="bonding_hoss_ground" value="no">
                                                          <input label="N/A" type="radio" name="bonding_hoss_ground" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Strong alligator clamps, special purpose C-clamps, or equivalent devices are used as connectors for bonding/grounding cables.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="strong_alligator" value="yes" checked>
                                                          <input label="No" type="radio" name="strong_alligator" value="no">
                                                          <input label="N/A" type="radio" name="strong_alligator" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Wheel chocks are available.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="wheel_chocks" value="yes" checked>
                                                          <input label="No" type="radio" name="wheel_chocks" value="no">
                                                          <input label="N/A" type="radio" name="wheel_chocks" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">PTO-driven vacuum pumps have a properly functioning engine overspeed trip device.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="PTO_driven" value="yes" checked>
                                                          <input label="No" type="radio" name="PTO_driven" value="no">
                                                          <input label="N/A" type="radio" name="PTO_driven" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row note-row">
                                                      <label>
                                                        <span>NOTE</span> If the answer to any of the statement above is “No” or “N/A”, a comment must be entered to explain.</label>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="tabVehOpeCheck">
                                            <div class="card">
                                              <div class="card-body">
                                                <h4 class="vehicle-checklist-title">Section 2 <span>Truck Operator Requirements</span></h4>
                                                <div class="row vehicle-check-row">
                                                  <div class="col-md-12">
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Familiar with the properties and hazards of the material being loaded.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="prop_hazard_load" value="yes" checked>
                                                          <input label="No" type="radio" name="prop_hazard_load" value="no">
                                                          <input label="N/A" type="radio" name="prop_hazard_load" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Equipped with hearing protection, respiratory protection, chemical protective suit, face shield, goggles, and chemical-resistant gloves and boots.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="equipped_other_ins" value="yes" checked>
                                                          <input label="No" type="radio" name="equipped_other_ins" value="no">
                                                          <input label="N/A" type="radio" name="equipped_other_ins" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Wearing proper hearing protection during vacuum operations.  Wearing respiratory protection when necessary/required. </label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="wearing_protection" value="yes" checked>
                                                          <input label="No" type="radio" name="wearing_protection" value="no">
                                                          <input label="N/A" type="radio" name="wearing_protection" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Carrying certification for proof of training that includes HAZWOPER and, when required, HAZMAT and Hazard Communication.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="carrying_certification" value="yes" checked>
                                                          <input label="No" type="radio" name="carrying_certification" value="no">
                                                          <input label="N/A" type="radio" name="carrying_certification" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Has a current vacuum truck log.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="vacuum_truck_log" value="yes" checked>
                                                          <input label="No" type="radio" name="vacuum_truck_log" value="no">
                                                          <input label="N/A" type="radio" name="vacuum_truck_log" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row note-row">
                                                      <label>
                                                        <span>NOTE</span> If the answer to any of the statement above is “No” or “N/A”, a comment must be entered to explain.</label>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="tabVehOpeCheck">
                                            <div class="card">
                                              <div class="card-body">
                                                <h4 class="vehicle-checklist-title">Section 3 <span>Truck Operation</span></h4>
                                                <div class="row vehicle-check-row">
                                                  <div class="col-md-12">
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">The truck is located at least 35 ft. (10 meters) upwind/crosswind of the material to be loaded.  A wind sock or other indicator is monitoring wind direction.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="truck_locate_35" value="yes" checked>
                                                          <input label="No" type="radio" name="truck_locate_35" value="no">
                                                          <input label="N/A" type="radio" name="truck_locate_35" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">The vacuum pump discharge is vented to a safe location at least 50 ft. (15 meters) downwind from the truck when loading volatile materials.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="vacuum_pump_discharge" value="yes" checked>
                                                          <input label="No" type="radio" name="vacuum_pump_discharge" value="no">
                                                          <input label="N/A" type="radio" name="vacuum_pump_discharge" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">The temperature of materials being loaded does not exceed 75°C (167°F).</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="temp_mat_not_75" value="yes" checked>
                                                          <input label="No" type="radio" name="temp_mat_not_75" value="no">
                                                          <input label="N/A" type="radio" name="temp_mat_not_75" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">Operator will remain between the truck and source or receiving container and within 25 ft. of the truck to monitor the operation. <span class="label_with_note"><b>NOTE:</b> The truck cab is not to be occupied while vacuum operation is occurring.</span></label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="ope_truck_source" value="yes" checked>
                                                          <input label="No" type="radio" name="ope_truck_source" value="no">
                                                          <input label="N/A" type="radio" name="ope_truck_source" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">The truck engine is shut down when the vacuum pump is powered with an auxiliary engine.  </label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="truck_engine_shut_down" value="yes" checked>
                                                          <input label="No" type="radio" name="truck_engine_shut_down" value="no">
                                                          <input label="N/A" type="radio" name="truck_engine_shut_down" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">The truck is parked on level ground, wheels are chocked for the loading or unloading operation, and the parking brake is set.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="truck_park_ground" value="yes" checked>
                                                          <input label="No" type="radio" name="truck_park_ground" value="no">
                                                          <input label="N/A" type="radio" name="truck_park_ground" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">A continuous conductive path between the truck and source or receiving container has been established before transfer of flammable liquids and verified visually or with an ohmmeter.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="cond_path_truck_source" value="yes" checked>
                                                          <input label="No" type="radio" name="cond_path_truck_source" value="no">
                                                          <input label="N/A" type="radio" name="cond_path_truck_source" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <label class="col-sm-12 col-form-label">The truck is bonded to a grounded metallic structure (e.g., storage tank, underground piping, or a ground rod) before the liquids transfer operation.</label>
                                                      <div class="col-md-5 custom-radio-main-section">
                                                        <div class="radio-custom-btn-section">
                                                          <input label="Yes" type="radio" name="grounded_metallic_structure" value="yes" checked>
                                                          <input label="No" type="radio" name="grounded_metallic_structure" value="no">
                                                          <input label="N/A" type="radio" name="grounded_metallic_structure" value="n/a">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-7 vacume_truck_comments_sec">
                                                        <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false" disabled="true">
                                                      </div>
                                                    </div>
                                                    <div class="row note-row">
                                                      <label><span>NOTE</span> If the answer to any of the statement above is “No” or “N/A”, a comment must be entered to explain.</label>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="tabVehOpeCheck">
                                            <div class="card">
                                              <div class="card-body">

                                                <div class="row input_row mt-2">
                                                  <div class="col-md-12 draw_signature_col text-center">
                                                      <label class="" for="">Signature (Drag Your cursor to sign below)</label>
                                                      <label class="sign_error_owner_msg" style="display: none;" for="">Please Put Your Signature</label>
                                                      <br/>
                                                      <div id="sig" ></div>
                                                          <input type="hidden" class="sign_input_validation_owner" value="0"> 
                                                      <br/>
                                                      <button id="clear" class="btn btn-sm btn-round btn-danger my-3">Clear Signature</button>
                                                      <textarea id="signature64" name="signed" style="display: none"></textarea>
                                                  </div>
                                                  <!-- <canvas id='blank' style='display:none'></canvas> -->
                                                </div>
                                                <div class="row">
                                                  <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="vehicle_check_date" class="bmd-label-floating input_label">Date</label>
                                                      <input type="text" class="form-control form_input_field form_fields datepicker" id="vehicle_check_date" name="vehicle_check_date" required="">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="printed_name" class="bmd-label-floating input_label">Printed Name</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="printed_name" name="printed_name" required="">
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                          <div style="overflow:auto;" class="row msf_btn_holder">
                                            <div class="col-md-6 save_btn_wrapper text-left" style="float:right;">
                                              <a class="btn btn-round btn-warning nextPrevBtnOne prev" href="#top_vacuum_card" type="button" id="prevBtnVehOpeCheck" onclick="nextPrevVehOpeCheck(-1)">Previous step</a>
                                            </div>
                                            <div class="col-md-6 save_btn_wrapper text-right">
                                              <a class="btn custom-btn-one btn-round btn-success nextPrevBtnOne next nextBtnAgreement" href="#top_vacuum_card" type="button" id="nextBtnVehOpeCheck" onclick="nextPrevVehOpeCheck(1)">Next step</a>
                                              <button class="btn custom-btn-one btn-round btn-success nextPrevBtnOne submitBtnVehOpeCheck nextBtnAgreement" style="display: none" type="submit" onclick="">Submit</button>
                                            </div>
                                          </div>

                                        </div>

                                      </div>
                                    </div>
                                  </div>
                                </form>
                              </div>
                              <div class="tabChecklist">
                                <form action="" class="bbs_form">
                                  <div class="row">
                                    <div class="col-md-12 mx-auto">
                                    
                                      <div class="card">
                                        <div class="card-body">
                                          <div class="row four_form_header_row">
                                            <div class="col-md-12">
                                              <div class="bbs_form_logo">
                                                <a href="{{route('home')}}" class="simple-text logo-normal">
                                                  <img src="{{ asset('assets/img/logo.png') }}"/>
                                                </a>
                                              </div>
                                            </div>
                                            <div class="col-md-12 text-center four_form_title_wrapper">
                                              <h2 class="main-title">BEHAVIOR BASED SAFETY OBSERVATION FORM</h2>
                                            </div>
                                            <div class="col-md-12">
                                              <p>Your concerns for safety and suggestions on how to improve our safety program are important. Use this form to submit either safety improvement input and/or a BBS Safety Observation. Your name is optional, and the name of the person being observed is not to be used. This information will be used to continually improve our safety system and conditions.</p>
                                            </div>
                                          </div>
                                          <div class="card">
                                            <div class="card-header card-header-rose card-header-text sub_card">
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">Improvement Input</h4>
                                              </div>
                                            </div>
                                            <div class="card-body">
                                              <div class="row bbs_checkbox_row">
                                                <div class="col-md-4 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="">BBS Observation
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-4 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="">Unsafe Act
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>

                                                <div class="col-md-4 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="">Unsafe Condition
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="row input_row bbs_checkbox_row">

                                                <div class="col-md-4 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="">Recognition
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>

                                                <div class="col-md-4 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="">Environment
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="row input_row">
                                                <div class="col-md-12 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="employe_obs_input" class="bmd-label-floating input_label">Employee / Observer Input</label>
                                                    <textarea class="form-control form_input_field form_fields" rows="5" name="employe_obs_input" id="employe_obs_input"></textarea>
                                                  </div>
                                                </div> 
                                              </div>

                                              <div class="row input_row">
                                                <div class="col-md-12 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="employe_action" class="bmd-label-floating input_label">Employee's Action Taken / Recommendation </label>
                                                    <textarea class="form-control form_input_field form_fields" rows="4" name="employe_action" id="employe_action"></textarea>
                                                  </div>
                                                </div> 
                                              </div>

                                              <div class="row input_row">
                                                <div class="col-md-12 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="sup_management_action" class="bmd-label-floating input_label">Supervisor / Management Action Taken</label>
                                                    <textarea class="form-control form_input_field form_fields" rows="4" name="sup_management_action" id="sup_management_action"></textarea>
                                                  </div>
                                                </div> 
                                              </div>

                                            </div>
                                          </div>

                                          <div class="card">
                                            <div class="card-header card-header-rose card-header-text sub_card">
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">Safety Observation Critical Factors ( S = Safe, C = Concern )</h4>
                                              </div>
                                            </div>
                                            <div class="card-body">
                                              <div class="row">
                                                <div class="col-md-3 input_wrapper bbs_single_tbl">
                                                  <div class="table-responsive bbs_form_table text-center">
                                                    <h4 class="card-title text-primary">PPE / Procedures / Methods </h4>
                                                    <table class="table">
                                                      <tbody>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="eye_head" value="s" checked="">
                                                              <input label="C" type="radio" name="eye_head" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Eye And Head</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="hand_body" value="s" checked="">
                                                              <input label="C" type="radio" name="hand_body" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Hand And Body</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="footwear" value="s" checked="">
                                                              <input label="C" type="radio" name="footwear" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Footwear</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio"name="trained_task" value="s" checked="">
                                                              <input label="C" type="radio" name="trained_task" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Trained On Task</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="work_permit_jsa" value="s" checked="">
                                                              <input label="C" type="radio" name="work_permit_jsa" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Work Permit / JSA </td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio"  name="all_train_bbs" value="s" checked="">
                                                              <input label="C" type="radio"name="all_train_bbs" value="c">
                                                            </div>
                                                          </td>
                                                          <td>All Trained In BBS</td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                                <div class="col-md-3 input_wrapper bbs_single_tbl">
                                                  <div class="table-responsive bbs_form_table text-center">
                                                    <h4 class="card-title text-primary">Body Position / Mechanics </h4>
                                                    <table class="table">
                                                      <tbody>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="proper_poss" value="s" checked="">
                                                              <input label="C" type="radio"name="proper_poss" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Proper Position</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="ask_for_help" value="s" checked="">
                                                              <input label="C" type="radio" name="ask_for_help" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Ask For Help</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="use_dolly" value="s" checked="">
                                                              <input label="C" type="radio" name="use_dolly" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Use Dolly</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="smaller_load" value="s" checked="">
                                                              <input label="C" type="radio" name="smaller_load" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Smaller Loads</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="dont_twist_body" value="s" checked="">
                                                              <input label="C" type="radio" name="dont_twist_body" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Dont Twist Body</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="get_closed_item" value="s" checked="">
                                                              <input label="C" type="radio" name="get_closed_item" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Get Closed To Item</td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                                <div class="col-md-3 input_wrapper bbs_single_tbl">
                                                  <div class="table-responsive bbs_form_table text-center">
                                                    <h4 class="card-title text-primary">Slips / Trips </h4>
                                                    <table class="table">
                                                      <tbody>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="proper_footwear" value="s" checked="">
                                                              <input label="C" type="radio" name="proper_footwear" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Proper Footwear</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="aware_hazard" value="s" checked="">
                                                              <input label="C" type="radio" name="aware_hazard" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Aware Of Hazards</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="prompt_clean_up" value="s" checked="">
                                                              <input label="C" type="radio" name="prompt_clean_up" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Prompt Clean Up</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="tripping_hazard" value="s" checked="">
                                                              <input label="C" type="radio" name="tripping_hazard" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Tripping Hazards</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="not_russing" value="s" checked="">
                                                              <input label="C" type="radio" name="not_russing" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Not Rushing</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="step_condition" value="s" checked="">
                                                              <input label="C" type="radio" name="step_condition" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Step Conditions</td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                                <div class="col-md-3 input_wrapper bbs_single_tbl">
                                                  <div class="table-responsive bbs_form_table text-center">
                                                    <h4 class="card-title text-primary">Eqipment / Work Environment </h4>
                                                    <table class="table">
                                                      <tbody>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="MSDS_need" value="s" checked="">
                                                              <input label="C" type="radio" name="MSDS_need" value="c">
                                                            </div>
                                                          </td>
                                                          <td>MSDS, if needed</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="lockout" value="s" checked="">
                                                              <input label="C" type="radio" name="lockout" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Lockout</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="tools_are_safe" value="s" checked="">
                                                              <input label="C" type="radio" name="tools_are_safe" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Tools Are Safe</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="adjacent_work" value="s" checked="">
                                                              <input label="C" type="radio" name="adjacent_work" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Adjacent Work</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="signage_need" value="s" checked="">
                                                              <input label="C" type="radio" name="signage_need" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Signage, if needed</td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="radio-custom-btn-section bbs_radio">
                                                              <input label="S" type="radio" name="spill_control" value="s" checked="">
                                                              <input label="C" type="radio" name="spill_control" value="c">
                                                            </div>
                                                          </td>
                                                          <td>Spill Control</td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                              </div>

                                              <div class="row input_row">
                                                <div class="col-md-12 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="sup_management_action" class="bmd-label-floating input_label">Observer’s Feedback Given to Other Employee </label>
                                                    <textarea class="form-control form_input_field form_fields" rows="5" name="sup_management_action" id="sup_management_action"></textarea>
                                                  </div>
                                                </div> 
                                              </div>

                                              <div class="row">
                                                <div class="col-md-6">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="make_equip" class="bmd-label-floating input_label">Location</label>
                                                    <input type="text" class="form-control form_input_field form_fields" id="make_equip" name="make" required="">
                                                  </div>
                                                </div>

                                                <div class="col-md-6">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="make_equip" class="bmd-label-floating input_label">Observer's Name</label>
                                                    <input type="text" class="form-control form_input_field form_fields" id="make_equip" name="make" required="">
                                                  </div>
                                                </div>

                                                <div class="col-md-6">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="make_equip" class="bmd-label-floating input_label">Dates</label>
                                                    <input type="text" class="datepicker form-control form_input_field form_fields" id="make_equip" name="make" required="">
                                                  </div>
                                                </div>

                                              </div>

                                            </div>
                                          </div>

                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </form>
                              </div>
                              <div class="tabChecklist">
                                <form id="jobs_site_analysis_form">
                                  <div class="card pre_trip_report_card" id="top_pre_trip_report_card">
                                    <div class="card-header card-header-rose card-header-text lead_card_header user_sub_card">
                                      <div class="card-icon">
                                          <i class="material-icons">supervised_user_circle</i>
                                      </div>
                                        <h4 class="card-title">Jobs Site Hazard analysis</h4>
                                    </div>
                                    <div class="card-body">
                                      <div class="row">
                                        <div class="col-md-1 progress_container jobs_site_analysis_arrow" style="text-align:center;margin-bottom:20px;">
                                          <span class="stepJobSiteAnalysis step-first active">
                                            <i class="material-icons">done</i>
                                          </span>
                                          <span class="stepJobSiteAnalysis step-inner">
                                            <i class="material-icons">done</i>
                                          </span>
                                          <span class="stepJobSiteAnalysis step-inner">
                                            <i class="material-icons">done</i>
                                          </span>
                                          <span class="stepJobSiteAnalysis step-inner">
                                            <i class="material-icons">done</i>
                                          </span>
                                        </div>

                                        <div class="col-md-11 jobs_site_analysis_content">
                                          <div class="tabJobSiteAnalysis">
                                            <div class="card">
                                              <div class="card-body">
                                                <div class="row input_row">

                                                  <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="prepared_by" class="bmd-label-floating input_label">Prepared By</label>
                                                      <input type="text" class="form-control form_input_field" id="prepared_by" name="prepared_by">
                                                    </div>
                                                  </div>

                                                  <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="crew" class="bmd-label-floating input_label">Crew</label>
                                                      <input type="text" class="form-control form_input_field" id="crew" name="crew">
                                                    </div>
                                                  </div>

                                                  <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="customer_well" class="bmd-label-floating input_label">Customer / Well</label>
                                                      <input type="text" class="form-control form_input_field" id="customer_well" name="customer_well">
                                                    </div>
                                                  </div>

                                                  <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="jobs_site_analysis_time" class="bmd-label-floating input_label">Time</label>
                                                      <input type="text" class="form-control form_input_field timepicker" id="jobs_site_analysis_time" name="jobs_site_analysis_time">
                                                    </div>
                                                  </div>

                                                  <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="jobs_site_analysis_date" class="bmd-label-floating input_label">Date</label>
                                                      <input type="text" class="form-control form_input_field datepicker" id="jobs_site_analysis_date" name="jobs_site_analysis_date">
                                                    </div>
                                                  </div>
                                                </div>

                                                <div class="row input_row check_job_site_row">
                                                  <div class="col-md-12">
                                                    <div class="form-check jobs_site_analysis_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value=""> Check Job Site Surface Equipment
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check jobs_site_analysis_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value=""> Check Job Site Pressure Ratings
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check jobs_site_analysis_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value=""> Hazardous Gasses or Fluids (H2S, Acid, etc.)
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>
                                                </div>

                                                <div class="row jobs_site_analysis_row">
                                                  <div class="col-md-12">
                                                   <div class="service_represent">
                                                    <span class="inline-block-span">E&V Oilfield Services Representative </span>
                                                    <div class="form-check jobs_site_analysis_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value=""> Verify all safety policies will be followed and adhered to throughout the job. (Stop Work Authority)
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                   </div>

                                                   <div class="service_represent">
                                                    <span class="inline-block-span">E&V Oilfield Services Representative </span> 
                                                    <div class="form-check jobs_site_analysis_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Verified that valves and wellhead are designated to be used for this job site
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                   </div>

                                                   <div class="service_represent">
                                                    <span class="inline-block-span">E&V Oilfield Services Representative </span>
                                                    <div class="form-check jobs_site_analysis_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">has reviewd customer and 3rd party supplied equipment safe to use: i.e. rig stairs, ladders, light towers. 
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                   </div>

                                                   <div class="service_represent">
                                                    <span>E&V Oilfield Services Representative </span>
                                                    <div class="form-check jobs_site_analysis_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">has communicated the requirement for 100% tie off over 4 foot heights. 
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                   </div>
                                                  </div>
                                                </div>

                                                <div class="row physical_hazard_discus_row">
                                                  <div class="headeing_with_chekbox">
                                                    <h3>Physical Hazards Discussed</h3>
                                                    <div class="form-check jobs_site_analysis_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value=""> Job Assignment &#8226; Equipment Operations &#8226; HouseKeeping
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>
                                                </div>

                                                <div class="row physical_hazard_discus_content">
                                                  <div class="col-md-4">
                                                    <ul class="physical_hazard_discus_list">
                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Cranes
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>

                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Extreme Temperatures
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>

                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Elevated Work Areas
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>

                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Fails
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>
                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Lightning
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>
                                                    </ul>
                                                  </div>
                                                  <div class="col-md-4">
                                                    <ul class="physical_hazard_discus_list">
                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Overhead Loads
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>
                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Overhead Utilities
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>
                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Pinch Points
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>
                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Rat Holes, Slick Spots, Cellars
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>
                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Spiders, Snakes, Wasps
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>
                                                    </ul>
                                                  </div>
                                                  <div class="col-md-4">
                                                    <ul class="physical_hazard_discus_list">
                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Slips, Trips, Fails
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>
                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Trapped Pressure
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>
                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Wind speed
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>
                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Wind Direction
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>
                                                      <li>
                                                        <div class="form-check physical_hazard_discus_check form-check-inline">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">WireLine / Lubricator
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </li>
                                                    </ul>
                                                  </div>
                                                  
                                                </div>

                                              </div>
                                            </div>
                                          </div>
                                          <div class="tabJobSiteAnalysis">
                                            <div class="row emergency_procedure_dis_row">
                                              <h4 class="sub-title">Emergency Procedures Discussed</h4>
                                              <div class="col-md-12">
                                                <div class="form-check emergency_procedure_check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" value="">Emergency Numbers
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>

                                                <div class="form-check emergency_procedure_check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" value="">Identified Designated Safe Areas
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>

                                                <div class="form-check emergency_procedure_check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" value="">Location of Cell Phone, First Aid Kit, Eye Wash, Monitors
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>

                                                


                                              </div>
                                            </div>

                                            <div class="row codition_hazard_policy_row">
                                              <h4 class="sub-title">
                                                Condition, Hazards, Policy
                                              </h4>
                                            </div>

                                            <div class="row codition_hazard_policy_content input_row">
                                              <div class="col-md-4 input_wrapper">
                                                <ul class="physical_hazard_discus_list">
                                                  
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Energized Fluids
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Equipment Grounding
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>

                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Heater Fires
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  
                                                </ul>
                                              </div>
                                              <div class="col-md-4 input_wrapper">
                                                <ul class="physical_hazard_discus_list">
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Hot Work; Welding/ Fueling
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Locked Gates
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>

                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Lock-out / Tag-out
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  
                                                </ul>
                                              </div>
                                              <div class="col-md-4 input_wrapper">
                                                <ul class="physical_hazard_discus_list">
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Primary Meeting Area
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Secondary Meeting Area
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  
                                                </ul>
                                              </div>
                                             
                                            </div>

                                            <div class="row job-hazard-analysis-row">
                                              <h4 class="sub-title">*** Communication: Be sure that individual activities are clean to each person on location ***</h4>
                                              <h5 class="job_hazard_sub_title">Job Hazard analysis: PPE Required</h5>
                                            </div>
                                            <div class="row job_hazard_analysis_content input_row">
                                              <div class="col-md-4 input_wrapper">
                                                <ul class="physical_hazard_discus_list">
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Chemical Suit
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Chemical Aprons
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Face Shield
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Fail Protection
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Fire Retardant Clothing
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Goggles
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                </ul>
                                              </div>
                                              <div class="col-md-4 input_wrapper">
                                                <ul class="physical_hazard_discus_list">
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Hand Protection
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Hard Hat
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>

                                                   <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Hering Protection
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Impact Gloves
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Personal H2S Monitor
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                </ul>
                                              </div>
                                              <div class="col-md-4 input_wrapper">
                                                <ul class="physical_hazard_discus_list">
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Respirtor
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Rubber Boots
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Rubber Gloves
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Safety Harness
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <li>
                                                    <div class="form-check physical_hazard_discus_check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="">Steel Toe Boots
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </li>
                                                </ul>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="tabJobSiteAnalysis">
                                            
                                            <div class="row jsa-step-table-row">
                                              <div class="col-md-12 ">
                                                <table class="table jsa-step-table">
                                                  <thead>
                                                    <tr>
                                                      <th>JSA Steps</th>
                                                      <th>Potential Accidents or Hazards</th>
                                                      <th>Recommended Safe Actions to Mitigate Hazards</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                    <tr>
                                                      <td>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Additonal Step
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Check PPE for all the crew
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Mobilization from location
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Mobilization from yard
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Spotting
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Transport/ Load/ Unload
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </td>
                                                      <td>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Blind Spots, Road Conditions
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Check Location Equipment
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Highway, Country / lease Roads
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Third-Party Equipment
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Use Spotters
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Vehicle Accidents, Directions
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        
                                                      </td>
                                                      <td>
                                                        <p>
                                                          Be careful at yard. Proper PPE, 3 Point contact, pretrip vehicle. Check trucks, chemicals, supplies, perform tailgate meeting. Discuss Convoy, route, seatbelts, following distance, road hazards. No Cell phones or tect while driving, follow speed limits. Lease Road max. 25 mph, beware of guards and livestock. Report all vehicle Accidents, spills, incidents, Near Miss, Injuries. Blind Spots, Road Conditions, Dust, other hazards. Clean Vehicle windows, lights, loose trash or unsecared items. 
                                                        </p>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <td>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Crew JSA Meeting
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Rig Up
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Rig Down
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Site Check
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </td>

                                                      <td>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Caught In-Between
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Elevated Work Area
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Overhead Loads
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Poor Walking / Working Surface
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Pinch Points, Trips and Falls
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Rat holes, Slick spots, cellars
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Standard Hazards
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </td>

                                                      <td>
                                                        <p>
                                                          Inspect work areas, check for hazards, work in teams, proper tools. Assign jobs, number of lines for iron and hoses, valves, pop-offs. Good Hand / Foot placements, watch slips / trips / falls, crane signals, carefull lifting iron / hoses, wire placement, proper tools, tag lines. Communicate tasks, tie off 4', H2S Monitors, Clear of hammering. Team work/ lift, proper PPE, oil-threads, equipment placement. Fire extingushers out, caution tape around red zone, PPSI sign out. If no railing -4" tie off required, harness, certified machine operators. 
                                                        </p>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <td>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Crew JSA Meeting
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Roll / Acid Check Chemicals
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Test Lines / Pressure / and Kills
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        
                                                        
                                                      </td>
                                                      <td>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Distraction, Chemicals, Valves
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Secondary Personnel
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Stay in Designated Areas
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Trapped Pressure
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </td>
                                                      <td>
                                                        <p>
                                                          Check rigging of lines, kicks, pick up unused iron/hoses/tools. Pressure up, stay clear, check for leaks on lines, valves, well-head and equipment, communicate to supervisor, stay clear of lines. Check valves, tanks, contain spills, don't overfill tank/tub.
                                                        </p>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <td>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Crew JSA Meeting
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Perform Job
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </td>
                                                      <td>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Distraction, Chemicals, Valves
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Stay in Designated Areas
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Wireline Operations
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Wireline - Lubricator Down
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </td>
                                                      <td>
                                                        <p>
                                                          Wear radio/ headphones, stay clear wireline and 3rd party operations. Release pressure before opening valve, contain any spills. Stay in designated areas, perform jobs safely, communicate hazards, pressure, rate, density, entering/ exiting red zones. 
                                                        </p>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <td>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Identify Hazards
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Site Check
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        
                                                      </td>
                                                      <td>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Check Location Equipment
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Emergency Metting Place
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Red Zone Area - caution Tape
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </td>
                                                      <td>
                                                        <p>
                                                          Housekeeping, fire extinguishers, chemicals, lights, wash trailer. Plan route directions out, meetings areas, designated drivers. Check location: holes - cellar - slick areas, equipment, use spotters. 
                                                        </p>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <td>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Crew JSA Meeting
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Sign JSA Sheet
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </td>
                                                      <td>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Distractions
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Heat - cold; Hydration
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                        
                                                        <div class="form-check jsa-step-table-check">
                                                          <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" value="">Standard Hazards
                                                            <span class="form-check-sign">
                                                              <span class="check"></span>
                                                            </span>
                                                          </label>
                                                        </div>
                                                      </td>
                                                      <td>
                                                        <p>
                                                          Communicate all hazards with crew and third parties. Identify those authorized to enter and work in red zone areas.
                                                        </p>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </div>
                                            </div>

                                          </div>

                                          <div class="tabJobSiteAnalysis">
                                            <div class="row">
                                              <div class="col-md-12 tailgate_safety_meet_tbl_res">
                                                <table class="table tailgate_safety_meet_table">
                                                  <thead>
                                                    <tr>
                                                      <th colspan="3">E&V Oilfield Services</th>
                                                      <th colspan="4">
                                                        <h4>TailGate Safety Meeting</h4>
                                                        <p>Invite All Personnel on location and discuss job Procedure</p>
                                                      </th>
                                                    </tr>
                                                  </thead>
                                                  <tr>
                                                    <td colspan="3">Customer:</td>
                                                    <td colspan="2">Supervisor:</td>
                                                    <td colspan="2">Date:</td>
                                                  </tr>
                                                  <tr>
                                                    <td colspan="3">Well Name & Number:</td>
                                                    <td colspan="2">Crew:</td>
                                                    <td colspan="2">Data Van #:</td>
                                                  </tr>
                                                  <tr>
                                                    <td colspan="3">Customer Representative:</td>
                                                    <td colspan="2">Max Pressure:</td>
                                                    <td colspan="2">Max Rate:</td>
                                                  </tr>
                                                  <tr>
                                                    <td colspan="3">Nearest Hospital:</td>
                                                    <td colspan="2">Recorded By:</td>
                                                    <td colspan="2">Number of Stages:</td>
                                                  </tr>
                                                  <tr>
                                                    <th></th>
                                                    <th colspan="2">Employee Print Name</th>
                                                    <th>Employee Signature</th>
                                                    <th>Hard Hat Color</th>
                                                    <th class="mentor_green_hard_hat">Mentor for Green Hard Hat</th>
                                                    <th>Company Name</th>
                                                  </tr>
                                                  <tr>
                                                    <td>1</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>2</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>3</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>4</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>5</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>6</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>7</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>8</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>9</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>10</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>

                                                  <tr>
                                                    <td>11</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>12</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>13</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>14</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>15</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>16</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>17</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>18</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>19</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>20</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>

                                                  <tr>
                                                    <td>21</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>22</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>23</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>24</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>25</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>26</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>27</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>28</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>29</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>30</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>

                                                  <tr>
                                                    <td>31</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>32</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>33</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>34</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>35</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>36</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>37</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>38</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>39</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>
                                                  <tr>
                                                    <td>40</td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>E&V Oilfield Service</td>
                                                  </tr>

                                                </table>

                                              </div>
                                              <div class="col-md-12 text-center">
                                                <p class="tailgate-jst-form-footer">HSE Form 136 JSA - TailGate Meeting Sign in Sheet</p>
                                              </div>
                                            </div>
                                          </div>
                                         

                                          <div style="overflow:auto;" class="row msf_btn_holder">
                                            <div class="col-md-6 save_btn_wrapper text-left" style="float:right;">
                                              <a class="btn btn-round btn-warning prevJobanlysisbtn prev" href="#top_pre_trip_report_card" type="button" id="prevBtnJobSiteAnalysis" onclick="nextPrevJobSiteAnalysis(-1)">Previous step</a>
                                            </div>
                                            <div class="col-md-6 save_btn_wrapper text-right">
                                              <a class="btn custom-btn-one btn-round btn-success nextPrevBtnOne next nextJobanlysisbtn" href="#top_pre_trip_report_card" type="button" id="nextBtnJobSiteAnalysis" onclick="nextPrevJobSiteAnalysis(1)">Next step</a>
                                              <button class="btn custom-btn-one btn-round btn-success nextPrevBtnOne submitBtnJobSiteAnalysis nextJobanlysisbtn" style="display: none" type="submit" onclick="">Submit</button>
                                            </div>
                                          </div>

                                        </div>


                                      </div>
                                    </div>
                                  </div>
                                </form>
                              </div>
                              <div class="tabChecklist">
                                <form class="nli_form" id="multiForm2">
                                  <div class="card" id="top_nli_card">
                                    <div class="card-body">
                                      <div class="row four_form_header_row">
                                        <div class="col-md-12 text-center four_form_title_wrapper">
                                          <h2 class="main-title">MIDSTREAM LOSS / NEAR LOSS REPORT FORM (LI/NLI)</h2>
                                        </div>
                                      </div>
                                      <div class="row nli_top_body">
                                        <label class="col-sm-5 col-form-label">Impact Incident ID</label>
                                        <div class="col-sm-3">
                                          <div class="form-group bmd-form-group">
                                            <input class="form-control" type="text" name="incident_id" email="true">
                                          </div>
                                        </div>
                                        <div class="col-md-12 text-center">
                                          <p class="include_top_para">( include after ID number assigned in IMPACT )</p>
                                        </div>
                                        <div class="col-md-12 text-center">
                                          <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value=""> Alert
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value=""> Bulletin
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-1 progress_container" style="text-align:center;margin-bottom:40px;">
                                          <span class="stepRen step-first">
                                            <i class="material-icons">done</i>
                                          </span>
                                          <span class="stepRen step-inner">
                                            <i class="material-icons">done</i>
                                          </span>
                                          <span class="stepRen step-inner">
                                            <i class="material-icons">done</i>
                                          </span>
                                          <span class="stepRen step-inner">
                                            <i class="material-icons">done</i>
                                          </span> 
                                          <span class="stepRen step-inner">
                                            <i class="material-icons">done</i>
                                          </span>
                                          <span class="stepRen step-inner">
                                            <i class="material-icons">done</i>
                                          </span>                                  
                                        </div>
                                        <div class="col-md-11 content_container rental_ag_content_container">
                                          <div class="tabRen">
                                            <div class="card nli_tab_main_card">
                                              <div class="card-header card-header-rose card-header-text sub_card">
                                                <div class="card-text card_text_title">
                                                  <h4 class="card-title">General</h4>
                                                </div>
                                              </div>
                                              <div class="card-body sub_row">
                                                <div class="row bbs_checkbox_row">
                                                  <div class="col-md-7 input_wrapper">
                                                    <div class="form-group bmd-form-group is-filled">
                                                      <label for="resource_code" class="bmd-label-floating input_label">Incident Title</label>
                                                      <input type="text" class="form-control form_input_field" id="incident_title" name="incident_title">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-2 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input loss_measurement" type="checkbox" value="" name="inc_loss">Loss
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-3">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input loss_measurement" type="checkbox" value="" name="inc_loss">Near Loss
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                    <div class="form-group bmd-form-group is-filled">
                                                      <label for="resource_code" class="bmd-label-floating input_label">Responsible Department</label>
                                                      <input type="text" class="form-control form_input_field" id="responsible_dept" name="responsible_dept">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                    <div class="form-group bmd-form-group is-filled">
                                                      <label for="resource_code" class="bmd-label-floating input_label">Responsible Superviser</label>
                                                      <input type="text" class="form-control form_input_field" id="responsible_supv" name="responsible_supv">
                                                    </div>
                                                  </div>
                                                </div>
                                                <h4 class="sub-title">Location (Process Unit): (Check the one that applies) </h4>
                                                <div class="row input_row nli_checkbox_row">
                                                
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input location_process_unit" type="checkbox" value="" name="process_unit">Loading Rack
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input location_process_unit" type="checkbox" value="" name="process_unit">Shop / Storage
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input location_process_unit" type="checkbox" value="" name="process_unit">Marine Doc/ Pier
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>

                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input location_process_unit" type="checkbox" value="" name="process_unit">Operation Control Center
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input location_process_unit" type="checkbox" value="" name="process_unit">Office
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>

                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input location_process_unit" type="checkbox" value="" name="process_unit">Pipeline Facility
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input location_process_unit" type="checkbox" value="" name="process_unit">Pipeline Row
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input location_process_unit" type="checkbox" value="" name="process_unit">Roadway
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>

                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input location_process_unit" type="checkbox" value="" name="process_unit">Tank Farm
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input location_process_unit" type="checkbox" value="" name="process_unit">Terminal
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                    <div class="form-group bmd-form-group is-filled">
                                                      <label for="resource_code" class="bmd-label-floating input_label">Date Occured</label>
                                                      <input type="text" class="form-control form_input_field datepicker" id="date_occured" name="date_occured">
                                                    </div>                                            
                                                  </div>
                                                  <div class="col-md-6">
                                                    <div class="form-group bmd-form-group is-filled">
                                                      <label for="resource_code" class="bmd-label-floating input_label">Date Reported</label>
                                                      <input type="text" class="form-control form_input_field datepicker" id="date_reported" name="date_reported">
                                                    </div>                                            
                                                  </div>
                                                  <div class="col-md-12">
                                                    <div class="form-group bmd-form-group is-filled">
                                                      <label for="resource_code" class="bmd-label-floating input_label">Time Reported</label>
                                                      <input type="text" class="timepicker form-control form_input_field" id="time_reported" name="time_reported">
                                                    </div>                                            
                                                  </div>
                                                  <div class="col-md-6">
                                                    <div class="form-group bmd-form-group is-filled">
                                                      <label for="resource_code" class="bmd-label-floating input_label">Reported By</label>
                                                      <input type="text" class="form-control form_input_field" id="reported_by" name="reported_by">
                                                    </div>                                            
                                                  </div>
                                                  <div class="col-md-3 custom_select2_col">
                                                    <div class="form-group custom-form-select">
                                                      <select class="custom-select type_of_ownership select2 form_input_field field_to_fill" id="shift" name="shift">
                                                        <option class="form-select-placeholder"></option>
                                                        <option value="day">Day</option>
                                                        <option value="night">NIght</option>
                                                      </select>
                                                      <div class="form-element-bar">
                                                      </div>
                                                      <label class="form-element-label" for="shift input_label">Shift</label>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-3 shift_check_wrapper">
                                                    <div class="form-check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="shift_check"> 1
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="shift_check"> 2
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="shift_check"> 3
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>
                                                </div>

                                              </div>
                                            </div>
                                          </div>

                                          <div class="tabRen">
                                            <div class="card nli_tab_main_card">
                                              <div class="card-header card-header-rose card-header-text sub_card"> 
                                                <div class="card-text card_text_title">
                                                  <h4 class="card-title">General</h4>
                                                </div>
                                              </div>
                                              <div class="card-body sub_row">
                                                <h4 class="sub-title">Incident Description </h4>
                                                <div class="row input_row">
                                                  <div class="col-md-12">
                                                    <h4 class="sub_child_title">Executive Summary (1-2 sentence summary Of incident) : </h4>
                                                    <hr class="sub_child_title_hr">
                                                    <p>While loading containments onto a trailer Marcus noticed an unsafe entailing the operations of a forklift while a worker was in the path of the truck.</p>
                                                  </div>
                                                  <div class="col-md-12">
                                                    <h4 class="sub_child_title">Incident Description (Max 1000 Characters): </h4>
                                                    <hr class="sub_child_title_hr">
                                                    <span class="sub_child_title_span">(Describe in detail WHAT happened, HOW it happened, and WHEN and WHERE in the process it happened): </span>
                                                    <p>Francisco Rodriguez was guiding the containment while Guillermo Sanchez was operating the forklift. Both employees failed to recognize the hazard of working in the path of a truck while it is not safely parked.</p>
                                                  </div>
                                                  <div class="col-md-12">
                                                    <h4 class="sub_child_title">Identify “Equivalent of Questionable Item(s)” For Near Loss/Loss.</h4>
                                                    <hr class="sub_child_title_hr">
                                                    <ul class="subcontractor_agre_list bold">
                                                      <li>
                                                        Safety while working around a forklift not in park.
                                                      </li>
                                                      <li>
                                                        Failure to identify at risk work behaviors
                                                      </li>
                                                      <li>
                                                        Failure to develop a language barrier plan prior to commencing work.
                                                      </li>
                                                    </ul>
                                                  </div>
                                                </div>
                                                <h4 class="sub_child_title">Activity: (Check The One That Applies):</h4>
                                                <hr class="sub_child_title_hr">
                                                <div class="row nli_checkbox_row">
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Ascending / Descending
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Bicycle Use
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Classroom / Meeting
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Communicating
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Computer/ Work Station
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Driving/Piloting/Riding
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Entering/Exiting/Equip.
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Handling Docs/Paperwork
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Handling Material/Chems
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Lifting/Handling manual
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Lifting/Handling Mech. Assist
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Operating Equip. Controls
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Reading/Interpret. Info
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Supervising/Directing Others
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Swimming/Diving
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Walking At Grade
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Working w/Cutting tools
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Working With Hands/No Tool
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Working With Tools
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input activity" type="checkbox" value="" name="activity_check">Not Activity Related
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="tabRen">
                                            <div class="card nli_tab_main_card">
                                              <div class="card-header card-header-rose card-header-text sub_card">
                                                <div class="card-text card_text_title">
                                                  <h4 class="card-title">General</h4>
                                                </div>
                                              </div>
                                              <div class="card-body sub_row">
                                                  <h4 class="sub_child_title">Job Task: (Check the one that applies)  </h4>
                                                  <hr class="sub_child_title_hr">
                                                <div class="row nli_checkbox_row">
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Business Travel
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Calibration
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Carbon Change
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Carpentry/Woodwork
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Cleaning/Abrasive Blast
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Cleaning/Non Blasting
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Cleaning/ Tank
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Cleaning/Housekeep
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Cleaning/Hydroblasting
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Confined Space Entry
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Construction/Installation
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Control Room Ops
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Crane/Rigging/Lifting
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Demolition/Removal
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Dewatering
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>

                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Drilling/Workov/Wireline
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Earthmov/Excav./Trench
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Electrical Repair/Maint
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Energy Isolation/Control
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Environmental Remediat
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Equipment Purging
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Equipment Operation
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Equipment Start/Shut
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Equipment Open/Blind
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Food Prep/Handling
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Gauging/Samplings
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">GeoProbe/Direct Push
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Inspection
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Lab-General
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label job_task">
                                                        <input class="form-check-input" type="checkbox" value="" name="job_task_check">Local Operation
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>

                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Marine Operation
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Masonry/Concrete/Paving
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Mobile Rem/Vacuum Event
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Monitoring/Rounds
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">O & M
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Office Work
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Painting/Coating/Insulation
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Pavement Cutting
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Pigging
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Pipeline Operations
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Plumbing / Piping
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Quality Control /Testing
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Railcar / Loading / Unloading
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Repair/Mtce./Electrical
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Repair/Mtce./Schl./Routine
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>

                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Repair/Un Schl./NonRoutine
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Rigging
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Scaffold/Erection/Dismantle
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Subsurface Clearance
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Surveying
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Tank Truck load/Unload
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Transfer/Between/Tanks
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Transportation/Personnel
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Transportation/Equip/Mat
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Underwater work
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Vegetation Control/Landscape
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Waste Management
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Welding/Cutting/Brazing
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Work Permitting
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input job_task" type="checkbox" value="" name="job_task_check">Working at Heights
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>                                                
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="tabRen">
                                            <div class="card nli_tab_main_card">
                                              <div class="card-header card-header-rose card-header-text sub_card"> 
                                                <div class="card-text card_text_title">
                                                  <h4 class="card-title">General</h4>
                                                </div>
                                              </div>
                                              <div class="card-body sub_row">
                                                <div class="row input_row">
                                                  <div class="col-md-12">
                                                    <h4 class="sub_child_title">Phase Of Operation </h4>
                                                    <hr class="sub_child_title_hr">
                                                  </div>
                                                </div>
                                                <div class="row nli_checkbox_row">
                                                  <div class="col-md-4 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input operation_phase" type="checkbox" value="" name="phase_check">Commissioning/Startup
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input operation_phase" type="checkbox" value="" name="phase_check">Maintenance
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-4 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input operation_phase" type="checkbox" value="" name="phase_check">Emergency Prep/Response
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input operation_phase" type="checkbox" value="" name="phase_check">Normal Operations
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-4 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input operation_phase" type="checkbox" value="" name="phase_check">Projects
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input operation_phase" type="checkbox" value="" name="phase_check">Abnormal/Non-Routine Ops
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="card">
                                              <div class="card-header card-header-rose card-header-text sub_card"> 
                                                <div class="card-text card_text_title">
                                                  <h4 class="card-title">SUBTYPES & DOMINANT INCIDENT TYPE</h4>
                                                </div>
                                              </div>
                                              <div class="card-body sub_row">
                                                <div class="row input_row">
                                                  <div class="col-md-12">
                                                    <h4 class="sub_child_title">Subtypes: (Check all that apply) </h4>
                                                    <hr class="sub_child_title_hr">
                                                    <h4 class="sub_child_title">Dominant Incident Type: (Circle or underline one only)</h4>
                                                    <hr class="sub_child_title_hr">
                                                    <span class="sub_child_title_span">For each loss type in “Italic/Bold”, a consequence form most be completed and input in to Impact.</span>
                                                  </div>
                                                </div>
                                                <div class="row nli_checkbox_row">
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Adverse Weather
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label label_to_be_italic">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Business Interruption
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Contamination
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Cyber Security 
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Fatality
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label label_to_be_italic">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Fire/ Explosion
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                  </div>

                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label label_to_be_italic">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Injury/ Illnesses
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Natural Disaster
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label label_to_be_italic">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Environmental
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Process Safety
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Product Quality
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                  </div>

                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Product Stewardship Event
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label label_to_be_italic">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Property Equip./Damage
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Reliability
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Reputation
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label label_to_be_italic">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Security
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                  </div>

                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Supply Interruption
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label label_to_be_italic">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Transportation of Commodities
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label label_to_be_italic">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Transportation of Personnel
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Worker Fatigue
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input dom_incident" type="checkbox" value="" name="dominent_check">Workplace Hazards
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                  </div>
                                                </div>
                                                <div class="row input_row">
                                                  <div class="col-md-12">
                                                    <h4 class="sub_child_title">Incident Flags (Check the one that applies)</h4>
                                                    <hr class="sub_child_title_hr">
                                                  </div>
                                                </div>
                                                <div class="row nli_checkbox_row">
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Air Patrol
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Alarm/Flood
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Alcohol Drug Test Conducted
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Contractor Incident 
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Employee Injury/Illness
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                  </div>

                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Environmental Impact
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Inhibited Alarms
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Life Saving Rule
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">One Call Incident 
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Quality Incident
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Process Safety Incident
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                  </div>

                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">COP (Critical Operating Parameter)
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Process Safety Near Miss – Demand on Safety System (DOSS)
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Process Safety Near Loss (Other)
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Process Safety Near Loss (Safe Oper Limit Exceedance (SOLE)
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                  </div>

                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Hazard Loss –Stewardable (Fatality or > $1M Direct Cost )
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Hazard Loss Non Stewardable (i.e. Natural Disaster)
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Security Corporate Report
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Standing Alarms
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>

                                                  </div>
                                                </div>
                                              </div>
                                            </div>

                                          </div>

                                          <div class="tabRen">
                                            <div class="card nli_tab_main_card">
                                              <div class="card-header card-header-rose card-header-text sub_card"> 
                                                <div class="card-text card_text_title">
                                                  <h4 class="card-title">IRAT SCORE (Not required for MVAs & Damage Prevention LIs)</h4>
                                                </div>
                                              </div>
                                              <div class="card-body sub_row">
                                                <div class="row input_row">
                                                  <div class="col-md-4 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="make_equip" class="bmd-label-floating input_label">Actual Consequence Level:</label>
                                                      <input type="number" class="form-control form_input_field form_fields" id="act_con_level" name="act_con_level" required="">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-8 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="make_equip" class="bmd-label-floating input_label">Comments</label>
                                                      <input type="number" class="form-control form_input_field form_fields" id="act_con_level" name="act_con_level" required="">
                                                    </div>
                                                  </div>
                                                </div>

                                                <div class="row input_row">
                                                  <div class="col-md-4 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="make_equip" class="bmd-label-floating input_label">Potential Consequence Level:</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="act_con_level" name="act_con_level" required="">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-8 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="make_equip" class="bmd-label-floating input_label">Comments</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="act_con_level" name="act_con_level" required="">
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row input_row">
                                                  <div class="col-md-12">
                                                    <h4 class="sub_child_title">IRAT Barriers: (Check all that applies) </h4>
                                                    <hr class="sub_child_title_hr">
                                                  </div>
                                                </div>
                                                <div class="row nli_checkbox_row">
                                                  <div class="col-md-4 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="irat_check">Human Intervention
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="irat_check">Safety Instrumented Systemss
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="irat_check">Safety valves & Rupture Discs
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>                                            
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="irat_check">Process Control Computer
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="irat_check">Post-release mitigation
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>

                                                  <div class="col-md-4 input_wrapper">
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="irat_check">Personal Protective Equipment
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="irat_check">Technical Security Countermeasure
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="irat_check">Physical Security Countermeasures
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>                                            
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="irat_check">Security Post-Incident Mitigation
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                    <div class="form-check">
                                                      <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="" name="irat_check">Other Independent Barriers
                                                        <span class="form-check-sign">
                                                          <span class="check"></span>
                                                        </span>
                                                      </label>
                                                    </div>
                                                  </div>

                                                  <div class="col-md-4">
                                                    <fieldset class="custom_fieldset irat_score_fieldsets">
                                                        <legend>IRAT Score </legend>
                                                        <div>
                                                          <span>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field" id="irat_score_one" name="irat_score_one">
                                                            </div>
                                                          </span>
                                                          <span> X </span>
                                                          <span>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field" id="irat_score_two" name="irat_score_two">
                                                            </div>
                                                          </span>
                                                        <span> = </span>
                                                          <span>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field" id="irat_score_two" name="irat_score_two">
                                                            </div>
                                                          </span>
                                                        </div>
                                                       
                                                    </fieldset>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="tabRen">  
                                            <div class="card">
                                              <div class="card-header card-header-rose card-header-text sub_card"> 
                                                <div class="card-text card_text_title">
                                                  <h4 class="card-title">INVESTIGATION & SOLUTIONS</h4>
                                                </div>
                                              </div>
                                              <div class="card-body sub_row">
                                                <div class="row">
                                                  <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="make_equip" class="bmd-label-floating input_label">Date Investigation Started</label>
                                                      <input type="text" class="datepicker form-control form_input_field form_fields" id="investiation_start_date" name="investiation_start_date">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="make_equip" class="bmd-label-floating input_label">Team Leader</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="investigation_team_leader" name="investigation_team_leader" required="">
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                  <div class="table-responsive">
                                                    <table class="table investigation_table">
                                                      <thead class="">
                                                        <tr>
                                                          <th>
                                                          Team Memebers
                                                          </th>
                                                          <th>
                                                          Position
                                                          </th>
                                                          <th>
                                                          Company
                                                          </th>
                                                        </tr>
                                                      </thead>
                                                      <tbody>
                                                        <tr>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="investigation_team_member" name="investigation_team_member[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="investigation_team_position" name="investigation_team_position[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="investigation_team_company" name="investigation_team_company[]" required="">
                                                            </div>
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="investigation_team_member" name="investigation_team_member[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="investigation_team_position" name="investigation_team_position[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="investigation_team_company" name="investigation_team_company[]" required="">
                                                            </div>
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="investigation_team_member" name="investigation_team_member[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="investigation_team_position" name="investigation_team_position[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="investigation_team_company" name="investigation_team_company[]" required="">
                                                            </div>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                  <div class="col-md-12">
                                                    <span class="sub_child_title_span">
                                                      Solutions: 1 Complete the FRCS and answer ALL 7 factor questions; 2. Identify root cause(s) when answering NO to factors 1-4 by explaining why the “equivalent of a questionable item(s) occurred and circle root cause(s) when answering YES to Factors 5-7; and 3. Transfer the solution(s) that addresses each root cause following the solution guidance.
                                                    </span>
                                                  </div>
                                                </div>

                                                <div class="row">
                                                  <div class="table-responsive">
                                                    <table class="table frcs_table">
                                                      <thead class="">
                                                        <tr>
                                                          <th>
                                                          #
                                                          </th>
                                                          <th>
                                                          FRCS Factor #
                                                          </th>
                                                          <th>
                                                          Related OIMS System # (IRAT > 200)
                                                          </th>
                                                          <th>
                                                          Solution(s) (Transfer solutions from FRCS Form) 
                                                          </th>
                                                          <th>
                                                          Person Responsible
                                                          </th>
                                                          <th>
                                                          Comletion Target Date
                                                          (MM/DD/YYYY)
                                                          </th>
                                                          <th>
                                                          Comletion Actual Date
                                                          (MM/DD/YYYY)
                                                          </th>
                                                          <th>
                                                          FLS V&V Date
                                                          (MM/DD/YYYY)
                                                          </th>
                                                        </tr>
                                                      </thead>
                                                      <tbody>
                                                        <tr>
                                                          <td>
                                                            1
                                                          </td>
                                                          <td>
                                                            1
                                                          </td>
                                                          <td>
                                                            1
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="trans_sol_frcs" name="trans_sol_frcs[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="person_responsible" name="person_responsible[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="target_date" name="target_date[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="actula_date" name="actual_date[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="v_and_v_date" name="v_and_v_date[]" required="">
                                                            </div>
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            2
                                                          </td>
                                                          <td>
                                                            2
                                                          </td>
                                                          <td>
                                                            2
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="trans_sol_frcs" name="trans_sol_frcs[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="person_responsible" name="person_responsible[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="target_date" name="target_date[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="actula_date" name="actula_date[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="v_and_v_date" name="v_and_v_date[]" required="">
                                                            </div>
                                                          </td>
                                                        </tr>

                                                        <tr>
                                                          <td>
                                                            3
                                                          </td>
                                                          <td>
                                                            3
                                                          </td>
                                                          <td>
                                                            3
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="trans_sol_frcs" name="trans_sol_frcs[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="form-control form_input_field form_fields" id="person_responsible" name="person_responsible[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="target_date" name="target_date[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="actula_date" name="actula_date[]" required="">
                                                            </div>
                                                          </td>
                                                          <td>
                                                            <div class="form-group bmd-form-group">
                                                              <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="v_and_v_date" name="v_and_v_date[]" required="">
                                                            </div>
                                                          </td>
                                                        </tr>

                                                      </tbody>
                                                    </table>
                                                  </div>
                                                </div>
                                                <div class="row"> 
                                                    <div class="col-md-12">
                                                      <div class="form-group bmd-form-group">
                                                        <label for="result_of_fls" class="bmd-label-floating input_label">Results of FLS Solution Verification & Validation</label>
                                                        <input type="text" class="form-control form_input_field form_fields" id="result_of_fls" name="result_of_fls" required="" aria-required="true">
                                                      </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                      <div class="form-group bmd-form-group">
                                                        <label for="recycle_comments" class="bmd-label-floating input_label">Recycle Comments</label>
                                                        <input type="text" class="form-control form_input_field form_fields" id="recycle_comments" name="recycle_comments" required="" aria-required="true">
                                                      </div>
                                                    </div> 
                                                </div>

                                                <div class="row"> 
                                                    <div class="col-md-6">
                                                      <div class="form-group bmd-form-group">
                                                        <label for="form_completed_by" class="bmd-label-floating input_label">Form Completed By</label>
                                                        <input type="text" class="form-control form_input_field form_fields" id="form_completed_by" name="form_completed_by" required="" aria-required="true">
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                      <div class="form-group bmd-form-group">
                                                        <label for="title" class="bmd-label-floating input_label">Title</label>
                                                        <input type="text" class="form-control form_input_field form_fields" id="title" name="title" required="" aria-required="true">
                                                      </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                      <div class="form-group bmd-form-group">
                                                        <label for="date_prepared" class="bmd-label-floating input_label">Date Prepared</label>
                                                        <input type="text" class="datepicker form-control form_input_field form_fields" id="date_prepared" name="date_prepared" required="" aria-required="true">
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                      <div class="form-group bmd-form-group">
                                                        <label for="reviewer_name" class="bmd-label-floating input_label">Reviewer Name</label>
                                                        <input type="text" class="form-control form_input_field form_fields" id="reviewer_name" name="reviewer_name" required="" aria-required="true">
                                                      </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                      <div class="form-group bmd-form-group">
                                                        <label for="approver_name" class="bmd-label-floating input_label">Approver Name</label>
                                                        <input type="text" class="form-control form_input_field form_fields" id="approver_name" name="approver_name" required="" aria-required="true">
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                      <div class="form-group bmd-form-group">
                                                        <label for="other" class="bmd-label-floating input_label">Other</label>
                                                        <input type="text" class="form-control form_input_field form_fields" id="other" name="other" required="" aria-required="true">
                                                      </div>
                                                    </div>
                                                    <div class="col-md-12 text-center my-5">
                                                      <span class="footer_note_danger text-danger">Completed FRCS Form(s) must be attached to this report</span>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="row msf_btn_holder">
                                            <div class="col-md-6 save_btn_wrapper text-left" style="float:right;">
                                              <a class="btn btn-round btn-warning nextPrevBtn prev" href="#top_nli_card" id="prevBtnRen" type="button" onclick="nextPrevRen(-1)" style="display: inline;">Previous step<div class="ripple-container"></div></a>
                                            </div>
                                            <div class="col-md-6 save_btn_wrapper text-right">
                                              <a class="btn custom-btn-one btn-round btn-success nextPrevBtn next" href="#top_nli_card" id="nextBtnRen" type="button" onclick="nextPrevRen(1)">Next step<div class="ripple-container"></div></a>
                                              
                                              <button class="btn custom-btn-one btn-round btn-success nextPrevBtn submitBtnRen" type="submit" onclick="" style="">Submit<div class="ripple-container"></div></button>
                                            </div>

                                          </div>

                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                </form>
                              </div>

                              <div style="overflow:auto;" class="checklist_btn_holder row">
                                <div class="col-md-6 text-left">
                                  <a type="button" id="prevBtn" class="checkListPrevBtn" onclick="nextPrevChecklist(-1, this)" href="#checklistForm btn">Previous form</a>
                                </div>
                                <div class="col-md-6 text-right">
                                  <a type="button" id="nextBtn" class="checkListNextBtn" onclick="nextPrevChecklist(1, this)" href="#checklistForm">Next form</a>
                                  <a type="button" id="" class="checklistSubmitBtn" onclick="" href="#checklistForm">Submit</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="tab-pane" id="post_checklist_tab">
                        <div class="card">
                          <div class="card-header card-header-tabs card-header-rose custom_card_header mng_view_card_header" id="agreement_tab_header">
                            <div class="nav-tabs-navigation">
                              <div class="nav-tabs-wrapper">
                                <ul class="nav nav-tabs sub-insurance-nav" data-tabs="tabs">
                                  <li class="nav-item">
                                    <a class="nav-link active" href="#jsa_worksheet" data-toggle="tab">
                                      <i class="material-icons">source</i>JSA Worksheet
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="#jobs_site_analysis" data-toggle="tab">
                                      <i class="material-icons">source</i>Job Analysis
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li>

                                  <li class="nav-item">
                                    <a class="nav-link" href="#pre_trip_report" data-toggle="tab">
                                      <i class="material-icons">source</i>Pre Trip Report
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>

                          <div class="tab-content">
                            <div class="tab-pane inventory_sub_card subcontractor_agreement_tab active" id="jsa_worksheet">
                              <form action="" id="jsa_worksheet_form">
                                <div class="card" id="jsa_worksheet_card">
                                  <div class="card-body">
                                    <div class="row jsa_header_row">
                                      <!-- <div class="col-md-12 text-center">
                                        <div class="logo">
                                        <img src="{{asset('assets/img/logo_new.png')}}" alt="logo"/>
                                        </div>
                                        <h2 class="main-title">JSA Worksheet</h2>
                                        <p> 903 W.Industrial, Midlan, Texas, 79701 </p>
                                        <p> 215 E. Elm Street, Loving, New Mexico 88256</p>
                                      </div> -->
                                      <div class="col-md-4 input_wrapper jsa_worksheet_left">
                                        <div class="form-group bmd-form-group">
                                          <label for="jsa_date" class="bmd-label-floating input_label">Date</label>
                                          <input type="text" class="datepicker form-control form_input_field" id="jsa_date" name="jsa_date">
                                        </div>
                                        <div class="form-group bmd-form-group">
                                          <label for="jsa_time" class="bmd-label-floating input_label">Time</label>
                                          <input type="text" class="timepicker form-control form_input_field" id="jsa_time" name="jsa_time">
                                        </div>                                  
                                      </div>
                                      <div class="col-md-4 input_wrapper text-center jsa_worksheet_center"> 
                                       <div class="logo">
                                        <img src="{{asset('assets/img/logo_new.png')}}" alt="logo"/>
                                        </div>
                                        <h2 class="main-title">JSA Worksheet</h2>
                                        <p> 903 W.Industrial, Midlan, Texas, 79701 </p>
                                        <p> 215 E. Elm Street, Loving, New Mexico 88256</p>
                                      </div>
                                      <div class="col-md-4 input_wrapper jsa_worksheet_right">
                                        <div class="form-group bmd-form-group">
                                          <label for="jsa_supervisor" class="bmd-label-floating input_label">Supervisor</label>
                                          <input type="text" class="form-control form_input_field" id="jsa_supervisor" name="jsa_supervisor">
                                        </div>
                                        <div class="form-group bmd-form-group">
                                          <label for="jsa_job_description" class="bmd-label-floating input_label">Job Description</label>
                                          <input type="text" class="form-control form_input_field" id="jsa_job_description" name="jsa_job_description">
                                        </div>                                  
                                      </div>                                
                                    </div>

                                    <div class="row">
                                      <div class="col-md-1 progress_container" style="text-align:center;margin-bottom:20px;">
                                        <span class="stepJsa step-first">
                                          <i class="material-icons">done</i>
                                        </span>
                                        <span class="stepJsa step-inner">
                                          <i class="material-icons">done</i>
                                        </span>
                                        <span class="stepJsa step-inner">
                                          <i class="material-icons">done</i>
                                        </span>
                                        <span class="stepJsa step-inner">
                                          <i class="material-icons">done</i>
                                        </span>
                                        <span class="stepJsa step-inner">
                                          <i class="material-icons">done</i>
                                        </span>                               
                                      </div>

                                      <div class="col-md-11 content_container rental_ag_content_container">
                                        <div class="tabJsa">
                                          <div class="card jsa_tab_main_card">
                                            <!-- <div class="card-header card-header-rose card-header-text sub_card">
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">General</h4>
                                              </div>
                                            </div> -->
                                            <div class="card-body sub_row">

                                              <div class="row jsa_body">
                                                <div class="col-md-3 first_tab_input_field">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="jsa_locaiton" class="bmd-label-floating input_label">Location</label>
                                                    <input type="text" class="form-control form_input_field" id="jsa_location" name="jsa_locaitons">
                                                  </div>
                                                </div>
                                                <div class="col-md-3 first_tab_input_field">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="jsa_sop" class="bmd-label-floating input_label">SOP Reviewed</label>
                                                    <input type="text" class="form-control form_input_field" id="jsa_sop" name="jsa_sop">
                                                  </div>
                                                </div>
                                                <div class="col-md-3 first_tab_input_field">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="jsa_lats" class="bmd-label-floating input_label">Lats</label>
                                                    <input type="text" class="form-control form_input_field" id="jsa_lats" name="jsa_lats">
                                                  </div>
                                                </div>
                                                <div class="col-md-3 first_tab_input_field">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="jsa_long" class="bmd-label-floating input_label">Long</label>
                                                    <input type="text" class="form-control form_input_field" id="jsa_long" name="jsa_long">
                                                  </div>
                                                </div>
                                                <div class="col-md-12 jsa_table_wrapper">
                                                  <div class="table-responsive jsa_table">
                                                    <table class="table">
                                                      <thead>
                                                        <tr>
                                                          <th>Basic Job Steps</th>
                                                          <th>Identify Potential Hazards</th>
                                                          <th>Hand/Finger Potential</th>
                                                          <th>Environmental</th>
                                                          <th>What you did to eliminate or reduce the risk</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody>
                                                        <tr>
                                                          <td>
                                                            <input type="text" class="form-control form_input_field" id="" name="" placeholder="value">
                                                          </td>
                                                          <td>
                                                            <input type="text" class="form-control form_input_field" id="" name="" placeholder="value">
                                                          </td>
                                                          <td class="">
                                                            <input type="number" class="form-control form_input_field" id="" name="" placeholder="value">
                                                          </td>
                                                          <td class="">
                                                            <input type="number" class="form-control form_input_field" id="" name="" placeholder="value">
                                                          </td>
                                                          <td class="">
                                                            <input type="number" class="form-control form_input_field" id="" name="" placeholder="value">
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                  <p class="text-center"> Use the go card on reverse and have all parties sign on the back.</p>
                                                  <p class="text-center"> Document revisions ( after break, change in job, addition of employees at worksite,etc )</p>
                                                </div>
                                              </div>

                                            </div>
                                          </div>
                                        </div>
                                        <div class="tabJsa">
                                          <div class="card jsa_tab_main_card">
                                            <div class="card-body sub_row">
                                              <h4 class="sub-title">Job Steps </h4>
                                              <div class="row">
                                                <div class="col-md-12">
                                                  <ul class="subcontractor_agre_list bold">
                                                    <li>
                                                      What is the written procedure for this task that we are going to do?
                                                    </li>
                                                    <li>
                                                      What are the job steps for this task? How are you going to do this job?
                                                    </li>
                                                    <li>
                                                      What job steps are each of you going to be doing during the job?
                                                    </li>
                                                    <li>
                                                      What training havve you had on the job steps and or procedure for the task?
                                                    </li>
                                                    <li>
                                                      What permit do we need before we do this task ( Hot Work Confined Space. LOTO. et.)?
                                                    </li>
                                                  </ul>
                                                </div>
                                              </div>
                                              <h4 class="sub-title">Equipment & Tools </h4>
                                              <div class="row">
                                                <div class="col-md-12">
                                                  <ul class="subcontractor_agre_list bold">
                                                  <li>
                                                    What tools do we need to do this job right?
                                                  </li>
                                                  <li>
                                                    What is the right way to use these tools?
                                                  </li>
                                                  <li>
                                                    When is the last time we looked at the tools to ensure they are clean and in good working shape?
                                                  </li>
                                                  </ul>
                                                </div>
                                              </div>

                                            </div>
                                          </div>
                                        </div>
                                        <div class="tabJsa">
                                          <div class="card jsa_tab_main_card">
                                            <div class="card-body sub_row">
                                              <h4 class="sub-title">Identify Hazards </h4>
                                              <div class="row">
                                                <div class="col-md-12">
                                                  <p class="jsa-paragraph"> Please rememeber - For every hazard you must do something about it!</p>
                                                  <p class="jsa-paragraph"> Elimnate the hazard or mitigate the risk down to an acceptable level! </p>
                                                  <ul class="subcontractor_agre_list bold">
                                                    <li>
                                                      What can fall on us or around us?
                                                    </li>
                                                    <li>
                                                      What can we get caught in?
                                                    </li>
                                                    <li>
                                                      What can we get smashed by or pinned between?
                                                    </li>
                                                    <li>
                                                      What can we get out hands/fingers pinched in between?
                                                    </li>
                                                    <li>
                                                      What could hit us in the face or head?
                                                    </li>
                                                    <li>
                                                      What can hurt our backs?
                                                    </li>
                                                    <li>
                                                      What can we trip over?
                                                    </li>
                                                    <li>
                                                      What can we fall off of or fall through?
                                                    </li>
                                                    <li>
                                                      What can burn us while we are doing this job??
                                                    </li>
                                                    <li>
                                                      What could sting or bite us?
                                                    </li>
                                                    <li>
                                                      What electrical current can we be shocked by?
                                                    </li>
                                                    <li>
                                                      What could we breathe in, swallow or get on our sking that could hurt us?
                                                    </li>
                                                    <li>
                                                      What could we be doing that would be too much for our bodies to take?
                                                    </li>
                                                    <li>
                                                      What new employees are on location who might be a hazard to themselves or us?
                                                    </li>
                                                  </ul>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="tabJsa">
                                          <div class="card jsa_tab_main_card">
                                            <div class="card-body sub_row">
                                              <h4 class="sub-title">Personal Protective Equipment</h4>
                                              <div class="row">
                                                <div class="col-md-12">
                                                  <ul class="subcontractor_agre_list bold">
                                                    <li>
                                                      What is the right PPE that we need to protect ourselves? ( Hard Hat, Glasses/ Goggles, Ear Plugs, Gloves, Steel Toe Boots, Respiratory Protectionk, Fire Retardant Clothing, Fall Protection)
                                                    </li>
                                                  </ul>
                                                </div>
                                              </div>
                                              <h4 class="sub-title">Changing The Course Of Works</h4>
                                              <div class="row">
                                                <div class="col-md-12">
                                                  <ul class="subcontractor_agre_list bold">
                                                    <li>
                                                      What would make us stop the job or change the job steps for the job?
                                                    </li>
                                                    <li>
                                                      What would we have to change our tools or equipment?
                                                    </li>
                                                    <li>
                                                      Why would we have to change where we are standing?
                                                    </li>
                                                    <li>
                                                      What would cause us to have to change out P.P.E?
                                                    </li>
                                                  </ul>
                                                </div>
                                              </div>
                                              <p class="jsa_form_foot_note text-center"> Each of us have the right and obligation to <span> Stop Unsafe acts </span></p>
                                              <p class="jsa_submission_foot_note text-center"> Please Submit The Completed JSA form to your superviser</p>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="tabJsa">
                                          <div class="card jsa_tab_main_card">
                                              <div class="card-body sub_row">
                                                <div class="row">
                                                  <div class="col-md-12 text-center ending_para_wrapper">
                                                    <p>Revisit and refresh the JSA after breaks, change in job/employees & when visitors are on location.</p>
                                                  </div>
                                                </div>
                                                <h4 class="sub-title">Signature</h4>
                                                <div class="row">
                                                  <div class="col-md-12 text-center ending_para_wrapper">
                                                    <p>JSA performed to protect the employees who signed above </p>
                                                  </div>
                                                </div>
                                              </div>
                                          </div>
                                        </div>                                
                                        <div class="row msf_btn_holder">
                                          <div class="col-md-6 save_btn_wrapper text-left" style="float:right;">
                                            <a class="btn btn-round btn-warning nextPrevBtn prev" href="#jsa_worksheet_card" id="prevBtnJsa" type="button" onclick="nextPrevJsa(-1)" style="display: inline;">Previous<div class="ripple-container"></div></a>
                                          </div>
                                          <div class="col-md-6 save_btn_wrapper text-right">
                                            <a class="btn custom-btn-one btn-round btn-success nextPrevBtn next" href="#jsa_worksheet_card" id="nextBtnJsa" type="button" onclick="nextPrevJsa(1)">Next<div class="ripple-container"></div></a>
                                            
                                            <button class="btn custom-btn-one btn-round btn-success nextPrevBtn submitBtnJsa" type="submit" onclick="" style="">Submit<div class="ripple-container"></div></button>
                                          </div>
                                        </div>

                                      </div>

                                    </div>
                                  </div>



                                    
                                </div>
                              </form>
                            </div>

                            <div class="tab-pane inventory_sub_card rental_agreement_tab" id="jobs_site_analysis">

                            </div>

                            <div class="tab-pane inventory_sub_card subcontractor_agreement_tab" id="pre_trip_report">
                              PRe TRip MOved
                            </div>   
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

<script>











$('.insurance_check').change(function() {

  $(".insurance_check").prop('checked',false);
  $(this).prop('checked',true);

  if($(this).attr("value") == 'delete') {
    $(".form_card input, select, textarea").not("input[name=send_mail_to], input[name=date]").attr('readonly',true);
    $(":checkbox").not("input[name=request]").bind("click", false);
    $('.input-files.driver_files').css('pointer-events','none')

  }
  else{
   $(".form_card input, select, textarea").removeAttr('readonly');
    $(":checkbox").unbind("click");
    $('.input-files.driver_files').css('pointer-events','auto')
  }      

});









//  File Upload JS
$(function(){
  $('.input-files').fileUploader({
    preloaded: preloaded,
    extensions: ['.pdf'],
    mimes: ['application/pdf'],
    //imagesInputName:'images'
    required: true,
  });


})


$('input[type="file"].imagesInput').each(function(){
    $(this).rules('add', {
        messages: {
            required: "this FILE upload field is required"
        }
    })
});



$(window).on('load', function(){
  setTimeout(function() {
     $('.material-datatables').css('overflow-x','unset');
  }, 100);
});

var date = new Date();
var only_year = date.getFullYear();
$('.year_to_pick').yearpicker({
  endYear:only_year,
  year:null
});






var currency_value_validator = function(input_field,key){

    var s = String.fromCharCode(key.which);
    if(s >=0 || s<=9 ){
      // input_field.attr('maxlength','14');
      // input_field.prop('type','text');
    }
    else{
      key.preventDefault();
      alert("Please Type A Number")
    }
}



// Jquery Dependency

$(".insurance_limit_currency input").on({
    keyup: function(e) {
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});

$(".currency_input").on({
    keyup: function(e) {
      console.log("currency typing")
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});



$('.rent_time select').on('change',function(){
  if($(this).val()!== ""){
    $(this).siblings('label').text("")
  }
})





// the selector will match all input controls of type :checkbox
// and attach a click event handler 
$("input.location_process_unit").on('click', function() {
  // in the handler, 'this' refers to the box clicked on
  var $box = $(this);
  if ($box.is(":checked")) {
    // the name of the box is retrieved using the .attr() method
    // as it is assumed and expected to be immutable
    var group = "input.location_process_unit[name='" + $box.attr("name") + "']";
    // the checked state of the group/box on the other hand will change
    // and the current value is retrieved using .prop() method
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});

$("input.loss_measurement").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input.loss_measurement[name='" + $box.attr("name") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});

//  Activity Check
$("input.activity").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input.activity[name='" + $box.attr("name") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});


//  Job task  Check
$("input.job_task").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input.job_task[name='" + $box.attr("name") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});
//  Phase of operation  Check
// $("input.operation_phase").on('click', function() {
//   var $box = $(this);
//   if ($box.is(":checked")) {
//     var group = "input.operation_phase[name='" + $box.attr("name") + "']";
//     $(group).prop("checked", false);
//     $box.prop("checked", true);
//   } else {
//     $box.prop("checked", false);
//   }
// });
// $('.agreement_info_tooltip').tooltip(); 



//  Dominant  Check
$("input.dom_incident").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input.dom_incident[name='" + $box.attr("name") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});


//  Incident Flag  Check
$("input[name='inc_flag_check']").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input[name='" + $box.attr("name") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});


$('input[name="speci_plate"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="speci_plate"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="pressure_vessel_code"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="pressure_vessel_code"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="truck_ins_sticker"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="truck_ins_sticker"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="cargo_tank_clean"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="cargo_tank_clean"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="truck_equip"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="truck_equip"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="press_valves_relief"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="press_valves_relief"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="float_liq_level_ind"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="float_liq_level_ind"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

//  Second Tab

$('input[name="vac_equi_50"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="vac_equi_50"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="hoses_access"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="hoses_access"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="conn_clean"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="conn_clean"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="hoses_conn_fitt"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="hoses_conn_fitt"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="hoses_100mm"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="hoses_100mm"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="bonding_hoss_ground"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="bonding_hoss_ground"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="strong_alligator"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="strong_alligator"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="wheel_chocks"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="wheel_chocks"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="PTO_driven"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="PTO_driven"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})


//  3rd Tab 

$('input[name="prop_hazard_load"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="prop_hazard_load"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="equipped_other_ins"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="equipped_other_ins"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="wearing_protection"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="wearing_protection"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="carrying_certification"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="carrying_certification"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="vacuum_truck_log"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="vacuum_truck_log"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})



//  4th Tab 

$('input[name="truck_locate_35"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="truck_locate_35"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="vacuum_pump_discharge"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="vacuum_pump_discharge"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="temp_mat_not_75"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="temp_mat_not_75"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="ope_truck_source"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="ope_truck_source"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="truck_engine_shut_down"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="truck_engine_shut_down"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})
$('input[name="truck_park_ground"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="truck_park_ground"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="cond_path_truck_source"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="cond_path_truck_source"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})

$('input[name="grounded_metallic_structure"]').on('change',function(){

    $(this).attr("checked","true");
    $(this).siblings().removeAttr("checked");

    console.log()
  
    if($('input[name="grounded_metallic_structure"][value="yes"]').attr("checked")){
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').attr('disabled',true)
    }
    else{
      $(this).parents('.custom-radio-main-section').siblings('.vacume_truck_comments_sec').find('input').removeAttr('disabled')
    }
  
})


</script>


<!--  Checklist Multi Step JS  -->

<script>
var currentTabChecklist = 0; // Current tab is set to be the first tab (0)
showTabChecklist(currentTabChecklist); // Display the current tab

function showTabChecklist(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tabChecklist");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  console.log(n)
  if (n == (x.length - 1)) {
    // document.getElementById("nextBtn").innerHTML = "Submit";
    // $('.checklistSubmitBtn').show();
    $('#nextBtn').show();
  }
  // else if( n == 1 ){
  //   $(".checkListNextBtn").hide();
    
  // }
  else {
    $('.checklistSubmitBtn').hide();
    $('#nextBtn').show();
    // document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicatorChecklist(n)





  if(currentTabChecklist == 0){
    $('.checkListNextBtn').attr('active_tab','pre_trip')
  }
  else if(currentTabChecklist == 1 ){
    $('.checkListNextBtn').removeAttr('active_tab')
    $('.checkListNextBtn').attr('active_tab','vacuum_checklist')
  }
  else if(currentTabChecklist == 2 ){


    $('.checkListNextBtn').removeAttr('active_tab')
    $('.checkListNextBtn').attr('active_tab','bbs_form')
  }
  else if(currentTabChecklist == 3 ){


    $('.checkListNextBtn').removeAttr('active_tab')
    $('.checkListNextBtn').attr('active_tab','job_analysis')
  }
  else if(currentTabChecklist == 4 ){


    $('.checkListNextBtn').removeAttr('active_tab')
    $('.checkListNextBtn').attr('active_tab','nli_form')
  }



}

function nextPrevChecklist(n,el) {
  console.log(el)
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tabChecklist");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateFormChecklist()) return false;
  // Hide the current tab:
  x[currentTabChecklist].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTabChecklist = currentTabChecklist + n;
  // if you have reached the end of the form...
  if (currentTabChecklist >= x.length) {
    // ... the form gets submitted:
    document.getElementById("checklistForm").submit();
    return false;

  }

  // Otherwise, display the correct tab:
  showTabChecklist(currentTabChecklist);



}


function blah(){
        let value = 1;
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getVariablesBySubcat') }}",
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result)
            { 

            }
         })
}


function validateFormChecklist() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tabChecklist");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = true;
    }
  }

  if($('.checkListNextBtn').attr('active_tab') == 'vacuum_checklist'){
     var sub_tab = document.getElementsByClassName("tabVehOpeCheck");
      if(!$(sub_tab[4]).is(':visible')){
        valid = false
        alert("Check And Complete the Full Form")
      }

  }
  if($('.checkListNextBtn').attr('active_tab') == 'job_analysis'){
     var job_analysis_sub_tab_ = document.getElementsByClassName("tabJobSiteAnalysis");
      if(!$(job_analysis_sub_tab_[3]).is(':visible')){
        valid = false
        alert("Check And Complete the Full form")
      }

  }
  if($('.checkListNextBtn').attr('active_tab') == 'nli_form'){
     var nli_sub_tab = document.getElementsByClassName("tabRen");
      if(!$(nli_sub_tab[5]).is(':visible')){
        valid = false
        alert("Check And Complete the Full form")
      }
      // if($(nli_sub_tab[5]).is(':visible')){
      //   $('.checklistSubmitBtn').show()
      //   $('#nextBtn').hide()
      // }

  }

  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("stepChecklist")[currentTabChecklist].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicatorChecklist(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("stepChecklist");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
    // x[n].firstChild.className = x[i].firstChild.className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
  // x[n].firstChild.className += " active"
}









  // SIGNATURE VALIDATION on submit

  // var canvas = $('canvas')[0];
  // if($('.sign_input_validation_owner').val() == "0"){
  //   $('.sign_error_owner_msg').show();
  //   return false;
  // }
  // else{
  //   $('.sign_error_owner_msg').hide();
  //   if($("#agree_chk").prop('checked') == false)
  //   {
  //     alert("You have to agree with terms & condition before submit, so please click on the checkbox");
  //     //e.preventDefault();
  //     return false;
  //   }
  // }


// SIGNATURE VALIDATION

function isCanvasBlank(canvas) {
    var blank = document.createElement('canvas');
    blank.width = canvas.width;
    blank.height = canvas.height;

    blank.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    return canvas.toDataURL() == blank.toDataURL();
}

// end SIGNATURE VALIDATION 



var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear').click(function(e) {
    e.preventDefault();
    sig.signature('clear');
    $("#signature64").val('');
    draw($(this));

    $('.sign_input_validation_owner').val("0");
    if($('.sign_input_validation_owner').val() == "0"){
      $('.sign_error_owner_msg').show();
    }
    else{
      $('.sign_error_owner_msg').hide();
    }



    // $('canvas').attr('width','100');
});


$(document).ready(function(){
if($('.owner_sign').length > 0){
  var canvas = $('.kbw-signature').find('canvas')[0];
  const context = canvas.getContext("2d");

  const img = new Image()
  img.src = $('.owner_sign').val()
  img.onload = () => {
    context.drawImage(img, 0, 0)
  }
}
});

$('#sig canvas').mousedown(function(){
  $(this).parents('#sig').siblings('.sign_input_validation_owner').val("1");
    if($('.sign_input_validation_owner').val() == "0"){
      $('.sign_error_owner_msg').show();
    }
    else{
      $('.sign_error_owner_msg').hide();
    }
})
// var canvas = document.getElementsByTagName('canvas')[0]
// var ctx = canvas.getContext("2d")
function draw(btn) {

  var canvas = btn.siblings('.kbw-signature').find('canvas')[0];
  var ctx = canvas.getContext("2d")
  //This line is actually not even needed...
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.rect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = 'white';
  ctx.fill();
}
$(document).ready(function(){
  // document.getElementsByTagName('canvas').width = 985.72;
  $('canvas').attr('width','985.72');
})








</script>



@endsection
