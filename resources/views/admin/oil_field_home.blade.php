<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />

  <!-- Validator Jquery plugin css -->

  <!-- <link href="{{asset('jquery-validation-1.19.2/demo/css/screen.css')}}" rel="stylesheet"> -->

 <!--  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('assets/img/favicon.png')}}"> -->
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
   Oilfield Management
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
   <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{asset('assets/css/material-dashboard.css?v=2.1.0')}}" rel="stylesheet" />

  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet" />
  <link href="{{asset('drag-drop-image-uploader/dist/image-uploader.min.css')}}" rel="stylesheet">
  <link href="{{asset('year_picker/yearpicker.css')}}" rel="stylesheet">
  <link href="{{asset('css/custom.css')}}" rel="stylesheet" />


<!--   <link rel="stylesheet" href="css/screen.css">
  <script src="../lib/jquery.js"></script> -->


  <body>

      <div class="wrapper wrapper-full-page">
          <div class="page-header pricing-page home_page_header header-filter" style="background: linear-gradient(to bottom, rgb(0 0 0 / 0%)0%, rgb(27 18 18 / 75%) 59%, rgba(0, 0, 0, 0.65) 100%), url(../../assets/img/oil-trucks.jpg) no-repeat;}">
            <div class="container">
              <div class="row">
                <div class="col-md-6 ml-auto mr-auto text-center">
                  <h2 class="title">Oil Field Services & Management</h2>
                  <!-- <h5 class="description">You have Free Unlimited Updates and Premium Support on each package.</h5> -->
                </div>
              </div>
              <div class="row home_row">
                <div class="col-lg-3 col-md-6">
                  <div class="card card-pricing ">
                    <div class="card-body">
                      <div class="card-icon icon-rose ">
                        <i class="material-icons">people_outline</i>
                      </div>
                      <h3 class="card-title">CUSTOMER</h3>
                    </div>
                    <div class="card-footer justify-content-center add_link_wrapper">
                      <a href="{{ route('customers.create') }}" class="btn btn-round btn-rose home_add_manage add_link">ADD </a>
                    </div>
                    <div class="card-footer justify-content-center manage_link_wrapper">
                      <a href="{{ route('customers.index') }}" class="btn btn-round btn-rose home_add_manage manage_link">Manage</a>
                    </div>
                  </div>
                </div>

                <div class="col-lg-3 col-md-6">
                  <div class="card card-pricing ">
                    <div class="card-body">
                      <div class="card-icon icon-rose ">
                        <i class="material-icons">ballot</i>
                      </div>
                      <h3 class="card-title">INVENTORY</h3>
                    </div>
                    <div class="card-footer justify-content-center add_link_wrapper">
                      <a href="{{ route('inventory.create') }}" class="btn btn-round btn-rose home_add_manage add_link">ADD </a>
                    </div>
                    <div class="card-footer justify-content-center manage_link_wrapper">
                      <a href="{{ route('inventory.index') }}" class="btn btn-round btn-rose home_add_manage manage_link">Manage</a>
                    </div>
                  </div>
                </div>

                <div class="col-lg-3 col-md-6">
                  <div class="card card-pricing ">
                    <div class="card-body">
                      <div class="card-icon icon-rose ">
                        <i class="material-icons">store</i>
                      </div>
                      <h3 class="card-title">SERVICES</h3>
                    </div>
                    <div class="card-footer justify-content-center add_link_wrapper">
                      <a href="{{ route('products.create') }}" class="btn btn-round btn-rose home_add_manage add_link">ADD </a>
                    </div>
                    <div class="card-footer justify-content-center manage_link_wrapper">
                      <a href="{{ route('products.index') }}" class="btn btn-round btn-rose home_add_manage manage_link">Manage</a>
                    </div>
                  </div>
                </div>

                <div class="col-lg-3 col-md-6">
                  <div class="card card-pricing ">
                    <div class="card-body">
                      <div class="card-icon icon-rose ">
                        <i class="material-icons">supervised_user_circle</i>
                      </div>
                      <h3 class="card-title">VENDORS</h3>
                    </div>
                    <div class="card-footer justify-content-center add_link_wrapper">
                      <a href="{{ route('vendors.create') }}" class="btn btn-round btn-rose home_add_manage add_link">ADD </a>
                    </div>
                    <div class="card-footer justify-content-center manage_link_wrapper">
                      <a href="{{ route('vendors.index') }}" class="btn btn-round btn-rose home_add_manage manage_link">Manage</a>
                    </div>
                  </div>
                </div>
                
              </div>

              <div class="row home_row">
                <div class="col-md-10 mx-auto">
                  <div class="row">

                    <div class="col-lg-4 col-md-6 single_link_col">
                      <div class="card card-pricing ">
                        <div class="card-body">
                          <div class="card-icon icon-rose ">
                            <i class="material-icons">ballot</i>
                          </div>
                          <h3 class="card-title">INSURANCE</h3>
                        </div>
                        <!-- <div class="card-footer justify-content-center add_link_wrapper">
                          <a href="{{ route('products.create') }}" class="btn btn-round btn-rose home_add_manage add_link">ADD </a>
                        </div> -->
                        <div class="card-footer justify-content-center manage_link_wrapper">
                          <a href="{{ route('insurance.index') }}" class="btn btn-round btn-rose home_add_manage manage_link">Manage</a>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                      <div class="card card-pricing ">
                        <div class="card-body">
                          <div class="card-icon icon-rose ">
                            <i class="material-icons">account_circle</i>
                          </div>
                          <h3 class="card-title">USER</h3>
                        </div>
                        <div class="card-footer justify-content-center add_link_wrapper">
                          <a href="{{ route('users.create') }}" class="btn btn-round btn-rose home_add_manage add_link"> ADD </a>
                        </div>
                        <div class="card-footer justify-content-center manage_link_wrapper">
                          <a href="{{ route('users.index') }}" class="btn btn-round btn-rose home_add_manage manage_link">Manage</a>
                        </div>
                      </div>
                    </div>

                    

                    <div class="col-lg-4 col-md-6 single_link_col">
                      <div class="card card-pricing ">
                        <div class="card-body">
                          <div class="card-icon icon-rose ">
                            <i class="material-icons">monetization_on</i>
                          </div>
                          <h3 class="card-title">QUOTATION</h3>
                        </div>
                        <div class="card-footer justify-content-center add_link_wrapper">
                          <a href="{{ route('quotation.create') }}" class="btn btn-round btn-rose home_add_manage add_link">ADD </a>
                        </div>
                        <!-- <div class="card-footer justify-content-center manage_link_wrapper">
                          <a href="{{ route('vendors.index') }}" class="btn btn-round btn-rose home_add_manage manage_link">Manage</a>
                        </div> -->
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
            <footer class="footer">
              <div class="container">
                <nav class="float-left">
                  
                </nav>
              </div>
            </footer>
          </div>
      </div>






















































  <script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
  <!-- <script src="{{asset('drag-drop-image-uploader/dist/image-uploader.min.js')}}"></script> -->
  <!-- <script src="{{asset('drag-drop-image-uploader/src/image-uploader.js')}}"></script> -->
  <script src="{{asset('drag-drop-image-uploader/src/image-uploader2.js')}}"></script>

  
  <!-- <script src="{{asset('multiple-file-upload-validation/jquery.MultiFile.min.js')}}"></script> -->
  <script src="{{asset('drag-drop-image-uploader/src/file_uploader.js')}}"></script>

  
  <script src="{{asset('year_picker/yearpicker.js')}}"></script>

    <!--  Select 2  -->

  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

  <!-- Editable Select -->
  <script src="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script>
  <link href="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet">

  <!-- Validator Jquery Plugin js-->
  <!-- <script src="{{asset('jquery-validation-1.19.2/dist/jquery.validate.js')}}"></script> -->


<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>


<!-- Signature LInks -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="{{asset('js/signature.js')}}"></script>


<link rel="stylesheet" type="text/css" href="{{asset('css/signature.css')}}">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

</head>
































<!--   Core JS Files   -->

<script src="{{asset('assets/js/core/popper.min.js')}}"></script>
<script src="{{asset('assets/js/core/bootstrap-material-design.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
<!-- Plugin for the momentJs  -->
<script src="{{asset('assets/js/plugins/moment.min.js')}}"></script>
<!--  Plugin for Sweet Alert -->
<script src="{{asset('assets/js/plugins/sweetalert2.js')}}"></script>
<!-- Forms Validations Plugin -->
<script src="{{asset('assets/js/plugins/jquery.validate.min.js')}}"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="{{asset('assets/js/plugins/jquery.bootstrap-wizard.js')}}"></script>
<!--  Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="{{asset('assets/js/plugins/bootstrap-selectpicker.js')}}"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="{{asset('assets/js/plugins/bootstrap-datetimepicker.min.js')}}"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="{{asset('assets/js/plugins/jquery.dataTables.min.js')}}"></script>
<!--  Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="{{asset('assets/js/plugins/bootstrap-tagsinput.js')}}"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="{{asset('assets/js/plugins/jasny-bootstrap.min.js')}}"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="{{asset('assets/js/plugins/fullcalendar.min.js')}}"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="{{asset('assets/js/plugins/jquery-jvectormap.js')}}"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{asset('assets/js/plugins/nouislider.min.js')}}"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="{{asset('assets/js/plugins/arrive.min.js')}}"></script>
<!--  Google Maps Plugin    -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->
<!-- Chartist JS -->
<script src="{{asset('assets/js/plugins/chartist.min.js')}}"></script>
<!--  Notifications Plugin    -->
<script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{asset('assets/js/material-dashboard.js?v=2.1.0')}}" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{asset('assets/demo/demo.js')}}"></script>

<script src="{{asset('assets/js/fontawesome.js')}}"></script>

<!-- <script src="{{asset('assets/js/jquery-input-mask-phone-number.js')}}"></script> -->

<script src="{{asset('js/custom.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.8/handlebars.min.js"></script>



<script>
  $(document).ready(function() {

    $().ready(function() {
      $sidebar = $('.sidebar');

      $sidebar_img_container = $sidebar.find('.sidebar-background');

      $full_page = $('.full-page');

      $sidebar_responsive = $('body > .navbar-collapse');

      window_width = $(window).width();

      fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

      if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
        if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
          $('.fixed-plugin .dropdown').addClass('open');
        }

      }

      $('.fixed-plugin a').click(function(event) {
        // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
        if ($(this).hasClass('switch-trigger')) {
          if (event.stopPropagation) {
            event.stopPropagation();
          } else if (window.event) {
            window.event.cancelBubble = true;
          }
        }
      });

      $('.fixed-plugin .active-color span').click(function() {
        $full_page_background = $('.full-page-background');

        $(this).siblings().removeClass('active');
        $(this).addClass('active');

        var new_color = $(this).data('color');

        if ($sidebar.length != 0) {
          $sidebar.attr('data-color', new_color);
        }

        if ($full_page.length != 0) {
          $full_page.attr('filter-color', new_color);
        }

        if ($sidebar_responsive.length != 0) {
          $sidebar_responsive.attr('data-color', new_color);
        }
      });

      $('.fixed-plugin .background-color .badge').click(function() {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');

        var new_color = $(this).data('background-color');

        if ($sidebar.length != 0) {
          $sidebar.attr('data-background-color', new_color);
        }
      });

      $('.fixed-plugin .img-holder').click(function() {
        $full_page_background = $('.full-page-background');

        $(this).parent('li').siblings().removeClass('active');
        $(this).parent('li').addClass('active');


        var new_image = $(this).find("img").attr('src');

        if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
          $sidebar_img_container.fadeOut('fast', function() {
            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $sidebar_img_container.fadeIn('fast');
          });
        }

        if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
          var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

          $full_page_background.fadeOut('fast', function() {
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
            $full_page_background.fadeIn('fast');
          });
        }

        if ($('.switch-sidebar-image input:checked').length == 0) {
          var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
          var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

          $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
          $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
        }

        if ($sidebar_responsive.length != 0) {
          $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
        }
      });

      $('.switch-sidebar-image input').change(function() {
        $full_page_background = $('.full-page-background');

        $input = $(this);

        if ($input.is(':checked')) {
          if ($sidebar_img_container.length != 0) {
            $sidebar_img_container.fadeIn('fast');
            $sidebar.attr('data-image', '#');
          }

          if ($full_page_background.length != 0) {
            $full_page_background.fadeIn('fast');
            $full_page.attr('data-image', '#');
          }

          background_image = true;
        } else {
          if ($sidebar_img_container.length != 0) {
            $sidebar.removeAttr('data-image');
            $sidebar_img_container.fadeOut('fast');
          }

          if ($full_page_background.length != 0) {
            $full_page.removeAttr('data-image', '#');
            $full_page_background.fadeOut('fast');
          }

          background_image = false;
        }
      });

      $('.switch-sidebar-mini input').change(function() {
        $body = $('body');

        $input = $(this);

        if (md.misc.sidebar_mini_active == true) {
          $('body').removeClass('sidebar-mini');
          md.misc.sidebar_mini_active = false;

          $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

        } else {

          $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

          setTimeout(function() {
            $('body').addClass('sidebar-mini');

            md.misc.sidebar_mini_active = true;
          }, 300);
        }

        // we simulate the window Resize so the charts will get updated in realtime.
        var simulateWindowResize = setInterval(function() {
          window.dispatchEvent(new Event('resize'));
        }, 180);

        // we stop the simulation of Window Resize after the animations are completed
        setTimeout(function() {
          clearInterval(simulateWindowResize);
        }, 1000);

      });
    });
  });
</script>
<script>
  $(document).ready(function() {
    // Javascript method's body can be found in assets/js/demos.js
    md.initDashboardPageCharts();

    md.initVectorMap();

  });
  $(document).ready(function() {
      // initialise Datetimepicker and Sliders
      md.initFormExtendedDatetimepickers();
      if ($('.slider').length != 0) {
        md.initSliders();
      }

    });


  //  Select 2 dw activation
  $(document).ready(function() {
    $('.custom_select2_col select').select2();

  });



</script>



</body>

</html>
