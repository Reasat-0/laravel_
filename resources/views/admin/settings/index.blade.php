@extends('layouts.master')

@section('content')

      <div class="content setting-content">
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                  
                <!-- seeting page -->

                    <div class="card">
                        <div class="card-header">
                          <h4 class="card-title">Site Setting</h4>
                        </div>

                        <div style="margin-top: 15px;">
                                @include('partials.messages')
                        </div>

                        <div class="card-body setting-content-body">
                          <div id="accordion" role="tablist">
                            
                            <div class="card-collapse">
                              <div class="card-header" role="tab" id="headingOne">
                                <h5 class="mb-0">
                                  <a data-toggle="collapse" href="#app_url" aria-expanded="false" aria-controls="app_url" class="collapsed">
                                    App Url
                                    <i class="material-icons">keyboard_arrow_down</i>
                                  </a>
                                </h5>
                              </div>
                              <div id="app_url" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                <div class="card-body">                             
                                    <!-- quibooks form -->
                                    <form method="post" action="{{ route('settings.store') }}" class="form-horizontal">

                                    @csrf
                                    <input type="hidden" name="type" value="appUrl">

                                        <div class="row">
                                          <label class="col-sm-2 col-form-label">URL</label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <input type="text" class="form-control" name="url" value="@if($app_url){{$app_url->url}}@endif">
                                              <span class="bmd-help">Enter App Url</span>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <label class="col-sm-2 col-form-label"></label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <button type="submit" class="btn custom-btn-one">Save</button>
                                            </div>
                                          </div>
                                        </div>
                                    </form>
                                    <!-- quibooks form -->
                                </div>
                              </div>
                            </div>


                            <div class="card-collapse">
                              <div class="card-header" role="tab" id="headingOne">
                                <h5 class="mb-0">
                                  <a data-toggle="collapse" href="#quibooks" aria-expanded="false" aria-controls="quibooks" class="collapsed">
                                    Quibooks Integration
                                    <i class="material-icons">keyboard_arrow_down</i>
                                  </a>
                                </h5>
                              </div>
                              <div id="quibooks" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                <div class="card-body">                             
                                    <!-- quibooks form -->
                                    <form method="post" action="{{ route('settings.store') }}" class="form-horizontal">

                                    @csrf
                                    <input type="hidden" name="type" value="quickbooks">

                                        <div class="row">
                                          <label class="col-sm-2 col-form-label">Client id</label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <input type="text" class="form-control" name="client_id" value="@if($quickbooks){{$quickbooks->client_id}}@endif">
                                              <span class="bmd-help">Enter client id</span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label">Client secret</label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <input type="text" class="form-control" name="client_secret" value="@if($quickbooks){{$quickbooks->client_secret}}@endif">
                                              <span class="bmd-help">Enter client secret</span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label">Oauth redirect uri</label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <input type="text" class="form-control" name="oauth_redirect_uri" value="@if($quickbooks){{$quickbooks->oauth_redirect_uri}}@endif">
                                              <span class="bmd-help">Enter oauth redirect uri</span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label">Environment</label>
                                          <div class="col-sm-10 environment-section">
                                              <div class="togglebutton admin-role-toggle">
                                                <label>
                                                    <input type="checkbox" name="environment" @if($quickbooks) @if($quickbooks->environment == 'Production') checked @endif @endif>
                                                    Development <span class="toggle"></span> Production
                                                </label>
                                              </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label"></label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <button type="submit" class="btn custom-btn-one">Save</button>
                                            </div>
                                          </div>
                                        </div>
                                    </form>
                                    <!-- quibooks form -->
                                </div>
                              </div>
                            </div>
                            <div class="card-collapse">
                              <div class="card-header" role="tab" id="headingTwo">
                                <h5 class="mb-0">
                                  <a class="collapsed" data-toggle="collapse" href="#invoice_integration" aria-expanded="false" aria-controls="invoice_integration">
                                    OpenInvoice Integration
                                    <i class="material-icons">keyboard_arrow_down</i>
                                  </a>
                                </h5>
                              </div>
                              <div id="invoice_integration" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <!-- Invoice form -->
                                    <form method="post" action="{{ route('settings.store') }}" class="form-horizontal">
                                    @csrf
                                    <input type="hidden" name="type" value="openinvoice">
                                    
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label">Public Key</label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <input type="text" class="form-control" name="public_key" value="@if($openinvoice){{$openinvoice->public_key}}@endif">
                                              <span class="bmd-help">Enter site public key</span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label">Secret key</label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <input type="text" class="form-control" name="secret_key" value="@if($openinvoice){{$openinvoice->secret_key}}@endif">
                                              <span class="bmd-help">Enter site secret key</span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label"></label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <button type="submit" class="btn custom-btn-one">Save</button>
                                            </div>
                                          </div>
                                        </div>
                                    </form>
                                    <!-- Invoice form -->
                                </div>
                              </div>
                            </div>
                            <div class="card-collapse">
                              <div class="card-header" role="tab" id="headingThree">
                                <h5 class="mb-0">
                                  <a class="collapsed" data-toggle="collapse" href="#smtp_integration" aria-expanded="false" aria-controls="smtp_integration">
                                    SMTP Integration
                                    <i class="material-icons">keyboard_arrow_down</i>
                                  </a>
                                </h5>
                              </div>
                              <div id="smtp_integration" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                  <!-- SMTP form -->
                                    <form method="post" action="{{ route('settings.store') }}" class="form-horizontal">
                                    @csrf
                                    <input type="hidden" name="type" value="smtp">
                                    
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label">From Email</label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <input type="text" class="form-control" name="from_email" value="@if($email_config){{$email_config->from_email}}@endif">
                                              <span class="bmd-help">Enter your site email</span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label">From Name</label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <input type="text" class="form-control" name="from_name" value="@if($email_config){{$email_config->from_name}}@endif">
                                              <span class="bmd-help">Enter your site name</span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label label-checkbox">Mailer</label>
                                          <div class="col-sm-10 checkbox-radios">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="mailer" value="gmail" @if($email_config) @if($email_config->mailer == 'gmail') checked @endif @endif> Gmail
                                                <span class="circle">
                                                <span class="check"></span>
                                                </span>
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="mailer" value="mailgun" @if($email_config) @if($email_config->mailer == 'mailgun') checked @endif @endif> Mailgun
                                                <span class="circle">
                                                <span class="check"></span>
                                                </span>
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="mailer" value="sendgrid" @if($email_config) @if($email_config->mailer == 'sendgrid') checked @endif @endif> Sendgrid
                                                <span class="circle">
                                                <span class="check"></span>
                                                </span>
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="mailer" value="other" @if($email_config) @if($email_config->mailer == 'other') checked @endif @endif> Others
                                                <span class="circle">
                                                <span class="check"></span>
                                                </span>
                                                </label>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label">SMTP Host</label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <input type="text" class="form-control" name="smtp_host" value="@if($email_config){{$email_config->smtp_host}}@endif">
                                              <span class="bmd-help">Enter your smtp host name</span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row align-items-baseline">
                                          <label class="col-sm-2 col-form-label">Auto TLS</label>
                                          <div class="col-sm-10 auto-tls-section">
                                            <div class="togglebutton">
                                                <label>
                                                    <input type="checkbox" name="auto_tls" @if($email_config) @if($email_config->auto_tls == 'ssl') checked @endif @endif><span class="toggle"></span>
                                                </label>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label">Port No</label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <input type="text" class="form-control" name="port_no" value="@if($email_config){{$email_config->port_no}}@endif">
                                              <span class="bmd-help">Enter your PORT no</span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row align-items-baseline">
                                          <label class="col-sm-2 col-form-label">Authentication</label>
                                          <div class="col-sm-10 authentication-section">
                                            <div class="togglebutton">
                                                <label>
                                                    <input type="checkbox" name="authentication" @if($email_config) @if($email_config->authentication == 'on') checked @endif @endif> Off <span class="toggle"></span> On
                                                </label>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label">SMTP User</label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <input type="text" class="form-control" name="smtp_user" value="@if($email_config){{$email_config->smtp_user}}@endif">
                                              <span class="bmd-help">Enter your smtp user name</span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label">Password</label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <input type="password" class="form-control" name="smtp_password" value="@if($email_config){{$email_config->smtp_password}}@endif">
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <label class="col-sm-2 col-form-label"></label>
                                          <div class="col-sm-10">
                                            <div class="form-group">
                                              <button type="submit" class="btn custom-btn-one">Save</button>
                                            </div>
                                          </div>
                                        </div>
                                    </form>
                                    <!-- SMTP form -->
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>

                <!-- setting page -->

              </div>
            </div>
        </div>
      </div>
    </div>


@endsection