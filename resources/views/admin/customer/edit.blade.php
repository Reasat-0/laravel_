@extends('layouts.master')

@section('content')

          <div class="content edit_customer_content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12 mx-auto">
                  <div class="card">
                    <div class="card-header card-header-tabs card-header-rose">
                      <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                          
                          <ul class="nav nav-tabs" data-tabs="tabs">
                            <li class="nav-item">
                              <a class="nav-link active" href="#profile" data-toggle="tab">
                                <i class="material-icons">ballot</i> Edit Customer
                                <div class="ripple-container"></div>
                              </a>
                            </li>
                            
                          </ul>
                        </div>
                      </div>
                    </div>

                    <div style="margin-top: 15px;">
                        @include('partials.messages')
                    </div>

                    <div class="card-body">

                      <form id="add_user_form" class="customer_form" action="{{ route('customers.update', $customer->id) }}" method="post">

                        @csrf
                        @method('PUT')

                        <div class="card card_modified">
                          <div class="card-header card-header-rose card-header-text sub_card">
                            <div class="card-text card_text_title">
                              <h4 class="card-title">Basic</h4>
                            </div>
                          </div>
                          <div class="card-body ">
                            <div class="row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="company_name" class="bmd-label-floating input_label">Company Name</label>
                                  <input type="text" class="form-control form_input_field" id="company_name" name="companyName" value="{{$customer->companyName}}">
                                </div>
                              </div>
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="display_name" class="bmd-label-floating input_label ">Display Name</label>
                                  <input type="text" class="form-control form_input_field" id="display_name" name="displayName" value="{{$customer->displayName}}" required="true" aria-required="true">
                                </div>
                              </div>
                            </div>
                            
                            <div class="row input_row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="title" class="bmd-label-floating input_label">Title</label>
                                  <input type="text" class="form-control form_input_field" id="title" name="title" value="{{$customer->title}}">
                                </div>
                              </div>
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="given_name" class="bmd-label-floating input_label">Given Name *</label>
                                  <input type="text" class="form-control form_input_field" id="given_name" name="givenName" value="{{$customer->givenName}}">
                                </div>
                              </div>
                            </div>
                            <div class="row input_row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="middle_name" class="bmd-label-floating input_label">Middle Name</label>
                                  <input type="text" class="form-control form_input_field" id="middle_name" name="middleName" value="{{$customer->middleName}}">
                                </div>
                              </div>
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="family_name" class="bmd-label-floating input_label">Family Name</label>
                                  <input type="text" class="form-control form_input_field" id="family_name" name="familyName" value="{{$customer->familyName}}">
                                </div>
                              </div>
                            </div>
                            <div class="row input_row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="suffix" class="bmd-label-floating input_label">Suffix</label>
                                  <input type="text" class="form-control form_input_field" id="suffix" name="suffix" value="{{$customer->suffix}}">
                                </div>
                              </div>
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="suffix" class="bmd-label-floating input_label">Account number</label>
                                  <input type="text" class="form-control form_input_field" id="account_no" name="account_no" value="{{preg_replace('/[^A-Za-z0-9\-]/', '', strtok($customer->displayName, " "))}}-EVOS-{{$customer->id}}" readonly="">
                                </div>
                              </div>                              
                            </div>
                          </div>
                        </div>



                        <div class="card">
                          <div class="card-header card-header-rose card-header-text sub_card">
                            <div class="card-text card_text_title">
                              <h4 class="card-title">Phone & Email</h4>
                            </div>
                          </div>
                          <div class="card-body ">
                            <div class="row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="primaryPhone_freeFormNumber" class="bmd-label-floating input_label">Phone</label>
                                  <input type="text" class="form-control form_input_field mobile" id="primaryPhone_freeFormNumber" name="primaryPhone_freeFormNumber" value="{{$customer->primaryPhone_freeFormNumber}}">
                                </div>
                              </div>
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="primaryEmailAddr_addess" class="bmd-label-floating input_label">Email Address</label>
                                  <input type="email" class="form-control form_input_field" id="primaryEmailAddr_addess" name="primaryEmailAddr_addess" value="{{$customer->primaryEmailAddr_addess}}">
                                </div>
                              </div>
                            </div>

                            <div class="row input_row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="mobile" class="bmd-label-floating input_label">Mobile</label>
                                  <input type="number" class="form-control form_input_field mobile" id="mobile" name="mobile" value="{{$customer->mobile}}">
                                </div>
                              </div>
                              <!-- <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="fax" class="bmd-label-floating input_label">Fax</label>
                                  <input type="text" class="form-control form_input_field mobile" id="fax" name="fax" value="{{$customer->fax}}">
                                </div>
                              </div> -->
                            <!-- </div>
                            <div class="row input_row"> -->
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="alternatePhone" class="bmd-label-floating input_label">Other no</label>
                                  <input type="text" class="form-control form_input_field mobile" id="alternatePhone" name="alternatePhone" value="{{$customer->alternatePhone}}">
                                </div>
                              </div>
                            </div>

                          </div>

                        </div>


                        <div class="card">
                            <div class="card-header card-header-rose card-header-text sub_card">
                              <div class="card-text card_text_title">
                                <h4 class="card-title">Billing Address</h4>
                              </div>
                            </div>

                            <div class="card-body ">
                              <div class="row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="billAddr_line1" class="bmd-label-floating input_label">Line 1</label>
                                    <input type="text" class="form-control form_input_field" id="billAddr_line1" name="billAddr_line1" value="{{$customer->billAddr_line1}}">
                                  </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="billAddr_line2" class="bmd-label-floating input_label">Line 2</label>
                                    <input type="text" class="form-control form_input_field" id="billAddr_line2" name="billAddr_line2" value="{{$customer->billAddr_line2}}">
                                  </div>
                                </div>
                              </div>
                              
                              <div class="row input_row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="billAddr_city" class="bmd-label-floating input_label">City</label>
                                    <input type="text" class="form-control form_input_field" id="billAddr_city" name="billAddr_city" value="{{$customer->billAddr_city}}">
                                  </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="billAddr_country" class="bmd-label-floating input_label">Country</label>
                                    <input type="text" class="form-control form_input_field" id="billAddr_country" name="billAddr_country" value="{{$customer->billAddr_country}}">
                                  </div>
                                </div>
                              </div>
                              <div class="row input_row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="billAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Country SubDivision Code</label>
                                    <input type="text" class="form-control form_input_field" id="billAddr_countrySubDivisionCode" name="billAddr_countrySubDivisionCode" value="{{$customer->billAddr_countrySubDivisionCodeo}}">
                                  </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="billAddr_postalCode" class="bmd-label-floating input_label">Postal Code</label>
                                    <input type="text" class="form-control form_input_field" id="billAddr_postalCode" name="billAddr_postalCode" value="{{$customer->billAddr_postalCode}}">
                                  </div>
                                </div>
                              </div>

                              <div class="row input_row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Billing Phone</label>
                                        <input type="text" class="form-control form_input_field mobile" id="billing_phone" name="billing_phone" value="{{$customer->billing_phone}}">
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_postalCode" class="bmd-label-floating input_label">Billing Email</label>
                                        <input type="email" class="form-control form_input_field" id="billing_email" name="billing_email" value="{{$customer->billing_email}}">
                                      </div>
                                    </div>
                              </div>
                              
                              <div class="row input_row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group">
                                    <label for="billAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Billing Contact Name</label>
                                    <input type="text" class="form-control form_input_field" id="billing_contact_name" name="billing_contact_name" value="{{$customer->billing_contact_name}}">
                                  </div>
                                </div>
                              </div>
                            </div>

                        </div>


                        <div class="card">
                          <div class="card-header card-header-rose card-header-text sub_card">
                            <div class="card-text card_text_title">
                              <h4 class="card-title">Service Location Address</h4>
                            </div>
                          </div>

                          <div class="card-body service_address_card_body">
                            
                            <div class="card single_address_card top_single_address_card">
                              <div class="card-header card-header-rose card-header-icon service_address_header">
                                <div class="timeline-heading">
                                  <h4 class="badge badge-pill badge-info">Default</h4>
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="hidden" name="addr_key[]" value="0">
                                      <input type="hidden" class="is_default" name="is_default[]" value="1">
                                      <input class="form-check-input" type="radio" name="service_default" value="1" checked=""> Set as default
                                      <span class="circle">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>
                                  
                                </div>
                                
                                <button class="btn btn-danger btn-sm btn-round address_remove_btn" disabled>
                                  <i class="material-icons">close</i>
                                  <div class="ripple-container"></div>
                                </button>
                                

                              </div>
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_line1" class="bmd-label-floating input_label">Line 1</label>
                                      <input type="text" class="form-control form_input_field" id="shipAddr_line1" name="shipAddr_line1[]" value="{{$customer->shipAddr_line1}}">
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_line2" class="bmd-label-floating input_label">Line 2</label>
                                      <input type="text" class="form-control form_input_field" id="shipAddr_line2" name="shipAddr_line2[]" value="{{$customer->shipAddr_line2}}">
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="row input_row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_city" class="bmd-label-floating input_label">City</label>
                                      <input type="text" class="form-control form_input_field" id="shipAddr_city" name="shipAddr_city[]" value="{{$customer->shipAddr_city}}">
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_country" class="bmd-label-floating input_label">Country</label>
                                      <input type="text" class="form-control form_input_field" id="shipAddr_country" name="shipAddr_country[]" value="{{$customer->shipAddr_country}}">
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="row input_row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Country SubDivision Code</label>
                                      <input type="text" class="form-control form_input_field" id="shipAddr_countrySubDivisionCode" name="shipAddr_countrySubDivisionCode[]" value="{{$customer->shipAddr_countrySubDivisionCode}}">
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_postalCode" class="bmd-label-floating input_label">Postal Code</label>
                                      <input type="text" class="form-control form_input_field" id="shipAddr_postalCode" name="shipAddr_postalCode[]" value="{{$customer->shipAddr_postalCode}}">
                                    </div>
                                  </div>
                                </div>

                                      <div class="row input_row">
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Lease Name</label>
                                            <input type="text" class="form-control form_input_field" id="lease_name" name="lease_name[]" value="{{$customer->lease_name}}">
                                          </div>
                                        </div>
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_postalCode" class="bmd-label-floating input_label">Rig Number</label>
                                            <input type="text" class="form-control form_input_field" id="rig_number" name="rig_number[]" value="{{$customer->rig_number}}">
                                          </div>
                                        </div>
                                      </div>

                                      <div class="row input_row">
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_postalCode" class="bmd-label-floating input_label">AFE Number</label>
                                            <input type="text" class="form-control form_input_field" id="afe_number" name="afe_number[]" value="{{$customer->afe_number}}">
                                          </div>
                                        </div>
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Site Contact Phone</label>
                                            <input type="text" class="form-control form_input_field mobile" id="site_contact_phone" name="site_contact_phone[]" value="{{$customer->site_contact_phone}}">
                                          </div>
                                        </div>                                        
                                      </div>



                                      <div class="row input_row">

                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_postalCode" class="bmd-label-floating input_label">Rig/Contact Email</label>
                                            <input type="email" class="form-control form_input_field" id="rig_contact_email" name="rig_contact_email[]" value="{{$customer->rig_contact_email}}">
                                          </div>
                                        </div>
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_postalCode" class="bmd-label-floating input_label">Company Man Contact Name</label>
                                            <input type="text" class="form-control form_input_field" id="company_man_contact_name" name="company_man_contact_name[]" value="{{$customer->company_man_contact_name}}">
                                          </div>
                                        </div>
                                      </div>

                                      <fieldset class="custom_fieldset_2">
                                        <legend>GPS location</legend>
                                        <div class="row input_row dd_sec">
                                          <div class="col-md-6 input_wrapper">
                                            <div class="form-group bmd-form-group">
                                              <label for="dd_latitude" class="bmd-label-floating input_label">Latitude</label>
                                              <input type="number" class="form-control form_input_field dd_latitude" name="latitude[]" value="{{$customer->latitude}}">
                                            </div>
                                          </div>
                                          <div class="col-md-6 input_wrapper">
                                            <div class="form-group bmd-form-group">
                                              <label for="dd_longitude" class="bmd-label-floating input_label">Longitude</label>
                                              <input type="number" class="form-control form_input_field dd_longitude" name="longitude[]" value="{{$customer->longitude}}">
                                            </div>
                                          </div>
                                        </div>
                                      </fieldset>

                              </div>
                            </div>
                            

                            @foreach($customer_service_location_address as $rowdata)
                            <div class="card single_address_card top_single_address_card">
                              <div class="card-header card-header-rose card-header-icon service_address_header">
                                <div class="timeline-heading">
                                  <h4 class="badge badge-pill badge-info service_remove">Default</h4>
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input type="hidden" name="addr_key[]" value="{{$rowdata->id}}">
                                      <input type="hidden" class="is_default" name="is_default[]" value="0">
                                      <input class="form-check-input radio-input" type="radio" name="service_default" value="1"> Set as default
                                      <span class="circle">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>
                                  
                                </div>
                                
                                <button class="btn btn-danger btn-sm btn-round address_remove_btn">
                                  <i class="material-icons">close</i>
                                  <div class="ripple-container"></div>
                                </button>
                                

                              </div>
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_line1" class="bmd-label-floating input_label">Line 1</label>
                                      <input type="text" class="form-control form_input_field" id="shipAddr_line1" name="shipAddr_line1[]" value="{{$rowdata->shipAddr_line1}}">
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_line2" class="bmd-label-floating input_label">Line 2</label>
                                      <input type="text" class="form-control form_input_field" id="shipAddr_line2" name="shipAddr_line2[]" value="{{$rowdata->shipAddr_line2}}">
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="row input_row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_city" class="bmd-label-floating input_label">City</label>
                                      <input type="text" class="form-control form_input_field" id="shipAddr_city" name="shipAddr_city[]" value="{{$rowdata->shipAddr_city}}">
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_country" class="bmd-label-floating input_label">Country</label>
                                      <input type="text" class="form-control form_input_field" id="shipAddr_country" name="shipAddr_country[]" value="{{$rowdata->shipAddr_country}}">
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="row input_row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Country SubDivision Code</label>
                                      <input type="text" class="form-control form_input_field" id="shipAddr_countrySubDivisionCode" name="shipAddr_countrySubDivisionCode[]" value="{{$rowdata->shipAddr_countrySubDivisionCode}}">
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_postalCode" class="bmd-label-floating input_label">Postal Code</label>
                                      <input type="text" class="form-control form_input_field" id="shipAddr_postalCode" name="shipAddr_postalCode[]" value="{{$rowdata->shipAddr_postalCode}}">
                                    </div>
                                  </div>
                                </div>

                                <div class="row input_row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Lease Name</label>
                                      <input type="text" class="form-control form_input_field" id="lease_name" name="lease_name[]" value="{{$rowdata->lease_name}}">
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_postalCode" class="bmd-label-floating input_label">Rig Number</label>
                                      <input type="text" class="form-control form_input_field" id="rig_number" name="rig_number[]" value="{{$rowdata->rig_number}}">
                                    </div>
                                  </div>
                                </div>


                                <div class="row input_row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_postalCode" class="bmd-label-floating input_label">AFE Number</label>
                                      <input type="text" class="form-control form_input_field" id="afe_number" name="afe_number[]" value="{{$rowdata->afe_number}}">
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Site Contact Phone</label>
                                      <input type="text" class="form-control form_input_field" id="site_contact_phone" name="site_contact_phone[]" value="{{$rowdata->site_contact_phone}}">
                                    </div>
                                  </div>
                                </div>



                                <div class="row input_row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_postalCode" class="bmd-label-floating input_label">Rig/Contact Email</label>
                                      <input type="email" class="form-control form_input_field" id="rig_contact_email" name="rig_contact_email[]" value="{{$rowdata->rig_contact_email}}">
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="shipAddr_postalCode" class="bmd-label-floating input_label">Company Man Contact Name</label>
                                      <input type="text" class="form-control form_input_field" id="company_man_contact_name" name="company_man_contact_name[]" value="{{$rowdata->company_man_contact_name}}">
                                    </div>
                                  </div>
                                </div>


                                <fieldset class="custom_fieldset_2">
                                        <legend>GPS location</legend>
                                        <div class="row input_row dd_sec">
                                          <div class="col-md-6 input_wrapper">
                                            <div class="form-group bmd-form-group">
                                              <label for="dd_latitude" class="bmd-label-floating input_label">Latitude</label>
                                              <input type="number" class="form-control form_input_field dd_latitude" name="latitude[]" value="{{$rowdata->latitude}}">
                                            </div>
                                          </div>
                                          <div class="col-md-6 input_wrapper">
                                            <div class="form-group bmd-form-group">
                                              <label for="dd_longitude" class="bmd-label-floating input_label">Longitude</label>
                                              <input type="number" class="form-control form_input_field dd_longitude" name="longitude[]" value="{{$rowdata->longitude}}">
                                            </div>
                                          </div>
                                        </div>
                                      </fieldset>

                              </div>
                            </div>
                            @endforeach

                            <div class="row add_address_btn_wrapper">
                              <div class="col-md-12 text-right">
                                <button class="btn btn-info btn-round btn-sm add_address_btn" type="button">
                                  <span class="btn-label">
                                    <i class="material-icons">add</i>
                                  </span>
                                  Add More
                                  <div class="ripple-container"></div>
                                </button>
                              </div>
                            </div>
                          </div>

                        </div>



                        <div class="card">
                            <div class="card-header card-header-rose card-header-text sub_card">
                              <div class="card-text card_text_title">
                                <h4 class="card-title">Others</h4>
                              </div>
                            </div>

                            <div class="card-body ">
                              
                              <div class="row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="webAddr" class="bmd-label-floating input_label">Web Address</label>
                                    <input type="url" class="form-control form_input_field" id="webAddr" name="webAddr" value="{{$customer->webAddr}}">
                                  </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="notes" class="bmd-label-floating input_label">Notes</label>
                                    <input type="text" class="form-control form_input_field" id="notes" name="notes" value="{{$customer->notes}}">
                                  </div>
                                </div>
                              </div>

                            </div>

                        </div>


                        <div class="card quickbooks_update_checking_card">
                              <div class="card-header card-header-rose card-header-text sub_card">
                                <div class="card-text card_text_title">
                                  <h4 class="card-title">Quickbooks</h4>
                                </div>
                              </div>

                              <!-- <div class="card-body">
                                
                                <div class="row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-check">
                                      <label class="form-check-label">
                                        <input class="form-check-input singleCustomerQB" type="checkbox" name="singleCustomerQB" value="1" > Check here If you want to update data to Quickbooks as well
                                        <span class="form-check-sign">
                                          <span class="check"></span>
                                        </span>
                                      </label>
                                    </div>
                                  </div>
                                </div>

                              </div> -->
                        </div>


                        <div class="card quickbooks_update_checking_card">
                          <div class="card-header card-header-rose card-header-text sub_card">
                            <div class="card-text card_text_title">
                              <h4 class="card-title">Credit Application</h4>
                            </div>
                          </div>

                          <div class="card-body">
                            
                            <div class="row">
                              <div class="col-md-12 input_wrapper">
                                <div class="form-check">
                                  <label class="form-check-label">
                                    <input class="form-check-input credit_app_creation" type="checkbox" name="credit_app_creation" value="1" @if($customer->credit_app_creation == 1) disabled @endif> Check here if you want to generate Credit Application during customer creation
                                    <span class="form-check-sign">
                                      <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                              </div>
                            </div>

                            <div class="row mt-2">
                              @if($customer->credit_app_creation == 1)
                              <div class="col-md-9 input_wrapper credit_app_list_wrapper">
                                <div class="table-responsive">
                                  <table class="table">
                                    <thead>
                                      <tr>
                                        <th> ID </th>
                                        <th> Credit Limit  </th>
                                        
                                        <th> Date </th>

                                        <th class="pdf_file_td"> Pdf </th>
                                        
                                        <th class="text-right"> Action </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td> <span class="edit_ca_badge">CR-{{$customer->id}}</span> <!-- CR-{{$customer->id}} --></td>
                                        @if($credit_app)
                                          @if($credit_app->status == 3)
                                            @if($credit_app->ca_credit_limit != null)
                                              <td> $ {{$credit_app->ca_credit_limit}} </td>
                                            @else
                                              <td>
                                                <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                              </td>
                                            @endif
                                          @else
                                            <td>
                                                <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                              </td>
                                          @endif
                                        @else
                                          <td>
                                            <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                          </td>
                                        @endif


                                        @if($credit_app)
                                          @if($credit_app->status == 3)
                                            @if($credit_app->ca_credit_limit != null)
                                              <td> {{$credit_app->customer_send_mail_date}} </td>
                                            @else
                                              <td>
                                                <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                              </td>
                                            @endif
                                          @else
                                            <td>
                                                <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                              </td>
                                          @endif
                                        @else
                                          <td>
                                            <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                          </td>
                                        @endif


                                        @if($credit_app)
                                          @if($credit_app->status == 3)
                                          <td class="pdf_file_td">
                                            <a target="_blank" class="btn btn-link btn-twitter" href="{{ route('creditAppPdf', $customer->id) }}"> Credit Application PDF</a>
                                          </td>
                                          @else
                                          <td class="pdf_file_td">
                                            <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                          </td>
                                          @endif
                                        @else
                                          <td class="pdf_file_td">
                                            <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                          </td>
                                        @endif



                                        <td class="text-right">
                                          @if($credit_app)
                                          @if($credit_app->status == 3)
                                          <a href="{{ route('creditAppEdit',$customer->id) }}" class="btn btn-link btn-info btn-just-icon view"><i class="material-icons">edit</i></a>
                                          @endif
                                          @endif

                                          @if($credit_app)
                                            @if($credit_app->status != 3)
                                            <a href="{{ route('creditAppEdit',$customer->id) }}" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a>
                                            @endif
                                          @else
                                            <a href="{{ route('creditAppEdit',$customer->id) }}" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a>
                                          @endif

                                          <a href="{{ route('creditAppDelete',$customer->id) }}" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons" onclick="return confirm('Are you sure you want to delete the credit application for this customer?');">close</i></a>
                                          
                                        </td>
                                      </tr>

                                    @if(!$credit_app_history == [])
                                      @foreach($credit_app_history as $rowdata)
                                        <tr>
                                        <td> <span class="edit_ca_badge">CR-{{$customer->id}}</span> <!-- CR-{{$customer->id}} --></td>
                                        <td> $ {{$rowdata->credit_limit}} </td>

                                        <td> {{$rowdata->date}} </td>
                                        
                                        @if($credit_app)
                                        @if($credit_app->status == 3)
                                        <td class="pdf_file_td">
                                          <a target="_blank" class="btn btn-link btn-twitter" href="{{ route('creditAppPdf', [$customer->id, 'cr_limit'=>$rowdata->id]) }}"> Credit Application PDF</a>
                                        </td>

                                        @endif
                                        @endif

                                        

                                        <td class="text-right">
                                          @if($credit_app)
                                          @if($credit_app->status == 3)
                                          <a href="{{ route('creditAppEdit',[$customer->id, 'cr_limit'=>$rowdata->id]) }}" class="btn btn-link btn-info btn-just-icon view"><i class="material-icons">edit</i></a>
                                          @endif
                                          @endif

                                          @if($credit_app)
                                            @if($credit_app->status != 3)
                                            <a href="{{ route('creditAppEdit',$customer->id) }}" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a>
                                            @endif
                                          @else
                                            <a href="{{ route('creditAppEdit',[$customer->id, 'cr_limit'=>$rowdata->id]) }}" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a>
                                          @endif

                                          <!-- <a href="{{ route('creditAppDelete',$customer->id) }}" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons" onclick="return confirm('Are you sure you want to delete this credit limit for this credit application?');">close</i></a> -->
                                          
                                        </td>
                                      </tr>
                                      @endforeach
                                    @endif
                                    </tbody>
                                  </table>
                                </div>
                              </div>  
                              @endif                              
                            </div>
                          

                          </div>
                        </div>

                        <div class="form-group bmd-form-group is-filled save_btn_wrapper text-right">
                          <button type="submit" class="btn custom-btn-one save_btn">Update</button>
                        </div>
                      </form>

                       
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

<script>
  $('.singleCustomerQB').change(function() {
          if($(this).is(":checked")) {
              $.ajax({
                        url:"{{ route('singleCustomerQB') }}",
                        method:"GET",
                        cache: false,
                        success:function(result)
                        {
                         if(result == 'connect'){
                            alert('Please click on connect to quickbooks button from to get access token');
                            $('.singleCustomerQB').prop("checked", false);
                         }
                         else if(result == 'expire'){
                          alert('Your access token is expired, Please click on Connect to Quickbooks button');
                          $('.singleCustomerQB').prop("checked", false);
                         }
                        
                        }
                     })
          }       
      });

$(document).on('change', '.radio-input', function (e) {
    $('.is_default').val('0');
    $(this).parent().find('.is_default').val('1');
    
});

</script>


@endsection