@extends('layouts.master')

@section('content')

        <div class="content">
          <div class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-10 mx-auto">
                  
                  
                  <div class="card">
                  <div class="card-header card-header-tabs card-header-rose">
                    <div class="nav-tabs-navigation">
                      <div class="nav-tabs-wrapper">
                        
                        <ul class="nav nav-tabs" data-tabs="tabs">
                          <li class="nav-item">
                            <a class="nav-link active" href="#profile" data-toggle="tab">
                              <i class="material-icons">ballot</i> View Customer
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                          
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div style="margin-top: 15px;">
                      @include('partials.messages')
                  </div>

                  <div class="card-body">

                    <form id="add_user_form" class="" action="{{ route('customers.update', $customer->id) }}" method="post">

                      @csrf
                      @method('PUT')

                      <div class="card">
                        <div class="card-header card-header-rose card-header-text sub_card">
                          <div class="card-text card_text_title">
                            <h6 class="card-title">Basic</h6>
                          </div>
                        </div>
                        <div class="card-body ">

                            <div class="row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="company_name" class="bmd-label-floating">Company Name</label>
                                  <input type="text" class="form-control" id="company_name" name="companyName" value="{{$customer->companyName}}">
                                </div>
                              </div>
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="display_name" class="bmd-label-floating">Display Name</label>
                                  <input type="text" class="form-control" id="display_name" name="displayName" value="{{$customer->displayName}}">
                                </div>
                              </div>

                            </div>
                          
                            <div class="row input_row">
                              <div class="col-md-2 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="title" class="bmd-label-floating">Title</label>
                                  <input type="text" class="form-control" id="title" name="title" value="{{$customer->title}}">
                                </div>
                              </div>
                              <div class="col-md-3 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="given_name" class="bmd-label-floating">Given Name *</label>
                                  <input type="text" class="form-control" id="given_name" name="givenName" required="true" aria-required="true" value="{{$customer->givenName}}">
                                </div>
                              </div>
                              <div class="col-md-3 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="middle_name" class="bmd-label-floating">Middle Name</label>
                                  <input type="text" class="form-control" id="middle_name" name="middleName" value="{{$customer->middleName}}">
                                </div>
                              </div>
                              <div class="col-md-3 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="family_name" class="bmd-label-floating">Family Name</label>
                                  <input type="text" class="form-control" id="family_name" name="familyName" value="{{$customer->familyName}}">
                                </div>
                              </div>
                              <div class="col-md-1 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="suffix" class="bmd-label-floating">Suffix</label>
                                  <input type="text" class="form-control" id="suffix" name="suffix" value="{{$customer->suffix}}">
                                </div>
                              </div>
                            </div>

                        </div>
                      </div>



                      <div class="card">
                        <div class="card-header card-header-rose card-header-text sub_card">
                          <div class="card-text card_text_title">
                            <h6 class="card-title">Phone & Email</h6>
                          </div>
                        </div>
                        <div class="card-body ">
                          
                            <div class="row input_row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="primaryPhone_freeFormNumber" class="bmd-label-floating">Phone</label>
                                    <input type="text" class="form-control" id="primaryPhone_freeFormNumber" name="primaryPhone_freeFormNumber" value="{{$customer->primaryPhone_freeFormNumber}}">
                                  </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="primaryEmailAddr_addess" class="bmd-label-floating">Email Address</label>
                                  <input type="text" class="form-control" id="primaryEmailAddr_addess" name="primaryEmailAddr_addess" value="{{$customer->primaryEmailAddr_addess}}">
                                </div>
                              </div>
                             </div>

                             <div class="row input_row">
                              
                              <div class="col-md-4 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="mobile" class="bmd-label-floating">Mobile</label>
                                    <input type="text" class="form-control" id="mobile" name="mobile" value="{{$customer->mobile}}">
                                  </div>
                                </div>
                                <!-- <div class="col-md-4 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="fax" class="bmd-label-floating">Fax</label>
                                    <input type="text" class="form-control" id="fax" name="fax" value="{{$customer->fax}}">
                                  </div>
                                </div> -->
                                <div class="col-md-4 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="alternatePhone" class="bmd-label-floating">Other no</label>
                                    <input type="text" class="form-control" id="alternatePhone" name="alternatePhone" value="{{$customer->alternatePhone}}">
                                  </div>
                                </div>
                            </div>

                          </div>

                      </div>


                      <div class="card">
                            <div class="card-header card-header-rose card-header-text sub_card">
                              <div class="card-text card_text_title">
                                <h6 class="card-title">Billing Address</h6>
                              </div>
                            </div>

                          <div class="card-body ">
                            <div class="row input_row">
                              <div class="col-md-4 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="billAddr_line1" class="bmd-label-floating">Line 1</label>
                                  <input type="text" class="form-control" id="billAddr_line1" name="billAddr_line1" value="{{$customer->billAddr_line1}}">
                                </div>
                              </div>
                              <div class="col-md-4 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="billAddr_line2" class="bmd-label-floating">Line 2</label>
                                  <input type="text" class="form-control" id="billAddr_line2" name="billAddr_line2" value="{{$customer->billAddr_line2}}">
                                </div>
                              </div>
                              <div class="col-md-4 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="billAddr_city" class="bmd-label-floating">City</label>
                                  <input type="text" class="form-control" id="billAddr_city" name="billAddr_city" value="{{$customer->billAddr_city}}">
                                </div>
                              </div>
                            </div>
                            
                            <div class="row input_row">
                              <div class="col-md-4 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="billAddr_country" class="bmd-label-floating">Country</label>
                                  <input type="text" class="form-control" id="billAddr_country" name="billAddr_country" value="{{$customer->billAddr_country}}">
                                </div>
                              </div>
                              <div class="col-md-4 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="billAddr_countrySubDivisionCode" class="bmd-label-floating">Country SubDivision Code</label>
                                  <input type="text" class="form-control" id="billAddr_countrySubDivisionCode" name="billAddr_countrySubDivisionCode" value="{{$customer->billAddr_countrySubDivisionCodeo}}">
                                </div>
                              </div>
                              <div class="col-md-4 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="billAddr_postalCode" class="bmd-label-floating">Postal Code</label>
                                  <input type="text" class="form-control" id="billAddr_postalCode" name="billAddr_postalCode" value="{{$customer->billAddr_postalCode}}">
                                </div>
                              </div>
                            </div>

                          </div>

                        </div>


                        <div class="card">
                            <div class="card-header card-header-rose card-header-text sub_card">
                              <div class="card-text card_text_title">
                                <h6 class="card-title">Service Location Address</h6>
                              </div>
                            </div>

                          <div class="card-body">
                            <div class="row input_row">
                              <div class="col-md-4 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="shipAddr_line1" class="bmd-label-floating">Line 1</label>
                                  <input type="text" class="form-control" id="shipAddr_line1" name="shipAddr_line1" value="{{$customer->shipAddr_line1}}">
                                </div>
                              </div>
                              <div class="col-md-4 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="shipAddr_line2" class="bmd-label-floating">Line 2</label>
                                  <input type="text" class="form-control" id="shipAddr_line2" name="shipAddr_line2" value="{{$customer->shipAddr_line2}}">
                                </div>
                              </div>
                              <div class="col-md-4 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="shipAddr_city" class="bmd-label-floating">City</label>
                                  <input type="text" class="form-control" id="shipAddr_city" name="shipAddr_city" value="{{$customer->shipAddr_city}}">
                                </div>
                              </div>
                            </div>
                            
                            <div class="row input_row">
                              <div class="col-md-4 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="shipAddr_country" class="bmd-label-floating">Country</label>
                                  <input type="text" class="form-control" id="shipAddr_country" name="shipAddr_country" value="{{$customer->shipAddr_country}}">
                                </div>
                              </div>
                              <div class="col-md-4 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="shipAddr_countrySubDivisionCode" class="bmd-label-floating">Country SubDivision Code</label>
                                  <input type="text" class="form-control" id="shipAddr_countrySubDivisionCode" name="shipAddr_countrySubDivisionCode" value="{{$customer->shipAddr_countrySubDivisionCode}}">
                                </div>
                              </div>
                              <div class="col-md-4 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="shipAddr_postalCode" class="bmd-label-floating">Postal Code</label>
                                  <input type="text" class="form-control" id="shipAddr_postalCode" name="shipAddr_postalCode" value="{{$customer->shipAddr_postalCode}}">
                                </div>
                              </div>
                            </div>

                          </div>

                        </div>



                        <div class="card">
                            <div class="card-header card-header-rose card-header-text sub_card">
                              <div class="card-text card_text_title">
                                <h6 class="card-title">Others</h6>
                              </div>
                            </div>

                          <div class="card-body ">
                            
                            <div class="row input_row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="webAddr" class="bmd-label-floating">Web Address</label>
                                  <input type="text" class="form-control" id="webAddr" name="webAddr" value="{{$customer->webAddr}}">
                                </div>
                              </div>
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="notes" class="bmd-label-floating">Notes</label>
                                  <input type="text" class="form-control" id="notes" name="notes" value="{{$customer->notes}}">
                                </div>
                              </div>
                            </div>

                          </div>

                        </div>
                    </form>

                     
                  </div>
                </div>
  
  
  
                </div>
              </div>
          </div>
        </div>
      </div>


@endsection