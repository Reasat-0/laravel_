@extends('layouts.master')

@section('content')

        <div class="content add_customer_content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12 mx-auto">
                
                
                <div class="card customer_root_card">
                  <div class="card-header card-header-tabs card-header-rose custom_card_header">
                    <div class="nav-tabs-navigation">
                      <div class="nav-tabs-wrapper">
                        <p class="nav-tabs-title">Add Customer</p>
                        <ul class="nav nav-tabs" data-tabs="tabs" id="tabMenu">
                          <!-- <li class="nav-item">
                            <a class="nav-link" href="#quickbooks" data-toggle="tab">
                              <i class="material-icons">account_circle</i> Quickbooks
                              <div class="ripple-container"></div>
                            </a>
                          </li> -->
                          <li class="nav-item">
                            <a class="nav-link active" href="#custom_customer" data-toggle="tab">
                              <i class="material-icons">account_circle</i> Manual
                              <div class="ripple-container"></div>
                            </a>
                          </li>

                          <!-- <li class="nav-item">
                            <a class="nav-link" href="#bbs_form_tab" data-toggle="tab">
                              <i class="material-icons">account_circle</i> BBS FORM
                              <div class="ripple-container"></div>
                            </a>
                          </li>

                          <li class="nav-item">
                            <a class="nav-link" href="#vacume_truck_checklist" data-toggle="tab">
                              <i class="material-icons">account_circle</i> Vacuum Truck Operator Checklist 
                              <div class="ripple-container"></div>
                            </a>
                          </li>

                          <li class="nav-item">
                            <a class="nav-link" href="#jsa_worksheet" data-toggle="tab">
                              <i class="material-icons">account_circle</i> JSA worksheet
                              <div class="ripple-container"></div>
                            </a>
                          </li> -->
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div style="margin-top: 15px;">
                      @include('partials.messages')
                  </div>

                  <div class="card-body">
                    <div class="tab-content">
                      <div class="tab-pane " id="quickbooks">
                        <div class="card">
                          <div class="card-body">
                            
                              <?php
                                $authUrl = $authUrl;
                              ?>

                              <a class="imgLink" href="#" onclick="oauth.loginPopup()"><img src="{{ asset('assets/img/quickbooks_connect_btn.png') }}" width="178" /></a>

                              <p>If there is no access token or the access token is invalid, click the <b>Connect to QuickBooks</b> button</p>


                              <pre id="accessToken" style="background-color:#efefef;overflow-x:scroll"">
                                <?php
                                  $displayString = isset($accessTokenJson) ? $accessTokenJson : "No Access Token Generated Yet";
                                  echo json_encode($displayString, JSON_PRETTY_PRINT); 
                                ?>
                              </pre>
                              
                              <hr />

                              @if(isset($accessTokenJson))
                                  <h5><b>Import new Customers (Incremental)</b>                
                                    <button  type="button" class="btn btn-success btn-sm" onclick="apiCall.getCompanyInfo()">Import Customer
                                    </button>
                                  </h5>

                                  <p id="loading_text" style="display: none;">customer data is importing from Quickbooks for review, Please wait.</p>

                                  <img src="{{asset('assets/img/loader.gif')}}" id="loader" style="display: none;">
                                  <p id="apiCall"></p>
                                  <hr />

                                  
                                  <div class="form-group bmd-form-group">
                                    <div class="togglebutton admin-role-toggle">
                                      
                                    <span style="color: black; font-size: 15px;">Synchronize Customer data</span>
                                      <label style="color: black; font-size: 15px; font-weight:400 ">
                                          <input type="checkbox" name="" checked="" id="syncCheck">
                                           From Quickbooks to EVOS <span class="toggle"></span> From EVOS to Quickbooks
                                      </label>

                                      <button  type="button" class="btn btn-success btn-sm syncbtn" onclick="sync()" style="margin-left: 10px;">Synchronize</button>

                                    </div>
                                  </div>

                                  <p id="loading_text_sync" style="display: none;">Customer data is synchronizing with Quickbooks, Please wait.</p>
                                  <img src="{{asset('assets/img/loader.gif')}}" id="loader_sync" style="display: none;">
                                  <p id="apiCall_sync"></p>

                                  <p id="loading_text_qb_sync" style="display: none;">Quickbooks is synchronizing with Evo customers, Please wait.</p>
                                  <img src="{{asset('assets/img/loader.gif')}}" id="loader_qb_sync" style="display: none;">
                                  <p id="apiCall_qb_sync"></p>

                              @endif
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane tab_pane_card active" id="custom_customer">
                        <div class="card">
                          <div class="card-header card-header-rose card-header-text lead_card_header user_sub_card">
                            <div class="card-icon">
                                <i class="material-icons">people_outline</i>
                            </div>
                            <!-- <div class="card-text"> -->
                              <h4 class="card-title">CUSTOMER DETAILS</h4>
                          </div>
                          <div class="card-body">
                            <form method="post" action="{{ route('customers.store')  }}" class="form-horizontal customer_form">
                              @csrf
                              <div class="card customer_top_card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Basic</h4>
                                  </div>
                                </div>
                                <div class="card-body ">

                                  <div class="row">
                                    <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="company_name" class="bmd-label-floating input_label">Company Name</label>
                                          <input type="text" class="form-control form_input_field" id="company_name" name="companyName">
                                        </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="display_name" class="bmd-label-floating input_label">Display Name</label>
                                          <input type="text" class="form-control form_input_field" id="display_name" name="displayName" required="true" aria-required="true">
                                        </div>
                                    </div>
                                  </div>
                                  
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="title" class="bmd-label-floating input_label">Title</label>
                                          <input type="text" class="form-control form_input_field" id="title" name="title">
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="given_name" class="bmd-label-floating input_label">Given Name *</label>
                                          <input type="text" class="form-control form_input_field" id="given_name" name="givenName">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="middle_name" class="bmd-label-floating input_label">Middle Name</label>
                                          <input type="text" class="form-control form_input_field" id="middle_name" name="middleName">
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="family_name" class="bmd-label-floating input_label">Family Name</label>
                                          <input type="text" class="form-control form_input_field" id="family_name" name="familyName">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="suffix" class="bmd-label-floating input_label">Suffix</label>
                                          <input type="text" class="form-control form_input_field" id="suffix" name="suffix">
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="account_no" class="bmd-label-floating input_label">Account number</label>
                                          <input type="text" class="form-control form_input_field" id="account_no" name="account_no">
                                        </div>
                                      </div> 
                                    </div>

                                </div>
                              </div>



                              <div class="card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Phone & Email</h4>
                                  </div>
                                </div>
                                <div class="card-body ">
                                  
                                    <div class="row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="primaryPhone_freeFormNumber" class="bmd-label-floating input_label">Phone</label>
                                          <input type="text" class="form-control form_input_field mobile" id="primaryPhone_freeFormNumber" name="primaryPhone_freeFormNumber">
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="primaryEmailAddr_addess" class="bmd-label-floating input_label">Email Address</label>
                                          <input type="email" class="form-control form_input_field" id="primaryEmailAddr_addess" name="primaryEmailAddr_addess">
                                        </div>
                                      </div>
                                    </div>

                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="mobile" class="bmd-label-floating input_label">Mobile</label>
                                          <input type="number" class="form-control form_input_field mobile" id="mobile" name="mobile">
                                        </div>
                                      </div>
                                      <!-- <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="fax" class="bmd-label-floating input_label">Fax</label>
                                          <input type="text" class="form-control form_input_field mobile" id="fax" name="fax">
                                        </div>
                                      </div> -->
                                    <!-- </div>
                                    <div class="row input_row"> -->
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="alternatePhone" class="bmd-label-floating input_label">Other No</label>
                                          <input type="text" class="form-control form_input_field mobile" id="alternatePhone" name="alternatePhone">
                                        </div>
                                      </div>
                                    </div>

                                </div>
                              </div>



                              <div class="card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Billing Address</h4>
                                  </div>
                                </div>

                                <div class="card-body ">
                                  <div class="row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_line1" class="bmd-label-floating input_label">Line 1</label>
                                        <input type="text" class="form-control form_input_field" id="billAddr_line1" name="billAddr_line1">
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_line2" class="bmd-label-floating input_label">Line 2</label>
                                        <input type="text" class="form-control form_input_field" id="billAddr_line2" name="billAddr_line2">
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <div class="row input_row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_city" class="bmd-label-floating input_label">City</label>
                                        <input type="text" class="form-control form_input_field" id="billAddr_city" name="billAddr_city">
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_country" class="bmd-label-floating input_label">Country</label>
                                        <input type="text" class="form-control form_input_field" id="billAddr_country" name="billAddr_country">
                                      </div>
                                    </div>
                                    
                                  </div>
                                  <div class="row input_row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Country SubDivision Code</label>
                                        <input type="text" class="form-control form_input_field" id="billAddr_countrySubDivisionCode" name="billAddr_countrySubDivisionCode">
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_postalCode" class="bmd-label-floating input_label">Postal Code</label>
                                        <input type="text" class="form-control form_input_field" id="billAddr_postalCode" name="billAddr_postalCode">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row input_row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Billing Phone</label>
                                        <input type="text" class="form-control form_input_field mobile" id="billing_phone" name="billing_phone">
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_postalCode" class="bmd-label-floating input_label">Billing Email</label>
                                        <input type="email" class="form-control form_input_field" id="billing_email" name="billing_email">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row input_row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Billing Contact Name</label>
                                        <input type="text" class="form-control form_input_field" id="billing_contact_name" name="billing_contact_name">
                                      </div>
                                    </div>
                                  </div>

                                </div>
                              </div>


                              <div class="card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Service Location Address</h4>
                                  </div>
                                </div>

                                <div class="card-body service_address_card_body">
                                  <div class="card single_address_card top_single_address_card">
                                    <div class="card-header card-header-rose card-header-icon service_address_header">
                                      <div class="timeline-heading">
                                        <h4 class="badge badge-pill badge-info">Default</h4>
                                        <div class="form-check">
                                          <label class="form-check-label">
                                            <input type="hidden" class="is_default" name="is_default[]" value="1">
                                            <input class="form-check-input radio-input" type="radio" name="service_default" value="1" checked=""> Set as default
                                            <span class="circle">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                        
                                      </div>
                                      
                                      <button class="btn btn-danger btn-sm btn-round address_remove_btn" disabled>
                                        <i class="material-icons">close</i>
                                        <div class="ripple-container"></div>
                                      </button>
                                      

                                    </div>
                                    <div class="card-body">
                                      <div class="row">
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_line1" class="bmd-label-floating input_label">Line 1</label>
                                            <input type="text" class="form-control form_input_field" id="shipAddr_line1" name="shipAddr_line1[]">
                                          </div>
                                        </div>
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_line2" class="bmd-label-floating input_label">Line 2</label>
                                            <input type="text" class="form-control form_input_field" id="shipAddr_line2" name="shipAddr_line2[]">
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <div class="row input_row">
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_city" class="bmd-label-floating input_label">City</label>
                                            <input type="text" class="form-control form_input_field" id="shipAddr_city" name="shipAddr_city[]">
                                          </div>
                                        </div>
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_country" class="bmd-label-floating input_label">Country</label>
                                            <input type="text" class="form-control form_input_field" id="shipAddr_country" name="shipAddr_country[]">
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <div class="row input_row">
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Country SubDivision Code</label>
                                            <input type="text" class="form-control form_input_field" id="shipAddr_countrySubDivisionCode" name="shipAddr_countrySubDivisionCode[]">
                                          </div>
                                        </div>
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_postalCode" class="bmd-label-floating input_label">Postal Code</label>
                                            <input type="text" class="form-control form_input_field" id="shipAddr_postalCode" name="shipAddr_postalCode[]">
                                          </div>
                                        </div>
                                      </div>

                                      <div class="row input_row">
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Lease Name</label>
                                            <input type="text" class="form-control form_input_field" id="lease_name" name="lease_name[]">
                                          </div>
                                        </div>
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_postalCode" class="bmd-label-floating input_label">Rig Number</label>
                                            <input type="text" class="form-control form_input_field" id="rig_number" name="rig_number[]">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row input_row">
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_postalCode" class="bmd-label-floating input_label">AFE Number</label>
                                            <input type="text" class="form-control form_input_field" id="afe_number" name="afe_number[]">
                                          </div>
                                        </div>
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Site Contact Phone</label>
                                            <input type="text" class="form-control form_input_field mobile" id="site_contact_phone" name="site_contact_phone[]">
                                          </div>
                                        </div>                                                                               
                                      </div>

                                      <div class="row input_row">

                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_postalCode" class="bmd-label-floating input_label">Rig/Contact Email</label>
                                            <input type="email" class="form-control form_input_field" id="rig_contact_email" name="rig_contact_email[]">
                                          </div>
                                        </div>
                                        <div class="col-md-6 input_wrapper">
                                          <div class="form-group bmd-form-group">
                                            <label for="shipAddr_postalCode" class="bmd-label-floating input_label">Company Man Contact Name</label>
                                            <input type="text" class="form-control form_input_field" id="company_man_contact_name" name="company_man_contact_name[]">
                                          </div>
                                        </div>
                                      </div>

                                      <fieldset class="custom_fieldset_2">
                                        <legend>GPS location</legend>
                                        <div class="row input_row dd_sec">
                                          <div class="col-md-6 input_wrapper">
                                            <div class="form-group bmd-form-group">
                                              <label for="dd_latitude" class="bmd-label-floating input_label">Latitude</label>
                                              <input type="number" class="form-control form_input_field dd_latitude" id="dd_latitude" name="latitude[]">
                                              <span class="error_msg"></span>
                                            </div>
                                          </div>
                                          <div class="col-md-6 input_wrapper">
                                            <div class="form-group bmd-form-group">
                                              <label for="dd_longitude" class="bmd-label-floating input_label">Longitude</label>
                                              <input type="number" class="form-control form_input_field dd_longitude" name="longitude[]">
                                              <span class="error_msg"></span>
                                            </div>
                                          </div>
                                        </div>
                                      </fieldset>



                                    </div>
                                  </div>
                                  <div class="row add_address_btn_wrapper">
                                    <div class="col-md-12 text-right">
                                      <button class="btn btn-info btn-round btn-sm add_address_btn" type="button">
                                        <span class="btn-label">
                                          <i class="material-icons">add</i>
                                        </span>
                                        Add More
                                        <div class="ripple-container"></div>
                                      </button>
                                    </div>
                                  </div>
                                </div>
                              </div>



                              <div class="card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Others</h4>
                                  </div>
                                </div>

                                <div class="card-body ">
                                  
                                  <div class="row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="webAddr" class="bmd-label-floating input_label">Web Address</label>
                                        <input type="url" class="form-control form_input_field" id="webAddr" name="webAddr">
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="notes" class="bmd-label-floating input_label">Notes</label>
                                        <input type="text" class="form-control form_input_field" id="notes" name="notes">
                                      </div>
                                    </div>
                                  </div>

                                </div>
                              </div>

                              <!-- <div class="card quickbooks_update_checking_card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Quickbooks</h4>
                                  </div>
                                </div>

                                <div class="card-body">
                                  
                                  <div class="row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input singleCustomerQB" type="checkbox" name="singleCustomerQB" value="1"> Check here if you want to update data to Quickbooks as well
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                              </div> -->

                              <div class="card quickbooks_update_checking_card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Credit Application</h4>
                                  </div>
                                </div>

                                <div class="card-body">
                                  
                                  <div class="row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input credit_app_creation" type="checkbox" name="credit_app_creation" value="1"> Check here if you want to generate Credit Application during customer creation
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                              </div>

                              <div class="form-group bmd-form-group save_btn_wrapper text-right">
                                <button type="submit" class="btn custom-btn-one btn_green save_btn">Save</button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane" id="bbs_form_tab">
                        <form action="" class="bbs_form">
                          <div class="card">
                            <div class="card-body">
                              <div class="row four_form_header_row">
                                <div class="col-md-12">
                                  <div class="bbs_form_logo">
                                    <a href="{{route('home')}}" class="simple-text logo-normal">
                                      <img src="{{ asset('assets/img/logo.png') }}"/>
                                    </a>
                                  </div>
                                </div>
                                <div class="col-md-12 text-center four_form_title_wrapper">
                                  <h2 class="main-title">BEHAVIOR BASED SAFETY OBSERVATION FORM</h2>
                                </div>
                                <div class="col-md-12">
                                  <p>Your concerns for safety and suggestions on how to improve our safety program are important. Use this form to submit either safety improvement input and/or a BBS Safety Observation. Your name is optional, and the name of the person being observed is not to be used. This information will be used to continually improve our safety system and conditions.</p>
                                </div>
                              </div>
                              <div class="card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Improvement Input</h4>
                                  </div>
                                </div>
                                <div class="card-body">
                                  <div class="row bbs_checkbox_row">
                                    <div class="col-md-4 input_wrapper">
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">BBS Observation
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </div>
                                    <div class="col-md-4 input_wrapper">
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Unsafe Act
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </div>

                                    <div class="col-md-4 input_wrapper">
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Unsafe Condition
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row input_row bbs_checkbox_row">

                                    <div class="col-md-4 input_wrapper">
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Recognition
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </div>

                                    <div class="col-md-4 input_wrapper">
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Environment
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row input_row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="employe_obs_input" class="bmd-label-floating input_label">Employee / Observer Input</label>
                                        <textarea class="form-control form_input_field form_fields" rows="5" name="employe_obs_input" id="employe_obs_input"></textarea>
                                      </div>
                                    </div> 
                                  </div>

                                  <div class="row input_row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="employe_action" class="bmd-label-floating input_label">Employee's Action Taken / Recommendation </label>
                                        <textarea class="form-control form_input_field form_fields" rows="4" name="employe_action" id="employe_action"></textarea>
                                      </div>
                                    </div> 
                                  </div>

                                  <div class="row input_row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="sup_management_action" class="bmd-label-floating input_label">Supervisor / Management Action Taken</label>
                                        <textarea class="form-control form_input_field form_fields" rows="4" name="sup_management_action" id="sup_management_action"></textarea>
                                      </div>
                                    </div> 
                                  </div>

                                </div>
                              </div>

                              <div class="card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Safety Observation Critical Factors ( S = Safe, C = Concern )</h4>
                                  </div>
                                </div>
                                <div class="card-body">
                                  <div class="row">
                                    <div class="col-md-3 input_wrapper">
                                      <div class="table-responsive bbs_form_table text-center">
                                        <h4 class="card-title text-primary">PPE / Procedures / Methods </h4>
                                        <table class="table">
                                          <tbody>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="eye_head" value="s" checked="">
                                                  <input label="C" type="radio" name="eye_head" value="c">
                                                </div>
                                              </td>
                                              <td>Eye And Head</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="hand_body" value="s" checked="">
                                                  <input label="C" type="radio" name="hand_body" value="c">
                                                </div>
                                              </td>
                                              <td>Hand And Body</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="footwear" value="s" checked="">
                                                  <input label="C" type="radio" name="footwear" value="c">
                                                </div>
                                              </td>
                                              <td>Footwear</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio"name="trained_task" value="s" checked="">
                                                  <input label="C" type="radio" name="trained_task" value="c">
                                                </div>
                                              </td>
                                              <td>Trained On Task</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="work_permit_jsa" value="s" checked="">
                                                  <input label="C" type="radio" name="work_permit_jsa" value="c">
                                                </div>
                                              </td>
                                              <td>Work Permit / JSA </td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio"  name="all_train_bbs" value="s" checked="">
                                                  <input label="C" type="radio"name="all_train_bbs" value="c">
                                                </div>
                                              </td>
                                              <td>All Trained In BBS</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                    <div class="col-md-3 input_wrapper">
                                      <div class="table-responsive bbs_form_table text-center">
                                        <h4 class="card-title text-primary">Body Position / Mechanics </h4>
                                        <table class="table">
                                          <tbody>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="proper_poss" value="s" checked="">
                                                  <input label="C" type="radio"name="proper_poss" value="c">
                                                </div>
                                              </td>
                                              <td>Proper Position</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="ask_for_help" value="s" checked="">
                                                  <input label="C" type="radio" name="ask_for_help" value="c">
                                                </div>
                                              </td>
                                              <td>Ask For Help</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="use_dolly" value="s" checked="">
                                                  <input label="C" type="radio" name="use_dolly" value="c">
                                                </div>
                                              </td>
                                              <td>Use Dolly</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="smaller_load" value="s" checked="">
                                                  <input label="C" type="radio" name="smaller_load" value="c">
                                                </div>
                                              </td>
                                              <td>Smaller Loads</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="dont_twist_body" value="s" checked="">
                                                  <input label="C" type="radio" name="dont_twist_body" value="c">
                                                </div>
                                              </td>
                                              <td>Dont Twist Body</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="get_closed_item" value="s" checked="">
                                                  <input label="C" type="radio" name="get_closed_item" value="c">
                                                </div>
                                              </td>
                                              <td>Get Closed To Item</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                    <div class="col-md-3 input_wrapper">
                                      <div class="table-responsive bbs_form_table text-center">
                                        <h4 class="card-title text-primary">Slips / Trips </h4>
                                        <table class="table">
                                          <tbody>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="proper_footwear" value="s" checked="">
                                                  <input label="C" type="radio" name="proper_footwear" value="c">
                                                </div>
                                              </td>
                                              <td>Proper Footwear</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="aware_hazard" value="s" checked="">
                                                  <input label="C" type="radio" name="aware_hazard" value="c">
                                                </div>
                                              </td>
                                              <td>Aware Of Hazards</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="prompt_clean_up" value="s" checked="">
                                                  <input label="C" type="radio" name="prompt_clean_up" value="c">
                                                </div>
                                              </td>
                                              <td>Prompt Clean Up</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="tripping_hazard" value="s" checked="">
                                                  <input label="C" type="radio" name="tripping_hazard" value="c">
                                                </div>
                                              </td>
                                              <td>Tripping Hazards</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="not_russing" value="s" checked="">
                                                  <input label="C" type="radio" name="not_russing" value="c">
                                                </div>
                                              </td>
                                              <td>Not Rushing</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="step_condition" value="s" checked="">
                                                  <input label="C" type="radio" name="step_condition" value="c">
                                                </div>
                                              </td>
                                              <td>Step Conditions</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                    <div class="col-md-3 input_wrapper">
                                      <div class="table-responsive bbs_form_table text-center">
                                        <h4 class="card-title text-primary">Eqipment / Work Environment </h4>
                                        <table class="table">
                                          <tbody>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="MSDS_need" value="s" checked="">
                                                  <input label="C" type="radio" name="MSDS_need" value="c">
                                                </div>
                                              </td>
                                              <td>MSDS, if needed</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="lockout" value="s" checked="">
                                                  <input label="C" type="radio" name="lockout" value="c">
                                                </div>
                                              </td>
                                              <td>Lockout</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="tools_are_safe" value="s" checked="">
                                                  <input label="C" type="radio" name="tools_are_safe" value="c">
                                                </div>
                                              </td>
                                              <td>Tools Are Safe</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="adjacent_work" value="s" checked="">
                                                  <input label="C" type="radio" name="adjacent_work" value="c">
                                                </div>
                                              </td>
                                              <td>Adjacent Work</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="signage_need" value="s" checked="">
                                                  <input label="C" type="radio" name="signage_need" value="c">
                                                </div>
                                              </td>
                                              <td>Signage, if needed</td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class="radio-custom-btn-section bbs_radio">
                                                  <input label="S" type="radio" name="spill_control" value="s" checked="">
                                                  <input label="C" type="radio" name="spill_control" value="c">
                                                </div>
                                              </td>
                                              <td>Spill Control</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row input_row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="sup_management_action" class="bmd-label-floating input_label">Observer’s Feedback Given to Other Employee </label>
                                        <textarea class="form-control form_input_field form_fields" rows="5" name="sup_management_action" id="sup_management_action"></textarea>
                                      </div>
                                    </div> 
                                  </div>

                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="make_equip" class="bmd-label-floating input_label">Location</label>
                                        <input type="text" class="form-control form_input_field form_fields" id="make_equip" name="make" required="">
                                      </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="make_equip" class="bmd-label-floating input_label">Observer's Name</label>
                                        <input type="text" class="form-control form_input_field form_fields" id="make_equip" name="make" required="">
                                      </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="make_equip" class="bmd-label-floating input_label">Dates</label>
                                        <input type="text" class="datepicker form-control form_input_field form_fields" id="make_equip" name="make" required="">
                                      </div>
                                    </div>

                                  </div>

                                </div>
                              </div>

                            </div>
                          </div>
                        </form>
                      </div>

                      <div class="tab-pane" id="vacume_truck_checklist">
                        <form id="vacume_truck_checklist_form">
                          <div class="card vacume_truck_checklist_card" id="top_vacuum_card">
                            <div class="card-header card-header-rose card-header-icon user_sub_card">
                              <div class="card-icon">
                                <i class="material-icons">assignment</i>
                              </div>
                              <h4 class="card-title">Vacuum Truck Operator Checklist</h4>
                            </div>
                            <div class="card-body">
                              <div class="row">
                                <div class="col-md-1 progress_container vacume_truck_checklist_arrow" style="text-align:center;margin-bottom:20px;">
                                  <span class="stepVehOpeCheck step-first active">
                                    <i class="material-icons">done</i>
                                  </span>
                                  <span class="stepVehOpeCheck step-inner">
                                    <i class="material-icons">done</i>
                                  </span>
                                  <span class="stepVehOpeCheck step-inner">
                                    <i class="material-icons">done</i>
                                  </span>
                                  <span class="stepVehOpeCheck step-inner">
                                    <i class="material-icons">done</i>
                                  </span>
                                  <span class="stepVehOpeCheck step-inner">
                                    <i class="material-icons">done</i>
                                  </span>
                                </div>

                                <div class="col-md-11 vacume_truck_checklist_content">
                                  <div class="tabVehOpeCheck">
                                    <div class="card">
                                      <div class="card-body">
                                        <h4 class="vehicle-checklist-title">Section 1 <span>Truck Requirements</span></h4>
                                        <div class="row vehicle-check-row">
                                          <div class="col-md-12">
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">The cargo tank displays a name plate/specification plate confirming current DOT compliance.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="speci_plate" value="yes" checked>
                                                  <input label="No" type="radio" name="speci_plate" value="no">
                                                  <input label="N/A" type="radio" name="speci_plate" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">The cargo tank is designed to ASME Boiler and Pressure Vessel Code, Section VIII, Division 1 (or Canadian National Board) standards.  (Minimum 25 psi design and 40 psi test pressures)</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="pressure_vessel_code" value="yes" checked>
                                                  <input label="No" type="radio" name="pressure_vessel_code" value="no">
                                                  <input label="N/A" type="radio" name="pressure_vessel_code" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Truck inspection stickers for over-the-road transport and operation are current.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="truck_ins_sticker" value="yes" checked>
                                                  <input label="No" type="radio" name="truck_ins_sticker" value="no">
                                                  <input label="N/A" type="radio" name="truck_ins_sticker" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">The truck is equipped with two 20 lb. or larger dry chemical fire extinguisher rated for Class B fires.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="truck_equip" value="yes" checked>
                                                  <input label="No" type="radio" name="truck_equip" value="no">
                                                  <input label="N/A" type="radio" name="truck_equip" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Cargo tank is clean, door and dome gaskets in good condition and seal tightly.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="cargo_tank_clean" value="yes" checked>
                                                  <input label="No" type="radio" name="cargo_tank_clean" value="no">
                                                  <input label="N/A" type="radio" name="cargo_tank_clean" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Pressure valves, pressure relief (safety) valves, gaskets, and shutoff valves have been inspected, are leak tight, working properly, and, where appropriate, can be capped while in transit.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="press_valves_relief" value="yes" checked>
                                                  <input label="No" type="radio" name="press_valves_relief" value="no">
                                                  <input label="N/A" type="radio" name="press_valves_relief" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Floats for liquid level indicators move freely and are working properly.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="float_liq_level_ind" value="yes" checked>
                                                  <input label="No" type="radio" name="float_liq_level_ind" value="no">
                                                  <input label="N/A" type="radio" name="float_liq_level_ind" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row note-row">
                                              <label>
                                                <span>NOTE</span> If the answer to any of the statement above is “No” or “N/A”, a comment must be entered to explain.</label>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="tabVehOpeCheck">
                                    <div class="card">
                                      <div class="card-body">
                                        <h4 class="vehicle-checklist-title">Section 1 <span>Truck Requirements</span></h4>
                                        <div class="row vehicle-check-row">
                                          <div class="col-md-12">
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">The vacuum truck is equipped with at least 50 ft. (15 meters) of loading hose and at least 50 ft. of vacuum pump vent (exhaust) hose.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="vac_equi_50" value="yes" checked>
                                                  <input label="No" type="radio" name="vac_equi_50" value="no">
                                                  <input label="N/A" type="radio" name="vac_equi_50" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Hoses and accessories are conductive and periodic inspection results are available.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="hoses_access" value="yes" checked>
                                                  <input label="No" type="radio" name="hoses_access" value="no">
                                                  <input label="N/A" type="radio" name="hoses_access" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Connections are clean and free of rust and paint.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="conn_clean" value="yes" checked>
                                                  <input label="No" type="radio" name="conn_clean" value="no">
                                                  <input label="N/A" type="radio" name="conn_clean" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Hoses, connections, and fittings are in good condition, suitable for the material being loaded/unloaded, and appropriately sized.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="hoses_conn_fitt" value="yes" checked>
                                                  <input label="No" type="radio" name="hoses_conn_fitt" value="no">
                                                  <input label="N/A" type="radio" name="hoses_conn_fitt" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">If hose > 4" (100mm), hose has protector in place.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="hoses_100mm" value="yes" checked>
                                                  <input label="No" type="radio" name="hoses_100mm" value="no">
                                                  <input label="N/A" type="radio" name="hoses_100mm" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">The truck is equipped with cables suitable for bonding the hoses and grounding the truck.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="bonding_hoss_ground" value="yes" checked>
                                                  <input label="No" type="radio" name="bonding_hoss_ground" value="no">
                                                  <input label="N/A" type="radio" name="bonding_hoss_ground" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Strong alligator clamps, special purpose C-clamps, or equivalent devices are used as connectors for bonding/grounding cables.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="strong_alligator" value="yes" checked>
                                                  <input label="No" type="radio" name="strong_alligator" value="no">
                                                  <input label="N/A" type="radio" name="strong_alligator" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Wheel chocks are available.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="wheel_chocks" value="yes" checked>
                                                  <input label="No" type="radio" name="wheel_chocks" value="no">
                                                  <input label="N/A" type="radio" name="wheel_chocks" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">PTO-driven vacuum pumps have a properly functioning engine overspeed trip device.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="PTO_driven" value="yes" checked>
                                                  <input label="No" type="radio" name="PTO_driven" value="no">
                                                  <input label="N/A" type="radio" name="PTO_driven" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row note-row">
                                              <label>
                                                <span>NOTE</span> If the answer to any of the statement above is “No” or “N/A”, a comment must be entered to explain.</label>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="tabVehOpeCheck">
                                    <div class="card">
                                      <div class="card-body">
                                        <h4 class="vehicle-checklist-title">Section 2 <span>Truck Operator Requirements</span></h4>
                                        <div class="row vehicle-check-row">
                                          <div class="col-md-12">
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Familiar with the properties and hazards of the material being loaded.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="prop_hazard_load" value="yes" checked>
                                                  <input label="No" type="radio" name="prop_hazard_load" value="no">
                                                  <input label="N/A" type="radio" name="prop_hazard_load" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Equipped with hearing protection, respiratory protection, chemical protective suit, face shield, goggles, and chemical-resistant gloves and boots.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="equipped_other_ins" value="yes" checked>
                                                  <input label="No" type="radio" name="equipped_other_ins" value="no">
                                                  <input label="N/A" type="radio" name="equipped_other_ins" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Wearing proper hearing protection during vacuum operations.  Wearing respiratory protection when necessary/required. </label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="wearing_protection" value="yes" checked>
                                                  <input label="No" type="radio" name="wearing_protection" value="no">
                                                  <input label="N/A" type="radio" name="wearing_protection" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Carrying certification for proof of training that includes HAZWOPER and, when required, HAZMAT and Hazard Communication.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="carrying_certification" value="yes" checked>
                                                  <input label="No" type="radio" name="carrying_certification" value="no">
                                                  <input label="N/A" type="radio" name="carrying_certification" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Has a current vacuum truck log.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="vacuum_truck_log" value="yes" checked>
                                                  <input label="No" type="radio" name="vacuum_truck_log" value="no">
                                                  <input label="N/A" type="radio" name="vacuum_truck_log" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row note-row">
                                              <label>
                                                <span>NOTE</span> If the answer to any of the statement above is “No” or “N/A”, a comment must be entered to explain.</label>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="tabVehOpeCheck">
                                    <div class="card">
                                      <div class="card-body">
                                        <h4 class="vehicle-checklist-title">Section 3 <span>Truck Operation</span></h4>
                                        <div class="row vehicle-check-row">
                                          <div class="col-md-12">
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">The truck is located at least 35 ft. (10 meters) upwind/crosswind of the material to be loaded.  A wind sock or other indicator is monitoring wind direction.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="truck_locate_35" value="yes" checked>
                                                  <input label="No" type="radio" name="truck_locate_35" value="no">
                                                  <input label="N/A" type="radio" name="truck_locate_35" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">The vacuum pump discharge is vented to a safe location at least 50 ft. (15 meters) downwind from the truck when loading volatile materials.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="vacuum_pump_discharge" value="yes" checked>
                                                  <input label="No" type="radio" name="vacuum_pump_discharge" value="no">
                                                  <input label="N/A" type="radio" name="vacuum_pump_discharge" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">The temperature of materials being loaded does not exceed 75°C (167°F).</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="temp_mat_not_75" value="yes" checked>
                                                  <input label="No" type="radio" name="temp_mat_not_75" value="no">
                                                  <input label="N/A" type="radio" name="temp_mat_not_75" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">Operator will remain between the truck and source or receiving container and within 25 ft. of the truck to monitor the operation. <span class="label_with_note"><b>NOTE:</b> The truck cab is not to be occupied while vacuum operation is occurring.</span></label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="ope_truck_source" value="yes" checked>
                                                  <input label="No" type="radio" name="ope_truck_source" value="no">
                                                  <input label="N/A" type="radio" name="ope_truck_source" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">The truck engine is shut down when the vacuum pump is powered with an auxiliary engine.  </label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="truck_engine_shut_down" value="yes" checked>
                                                  <input label="No" type="radio" name="truck_engine_shut_down" value="no">
                                                  <input label="N/A" type="radio" name="truck_engine_shut_down" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">The truck is parked on level ground, wheels are chocked for the loading or unloading operation, and the parking brake is set.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="truck_park_ground" value="yes" checked>
                                                  <input label="No" type="radio" name="truck_park_ground" value="no">
                                                  <input label="N/A" type="radio" name="truck_park_ground" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">A continuous conductive path between the truck and source or receiving container has been established before transfer of flammable liquids and verified visually or with an ohmmeter.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="cond_path_truck_source" value="yes" checked>
                                                  <input label="No" type="radio" name="cond_path_truck_source" value="no">
                                                  <input label="N/A" type="radio" name="cond_path_truck_source" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <label class="col-sm-12 col-form-label">The truck is bonded to a grounded metallic structure (e.g., storage tank, underground piping, or a ground rod) before the liquids transfer operation.</label>
                                              <div class="col-md-5 custom-radio-main-section">
                                                <div class="radio-custom-btn-section">
                                                  <input label="Yes" type="radio" name="grounded_metallic_structure" value="yes" checked>
                                                  <input label="No" type="radio" name="grounded_metallic_structure" value="no">
                                                  <input label="N/A" type="radio" name="grounded_metallic_structure" value="n/a">
                                                </div>
                                              </div>
                                              <div class="col-md-7 vacume_truck_comments_sec">
                                                <input type="text" class="form-control form_input_field form_fields field_to_fill valid" id="" name="" value="" aria-invalid="false">
                                              </div>
                                            </div>
                                            <div class="row note-row">
                                              <label><span>NOTE</span> If the answer to any of the statement above is “No” or “N/A”, a comment must be entered to explain.</label>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="tabVehOpeCheck">
                                    <div class="card">
                                      <div class="card-body">

                                        <div class="row input_row mt-2">
                                          <div class="col-md-12 draw_signature_col text-center">
                                              <label class="" for="">Signature (Drag Your cursor to sign below)</label>
                                              <label class="sign_error_owner_msg" style="display: none;" for="">Please Put Your Signature</label>
                                              <br/>
                                              <div id="sig" ></div>
                                                  <input type="hidden" class="sign_input_validation_owner" value="0"> 
                                              <br/>
                                              <button id="clear" class="btn btn-sm btn-round btn-danger my-3">Clear Signature</button>
                                              <textarea id="signature64" name="signed" style="display: none"></textarea>
                                          </div>
                                          <!-- <canvas id='blank' style='display:none'></canvas> -->
                                        </div>
                                        <div class="row">
                                          <div class="col-md-6 input_wrapper">
                                            <div class="form-group bmd-form-group">
                                              <label for="vehicle_check_date" class="bmd-label-floating input_label">Date</label>
                                              <input type="text" class="form-control form_input_field form_fields datepicker" id="vehicle_check_date" name="vehicle_check_date" required="">
                                            </div>
                                          </div>
                                          <div class="col-md-6 input_wrapper">
                                            <div class="form-group bmd-form-group">
                                              <label for="printed_name" class="bmd-label-floating input_label">Printed Name</label>
                                              <input type="text" class="form-control form_input_field form_fields" id="printed_name" name="printed_name" required="">
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div style="overflow:auto;" class="row msf_btn_holder">
                                    <div class="col-md-6 save_btn_wrapper text-left" style="float:right;">
                                      <a class="btn btn-round btn-warning nextPrevBtnOne prev" href="#top_vacuum_card" type="button" id="prevBtnVehOpeCheck" onclick="nextPrevVehOpeCheck(-1)">Previous</a>
                                    </div>
                                    <div class="col-md-6 save_btn_wrapper text-right">
                                      <a class="btn custom-btn-one btn-round btn-success nextPrevBtnOne next nextBtnAgreement" href="#top_vacuum_card" type="button" id="nextBtnVehOpeCheck" onclick="nextPrevVehOpeCheck(1)">Next</a>
                                      <button class="btn custom-btn-one btn-round btn-success nextPrevBtnOne submitBtnVehOpeCheck nextBtnAgreement" style="display: none" type="submit" onclick="">Submit</button>
                                    </div>
                                  </div>

                                </div>

                              </div>
                            </div>
                          </div>
                        </form>
                      </div>

                      <div class="tab-pane" id="jsa_worksheet">
                        <form action="" id="jsa_worksheet_form">
                          <div class="card" id="jsa_worksheet_card">
                            <div class="card-body">
                              <div class="row jsa_header_row">
                                <div class="col-md-12 text-center">
                                  <div class="logo">
                                  <img src="{{asset('assets/img/logo_new.png')}}" alt="logo"/>
                                  </div>
                                  <h2 class="main-title">JSA Worksheet</h2>
                                  <p> 903 W.Industrial, Midlan, Texas, 79701 </p>
                                  <p> 215 E. Elm Street, Loving, New Mexico 88256</p>
                                </div>
                                <div class="col-md-4 input_wrapper">
                                  <div class="form-group bmd-form-group">
                                    <label for="jsa_date" class="bmd-label-floating input_label">Date</label>
                                    <input type="text" class="datepicker form-control form_input_field" id="jsa_date" name="jsa_date">
                                  </div>
                                  <div class="form-group bmd-form-group">
                                    <label for="jsa_time" class="bmd-label-floating input_label">Time</label>
                                    <input type="text" class="timepicker form-control form_input_field" id="jsa_time" name="jsa_time">
                                  </div>                                  
                                </div>
                                <div class="col-md-4 input_wrapper">                                  
                                </div>
                                <div class="col-md-4 input_wrapper">
                                  <div class="form-group bmd-form-group">
                                    <label for="jsa_supervisor" class="bmd-label-floating input_label">Supervisor</label>
                                    <input type="text" class="form-control form_input_field" id="jsa_supervisor" name="jsa_supervisor">
                                  </div>
                                  <div class="form-group bmd-form-group">
                                    <label for="jsa_job_description" class="bmd-label-floating input_label">Job Description</label>
                                    <input type="text" class="form-control form_input_field" id="jsa_job_description" name="jsa_job_description">
                                  </div>                                  
                                </div>                                
                              </div>

                              <div class="row">
                                <div class="col-md-1 progress_container" style="text-align:center;margin-bottom:20px;">
                                  <span class="stepJsa step-first">
                                    <i class="material-icons">done</i>
                                  </span>
                                  <span class="stepJsa step-inner">
                                    <i class="material-icons">done</i>
                                  </span>
                                  <span class="stepJsa step-inner">
                                    <i class="material-icons">done</i>
                                  </span>
                                  <span class="stepJsa step-inner">
                                    <i class="material-icons">done</i>
                                  </span>
                                  <span class="stepJsa step-inner">
                                    <i class="material-icons">done</i>
                                  </span>                               
                                </div>

                                <div class="col-md-11 content_container rental_ag_content_container">
                                  <div class="tabJsa">
                                    <div class="card jsa_tab_main_card">
                                      <!-- <div class="card-header card-header-rose card-header-text sub_card">
                                        <div class="card-text card_text_title">
                                          <h4 class="card-title">General</h4>
                                        </div>
                                      </div> -->
                                      <div class="card-body sub_row">

                                        <div class="row jsa_body">
                                          <div class="col-md-3">
                                            <div class="form-group bmd-form-group">
                                              <label for="jsa_locaiton" class="bmd-label-floating input_label">Location</label>
                                              <input type="text" class="form-control form_input_field" id="jsa_location" name="jsa_locaitons">
                                            </div>
                                          </div>
                                          <div class="col-md-3">
                                            <div class="form-group bmd-form-group">
                                              <label for="jsa_sop" class="bmd-label-floating input_label">SOP Reviewed</label>
                                              <input type="text" class="form-control form_input_field" id="jsa_sop" name="jsa_sop">
                                            </div>
                                          </div>
                                          <div class="col-md-3">
                                            <div class="form-group bmd-form-group">
                                              <label for="jsa_lats" class="bmd-label-floating input_label">Lats</label>
                                              <input type="text" class="form-control form_input_field" id="jsa_lats" name="jsa_lats">
                                            </div>
                                          </div>
                                          <div class="col-md-3">
                                            <div class="form-group bmd-form-group">
                                              <label for="jsa_long" class="bmd-label-floating input_label">Long</label>
                                              <input type="text" class="form-control form_input_field" id="jsa_long" name="jsa_long">
                                            </div>
                                          </div>
                                          <div class="col-md-12 jsa_table_wrapper">
                                            <div class="table-responsive jsa_table">
                                              <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th>Basic Job Steps</th>
                                                    <th>Identify Potential Hazards</th>
                                                    <th>Hand/Finger Potential</th>
                                                    <th>Environmental</th>
                                                    <th>What you did to eliminate or reduce the risk</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td>blah</td>
                                                    <td>USA</td>
                                                    <td class="">2.920</td>
                                                    <td class="">53</td>
                                                    <td class="">53</td>
                                                  </tr>
                                                  <tr>
                                                    <td>blah</td>
                                                    <td>USA</td>
                                                    <td class="">2.920</td>
                                                    <td class="">53</td>
                                                    <td class="">53</td>
                                                  </tr>
                                                  <tr>
                                                    <td>blah</td>
                                                    <td>USA</td>
                                                    <td class="">2.920</td>
                                                    <td class="">53</td>
                                                    <td class="">53</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <p class="text-center"> Use the go card on reverse and have all parties sign on the back.</p>
                                            <p class="text-center"> Document revisions ( after break, change in job, addition of employees at worksite,etc )</p>
                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                  </div>
                                  <div class="tabJsa">
                                    <div class="card jsa_tab_main_card">
                                      <div class="card-body sub_row">
                                        <h4 class="sub-title">Job Steps </h4>
                                        <div class="row">
                                          <ul class="subcontractor_agre_list bold">
                                            <li>
                                              What is the written procedure for this task that we are going to do?
                                            </li>
                                            <li>
                                              What are the job steps for this task? How are you going to do this job?
                                            </li>
                                            <li>
                                              What job steps are each of you going to be doing during the job?
                                            </li>
                                            <li>
                                              What training havve you had on the job steps and or procedure for the task?
                                            </li>
                                            <li>
                                              What permit do we need before we do this task ( Hot Work Confined Space. LOTO. et.)?
                                            </li>
                                          </ul>
                                        </div>
                                        <h4 class="sub-title">Equipment & Tools </h4>
                                        <div class="row">
                                            <ul class="subcontractor_agre_list bold">
                                            <li>
                                              What tools do we need to do this job right?
                                            </li>
                                            <li>
                                              What is the right way to use these tools?
                                            </li>
                                            <li>
                                              When is the last time we looked at the tools to ensure they are clean and in good working shape?
                                            </li>
                                            </ul>
                                        </div>

                                      </div>
                                    </div>
                                  </div>
                                  <div class="tabJsa">
                                    <div class="card jsa_tab_main_card">
                                      <div class="card-body sub_row">
                                        <h4 class="sub-title">Identify Hazards </h4>
                                        <div class="row">
                                          <p> Please rememeber - For every hazard you must do something about it!</p>
                                          <p> Elimnate the hazard or mitigate the risk down to an acceptable level! </p>
                                            <ul class="subcontractor_agre_list bold">
                                              <li>
                                                What can fall on us or around us?
                                              </li>
                                              <li>
                                                What can we get caught in?
                                              </li>
                                              <li>
                                                What can we get smashed by or pinned between?
                                              </li>
                                              <li>
                                                What can we get out hands/fingers pinched in between?
                                              </li>
                                              <li>
                                                What could hit us in the face or head?
                                              </li>
                                              <li>
                                                What can hurt our backs?
                                              </li>
                                              <li>
                                                What can we trip over?
                                              </li>
                                              <li>
                                                What can we fall off of or fall through?
                                              </li>
                                              <li>
                                                What can burn us while we are doing this job??
                                              </li>
                                              <li>
                                                What could sting or bite us?
                                              </li>
                                              <li>
                                                What electrical current can we be shocked by?
                                              </li>
                                              <li>
                                                What could we breathe in, swallow or get on our sking that could hurt us?
                                              </li>
                                              <li>
                                                What could we be doing that would be too much for our bodies to take?
                                              </li>
                                              <li>
                                                What new employees are on location who might be a hazard to themselves or us?
                                              </li>
                                            </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="tabJsa">
                                    <div class="card jsa_tab_main_card">
                                      <div class="card-body sub_row">
                                        <h4 class="sub-title">Personal Protective Equipment</h4>
                                        <div class="row">
                                            <ul class="subcontractor_agre_list bold">
                                              <li>
                                                What is the right PPE that we need to protect ourselves? ( Hard Hat, Glasses/ Goggles, Ear Plugs, Gloves, Steel Toe Boots, Respiratory Protectionk, Fire Retardant Clothing, Fall Protection)
                                              </li>
                                            </ul>
                                        </div>
                                        <h4 class="sub-title">Changing The Course Of Works</h4>
                                        <div class="row">
                                            <ul class="subcontractor_agre_list bold">
                                              <li>
                                                What would make us stop the job or change the job steps for the job?
                                              </li>
                                              <li>
                                                What would we have to change our tools or equipment?
                                              </li>
                                              <li>
                                                Why would we have to change where we are standing?
                                              </li>
                                              <li>
                                                What would cause us to have to change out P.P.E?
                                              </li>
                                            </ul>
                                        </div>
                                        <p class="jsa_form_foot_note text-center"> Each of us have the right and obligation to <span> Stop Unsafe acts </span></p>
                                        <p class="jsa_submission_foot_note text-center"> Please Submit The Completed JSA form to your superviser</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="tabJsa">
                                    <div class="card jsa_tab_main_card">
                                        <div class="card-body sub_row">
                                          <div class="row">
                                            <div class="col-md-12 text-center ending_para_wrapper">
                                              <p>Revisit and refresh the JSA after breaks, change in job/employees & when visitors are on location.</p>
                                            </div>
                                          </div>
                                          <h4 class="sub-title">Signature</h4>
                                          <div class="row">
                                            <div class="col-md-12 text-center ending_para_wrapper">
                                              <p>JSA performed to protect the employees who signed above </p>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                  </div>                                
                                  <div class="row msf_btn_holder">
                                    <div class="col-md-6 save_btn_wrapper text-left" style="float:right;">
                                      <a class="btn btn-round btn-warning nextPrevBtn prev" href="#jsa_worksheet_card" id="prevBtnJsa" type="button" onclick="nextPrevJsa(-1)" style="display: inline;">Previous<div class="ripple-container"></div></a>
                                    </div>
                                    <div class="col-md-6 save_btn_wrapper text-right">
                                      <a class="btn custom-btn-one btn-round btn-success nextPrevBtn next" href="#jsa_worksheet_card" id="nextBtnJsa" type="button" onclick="nextPrevJsa(1)">Next<div class="ripple-container"></div></a>
                                      
                                      <button class="btn custom-btn-one btn-round btn-success nextPrevBtn submitBtnJsa" type="submit" onclick="" style="">Submit<div class="ripple-container"></div></button>
                                    </div>
                                  </div>

                                </div>

                              </div>
                            </div>



                              
                          </div>
                        </form>
                      </div>

                  </div>
                </div>
              </div>



              </div>
            </div>
          </div>
        </div>


<script>

        var url = '<?php echo $authUrl; ?>';

        var OAuthCode = function(url) {

            this.loginPopup = function (parameter) {
                this.loginPopupUri(parameter);
            }

            this.loginPopupUri = function (parameter) {

                // Launch Popup
                var parameters = "location=1,width=800,height=650";
                parameters += ",left=" + (screen.width - 800) / 2 + ",top=" + (screen.height - 650) / 2;

                var win = window.open(url, 'connectPopup', parameters);
                var pollOAuth = window.setInterval(function () {
                    try {

                        if (win.document.URL.indexOf("code") != -1) {
                            window.clearInterval(pollOAuth);
                            win.close();
                            location.reload();
                        }
                    } catch (e) {
                        console.log(e)
                    }
                }, 100);
            }
        }


        var apiCall = function() {
            this.getCompanyInfo = function() {
                $.ajax({
                      url:"{{ route('qbCustomer') }}",
                      method:"GET",
                      cache: false,
                      beforeSend: function(){
                          $('#loading_text').show();
                          $('#loader').show();
                      },
                      complete: function(){
                          $('#loading_text').hide();
                          $('#loader').hide();
                      },
                      success:function(result)
                      {
                       $('#apiCall').html(result);
                      }
                   })
            }           

        }


          function sync(){
            if($('#syncCheck').prop("checked") == true){
              synchronizeQb();
            }
            else{
              synchronizeCustomer();
            }
          }  

          function synchronizeCustomer() {
                $.ajax({
                      url:"{{ route('synchronizeCustomer') }}",
                      method:"GET",
                      cache: false,
                      beforeSend: function(){
                          $('#loading_text_sync').show();
                          $('#loader_sync').show();
                      },
                      complete: function(){
                          $('#loading_text_sync').hide();
                          $('#loader_sync').hide();
                      },
                      success:function(result)
                      {
                       $('#apiCall_sync').html(result);
                      }
                   })
            }

            function synchronizeQb() {
                $.ajax({
                      url:"{{ route('synchronizeQb') }}",
                      method:"GET",
                      cache: false,
                      beforeSend: function(){
                          $('#loading_text_qb_sync').show();
                          $('#loader_qb_sync').show();
                      },
                      complete: function(){
                          $('#loading_text_qb_sync').hide();
                          $('#loader_qb_sync').hide();
                      },
                      success:function(result)
                      {
                       $('#apiCall_qb_sync').html(result);
                      }
                   })
            } 

      $('.singleCustomerQB').change(function() {
          if($(this).is(":checked")) {
              $.ajax({
                        url:"{{ route('singleCustomerQB') }}",
                        method:"GET",
                        cache: false,
                        success:function(result)
                        {
                         if(result == 'connect'){
                            alert('Please click on connect to quickbooks button from Quickbooks Tab to get access token');
                            $('.singleCustomerQB').prop("checked", false);
                         }
                         else if(result == 'expire'){
                          alert('Your access token is expired, Please click on Connect to Quickbooks button from QUickbooks tab');
                          $('.singleCustomerQB').prop("checked", false);
                         }
                        
                        }
                     })
          }       
      });



        var oauth = new OAuthCode(url);
        var apiCall = new apiCall();


$(document).on('change', '.radio-input', function (e) {
    $('.is_default').val('0');
    $(this).parent().find('.is_default').val('1');
    
});

$(document).ready(function () {

if($('#tabMenu a[href="#{{ old('tab') }}"]').attr('href') == undefined){
    $('#tabMenu a[href="#quickbooks"]').tab('show')  
}
else{
    $('#tabMenu a[href="#{{ old('tab') }}"]').tab('show')
}


$('.custom-select.select2').each(function(){

  if($(this).val()!=""){
    $(this).addClass('hasvalue')
  }

})

});


$('.custom-select.select2').on('change',function(){
  if($(this).val()!=""){
    $(this).addClass('hasvalue')
  }
})




  // SIGNATURE VALIDATION





// function isCanvasBlank(canvas) {
//     var blank = document.createElement('canvas');
//     blank.width = canvas.width;
//     blank.height = canvas.height;

//     blank.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
//     return canvas.toDataURL() == blank.toDataURL();
// }

// end SIGNATURE VALIDATION 

/*

var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear').click(function(e) {
    e.preventDefault();
    sig.signature('clear');
    $("#signature64").val('');
    draw($(this));

    $('.sign_input_validation').val("0");
    if($('.sign_input_validation').val() == "0"){
      $('.sign_error_msg').show();
    }
    else{
      $('.sign_error_msg').hide();
    }

});


$(document).ready(function(){
  if($('.sc_sign').length > 0){
    var canvas = $('.kbw-signature').find('canvas')[0];
    const context = canvas.getContext("2d");

    const img = new Image()
    img.src = $('.sc_sign').val()
    img.onload = () => {
      context.drawImage(img, 0, 0)
    }
  }

});

$('#sig canvas').mousedown(function(){
  $(this).parents('#sig').siblings('.sign_input_validation').val("1");
    if($('.sign_input_validation').val() == "0"){
      $('.sign_error_msg').show();
    }
    else{
      $('.sign_error_msg').hide();
    }
})
function draw(btn) {

  var canvas = btn.siblings('.kbw-signature').find('canvas')[0];
  var ctx = canvas.getContext("2d")
  //This line is actually not even needed...
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.rect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = 'white';
  ctx.fill();
}
$(document).ready(function(){
  $('canvas').attr('width','985.72');
})
*/


//  Vehicle Operator SIgnature Validation


// SIGNATURE VALIDATION

function isCanvasBlank(canvas) {
    var blank = document.createElement('canvas');
    blank.width = canvas.width;
    blank.height = canvas.height;

    blank.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    return canvas.toDataURL() == blank.toDataURL();
}

// end SIGNATURE VALIDATION 



var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear').click(function(e) {
    e.preventDefault();
    sig.signature('clear');
    $("#signature64").val('');
    draw($(this));

    $('.sign_input_validation_owner').val("0");
    if($('.sign_input_validation_owner').val() == "0"){
      $('.sign_error_owner_msg').show();
    }
    else{
      $('.sign_error_owner_msg').hide();
    }



    // $('canvas').attr('width','100');
});


$(document).ready(function(){
if($('.owner_sign').length > 0){
  var canvas = $('.kbw-signature').find('canvas')[0];
  const context = canvas.getContext("2d");

  const img = new Image()
  img.src = $('.owner_sign').val()
  img.onload = () => {
    context.drawImage(img, 0, 0)
  }
}
});

$('#sig canvas').mousedown(function(){
  $(this).parents('#sig').siblings('.sign_input_validation_owner').val("1");
    if($('.sign_input_validation_owner').val() == "0"){
      $('.sign_error_owner_msg').show();
    }
    else{
      $('.sign_error_owner_msg').hide();
    }
})
// var canvas = document.getElementsByTagName('canvas')[0]
// var ctx = canvas.getContext("2d")
function draw(btn) {

  var canvas = btn.siblings('.kbw-signature').find('canvas')[0];
  var ctx = canvas.getContext("2d")
  //This line is actually not even needed...
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.rect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = 'white';
  ctx.fill();
}
$(document).ready(function(){
  // document.getElementsByTagName('canvas').width = 985.72;
  $('canvas').attr('width','985.72');
})


</script>


@endsection