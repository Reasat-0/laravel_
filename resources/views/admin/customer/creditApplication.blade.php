@extends('layouts.master')

@section('content')

        <div class="content add_customer_content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12 mx-auto">
                
                
                <div class="">
					<!-- <div class="card-header card-header-tabs card-header-rose custom_card_header">
						<div class="nav-tabs-navigation">
						  <div class="nav-tabs-wrapper">
						    <p class="nav-tabs-title">Add Customer</p>
						    <ul class="nav nav-tabs" data-tabs="tabs" id="tabMenu">
						      <li class="nav-item">
						        <a class="nav-link" href="#quickbooks" data-toggle="tab">
						          <i class="material-icons">account_circle</i> Quickbooks
						          <div class="ripple-container"></div>
						        </a>
						      </li>
						      <li class="nav-item">
						        <a class="nav-link" href="#custom_customer" data-toggle="tab">
						          <i class="material-icons">account_circle</i> Manual
						          <div class="ripple-container"></div>
						        </a>
						      </li>
						      <li class="nav-item">
						        <a class="nav-link" href="#customer_info" data-toggle="tab">
						          <i class="material-icons">account_circle</i> Credit Application
						          <div class="ripple-container"></div>
						        </a>
						      </li>
						    </ul>
						  </div>
						</div>
					</div> -->


                  	<div class="">
	                  <div class="tab-pane-customer-info" id="customer_info">
	                    <form id="customer_info_form" method="post" action="{{ route('creditAppUpdate') }}">

	                    @csrf

	                    @if(!$credit_app)
	                    	<input type="hidden" class="status" name="status" value="1">
	                    	<input type="hidden" class="status" name="customer_id" value="{{$customer_credit->id}}">
	                    @else
							<input type="hidden" class="status" name="status" value="3">
							<input type="hidden" class="status" name="customer_id" value="{{$customer_credit->id}}">
							<input type="hidden" name="p_key" value="{{$credit_app->id}}">

							<input type="hidden" class="status_for_readOnly" value="{{$credit_app->status}}">
						@endif

	                      <div class="card customer_info_form_root_card" id="credit_app_top_card">
	                        <div class="card-header card-header-rose card-header-icon user_sub_card">
	                          	<div class="card-icon">
	                            	<i class="material-icons">people_outline</i>
	                          	</div>
	                          	<h4 class="card-title">Credit Application</h4>

			                    <div class="col-md-12 text-center">
			                      <div class="credit_app_id_badge text-right">
			                        <div class="timeline-heading">
			                          <h4 class="badge badge-pill"> CR-{{$customer_credit->id}} </h4>
			                        </div>
			                      </div>  
			                    </div>

	                        </div>

	                        <div style="margin-top: 15px;">
					  			@include('partials.messages')
							</div>

	                        <div class="card-body">
	                          <div class="row">
	                            <div class="col-md-1 progress_container customer_progress" style="text-align:center;margin-bottom:20px;">
	                              <span class="step_customer step-first">
	                                <i class="material-icons">done</i>
	                              </span>
	                              <span class="step_customer step-inner">
	                                <i class="material-icons">done</i>
	                              </span>
	                              <span class="step_customer step-inner">
	                                <i class="material-icons">done</i>
	                              </span>
	                              <span class="step_customer step-inner">
	                                <i class="material-icons">done</i>
	                              </span>
	                              <span class="step_customer step-inner">
	                                <i class="material-icons">done</i>
	                              </span>
	                              <span class="step_customer step-inner">
	                                <i class="material-icons">done</i>
	                              </span>                                    
	                            </div>

	                            <div class="col-md-11 content_container customer_info_container">

	                              <div class="tab_customer_info">
	                                <div class="card">
	                                  <div class="card-body">
	                                    <h4 class="sub-title">Basic Information</h4>
	                                    <div class="row input_row">
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="company_name" class="bmd-label-floating input_label">Company Name</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="company_name" name="company_name" value="@if(!$credit_app){{$customer_credit->companyName}}@else{{$credit_app->company_name}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="client_telephone" class="bmd-label-floating input_label">Telephone No</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill mobile" id="client_telephone" name="client_telephone" value="@if(!$credit_app){{$customer_credit->primaryPhone_freeFormNumber}}@else{{$credit_app->client_telephone}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="company_email" class="bmd-label-floating input_label">Email</label>
	                                          <input type="email" class="form-control form_input_field form_fields field_to_fill" id="company_email" name="company_email" value="@if(!$credit_app){{$customer_credit->primaryEmailAddr_addess}}@else{{$credit_app->company_email}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="client_contact_no" class="bmd-label-floating input_label">Contact</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="client_contact_no" name="client_contact_no" value="@if($credit_app){{$credit_app->client_contact_no}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="client_address" class="bmd-label-floating input_label">Address</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="client_address" name="client_address" value="@if(!$credit_app){{$customer_credit->billAddr_line1}}@else{{$credit_app->client_address}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="client_city" class="bmd-label-floating input_label">City</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="client_city" name="client_city" value="@if(!$credit_app){{$customer_credit->billAddr_city}}@else{{$credit_app->client_city}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="client_state" class="bmd-label-floating input_label">State</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="client_state" name="client_state" value="@if($credit_app){{$credit_app->client_state}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="client_zip" class="bmd-label-floating input_label">Zip</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="client_zip" name="client_zip" value="@if(!$credit_app){{$customer_credit->billAddr_postalCode}}@else{{$credit_app->client_zip}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="client_business_type" class="bmd-label-floating input_label">Type of Business</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="client_business_type" name="client_business_type" value="@if($credit_app){{$credit_app->client_business_type}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="client_business_date" class="bmd-label-floating input_label">Business Start Date</label>
	                                          <input type="text" class="datepicker form-control form_input_field form_fields field_to_fill datepicker" id="client_business_date" name="client_business_date" value="@if($credit_app){{$credit_app->client_business_date}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="client_years_address" class="bmd-label-floating input_label">Years at Address</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="client_years_address" name="client_years_address" value="@if($credit_app){{$credit_app->client_years_address}}@endif">
	                                        </div>
	                                      </div>
	                                      <!-- <div class="col-md-6 input_wrapper custom_select2_col">
	                                        <div class="form-group custom-form-select">
	                                          <select class="custom-select type_of_ownership select2 form_input_field field_to_fill" id="client_type" name="type_of_ownership">
	                                            <option class="form-select-placeholder"></option>
	                                            <option value="ceo">CEO</option>
	                                            <option value="owner">Owner</option>
	                                            <option value="partner">Partner</option>
	                                            <option value="proprietor">Proprietor</option>
	                                          </select>
	                                          <div class="form-element-bar">
	                                          </div>
	                                          <label class="form-element-label" for="type_of_ownership input_label">Type of ownership</label>
	                                        </div>
	                                      </div> -->
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="client_no_employee" class="bmd-label-floating input_label">No. Employees</label>
	                                          <input type="number" class="form-control form_input_field form_fields field_to_fill" id="client_no_employee" name="client_no_employee" value="@if($credit_app){{$credit_app->client_no_employee}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper custom_select2_col">
	                                        <div class="form-group custom-form-select">
	                                          <select class="custom-select type_of_organization select2 form_input_field field_to_fill" id="organization_type" name="type_of_organization">
	                                            <option class="form-select-placeholder"></option>
	                                            <option value="Corporation" @if($credit_app) @if($credit_app->type_of_organization == 'Corporation') selected @endif @endif >Corporation</option>
	                                            <option value="Partnership" @if($credit_app) @if($credit_app->type_of_organization == 'Partnership') selected @endif @endif>Partnership</option>
	                                            <option value="Individual Proprietor" @if($credit_app) @if($credit_app->type_of_organization == 'Individual Proprietor') selected @endif @endif>Individual Proprietor</option>
	                                            <option value="LLC" @if($credit_app) @if($credit_app->type_of_organization == 'LLC') selected @endif @endif>LLC</option>
	                                            <option value="Others" @if($credit_app) @if($credit_app->type_of_organization == 'Others') selected @endif @endif>Others</option>
	                                          </select>
	                                          <div class="form-element-bar">
	                                          </div>
	                                          <label class="form-element-label" for="organization_type input_label">Type of Organization</label>
	                                        </div>
	                                      </div>
	                                    </div>
	                                  </div>
	                                </div>
	                              </div>
	                              
	                              <div class="tab_customer_info">
	                                <div class="card">
	                                  <div class="card-body">
	                                    <h4 class="sub-title">Basic Information</h4>
	                                    <div class="row">
	                                      <!-- <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="client_reason_associate" class="bmd-label-floating input_label">Reason for Associates</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="client_reason_associate" name="client_reason_associate" value="">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="hours_per_week" class="bmd-label-floating input_label">Hours Per Week</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="hours_per_week" name="hours_per_week" value="">
	                                        </div>
	                                      </div> -->
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="party_charge_acts" class="bmd-label-floating input_label">Party in Charge of Acts Payable</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="party_charge_acts" name="party_charge_acts" value="@if($credit_app){{$credit_app->party_charge_acts}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper custom_select2_col">
	                                        <div class="form-group custom-form-select">
	                                          <select class="custom-select po_required select2 form_input_field field_to_fill" id="po_required" name="po_required">
	                                            <option class="form-select-placeholder"></option>
	                                            <option value="Yes" @if($credit_app) @if($credit_app->po_required == 'Yes') selected @endif @endif>Yes</option>
	                                            <option value="No" @if($credit_app) @if($credit_app->po_required == 'No') selected @endif @endif>No</option>
	                                          </select>
	                                          <div class="form-element-bar">
	                                          </div>
	                                          <label class="form-element-label" for="po_required input_label">PO Required</label>
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="terms_are" class="bmd-label-floating input_label">Terms are</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="terms_are" name="terms_are" value="@if($credit_app){{$credit_app->terms_are}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="person_accepting_term" class="bmd-label-floating input_label">Person Accepting Our Terms</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="person_accepting_term" name="person_accepting_term" value="@if($credit_app){{$credit_app->person_accepting_term}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="title" class="bmd-label-floating input_label">Title</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="title" name="title" value="@if($credit_app){{$credit_app->title}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper social_tax_section" >
	                                    @if($credit_app) 
	                                    	@if(strlen($credit_app->social_tax_number) == 4 || ($credit_app->social_tax_number == null)) 
	                                    		<?php 
	                                    			$checked = 'checked'; 
	                                    			$id = 'social_security'; 
	                                    			$placeholder = 'Social Security(Last 4 digit)';
												?>
											@else
												<?php 
	                                    			$checked = ''; 
	                                    			$id = 'tax_number'; 
	                                    			$placeholder = 'Tax ID Number(9 digit)';
												?>
	                                    	@endif 
	                                    @else 
	                                    	<?php 
	                                    		$checked = 'checked'; 
	                                    		$id = 'social_security';
	                                    		$placeholder = 'Social Security(Last 4 digit)'; 
											?> 
	                                    @endif
	                                      	<div class="togglebutton social_tax_toggle">
		                                        <label>
		                                            <input type="checkbox" {{$checked}}>
		                                            Tax Id Num.<span class="toggle"></span>soc.sec.
		                                        </label>
		                                     </div>
	                                       
	                                        <div class="form-group bmd-form-group tax_sec">
	                                          <label for="social_security" class="bmd-label-floating input_label">{{$placeholder}}</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="{{$id}}" name="social_tax_number" value="@if($credit_app){{$credit_app->social_tax_number}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="primary_bank_used" class="bmd-label-floating input_label">Primary Bank Used</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="primary_bank_used" name="primary_bank_used" value="@if($credit_app){{$credit_app->primary_bank_used}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="bank_telephone" class="bmd-label-floating input_label">Telephone</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill mobile" id="bank_telephone" name="bank_telephone" value="@if($credit_app){{$credit_app->bank_telephone}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="bank_representatative" class="bmd-label-floating input_label">Account Representative</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="bank_representatative" name="bank_representatative" value="@if($credit_app){{$credit_app->bank_representatative}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="types_of_account" class="bmd-label-floating input_label">Types of Account</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="types_of_account" name="types_of_account" value="@if($credit_app){{$credit_app->types_of_account}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="date_opened" class="bmd-label-floating input_label">Date Opened</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill datepicker" id="date_opened" name="date_opened" value="@if($credit_app){{$credit_app->date_opened}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group currency_form_group">
	                                          <label for="avg_checking_balance" class="bmd-label-floating input_label">Avg. Checking Balance</label>
	                                          <span class="currency_symbol">$</span>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input currency_input" id="avg_checking_balance" name="avg_checking_balance" value="@if($credit_app){{$credit_app->avg_checking_balance}}@endif">
	                                        </div>
	                                      </div>

	                                    </div>
	                                  </div>
	                                </div>
	                              </div>
	                              
	                              <div class="tab_customer_info">
	                                <div class="card">
	                                  <div class="card-body">
	                                    <h4 class="sub-title">Basic Information</h4>
	                                    <div class="row">
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group currency_form_group">
	                                          <label for="hi_balance" class="bmd-label-floating input_label">HI Balance</label>
	                                          <span class="currency_symbol">$</span>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input currency_input" id="hi_balance" name="hi_balance" value="@if($credit_app){{$credit_app->hi_balance}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group currency_form_group">
	                                          <label for="avg_balance" class="bmd-label-floating input_label">Avg. Balance</label>
	                                          <span class="currency_symbol">$</span>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input currency_input" id="avg_balance" name="avg_balance" value="@if($credit_app){{$credit_app->avg_balance}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="d&b_rating" class="bmd-label-floating input_label">D&B Rating</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="d&b_rating" name="db_rating" value="@if($credit_app){{$credit_app->db_rating}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper custom_select2_col">
	                                        <div class="form-group custom-form-select">
	                                          <select class="custom-select loan_outstanding select2 form_input_field field_to_fill" id="loan_outstanding" name="loan_outstanding">
	                                            <option class="form-select-placeholder"></option>
	                                            <option value="Secured" @if($credit_app) @if($credit_app->loan_outstanding == 'Secured') selected @endif @endif>Secured</option>
	                                            <option value="Unsecured" @if($credit_app) @if($credit_app->loan_outstanding == 'Unsecured') selected @endif @endif>Unsecured</option>
	                                          </select>
	                                          <div class="form-element-bar">
	                                          </div>
	                                          <label class="form-element-label" for="loan_outstanding input_label">Loan Outstanding</label>
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group currency_form_group">
	                                          <label for="current_loan_balance" class="bmd-label-floating input_label">Current Loan Balance</label>
	                                          <span class="currency_symbol">$</span>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input currency_input" id="current_loan_balance" name="current_loan_balance" value="@if($credit_app){{$credit_app->current_loan_balance}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group currency_form_group">
	                                          <label for="hi_balance2" class="bmd-label-floating input_label">HI Balance</label>
	                                          <span class="currency_symbol">$</span>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input currency_input" id="hi_balance2" name="hi_balance2" value="@if($credit_app){{$credit_app->hi_balance2}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group currency_form_group">
	                                          <label for="avg_balance2" class="bmd-label-floating input_label">Avg. Balance</label>
	                                          <span class="currency_symbol">$</span>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input currency_input" id="avg_balance2" name="avg_balance2" value="@if($credit_app){{$credit_app->avg_balance2}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper custom_select2_col">
	                                        <div class="form-group custom-form-select">
	                                          <select class="custom-select payment_history select2 form_input_field field_to_fill" id="payment_history" name="payment_history">
	                                            <option class="form-select-placeholder"></option>
	                                            <option value="Excellent" @if($credit_app) @if($credit_app->payment_history == 'Excellent') selected @endif @endif >Excellent</option>
	                                            <option value="Good" @if($credit_app) @if($credit_app->payment_history == 'Good') selected @endif @endif>Good</option>
	                                            <option value="Fair" @if($credit_app) @if($credit_app->payment_history == 'Fair') selected @endif @endif>Fair</option>
	                                            <option value="Poor" @if($credit_app) @if($credit_app->payment_history == 'Poor') selected @endif @endif>Poor</option>
	                                          </select>
	                                          <div class="form-element-bar">
	                                          </div>
	                                          <label class="form-element-label" for="payment_history input_label">Payment History</label>
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper custom_select2_col">
	                                        <div class="form-group custom-form-select">
	                                          <select class="custom-select bank_credit_rating select2 form_input_field field_to_fill" id="bank_credit_rating" name="bank_credit_rating">
	                                            <option class="form-select-placeholder"></option>
	                                            <option value="Excellent" @if($credit_app) @if($credit_app->bank_credit_rating == 'Excellent') selected @endif @endif>Excellent</option>
	                                            <option value="Good" @if($credit_app) @if($credit_app->bank_credit_rating == 'Good') selected @endif @endif>Good</option>
	                                            <option value="Fair" @if($credit_app) @if($credit_app->bank_credit_rating == 'Fair') selected @endif @endif>Fair</option>
	                                            <option value="Poor" @if($credit_app) @if($credit_app->bank_credit_rating == 'Poor') selected @endif @endif>Poor</option>
	                                          </select>
	                                          <div class="form-element-bar">
	                                          </div>
	                                          <label class="form-element-label" for="bank_credit_rating input_label">Bank Credit Rating</label>
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="sic_code" class="bmd-label-floating input_label">SIC Code</label>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="sic_code" name="sic_code" value="@if($credit_app){{$credit_app->sic_code}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-12 input_wrapper">
	                                        <div class="form-group bmd-form-group">
	                                          <label for="comments" class="bmd-label-floating input_label">Comments</label>
	                                          <textarea class="form-control form_input_field form_fields" rows="4" name="comments" id="comments" required>@if($credit_app){{$credit_app->comments}}@endif</textarea>
	                                        </div>
	                                      </div>

	                                    </div>
	                                  </div>
	                                </div>
	                              </div>

	                              <div class="tab_customer_info">
	                                <div class="card service_sup_card">
	                                  <div class="card-body">
	                                    
	                                      <h4 class="service_sup_title">Credit Reference <span>(Please Submit 3 Credit References)</span> </h4>
	                                      <fieldset class="customer_fieldset supplier_fieldset">
	                                        <legend>Reference <span> 1</span></legend>
	                                        <div class="row input_row">
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_name" class="bmd-label-floating input_label">Name of Suplier</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="suplier_name" name="cr_suplier_name1" value="@if($credit_app){{$credit_app->cr_suplier_name1}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="type_of_business" class="bmd-label-floating input_label">Type of Business</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="type_of_business" name="cr_type_of_business1" value="@if($credit_app){{$credit_app->cr_type_of_business1}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_addr" class="bmd-label-floating input_label">Address</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="suplier_addr" name="cr_suplier_addr1" value="@if($credit_app){{$credit_app->cr_suplier_addr1}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_contact" class="bmd-label-floating input_label">Contact</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="suplier_contact" name="cr_suplier_contact1" value="@if($credit_app){{$credit_app->cr_suplier_contact1}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_telephone" class="bmd-label-floating input_label">Telephone No</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill mobile" id="suplier_telephone" name="cr_suplier_telephone1" value="@if($credit_app){{$credit_app->cr_suplier_telephone1}}@endif">
	                                            </div>
	                                          </div>
	                                          <!-- <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_fax" class="bmd-label-floating input_label">Fax No</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill mobile" id="suplier_fax" name="cr_suplier_fax1" value="@if($credit_app){{$credit_app->cr_suplier_fax1}}@endif">
	                                            </div>
	                                          </div> -->
	                                        </div>
	                                      </fieldset>

	                                      <h4 class="service_sup_title">For EV Oilfield Services Use Only</h4>
	                                      <fieldset class="express_service evos_field">
	                                        <legend>Service #</legend>
	                                        <div class="row input_row">
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group currency_form_group">
	                                              <label for="credit_limit" class="bmd-label-floating input_label">Credit Limit</label>
	                                              <span class="currency_symbol">$</span>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input" id="credit_limit" name="cr_credit_limit1" value="@if($credit_app){{$credit_app->cr_credit_limit1}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="date_acct_opened" class="bmd-label-floating input_label">Date Account Open</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill datepicker" id="date_acct_opened" name="cr_date_acct_opened1" value="@if($credit_app){{$credit_app->cr_date_acct_opened1}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group currency_form_group">
	                                              <label for="accout_average" class="bmd-label-floating input_label">Account Average</label>
	                                              <span class="currency_symbol">$</span>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input" id="accout_average" name="cr_account_average1" value="@if($credit_app){{$credit_app->cr_account_average1}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group currency_form_group">
	                                              <label for="account_high" class="bmd-label-floating input_label">Account High</label>
	                                              <span class="currency_symbol">$</span>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input" id="account_high" name="cr_account_high1" value="@if($credit_app){{$credit_app->cr_account_high1}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="avg_day_pay" class="bmd-label-floating input_label">Average Day to Pay</label>
	                                              <input type="number" class="form-control form_input_field form_fields field_to_fill" id="avg_day_pay" name="cr_avg_day_pay1" value="@if($credit_app){{$credit_app->cr_avg_day_pay1}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="terms" class="bmd-label-floating input_label">Terms</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="terms" name="cr_terms1" value="@if($credit_app){{$credit_app->cr_terms1}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper custom_select2_col service_suplier_select">
	                                            <div class="form-group custom-form-select">
	                                              <select class="custom-select exp_service_rating select2 form_input_field field_to_fill" id="rating" name="cr_rating1">
	                                                <option class="form-select-placeholder"></option>
	                                                <option value="Excellent" @if($credit_app) @if($credit_app->cr_rating1 == 'Excellent') selected @endif @endif >Excellent</option>
	                                                <option value="Good" @if($credit_app) @if($credit_app->cr_rating1 == 'Good') selected @endif @endif>Good</option>
	                                                <option value="Fair" @if($credit_app) @if($credit_app->cr_rating1 == 'Fair') selected @endif @endif>Fair</option>
	                                                <option value="Poor" @if($credit_app) @if($credit_app->cr_rating1 == 'Poor') selected @endif @endif>Poor</option>
	                                              </select>
	                                              <div class="form-element-bar">
	                                              </div>
	                                              <label class="form-element-label" for="rating input_label">Rating</label>
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="express_comments" class="bmd-label-floating input_label">Comments</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="express_comments" name="cr_express_comments1" value="@if($credit_app){{$credit_app->cr_express_comments1}}@endif">
	                                            </div>
	                                          </div>
	                                        </div>
	                                      </fieldset>
	                                  </div>
	                                </div>
	                                
	                                <div class="card service_sup_card">
	                                  <div class="card-body">    
	                                      <h4 class="service_sup_title">Credit Reference <span>(Please Submit 3 Credit References)</span> </h4>
	                                      <fieldset class="customer_fieldset supplier_fieldset">
	                                        <legend>Reference <span> 2</span></legend>
	                                        <div class="row input_row">
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_name_1" class="bmd-label-floating input_label">Name of Suplier</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="suplier_name_1" name="cr_suplier_name2" value="@if($credit_app){{$credit_app->cr_suplier_name2}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="type_of_business_1" class="bmd-label-floating input_label">Type of Business</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="type_of_business_1" name="cr_type_of_business2" value="@if($credit_app){{$credit_app->cr_type_of_business2}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_addr_1" class="bmd-label-floating input_label">Address</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="suplier_addr_1" name="cr_suplier_addr2" value="@if($credit_app){{$credit_app->cr_suplier_addr2}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_contact_1" class="bmd-label-floating input_label">Contact</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="suplier_contact_1" name="cr_suplier_contact2" value="@if($credit_app){{$credit_app->cr_suplier_contact2}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_telephone_1" class="bmd-label-floating input_label">Telephone No</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill mobile" id="suplier_telephone_1" name="cr_suplier_telephone2" value="@if($credit_app){{$credit_app->cr_suplier_telephone2}}@endif">
	                                            </div>
	                                          </div>
	                                          <!-- <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_fax_1" class="bmd-label-floating input_label">Fax No</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill mobile" id="suplier_fax_1" name="cr_suplier_fax2" value="@if($credit_app){{$credit_app->cr_suplier_fax2}}@endif">
	                                            </div>
	                                          </div> -->
	                                        </div>
	                                      </fieldset>

	                                      <h4 class="service_sup_title">For EV Oilfield Services Use Only</h4>
	                                      <fieldset class="express_service evos_field">
	                                        <legend>Service #</legend>
	                                        <div class="row input_row">
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group currency_form_group">
	                                              <label for="credit_limit_1" class="bmd-label-floating input_label">Credit Limit</label>
	                                              <span class="currency_symbol">$</span>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input" id="credit_limit_1" name="cr_credit_limit2" value="@if($credit_app){{$credit_app->cr_credit_limit2}}@endif" >
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="date_acct_opened_1" class="bmd-label-floating input_label">Date Account Open</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill datepicker" id="date_acct_opened_1" name="cr_date_acct_opened2" value="@if($credit_app){{$credit_app->cr_date_acct_opened2}}@endif" >
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group currency_form_group">
	                                              <label for="accout_average_1" class="bmd-label-floating input_label">Account Average</label>
	                                              <span class="currency_symbol">$</span>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input" id="accout_average_1" name="cr_account_average2" value="@if($credit_app){{$credit_app->cr_account_average2}}@endif" >
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group currency_form_group">
	                                              <label for="account_high_1" class="bmd-label-floating input_label">Account High</label>
	                                              <span class="currency_symbol">$</span>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input" id="account_high_1" name="cr_account_high2" value="@if($credit_app){{$credit_app->cr_account_high2}}@endif" >
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="avg_day_pay_1" class="bmd-label-floating input_label">Average Day to Pay</label>
	                                              <input type="number" class="form-control form_input_field form_fields field_to_fill" id="avg_day_pay_1" name="cr_avg_day_pay2" value="@if($credit_app){{$credit_app->cr_avg_day_pay2}}@endif" >
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="terms_1" class="bmd-label-floating input_label">Terms</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="terms_1" name="cr_terms2" value="@if($credit_app){{$credit_app->cr_terms2}}@endif" >
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper custom_select2_col service_suplier_select">
	                                            <div class="form-group custom-form-select">
	                                              <select class="custom-select exp_service_rating select2 form_input_field field_to_fill" id="rating_1" name="cr_rating2" >
	                                                <option class="form-select-placeholder"></option>
	                                                <option value="Excellent" @if($credit_app) @if($credit_app->cr_rating2 == 'Excellent') selected @endif @endif>Excellent</option>
	                                                <option value="Good" @if($credit_app) @if($credit_app->cr_rating2 == 'Good') selected @endif @endif>Good</option>
	                                                <option value="Fair" @if($credit_app) @if($credit_app->cr_rating2 == 'Fair') selected @endif @endif>Fair</option>
	                                                <option value="Poor" @if($credit_app) @if($credit_app->cr_rating2 == 'Poor') selected @endif @endif>Poor</option>
	                                              </select>
	                                              <div class="form-element-bar">
	                                              </div>
	                                              <label class="form-element-label" for="rating_1 input_label">Rating</label>
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="express_comments_1" class="bmd-label-floating input_label">Comments</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="express_comments_1" name="cr_express_comments2" value="@if($credit_app){{$credit_app->cr_express_comments2}}@endif" >
	                                            </div>
	                                          </div>
	                                        </div>
	                                      </fieldset>
	                                    
	                                    </div>
	                                  </div>

	                                <div class="card service_sup_card">
	                                  <div class="card-body">
	                                      <h4 class="service_sup_title">Credit Reference <span>(Please Submit 3 Credit References)</span> </h4>
	                                      <fieldset class="customer_fieldset supplier_fieldset">
	                                        <legend>Reference <span> 3</span></legend>
	                                        <div class="row input_row">
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_name_2" class="bmd-label-floating input_label">Name of Suplier</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="suplier_name_2" name="cr_suplier_name3" value="@if($credit_app){{$credit_app->cr_suplier_name3}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="type_of_business_2" class="bmd-label-floating input_label">Type of Business</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="type_of_business_2" name="cr_type_of_business3" value="@if($credit_app){{$credit_app->cr_type_of_business3}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_addr_2" class="bmd-label-floating input_label">Address</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="suplier_addr_2" name="cr_suplier_addr3" value="@if($credit_app){{$credit_app->cr_suplier_addr3}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_contact_2" class="bmd-label-floating input_label">Contact</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="suplier_contact_2" name="cr_suplier_contact3" value="@if($credit_app){{$credit_app->cr_suplier_contact3}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_telephone_2" class="bmd-label-floating input_label">Telephone No</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill mobile" id="suplier_telephone_2" name="cr_suplier_telephone3" value="@if($credit_app){{$credit_app->cr_suplier_telephone3}}@endif">
	                                            </div>
	                                          </div>
	                                          <!-- <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="suplier_fax_2" class="bmd-label-floating input_label">Fax No</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill mobile" id="suplier_fax_2" name="cr_suplier_fax3" value="@if($credit_app){{$credit_app->cr_suplier_fax3}}@endif">
	                                            </div>
	                                          </div> -->
	                                        </div>
	                                      </fieldset>

	                                      <h4 class="service_sup_title">For EV Oilfield Services Use Only</h4>
	                                      <fieldset class="express_service evos_field">
	                                        <legend>Service #</legend>
	                                        <div class="row input_row">
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group currency_form_group">
	                                              <label for="credit_limit_2" class="bmd-label-floating input_label">Credit Limit</label>
	                                              <span class="currency_symbol">$</span>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input" id="credit_limit_2" name="cr_credit_limit3" value="@if($credit_app){{$credit_app->cr_credit_limit3}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="date_acct_opened_2" class="bmd-label-floating input_label">Date Account Open</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill datepicker" id="date_acct_opened_2" name="cr_date_acct_opened3" value="@if($credit_app){{$credit_app->cr_date_acct_opened3}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group currency_form_group">
	                                              <label for="accout_average_2" class="bmd-label-floating input_label">Account Average</label>
	                                              <span class="currency_symbol">$</span>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input" id="accout_average_2" name="cr_account_average3" value="@if($credit_app){{$credit_app->cr_account_average3}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group currency_form_group">
	                                              <label for="account_high_2" class="bmd-label-floating input_label">Account High</label>
	                                              <span class="currency_symbol">$</span>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input" id="account_high_2" name="cr_account_high3" value="@if($credit_app){{$credit_app->cr_account_high3}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="avg_day_pay_2" class="bmd-label-floating input_label">Average Day to Pay</label>
	                                              <input type="number" class="form-control form_input_field form_fields field_to_fill" id="avg_day_pay_2" name="cr_avg_day_pay3" value="@if($credit_app){{$credit_app->cr_avg_day_pay3}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="terms_2" class="bmd-label-floating input_label">Terms</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="terms_2" name="cr_terms3" value="@if($credit_app){{$credit_app->cr_terms3}}@endif">
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper custom_select2_col service_suplier_select">
	                                            <div class="form-group custom-form-select">
	                                              <select class="custom-select exp_service_rating select2 form_input_field field_to_fill" id="rating_2" name="cr_rating3">
	                                                <option class="form-select-placeholder"></option>
	                                                <option value="Excellent" @if($credit_app) @if($credit_app->cr_rating3 == 'Excellent') selected @endif @endif >Excellent</option>
	                                                <option value="Good" @if($credit_app) @if($credit_app->cr_rating3 == 'Good') selected @endif @endif>Good</option>
	                                                <option value="Fair" @if($credit_app) @if($credit_app->cr_rating3 == 'Fair') selected @endif @endif>Fair</option>
	                                                <option value="Poor" @if($credit_app) @if($credit_app->cr_rating3 == 'Poor') selected @endif @endif>Poor</option>
	                                              </select>
	                                              <div class="form-element-bar">
	                                              </div>
	                                              <label class="form-element-label" for="rating_2 input_label">Rating</label>
	                                            </div>
	                                          </div>
	                                          <div class="col-md-6 input_wrapper">
	                                            <div class="form-group bmd-form-group">
	                                              <label for="express_comments_2" class="bmd-label-floating input_label">Comments</label>
	                                              <input type="text" class="form-control form_input_field form_fields field_to_fill" id="express_comments_2" name="cr_express_comments3" value="@if($credit_app){{$credit_app->cr_express_comments3}}@endif">
	                                            </div>
	                                          </div>
	                                        </div>
	                                      </fieldset>
	                                    
	                                  </div>
	                                </div>

	                              </div>

	                              <div class="tab_customer_info">
	                                <div class="card">
	                                  <div class="card-body">
	                                    <h4 class="sub-title" style="margin-bottom: 25px !important;">Terms Agreement Built Into Credit Application</h4>
	                                    <div class="row input_row">
	                                      <div class="col-md-12 input_wrapper">
	                                        <p class="exp_content">The undersigned, in consideration of extending credit to aforesaid business, individually, jointly and severally as individuals unconditionally guarantee the payment of any and all future obligations of the said company which may be owing to EV Oilfield Services, LLC. upon demand including reasonable attorney’s fees and all costs and other expenses incurred by EV Oilfield Services, LLC. in collecting an indebtedness of the aforesaid customer. Notice is waived. This is a continuing guarantee. Should a lawsuit be necessary to enforce the guarantee, venue is waived and suit may be brought in The State of Texas. A photocopy or facsimile copy of the account application and signature shall be valid as an original thereof.All information given above is correct to the best of the undersigned's knowledge. It is agreed that:</p>
	                                        <ul class="customer_list">
	                                          <li>Charges for temporary help are labor related and due 10 days from the date of invoice</li>
	                                          <li>Creditor is authorized to investigate credit, banking and financial history and to disclose findings of that investigation as necessary.</li>
	                                        </ul>
	                                      </div>
	                                    </div>

	                               	@if($credit_app)
	                               	@if($credit_app->status != 1)
	                               	@if($customer_sign != null)
	                                    <!-- <fieldset class="custom_fieldset_2">
	                                      <legend>Signature Section</legend> -->
	                                    <fieldset class="custom_fieldset_2 signature_section_fieldset">
	                                      <legend>Signature Section</legend> 
	                                      <div class="row input_row mt-2" style="margin-bottom: 20px;">
	                                        <div class="col-md-12 draw_signature_col text-center">
	                                            <label class="" for="">Signature</label>
	                                            <!-- <br/> -->
	                                            <div style="border: 2px solid #DDD">
	                                            	<img src="{{$customer_sign->url}}">
	                                            </div>
	                                        </div>
	                                        <!-- <canvas id='blank' style='display:none'></canvas> -->
	                                      </div>
	                                      <div class="row">
	                                        <div class="col-md-5 input_wrapper sign_date_section mx-auto">
	                                          <div class="form-group bmd-form-group">
	                                            <label for="customer_sign_date" class="bmd-label-floating input_label">Date</label>
	                                            <input type="text" class="form-control form_input_field form_fields field_to_fill datepicker" id="customer_sign_date" name="customer_sign_date" value="@if($credit_app){{$credit_app->customer_sign_date}}@endif">
	                                          </div>
	                                        </div>
	                                      </div>
	                                    </fieldset>
	                                    <!-- </fieldset> -->
	                                    @endif
	                                    @endif
	                                    @endif


	                                  </div>
	                                </div>
	                              </div>

	                              <div class="tab_customer_info">
	                                <div class="card">
	                                  <div class="card-body">
	                                    <h4 class="sub-title">Credit Approve</h4>
	                                    <div class="row input_row evos_field">
	                                      <div class="col-md-6 input_wrapper">
	                                          <div class="form-group bmd-form-group">
	                                            <label for="approved_by" class="bmd-label-floating input_label">Approved By</label>
	                                            <input type="text" class="form-control form_input_field form_fields field_to_fill" id="approved_by" name="ca_approved_by" value="@if($credit_app){{$credit_app->ca_approved_by}}@endif">
	                                          </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                        <div class="form-group bmd-form-group currency_form_group">
	                                          <label for="credit_limit2" class="bmd-label-floating input_label">Credit Limit</label>
	                                          <span class="currency_symbol">$</span>
	                                          <input type="text" class="form-control form_input_field form_fields field_to_fill currency_field currency_input" id="credit_limit2" name="ca_credit_limit" value="@if($credit_app){{$cr_limit}}@endif">

	                                          <input type="hidden" class="cr_limit_for_chk" value="@if($credit_app){{$cr_limit}}@endif">
	                                        </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                          <div class="form-group bmd-form-group">
	                                            <label for="terms2" class="bmd-label-floating input_label">Terms</label>
	                                            <input type="text" class="form-control form_input_field form_fields field_to_fill" id="terms2" name="ca_terms" value="@if($credit_app){{$ca_terms}}@endif">
	                                          </div>
	                                      </div>
	                                      <!-- <div class="col-md-6 input_wrapper">
	                                          <div class="form-group bmd-form-group">
	                                            <label for="source_code" class="bmd-label-floating input_label">Source Code</label>
	                                            <input type="text" class="form-control form_input_field form_fields field_to_fill" id="source_code" name="ca_source_code" value="@if($credit_app){{$credit_app->ca_source_code}}@endif">
	                                          </div>
	                                      </div> -->
	                                      <div class="col-md-6 input_wrapper">
	                                          <div class="form-group bmd-form-group">
	                                            <label for="client_account_no" class="bmd-label-floating input_label">Customer No</label>
	                                            <input type="text" class="form-control form_input_field form_fields field_to_fill" id="client_account_no" name="ca_client_account_no" value="@if(!$credit_app){{$customer_credit->primaryPhone_freeFormNumber}}@else{{$credit_app->ca_client_account_no}}@endif">
	                                          </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                          <div class="form-group bmd-form-group">
	                                            <label for="completed_by" class="bmd-label-floating input_label">Completed By</label>
	                                            <input type="text" class="form-control form_input_field form_fields field_to_fill" id="completed_by" name="ca_completed_by" value="@if($credit_app){{$credit_app->ca_completed_by}}@endif">
	                                          </div>
	                                      </div>
	                                      <div class="col-md-6 input_wrapper">
	                                          <div class="form-group bmd-form-group">
	                                            <label for="checked_by" class="bmd-label-floating input_label">Checked By</label>
	                                            <input type="text" class="form-control form_input_field form_fields field_to_fill" id="checked_by" name="ca_checked_by" value="@if($credit_app){{$credit_app->ca_checked_by}}@endif">
	                                          </div>
	                                      </div>
	                                      <!-- <div class="col-md-6 input_wrapper">
	                                          <div class="form-group bmd-form-group">
	                                            <label for="cust_send_mail_to" class="bmd-label-floating input_label">Send Mail To</label>
	                                            <input type="text" class="form-control form_input_field form_fields field_to_fill cust_send_mail_to" id="cust_send_mail_to" name="cust_send_mail_to" value="">
	                                          </div>
	                                      </div> -->
	                                      <div class="col-md-6 input_wrapper">
	                                          <div class="form-group bmd-form-group">
	                                            <label for="date_3" class="bmd-label-floating input_label">Date</label>
	                                            <input type="text" class="form-control form_input_field form_fields field_to_fill datepicker" id="date_3" name="ca_date" value="@if($credit_app){{$credit_app->ca_date}}@endif">
	                                          </div>
	                                      </div>
	                                    </div>
	                  					<fieldset class="custom_fieldset_2 contractor_fieldset mt-5 mb-3">
	                                        <legend>Sending Information</legend>
	                                        <div class="row">
	                                          <div class="col-md-6">
	                                            <div class="form-group bmd-form-group">
	                                            <label for="agreement_send_mail_to" class="bmd-label-floating input_label">Send Mail To</label>
	                                              <input type="email" class="form-control form_input_field form_fields" id="customer_send_mail_to" name="customer_send_mail_to" value="@if(!$credit_app){{$customer_credit->primaryEmailAddr_addess}}@else{{$credit_app->customer_send_mail_to}}@endif" required="true" aria-required="true">
	                                            </div>                                          
	                                          </div>
	                                          <div class="col-md-6">
	                                            <div class="form-group bmd-form-group is-filled">
	                                            <label for="customer_send_mail_date" class="bmd-label-floating input_label">Sending Date</label>
	                                              <input type="text" class="datepicker form-control form_input_field form_fields" id="customer_send_mail_date" name="customer_send_mail_date" value="{{date('m/d/Y')}}" required="true" aria-required="true">
	                                            </div>                                          
	                                          </div>
	                                        </div>
	                                    </fieldset>
	                                  </div>
	                                </div>
	                              </div>
	                             
	                              <input type="hidden" name="submitted_by_email" value="@if($credit_app){{$credit_app->submitted_by_email}}@endif">
	                              <input type="hidden" name="submitted_by_name" value="@if($credit_app){{$credit_app->submitted_by_name}}@endif">

	                              <div style="overflow:auto;" class="row msf_btn_holder">
	                                <div class="col-md-4 save_btn_wrapper text-left" style="float:right;">
	                                  <a class="btn btn-round btn-warning nextPrevBtn prev" href="#credit_app_top_card" id="prevBtnCustomer" type="button" onclick="nextPrevCustomer(-1)">Previous</a>
	                                </div>
	                                <div class="col-md-8 save_btn_wrapper text-right">

	                                  <a class="btn custom-btn-one btn-round btn-success nextPrevBtn next" href="#credit_app_top_card" id="nextBtnCustomer" type="button" onclick="nextPrevCustomer(1)">Next</a>

	                                @if(!$credit_app)
	                                <button class="btn custom-btn-one btn-round btn-success nextPrevBtn submitBtnCustomer" id="" type="submit" onclick="">Submit</button>

	                                @elseif($credit_app->status == 2)
	                                  <button class="btn btn-round btn-danger  nextPrevBtnOne submitBtn nextBtnAgreement" type="button" id="customerRejecgBtn" onclick="" data-toggle="modal" data-target="#rejectModal">Reject</button>
	                                  <button class="btn custom-btn-one btn-round btn-success nextPrevBtn submitBtnCustomer" id="" type="submit" name="submit" value="first_submit" onclick="">Submit</button>
	                                @elseif($credit_app->status == 3)
	                                  <button class="btn custom-btn-one btn-round btn-success nextPrevBtn submitBtnCustomer" id="" type="submit" name="submit" value="cr_limit_history" onclick="">Submit</button>
	                                @endif

	                                </div>
	                              </div>

	                            </div>

	                          </div>

	                        </div>
	                      </div>

	                      <div class="modal fade bs_modal" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	                        <div class="modal-dialog" role="document">
	                          <div class="modal-content">
	                            <div class="modal-header">
	                              <h5 class="modal-title" id="exampleModalLabel"><u>Reason For The Rejection</u></h5>
	                              <button type="button" class="close close_icon" data-dismiss="modal" aria-label="Close">
	                                <span aria-hidden="true">&times;</span>
	                              </button>
	                            </div>
	                            <div class="modal-body">
	                              <textarea class="form-control summernote" style="min-width: 100%" rows="6" name="reject"></textarea>
	                            </div>
	                            <div class="modal-footer">
	                              <button type="button" class="btn" data-dismiss="modal">Close</button>
	                              <button type="submit" class="btn btn-primary" name="submit" value="reject">Submit</button>
	                            </div>
	                          </div>
	                        </div>
	                      </div>


	                    </form>
	                  </div>
                  	</div>
                </div>
              </div>



              </div>
            </div>
          </div>
        </div>


<script>

        

$(document).on('change', '.radio-input', function (e) {
    $('.is_default').val('0');
    $(this).parent().find('.is_default').val('1');
    
});

$(document).ready(function () {

////credit ref and approval block should be able to enter data once after recieving from customer end/////
if($('.status').val() == 1){
	$('.evos_field').find('input').attr('readonly', true);
	$('.evos_field').find('select').attr('disabled', true);
}
////credit ref and approval block should be able to enter data once after recieving from customer end/////

//// When changes credit limit after submission, all field will be reaonly except credit limit, sending info/////
if($('.status_for_readOnly').val() == 3){
	$('.customer_info_container').find('input, textarea').not('input[name=ca_credit_limit], input[name=ca_terms], input[name=customer_send_mail_to], input[name=customer_send_mail_date]').attr('readonly', true);
	$('.customer_info_container').find('select').attr('disabled', true);
}
//// When changes credit limit after submission, all field will be reaonly except credit limit, sending info/////


///check if credit limit field changes, if not cannot submit(cr limit history step)///
$( "form" ).submit(function () {
    if($(document.activeElement).val() == 'cr_limit_history'){
	 	if($( "input[name*='ca_credit_limit']" ).val() == $('.cr_limit_for_chk').val()){
	    	alert('Please change credit limit if you want to submit');
	    	return false;
	    }
    	return true;
    }
});
///check if credit limit field changes, if not cannot submit(cr limit history step)///

$('.custom-select.select2').each(function(){

  if($(this).val()!=""){
    $(this).addClass('hasvalue')
  }

})

});


$('.custom-select.select2').on('change',function(){
  if($(this).val()!=""){
    $(this).addClass('hasvalue')
  }
})




  // SIGNATURE VALIDATION





function isCanvasBlank(canvas) {
    var blank = document.createElement('canvas');
    blank.width = canvas.width;
    blank.height = canvas.height;

    blank.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    return canvas.toDataURL() == blank.toDataURL();
}

// end SIGNATURE VALIDATION 



var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear').click(function(e) {
    e.preventDefault();
    sig.signature('clear');
    $("#signature64").val('');
    draw($(this));

    $('.sign_input_validation').val("0");
    if($('.sign_input_validation').val() == "0"){
      $('.sign_error_msg').show();
    }
    else{
      $('.sign_error_msg').hide();
    }



    // $('canvas').attr('width','100');
});


$(document).ready(function(){
  if($('.sc_sign').length > 0){
    var canvas = $('.kbw-signature').find('canvas')[0];
    const context = canvas.getContext("2d");

    const img = new Image()
    img.src = $('.sc_sign').val()
    img.onload = () => {
      context.drawImage(img, 0, 0)
    }
  }

});

$('#sig canvas').mousedown(function(){
  $(this).parents('#sig').siblings('.sign_input_validation').val("1");
    if($('.sign_input_validation').val() == "0"){
      $('.sign_error_msg').show();
    }
    else{
      $('.sign_error_msg').hide();
    }
})
function draw(btn) {

  var canvas = btn.siblings('.kbw-signature').find('canvas')[0];
  var ctx = canvas.getContext("2d")
  //This line is actually not even needed...
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.rect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = 'white';
  ctx.fill();
}
$(document).ready(function(){
  $('canvas').attr('width','985.72');
})



$(document).ready(function() {
  $('.summernote').summernote({
  	toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']]
  ]
 });

});





</script>


@endsection