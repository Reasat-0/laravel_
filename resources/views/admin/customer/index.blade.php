@extends('layouts.master')

@section('content')

<!-- Change -->
  <div class="content manage-customer-content">
        
          <div class="container-fluid ">
            <div class="row">
              <div class="col-md-12 mx-auto">
                <div class="card customer_root_card">
                  <div class="card-header card-header-tabs card-header-rose custom_card_header mang_customer_header_card">
                    <div class="nav-tabs-navigation">
                      <div class="nav-tabs-wrapper">
                        <p class="nav-tabs-title">Manage Customers</p>
                        <ul class="nav nav-tabs" data-tabs="tabs">
                          <li class="nav-item">
                            <a class="nav-link active" href="#manual" data-toggle="tab">
                              <i class="material-icons">code</i> Manual
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                          <!-- <li class="nav-item">
                            <a class="nav-link active" href="#quickbooks" data-toggle="tab">
                              <i class="material-icons">bug_report</i> Quickbooks
                              <div class="ripple-container"></div>
                            </a>
                          </li> -->
                        </ul>
                      </div>
                    </div>
                  </div>
<!-- Change -->
                  <div class="card-body manage-customer-content-card-body">
                    <div class="tab-content">
                      
                      <div class="tab-pane active inventory_sub_card" id="manual">
                        <div class="card">
                          <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                              <i class="material-icons">assignment</i>
                            </div>
                            <h4 class="card-title">Manual</h4>
                          </div>
                          <div class="card-body">
                            <div class="toolbar">
                            </div>
                            <div class="material-datatables customers-datable-manual" style="overflow-x: hidden; overflow-y: hidden;">
                              <table id="all_customer_table_manual" class="table table-striped table-no-bordered table-hover all_table" cellspacing="0" width="100%" style="width:100%; table-layout: fixed;">
                                <thead>
                                  <tr>
                                    <th style="max-width: 45px;">Account No</th>
                                    <th>Display Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Credit Limit</th>
                                    <th>Available Amount</th>
                                    <th>Credit App. Status</th>
                                    <th>Sent</th>
                                    <th>Seen</th>
                                    <th class="disabled-sorting text-right" style="min-width: 95px;" >Actions</th>
                                  </tr>
                                </thead>

                                <tbody>

                                @foreach($customers_manual as $rowdata)
                                <?php
                                  $credit_app = $rowdata->credit_app;
                                  if($credit_app){
                                    $sent_json = json_decode($credit_app->sent);
                                    $sent = $sent_json ? end($sent_json) : null;
                                    $seen_json = json_decode($credit_app->seen);
                                    $seen = $seen_json ? end($seen_json) : null;
                                  }
                                ?>

                                  <tr>
                                    <td>{{preg_replace('/[^A-Za-z0-9\-]/', '', strtok($rowdata->displayName, " "))}}-EVOS-{{$rowdata->id}}</td>
                                    <td>{{$rowdata->displayName}}</td>
                                    
                                    <td>{{$rowdata->primaryPhone_freeFormNumber}}</td>
                                    <td>{{$rowdata->primaryEmailAddr_addess}}</td>

                                    <td>
                                      <div class="crdit_limit_cross">
                                        @if($rowdata->credit_app_creation == 1)
                                          @if($credit_app)
                                            
                                            @if($credit_app->ca_credit_limit != null)
                                              ${{$credit_app->ca_credit_limit}}
                                            @else
                                              <span class="btn btn-danger btn-link btn-just-icon remove">
                                                <i class="material-icons">cancel</i>
                                              </span>
                                            @endif

                                          @else
                                            <span class="btn btn-danger btn-link btn-just-icon remove">
                                              <i class="material-icons">cancel</i>
                                            </span>
                                          @endif

                                        @else
                                          <span class="btn btn-danger btn-link btn-just-icon remove">
                                            <i class="material-icons">cancel</i>
                                          </span>
                                        @endif
                                      </div>
                                    </td>

                                    <td>
                                      <div class="crdit_limit_cross">
                                        <span class="btn btn-danger btn-link btn-just-icon remove">
                                          <i class="material-icons">cancel</i>
                                        </span>
                                      </div>
                                    </td>

                                    <td>
                                      <!-- <span class="ins_add">Submitted</span>
                                      <span class="ins_delete">Rejected</span>
                                      <span class="ins_amend">Sent To Customer</span> -->
                                      <!-- <span class="ins_amend">Pending For Review</span> -->
                                      @if($rowdata->credit_app_creation == 1)
                                        
                                        @if($credit_app)
                                          @if($credit_app->status == 1)
                                            <span class="ins_amend">Sent To Customer</span>
                                          @elseif($credit_app->status == 2)
                                            <span class="ins_amend">Pending For Review</span>
                                          @elseif($credit_app->status == 3)
                                            <span class="ins_add">Submitted</span>
                                          @elseif($credit_app->status == 4)
                                            <span class="ins_delete">Rejected</span>
                                          @endif
                                        @else
                                          <span class="ins_delete">Not sent yet</span>
                                        @endif

                                      @else
                                         <div class="tooltip_td"> 
                                          <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                        </div>

                                      @endif

                                    </td>

                                    <td>
                                      @if($rowdata->credit_app_creation == 0)
                                                  <div class="tooltip_td"> 
                                                  <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                                  </div>

                                                @else
                                                
                                                  @if(!$credit_app)
                                                    <div class="tooltip_td"> 
                                                    <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                                    </div>
                                                  @else    
                                                    <div class="tooltip_td"> 

                                                    @if($credit_app->sent == null)
                                                        <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                                    @else  

                                                    <span>{{ \Carbon\Carbon::parse($sent)->timezone('America/New_York')->format('m/d/y g:i A') }}</span>
                                                    @if(count($sent_json) > 1)
                                                    <div class="container_tooltip">
                                                      <div class="custom_tooltip">
                                                        <div class="tooltip_content">
                                                          @foreach($sent_json as $row)
                                                          <span>{{ \Carbon\Carbon::parse($row)->timezone('America/New_York')->format('m/d/y g:i A') }} </span>
                                                          @endforeach
                                                        </div>
                                                        <div class="triangle"></div>
                                                      </div>
                                                      <a href="#" class="tooltip_link"><i class="material-icons">info</i></a>
                                                    </div>
                                                    @endif

                                                    </div>
                                                    @endif
                                                  @endif
                                            @endif
                                    </td>
                                    
                                    <td>
                                      @if($rowdata->credit_app_creation == 0)
                                                  <div class="tooltip_td"> 
                                                  <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                                  </div>

                                                @else
                                                
                                                  @if(!$credit_app)
                                                    <div class="tooltip_td"> 
                                                    <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                                    </div>
                                                  @else    
                                                    <div class="tooltip_td">

                                                    @if($credit_app->seen == null)
                                                        <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                                    @else                 
                                                    <span>{{ \Carbon\Carbon::parse($seen)->timezone('America/New_York')->format('m/d/y g:i A') }}</span>
                                                    @if(count($seen_json) > 1)
                                                    <div class="container_tooltip">
                                                      <div class="custom_tooltip">
                                                        <div class="tooltip_content">
                                                          @foreach($seen_json as $row)
                                                          <span>{{ \Carbon\Carbon::parse($row)->timezone('America/New_York')->format('m/d/y g:i A') }} </span>
                                                          @endforeach
                                                        </div>
                                                        <div class="triangle"></div>
                                                      </div>
                                                      <a href="#" class="tooltip_link"><i class="material-icons">info</i></a>
                                                    </div>
                                                    @endif

                                                    </div>
                                                    @endif
                                                  @endif
                                            @endif
                                    </td>
                                    
                                    <td class="text-right">
                                      
                                      <form action="{{ route('customers.destroy',$rowdata->id) }}" method="POST">
                                          <a href="{{ route('customers.edit',$rowdata->id) }}" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a>
                                       
                                          @csrf
                                          @method('DELETE')
                          
                                          <button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm('Are you sure you want to delete?');"><i class="material-icons">close</i></button>
                                      </form>
                                    
                                    </td>
                                  </tr>
                                @endforeach
                                
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>              
                


                
              </div>



              </div>
            </div>
        
      </div>


<script>
  function insertQbCustomer(){
    $.ajax({
                      url:"{{ route('insertQbCustomer') }}",
                      method:"GET",
                      cache: false,
                      beforeSend: function(){
                          $('#loading_text').show();
                          $('#loader').show();
                      },
                      complete: function(){
                          $('#loading_text').hide();
                          $('#loader').hide();
                      },
                      success:function(result)
                      {
                       //$(".all_table").load(location.href + " .all_table");
                       location.reload();
                       //$('#qbCustomer').html(result);
                      }
                   })
  }

  function discardQbCustomer(){
    $.ajax({
                      url:"{{ route('discardQbCustomer') }}",
                      method:"GET",
                      cache: false,
                      beforeSend: function(){
                          $('#loading_text').show();
                          $('#loader').show();
                      },
                      complete: function(){
                          $('#loading_text').hide();
                          $('#loader').hide();
                      },
                      success:function(result)
                      {
                       //$(".all_table").load(location.href + " .all_table");
                       location.reload();
                       //$('#qbCustomer').html(result);
                      }
                   })
  }


  $(window).on('load', function(){
    
    $('.customers-datable table thead th:last-child').css('width','60px');
    $('.customers-datable-manual table thead th:last-child').css('width','60px'); 
    
    setTimeout(function() {
       $('.material-datatables').css('overflow-x','unset');
       $('#all_customer_table').css('table-layout','unset');
       $('#all_customer_table_manual').css('table-layout','unset');
       $('.customers-datable table thead th:last-child').css('min-width','60px');
       $('.customers-datable-manual table thead th:last-child').css('min-width','60px'); 
    
       
    }, 100);
  });


</script>
@endsection