<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Credit Application</title>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
body{
	font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
}
.request_id_badge{
	background: #f6cb61;
	width:140px;
	height: 30px;
	line-height: 1.3;
	border-radius: 0;
	text-align: center;
	color:white;
	border-radius: 50%;
	position: fixed;
	right: -30px;
	top: -30px;
	border: 1px solid #000;
	border-radius: 15px;
}
.request_id_badge h4{
	padding: 6px 5px;
	font-size:12px;
	font-weight: 300;
	margin: 0;
	text-transform: uppercase;
	color: #000;
}
.insurance_title_secton{
	text-align: center;
}

footer {
    position: absolute; 
    bottom: -30px; 
    left: 0; 
    right: 0;
    color: white;
    height: 45px;
    text-align: center;
} 
.footer-section-table{
	display: table;
	width: 100%;
	vertical-align: middle;
	table-layout: fixed;
}
.footer-section-row{
	display: table-row;
}
.footer-part{
	display: table-cell;
	height: 45px;
	vertical-align: middle;
}
.footer-part img{
	height: 30px;
	display: block;
	text-align: left;
}
.footer-part p{
	margin: 0;
	color: #676767;
	font-size: 12px;
	line-height: 1.3;
}
.insurance_logo img{
	position: fixed;
	top: -30px;
	left: 40%;
	height: 80px;
	transform: translate(-40%, 0);
}
.insurance_title_secton{
	margin-top: 60px;
}
.main_header h2{
	text-align: center;
	margin-top: 50px;
}
.inline-input-field{
	padding: 0 5px;
	display: inline-block;
}
p{
	font-size: 14px;
	line-height: 1.6;
}
.info-form-section .row-input {
	margin: 0;
	position: relative;
}
.info-form-section .row-input input{
	margin-top:5px; 
	width:100%;
	outline: none;
	border:none; 
	border-bottom:1px solid black;
	font-size:13px;
	height: 18px;
	margin-bottom: 5px;
	display: inline-block;
}
.crdit-applicaton-table{
	width: 100%;
}
.crdit-applicaton-table tr td{
	width: 50%;
}
.custom-fieldset{
	margin: 15px 0 10px 0;
	border-radius: 4px;
}
table{
	margin-top: 5px;
	padding: 2px;
}
.custom-fieldset legend{
	font-weight: bold;
}
.credit-app-list{
  padding-left: 30px;
}
.credit-app-list li{
    font-size: 14px;
    line-height: 1.5;
    margin-bottom: 10px;
    font-weight: bold;
}
.row-input > p{
	margin-bottom: 0;
	position: absolute;
	top: -13px;
	font-size: 12.5px;
	letter-spacing: 0.4px;
	left: 2px;
	font-weight: bold;
}
.insurance_input_field{
	max-width: 150px;
}
.sub-title{
	display: inline-block;
	text-transform: capitalize;
	text-decoration: underline;
	margin-bottom: 0px;
}
.inline-input-field{
	border: none;
	outline: none;
	border-bottom: 1px solid black;
}
.registration_list li{
	margin-bottom: 15px;
}
.registration_list li span:first-child{
	margin-left: 5px;
}
.registration_list li input{
	width: 80px;
	border: 0;
	border-bottom: 1px solid #000;
}
.info-form-section a{
	word-wrap: break-word;
	font-size: 14px;
	margin-top: -10px;
	display: inline-block;
}
.registration_list li input{
	display: inline-block;
	padding-bottom: 0px;
}
.credit-ref-row{
	border: 1px solid #000;
	padding: 10px;
	border-radius: 5px;
}
.row-input{
	position: relative;
}
/*.dollar{
	position: absolute;
	margin-top: 5px;
	display: inline-block;
}*/
.signature-row table tr:first-child td{
	text-align: center;
}
.signature-row table td img{
	max-height: 120px;
}
.signature-row table tr:last-child td{
	width: 50% !important;
	margin: 0 auto;
}

</style>
<body>
	<header>
		<div class="request_id_badge">
			<h4 class="title">CR-{{$credit_app->customer_id}}</h4>
		</div>
	    <div class="insurance_logo" >
			<img src="{{ public_path() . '/assets/img/logo.png' }}">
		</div>
	</header>
  
	<div class="main">
		<div class="main_header">
			<h2>Credit Application</h2>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">Basic Information</h3>
				<table class="crdit-applicaton-table">
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Company Name</p>
									<span><input type="text" value="{{$credit_app->company_name}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Telephone No</p>
									<span><input type="text" value="{{$credit_app->client_telephone}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Email</p>
									<span><input type="text" value="{{$credit_app->company_email}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Contact</p>
									<span><input type="text" value="{{$credit_app->client_contact_no}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Address</p>
									<span><input type="text" value="{{$credit_app->client_address}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> City</p>
									<span><input type="text" value="{{$credit_app->client_city}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> State</p>
									<span><input type="text" value="{{$credit_app->client_state}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Zip</p>
									<span><input type="text" value="{{$credit_app->client_zip}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Type of Business</p>
									<span><input type="text" value="{{$credit_app->client_business_type}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Business Start Date</p>
									<span><input type="text" value="{{$credit_app->client_business_date}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Years at Address</p>
									<span><input type="text" value="{{$credit_app->client_years_address}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> No. Employees</p>
									<span><input type="text" value="{{$credit_app->client_no_employee}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Type of Organization</p>
									<span><input type="text" value="{{$credit_app->type_of_organization}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Party in Charge of Accts Payable</p>
									<span><input type="text" value="{{$credit_app->party_charge_acts}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> PO Required</p>
									<span><input type="text" value="{{$credit_app->po_required}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Terms are</p>
									<span><input type="text" value="{{$credit_app->terms_are}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Person Accepting Our Terms</p>
									<span><input type="text" value="{{$credit_app->person_accepting_term}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Title</p>
									<span><input type="text" value="{{$credit_app->title}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Social security/Tax ID Number</p>
									<span><input type="text" value="{{$credit_app->social_tax_number}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Primary Bank Used</p>
									<span><input type="text" value="{{$credit_app->primary_bank_used}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Telephone</p>
									<span><input type="text" value="{{$credit_app->bank_telephone}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Account Representative</p>
									<span><input type="text" value="{{$credit_app->bank_representatative}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Types of Account</p>
									<span><input type="text" value="{{$credit_app->types_of_account}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Date Opened</p>
									<span><input type="text" value="{{$credit_app->date_opened}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Avg. Checking Balance</p>
									<span><input type="text" value="$ {{$credit_app->avg_checking_balance}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> HI Balance</p>
									<span><input type="text" value="$ {{$credit_app->hi_balance}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Avg. Balance</p>
									<span><input type="text" value="$ {{$credit_app->avg_balance}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> D&B Rating</p>
									<span><input type="text" value="{{$credit_app->db_rating}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Loan Outstanding</p>
									<span><input type="text" value="{{$credit_app->loan_outstanding}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Current Loan Balance</p>
									<span><input type="text" value="$ {{$credit_app->current_loan_balance}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> HI Balance</p>
									<span><input type="text" value="$ {{$credit_app->hi_balance2}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Avg. Balance</p>
									<span><input type="text" value="$ {{$credit_app->avg_balance2}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Payment History</p>
									<span><input type="text" value="{{$credit_app->payment_history}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Bank Credit Rating</p>
									<span><input type="text" value="{{$credit_app->bank_credit_rating}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> SIC Code</p>
									<span><input type="text" value="{{$credit_app->sic_code}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Comments</p>
									<span><input type="text" value="{{$credit_app->comments}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>

		<br><br><br><br><br>
		<div class="row credit-ref-row">
			<div class="column">
				<h4>Credit Reference <span>(Please Submit 3 Credit References)</span></h4>
				<fieldset class="custom-fieldset">
					<legend class="credit-legend">Reference <span>1</span></legend>
					<table class="crdit-applicaton-table">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Name of Suplier</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_name1}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Type of Business</p>
										<span><input type="text" value="{{$credit_app->cr_type_of_business1}}@" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Address</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_addr1}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Contact</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_contact1}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Telephone No</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_telephone1}}" name="" /></span>
									</div>
								</div>
							</td>
							<!-- <td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Fax No</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_fax1}}" name="" /></span>
									</div>
								</div>
							</td> -->
						</tr>
					</table>
				</fieldset>

				<!-- <h4>For EV Oilfield Services Use Only</h4>
				<fieldset class="custom-fieldset">
					<legend>Service #</legend>
					<table class="crdit-applicaton-table">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Credit Limit</p>
										<span><input type="text" value="$ {{$credit_app->cr_credit_limit1}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Date Account Open</p>
										<span><input type="text" value="{{$credit_app->cr_date_acct_opened1}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Account Average</p>
										<span><input type="text" value="$ {{$credit_app->cr_account_average1}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Account High</p>
										<span><input type="text" value="$ {{$credit_app->cr_account_high1}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Average Day to Pay</p>
										<span><input type="text" value="{{$credit_app->cr_avg_day_pay1}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Terms</p>
										<span><input type="text" value="{{$credit_app->cr_terms1}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Rating</p>
										<span><input type="text" value="{{$credit_app->cr_rating1}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Comments</p>
										<span><input type="text" value="{{$credit_app->cr_express_comments1}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</fieldset> -->
			</div>
		</div>

		<br><br><br><br><br>
		<div class="row credit-ref-row">
			<div class="column">
				<h4>Credit Reference <span>(Please Submit 3 Credit References)</span></h4>
				<fieldset class="custom-fieldset">
					<legend class="credit-legend">Reference <span>2</span></legend>
					<table class="crdit-applicaton-table">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Name of Suplier</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_name2}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Type of Business</p>
										<span><input type="text" value="{{$credit_app->cr_type_of_business2}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Address</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_addr2}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Contact</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_contact2}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Telephone No</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_telephone2}}" name="" /></span>
									</div>
								</div>
							</td>
							<!-- <td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Fax No</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_fax2}}" name="" /></span>
									</div>
								</div>
							</td> -->
						</tr>
					</table>
				</fieldset>

				<!-- <h4>For EV Oilfield Services Use Only</h4>
				<fieldset class="custom-fieldset">
					<legend>Service #</legend>
					<table class="crdit-applicaton-table">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Credit Limit</p>
										<span><input type="text" value="$ {{$credit_app->cr_credit_limit2}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Date Account Open</p>
										<span><input type="text" value="{{$credit_app->cr_date_acct_opened2}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Account Average</p>
										<span><input type="text" value="$ {{$credit_app->cr_account_average2}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Account High</p>
										<span><input type="text" value="$ {{$credit_app->cr_account_high2}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Average Day to Pay</p>
										<span><input type="text" value="{{$credit_app->cr_avg_day_pay2}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Terms</p>
										<span><input type="text" value="{{$credit_app->cr_terms2}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Rating</p>
										<span><input type="text" value="{{$credit_app->cr_rating2}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Comments</p>
										<span><input type="text" value="{{$credit_app->cr_express_comments2}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</fieldset> -->
			</div>
		</div>

		<br><br><br><br><br><br>
		<div class="row credit-ref-row">
			<div class="column">
				<h4>Credit Reference <span>(Please Submit 3 Credit References)</span></h4>
				<fieldset class="custom-fieldset">
					<legend class="credit-legend">Reference <span>3</span></legend>
					<table class="crdit-applicaton-table">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Name of Suplier</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_name3}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Type of Business</p>
										<span><input type="text" value="{{$credit_app->cr_type_of_business3}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Address</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_addr3}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Contact</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_contact3}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Telephone No</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_telephone3}}" name="" /></span>
									</div>
								</div>
							</td>
							<!-- <td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Fax No</p>
										<span><input type="text" value="{{$credit_app->cr_suplier_fax3}}" name="" /></span>
									</div>
								</div>
							</td> -->
						</tr>
					</table>
				</fieldset>

				<!-- <h4>For EV Oilfield Services Use Only</h4>
				<fieldset class="custom-fieldset">
					<legend>Service #</legend>
					<table class="crdit-applicaton-table">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Credit Limit</p>
										<span><input type="text" value="$ {{$credit_app->cr_credit_limit3}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Date Account Open</p>
										<span><input type="text" value="{{$credit_app->cr_date_acct_opened3}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Account Average</p>
										<span><input type="text" value="$ {{$credit_app->cr_account_average3}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Account High</p>
										<span><input type="text" value="$ {{$credit_app->cr_account_high3}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Average Day to Pay</p>
										<span><input type="text" value="{{$credit_app->cr_avg_day_pay3}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Terms</p>
										<span><input type="text" value="{{$credit_app->cr_terms3}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Rating</p>
										<span><input type="text" value="{{$credit_app->cr_rating3}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Comments</p>
										<span><input type="text" value="{{$credit_app->cr_express_comments3}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</fieldset> -->
			</div>
		</div>

		<br><br>
		<div class="row" style="margin-top: 20px;">
			<div class="column">
				<h3 class="sub-title">Terms Agreement Built Into Credit Application</h3>
				<p>The undersigned, in consideration of extending credit to aforesaid business, individually, jointly and severally as individuals unconditionally guarantee the payment of any and all future obligations of the said company which may be owing to EV Oilfield Services, LLC. upon demand including reasonable attorney’s fees and all costs and other expenses incurred by EV Oilfield Services, LLC. in collecting an indebtedness of the aforesaid customer. Notice is waived. This is a continuing guarantee. Should a lawsuit be necessary to enforce the guarantee, venue is waived and suit may be brought in The State of Texas. A photocopy or facsimile copy of the account application and signature shall be valid as an original thereof.All information given above is correct to the best of the undersigned's knowledge. It is agreed that:</p>
				<ul class="credit-app-list">
					<li>Charges for temporary help are labor related and due 10 days from the date of invoice</li>
					<li>Creditor is authorized to investigate credit, banking and financial history and to disclose findings of that investigation as necessary.</li>
				</ul>
			</div>
		</div>

		<div class="row signature-row">
			<div class="column">
				<fieldset class="custom-fieldset">
					<legend class="credit-legend">Signature Section</legend>
					<table class="crdit-applicaton-table">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Signature</p>
										<span><img src="{{ public_path() . '/storage/files/'. $customer_sign->name }}" class="signature-img"></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>

							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Date</p>
										<span><input type="text" value="{{$credit_app->customer_sign_date}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</fieldset>
			</div>
		</div>

		<br><br>
		<div class="row" style="margin-top: 30px;">
			<div class="column">
				<h3 class="sub-title">Credit Approve</h3>
				<table class="crdit-applicaton-table">
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Approved By</p>
									<span><input type="text" value="{{$credit_app->ca_approved_by}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Credit Limit</p>
									<span><input type="text" value="$ {{$cr_limit}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Terms</p>
									<span><input type="text" value="{{$ca_terms}}" name="" /></span>
								</div>
							</div>
						</td>
						<!-- <td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Source Code</p>
									<span><input type="text" value="{{$credit_app->ca_source_code}}" name="" /></span>
								</div>
							</div>
						</td> -->
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Client Account No</p>
									<span><input type="text" value="{{$credit_app->ca_client_account_no}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Completed By</p>
									<span><input type="text" value="{{$credit_app->ca_completed_by}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Checked By</p>
									<span><input type="text" value="{{$credit_app->ca_checked_by}}" name="" /></span>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="info-form-section">
								<div class="row-input">
									<p> Date</p>
									<span><input type="text" value="{{$credit_app->ca_date}}" name="" /></span>
								</div>
							</div>
						</td>
						<td>
							
						</td>
					</tr>
				</table>
			</div>
		</div>



	</div>

	<footer>
	    <div class="footer-section-table">
	    	<div class="footer-section-row">
		    	<div class="footer-part footer-one">
		    		<img src="{{ public_path() . '/assets/img/logo.png' }}">
		    	</div>
		    	<div class="footer-part footer-two">
		    		<p>903 W. Industrial Ave. Midland, TX 79701</p>
		    	</div>
		    	<div class="footer-part footer-three">
		    		<p></i>432-253-9651</p>
		    	</div>
		    	<div class="footer-part footer-four">
		    		<p>info@evoilfieldservices.com</p>
		    	</div>
		    </div>
	    </div>
	</footer>

</body>
</html>
