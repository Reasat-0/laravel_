@extends('layouts.master')

@section('content')

            <div class="content add_inventory_content">
              <!-- Whole Container Food -->
              <div class="container-fluid">
                <div class="row">
                  <!-- Whole Card-->
                  <div class="col-md-12 mx-auto card_holder_box">
                    <div class="card add_inventory_root_card add_inv_service_root_card">
                      <!-- Whole Card Header --> 
                      <div class="card-header card-header-tabs card-header-rose custom_card_header inv_sub_card_img_zindex inventory_sub_card creat_inv_pro_card_header">
                        <div class="nav-tabs-navigation">
                          <div class="nav-tabs-wrapper">
                            <p class="nav-tabs-title">Add Inventory</p>
                            <ul class="nav nav-tabs" data-tabs="tabs" id="tabMenu">
                              <li class="nav-item">
                                <a class="nav-link" href="#profile" data-toggle="tab">
                                  <i class="material-icons">bug_report</i> Category
                                  <div class="ripple-container"></div>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#messages" data-toggle="tab">
                                  <i class="material-icons">code</i> Sub Category
                                  <div class="ripple-container"></div>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#variable" data-toggle="tab">
                                  <i class="material-icons">code</i> Variables
                                  <div class="ripple-container"></div>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#settings" data-toggle="tab">
                                  <i class="material-icons">cloud</i> Resources
                                  <div class="ripple-container"></div>
                                </a>
                              </li>

                              <!-- <li class="nav-item">
                                <a class="nav-link" href="#nli_form_tab_pane" data-toggle="tab">
                                  <i class="material-icons">cloud</i> NLI Form
                                  <div class="ripple-container"></div>
                                </a>
                              </li> -->
                            </ul>
                          </div>
                        </div>
                      </div>
                      <!-- Partial Message Div -->
                      <div style="margin-top: 15px;">
                        @include('partials.messages')
                      </div>
                      <!-- Whole Card Body -->
                      <div class="card-body">
                        <div class="tab-content">
                            <!-- Category Tab Pane -->
                            <div class="tab-pane" id="profile">
                              <div class="card card_modified">
                                <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card">
                                  <div class="card-text">
                                    <h4 class="card-title box_label_title">Add Category</h4>
                                  </div>
                                </div>
                                <div class="card-body ">
                                  <form method="post" action="{{ route('inventory.store') }}" class="form-horizontal">

                                    @csrf
                                    <input type="hidden" name="tab" value="category">
                                    <input type="hidden" name="type" value="inventory">

                                        <div class="form-group bmd-form-group">
                                          <label for="category_name" class="bmd-label-floating input_label"> Category Name *</label>
                                          <input type="text" class="form-control form_input_field" id="category_name" name="category_name" required="true" aria-required="true">
                                        </div>
                                        <div class="form-group bmd-form-group save_btn_wrapper text-right">
                                          <button type="submit" class="btn custom-btn-one save_btn">Save</button>
                                        </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            <!-- Sub Category Tab Pane -->
                            <div class="tab-pane sub_holder" id="messages">
                              <div class="card card_modified">
                                <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card">
                                  <div class="card-text">
                                    <h4 class="card-title box_label_title">Add Sub Category</h4>
                                  </div>
                                </div>
                                <div class="card-body ">
                                  <form method="post" action="{{ route('inventory.store') }}" class="form-horizontal">

                                    @csrf
                                    <input type="hidden" name="tab" value="subcategory">
                                    <input type="hidden" name="type" value="inventory">

                                        <div class="row">
                                          <div class="col-md-6 custom_select2_col">
                                            <div class="form-group custom-form-select">
                                              <select class="custom-select select2" id="category-custom-select" name="cat_id" required="true" aria-required="true">
                                                <option class="form-select-placeholder"></option>
                                                @foreach($cat as $rowdata)
                                                <option value="{{$rowdata->id}}">{{$rowdata->category_name}}</option>
                                                @endforeach
                                              </select>
                                              <div class="form-element-bar">
                                              </div>
                                              <label class="form-element-label" for="category-custom-select input_label">Category</label>
                                            </div>
                                          </div>
                                          <div class="col-md-6 input_wrapper">
                                            <div class="form-group bmd-form-group">
                                              <label for="category_name" class="bmd-label-floating input_label">Sub Category Name *</label>
                                              <input type="text" class="form-control form_input_field" id="sub_category_name" name="subcategory_name" required="true" aria-required="true">
                                            </div>
                                          </div>
                                        </div>

                                        <fieldset class="custom_fieldset_2">
                                        	<legend class="resource_para_label">Add Parameter for Resource</legend>
	                                        <div class="row input_row unit_parameter_row">
        												<div class="col-md-5 input_wrapper first">
        													<div class="form-group bmd-form-group">
        														<label for="unit_parameter_name" class="bmd-label-floating input_label">Name of parameter *</label>
        														<input type="text" class="form-control form_input_field" id="unit_parameter_name" name="unit_param_name[]" required="true" aria-required="true">
        													</div>
        												</div>
        												<div class="col-md-5 second input_wrapper custom_select2_col">
        													<div class="form-group  custom-form-select">
        													  <select class="custom-select unit_parameter_type select2 form_input_field" id="unit_data_type" name="unit_param_data_type[]" required="true" aria-required="true">
        													    <option class="form-select-placeholder"></option>
        													    <option value="string">String</option>
                                      <option value="integer">Integer</option>
        													    <option value="decimal">Decimal</option>
        													    <option value="datetime">DateTime</option>
        													    <option value="checkbox">Checkbox</option>
        													    <option value="vin_number">VIN Number</option>
        													    <option value="upc_number">UPC Number</option>
        													  </select>
        													  <div class="form-element-bar">
        													  </div>
        													  <label class="form-element-label" for="unit_data_type input_label">Data Type</label>
        													</div>
        												</div>
		                                          <div class="col-md-2 unit_parameter_cross text-right">
		                                              <a href="#" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">close</i><div class="ripple-container"></div></a>
		                                          </div>
	                                        </div>
	                                        <div class="row add_parameter_btn_row">
	                                            <div class="col-md-12 text-right">
	                                              <button class="btn btn-info btn-round btn-sm add_parameter_btn" type="button">
	                                                <span class="btn-label">
	                                                  <i class="material-icons">add</i>
	                                                </span>
	                                                Add Parameter
	                                                <div class="ripple-container"></div>
	                                              </button>
	                                            </div>
	                                        </div>
	                                    </fieldset>
                                      <div class="form-group bmd-form-group save_btn_wrapper text-right">
                                        <button type="submit" class="btn custom-btn-one save_btn">Save</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>


                            <div class="tab-pane sub_holder" id="variable">
                              <div class="card card_modified">
                                <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card">
                                  <div class="card-text">
                                    <h4 class="card-title box_label_title">Add Variables</h4>
                                  </div>
                                </div>
                                <div class="card-body variable_card_body_main_section">
                                  <form method="post" action="{{ route('inventory.store') }}" class="form-horizontal">

                                    @csrf
                                    <input type="hidden" name="tab" value="variable">
                                    <input type="hidden" name="type" value="inventory">

                                        <div class="row variable_first_row">
                                          <div class="col-md-5 input_wrapper">
                                            <div class="form-group bmd-form-group">
                                              <label for="variable_name" class="bmd-label-floating input_label">Name of Variable *</label>
                                              <input type="text" class="form-control form_input_field" id="variable_name" name="variable_name" required="true" aria-required="true">
                                            </div>
                                          </div>
                                        </div>

                                        <div class="row var_param_row">
                                          
                                          <div class="col-md-3 input_wrapper first">
                                            <div class="form-group bmd-form-group">
                                              <label for="parameter_name" class="bmd-label-floating input_label">Name of parameter *</label>
                                              <input type="text" class="form-control form_input_field" id="parameter_name" name="var_param_name[]" required="true" aria-required="true">
                                            </div>
                                          </div>

                                          <div class="col-md-2 second input_wrapper custom_select2_col">
                                            <div class="form-group custom-form-select">
                                              <select class="custom-select var_data_type select2 form_input_field" id="var_data_type" name="var_param_data_type[]" required="true" aria-required="true">
                                                <option class="form-select-placeholder"></option>
                                                <option value="decimal">Decimal</option>
                                                <option value="integer">Integer</option>
                                                <option value="datetime">DateTime</option>
                                                <option value="string">String</option>
                                                <option value="percentage">Percentage</option>
                                              </select>
                                              <div class="form-element-bar">
                                              </div>
                                              <label class="form-element-label" for="variable_data_type input_label">Data Type</label>
                                            </div>
                                          </div>

                                          <div class="col-md-3 d-flex align-items-center  var_param_value_wrapper input_wrapper">
                                            <div class="togglebutton admin-role-toggle priceclass_toggle_section">
                                                <label>
                                                  <input type="hidden" class="var_param_value_type" name="var_param_value_type[]" value="0">
                                                    <input class="var_param_toggle price_class_toggle" type="checkbox" name="" checked="">
                                                    Predefined <span class="toggle"></span> UserDefined
                                                </label>
                                            </div>
                                          </div>

                                          <div class="col-md-3 input_wrapper four">
                                            <div class="form-group bmd-form-group">
                                              <label for="parameter_name" class="bmd-label-floating input_label">Value </label>
                                              <input type="text" class="form-control form_input_field" id="var_param_value" name="var_param_value[]">
                                            </div>
                                          </div>

                                          <div class="col-md-1 var_cross_btn">
                                              <a href="#" class="btn btn-link btn-danger btn-just-icon remove disabled"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                                          </div>

                                        </div>

                                        <div class="row add_variable_btn_wrapper new_var_wrapper">
                                            <div class="col-md-12 text-right">
                                              <button class="btn btn-info btn-round btn-sm add_var_btn" type="button">
                                                <span class="btn-label">
                                                  <i class="material-icons">add</i>
                                                </span>
                                                Add Parameter
                                                <div class="ripple-container"></div>
                                              </button>
                                            </div>
                                        </div>



                                      <div class="form-group bmd-form-group save_btn_wrapper text-right">
                                        <button type="submit" class="btn custom-btn-one save_btn">Save</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>



                            <!-- Resource Tab Pane -->
                            <div class="tab-pane tab_pane_card" id="settings">
                              <div class="card">
                                <div class="card-header card-header-rose card-header-text lead_card_header user_sub_card">
                                  <div class="card-icon">
                                      <i class="material-icons">ballot</i>
                                  </div>
                                  <!-- <div class="card-text"> -->
                                    <h4 class="card-title">RESOURCE DETAILS</h4>
                                </div>
                                <div class="card-body">
                                  <form method="post" action="{{ route('inventory.store') }}" class="form-horizontal" enctype="multipart/form-data">

                                     @csrf
                                    <input type="hidden" name="tab" value="inventory">
                                    <input type="hidden" name="type" value="inventory">

                                      <!-- Add Resource Section -->
                                      <div class="card resource_general_section_card">
                                        <!-- Add resource Header -->
                                        <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card inv_sub_card_img_zindex">
                                          <div class="card-text">
                                            <h4 class="card-title box_label_title">General Section</h4>
                                          </div>
                                        </div>
                                        <!-- Add Resource Body -->
                                        <div class="card-body ">
                                          <input type="hidden" name="tab" value="inventory">
                                          <!-- Category and Sub Category Dropdown row -->
                                          <div class="row">
                                            <div class="col-md-6 input_wrapper custom_select2_col">
                                              <div class="form-group custom-form-select">
                                                <select class="custom-select cat select2" id="res-category-custom-select" name="cat_id">
                                                  <option class="form-select-placeholder"></option>
                                                  @foreach($cat as $rowdata)
                                                  <option value="{{$rowdata->id}}">{{$rowdata->category_name}}</option>
                                                  @endforeach
                                                </select>
                                                <div class="form-element-bar">
                                                </div>
                                                <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Category</label>
                                              </div>
                                            </div>
                                            <div class="col-md-6 input_wrapper custom_select2_col">
                                              <div class="form-group custom-form-select">
                                                <select class="custom-select subcat select2" id="sub-category-custom-select" name="subcat_id">   
                                                </select>
                                                <div class="form-element-bar">
                                                </div>
                                                <label class="form-element-label input_label top_select_input_label" for="sub-category-custom-select">Sub Category</label>
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Resource Name, Resource Code row -->
                                          <div class="row input_row">
                                            <div class="col-md-6 input_wrapper">
                                              <div class="form-group bmd-form-group">
                                                <label for="resource_name" class="bmd-label-floating input_label">Resource Name *</label>
                                                <input type="text" class="form-control form_input_field" id="resource_name" name="resource_name" required="true" aria-required="true" value="">
                                              </div>
                                            </div>
                                            <div class="col-md-6 input_wrapper">
                                              <div class="form-group bmd-form-group">
                                                <label for="resource_code" class="bmd-label-floating input_label">Resource Code *</label>
                                                <input type="text" class="form-control form_input_field" id="resource_code" name="resource_code" required="true" aria-required="true">
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Total Unit, Available Unit row-->
                                          <div class="row input_row">
                                            <div class="col-md-6 input_wrapper">
                                              <div class="form-group bmd-form-group">
                                                <label for="total_unit" class="bmd-label-floating input_label">Total Unit *</label>
                                                <input type="number" class="form-control form_input_field" id="total_unit" name="total_unit" required="true" aria-required="true" value="0" readonly="">
                                              </div>
                                            </div>
                                            <div class="col-md-6 input_wrapper">
                                              <div class="form-group bmd-form-group">
                                                <label for="available_unit" class="bmd-label-floating input_label">Available Unit *</label>
                                                <input type="number" class="form-control form_input_field" id="available_unit" name="available_unit" required="true" aria-required="true">
                                              </div>
                                            </div>
                                          </div>
                                          <div class="row input_row">
                                            <div class="col-md-6 input_wrapper">
                                              <div class="form-group bmd-form-group">
                                                <label for="allocated_unit" class="bmd-label-floating input_label">Allocated Unit *</label>
                                                <input type="number" class="form-control form_input_field" id="allocated_unit" name="allocated_unit" required="true" aria-required="true">
                                              </div>
                                            </div>
                                            <div class="col-md-6 input_wrapper">
                                              <div class="form-group bmd-form-group">
                                                <label for="model_num" class="bmd-label-floating input_label">Model Number *</label>
                                                <input type="text" class="form-control form_input_field" id="model_num" name="model_num" required="true" aria-required="true" step="0.01">
                                              </div>
                                            </div>
                                          </div>
                                          <!--Web short and long description row -->
                                          <div class="row input_row">
                                            <div class="col-md-6 input_wrapper">
                                              <div class="form-group bmd-form-group">
                                                <label for="web_short_des" class="bmd-label-floating input_label textarea_label">Web Short Description</label>
                                                <textarea class="form-control form_input_field" rows="4" name="web_short_des" id="web_short_des"></textarea>
                                              </div>
                                            </div>
                                            <div class="col-md-6 input_wrapper">
                                              <div class="form-group bmd-form-group">
                                                <label for="web_long_des" class="bmd-label-floating input_label textarea_label">Web Long Description</label>
                                                <textarea class="form-control form_input_field" rows="4" name="web_long_des" id="web_long_des"></textarea>
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Image row -->
                                          <div class="row">
                                              <div class="col-md-12 mx-auto">
                                                  <h4 class="title">Image</h4>
                                                  <div id="old_img"></div>
 
                                                  <div class="input-images1"></div>
                                                  <div id="genModal" class="inv_img_modal">
                                                    <span class="gen_close inv_img_close">×</span>
                                                    <img class="inv_modal_content" id="gen_modal_img">
                                                    <div id="caption"></div>
                                                  </div>
                                                
                                              </div>
                                          </div>
                                        
                                          
                                          
                                          <!-- IMage row -->
                                          
                                        </div>
                                      </div>
                                      
                                      <!-- Price class section -->

                                      <div class="card price_class_section_card">
                                        <div class="card-header card-header-rose card-header-text sub_card">
                                          <div class="card-text card_text_title">
                                              <h4 class="card-title">Price Type</h4>
                                          </div>
                                        </div>
                                        <div class="card-body">
                                          <div class="row resource_row price_class_row">
                                            <div class="col-md-3 justify-content-center d-flex align-items-center priceclass_toggle_div">
                                                <div class="togglebutton admin-role-toggle priceclass_toggle_section">
                                                    <label>
                                                        <input class="price_class_toggle" type="checkbox" name="" checked="">
                                                        Service <span class="toggle"></span> Rental
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-9 priceclass_toggle_content_div">
                                                <div class="row" id="service_price_class">
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="price_per_hour_service" class="bmd-label-floating input_label">Price Per Hour</label>
                                                      <input type="number" class="form-control form_input_field" id="price_per_hour_service" name="price_per_hour_service" required="true" aria-required="true" step="0.01">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="price_per_day_service" class="bmd-label-floating input_label">Price Per Day</label>
                                                      <input type="number" class="form-control form_input_field" id="price_per_day_service" name="price_per_day_service" step="0.01">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="price_per_week_service" class="bmd-label-floating input_label">Price Per Week</label>
                                                      <input type="number" class="form-control form_input_field" id="price_per_week_service" name="price_per_week_service" step="0.01">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="price_per_month_service" class="bmd-label-floating input_label">Price Per Month</label>
                                                      <input type="number" class="form-control form_input_field" id="price_per_month_service" name="price_per_month_service" step="0.01">
                                                    </div>
                                                  </div>
                                                </div>

                                                <div class="row" id="rental_price_class" style="display: none">
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="price_per_hour_rental" class="bmd-label-floating input_label">Price Per Hour</label>
                                                      <input type="number" class="form-control form_input_field" id="price_per_hour_rental" name="price_per_hour_rental" step="0.01">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="price_per_day_rental" class="bmd-label-floating input_label">Price Per Day</label>
                                                      <input type="number" class="form-control form_input_field" id="price_per_day_rental" name="price_per_day_rental" step="0.01">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="price_per_week_rental" class="bmd-label-floating input_label">Price Per Week</label>
                                                      <input type="number" class="form-control form_input_field" id="price_per_week_rental" name="price_per_week_rental" step="0.01">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-3 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="price_per_month_rental" class="bmd-label-floating input_label">Price Per Month</label>
                                                      <input type="number" class="form-control form_input_field" id="price_per_month_rental" name="price_per_month_rental" step="0.01">
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>


                                      <!-- Variable Section -->

                                      <div class="card">
                                        <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card inv_sub_card_img_zindex">
                                          <div class="card-text">
                                            <h4 class="card-title">Variable Section</h4>
                                          </div>
                                        </div>
                                        <div class="card-body variable_card_body variable_selection_card_body">
                                        
                                        <span id="var_tbl"></span>

                                          <div class="row top_variable_row input_row" id="var_row_create">
                                            <div class="col-md-6 input_wrapper custom_select2_col">
                                              <div class="form-group custom-form-select">
                                                <select class="custom-select varSelection select2 var_blank_it" id="variable_selection_dw" name="">
                                                  <option class="form-select-placeholder"></option>
                                                  @foreach($variables as $rowdata)
                                                  <option value="{{$rowdata->id}}">{{$rowdata->variable_name}}</option>
                                                  @endforeach
                                                </select>
                                                <div class="form-element-bar">
                                                </div>
                                                <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Select Variable</label>
                                              </div>
                                            </div>
                                            <div class="col-md-6 text-right variable_adder_btn_wrapper">
                                              <button class="btn btn-info btn-round btn-sm variable_adder_btn" type="button">
                                                <span class="btn-label">
                                                  <i class="material-icons">add</i>
                                                </span>
                                                Add
                                                <div class="ripple-container"></div>
                                              </button>                                              
                                            </div>
                                          </div>
                                          <div class="card product_quotaion_table_card variable_selection_table_card" style="display:none">
                                            <div class="card-body table_card_wrapper">
                                              <div class="card variable_table_header">
                                                <div class="quotation_table_section">
                                                  <div class="quotation_table_row">
                                                    <div class="var_col">
                                                      <p>Variable Name</p>
                                                    </div>
                                                    <div class="quote_col single_resource_main">
                                                      <div class="all_single_resounce">
                                                        <div class="single_resource_col">
                                                          <p>Parameters</p>
                                                        </div>
                                                        <div class="single_resource_col">
                                                         <p>Data Type</p>
                                                        </div>
                                                        <div class="single_resource_col">
                                                         <p>Status</p>
                                                        </div>
                                                        <div class="single_resource_col">
                                                         <p>Value</p>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>                                                
                                              </div>

                                              <div class="var_section">

                                              </div>

                                            </div>   

                                          </div>

                                        </div>
                                      </div>



                                      <!-- Unit Details Section -->
                                      <div class="card resource_unit_section_card">
                                       <!-- Unit Header -->
                                        <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card inv_sub_card_img_zindex">
                                          <div class="card-text">
                                            <h4 class="card-title">Unit Section</h4>
                                          </div>
                                        </div>
                                        <input type="hidden" name="prefix" id="prefix_input">
                                       <!-- Unit Body-->
                                        <div class="card-body unit_details create_inv_unit_card_body">
                                           <!-- Serial Name and UPC toggle and Number row -->
                                            <div class="row input_row unit_details_param">
                                              <div class="col-md-6 serial_name_wrapper input_wrapper">
                                  <div class="form-group bmd-form-group">
                                            <label for="inventory_name" class="bmd-label-floating input_label">Serial Name</label>
                                            <span class="serial_no_badge" style="display: none;">0</span>
                                            <span class="serial_no_badge_postfix" style="display: none;"></span>
                                            <span class="serial_no_badge_hyphen" style="display: none;"> - </span>
                                            <input type="text" class="form-control form_input_field serial_no_field" id="inventory_name" name="serial_name">
                                  </div>
                              </div>

                                            </div>
                                            <div class="row">
                                              <div class="col-md-12">
                                                  <h4 class="title">Image</h4>
                                                  <div class="input-images2"></div>
                                                
                                              </div>

                                              

                                            </div>
                                            
                                            
                                            
                                            
                                        </div>
                                      </div>

                                      
                                      

                                    <div class="form-group bmd-form-group save_btn_wrapper text-right">
                                      <button type="submit" class="btn custom-btn-one save_btn">Save</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                             </div>

                            <!-- NLI FORM  -->
                            <div class="tab-pane" id="nli_form_tab_pane">
                              <form class="nli_form" id="multiForm2">
                                <div class="card" id="top_nli_card">
                                  <div class="card-body">
                                    <div class="row four_form_header_row">
                                      <div class="col-md-12 text-center four_form_title_wrapper">
                                        <h2 class="main-title">MIDSTREAM LOSS / NEAR LOSS REPORT FORM (LI/NLI)</h2>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-5 col-form-label">Impact Incident ID</label>
                                      <div class="col-sm-3">
                                        <div class="form-group bmd-form-group">
                                          <input class="form-control" type="text" name="incident_id" email="true">
                                        </div>
                                      </div>
                                      <div class="col-md-12 text-center">
                                        <p class="include_top_para">( include after ID number assigned in IMPACT )</p>
                                      </div>
                                      <div class="col-md-12 text-center">
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" value=""> Alert
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" value=""> Bulletin
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-1 progress_container" style="text-align:center;margin-bottom:20px;">
                                        <span class="stepRen step-first">
                                          <i class="material-icons">done</i>
                                        </span>
                                        <span class="stepRen step-inner">
                                          <i class="material-icons">done</i>
                                        </span>
                                        <span class="stepRen step-inner">
                                          <i class="material-icons">done</i>
                                        </span>
                                        <span class="stepRen step-inner">
                                          <i class="material-icons">done</i>
                                        </span> 
                                        <span class="stepRen step-inner">
                                          <i class="material-icons">done</i>
                                        </span>
                                        <span class="stepRen step-inner">
                                          <i class="material-icons">done</i>
                                        </span>                                  
                                      </div>
                                      <div class="col-md-11 content_container rental_ag_content_container">
                                        <div class="tabRen">
                                          <div class="card nli_tab_main_card">
                                            <div class="card-header card-header-rose card-header-text sub_card">
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">General</h4>
                                              </div>
                                            </div>
                                            <div class="card-body sub_row">
                                              <div class="row bbs_checkbox_row">
                                                <div class="col-md-7 input_wrapper">
                                                  <div class="form-group bmd-form-group is-filled">
                                                    <label for="resource_code" class="bmd-label-floating input_label">Incident Title</label>
                                                    <input type="text" class="form-control form_input_field" id="incident_title" name="incident_title">
                                                  </div>
                                                </div>
                                                <div class="col-md-2 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_loss">Loss
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-3">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_loss">Near Loss
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group bmd-form-group is-filled">
                                                    <label for="resource_code" class="bmd-label-floating input_label">Responsible Department</label>
                                                    <input type="text" class="form-control form_input_field" id="responsible_dept" name="responsible_dept">
                                                  </div>
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group bmd-form-group is-filled">
                                                    <label for="resource_code" class="bmd-label-floating input_label">Responsible Superviser</label>
                                                    <input type="text" class="form-control form_input_field" id="responsible_supv" name="responsible_supv">
                                                  </div>
                                                </div>
                                              </div>
                                              <h4 class="sub-title">Location (Process Unit): (Check the one that applies) </h4>
                                              <div class="row input_row nli_checkbox_row">
                                              
                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="process_unit">Loading Rack
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="process_unit">Shop / Storage
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="process_unit">Marine Doc/ Pier
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>

                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="process_unit">Operation Control Center
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="process_unit">Office
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>

                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="process_unit">Pipeline Facility
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="process_unit">Pipeline Row
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="process_unit">Roadway
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>

                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="process_unit">Tank Farm
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="process_unit">Terminal
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group bmd-form-group is-filled">
                                                    <label for="resource_code" class="datepicker bmd-label-floating input_label">Date Occured</label>
                                                    <input type="text" class="form-control form_input_field" id="date_occured" name="date_occured">
                                                  </div>                                            
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group bmd-form-group is-filled">
                                                    <label for="resource_code" class="datepicker bmd-label-floating input_label">Date Reported</label>
                                                    <input type="text" class="form-control form_input_field" id="date_reported" name="date_reported">
                                                  </div>                                            
                                                </div>
                                                <div class="col-md-12">
                                                  <div class="form-group bmd-form-group is-filled">
                                                    <label for="resource_code" class="datepicker bmd-label-floating input_label">Time Reported</label>
                                                    <input type="text" class="form-control form_input_field" id="time_reported" name="time_reported">
                                                  </div>                                            
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group bmd-form-group is-filled">
                                                    <label for="resource_code" class="datepicker bmd-label-floating input_label">Reported By</label>
                                                    <input type="text" class="form-control form_input_field" id="reported_by" name="reported_by">
                                                  </div>                                            
                                                </div>
                                                <div class="col-md-3 custom_select2_col">
                                                  <div class="form-group custom-form-select">
                                                    <select class="custom-select type_of_ownership select2 form_input_field field_to_fill" id="shift" name="shift">
                                                      <option class="form-select-placeholder"></option>
                                                      <option value="day">Day</option>
                                                      <option value="night">NIght</option>
                                                    </select>
                                                    <div class="form-element-bar">
                                                    </div>
                                                    <label class="form-element-label" for="shift input_label">Shift</label>
                                                  </div>
                                                </div>
                                                <div class="col-md-3 shift_check_wrapper">
                                                  <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="shift_check"> 1
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="shift_check"> 2
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="shift_check"> 3
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                              </div>

                                            </div>
                                          </div>
                                        </div>

                                        <div class="tabRen">
                                          <div class="card nli_tab_main_card">
                                            <div class="card-header card-header-rose card-header-text sub_card"> 
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">General</h4>
                                              </div>
                                            </div>
                                            <div class="card-body sub_row">
                                              <h4 class="sub-title">Incident Description </h4>
                                              <div class="row input_row">
                                                <div class="col-md-12">
                                                  <h4 class="sub_child_title">Executive Summary (1-2 sentence summary Of incident) : </h4>
                                                  <hr class="sub_child_title_hr">
                                                  <p>While loading containments onto a trailer Marcus noticed an unsafe entailing the operations of a forklift while a worker was in the path of the truck.</p>
                                                </div>
                                                <div class="col-md-12">
                                                  <h4 class="sub_child_title">Incident Description (Max 1000 Characters): </h4>
                                                  <hr class="sub_child_title_hr">
                                                  <span class="sub_child_title_span">(Describe in detail WHAT happened, HOW it happened, and WHEN and WHERE in the process it happened): </span>
                                                  <p>Francisco Rodriguez was guiding the containment while Guillermo Sanchez was operating the forklift. Both employees failed to recognize the hazard of working in the path of a truck while it is not safely parked.</p>
                                                </div>
                                                <div class="col-md-12">
                                                  <h4 class="sub_child_title">Identify “Equivalent of Questionable Item(s)” For Near Loss/Loss.</h4>
                                                  <hr class="sub_child_title_hr">
                                                  <ul class="subcontractor_agre_list bold">
                                                    <li>
                                                      Safety while working around a forklift not in park.
                                                    </li>
                                                    <li>
                                                      Failure to identify at risk work behaviors
                                                    </li>
                                                    <li>
                                                      Failure to develop a language barrier plan prior to commencing work.
                                                    </li>
                                                  </ul>
                                                </div>
                                              </div>
                                              <h4 class="sub_child_title">Activity: (Check The One That Applies):</h4>
                                              <hr class="sub_child_title_hr">
                                              <div class="row nli_checkbox_row">
                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Ascending / Descending
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Bicycle Use
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Classroom / Meeting
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Communicating
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Computer/ Work Station
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Driving/Piloting/Riding
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Entering/Exiting/Equip.
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Handling Docs/Paperwork
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Handling Material/Chems
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Lifting/Handling manual
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Lifting/Handling Mech. Assist
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Operating Equip. Controls
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Reading/Interpret. Info
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Supervising/Directing Others
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Swimming/Diving
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Walking At Grade
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Working w/Cutting tools
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Working With Hands/No Tool
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Working With Tools
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Not Activity Related
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="tabRen">
                                          <div class="card nli_tab_main_card">
                                            <div class="card-header card-header-rose card-header-text sub_card">
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">General</h4>
                                              </div>
                                            </div>
                                            <div class="card-body sub_row">
                                                <h4 class="sub_child_title">Job Task: (Check the one that applies)  </h4>
                                                <hr class="sub_child_title_hr">
                                              <div class="row nli_checkbox_row">
                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Business Travel
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Calibration
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Carbon Change
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Carpentry/Woodwork
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Cleaning/Abrasive Blast
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Cleaning/Non Blasting
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Cleaning/ Tank
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Cleaning/Housekeep
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Cleaning/Hydroblasting
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Confined Space Entry
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Construction/Installation
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Control Room Ops
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Crane/Rigging/Lifting
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Demolition/Removal
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Dewatering
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>

                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Drilling/Workov/Wireline
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Earthmov/Excav./Trench
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Electrical Repair/Maint
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Energy Isolation/Control
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Environmental Remediat
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Equipment Purging
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Equipment Operation
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Equipment Start/Shut
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Equipment Open/Blind
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Food Prep/Handling
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Gauging/Samplings
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">GeoProbe/Direct Push
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Inspection
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Lab-General
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Local Operation
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>

                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Marine Operation
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Masonry/Concrete/Paving
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Mobile Rem/Vacuum Event
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Monitoring/Rounds
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">O & M
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Office Work
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Painting/Coating/Insulation
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Pavement Cutting
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Pigging
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Pipeline Operations
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Plumbing / Piping
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Quality Control /Testing
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Railcar / Loading / Unloading
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Repair/Mtce./Electrical
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Repair/Mtce./Schl./Routine
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>

                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Repair/Un Schl./NonRoutine
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Rigging
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Scaffold/Erection/Dismantle
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Subsurface Clearance
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Surveying
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Tank Truck load/Unload
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Transfer/Between/Tanks
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Transportation/Personnel
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Transportation/Equip/Mat
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Underwater work
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Vegetation Control/Landscape
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Waste Management
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Welding/Cutting/Brazing
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Work Permitting
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="job_task_check">Working at Heights
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>                                                
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="tabRen">
                                          <div class="card nli_tab_main_card">
                                            <div class="card-header card-header-rose card-header-text sub_card"> 
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">General</h4>
                                              </div>
                                            </div>
                                            <div class="card-body sub_row">
                                              <div class="row input_row">
                                                <div class="col-md-12">
                                                  <h4 class="sub_child_title">Phase Of Operation </h4>
                                                  <hr class="sub_child_title_hr">
                                                </div>
                                              </div>
                                              <div class="row nli_checkbox_row">
                                                <div class="col-md-4 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="phase_check">Commissioning/Startup
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="phase_check">Maintenance
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-4 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Emergency Prep/Response
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Normal Operations
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-md-4 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Projects
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="activity_check">Abnormal/Non-Routine Ops
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="card">
                                            <div class="card-header card-header-rose card-header-text sub_card"> 
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">SUBTYPES & DOMINANT INCIDENT TYPE</h4>
                                              </div>
                                            </div>
                                            <div class="card-body sub_row">
                                              <div class="row input_row">
                                                <div class="col-md-12">
                                                  <h4 class="sub_child_title">Subtypes: (Check all that apply) </h4>
                                                  <hr class="sub_child_title_hr">
                                                  <h4 class="sub_child_title">Dominant Incident Type: (Circle or underline one only)</h4>
                                                  <hr class="sub_child_title_hr">
                                                  <span class="sub_child_title_span">For each loss type in “Italic/Bold”, a consequence form most be completed and input in to Impact.</span>
                                                </div>
                                              </div>
                                              <div class="row nli_checkbox_row">
                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Adverse Weather
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label label_to_be_italic">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Business Interruption
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Contamination
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Cyber Security 
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Fatality
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label label_to_be_italic">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Fire/ Explosion
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                </div>

                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label label_to_be_italic">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Injury/ Illnesses
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Natural Disaster
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label label_to_be_italic">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Environmental
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Process Safety
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Product Quality
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                </div>

                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Product Stewardship Event
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label label_to_be_italic">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Property Equip./Damage
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Reliability
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Reputation
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label label_to_be_italic">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Security
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                </div>

                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Supply Interruption
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label label_to_be_italic">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Transportation of Commodities
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label label_to_be_italic">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Transportation of Personnel
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Worker Fatigue
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="dominent_check">Workplace Hazards
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                </div>
                                              </div>
                                              <div class="row input_row">
                                                <div class="col-md-12">
                                                  <h4 class="sub_child_title">Incident Flags (Check the one that applies)</h4>
                                                  <hr class="sub_child_title_hr">
                                                </div>
                                              </div>
                                              <div class="row nli_checkbox_row">
                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Air Patrol
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Alarm/Flood
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Alcohol Drug Test Conducted
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Contractor Incident 
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Employee Injury/Illness
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                </div>

                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Environmental Impact
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Inhibited Alarms
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Life Saving Rule
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">One Call Incident 
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Quality Incident
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Process Safety Incident
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                </div>

                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">COP (Critical Operating Parameter)
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Process Safety Near Miss – Demand on Safety System (DOSS)
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Process Safety Near Loss (Other)
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Process Safety Near Loss (Safe Oper Limit Exceedance (SOLE)
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                </div>

                                                <div class="col-md-3 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Hazard Loss –Stewardable (Fatality or > $1M Direct Cost )
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Hazard Loss Non Stewardable (i.e. Natural Disaster)
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Security Corporate Report
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="inc_flag_check">Standing Alarms
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>

                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                        </div>

                                        <div class="tabRen">
                                          <div class="card nli_tab_main_card">
                                            <div class="card-header card-header-rose card-header-text sub_card"> 
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">IRAT SCORE (Not required for MVAs & Damage Prevention LIs)</h4>
                                              </div>
                                            </div>
                                            <div class="card-body sub_row">
                                              <div class="row input_row">
                                                <div class="col-md-4 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="make_equip" class="bmd-label-floating input_label">Actual Consequence Level:</label>
                                                    <input type="text" class="form-control form_input_field form_fields" id="act_con_level" name="act_con_level" required="">
                                                  </div>
                                                </div>
                                                <div class="col-md-8 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="make_equip" class="bmd-label-floating input_label">Comments</label>
                                                    <input type="text" class="form-control form_input_field form_fields" id="act_con_level" name="act_con_level" required="">
                                                  </div>
                                                </div>
                                              </div>

                                              <div class="row input_row">
                                                <div class="col-md-4 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="make_equip" class="bmd-label-floating input_label">Potential Consequence Level:</label>
                                                    <input type="text" class="form-control form_input_field form_fields" id="act_con_level" name="act_con_level" required="">
                                                  </div>
                                                </div>
                                                <div class="col-md-8 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="make_equip" class="bmd-label-floating input_label">Comments</label>
                                                    <input type="text" class="form-control form_input_field form_fields" id="act_con_level" name="act_con_level" required="">
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="row input_row">
                                                <div class="col-md-12">
                                                  <h4 class="sub_child_title">IRAT Barriers: (Check all that applies) </h4>
                                                  <hr class="sub_child_title_hr">
                                                </div>
                                              </div>
                                              <div class="row nli_checkbox_row">
                                                <div class="col-md-4 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="irat_check">Human Intervention
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="irat_check">Safety Instrumented Systemss
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="irat_check">Safety valves & Rupture Discs
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>                                            
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="irat_check">Process Control Computer
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="irat_check">Post-release mitigation
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>

                                                <div class="col-md-4 input_wrapper">
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="irat_check">Personal Protective Equipment
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="irat_check">Technical Security Countermeasure
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="irat_check">Physical Security Countermeasures
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>                                            
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="irat_check">Security Post-Incident Mitigation
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                  <div class="form-check">
                                                    <label class="form-check-label">
                                                      <input class="form-check-input" type="checkbox" value="" name="irat_check">Other Independent Barriers
                                                      <span class="form-check-sign">
                                                        <span class="check"></span>
                                                      </span>
                                                    </label>
                                                  </div>
                                                </div>

                                                <div class="col-md-4">
                                                  <fieldset class="custom_fieldset irat_score_fieldsets">
                                                      <legend>IRAT Score </legend>
                                                      <div>
                                                        <span>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field" id="irat_score_one" name="irat_score_one">
                                                          </div>
                                                        </span>
                                                        <span> X </span>
                                                        <span>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field" id="irat_score_two" name="irat_score_two">
                                                          </div>
                                                        </span>
                                                      <span> = </span>
                                                        <span>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field" id="irat_score_two" name="irat_score_two">
                                                          </div>
                                                        </span>
                                                      </div>
                                                     
                                                  </fieldset>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="tabRen">  
                                          <div class="card">
                                            <div class="card-header card-header-rose card-header-text sub_card"> 
                                              <div class="card-text card_text_title">
                                                <h4 class="card-title">INVESTIGATION & SOLUTIONS</h4>
                                              </div>
                                            </div>
                                            <div class="card-body sub_row">
                                              <div class="row">
                                                <div class="col-md-6 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="make_equip" class="bmd-label-floating input_label">Date Investigation Started</label>
                                                    <input type="text" class="datepicker form-control form_input_field form_fields" id="investiation_start_date" name="investiation_start_date">
                                                  </div>
                                                </div>
                                                <div class="col-md-6 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="make_equip" class="bmd-label-floating input_label">Team Leader</label>
                                                    <input type="text" class="form-control form_input_field form_fields" id="investigation_team_leader" name="investigation_team_leader" required="">
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="row">
                                                <div class="table-responsive">
                                                  <table class="table investigation_table">
                                                    <thead class="">
                                                      <tr>
                                                        <th>
                                                        Team Memebers
                                                        </th>
                                                        <th>
                                                        Position
                                                        </th>
                                                        <th>
                                                        Company
                                                        </th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="investigation_team_member" name="investigation_team_member[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="investigation_team_position" name="investigation_team_position[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="investigation_team_company" name="investigation_team_company[]" required="">
                                                          </div>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="investigation_team_member" name="investigation_team_member[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="investigation_team_position" name="investigation_team_position[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="investigation_team_company" name="investigation_team_company[]" required="">
                                                          </div>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="investigation_team_member" name="investigation_team_member[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="investigation_team_position" name="investigation_team_position[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="investigation_team_company" name="investigation_team_company[]" required="">
                                                          </div>
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </div>
                                                <div class="col-md-12">
                                                  <span class="sub_child_title_span">
                                                    Solutions: 1 Complete the FRCS and answer ALL 7 factor questions; 2. Identify root cause(s) when answering NO to factors 1-4 by explaining why the “equivalent of a questionable item(s) occurred and circle root cause(s) when answering YES to Factors 5-7; and 3. Transfer the solution(s) that addresses each root cause following the solution guidance.
                                                  </span>
                                                </div>
                                              </div>

                                              <div class="row">
                                                <div class="table-responsive">
                                                  <table class="table frcs_table">
                                                    <thead class="">
                                                      <tr>
                                                        <th>
                                                        #
                                                        </th>
                                                        <th>
                                                        FRCS Factor #
                                                        </th>
                                                        <th>
                                                        Related OIMS System # (IRAT > 200)
                                                        </th>
                                                        <th>
                                                        Solution(s) (Transfer solutions from FRCS Form) 
                                                        </th>
                                                        <th>
                                                        Person Responsible
                                                        </th>
                                                        <th>
                                                        Comletion Target Date
                                                        (MM/DD/YYYY)
                                                        </th>
                                                        <th>
                                                        Comletion Actual Date
                                                        (MM/DD/YYYY)
                                                        </th>
                                                        <th>
                                                        FLS V&V Date
                                                        (MM/DD/YYYY)
                                                        </th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td>
                                                          1
                                                        </td>
                                                        <td>
                                                          1
                                                        </td>
                                                        <td>
                                                          1
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="trans_sol_frcs" name="trans_sol_frcs[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="person_responsible" name="person_responsible[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="target_date" name="target_date[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="actula_date" name="actula_date[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="v_and_v_date" name="v_and_v_date[]" required="">
                                                          </div>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>
                                                          2
                                                        </td>
                                                        <td>
                                                          2
                                                        </td>
                                                        <td>
                                                          2
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="trans_sol_frcs" name="trans_sol_frcs[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="person_responsible" name="person_responsible[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="target_date" name="target_date[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="actula_date" name="actula_date[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="v_and_v_date" name="v_and_v_date[]" required="">
                                                          </div>
                                                        </td>
                                                      </tr>

                                                      <tr>
                                                        <td>
                                                          3
                                                        </td>
                                                        <td>
                                                          3
                                                        </td>
                                                        <td>
                                                          3
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="trans_sol_frcs" name="trans_sol_frcs[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="form-control form_input_field form_fields" id="person_responsible" name="person_responsible[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="target_date" name="target_date[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="actula_date" name="actula_date[]" required="">
                                                          </div>
                                                        </td>
                                                        <td>
                                                          <div class="form-group bmd-form-group">
                                                            <input type="text" class="datepicker month_first form-control form_input_field form_fields" id="v_and_v_date" name="v_and_v_date[]" required="">
                                                          </div>
                                                        </td>
                                                      </tr>

                                                    </tbody>
                                                  </table>
                                                </div>
                                              </div>
                                              <div class="row"> 
                                                  <div class="col-md-12">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="result_of_fls" class="bmd-label-floating input_label">Results of FLS Solution Verification & Validation</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="result_of_fls" name="result_of_fls" required="" aria-required="true">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-12">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="recycle_comments" class="bmd-label-floating input_label">Recycle Comments</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="recycle_comments" name="recycle_comments" required="" aria-required="true">
                                                    </div>
                                                  </div> 
                                              </div>

                                              <div class="row"> 
                                                  <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="form_completed_by" class="bmd-label-floating input_label">Form Completed By</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="form_completed_by" name="form_completed_by" required="" aria-required="true">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="title" class="bmd-label-floating input_label">Title</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="title" name="title" required="" aria-required="true">
                                                    </div>
                                                  </div>

                                                  <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="date_prepared" class="bmd-label-floating input_label">Date Prepared</label>
                                                      <input type="text" class="datepicker form-control form_input_field form_fields" id="date_prepared" name="date_prepared" required="" aria-required="true">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="reviewer_name" class="bmd-label-floating input_label">Reviewer Name</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="reviewer_name" name="reviewer_name" required="" aria-required="true">
                                                    </div>
                                                  </div>

                                                  <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="approver_name" class="bmd-label-floating input_label">Approver Name</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="approver_name" name="approver_name" required="" aria-required="true">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="other" class="bmd-label-floating input_label">Other</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="other" name="other" required="" aria-required="true">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-12 text-center my-5">
                                                    <span class="footer_note_danger text-danger">Completed FRCS Form(s) must be attached to this report</span>
                                                  </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="row msf_btn_holder">
                                          <div class="col-md-6 save_btn_wrapper text-left" style="float:right;">
                                            <a class="btn btn-round btn-warning nextPrevBtn prev" href="#top_nli_card" id="prevBtnRen" type="button" onclick="nextPrevRen(-1)" style="display: inline;">Previous<div class="ripple-container"></div></a>
                                          </div>
                                          <div class="col-md-6 save_btn_wrapper text-right">
                                            <a class="btn custom-btn-one btn-round btn-success nextPrevBtn next" href="#top_nli_card" id="nextBtnRen" type="button" onclick="nextPrevRen(1)">Next<div class="ripple-container"></div></a>
                                            
                                            <button class="btn custom-btn-one btn-round btn-success nextPrevBtn submitBtnRen" type="submit" onclick="" style="">Submit<div class="ripple-container"></div></button>
                                          </div>

                                        </div>

                                      </div>
                                    </div>

                                  </div>
                                </div>
                              </form>
                            </div>


                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      
    <!-- ************************ -->
    <!--</div>-->


<script>
  $(document).ready(function() {

    $('.price_class_toggle').change(function() {
      //alert('okk');
      if (this.checked) {
          $('#service_price_class').show();
          $('#rental_price_class').hide();
      }
      else{
          $('#rental_price_class').show();
          $('#service_price_class').hide();
      }        
    });
    
    $( ".custom-select" ).change(function() {
      if(this.value.length > 0){
          $( this ).addClass( "hasvalue" );
      }
    })


    $('.cat').change(function(){
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getSubcategory') }}",
            method:"POST",
            data:{value:value, _token:_token, type:'inventory'},
            success:function(result)
            {
             $('.subcat').html(result);
            }
         })
      }
     });

    $('.subcat').change(function(){
      //alert($(this).val());
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getResourceInfoBySubcat') }}",
            context: this,
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result)
            {
              var obj = JSON.parse(result)
              //alert(obj[0].resource_name);
              //alert(obj[0].total_unit);
             if(obj[0].resource_name != null){
              $('#resource_name').parent().addClass('is-filled'); 
              $('#resource_name').attr('readonly', true);
             }
             else{
              $('#resource_name').parent().removeClass('is-filled'); 
              $('#resource_name').attr('readonly', false);
             }

             if(obj[0].resource_code != null){
              $('#resource_code').parent().addClass('is-filled'); 
              $('#resource_code').attr('readonly', true);
             }
             else{
              $('#resource_code').parent().removeClass('is-filled'); 
              $('#resource_code').attr('readonly', false);
             }

             /*if(obj[0].total_unit != null){
              $('#total_unit').parent().addClass('is-filled'); 
              $('#total_unit').attr('readonly', true);
             }
              else{
              $('#total_unit').parent().removeClass('is-filled'); 
              $('#total_unit').attr('readonly', false);
             }*/

             if(obj[0].available_unit != null){
              $('#available_unit').parent().addClass('is-filled'); 
              $('#available_unit').attr('readonly', true);
             }
             else{
              $('#available_unit').parent().removeClass('is-filled'); 
              $('#available_unit').attr('readonly', false);
             }

             if(obj[0].allocated_unit != null){
              $('#allocated_unit').parent().addClass('is-filled'); 
              $('#allocated_unit').attr('readonly', true);
             }
             else{
              $('#allocated_unit').parent().removeClass('is-filled'); 
              $('#allocated_unit').attr('readonly', false);
             }

             if(obj[0].price_per_hour_service != null){
              $('#price_per_hour_service').parent().addClass('is-filled'); 
              $('#price_per_hour_service').attr('readonly', true);
             }
             else{
              $('#price_per_hour_service').parent().removeClass('is-filled'); 
              $('#price_per_hour_service').attr('readonly', false);
             }

             if(obj[0].price_per_day_service != null){
              $('#price_per_day_service').parent().addClass('is-filled'); 
              $('#price_per_day_service').attr('readonly', true);
             }
             else{
              $('#price_per_day_service').parent().removeClass('is-filled'); 
              $('#price_per_day_service').attr('readonly', false);
             }

             if(obj[0].price_per_week_service != null){
              $('#price_per_week_service').parent().addClass('is-filled'); 
              $('#price_per_week_service').attr('readonly', true);
             }
             else{
              $('#price_per_week_service').parent().removeClass('is-filled'); 
              $('#price_per_week_service').attr('readonly', false);
             }

             if(obj[0].price_per_month_service != null){
              $('#price_per_month_service').parent().addClass('is-filled'); 
              $('#price_per_month_service').attr('readonly', true);
             }
             else{
              $('#price_per_month_service').parent().removeClass('is-filled'); 
              $('#price_per_month_service').attr('readonly', false);
             }

             if(obj[0].price_per_hour_rental != null){
              $('#price_per_hour_rental').parent().addClass('is-filled'); 
              $('#price_per_hour_rental').attr('readonly', true);
             }
             else{
              $('#price_per_hour_rental').parent().removeClass('is-filled'); 
              $('#price_per_hour_rental').attr('readonly', false);
             }

             if(obj[0].price_per_day_rental != null){
              $('#price_per_day_rental').parent().addClass('is-filled'); 
              $('#price_per_hday_rental').attr('readonly', true);
             }
             else{
              $('#price_per_day_rental').parent().removeClass('is-filled'); 
              $('#price_per_day_rental').attr('readonly', false);
             }

             if(obj[0].price_per_week_rental != null){
              $('#price_per_week_rental').parent().addClass('is-filled'); 
              $('#price_per_week_rental').attr('readonly', true);
             }
             else{
              $('#price_per_week_rental').parent().removeClass('is-filled'); 
              $('#price_per_week_rental').attr('readonly', false);
             }

             if(obj[0].price_per_month_rental != null){
              $('#price_per_month_rental').parent().addClass('is-filled'); 
              $('#price_per_month_rental').attr('readonly', true);
             }
             else{
              $('#price_per_month_rental').parent().removeClass('is-filled'); 
              $('#price_per_month_rental').attr('readonly', false);
             }

             if(obj[0].model_num != null){
              $('#model_num').parent().addClass('is-filled'); 
              $('#model_num').attr('readonly', true);
             }
             else{
              $('#model_num').parent().removeClass('is-filled'); 
              $('#model_num').attr('readonly', false);
             }

             if(obj[0].web_short_des != null){
              $('#web_short_des').parent().addClass('is-filled'); 
              $('#web_short_des').attr('readonly', true);
             }
             else{
              $('#web_short_des').parent().removeClass('is-filled'); 
              $('#web_short_des').attr('readonly', false);
             }

             if(obj[0].web_long_des != null){
              $('#web_long_des').parent().addClass('is-filled'); 
              $('#web_long_des').attr('readonly', true);
             }
             else{
              $('#web_long_des').parent().removeClass('is-filled'); 
              $('#web_long_des').attr('readonly', false);
             }

             if(obj[0].common_image != null){
              $('#inv_general_img').attr("src", obj[0].common_image);
              $('.img_change_btn').hide();
              $( "input[name*='common_image']" ).prop('required',false);
             }
             else{
              $('#inv_general_img').attr("src", "{{asset('assets/img/vendor_image.png')}}");
              $('.img_change_btn').show();
              $( "input[name*='common_image']" ).prop('required',true);
             }
             
//alert(obj[0].prefix);





             if(obj[0].prefix != null){
                //$("#prefix").show();
                $(".serial_no_badge").html(obj[0].prefix+1);
                $('#prefix_input').val(obj[0].prefix+1);
                $('#total_unit').val(obj[0].prefix+1);


              // Extra 
              var resource_code_val = $(this).parents('.resource_general_section_card').find('#resource_code').val();
              var serial_no =  $(this).parents('.resource_general_section_card').siblings('.resource_unit_section_card').find('.serial_no_badge');
              var serial_no_html = obj[0].prefix+1;
              serial_no.html(serial_no_html+ ' - '+resource_code_val)
             }
             else{
                //$("#prefix").show();
                // $(".serial_no_badge").text('1');
                $('#prefix_input').val('1');
                $('#total_unit').val(1);

              // Extra 
              var resource_code_val = $(this).parents('.resource_general_section_card').find('#resource_code').val();
              var serial_no =  $(this).parents('.resource_general_section_card').siblings('.resource_unit_section_card').find('.serial_no_badge');
              var serial_no_html = '1';
              serial_no.html(serial_no_html+ ' - '+resource_code_val)
             }
             
             $('#resource_name').val(obj[0].resource_name);
             $('#resource_code').val(obj[0].resource_code);
             //obj[0].total_unit == null ? $('#total_unit').val(1) : $('#total_unit').val(obj[0].total_unit + 1);
             $('#available_unit').val(obj[0].available_unit);
             $('#allocated_unit').val(obj[0].allocated_unit);

             $('#price_per_hour_service').val(obj[0].price_per_hour_service);
             $('#price_per_day_service').val(obj[0].price_per_day_service);
             $('#price_per_week_service').val(obj[0].price_per_week_service);
             $('#price_per_month_service').val(obj[0].price_per_month_service);
             $('#price_per_hour_rental').val(obj[0].price_per_hour_rental);
             $('#price_per_day_rental').val(obj[0].price_per_day_rental);
             $('#price_per_week_rental').val(obj[0].price_per_week_rental);
             $('#price_per_month_rental').val(obj[0].price_per_month_rantal);

             $('#model_num').val(obj[0].model_num);
             $('#web_short_des').val(obj[0].web_short_des);
             $('#web_long_des').val(obj[0].web_long_des);



            }
         })
      }




     });

$('.subcat').change(function(){
      //alert($(this).val());
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getVariablesBySubcat') }}",
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result)
            { 

              if(result != ''){
                $('.var_section').html(result);
                $('.variable_selection_table_card').show();
                $('#var_row_create').hide();
              }
              else{
                $('.variable_selection_table_card').hide();
                $('#var_row_create').show();
              }

              //console.log(result);
            }
         })
      }
     });

    $('.subcat').change(function(){
      //alert($(this).val());
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getImagesBySubcat') }}",
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result)
            { 
              if(result != ''){
                $('#old_img').html(result);
                $('#old_img').show();
                $('.input-images1').hide();
              }
              else{
                $('#old_img').hide();
                $('.input-images1').show();
                
              }
            }
         })
      }
     });


    $('.subcat').change(function(){
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getUnitDetailsBySubcat') }}",
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result)
            { 

              if(result != ''){
                $('.unit_details_param').html(result);

                $('.serial_no_badge').html($('#prefix_input').val());
                //alert($('#resource_code').val());
                //$('.serial_no_badge_postfix').show();
                $('.serial_no_badge_postfix').html($('#resource_code').val());
                //$('.variable_selection_table_card').show();
                //$('#var_row_create').hide();
              }
              else{
                //$('.variable_selection_table_card').hide();
                //$('#var_row_create').show();
              }

              //console.log(result);
            }
         })
      }
     });



    $('.u_id').change(function() {
      //alert('okk');
        if(this.checked) {
            $('.upc_div').show();
            $('.upc_btn_div').show();

            $('.vin_div').hide();
            $('.vin_btn_div').hide();

            $( "#vin" ).prop( "disabled", true );
            $( "#upc" ).prop( "disabled", false );
        }
        else{
            $('.vin_div').show();
            $('.vin_btn_div').show();

            $('.upc_div').hide();
            $('.upc_btn_div').hide();

            $( "#upc" ).prop( "disabled", true );
            $( "#vin" ).prop( "disabled", false );
        }        
    });

    $('body').on('click', '.upc_btn', function() {
    //$('.upc_btn').click(function() {
      let random_upc =  Math.floor(100000000000 + Math.random() * 900000000000);
      $('#upc').parent().addClass('is-filled'); 
      $('#upc').val(random_upc);
    });

    $('body').on('click', '.vin_btn', function() {
    //$('.vin_btn').click(function() {
      let random_vin =  rand_str();
      $('#vin').parent().addClass('is-filled'); 
      $('#vin').val(random_vin);
    });



    function rand_str() {
      const list = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
      var res = "";
      for(var i = 0; i < 17; i++) {
          var rnd = Math.floor(Math.random() * list.length);
          res = res + list.charAt(rnd);
      }
      return res;
    }

    var gen_modal       =       $('#genModal');

    var img   = $('#inv_general_img_wrapper #inv_general_img');
    var edit_page_dynamic_image_div   = $('#inv_general_img_wrapper .fileinput-preview.fileinput-exists.thumbnail');
                                          
      
      img.on('click', function(){

        if($('#inv_general_img').attr('src') !== "{{asset('assets/img/vendor_image.png')}}" ){

          gen_modal.show();
          $('#gen_modal_img').attr('src', $('#inv_general_img_wrapper #inv_general_img').attr('src'));


        }
        

      });

      edit_page_dynamic_image_div.click(function(){
        gen_modal.show();
        $('#gen_modal_img').attr('src', $('#inv_general_img_wrapper .fileinput-preview.fileinput-exists.thumbnail img').attr('src'));
      }); 


  });


$('.input-images1').imageUploader({imagesInputName:'images'});
$('.input-images2').imageUploader({imagesInputName:'images2'});


$('.var_param_toggle').change(function() {
    if(this.checked){
      $(this).parent().find('.var_param_value_type').val(0);
      $(this).parents('.var_param_row').find('#var_param_value').attr('readonly', false).val('');
    }
    else{
      $(this).parent().find('.var_param_value_type').val(1);
      $(this).parents('.var_param_row').find('#var_param_value').attr('readonly', true).val('');
    }

});

$('.var_data_type').change(function() {
    if($(this).val() == 'decimal'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'number').prop('step', '0.01');
    }
    else if($(this).val() == 'integer'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'number').prop('step', false);
    }
    else if($(this).val() == 'datetime'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'text').prop('step', false);
    }
    else if($(this).val() == 'string'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'text').prop('step', false);
    }
    else if($(this).val() == 'percentage'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'text').prop('step', false);
    }
});



$(document).on('click','.variable_adder_btn', function(){
  let var_id = $('.varSelection').val();
  let _token = $('input[name="_token"]').val();

  $.ajax({
            url:"{{ route('addVariableSection') }}",
            method:"POST",
            data:{var_id:var_id, _token:_token},
            success:function(result)
            { 
              $('.var_section').append(result);
              $('.variable_selection_table_card').show();
            }
         })

  $('.custom_select2_col select.var_blank_it').val(null).trigger('change');
  $('#variable_selection_dw').removeClass('hasvalue')
});


$(document).ready(function () {


if($('#tabMenu a[href="#{{ old('tab') }}"]').attr('href') == undefined){
    $('#tabMenu a[href="#profile"]').tab('show')  
}
else{
    $('#tabMenu a[href="#{{ old('tab') }}"]').tab('show')
}



// Making the serial_no visible or not
//$('.serial_no_field').on('input',function(){
$(document).on('input','.serial_no_field',function(){
  change_the_padding('.serial_no_badge','.serial_no_badge_postfix','.serial_no_badge_hyphen')
  $('.serial_no_badge_hyphen').show();
})

$(document).on('focus','.serial_no_field',function(){
  // Making the serial no visible or not
  $(this).siblings('.serial_no_badge').show();
  $(this).siblings('.serial_no_badge_postfix').show();
  change_the_padding('.serial_no_badge','.serial_no_badge_postfix','.serial_no_badge_hyphen')
})

$(document).on('blur','.serial_no_field',function(){
  // Making the serial no visible or not
  checking_the_input_field();
})

});

$(document).on('focus',".datepicker", function(){
    $(this).datetimepicker({
      format: 'MM/DD/YYYY',
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });

  $('.datepicker').parent().addClass('is-filled');
});


$('#resource_code').on('blur',function(){
  var resource_code_val = $(this).val();
  var serial_no =  $(this).parents('.resource_general_section_card').siblings('.resource_unit_section_card').find('.serial_no_badge_postfix');
  var serial_no_html = $(this).parents('.resource_general_section_card').siblings('.resource_unit_section_card').find('.serial_no_badge_postfix').html();

  serial_no.html(resource_code_val)
})




</script>
@endsection