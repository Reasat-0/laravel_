@extends('layouts.master')

@section('content')

          <div class="content edit_inventory_content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12 mx-auto card_holder_box">
                  
                  
                  <div class="card">
                    <div class="card-header card-header-tabs card-header-rose">
                      <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                          
                          <ul class="nav nav-tabs" data-tabs="tabs">
                            <li class="nav-item">
                              <a class="nav-link active" href="#profile" data-toggle="tab">
                                <i class="material-icons">ballot</i> Edit Inventory
                                <div class="ripple-container"></div>
                              </a>
                            </li>
                            
                          </ul>
                        </div>
                      </div>
                    </div>

                  <div style="margin-top: 15px;">
                      @include('partials.messages')
                  </div>

                  <div class="card-body">
                      <form method="post" action="{{ route('inventory.update', $inventory->id) }}" class="form-horizontal" enctype="multipart/form-data">

                      @csrf
                      @method('PUT')

                        <div class="card resource_general_section_card">
                          <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card inv_sub_card_img_zindex">
                            <div class="card-text">
                              <h4 class="card-title box_label_title">General Details</h4>
                            </div>
                          </div>

                          <div class="card-body increased_margin_t_cardbody_proincust">
                            
                              <input type="hidden" name="tab" value="inventory">
                              <input type="hidden" name="inventory_key" value="{{$inventory->id}}">

                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select hasvalue cat select2" id="category-custom-select" name="cat_id">
                                        <option class="form-select-placeholder"></option>
                                        @foreach($cat as $rowdata)
                                            <option value="{{$rowdata->id}}" @if($rowdata->id == $inventory->cat_id) selected @endif>{{$rowdata->category_name}}</option>
                                        @endforeach
                                      </select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Category</label>
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select hasvalue subcat select2" id="sub-category-custom-select" name="subcat_id">
                                        <option class="form-select-placeholder"></option>
                                        @foreach($subcat as $rowdata)
                                            <option value="{{$rowdata->id}}" @if($rowdata->id == $inventory->subcat_id) selected @endif>{{$rowdata->subcategory_name}}</option>
                                        @endforeach
                                      </select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label top_select_input_label" for="sub-category-custom-select">Sub Category</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row input_row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label for="resource_name" class="bmd-label-floating input_label">Resource Name *</label>
                                      <input type="text" class="form-control form_input_field" id="resource_name" name="resource_name" required="true" aria-required="true" value="{{$inventory->resource_name}}">
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label for="resource_code" class="bmd-label-floating input_label">Resource Code *</label>
                                      <input type="text" class="form-control form_input_field" id="resource_code" name="resource_code" required="true" aria-required="true" value="{{$inventory->resource_code}}">
                                    </div>
                                  </div>
                                </div>
                                <div class="row input_row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label for="total_unit" class="bmd-label-floating input_label">Total Unit *</label>
                                      <input type="number" class="form-control form_input_field" id="total_unit" required="true" aria-required="true" name="total_unit" value="@if($total_unit->count() != 0){{$total_unit->count()}}@else 1 @endif" readonly="">
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label for="avaliable_unit" class="bmd-label-floating input_label">Available Unit *</label>
                                      <input type="number" class="form-control form_input_field" id="available_unit" required="true" aria-required="true" name="available_unit" value="{{$inventory->available_unit}}">
                                    </div>
                                  </div>
                                </div>
                                <div class="row input_row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label for="allocated_unit" class="bmd-label-floating input_label">Allocated Unit *</label>
                                      <input type="number" class="form-control form_input_field" id="allocated_unit" required="true" aria-required="true" name="allocated_unit" value="{{$inventory->allocated_unit}}">
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label for="model_num" class="bmd-label-floating input_label">Model Number *</label>
                                      <input type="text" class="form-control form_input_field" id="model_num" name="model_num" required="true" aria-required="true" value="{{$inventory->model_num}}">
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="row input_row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group textarea_label_wrapper_div">
                                      <label for="web_short_des" class="bmd-label-floating input_label textarea_label">Web Short Description</label>
                                      <textarea class="form-control form_input_field" rows="4" name="web_short_des">{{$inventory->web_short_des}}</textarea>
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group textarea_label_wrapper_div">
                                      <label for="web_long_des" class="bmd-label-floating input_label textarea_label">Web Long Description</label>
                                      <textarea class="form-control form_input_field" rows="4" name="web_long_des">{{$inventory->web_long_des}}</textarea>
                                    </div>
                                  </div>
                                </div>
                               

                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="title">Image</h4>
                                        <div class="input-images edit_section_img"></div>
                                        <div id="genModal" class="inv_img_modal">
                                          <span class="gen_close inv_img_close">×</span>
                                          <img class="inv_modal_content" id="gen_modal_img">
                                          <div id="caption"></div>
                                        </div>
                                      
                                    </div>
                                </div>
                                
                           </div>
                        </div>



                        <!-- Price class section -->

                        <div class="card price_class_section_card">
                          <div class="card-header card-header-rose card-header-text sub_card">
                            <div class="card-text card_text_title">
                                <h4 class="card-title">Price Type</h4>
                            </div>
                          </div>
                          <div class="card-body">
                            <div class="row resource_row price_class_row">
                              <div class="col-md-3 justify-content-center d-flex align-items-center priceclass_toggle_div">
                                  <div class="togglebutton admin-role-toggle priceclass_toggle_section">
                                      <label>
                                          <input class="price_class_toggle" type="checkbox" name="" checked="">
                                          Service <span class="toggle"></span> Rental
                                      </label>
                                  </div>
                              </div>
                              <div class="col-md-9 priceclass_toggle_content_div">
                                  <div class="row" id="service_price_class">
                                    <div class="col-md-3 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="price_per_hour_service" class="bmd-label-floating input_label">Price Per Hour</label>
                                        <input type="number" class="form-control form_input_field" id="price_per_hour_service" name="price_per_hour_service" required="true" aria-required="true" step="0.01" value="{{$inventory->price_per_hour_service}}">
                                      </div>
                                    </div>
                                    <div class="col-md-3 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="price_per_day_service" class="bmd-label-floating input_label">Price Per Day</label>
                                        <input type="number" class="form-control form_input_field" id="price_per_day_service" name="price_per_day_service" step="0.01" value="{{$inventory->price_per_day_service}}">
                                      </div>
                                    </div>
                                    <div class="col-md-3 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="price_per_week_service" class="bmd-label-floating input_label">Price Per Week</label>
                                        <input type="number" class="form-control form_input_field" id="price_per_week_service" name="price_per_week_service" step="0.01" value="{{$inventory->price_per_week_service}}">
                                      </div>
                                    </div>
                                    <div class="col-md-3 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="price_per_month_service" class="bmd-label-floating input_label">Price Per Month</label>
                                        <input type="number" class="form-control form_input_field" id="price_per_month_service" name="price_per_month_service" step="0.01" value="{{$inventory->price_per_month_service}}">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row" id="rental_price_class" style="display: none">
                                    <div class="col-md-3 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="price_per_hour_rental" class="bmd-label-floating input_label">Price Per Hour</label>
                                        <input type="number" class="form-control form_input_field" id="price_per_hour_rental" name="price_per_hour_rental" step="0.01" value="{{$inventory->price_per_hour_rental}}">
                                      </div>
                                    </div>
                                    <div class="col-md-3 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="price_per_day_rental" class="bmd-label-floating input_label">Price Per Day</label>
                                        <input type="number" class="form-control form_input_field" id="price_per_day_rental" name="price_per_day_rental" step="0.01" value="{{$inventory->price_per_day_rental}}">
                                      </div>
                                    </div>
                                    <div class="col-md-3 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="price_per_week_rental" class="bmd-label-floating input_label">Price Per Week</label>
                                        <input type="number" class="form-control form_input_field" id="price_per_week_rental" name="price_per_week_rental" step="0.01" value="{{$inventory->price_per_week_rental}}">
                                      </div>
                                    </div>
                                    <div class="col-md-3 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="price_per_month_rental" class="bmd-label-floating input_label">Price Per Month</label>
                                        <input type="number" class="form-control form_input_field" id="price_per_month_rental" name="price_per_month_rental" step="0.01" value="{{$inventory->price_per_month_rental}}">
                                      </div>
                                    </div>
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <!-- Variable Section -->
                        <div class="card">
                          <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card inv_sub_card_img_zindex">
                            <div class="card-text">
                              <h4 class="card-title">Variable Section</h4>
                            </div>
                          </div>
                          <div class="card-body variable_card_body variable_selection_card_body">
                          
                          <span id="var_tbl"></span>

                            <div class="row top_variable_row input_row" id="var_row_create">
                              <div class="col-md-6 input_wrapper custom_select2_col">
                                <div class="form-group custom-form-select">
                                  <select class="custom-select varSelection select2 var_blank_it" id="variable_selection_dw">
                                    <option class="form-select-placeholder"></option>
                                    @foreach($variables as $rowdata)
                                    <option value="{{$rowdata->id}}">{{$rowdata->variable_name}}</option>
                                    @endforeach
                                  </select>
                                  <div class="form-element-bar">
                                  </div>
                                  <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Select Variable</label>
                                </div>
                              </div>
                              <div class="col-md-6 text-right">
                                <button class="btn btn-info btn-round btn-sm variable_adder_btn" type="button">
                                  <span class="btn-label">
                                    <i class="material-icons">add</i>
                                  </span>
                                  Add
                                  <div class="ripple-container"></div>
                                </button>                                              
                              </div>
                            </div>
                            <div class="card product_quotaion_table_card variable_selection_table_card">
                              <div class="card-body table_card_wrapper">
                                <div class="card variable_table_header">
                                  <div class="quotation_table_section">
                                    <div class="quotation_table_row">
                                      <div class="var_col">
                                        <p>Variable Name</p>
                                      </div>
                                      <div class="quote_col single_resource_main">
                                        <div class="all_single_resounce">
                                          <div class="single_resource_col">
                                            <p>Parameters</p>
                                          </div>
                                          <div class="single_resource_col">
                                           <p>Data Type</p>
                                          </div>
                                          <div class="single_resource_col">
                                           <p>Status</p>
                                          </div>
                                          <div class="single_resource_col">
                                           <p>Value</p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>                                                
                                </div>

                            <div class="var_section">
                              @foreach($variable_section as $rowdata)
                              <?php
                                $variable_row = App\Models\Variable::where('id', $rowdata->var_id)->first();
                              ?>
                                <div class="card single_card_for_single_service variable_single_card">
                                  <div class="quotation_table_main_section variable_table_main_section">
                                    <div class="quotation_table_section">
                                      <div class="quotation_table_row variable_table_row">
                                        <div class="quote_col">
                                          <input type="hidden" name="var_section_key[]" value="{{$rowdata->id}}">
                                          <input type="hidden" name="var_id[]" value="{{$rowdata->var_id}}">
                                          <p>{{$variable_row->variable_name}}</p>
                                        </div>
                                        <div class="quote_col single_resource_main">
                                        <?php $var_params = App\Models\VariableParameter::where('var_id', $rowdata->var_id)->get(); ?>
                                        @foreach($var_params as $rowdata)
                                          <div class="all_single_resounce">
                                            <div class="single_resource_col price_select_wrapper">
                                              <p>{{$rowdata->var_param_name}}</p>
                                            </div>
                                            <div class="single_resource_col">
                                              <p> {{$rowdata->var_param_data_type}} </p>
                                            </div>
                                            <div class="single_resource_col">
                                              <p> @if($rowdata->var_param_value_type == 0) PreDefined @else UserDefined @endif</p>
                                            </div>
                                            <div class="single_resource_col colTotal">
                                              {{$rowdata->var_param_value}}
                                            </div>
                                          </div>
                                        @endforeach
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="cross">
                                    <a href="#" class="btn btn-danger btn-round var_remove_btn">
                                      <i class="material-icons">close</i>
                                    </a>
                                  </div>
                                </div>
                              @endforeach
                            </div>

                              </div>                                            
                            </div>
                          </div>
                        </div>






                        <div class="card resource_unit_section_card">
                          <div class="card-header card-header-rose card-header-text sub_card">
                            <div class="card-text">
                              <h4 class="card-title">Unit Details</h4>
                            </div>
                          </div>
                          
                          <div class="card-body unit_details">
                            <div class="card search-add-unit-card" style="margin-top:10px!important;">
                              <div class="card-body">
                                <div class="row my-3">
                                  <div class="col-md-12 text-center">
                                    <button class="btn btn-info btn-sm btn-round add_unit_btn_new" type="button" unit_p_key="0" subcat_id="{{$inventory->subcat_id}}" unit_type="new">
                                      <span class="btn-label">
                                        <i class="material-icons">add</i>
                                      </span>
                                      Add Unit
                                    </button>
                                  </div>

                                  <div class="col-md-12 text-center add_a_new_unit_text ">
                                    <span >OR </span>
                                  </div>

                                  <div class="col-md-12 text-center add_a_new_unit_text ">
                                    <span class="text-info">All Existing Units Are Shown Below  </span>
                                  </div>                                
                                </div>                                
                              </div>
                            </div>

                            <div class="alert alert-success alert-dismissible unit_dlt_msg" role="alert" style="display: none;">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <strong>Success:</strong> Successfully Deleted
                            </div>


                          <div class="new_card">
                            <!-- Add new unit properties will append here -->    
                          </div>



                            <div class="card all_unit_list_view">
                              <div class="card-body">
                                <div class="table-responsive">
                                  <table class="table previous_unit_tbl">
                                    <thead>
                                      <tr>
                                        <th>Serial</th>
                                      @foreach($unit_params as $rowdata)
                                        <th>{{$rowdata->unit_param_name}}</th>
                                      @endforeach
                                        <th class="text-right" style="min-width: 70px">Actions</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <?php $sr=1; ?>
                                    @foreach($total_unit as $rowdata)
                                      <tr class="unit_list_tr">
                                        <td><span style="padding: 5px; background: #135882; color: white; margin-right: 3px; margin-bottom: 2px; display: inline-block;">{{$sr}}</span>{{$inventory->resource_code}} @if($rowdata->serial_name) - {{$rowdata->serial_name}}@endif</td>
                                        @foreach($unit_params as $param)
                                  
                                          <?php
                                          if($rowdata->properties != null){
                                            $arr_key = array_search($param->id, array_column($rowdata->properties, 'unit_param_id'));
                                            if($arr_key !== false){
                                              if($rowdata->properties[$arr_key]['type'] == 'checkbox'){
                                                if($rowdata->properties[$arr_key]['value'] == 'yes'){
                                                  $value = '<a href="" class="btn btn-success btn-link btn-just-icon done"><i class="material-icons">check_circle</i></a>';
                                                }
                                                else{
                                                  $value = '<a href="" class="btn btn-danger btn-link btn-just-icon remove close_btn"><i class="material-icons">cancel</i></a>';
                                                }
                                              }
                                              else{
                                                if($rowdata->properties[$arr_key]['value'] != null){
                                                  $value = $rowdata->properties[$arr_key]['value'];
                                                }
                                                else{
                                                  $value = '<a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>';
                                                }
                                                
                                              }
                                            }
                                            else{
                                              $value = '<a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>';
                                            }
                                          }
                                          else{
                                            $value = '<a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>';
                                          }
                                          ?>

                                        <td class="status_td">{!! $value !!}</td>

                                        
                                        @endforeach
                                      <td class="td-actions text-right">
                                          <button unit_p_key="{{$rowdata->id}}" subcat_id="{{$inventory->subcat_id}}" unit_type="existing" type="button" rel="tooltip" class="btn btn-success btn-link unit_list_edit">
                                            <i class="material-icons">edit</i>
                                          </button>
                                          <button type="button" rel="tooltip" class="btn btn-danger btn-link unit_list_dlt" data-original-title="" title="" unit_p_key="{{$rowdata->id}}">
                                            <i class="material-icons">close</i>
                                          </button>
                                        </td>
                                        
                                      </tr>
                                      
                                      <tr class="inventory_accordion_row">
                                        <td colspan="10">
                                        </td>
                                      </tr>
                                    
                                    <?php $sr++; ?>
                                    @endforeach

                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>

                            


                          </div>
                        </div>
                        <div class="form-group bmd-form-group save_btn_wrapper text-right">
                          <button type="submit" class="btn save_btn custom-btn-one">Update Resources</button>
                        </div>
                    </form>


                     
                  </div>
                </div>
  
  
  
                </div>
              </div>
            </div>
          </div>

<script>
  $(document).ready(function() {

    $('.price_class_toggle').change(function() {
      if (this.checked) {
          $('#service_price_class').show();
          $('#rental_price_class').hide();
      }
      else{
          $('#rental_price_class').show();
          $('#service_price_class').hide();
      }        
    });

    $( ".custom-select" ).change(function() {
      if(this.value.length > 0){
          $( this ).addClass( "hasvalue" );
      }
    })
    
    $('.cat').change(function(){
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getSubcategory') }}",
            method:"POST",
            data:{value:value, _token:_token, type: 'inventory'},
            success:function(result)
            {
             $('.subcat').html(result);
            }
         })
      }
     });





    //$('.upc_btn').click(function() {
    $(document).on('click','.upc_btn', function(){
      let random_upc =  Math.floor(100000000000 + Math.random() * 900000000000);
      $('#upc').val(random_upc).parent().addClass('is-filled');
    });

    //$('.vin_btn').click(function() {
    $(document).on('click','.vin_btn', function(){
      let random_vin =  rand_str();
      $('#vin').val(random_vin).parent().addClass('is-filled');
    });


    function rand_str() {
      const list = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
      var res = "";
      for(var i = 0; i < 17; i++) {
          var rnd = Math.floor(Math.random() * list.length);
          res = res + list.charAt(rnd);
      }
      return res;
    }
  });



let value = $('input[name="inventory_key"]').val();
let type = 'inventory';
let _token = $('input[name="_token"]').val();
var preloaded = [];
$.ajax({
            async: false,
            url:"{{ route('getImage') }}",
            method:"POST",
            data:{value:value, type:type, _token:_token},
            success:function(result)
            {
              var obj = JSON.parse(result)
              $.each(obj, function(index) {
                var data = { id: obj[index].id, src: obj[index].file };
                preloaded.push(data);
              });

              preloadedImage(preloaded);
            }
         })

function preloadedImage(param) {

    $('.input-images').imageUploader({
      preloaded: param,
      imagesInputName:'images',
      preloadedInputName:'preloaded',
      label:'Drag & Drop files here or click to browse'
  });
}




$(document).on('click','.variable_adder_btn', function(){
  let var_id = $('.varSelection').val();
  let _token = $('input[name="_token"]').val();

  $.ajax({
            url:"{{ route('addVariableSection') }}",
            method:"POST",
            data:{var_id:var_id, _token:_token},
            success:function(result)
            { 
              $('.var_section').append(result);
              $('.variable_selection_table_card').show();
            }
         })
  $('.custom_select2_col select.var_blank_it').val(null).trigger('change');
  $('#variable_selection_dw').removeClass('hasvalue')
});

$("#category-custom-select").css("pointer-events","none").css('background-color', '#e9ecef');
$("#sub-category-custom-select").css("pointer-events","none").css('background-color', '#e9ecef');

//  Changing the padding of the serial name input field based on badge text and width change

var change_the_field_padding = function(badge_span){
  var width_of_badge = $(badge_span).outerWidth();
  var new_width = width_of_badge+3;
  $(badge_span).siblings('.serial_no_field').css('padding-left', ''+new_width+'px');
}

$('body').on('DOMSubtreeModified', '.serial_no_badge', function(){
  change_the_field_padding($(this))
});

$(document).ready(function(){
  change_the_field_padding('.serial_no_badge')
});



$('.add_unit_btn_new').on('click',function(){
  unitProperties($(this));
});

$('.unit_list_edit').on('click',function(){
  unitProperties($(this));
})

$('.unit_list_dlt').on('click',function(){
  unitPropertiesDlt($(this));
})

$(document).on('click','.unit_properties_close',function(){
  $('.all_unit_list_view .table-responsive').css('overflow-x','auto');
  $('.unit_visibility_card').css({"width": "99%", "margin": "0 auto"});
  $(this).parents('.unit_visibility_card').remove();
})


function unitProperties(el){
  let unit_p_key = el.attr('unit_p_key');
  let subcat_id = el.attr('subcat_id');
  let unit_type = el.attr('unit_type');
  let inventory_id = $('input[name="inventory_key"]').val();
  let _token = $('input[name="_token"]').val();
  $.ajax({
            async: false,
            context: el,
            url:"{{ route('unitProperties') }}",
            method:"POST",
            data:{unit_p_key:unit_p_key, subcat_id:subcat_id, unit_type:unit_type, inventory_id:inventory_id, _token:_token},
            success:function(result)
            {
            if(el.attr('unit_p_key') != 0){
              if(el.parents('.unit_list_tr').next().find('td .unit_visibility_card').length < 1 ){
                $('.unit_visibility_card').remove();
                el.parents('.unit_list_tr').next().find('td').append(result);

                  change_the_padding('.serial_no_badge','.serial_no_badge_postfix','.serial_no_badge_hyphen')
              }
              else{
                el.parents('.unit_list_tr').next().find('td .unit_visibility_card').remove();
              }
            }
            else{
              $('.unit_visibility_card').remove();
              $('.new_card').append(result);

               change_the_padding('.serial_no_badge','.serial_no_badge_postfix','.serial_no_badge_hyphen')
            }

            }
         });

        let value2 = el.attr('unit_p_key');
        let type2 = 'inventory_unit';
        let _token2 = $('input[name="_token"]').val();
        var preloaded2 = [];
        $.ajax({
            async: false,
            context:this,
            url:"{{ route('getImage') }}",
            method:"POST",
            data:{value:value2, type:type2, _token:_token2},
            success:function(result)
            {
              var obj = JSON.parse(result)
              $.each(obj, function(index) {
                var data = { id: obj[index].id, src: obj[index].file };
                preloaded2.push(data);
              });

              //$('.input-images2').empty();
              preloadedImage2(preloaded2);
            }
         })

        function preloadedImage2(param) {

            $('.input-images2').imageUploader({
              preloaded: param,
              imagesInputName:'images2',
              preloadedInputName:'preloaded2',
              label:'Drag & Drop files here or click to browse'
          });
        }

if($('.unit_visibility_card .serial_no_field').val() != ""){
  $('.unit_visibility_card .serial_no_badge_hyphen').show()
}

}


function unitPropertiesDlt(el){
  let unit_p_key = el.attr('unit_p_key');
  let _token = $('input[name="_token"]').val();
  $.ajax({
            async: false,
            context: el,
            url:"{{ route('unitPropertiesDlt') }}",
            method:"POST",
            data:{unit_p_key:unit_p_key, _token:_token},
            beforeSend:function(){
              return confirm("Are you sure to delete?");
            },
            success:function(result)
            {
              $('.unit_dlt_msg').show();

              setTimeout(function(){
               location.reload(); 
              }, 1000);
            }

         });
}


// Making the serial_no visible or not
//$('.serial_no_field').on('input',function(){
$(document).on('input','.serial_no_field',function(){
  change_the_padding('.serial_no_badge','.serial_no_badge_postfix','.serial_no_badge_hyphen')
  $('.serial_no_badge_hyphen').show();
})

$(document).on('focus','.serial_no_field',function(){
  // Making the serial no visible or not
  $(this).siblings('.serial_no_badge').show();
  $(this).siblings('.serial_no_badge_postfix').show();
  change_the_padding('.serial_no_badge','.serial_no_badge_postfix','.serial_no_badge_hyphen')
})

$(document).on('blur','.serial_no_field',function(){
  // Making the serial no visible or not
  checking_the_input_field();
})

$('#resource_code').on('blur',function(){
  var resource_code_val = $(this).val();
  var serial_no =  $(this).parents('.resource_general_section_card').siblings('.resource_unit_section_card').find('.serial_no_badge_postfix');
  var serial_no_html = $(this).parents('.resource_general_section_card').siblings('.resource_unit_section_card').find('.serial_no_badge_postfix').html();

  serial_no.html(resource_code_val)
})





</script>

@endsection