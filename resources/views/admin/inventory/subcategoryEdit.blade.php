@extends('layouts.master')

@section('content')

        <div class="content">
          <div class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12 mx-auto card_holder_box">
                  
                  
                  <div class="card">
                  <div class="card-header card-header-tabs card-header-rose">
                    <div class="nav-tabs-navigation">
                      <div class="nav-tabs-wrapper">
                        
                        <ul class="nav nav-tabs" data-tabs="tabs">
                          <li class="nav-item">
                            <a class="nav-link active" href="#profile" data-toggle="tab">
                              <i class="material-icons">ballot</i> Edit Sub Category
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                          
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div style="margin-top: 15px;">
                      @include('partials.messages')
                  </div>

                  <div class="card-body">
                     
                        <div class="card edit_category_inner_card">
                          <div class="card-header card-header-rose card-header-text">
                            
                          </div>

                          <div class="card-body col-md-12 mx-auto increased_margin_t_cardbody">
                            <form method="post" action="{{ route('inventory.update', $subcat->id) }}" class="form-horizontal">
                            @csrf
                            @method('PUT')

                            <input type="hidden" name="tab" value="subcategory">
                            <input type="hidden" name="p_key" value="{{$subcat->id}}">

                                <div class="row sub_holder">
                                  <div class="col-md-6 custom_select2_col">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select hasvalue select2" id="category-custom-select" name="cat_id">
                                        <option class="form-select-placeholder"></option>
                                        @foreach($cat as $rowdata)
                                          <option value="{{$rowdata->id}}" @if($rowdata->id == $subcat->cat_id) selected @endif>{{$rowdata->category_name}}</option>
                                        @endforeach
                                      </select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Category</label>
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label for="subcategory_name" class="bmd-label-floating input_label">Sub Category Name *</label>
                                      <input type="text" class="form-control form_input_field" id="category_name" name="subcategory_name" required="true" aria-required="true" value="{{$subcat->subcategory_name}}">
                                    </div>
                                  </div>
                                </div>


                    <fieldset class="custom_fieldset_2">
                      <legend class="resource_para_label">Add Parameter for Resource</legend>

                  @if(!$unit_params->isEmpty())
                    @foreach($unit_params as $rowdata)
                   
                      <div class="row input_row unit_parameter_row">
                        
                        
                        
                        <div class="col-md-5 input_wrapper first">
                          <div class="form-group bmd-form-group">
                            <label for="unit_parameter_name" class="bmd-label-floating input_label">Name of parameter *</label>
                            <input type="hidden" name="unit_param_key[]" value="{{$rowdata->id}}">
                            <input type="text" class="form-control form_input_field" id="unit_parameter_name" name="unit_param_name[]" required="true" aria-required="true" value="{{$rowdata->unit_param_name}}">
                          </div>
                        </div>
                        <div class="col-md-5 second input_wrapper custom_select2_col">
                          <div class="form-group  custom-form-select">
                            <select class="custom-select unit_parameter_type select2 form_input_field hasvalue" id="unit_data_type" name="unit_param_data_type[]" required="true" aria-required="true">
                              <option class="form-select-placeholder"></option>
                              <option value="string" @if($rowdata->unit_param_data_type == 'string') selected @endif>String</option>
                              <option value="integer" @if($rowdata->unit_param_data_type == 'integer') selected @endif>Integer</option>
                              <option value="decimal" @if($rowdata->unit_param_data_type == 'decimal') selected @endif>Decimal</option>
                              <option value="datetime" @if($rowdata->unit_param_data_type == 'datetime') selected @endif>DateTime</option>
                              <option value="checkbox" @if($rowdata->unit_param_data_type == 'checkbox') selected @endif>Checkbox</option>
                              <option value="vin_number" @if($rowdata->unit_param_data_type == 'vin_number') selected @endif>VIN Number</option>
                              <option value="upc_number" @if($rowdata->unit_param_data_type == 'upc_number') selected @endif>UPC Number</option>
                            </select>
                            <div class="form-element-bar">
                            </div>
                            <label class="form-element-label" for="unit_data_type input_label">Data Type</label>
                          </div>
                        </div>
                          <div class="col-md-2 unit_parameter_cross text-right">
                              <a href="#" class="btn btn-link btn-danger btn-just-icon remove unit_param_dlt_btn"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                          </div>
                      </div>
                    @endforeach
                  @else
                    <div class="row input_row unit_parameter_row">
                        <div class="col-md-5 input_wrapper first">
                          <div class="form-group bmd-form-group">
                            <label for="unit_parameter_name" class="bmd-label-floating input_label">Name of parameter *</label>
                            <input type="text" class="form-control form_input_field" id="unit_parameter_name" name="unit_param_name[]" required="true" aria-required="true">
                          </div>
                        </div>
                        <div class="col-md-5 second input_wrapper custom_select2_col">
                          <div class="form-group  custom-form-select">
                            <select class="custom-select unit_parameter_type select2 form_input_field" id="unit_data_type" name="unit_param_data_type[]" required="true" aria-required="true">
                              <option class="form-select-placeholder"></option>
                              <option value="string">String</option>
                              <option value="integer">Integer</option>
                              <option value="decimal">Decimal</option>
                              <option value="datetime">DateTime</option>
                              <option value="checkbox">Checkbox</option>
                              <option value="vin_number">VIN Number</option>
                              <option value="upc_number">UPC Number</option>
                            </select>
                            <div class="form-element-bar">
                            </div>
                            <label class="form-element-label" for="unit_data_type input_label">Data Type</label>
                          </div>
                        </div>
                          <div class="col-md-2 unit_parameter_cross text-right">
                              <a href="#" class="btn btn-link btn-danger btn-just-icon remove disabled"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                          </div>
                      </div>
                  @endif

                      <div class="row add_parameter_btn_row">
                          <div class="col-md-12 text-right">
                            <button class="btn btn-info btn-round btn-sm add_parameter_btn" type="button">
                              <span class="btn-label">
                                <i class="material-icons">add</i>
                              </span>
                              Add Parameter
                              <div class="ripple-container"></div>
                            </button>
                          </div>
                      </div>
                  </fieldset>

                              <div class="form-group bmd-form-group save_btn_wrapper text-right">
                                <button type="submit" class="btn save_btn custom-btn-one">Update</button>
                              </div>
                          </form>
                          </div>
                      </div>


                     
                  </div>
                </div>
  
  
  
                </div>
              </div>
          </div>
        </div>
      </div>

@endsection