<script id="cat-details-template" type="text/x-handlebars-template">
<div class="treeview">
<details>
    <summary class="detail-view-cat" style="padding: 10px;">Category: {{{ category_name }}}</summary>
    {{#subcat}}
    <details>
      <summary class="detail-view-subcat" style="margin-left: 15px; padding: 10px;">Sub-Category: {{{ subcategory_name }}}</summary>
      {{#resources}}
          <li class="detail-view-resource" style="margin-left: 30px; padding: 10px;">Resource Name- {{{ resource_name }}}</sum</li>
      {{/resources}}
    </details>  
    {{/subcat}}    
  </details>
</div>
</script>


<!-- <script id="cat-details-template" type="text/x-handlebars-template">
  <ul>
  <li class="container"><p>Category: {{{ category_name }}} </p></li>
  {{#subcat}}
    <ul>
      <li class="container"><p>Sub-Category: {{{ subcategory_name }}} </p>
      {{#resources}}
        <ul>
          <li><p>Resource- {{{ resource_name }}} </p></li>
        </ul>
      {{/resources}}
      </li>
    </ul>
  {{/subcat}}
  </ul>
</script> -->