@extends('layouts.master')

@section('content')

	      <div class="content manage-inventory-content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12 mx-auto">
                <div class="card manage_inv_ser_root_card">
                  <div class="card-header card-header-tabs card-header-rose custom_card_header mang_inventory_card_header inv_mang_card_tab_header">
                    <div class="nav-tabs-navigation">
                      <div class="nav-tabs-wrapper">
                        <p class="nav-tabs-title">Manage Inventory</p>
                        <ul class="nav nav-tabs" data-tabs="tabs" id="tabMenu">
                          <li class="nav-item">
                            <a class="nav-link " href="#category_management" data-toggle="tab">
                              <i class="material-icons">bug_report</i> Category Management
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#sub_category_management" data-toggle="tab">
                              <i class="material-icons">code</i> Sub Category Management
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                           <li class="nav-item">
                            <a class="nav-link" href="#variable_management" data-toggle="tab">
                              <i class="material-icons">code</i> Variable Management
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#resources_management" data-toggle="tab">
                              <i class="material-icons">cloud</i> Added Inventory
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div class="alert alert-success alert-dismissible var_msg_wrapper" role="alert" style="display: none;">
                    <!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
                    <strong class="var_msg"></strong>
                  </div>

                  <div class="card-body manage-inventory-content-card-body">
                    <div class="tab-content">
                      <div class="tab-pane inventory_sub_card" id="category_management">
                        <div class="card">
                          <div class="card-header card-header-rose card-header-icon icon_box_wrapper">
                            <div class="card-icon">
                              <i class="material-icons">assignment</i>
                            </div>
                            <h4 class="card-title">Category</h4>
                          </div>

                          <div style="margin-top: 15px;">
                            @include('partials.messages')
                          </div>

                          <div class="card-body">
                            <div class="toolbar">
                              <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <div class="material-datatables inventory_category_datatables" style="overflow-x: hidden; overflow-y: hidden;">
                              <table id="manage_cat_inventory" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%;">
                                <thead>
                                  <tr>
                                    <th class="disabled-sorting"></th>
                                    <th> Category Name</th>
                                    <th class="disabled-sorting text-right">Actions</th>
                                  </tr>
                                </thead>
                              </table>

                              @include('admin.inventory.cat') 
                          


                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane inventory_sub_card" id="sub_category_management">
                        <div class="card">
                          <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                              <i class="material-icons">assignment</i>
                            </div>
                            <h4 class="card-title">Sub Category</h4>
                          </div>

                          <div style="margin-top: 15px;">
                            @include('partials.messages')
                          </div>

                          <div class="card-body">
                            <div class="toolbar">
                            </div>
                            <div class="material-datatables inventory_subcategory_datatables" style="overflow-x: hidden; overflow-y: hidden;">
                              <table id="cat_sub_table" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%;">
                                <thead>
                                  <tr>
                                    <th class="disabled-sorting"></th>
                                    <th>Category Name</th>
                                    <th>Sub Category Name</th>
                                    
                                    <th class="disabled-sorting text-right">Actions</th>
                                  </tr>
                                </thead>
                              </table>


                              @include('admin.inventory.subcat') 


                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="tab-pane inventory_sub_card" id="variable_management">
                        <div class="card">
                          <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                              <i class="material-icons">assignment</i>
                            </div>
                            <h4 class="card-title">Variable</h4>
                          </div>

                          <div style="margin-top: 15px;">
                            @include('partials.messages')
                          </div>

                          <div class="card-body">
                            <div class="toolbar">
                            </div>
                            <div class="material-datatables variable_table_management inventory_subcategory_datatables" style="overflow-x: hidden;">
                              <table id="inventory_variable_mgt_table" class="table table-striped table-no-bordered table-hover all_table variable_table" cellspacing="0" width="100%" style="width:100%;">
                                <thead>
                                  <tr>
                                    <th>Variable Name</th>
                                    <th>Status</th>
                                    <th class="disabled-sorting text-right">Actions</th>
                                  </tr>
                                </thead>
                                <tbody>

                                @foreach($variables as $rowdata)
                                  <tr>
                                    <td>{{$rowdata->variable_name}}</td>
                                    <td>
                                      <span class="togglebutton">
                                        <label>
                                            <input type="checkbox" name="" class="enable_disable" var_key="{{$rowdata->id}}" value="{{$rowdata->enable_disable}}" @if($rowdata->enable_disable == 1) checked @endif>
                                            Disable <span class="toggle"></span> Enable
                                        </label>
                                      </span>
                                    </td>
                                    <td class="text-right">
                                    
                                    <form action="{{ route('inventory.destroy',[$rowdata->id, 'type' => 'variable']) }}" method="POST">

                                      

                                      <a href="{{ route('inventory.edit', [$rowdata->id, 'type' => 'variable']) }}" name="" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a>

                                      @csrf
                                      @method('DELETE')

                                      <button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm('Are you sure you want to delete?');"><i class="material-icons">close</i></button>

                                      
                                    </form>
                                    
                                    </td>
                                  </tr>
                                @endforeach
                                
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="tab-pane inventory_sub_card" id="resources_management">
                        <div class="card">
                          <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                              <i class="material-icons">assignment</i>
                            </div>
                            <h4 class="card-title">Inventories</h4>
                          </div>

                          <div style="margin-top: 15px;">
                            @include('partials.messages')
                          </div>
                          
                          <div class="card-body">
                            <div class="toolbar">
                              <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <div class="material-datatables inventory_datatables" style="overflow-x: hidden;">
                              <table id="cat_sub_res_table" class="table table-striped table-no-bordered table-hover all_table" cellspacing="0" width="100%" style="width:100%;">
                                <thead>
                                  <tr>
                                    <th>Category Name</th>
                                    <th>Sub Category Name</th>
                                    <th>Resource Name</th>
                                    <th>Serial Name</th>
                                    <!-- <th>UPC/VIN</th> -->
                                    <th>Price(per hour)</th>                                    
                                    <th class="disabled-sorting text-right">Actions</th>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach($inventory as $rowdata)
                                <?php
                                  $inventory_info = $rowdata->inventory_info($rowdata->id);
                                ?>
                                  <tr>
                                    <td>@if($rowdata->category){{$rowdata->category->category_name}}@endif</td>
                                    <td>@if($rowdata->subcategory){{$rowdata->subcategory->subcategory_name}}@endif</td>
                                    <td>{{$rowdata->resource_name}}</td>
                                    <td class="inventory">
                                      <select class="custom-select inventory_name" id="category-custom-select" name="cat_id" required="true" aria-required="true">
                                        <?php $i=1; ?>
                                          @foreach($inventory_info as $inventory_info_row)
                                          @if($inventory_info_row->serial_name)
                                          <?php $sr_name = ' - ' . $inventory_info_row->serial_name; ?>
                                          @else
                                          <?php $sr_name = ''; ?>
                                          @endif
                                            <option inventory_id="{{$rowdata->id}}" value="{{$inventory_info_row->id}}">{{$i}} {{$rowdata->resource_code}} {{$sr_name}}</option>
                                          <?php $i++; ?>
                                          @endforeach
                                          
                                      </select>
                                    </td>
                                    <!-- <td class="upc">
                                      <select class="custom-select upc_vin upc_vin_class" id="category-custom-select" name="cat_id" required="true" aria-required="true">
                                          @foreach($inventory_info as $inventory_info_row)
                                            <option inventory_id="{{$rowdata->id}}" value="{{$inventory_info_row->id}}">{{$inventory_info_row->upc_vin}}</option>
                                          @endforeach
                                          
                                      </select>
                                    </td> -->
                                    <td>$ {{$rowdata->price_per_hour_service}}</td>
                                                                      
                                    <td class="text-right">
                                    <?php 
                                    $inventory_unit = App\Models\InventoryUnit::where('inventory_id', $rowdata->id)->first();
                                    ?>
                                    <form class="inventory_destroy" action="{{ route('inventory.destroy',[$rowdata->id, 'type' => 'inventory', 'unit' => $inventory_unit->id]) }}" method="POST">

                                      <a href="{{ route('inventory.edit', [$rowdata->id, 'type' => 'inventory', 'unit' => $inventory_unit->id]) }}" name="" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a>

                                      @csrf
                                      @method('DELETE')
                                      <button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm('Are you sure you want to delete?');"><i class="material-icons">close</i></button>
                                    </form>

                                    </td>
                                  </tr>
                                @endforeach
                                
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

<script>
  $(document).ready(function() {
    $('.inventory_name').change(function() {
        let row_id = $(this).val();
        let inventory_id = $(this).find(':selected').attr('inventory_id');

        //alert(row_id);

        $(this).parent().parent().find(".upc_vin option[value=" + row_id + " ]").prop("selected", "selected"); 

        inventory_destroy_url = "{!! route('inventory.destroy',[':id', 'type' => 'inventory', 'unit' => 'unitValue']) !!}";
        inventory_destroy_url = inventory_destroy_url.replace(':id',inventory_id);
        inventory_destroy_url = inventory_destroy_url.replace('unitValue',row_id);

        inventory_edit_url = "{!! route('inventory.edit', [':id', 'type' => 'inventory', 'unit' => 'unitValue']) !!}";
        inventory_edit_url = inventory_edit_url.replace(':id',inventory_id);
        inventory_edit_url = inventory_edit_url.replace('unitValue',row_id);

        $(this).parent().parent().find('.inventory_destroy').attr("action", inventory_destroy_url); 
        $(this).parent().parent().find('a').attr("href", inventory_edit_url); 

    });

    $('.upc_vin').change(function() {
        let row_id = $(this).val();
        let inventory_id = $(this).find(':selected').attr('inventory_id');

        $(this).parent().parent().find(".inventory_name option[value=" + row_id + " ]").prop("selected", "selected");

        inventory_destroy_url = "{!! route('inventory.destroy',[':id', 'type' => 'inventory', 'unit' => 'unitValue']) !!}";
        inventory_destroy_url = inventory_destroy_url.replace(':id',inventory_id);
        inventory_destroy_url = inventory_destroy_url.replace('unitValue',row_id);

        inventory_edit_url = "{!! route('inventory.edit', [':id', 'type' => 'inventory', 'unit' => 'unitValue']) !!}";
        inventory_edit_url = inventory_edit_url.replace(':id', inventory_id);
        inventory_edit_url = inventory_edit_url.replace('unitValue', row_id);

        $(this).parent().parent().find('.inventory_destroy').attr("action", inventory_destroy_url); 
        $(this).parent().parent().find('a').attr("href", inventory_edit_url); 

    });

  });

  $(window).on('load', function(){
    setTimeout(function() {
       $('.material-datatables').css('overflow-x','unset');
       $('.inventory_subcategory_datatables').css('overflow-y','unset');
       $('.inventory_category_datatables').css('overflow-y', 'unset');
       $('.inventory_datatables table thead th:last-child').css('width','100px');
       $('.inventory_category_datatables table thead th:last-child').css('width','100px');
       $('.inventory_subcategory_datatables table thead th:last-child').css('width','100px');
    }, 100);
  });

$('.enable_disable').change(function() {
    let value = $(this).prop("checked");
    let var_key = $(this).attr('var_key');
    let _token = $('input[name="_token"]').val();

    $.ajax({
            url:"{{ route('variableEnableDisable') }}",
            method:"POST",
            data:{var_key:var_key, _token:_token, value:value},
            success:function(result)
            { 
                if(result == 0){
                    $('.var_msg').html('Success: Variable has been disabled');
                    $('.var_msg_wrapper').show();
                }
                else{
                    $('.var_msg').html('Success: Variable has been enabled');
                    $('.var_msg_wrapper').show();
                }
            }
    })

});

$(document).ready(function () {

if($('#tabMenu a[href="#{{ old('tab') }}"]').attr('href') == undefined){
    $('#tabMenu a[href="#resources_management"]').tab('show')  
}
else{
    $('#tabMenu a[href="#{{ old('tab') }}"]').tab('show')
}

});




$(document).ready(function(){

///////CAT DATATABLE///////
var cat_template = Handlebars.compile($("#cat-details-template").html());
var cat_datatable = $('#manage_cat_inventory').DataTable({
  autoWidth: false,
  processing: true,
  serverSide: true,
  ajax:{
   url: "{{ route('inventory.index') }}?type=cat",
  },
  language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records"
  },
  columnDefs: [
                  { "width": "20px", "targets": 0 },

              ],
  columns:[
    {
      "className":      'details-control',
      "orderable":      false,
      "searchable":     false,
      "data":           null,
      "defaultContent": ''
    },
    {
      data: 'category_name',
      name: 'category_name',
      orderable: false
    },
    {
     "className": 'text-right',
      data: 'action',
      name: 'action',
      orderable: false,
    }
  ]
 });

  // Add event listener for opening and closing details
    $('#manage_cat_inventory tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = cat_datatable.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            //row.child( format(row.data()) ).show();
            row.child( cat_template(row.data()) ).show();
            
            tr.addClass('shown');
        }
    });



///////SUBCAT DATATABLE///////
var subcat_template = Handlebars.compile($("#subcat-details-template").html());
var subcat_datatable = $('#cat_sub_table').DataTable({
  autoWidth: false,
  processing: true,
  serverSide: true,
  ajax:{
   url: "{{ route('inventory.index') }}?type=subcat",
  },
  language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records"
  },
  columnDefs: [
                  { "width": "20px", "targets": 0 },

              ],
  columns:[
    {
        "className":      'details-control',
        "orderable":      false,
        "searchable":     false,
        "data":           null,
        "defaultContent": ''
    },
    {
      data: 'category.category_name',
      name: 'category.category_name',
      orderable: false
    },
    {
      data: 'subcategory_name',
      name: 'subcategory_name',
      orderable: false
    },
    {
      "className": 'text-right',
      data: 'action',
      name: 'action',
      orderable: false
    }
  ]
 });

  // Add event listener for opening and closing details
    $('#cat_sub_table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = subcat_datatable.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            //row.child( format(row.data()) ).show();
            row.child( subcat_template(row.data()) ).show();
            tr.addClass('shown');
        }
    });


});

</script>

@endsection