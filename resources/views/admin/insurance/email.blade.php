<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.css">
  

<style>

body{
  font-family: 'Roboto', sans-serif;
}
.logo{
  text-align: center;
  margin-bottom: 10px;
}
.container{
  background-color: #f2f2f2;
  margin: 0 auto;
  width: 750px;
  padding:10px;
  padding-bottom: 0;
}

.body{
   background-color: #ffffff;
   margin: 0 auto;
   width: 650px;
}
.content{
  padding: 20px;
  text-align: center;
  font-family: 'Roboto', sans-serif;
}

.footer h3{
  text-align:center;
  font-weight:bold;
}
.footer .column{
 text-align:center;
}

.footer .column img{
 width: 30px;
 height: 30px;
 padding: 5px;
}

.footer .column a{
 text-decoration: none;
}
.copyright{
  font-style: italic;
  font-weight: bold;
  text-align: center;
  padding: 10px 0;
  line-height: 1.9;
}

.button-wrapper-cta {
  display: block;
  text-align: center;
  margin-top: 15px;
}

.cta-button {
  border: none;
  border-radius: 3em;
  box-shadow: 0 2px 5px 0 rgba(44, 30, 233, 0.17), 0 3px 5px -2px rgba(80, 30, 233, 0.15), 0 1px 5px 0 rgba(30, 44, 233, 0.08);
  display: inline-block;
  font-size: 17px;
  letter-spacing: 0.75px;
  text-decoration: none;
  padding: 8px 16px;
  width: auto;
}

.button-cta {
  background: linear-gradient(60deg, #20c676, #19e461)!important;
  color: #000000!important;
  font-weight: 600;
}

.button-cta:hover {
  box-shadow: 11px 5px 20px -12px rgba(20, 160, 72, 0.27), 0 4px 10px 0px rgba(20, 157, 71, 0.2), 0 8px 10px -5px rgba(20, 159, 71, 0.29);
}
.content p{
  text-transform: capitalize;
  text-align: center;
  font-family: 'Roboto', sans-serif;
  font-size: 15px;
  line-height: 1.4;
}
.content p span{
  display: block;
  margin-bottom: 5px;
  font-weight: 300;
  font-family: 'Roboto', sans-serif;
  font-size: 15px;
  line-height: 1.4;
}
span.access-code{
  font-weight: 600 !important;
  margin: 10px 0;
}
</style>
</head>

<body style="color: black">

<div class="container">
  
  <div class="logo">
  <img src="{{asset('assets/img/logo_new.png')}}" alt="logo" style="width: 220px; height: auto; object-fit: contain;"/>
  </div>

  <div class="body">
    <div class="content">
      <p> <span>Hello, An insurance request has been generated by -</span> 
        <span>{{$data['submitted_by']}}</span>
        <span class="access-code">Your Access code is: {{$data['token']}}</span>
      </p>
      <p>Please click on the Button below -</p>
       <!-- <a href="">check insurance request</a> -->
       <div class="button-wrapper-cta">
        <a class="button-cta cta-button" href="{{$data['link']}}">Check Insurance Request</a>
      </div>
    </div>

  </div>
 

   <div class="footer">    
     <div class="copyright">
       Copyright(c) {{date('Y')}}. All rights reserved, EV Oilfield Services. 
     </div>

   </div>
  
  

</div>
     


</body>
</html>