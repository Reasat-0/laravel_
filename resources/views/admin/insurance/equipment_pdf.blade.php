<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title>Equipment Change Request</title>
</head>
<style>
body{
	font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
}
.request_id_badge{
	background: #f6cb61;
	width:140px;
	height: 30px;
	line-height: 1.3;
	border-radius: 0;
	text-align: center;
	color:white;
	border-radius: 50%;
	position: fixed;
	right: -30px;
	top: -30px;
	border: 1px solid #000;
	border-radius: 15px;
}
.request_id_badge h4{
	padding: 6px 5px;
	font-size:12px;
	font-weight: 300;
	margin: 0;
	text-transform: uppercase;
	color: #000;
}
.insurance_title_secton{
	text-align: center;
}
.insurance_logo img{
	height: 140px;
}
.insurance_title_secton .title{
	font-weight: bold;
	text-transform: capitalize;
	font-style: italic;
	font-size: 21px;
	font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
	margin: 0;
	line-height: 1;
	margin-top: 20px;
}
.sub-title{
	font-size: 10px;
}
.sub-title p{
	text-align: center;
	font-size: 5px;
	margin: 0;
	font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
}
.label_seciton{
	display: inline-block;
	padding: 0 15px;
}
.insurance_subtitle{
	text-align: center;
}
.insurance_subtitle p{
	margin: 15px 0;
	font-size: 14px;
}
.checkbox-section{
	text-align: left;
	margin-left: -160px;
	margin-bottom: -10px;
	margin-top: 5px;
}
.label_seciton{
	text-align: center;
}
table{
	width: 100%;
	border-collapse: collapse;
}
table tr td{
	vertical-align: middle;
	height: 30px;
	line-height: 1.4;
	font-size: 1rem;
}
.info-form-section .row{
	margin:10px 0;
}
table .info-form-section .row{
	margin: 0;
}
.info-form-section .row input{
	margin-top:5px; 
	width:80%;
	border:none; 
	border-bottom:1px solid black;
	font-size:14px;
}
.info-form-section .row label{
	width:20%;
	font-size:14px;
	font-weight: 100;
	position: relative;
	letter-spacing: 0.5px;
}
.info-form-section .row textarea{
	border: none;
	border-bottom:1px solid black;
}

.submission-section{
	margin-top:40px;
}
.submission-section .row .col:first-child{
	width:60%; 
	display:inline-block;
}
.submission-section .row .col:last-child{
	display:inline-block;
	width:40%;
	margin-left: 20px;
}
.submission-section .row .col label{
	font-size:14px;
	font-weight: 100;
	position: relative;
	letter-spacing: 0.5px;
}
.submission-section .row .col.left_col input{
	margin-top:5px;
	width:70%;
	border: none;
	border-bottom:1px solid black;
	font-size:14px;
}
.submission-section .row .col.right_col input{
	margin-top:5px;
	width:70%;
	border: none;
	border-bottom:1px solid black;
	font-size:14px;
}
.Equipment-Information .title{
	margin: 5px 0 0 2px;
}
footer {
    position: fixed; 
    bottom: -30px; 
    left: 0; 
    right: 0;
    color: white;
    height: 45px;
    text-align: center;
    /*background-color: #eff0f1;*/
} 
.footer-section-table{
	display: table;
	width: 100%;
	vertical-align: middle;
	table-layout: fixed;
}
.footer-section-row{
	display: table-row;
}
.footer-part{
	display: table-cell;
	height: 45px;
	vertical-align: middle;
}
.footer-part img{
	height: 30px;
	display: block;
	text-align: left;
}
.footer-part p{
	margin: 0;
	color: #676767;
	font-size: 12px;
	line-height: 1.3;
}
.pdf-fieldset{
	margin: 13px 0;
	margin-bottom: 0;
	padding: 5px 10px;
	padding-bottom: 0px;
	border: 0.5px solid #9a9a9a;
}
.pdf-fieldset legend{
	font-size: 12.5px;
	font-weight: bold;
	margin: 0;
	padding: 0 5px;
	padding-bottom: 0;
}
.pdf-fieldset .info-form-section .row{
	margin: 0;
	margin-top: -5px;
}
.pdf-fieldset table tr:last-child .info-form-section .row{
	margin-top: -15px;
}
.insurance_logo img{
	position: fixed;
	top: -40px;
	left: 0px;
	height: 80px;
}
.insurance_title_secton{
	margin-top: 50px;
}
</style>
<body>
  <div class="request_id_badge">
	   <h4 class="title"> Request ID : EQ-{{$data->id}}</h4>
  </div>
  <div class="insurance_logo">
		<img src="{{ public_path() . '/assets/img/logo_ins.png' }}">
	</div>
  <div class="insurance_pdf_main_section">  	
  	<div class="insurance_title_secton">
  		<h4 class="title">Equipment Change Request</h4>
  	</div>
  	<div class="insurance_subtitle">
  		<p>Please fill out the following information when adding or deleting equipment to your coverage</p>
  	</div>
  	<div class="checkbox-section">
		<div class="label_seciton">
			<input type="checkbox" @if($data->request == 'add') checked @endif style="display: block;">
			<label style="display: block;">ADD</label>
		</div>
		<div class="label_seciton">
			<input type="checkbox" @if($data->request == 'delete') checked @endif style="display: block;">
			<label style="display: block;">DELETE</label>
		</div>
		<div class="label_seciton">
			<input type="checkbox" @if($data->request == 'amend') checked @endif style="display: block; ">
			<label style="display: block;">AMEND</label>
		</div>
	</div>

	<div class="info-form-section">
		<div class="row">
			<label for="company_name" style=""> Company Name :</label>
			<input type="text" value="{{$data->company_name}}" style="" />
		</div>
		<div class="row">
			<label for="company_name" style=""> Effective Date :</label>
			<input type="text" value="{{$data->effective_date}}" style="width:40%"  />
		</div>
	</div>

	<div class="Equipment-Information">
		<h5 class="title">Equipment Information:</h5>
		<table>
			<tr>
				<td>
					<div class="info-form-section">
						<div class="row">
							<label for="company_name" style=""> Year:</label>
							<input type="text" value="{{$data->year}}" style="" />
						</div>
					</div>
				</td>
				<td>
					<div class="info-form-section">
						<div class="row">
							<label for="company_name" style=""> Make:</label>
							<input type="text" value="{{$data->make}}" style="" />
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="info-form-section">
						<div class="row">
							<label for="company_name" style=""> Model:</label>
							<input type="text" value="{{$data->model}}" style="" />
						</div>
					</div>
				</td>
				<td>
					<div class="info-form-section">
						<div class="row">
							<label for="company_name" style=""> VIN:</label>
							<input type="text" value="{{$data->vin}}" style="" />
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="info-form-section">
						<div class="row">
							<label for="company_name" style=""> Unit#:</label>
							<input type="text" value="{{$data->unit}}" style="" />
						</div>
					</div>
				</td>
				<td>
					<div class="info-form-section">
						<div class="row">
							<label for="company_name" style=""> Value:</label>
							<input type="text" value="{{$data->value}}" style="" />
						</div>
					</div>
				</td>
			</tr>
		</table>
	</div>

	<div class="info-form-section">
		<div class="row">
			<label for="company_name" style=""> Description of Equipment Accessories:</label>
			<input type="text" value="{{$data->description_equipment_accessories}}" style="width:61.75%; " />
			<!-- <textarea name="" id="" cols="30" rows="1" style="width:99%" value="">{{$data->description_equipment_accessories}}</textarea> -->
		</div>
		<!-- <div class="row">
			<label for="company_name" style="">Garaging Location/Address:</label>
			<input type="text" value="{{$data->garaging_location_address}}" style="width:72%; " />
		</div> -->

		<fieldset class="pdf-fieldset">
			<legend>Garraging Location Address</legend>
			<table>
				<tr>
					<td colspan="2" width="65%">
						<div class="info-form-section">
							<div class="row">
								<label for="company_name" style=""> Address 1:</label>
								<input type="text" value="{{$data->equip_garraging_address_1}}" style="width: 77.5%; " />
							</div>
						</div>
					</td>
					<td width="35%">
						<div class="info-form-section">
							<div class="row">
								<label for="company_name" style=""> Address 2:</label>
								<input type="text" value="{{$data->equip_garraging_address_2}}" style="width: 65%; " />
							</div>
						</div>						
					</td>
				</tr>
				<tr>
					<td>
						<div class="info-form-section">
							<div class="row">
								<label for="company_name" style=""> State: </label>
								<input type="text" value="{{$data->equip_garraging_state}}" style="width: 70%; " />
							</div>
						</div>
					</td>
					<td>
						<div class="info-form-section">
							<div class="row">
								<label for="company_name" style=""> City:</label>
								<input type="text" value="{{$data->equip_garraging_city}}" style="width: 75.5%; " />
							</div>
						</div>
					</td>
					<td>
						<div class="info-form-section">
							<div class="row">
								<label for="company_name" style=""> Zip:</label>
								<input type="text" value="{{$data->equip_garraging_zip}}" style="width: 82%; " />
							</div>
						</div>
					</td>
				</tr>
			</table>
		</fieldset>

		<div class="row">
			<label for="company_name" style="">Lease or Purchase:</label>
			<input type="text" value="{{$data->lease_purchase}}" style="width:30%; " />
		</div>
		<div class="row">
			<label for="company_name" style="">Finance Company/Lessor Name: </label>
			<input type="text" value="{{$data->finance_company_name}}" style="width:67.5%; " />
		</div>
		<!-- <div class="row">
			<label for="company_name" style="">Finance Company/Lessor Address:</label>
			<input type="text" value="{{$data->finance_company_address}}" style="width:65.5%;  " />
		</div> -->
		<fieldset class="pdf-fieldset">
			<legend>Finance Company/Lessor Address</legend>
			<table>
				<tr>
					<td colspan="2" width="65%">
						<div class="info-form-section">
							<div class="row">
								<label for="company_name" style=""> Address 1:</label>
								<input type="text" value="{{$data->equip_lessor_address_1}}" style="width: 77.5%; " />
							</div>
						</div>
					</td>
					<td width="35%">
						<div class="info-form-section">
							<div class="row">
								<label for="company_name" style=""> Address 2:</label>
								<input type="text" value="{{$data->equip_lessor_address_2}}" style="width: 65%; " />
							</div>
						</div>						
					</td>
				</tr>
				<tr>
					<td>
						<div class="info-form-section">
							<div class="row">
								<label for="company_name" style=""> State: </label>
								<input type="text" value="{{$data->equip_lessor_state}}" style="width: 70%; " />
							</div>
						</div>
					</td>
					<td>
						<div class="info-form-section">
							<div class="row">
								<label for="company_name" style=""> City:</label>
								<input type="text" value="{{$data->equip_lessor_city}}" style="width: 75.5%; " />
							</div>
						</div>
					</td>
					<td>
						<div class="info-form-section">
							<div class="row">
								<label for="company_name" style=""> Zip:</label>
								<input type="text" value="{{$data->equip_lessor_zip}}" style="width: 82%; " />
							</div>
						</div>
					</td>
				</tr>
			</table>
		</fieldset>

		<div class="row">
			<label for="company_name" style="">Finance Company/Lessor Phone or Email for EPI: </label>
			<input type="text" value="{{$data->finance_company_phone_email}}" style="width:50.5%; " />
		</div>
		<div class="row">
			<label for="company_name" style="">Additional Notes:</label>
			<input type="text" value="{{$data->additional_notes}}" style="width:81.5%; " />
			<!-- <textarea name="" id="" cols="30" rows="1" style="width:99%" value="{{$data->additional_notes}}"></textarea> -->
		</div>

	</div>
	<div class="submission-section">
		<div class="row">
			<div class="col left_col" style="">
				<label for="company_name" style=""> Submitted By :</label>
				<input type="text" value="{{$data->submitted_by}}" style="" />
			</div>
			<div class="col right_col" style="">
				<label for="company_name" style=""> Date:</label>
				<input type="text" value="{{$data->date}}" style="" />
			</div>
		</div>
	</div>
  </div>

  <footer>
    <div class="footer-section-table">
    	<div class="footer-section-row">
	    	<div class="footer-part footer-one">
	    		<img src="{{ public_path() . '/assets/img/logo.png' }}">
	    	</div>
	    	<div class="footer-part footer-two">
	    		<p>903 W. Industrial Ave. Midland, TX 79701</p>
	    	</div>
	    	<div class="footer-part footer-three">
	    		<p></i>432-253-9651</p>
	    	</div>
	    	<div class="footer-part footer-four">
	    		<p>info@evoilfieldservices.com</p>
	    	</div>
	    </div>
    </div>
</footer>

</body>
</html>
