@extends('layouts.email')

@section('agreement_id')
    <div class="email_agreement_id_badge text-right">
      <div class="timeline-heading">
        <h4 class="badge badge-pill">SC-{{$data->id}} </h4>
      </div>
    </div>                        
  </div>
@endsection


@section('content')

    <form id="regForm" method="post" action="{{ route('insurance.store') }}" class="sub_agreement_form" enctype="multipart/form-data" onsubmit="return validateMyForm();">
      @csrf
      <input type="hidden" name="tab" value="subcontractor_agreement">
      <input type="hidden" class="status" name="status" value="{{$status}}">
      <input type="hidden" name="p_key" value="{{$data->id}}">
      <input type="hidden" name="url_token" value="{{$url_token}}">

        <!-- Circles which indicates the steps of the form: -->
      <div class="card subcontractor_agreement_card_section insurance_parent_tab_content" id="top_subcontractor_card">
        <!-- <div class="card-header card-header-rose card-header-icon">
          <div class="card-icon">
            <i class="material-icons">assignment</i>
          </div>
          <h4 class="card-title">Subcontractor Agreement</h4>
        </div> -->
        <div class="card-body subcontractor_agreement_card_body">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="main-title">SUBCONTRACTOR AGREEMENT</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-1 progress_container" style="text-align:center;margin-bottom:20px;">
              <span class="step step-first active">
                <i class="material-icons">done</i>
              </span>
              <span class="step step-inner">
                <i class="material-icons">done</i>
              </span>
              <span class="step step-inner">
                <i class="material-icons">done</i>
              </span>
              <span class="step step-inner">
                <i class="material-icons">done</i>
              </span>
              <span class="step step-inner">
                <i class="material-icons">done</i>
              </span>
              <span class="step step-last">
                <i class="material-icons">done</i>
              </span>
              <span class="step">
                <i class="material-icons">done</i>
              </span>
              <span class="step">
                <i class="material-icons">done</i>
              </span>
            </div>
            <div class="col-md-11 content_container">
              <div class="tab">
                <div class="row input_row">
                  <div class="col-md-12">
                    <div>
                      <span>This Subcontractor Agreement (“Agreement”) is entered into and shall be effective as of the</span>
                      <span>
                        <div class="form-group bmd-form-group day day-month-year">
                          <input type="number" class="form-control form_input_field form_fields field_to_fill sub_agree_day" id="sub_agree_day" name="day" placeholder="Day" min="1" max="31" value="{{$data->day}}">
                        </div>
                      </span>
                      <span>day of</span> 
                      <span class="month_span">

                        <div class="input_wrapper custom_select2_col month">
                          <div class="form-group custom-form-select">
                            <select class="custom-select contractor_select select2 form_input_field field_to_fill subContractoMonth" id="subContractoMonth " name="month">
                              <option class="form-select-placeholder"></option>
                              <option value="january" @if($data->month == 'january') selected @endif>January</option>
                              <option value="february" @if($data->month == 'february') selected @endif>February</option>
                              <option value="march" @if($data->month == 'march') selected @endif>March</option>
                              <option value="april" @if($data->month == 'april') selected @endif>April</option>
                              <option value="may" @if($data->month == 'may') selected @endif>May</option>
                              <option value="june" @if($data->month == 'june') selected @endif>June</option>
                              <option value="july" @if($data->month == 'july') selected @endif>July</option>
                              <option value="august" @if($data->month == 'august') selected @endif>August</option>
                              <option value="september" @if($data->month == 'september') selected @endif>September</option>
                              <option value="october" @if($data->month == 'october') selected @endif>October</option>
                              <option value="october" @if($data->month == 'october') selected @endif>November</option>
                              <option value="december" @if($data->month == 'december') selected @endif>December</option>
                            </select>
                            <div class="form-element-bar">
                            </div>
                            <label class="form-element-label" for="contractor_select input_label">Enter a month</label>
                          </div>
                        </div>
                      </span>
                      <span>
                        <div class="form-group bmd-form-group year day-month-year">
                          <input type="number" class="form-control form_input_field form_fields field_to_fill" id="sub_agree_year" name="year" placeholder="Year *" value="{{$data->year}}">
                      </div>
                      </span>
                      <span>
                         (the “Effective Date”), by and among EV Oilfield Services, LLC (“Contractor”), and
                      </span>
                    </div>
                  </div>
                </div>
                <fieldset class="custom_fieldset_2 contractor_fieldset">
                  <legend>Contact Form</legend>
                  <div class="row input_row">
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="subcontractor_name" class="bmd-label-floating input_label">Subcontractor *</label>
                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="subcontractor_name" name="subcontractor_name" value="{{$data->subcontractor_name}}">
                        </div>
                    </div>
                    <div class="col-md-6 input_wrapper custom_select2_col">
                      <div class="form-group custom-form-select">
                        <select class="custom-select contractor_select select2 form_input_field field_to_fill" id="contractor_select" name="type_of_entity">
                          <option class="form-select-placeholder"></option>
                          <option value="corporation" @if($data->type_of_entity == 'corporation') selected @endif>Corporation</option>
                          <option value="llc" @if($data->type_of_entity == 'llc') selected @endif>LLC</option>
                          <option value="limited_partnership" @if($data->type_of_entity == 'limited_partnership') selected @endif>Limited Partnership</option>
                          <option value="sole_proprietorship" @if($data->type_of_entity == 'sole_proprietorship') selected @endif>Sole Proprietorship</option>
                        </select>
                        <div class="form-element-bar">
                        </div>
                        <label class="form-element-label" for="contractor_select input_label">Type of Entity *</label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="mailing_addr" class="bmd-label-floating input_label">Mailing Address *</label>
                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="mailing_addr" name="mailing_addr" value="{{$data->mailing_addr}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="physical_addr" class="bmd-label-floating input_label">Physical Address (if different from mailing Address) *</label>
                          <input type="text" class="form-control form_input_field form_fields" id="physical_addr" name="physical_addr" value="{{$data->physical_addr}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contact_person" class="bmd-label-floating input_label">Contact Person *</label>
                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="contact_person" name="contact_person" value="{{$data->contact_person}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contact_person_title" class="bmd-label-floating input_label">Title *</label>
                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="contact_person_title" name="contact_person_title" value="{{$data->contact_person_title}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contact_person_email" class="bmd-label-floating input_label">Email *</label>
                          <input type="email" class="form-control form_input_field form_fields field_to_fill" id="contact_person_email" name="contact_person_email" value="{{$data->contact_person_email}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contact_person_phone" class="bmd-label-floating input_label">Phone *</label>
                          <input type="text" class="form-control form_input_field form_fields mobile field_to_fill" id="contact_person_phone" name="contact_person_phone" value="{{$data->contact_person_phone}}">
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contact_person_fax" class="bmd-label-floating input_label">Fax *</label>
                          <input type="text" class="form-control form_input_field form_fields mobile field_to_fill" id="contact_person_fax" name="contact_person_fax" value="{{$data->contact_person_fax}}">
                        </div>
                    </div> -->
                  </div>
                </fieldset>
                <div class="row agrement_tab_content">
                  <div class="col-md-12">
                    <p>Contractor and Subcontractor may be individually referred to herein as a “Party” or collectively as the “Parties.” </p>
                  </div>
                </div>
                <div class="row agrement_tab_content">
                  <div class="col-md-12">
                    <h4 class="title">CONSPICUOUS AND FAIR NOTICE</h4>
                    
                    <p>Both parties represent to each other that --</p>
                    <ul class="subcontractor_agre_list bold">
                      <li>
                        They have consulted an attorney concerning this agreement or, if they have not consulted an attorney, they were given the opportunity and had the ability to consult, but made an informed decision not to do so, and
                      </li>
                      <li>
                        They fully understand their rights and obligations under this agreement.
                      </li> 
                    </ul>
                    <p>Now, Therefore, in consideration of the mutual promises, conditions and agreements herein contained, the sufficiency of which is acknowledged, Contractor and Subcontractor hereby agree as follows:</p>
                  </div>
                </div>
              </div>

              <div class="tab">
                <div class="row agrement_tab_content">
                  <div class="col-md-12">
                    <div class="section_head_content">
                      <h4 class="sub-title">1. Scope of Agreement</h4>
                      
                      <p>
                        Work for Hire. It is contemplated that from time to time Subcontractor will be requested by Contractor or its present or future affiliated entities to perform certain work and services (“Work”) .<span class="dots">...</span> <span class="more">Neither Contractor nor its affiliates shall be obligated to request Subcontractor to perform any Work, and Subcontractor shall not be obligated to accept requests to perform Work from either Contractor or its affiliates, but it is expressly understood and agreed that any and all Work requested by Contractor or its affiliates and accepted by Subcontractor shall be controlled and governed by the provisions of this Agreement. If and when Contractor desires Subcontractor to perform Work hereunder, Contractor will issue a Work order describing the Work Contractor desires Subcontractor to perform and the compensation Contractor will pay Subcontractor for the Work described in the Work order.  Subcontractor shall notify Contractor in writing within five (5) business days of Subcontractor’s receipt of such Work order if Subcontractor intends to accept the Work order. The term “Agreement” means this Agreement as incorporated in a work order and the term “Contractor” as used herein shall mean the Contractor or affiliated entity that issued the work order.</span>
                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                      </p>
                    </div>

                    <div class="section_head_content">
                       <h4 class="sub-title">2. Term and Termination</h4>
                       
                       <p>This Agreement shall remain in effect for a period of one year from the Effective Date, and from year to year thereafter, subject to the right of either party here to cancel or terminate the Agreement at any time upon not less than thirty (30) days written notice of one party to the other.</p>
                    </div>

                    <div class="section_head_content">
                      <h4 class="sub-title">3. Representations and Warranties</h4>
                      
                      <p>Subcontractor will furnish all necessary materials, equipment, permits, and certificates that are required for the Work. All of Subcontractor’s materials and equipment are suitable for their intended use, and are free from all faults and defects. Subcontractor will remove and replace any defective materials <span class="dots">...</span><span class="more"> or Work forthwith on notice from Contractor.Subcontractor will perform the Work entirely at Subcontractor’s risk. Subcontractor will provide all proper and sufficient and necessary safeguards against all injuries and damage whatsoever, and to comply with all safety requirements imposed by law. Subcontractor shall conform to Contractor’s reasonable progress schedule. Subcontractor shall perform the Work in a professional, prompt, and diligent manner, consistent with applicable law and industry standards, without delaying or hindering Contractor’s work. If Subcontractor shall default in performance of the work or otherwise commit any act which causes delay to Contractor’s work, Subcontractor shall be liable for all losses, costs, expenses, liabilities and damages, including actual damages, consequential damages and any liquidated damages sustained by Contractor. Subcontractor will comply with all applicable federal, state and local laws, codes, ordinances, rules, regulations, orders and decrees of any government or quasi-government entity having jurisdiction over the project, the project site, the practices involved in the Work, or any subcontract work. Subcontractor, its agents and employees are properly trained and qualified to complete the Work for which they have been hired to do.</span>
                      <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                      </p>
                    </div>

                    <div class="section_head_content">
                      <h4 class="sub-title">4. Indemnification</h4>
                      
                      <p>
                        “Claims” shall include, without limitation, any and all claims, losses, damages (including, without limitation, punitive damages and damages for bodily injury, death or property damage ), causes of action, fines, penalties, enforcement proceedings, suits, and liabilities of every nature or character (including interest and all expenses of litigation, court costs, and attorneys' fees), <span class="dots">...</span><span class="more">whether or not arising in tort, contract, strict liability, under statute, or of any other character whatsoever, and whether or not caused by a legal duty. “Subcontractor Group” means subcontractor, its parent, subsidiary and affiliated companies, and their contractors (of whatever tier), and its and their respective directors, officers, employees, agents, and representatives. “Contractor Group” means Contractor, its parent, subsidiary and affiliated companies, its and their co-lessees, partners, joint ventures, co-owners, contractors (other than Subcontractor), and its and their respective directors, officers, employees, agents, and representatives.Subcontractor shall defend, indemnify, and hold harmless the Contractor Group for any Claims which any or all of the Contractor Group may incur in connection with any third party Claim arising out of or relating to the Subcontractor Group’s (or any member of the Subcontractor Group’s) actual or alleged negligence, willful misconduct, breach of this Agreement or violation of Law.  The foregoing indemnification obligation includes any Claims asserted against the Contractor Group (or any member thereof) arising from pollution or contamination which originates above the surface of the land or water from spills of fuels, lubricants, motor oils, pipe dope, paints, solvents, cleaning solutions or other liquids, fumes and garbage which is in any manner associated with or alleged to have been associated with, resulting from or caused by Subcontractor Group’s Work, equipment or personnel </span>
                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="tab">
                <div class="row agrement_tab_content padding-content">
                  <div class="col-md-12">
                    <div class="section_head_content">
                      <h4 class="sub-title">5. Insurance</h4>
                      
                      <p>
                        Subcontractor, at its own cost and expense, shall procure, carry, and maintain insurance coverage satisfactory to Contractor, which shall include not less than the following coverage:
                      </p>
                      <ul class="subcontractor_agre_list evos_field">
                        <li>General liability insurance with limits not less than $
                          <span>
                            <div class="form-group bmd-form-group insurance_limit_currency">
                              <input type="text" class="form-control form_input_field form_fields required" id="general_liability_limit" name="general_liability_limit" placeholder="" value="{{$data->general_liability_limit}}">
                          </div>
                          </span>
                        per occurrence.</li>
                        <li>Automobile Liability insurance with limits not less than $
                        <span>
                            <div class="form-group bmd-form-group insurance_limit_currency">
                              <input type="text" class="form-control form_input_field form_fields required" id="automobile_liability_limit" name="automobile_liability_limit" placeholder="" value="{{$data->automobile_liability_limit}}">
                          </div>
                        </span>
                        per occurrence.</li>
                        <li>Umbrella insurance with limits not less than $
                        <span>
                          <div class="form-group bmd-form-group insurance_limit_currency">
                            <input type="text" class="form-control form_input_field form_fields required" id="umbrella_limit" name="umbrella_limit" placeholder="" value="{{$data->umbrella_limit}}">
                          </div>
                        </span>
                        per occurrence.</li>
                        <li>Workers Compensation and Employers’ Liability with limits not less than
                        <span>
                          <div class="form-group bmd-form-group insurance_limit_currency">
                            <input type="text" class="form-control form_input_field form_fields required" id="employer_liability_limit" name="employer_liability_limit" placeholder="" value="{{$data->employer_liability_limit}}">
                          </div>
                        </span>                                            
                        </li>
                        <li>Riggers Liability Insurance with limits not less than 
                        <span>
                          <div class="form-group bmd-form-group insurance_limit_currency">
                            <input type="text" class="form-control form_input_field form_fields required" id="riggers_liability_limit" name="riggers_liability_limit" placeholder="" value="{{$data->riggers_liability_limit}}">
                          </div>
                        </span> 
                        per occurrence.</li>
                        <li>Motor Trade Cargo Insurance with limits not less than
                        <span>
                          <div class="form-group bmd-form-group insurance_limit_currency">
                            <input type="text" class="form-control form_input_field form_fields required" id="motor_trade_limit" name="motor_trade_limit" placeholder="" value="{{$data->motor_trade_limit}}">
                          </div>
                        </span> 
                        per occurrence.</li>
                        <li>Installation Floater Insurance with limits not less than 
                        <span>
                          <div class="form-group bmd-form-group insurance_limit_currency">
                            <input type="text" class="form-control form_input_field form_fields required" id="installation_floter_limit" name="installation_floater_limit" placeholder="" value="{{$data->installation_floater_limit}}">
                          </div>
                        </span>
                        per occurrence.</li>
                        <li>Any other insurance required by Contractor.</li>
                      </ul>
                    </div>
                    <div class="section_head_content">
                      <p>Subcontractor shall name Contractor as an additional insured on all policies of insurance. Subcontractor shall cause its insurance carrier to forward forthwith to Contractor a standard certificate of insurance such certificate shall require the insurance carrier to give Contractor written thirty (30) days prior notice of the cancellation <span class="dots">...</span><span class="more"> of such insurance. Subcontractor shall not perform any work under the Agreement until a Certificate of Insurance has been delivered to Contractor. If Subcontractor fails to furnish current evidence upon demand of any insurance required hereunder, or if any insurance is cancelled or materially changed, Contractor may suspend or terminate this Agreement until insurance is obtained. The insurance coverage required herein shall in no way limit the Subcontractor’s indemnification obligations or other  liability under this Agreement.</span>
                      <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                      </p>
                    </div>
                    <div class="section_head_content">
                      <h4 class="sub-title">6. Waiver of Subrogation</h4>
                      
                      <p>
                        Subcontractor hereby waives any right of subrogation that it, or anyone claiming through or under it, may have against contractor, its agents, employees, or anyone for <span class="dots">...</span><span class="more"> whom blakely construction company, inc. may be responsible, for any loss or damage shall have been caused by the fault or negligence of contractor. However, this waiver shall not adversely or impair any policies of insurance or prejudice the right of either party to recover thereunder. Subcontractor shall provide an endorsement from subcontractor’s subcontractor or subcontractors that any right of subrogation is waived as against contractor and that such waiver will not impair subcontractor's policies of insurance. Any failure or delay by contractor to require such an endorsement shall not constitute a waiver, nor shall it otherwise affect this mutual waiver of subrogation.</span>
                        <button class="btn btn-link btn_read_more" type="button">Read More</button>  
                      </p>
                    </div>
                    <div class="section_head_content">
                      <h4 class="sub-title">7. Independent Contractor</h4>
                      
                      <p>
                        Subcontractor shall have the status of an "independent contractor". Subcontractor shall employ and direct all persons engaged in the performance<span class="dots">...</span> <span class="more"> of any and all of its services under this Agreement, and such agents, servants or employees are subject to the sole control and direction of Subcontractor, and shall not be agents, servants or employees of Contractor. Subcontractor’s agents, contractors and employees shall not be entitled to any compensation, benefits or other remuneration from Contractor. Contractor shall not have any authority to supervise or direct the manner in which Subcontractor shall perform the services to be rendered hereunder. Subcontractor shall provide and be responsible for providing, using and maintaining all equipment, machinery and tools necessary to perform the Work.</span>
                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                      </p>
                    </div>
                  </div>
                </div>
              </div>


              <div class="tab">
                <div class="row agrement_tab_content">
                  <div class="col-md-12">
                    
                    <div class="section_head_content">
                      <h4 class="sub-title">8. Notices</h4>
                      
                      <p>
                        All notices and other communications pertaining to this Agreement shall be in writing and shall be deemed to have been given by a party hereto <span class="dots">...</span> <span class="more">if personally delivered to the other party or if sent by certified mail, return receipt requested, postage prepaid. All notices shall be addressed as shown below</span>
                         <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                      </p>
                    </div>
                    
                  </div>
                </div>
                <fieldset class="custom_fieldset_2 contractor_fieldset evos_field">
                  <legend>Contractor Form</legend>
                  <div class="row input_row">
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contractor_name_2" class="bmd-label-floating input_label">Contractor</label>
                          <input type="text" class="form-control form_input_field form_fields" id="contractor_name_2" name="notice_contractor_name" value="{{$data->notice_contractor_name}}">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contractor_address" class="bmd-label-floating input_label">Address</label>
                          <input type="text" class="form-control form_input_field form_fields" id="contractor_address" name="notice_contractor_address" value="{{$data->notice_contractor_address}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contractor_phone" class="bmd-label-floating input_label">Phone</label>
                          <input type="text" class="form-control form_input_field form_fields" id="contractor_phone" name="notice_contractor_phone" value="{{$data->notice_contractor_phone}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contractor_email" class="bmd-label-floating input_label">Email</label>
                          <input type="email" class="form-control form_input_field form_fields" id="contractor_email" name="notice_contractor_email" value="{{$data->notice_contractor_email}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contractor_attn" class="bmd-label-floating input_label">Attn</label>
                          <input type="text" class="form-control form_input_field form_fields" id="contractor_attn" name="notice_contractor_attn" value="{{$data->notice_contractor_attn}}">
                        </div>
                    </div>
                  </div>
                </fieldset>
                <fieldset class="custom_fieldset_2 contractor_fieldset">
                  <legend>Subcontractor Form</legend>
                  <div class="row input_row">
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="subcontractor_name_2" class="bmd-label-floating input_label">Subcontractor Name *</label>
                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="subcontractor_name_2" name="notice_subcontractor_name" value="{{$data->notice_subcontractor_name}}">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="subcontractor_address" class="bmd-label-floating input_label">Address *</label>
                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="subcontractor_address" name="notice_subcontractor_address" value="{{$data->notice_subcontractor_address}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="subcontractor_phone" class="bmd-label-floating input_label">Phone *</label>
                          <input type="text" class="form-control form_input_field form_fields field_to_fill mobile" id="subcontractor_phone" name="notice_subcontractor_phone" value="{{$data->notice_subcontractor_phone}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="subcontractor_email" class="bmd-label-floating input_label">Email *</label>
                          <input type="email" class="form-control form_input_field form_fields field_to_fill" id="subcontractor_email" name="notice_subcontractor_email" value="{{$data->notice_subcontractor_email}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="subcontractor_contact" class="bmd-label-floating input_label">Contact *</label>
                          <input type="text" class="form-control form_input_field form_fields mobile field_to_fill" id="subcontractor_contact" name="notice_subcontractor_contact" value="{{$data->notice_subcontractor_contact}}">
                        </div>
                    </div>
                  </div>
                </fieldset>
              </div>


              <div class="tab">
                
                <div class="row agrement_tab_content">
                  <div class="col-md-12">
                    <div class="section_head_content">
                      <h4 class="sub-title">9. Breach of Agreement</h4>
                      
                      <p>
                        If Subcontractor breaches any of the terms of this agreement, then Contractor has the option, at its sole and absolute discretion, to terminate this agreement immediately, without written notice.<span class="dots">...</span> <span class="more">Failure of Contractor to insist upon Subcontractor’s performance under this Agreement or to exercise any right or privilege shall not be a waiver of any of Contractor’s rights or privileges contained herein</span>
                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                      </p>
                    </div>
                    <div class="section_head_content">
                      <h4 class="sub-title">10. Record Keeping</h4>
                      
                      <p>
                        For the purposes of permitting verification by the Contractor of any amounts paid to or claimed by the Subcontractor, the Subcontractor shall keep and preserve, for not less than two years from date of invoice all general<span class="dots">...</span> <span class="more">ledgers, work orders, receipts, disbursements journals, bids, bid proposals, price lists, and supporting documentation obtained from manufacturers and suppliers in connection with the Work performed.</span>
                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                      </p>
                    </div>
                    <div class="section_head_content">
                      <h4 class="sub-title">11. Reports</h4>
                      
                      <p>
                        Subcontractor shall provide to Contractor an oral report, confirmed in writing as soon as practicable, of all accidents or occurrences resulting in death or injuries to Subcontractor’s employees, subcontractors, or any third parties, damage<span class="dots">...</span> <span class="more">to Contractor’s property, or physical damage to the property of Contractor or any third party, arising out of or during the course of work to be performed. Subcontractor shall furnish Contractor with a copy of all reports made by Subcontractor to Subcontractor’s insurer, governmental authorities, or to others of such accidents or occurrences.</span>
                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                      </p>
                    </div>
                    <div class="section_head_content">
                      <h4 class="sub-title">12. Payment Terms</h4>
                      
                      <p> Contractor agrees to pay Subcontractor for any work at the rates set forth in the applicable Work order. Subcontractor shall promptly invoice Contractor upon the completion of any Work, but in any <span class="dots">...</span> <span class="more">event shall invoice Contractor (i) at least once every thirty (30) days and (ii) within ten (10) days following completion of all of the Work under any applicable Work order or purchase order, and (iii) in the unlikely event that Work is completed without an applicable work order or purchase order, within ten (10) days of completion of such Work at Subcontractor’s best rates in the geographical area where the Work was completed. Contractor shall remit full payment of all undisputed portions of any and all invoices in full in United States funds by check, bank draft, money order, or bank/wire transfer payable to Contractor at its offices (or bank account) within sixty (60) days following actual receipt of Subcontractor’s invoice. In the event Contractor has a bona fide question or dispute concerning a Subcontractor invoice or a portion thereof, Contractor may withhold the disputed portion of the payment only. Contractor shall give written notice of any disputed amounts to Subcontractor specifying the reasons therefore within sixty (60) days after receipt of the applicable invoice. Invoices submitted later than one hundred twenty (120) days will not be considered for reimbursement.Contractor shall not pay or reimburse Subcontractor for any travel or other expenses unless Contractor approves such expenses in advance and in writing. </span>
                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                      </p>
                    </div>

                    <div class="section_head_content">
                      <h4 class="sub-title">13. Audit</h4>
                     
                      <p>
                        Subcontractor shall maintain, and shall cause any of Subcontractor’s Subcontractors to maintain, a true and correct set of records pertaining to all work performed under each work order, including supporting <span class="dots">...</span> <span class="more">documentation, for two (2) years following completion of the Work. Contractor may, at its expense require Subcontractor, or any of Subcontractor’s Subcontractors, at any time within said two-year period to furnish sufficient evidence, with documentary support, to enable Contractor to verify the correctness and accuracy of payments to Subcontractor or such Subcontractors. Contractor may, following written notice to Subcontractor or such Subcontractor, audit any and all records of Subcontractor and any Subcontractor relating to the work performed by or on behalf of Subcontractor hereunder, and all payments made in regard thereto, in order to verify the accuracy and compliance with this provision; provided however, Contractor shall not have the right to inspect or audit Subcontractor’s trade secrets or any proprietary information. If Contractor’s examination discloses that Subcontractor’s invoices to Contractor were in error, Subcontractor will immediately pay to Contractor any amounts overpaid by Contractor, plus interest from the date of the error at the lesser of one percent (1%) per month or the maximum rate allowed by law.</span>
                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                      </p>
                    </div>
                    
                  </div>
                </div>
              </div>


              <div class="tab">
                
                <div class="row input_row agrement_tab_content">
                  <div class="col-md-12">
                    
                    
                    <div class="section_head_content">
                      <h4 class="sub-title">14. No delay</h4>
                      
                      <p>
                        In no case (unless otherwise directed in writing by the Contractor) shall any claim or dispute, whether or not involving litigation, permit the Subcontractor to delay or discontinue any of the work hereunder, but rather the <span class="dots">...</span> <span class="more">Subcontractor shall diligently pursue the work hereunder while awaiting the resolution of any dispute or claim; provided, however, that the Contractor shall not withhold, pending the resolution of any claim or dispute, the payment of any undisputed sum otherwise due Subcontractor under this Agreement.</span>
                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                      </p>
                    </div>
                    <div class="section_head_content">
                      <h4 class="sub-title">15. Drugs, Alcohol & Firearms</h4>
                     
                      <p>
                        To help ensure a safe, productive work environment, Contractor prohibits the use, transportation and possession of firearms, drugs and/or controlled substances, drug paraphernalia and alcoholic beverages. Illegal drugs shall include, but not be limited <span class="dots">...</span> <span class="more">to, marijuana, amphetamines, barbiturates, opiates, cocaine, codeine, morphine, hallucinogenic substances (LSD) and any similar drugs and/or chemical synthetics deemed hazardous by Contractor. Such prohibitions shall apply to Subcontractor and its employees, agents, servants and subcontractors. Subcontractor’s employees, agents, contractors and invitees shall abide the more stringent of Subcontractor’s or Contractor’s drug, alcohol and firearm policy. Contractor may request that Subcontractor carry out drug and alcohol tests of its employees and/or that Subcontractor carry out reasonable searches of individuals, their personal effects and vehicles when entering on and leaving assigned premises at any time, at scheduled times, or at random. Individuals found in violation shall be removed from the premises by Subcontractor immediately. Submission to such a search is strictly voluntary, however, refusal may be cause for not allowing that individual on the well site or Contractor’s other premises. Subcontractor shall (1) test at Subcontractor’s expense any individual involved in or related to an accident or injury within twelve (12) hours of such accident or injury and (2) submit to Contractor any drug or alcohol test results for any individual involved in or related to an accident or injury on Contractor’s premises. It is Subcontractor’s responsibility to notify its employees, contractors, subcontractors, agents and invitees of this prohibition, the provisions of this paragraph and its enforcement and obtain any acknowledgement or release from any person in order to comply with this provision and applicable law.</span>
                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                      </p>
                    </div>
                    <div class="section_head_content">
                      <h4 class="sub-title">16. Miscellaneous</h4>
                      
                    </div>
                    <div class="section_head_content">
                      <h4 class="sub-title">A. Governing Law</h4>
                      
                      <p>The laws of the State of Texas shall apply and govern the validity, interpretation, and performance of this Agreement. The Parties waive any right to trial by jury in any action arising out of or relating to this Agreement or the Parties’ relationship </p>
                    </div>
                    <div class="section_head_content">
                      <h4 class="sub-title">B. Severability</h4>
                      
                      <p>If any part of this Agreement contravenes any applicable statutes, regulations, rules, or common law requirements, then, to the extent of and only to the extent of such contravention, such part shall be severed from this Agreement and deemed non-binding while all other parts of this Agreement shall remain binding.</p>
                    </div>

                  </div>
                </div>
              </div>

              <div class="tab">
                <div class="row input_row agrement_tab_content">
                  <div class="col-md-12">
                    <div class="section_head_content">
                      <h4 class="sub-title">C. Amendment</h4>
                     
                      <p>This Agreement may not be modified or amended unless it is in writing and signed by both Parties. </p>
                    </div>
                    <div class="section_head_content">
                      <h4 class="sub-title">D. Assignment</h4>
                      
                      <p>Neither Party shall assign all or any part of its rights or obligations under this Agreement without the prior written consent from the other Party. </p>
                    </div>
                    <div class="section_head_content">
                      <h4 class="sub-title">E. Compliance with Laws</h4>
                      
                      <p>Subcontractor agrees to comply with all laws, statutes, codes, rules, and regulations, which are now or may hereafter become applicable to Work covered by this Agreement or arising out of the performance of such operations. </p>
                    </div>
                    <div class="section_head_content">
                      <h4 class="sub-title">F. Headings</h4>
                      
                      <p>The headings, sub-headings, and other subdivisions of this Agreement are inserted for convenience only. The Parties do not intend them to be an aid in legal construction. </p>
                    </div>
                    <div class="section_head_content">
                      <h4 class="sub-title">G. Authority to Sign and Bind</h4>
                     
                      <p>By their signatures below, Subcontractor represents that the person signing this Agreement has the authority to execute same on behalf of Subcontractor and to bind Subcontract to the obligations set forth herein. </p>
                    </div>

                  </div>
                </div>
              </div>

              <div class="tab">
                <div class="row input_row">
                  <div class="col-md-12">
                    <div>
                      <span>Executed this</span>
                      <span>
                        <div class="form-group bmd-form-group day day-month-year day_input_wrapper">
                          <input type="text" class="form-control form_input_field form_fields field_to_fill sub_agree_day_2" id="sub_agree_day_2" name="executed_day" placeholder="Day" value="{{$data->executed_day}}" max="31">
                        </div>
                      </span>
                      <span>day of</span> 
                      <span class="month_span">
                        <div class="input_wrapper custom_select2_col month month_input_wrapper">
                          <div class="form-group custom-form-select">
                            <select class="custom-select contractor_select select2 form_input_field is-filled field_to_fill subContractoMonth2" id="subContractoMonth2" name="executed_month">
                            <option class="form-select-placeholder"></option>
                            <option value="january" @if($data->executed_month == 'january') selected @endif>January</option>
                                <option value="february" @if($data->executed_month == 'february') selected @endif>February</option>
                                <option value="march" @if($data->executed_month == 'march') selected @endif>March</option>
                                <option value="april" @if($data->executed_month == 'april') selected @endif>April</option>
                                <option value="may" @if($data->executed_month == 'may') selected @endif>May</option>
                                <option value="june" @if($data->executed_month == 'june') selected @endif>June</option>
                                <option value="july" @if($data->executed_month == 'july') selected @endif>July</option>
                                <option value="august" @if($data->executed_month == 'august') selected @endif>August</option>
                                <option value="september" @if($data->executed_month == 'september') selected @endif>September</option>
                                <option value="october" @if($data->executed_month == 'october') selected @endif>October</option>
                                <option value="october" @if($data->executed_month == 'october') selected @endif>November</option>
                                <option value="december" @if($data->executed_month == 'december') selected @endif>December</option>
                            </select>
                            <div class="form-element-bar">
                            </div>
                            <label class="form-element-label" for="contractor_select input_label">Enter a month</label>
                          </div>
                        </div>
                      </span>
                      <span>
                        <div class="form-group bmd-form-group year day-month-year year_input_wrapper">
                          <input type="text" class="form-control form_input_field form_fields field_to_fill sub_contractor_year2" id="sub_agree_year_2" name="executed_year" placeholder="Year" value="{{$data->executed_year}}">
                      </div>
                      </span>
                    </div>
                  </div>
                </div>
                <fieldset class="custom_fieldset_2 contractor_fieldset evos_field">
                  <legend>Contractor</legend>
                  <div class="row input_row">
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="Contractor_name_3" class="bmd-label-floating input_label">Contractor</label>
                          <input type="text" class="form-control form_input_field form_fields" id="Contractor_name_3" name="executed_contractor_name" value="{{$data->executed_contractor_name}}">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contractor_by" class="bmd-label-floating input_label">Contractor By</label>
                          <input type="text" class="form-control form_input_field form_fields" id="contractor_by" name="executed_contractor_by" value="{{$data->executed_contractor_by}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contractor_title" class="bmd-label-floating input_label">Title</label>
                          <input type="text" class="form-control form_input_field form_fields" id="contractor_title" name="executed_contractor_title" value="{{$data->executed_contractor_title}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contractor_date" class="bmd-label-floating input_label">Date *</label>
                          <input type="text" class="form-control form_input_field form_fields datepicker" id="contractor_date" name="executed_contractor_date" value="{{$data->executed_contractor_date}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="contractor_initial" class="bmd-label-floating input_label">Contractor Initial *</label>
                          <input type="text" class="form-control form_input_field form_fields" id="contractor_initial" name="contractor_initial" value="{{$data->contractor_initial}}">
                        </div>
                    </div>
                    
                  </div>
                </fieldset>

                <fieldset class="custom_fieldset_2 contractor_fieldset">
                  <legend>Sub Contractor</legend>
                  <div class="row input_row">
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="Subcontractor_name_3" class="bmd-label-floating input_label">Sub Contractor *</label>
                          <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="Subcontractor_name_3" name="executed_subcontractor_name" value="{{$data->executed_subcontractor_name}}">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="subcontractor_by" class="bmd-label-floating input_label">Sub Contractor By *</label>
                          <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="subcontractor_by" name="executed_subcontractor_by" value="{{$data->executed_subcontractor_by}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="subcontractor_title" class="bmd-label-floating input_label">Title *</label>
                          <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="subcontractor_title" name="executed_subcontractor_title" value="{{$data->executed_subcontractor_title}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="subcontractor_date" class="bmd-label-floating input_label">Date *</label>
                          <input type="text" class="form-control form_input_field form_fields datepicker field_to_fill field_to_fill_validator" id="subcontractor_date" name="executed_subcontractor_date" value="{{$data->executed_subcontractor_date}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                        <label for="subcontractor_initial" class="bmd-label-floating input_label">Subcontractor Initial *</label>
                          <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="subcontractor_initial" name="subcontractor_initial" value="{{$data->subcontractor_initial}}">
                        </div>
                    </div>
                    
                  </div>
                </fieldset>

                <fieldset class="custom_fieldset_2 contractor_fieldset">
                  <legend>Upload necessary file</legend>
                  <div class="row input_row">
                    <div class="col-md-12">
                      <label for="multiple_file input_label"> Upload W9 * </label>
                      <div class="agreement_w9_2 agreement_upload">
                      </div>
                    </div>

                    <div class="col-md-12">
                      <label for="multiple_file input_label"> Upload Insurace Certificate * </label>
                      <div class="insurance_certificate_2 agreement_upload">
                      </div>
                    </div>
                  </div>
                </fieldset>

                <div class="row input_row agrement_checkbox_row">
                  <div class="col-md-8 agrement_checkbox">
                    <div class="form-check">
                        <label class="form-check-label">
                          <input class="form-check-input" id="agree_chk" type="checkbox" value=""> Upon clicking this checkbox, I agree with all terms and condition described in this agreement.
                          <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                        </label>
                      </div>
                  </div>
              <!--                   <div class="col-md-4">
                    <div class="form-group bmd-form-group">
                      <label for="signed_by" class="bmd-label-floating input_label">Sign By (Your name) *</label>
                      <input type="text" class="form-control form_input_field form_fields" id="signed_by" name="signed_by" value="{{$data->signed_by}}" required="">
                    </div>
                  </div> -->
                </div>

                @if($status != 2 && $status != 4)
                <fieldset class="custom_fieldset_2 contractor_fieldset mt-5 mb-3">
                  <legend>Sending Information</legend>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                      <label for="agreement_send_mail_to" class="bmd-label-floating input_label">Send Mail To</label>
                        <input type="text" class="form-control form_input_field form_fields" id="agreement_send_mail_to" name="agreement_send_mail_to" value="{{$data->agreement_send_mail_to}}" required="">
                      </div>                                          
                    </div>
                    <div class="col-md-6">
                      <div class="form-group bmd-form-group">
                      <label for="agreement_send_mail_date" class="bmd-label-floating input_label">Date</label>
                        <input type="text" class="datepicker form-control form_input_field form_fields" id="agreement_send_mail_date" name="agreement_send_mail_date" value="{{$data->agreement_send_mail_date}}" required="">
                      </div>                                          
                    </div>
                  </div>
                </fieldset>
                @else
                <input type="hidden" class="form-control form_input_field form_fields" id="agreement_send_mail_to" name="agreement_send_mail_to" value="{{$data->agreement_send_mail_to}}">
              
                @endif

                <input type="hidden" name="submitted_by_name" value="{{ $data->submitted_by_name }}">
                <input type="hidden" name="submitted_by_email" value="{{ $data->submitted_by_email }}">

                @if($sc_sign)
                <input type="hidden" class="sc_sign" value="{{$sc_sign->url}}">
                @endif

                <div class="row input_row mt-2">
                  <div class="col-md-12 draw_signature_col text-center">
                      <label class="" for="">Signature (Drag Your cursor to sign below)</label>
                      <label class="sign_error_msg" style="display: none;" for="">Please Put Your Signature</label>
                      <br/>
                      <div id="sig" ></div>
                      @if($sc_sign)
                          <input type="hidden" class="sign_input_validation" value="1">
                      @else
                          <input type="hidden" class="sign_input_validation" value="0">
                      @endif
                      
                      <br/>
                      <button id="clear" class="btn btn-sm btn-round btn-danger my-3">Clear Signature</button>
                      <textarea id="signature64" name="signed" style="display: none"></textarea>
                  </div>
                  <!-- <canvas id='blank' style='display:none'></canvas> -->
                </div>       

              </div> 



              <div style="overflow:auto;" class="row msf_btn_holder">
                <div class="col-md-6 save_btn_wrapper text-left" style="float:right;">
                  <a class="btn btn-round btn-warning nextPrevBtnOne prev" href="#top_subcontractor_card" type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</a>
                </div>
                <div class="col-md-6 save_btn_wrapper text-right">
                  <a class="btn custom-btn-one btn-round btn-success nextPrevBtnOne next nextBtnAgreement" href="#top_subcontractor_card" type="button" id="nextBtn" onclick="nextPrev(1)">Next</a>

                  <button class="btn custom-btn-one btn-round btn-success nextPrevBtnOne submitBtn nextBtnAgreement" style="display: none" type="submit" id="" onclick="">Submit</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </form>


<script>

$(function(){
  if($('.status').val() == 2 || $('.status').val() == 4){
    $('.evos_field').find('input').attr('readonly', true);
    $('.field_to_fill').each(function(){
      $(this).addClass('required')
      if($(this).hasClass('field_to_fill_validator')){
        $(this).attr('required',true)
      }
    })
  }
  if($('.status').val() == 3){
    //$('.evos_field').find('input').attr('readonly', true);
  }
});

$(document).ready(function() {
  let value = $('input[name="p_key"]').val();
  let type = 'subcontractor_agreement_w9';
  let _token = $('input[name="_token"]').val();
  var preloaded = [];
  $.ajax({
            async: false,
            url:"{{ route('getInsuranceImage') }}",
            method:"POST",
            data:{value:value, type:type, _token:_token},
            success:function(result)
            {
              var obj = JSON.parse(result)
              $.each(obj, function(index) {
                var data = { id: obj[index].id, src: '<a href="'+obj[index].file+'" download>'+obj[index].name+'</a>' };
                preloaded.push(data);
              });

              preloadedImage(preloaded);
            }
         })
  function preloadedImage(param) {
    $('.agreement_w9_2').fileUploader({
      preloaded: param,
      imagesInputName:'subcontractor_agreement_w9',
      preloadedInputName:'preloaded',
      label:'Drag & Drop files here or click to browse',
      extensions: ['.pdf','.doc','.docx','DOCX','.jpg','.jpeg','.png','.gif','.svg'],
      required: true,
      mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','image/jpeg','image/png','image/gif','image/svg+xml']
  });
}


let value2 = $('input[name="p_key"]').val();
  let type2 = 'subcontractor_insurance_certificate';
  let _token2 = $('input[name="_token"]').val();
  var preloaded2 = [];
  $.ajax({
            async: false,
            url:"{{ route('getInsuranceImage') }}",
            method:"POST",
            data:{value:value2, type:type2, _token:_token2},
            success:function(result)
            {
              var obj = JSON.parse(result)
              $.each(obj, function(index) {
                var data = { id: obj[index].id, src: '<a href="'+obj[index].file+'" download>'+obj[index].name+'</a>' };
                preloaded2.push(data);
              });

              preloadedImage2(preloaded2);
            }
         })
  function preloadedImage2(param) {
    $('.insurance_certificate_2').fileUploader({
      preloaded: param,
      imagesInputName:'subcontractor_insurance_certificate',
      preloadedInputName:'preloaded2',
      label:'Drag & Drop files here or click to browse',
      extensions: ['.pdf','.doc','.docx','DOCX','.jpg','.jpeg','.png','.gif','.svg'],
      required: true,
      mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','image/jpeg','image/png','image/gif','image/svg+xml']
  });
}


});

$(function(){
  /*$('.agreement_w9_2').fileUploader({
    imagesInputName: 'subcontractor_agreement_w9',
    extensions: ['.pdf','.doc','.docx'],
    mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document']
  });*/
})
$(function(){
/*  $('.insurance_certificate_2').fileUploader({
    imagesInputName: 'subcontractor_insurance_certificate',
    extensions: ['.pdf','.doc','.docx'],
    mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document']
  });*/
})

$('.month select').on('change',function(){
  $(this).siblings('label').text('')
})

if($('.month select').val()!=""){
  $('.month select').siblings('label').text("");
}


$(document).ready(function() {

  //month label hide
  if($('#subContractoMonth').val() !== ''){
    $('#subContractoMonth').parent().children('label').text('');
  }
  if($('#subContractoMonth2').val() !== ''){
    $('#subContractoMonth2').parent().children('label').text('');
  }

  if($('#contractor_select').val() !== ''){
    $('#contractor_select').addClass( "hasvalue" );
  }

  //year picker 
var date = new Date();
var only_year = date.getFullYear();



  $('#sub_agree_year').yearpicker({
  endYear:only_year,
  year:null,
  template: `<div class="yearpicker-container">
              <div class="yearpicker-header">
                  <div class="yearpicker-prev" data-view="yearpicker-prev">&lsaquo;</div>
                  <div class="yearpicker-current" data-view="yearpicker-current">SelectedYear</div>
                  <div class="yearpicker-next" data-view="yearpicker-next">&rsaquo;</div>
              </div>
              <div class="yearpicker-body">
                  <ul class="yearpicker-year sub_contractor_year_email" data-view="years">
                  </ul>
              </div>
          </div>`
});

$('#sub_agree_year_2').yearpicker({
  endYear:only_year,
  year:null,
  template: `<div class="yearpicker-container">
              <div class="yearpicker-header">
                  <div class="yearpicker-prev" data-view="yearpicker-prev">&lsaquo;</div>
                  <div class="yearpicker-current" data-view="yearpicker-current">SelectedYear</div>
                  <div class="yearpicker-next" data-view="yearpicker-next">&rsaquo;</div>
              </div>
              <div class="yearpicker-body">
                  <ul class="yearpicker-year sub_contractor_year2_email" data-view="years">
                  </ul>
              </div>
          </div>`
});

//intial value reset
//$('#sub_agree_year').val('');
//$('#sub_agree_year_2').val('');

// Year Validation 
$('.sub_contractor_year_email').on('click','.yearpicker-items',function(){
  console.log("Year Picker Value taken")
  // console.log($(this))
  var getYear, getMonth, getDate, getDateValue,setdateMax;
  // getYear = $('#sub_agree_year').val();
  getYear = $(this).html();  

  getDate = $('#sub_agree_year').parents('span').siblings('span').children('.day').children();

  getDateValue = getDate.val();
  getMonth = $('#sub_agree_year').parents('span').siblings('span.month_span').find('.subContractoMonth').val();

  console.log($('#sub_agree_year').parents('span').siblings('span.month_span').find('.subContractoMonth'))

  setdateMax = getDate.attr('max');
  if(getYear%4 === 0){
    if(getMonth == 'february'){

       getDate.attr('max', '29');
      if(getDateValue>setdateMax){
        getDate.val('');
      }
    }
  }
  else if(getYear%4 != 0){
    if((getMonth == 'february')){
      getDate.attr('max','28');
      if(getDateValue>setdateMax){
        getDate.val('');
      }
    }
  }
})

$('.sub_contractor_year2_email').on('click','.yearpicker-items',function(){
  console.log("Year Picker Value taken")
  var getYear, getMonth, getDate, getDateValue,setdateMax;
  // getYear = $('#sub_agree_year').val();
  getYear = $(this).html();
  

  getDate = $('#sub_agree_year_2').parents('span').siblings('span').children('.day').children();

  getDateValue = getDate.val();
  getMonth = $('#sub_agree_year_2').parents('span').siblings('span.month_span').children('.month').children().children('.subContractoMonth2').val();

  setdateMax = getDate.attr('max');

  if(getYear%4 === 0){
    if(getMonth == 'february'){
       getDate.attr('max', '29');
      if(getDateValue>setdateMax){
        getDate.val('');
      }
    }
  }
  else if(getYear%4 != 0){
    if((getMonth == 'february')){
      getDate.attr('max','28');
      if(getDateValue>setdateMax){
        getDate.val('');
      }
    }
  }
})

//month validation

$('#subContractoMonth').on('change', function(){
  var getMon = $(this).val();
  var setDateLength = $(this).parents('span').siblings('span').children('.day').children();
  if(getMon == 'january' || getMon == 'march' || getMon == 'may' || getMon == 'july' || getMon =='august' || getMon =='october' || getMon =='december' ){
    setDateLength.attr('max', '31');
  }
  else if(getMon == 'april' || getMon == 'june' || getMon == 'september' || getMon == 'november'){
    setDateLength.attr('max', '30');
  }
  else if (getMon == 'february') {
    setDateLength.attr('max', '28');
  }
  var setDateValue = setDateLength.val();
  var setdateMax = setDateLength.attr('max');
  if(setDateValue>setdateMax){
    setDateLength.val('');
  }

});

$('#subContractoMonth2').on('change', function(){
  var getMon = $(this).val();
  var setDateLength = $(this).parents('span').siblings('span').children('.day').children();
  if(getMon == 'january' || getMon == 'march' || getMon == 'may' || getMon == 'july' || getMon =='august' || getMon =='october' || getMon =='december' ){
    setDateLength.attr('max', '31');
  }
  else if(getMon == 'april' || getMon == 'june' || getMon == 'september' || getMon == 'november'){
    setDateLength.attr('max', '30');
  }
  else if (getMon == 'february') {
    setDateLength.attr('max', '28');
  }
  var setDateValue = setDateLength.val();
  var setdateMax = setDateLength.attr('max');
  if(setDateValue>setdateMax){
    setDateLength.val('');
  }

});

//day validation
$('#sub_agree_day').on('blur', function(){
  var dayValue = $(this).val();
  var maxAttr = $(this).attr('max');
  maxAttr = parseInt(maxAttr);
  console.log(maxAttr);
  if($(this).val().length > 1){
    if (!(dayValue>=1 && dayValue<=maxAttr)){
      alert('Please put a date from 1 to '+ maxAttr +'');
      $(this).val('');
      $(this).siblings('.error').hide();
      $(this).addClass('invalid')
    }
    else{
      $(this).removeClass('invalid')
    }  
  }
})

$('#sub_agree_day_2').on('blur', function(){
  var dayValue = $(this).val();
  var maxAttr = $(this).attr('max');
  maxAttr = parseInt(maxAttr);
  console.log(maxAttr);
  if($(this).val().length > 1){
    if (!(dayValue>=1 && dayValue<=maxAttr)){
      alert('Please put a date from 1 to '+ maxAttr +'');
      $(this).val('');
      $(this).siblings('.error').hide();
      $(this).addClass('invalid')
    }
    else{
      $(this).removeClass('invalid')
    }  
  }
})


if($('input[name="preloaded2[]"]').length){
  if($('input[name="preloaded2[]"]').val()){
    $('.insurance_certificate_2 input[name="subcontractor_insurance_certificate[]"]').removeAttr('required')
  }
}

if($('input[name="preloaded[]"]').length){
  if($('input[name="preloaded[]"]').val()){
    $('.agreement_w9_2 input[name="subcontractor_agreement_w9[]"]').removeAttr('required')
  }
}


});



// $('.agreement_upload .remove_file').click(function(){
//   var files = $(this).parents('.agreement_upload').find('.uploaded_file');
//   console.log(files)
// })




function validateMyForm(){
  if($('.insurance_certificate_2 .uploaded-file').length <= 0){
    //console.log($('.insurance_certificate_2 .uploaded_file').length)
    $('.insurance_certificate_2 input[name="subcontractor_insurance_certificate[]"]').attr('required',true)
  }

  if($('.agreement_w9_2 .uploaded-file').length <= 0){
    $('.agreement_w9_2 input[name="subcontractor_agreement_w9[]"]').attr('required',true)
  }


  // SIGNATURE VALIDATION on submit

  var canvas = $('canvas')[0];
  if($('.sign_input_validation').val() == "0"){
    $('.sign_error_msg').show();
    return false;
  }
  else{
    $('.sign_error_msg').hide();
    if($("#agree_chk").prop('checked') == false)
    {
      alert("You have to agree with terms & condition before submit, so please click on the checkbox");
      //e.preventDefault();
      return false;
    }
  }



  return true;
}





  // SIGNATURE VALIDATION





function isCanvasBlank(canvas) {
    var blank = document.createElement('canvas');
    blank.width = canvas.width;
    blank.height = canvas.height;

    blank.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    return canvas.toDataURL() == blank.toDataURL();
}

// end SIGNATURE VALIDATION 



var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear').click(function(e) {
    e.preventDefault();
    sig.signature('clear');
    $("#signature64").val('');
    draw($(this));

    $('.sign_input_validation').val("0");
    if($('.sign_input_validation').val() == "0"){
      $('.sign_error_msg').show();
    }
    else{
      $('.sign_error_msg').hide();
    }



    // $('canvas').attr('width','100');
});


$(document).ready(function(){
if($('.sc_sign').length > 0){
  var canvas = $('.kbw-signature').find('canvas')[0];
  const context = canvas.getContext("2d");

  const img = new Image()
  img.src = $('.sc_sign').val()
  img.onload = () => {
    context.drawImage(img, 0, 0)
  }
}

});

$('#sig canvas').mousedown(function(){
  $(this).parents('#sig').siblings('.sign_input_validation').val("1");
    if($('.sign_input_validation').val() == "0"){
      $('.sign_error_msg').show();
    }
    else{
      $('.sign_error_msg').hide();
    }
})
// var canvas = document.getElementsByTagName('canvas')[0]
// var ctx = canvas.getContext("2d")
function draw(btn) {

  var canvas = btn.siblings('.kbw-signature').find('canvas')[0];
  var ctx = canvas.getContext("2d")
  //This line is actually not even needed...
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.rect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = 'white';
  ctx.fill();
}
$(document).ready(function(){
  // document.getElementsByTagName('canvas').width = 985.72;
  $('canvas').attr('width','985.72');
})



</script>
@endsection