@extends('layouts.master')

@section('content')

        <div class="content manage-inventory-content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12 mx-auto card_holder_box">
                <div class="card">
                  <div class="card-header card-header-tabs card-header-rose">
                    <div class="nav-tabs-navigation">
                      <div class="nav-tabs-wrapper">
                        
                        <ul class="nav nav-tabs" data-tabs="tabs">
                          <li class="nav-item">
                            <a class="nav-link active" href="#profile" data-toggle="tab">
                              <i class="material-icons">ballot</i> Edit 
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                          
                        </ul>
                      </div>
                    </div>
                  </div>
                  
                  <div style="margin-top: 15px;">
                      @include('partials.messages')
                  </div>

                  <div class="card-body" style="padding: 0.9375rem 10px;">
                    
                  
                    @if($type == 'equipment')
                    <div class="tab-pane inventory_sub_card" id="equipment_tab">
                      <form method="post" action="{{ route('insurance.store') }}" class="form-horizontal">
                      @csrf
                      <input type="hidden" name="tab" value="equipment">

                      <div class="row">
                        <div class="col-md-12 mx-auto">
                          <div class="card form_card">
                            <div class="card-header card-header-rose card-header-icon">
                              <div class="card-icon">
                                <i class="material-icons">assignment</i>
                              </div>
                              <h4 class="card-title">Equipment Insurance</h4>
                            </div>

                            <div class="card-body">
                              <div class="row">
                                <div class="col-md-11 mx-auto">

                                  <div class="toolbar">
                                  </div>
                                  <div class="row form_header mb-5">
                                    <div class="col-md-6 mx-auto text-center">
                                      <h2>
                                        Equipment Request
                                      </h2>
                                    </div>
                                    <div class="col-md-12 text-center">
                                      <p>
                                        Please fill out the following information when adding or deleting equipment to your coverage
                                      </p>
                                    </div>
                                  </div>
                                  <div class="row form-top_body mx-auto">
                                    <div class="col-md-12 text-center">
                                      <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input insurance_check" type="checkbox" name="request" value="add" disabled>ADD
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                      <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input insurance_check" type="checkbox" name="request" value="delete">DELETE
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                      <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input insurance_check" type="checkbox" name="request" value="amend">AMEND
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row input_row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="company_name_equip" class="bmd-label-floating input_label">Company Name</label>
                                        <input type="text" class="form-control form_input_field form_fields" id="dr_company_name_equip" name="company_name" value="{{$data->company_name}}">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row input_row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="effective_date_equip" class="bmd-label-floating input_label">Effective Date</label>
                                        <input type="text" class="form-control form_input_field form_fields datepicker" id="effective_date_equip" name="effective_date" value="{{$data->effective_date}}">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row mt-3 mb-3">
                                    <div class="col-md-12 insurance_info">
                                      <h4>Equipment Information : </h4>
                                    </div>
                                  </div>
                                  <div class="row input_row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="year_equip" class="bmd-label-floating input_label">Year</label>
                                        <input type="text" class="form-control form_input_field form_fields" id="year_equip" name="year" value="{{$data->year}}">
                                      </div>
                                    </div>

                                    <div class="col-md-6 input_wrapper ">
                                      <div class="form-group bmd-form-group">
                                        <label for="make_equip" class="bmd-label-floating input_label">Make</label>
                                        <input type="text" class="form-control form_input_field form_fields" id="make_equip" name="make" value="{{$data->make}}">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row input_row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="model_equip" class="bmd-label-floating input_label">Model</label>
                                        <input type="text" class="form-control form_input_field form_fields" id="model_equip" name="model" value="{{$data->model}}">
                                      </div>
                                    </div>

                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="vin_equip" class="bmd-label-floating input_label">VIN</label>
                                        <input type="text" class="form-control form_input_field form_fields" id="vin_equip" name="vin" value="{{$data->vin}}">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row input_row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="unit_equip" class="bmd-label-floating input_label">Unit</label>
                                        <input type="text" class="form-control form_input_field form_fields" id="unit_equip" name="unit" value="{{$data->unit}}">
                                      </div>
                                    </div>

                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="value_equip" class="bmd-label-floating input_label">Value</label>
                                        <input type="text" class="form-control form_input_field form_fields" id="value_equip" name="value" value="{{$data->value}}">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row input_row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="desc_equip_accessories" class="bmd-label-floating input_label">Description of Equipment Accessories</label>
                                        <textarea class="form-control form_input_field form_fields" rows="5" name="description_equipment_accessories" id="desc_equip_accessories">{{$data->description_equipment_accessories}}</textarea>
                                      </div>
                                    </div>
                                  </div>  

                                  <div class="row input_row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="garaging_location_equip" class="bmd-label-floating input_label">Garaging Location/Address</label>
                                        <textarea class="form-control form_input_field form_fields" rows="5" name="garaging_location_address" id="garaging_location_equip">{{$data->garaging_location_address}}</textarea>
                                      </div>
                                    </div>
                                  </div>  

                                  
                                  <div class="row input_row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="lease_purchase_equip" class="bmd-label-floating input_label">Lease or Purchase</label>
                                        <input type="text" class="form-control form_input_field form_fields" id="lease_purchase_equip" name="lease_purchase" value="{{$data->lease_purchase}}">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row input_row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="finance_company_name_equip" class="bmd-label-floating input_label">Finance Company/Lessor Name</label>
                                        <input type="text" class="form-control form_input_field form_fields" id="finance_company_name_equip" name="finance_company_name" value="{{$data->finance_company_name}}">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row input_row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="finance_company_address_equip" class="bmd-label-floating input_label">Finance Company/Lessor Address</label>
                                        <textarea class="form-control form_input_field form_fields" rows="5" name="finance_company_address" id="finance_company_address_equip">{{$data->finance_company_address}}</textarea>
                                      </div>
                                    </div>
                                  </div> 

                                  <div class="row input_row">
                                    <div class="col-md-12 input_wrapper long_input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="company_phone_lessor_equip" class="bmd-label-floating input_label">Finance Company/Lessor Phone or Email for EPI</label>
                                        <input type="text" class="form-control form_input_field form_fields" id="company_phone_lessor_equip" name="finance_company_phone_email" value="{{$data->finance_company_phone_email}}">
                                      </div>
                                    </div>
                                  </div> 

                                  <div class="row input_row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="additional_note_equip" class="bmd-label-floating input_label">Additional Notes</label>
                                        <textarea class="form-control form_input_field form_fields" rows="5" name="additional_notes" id="additional_note_equip">{{$data->additional_notes}}</textarea>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row mt-5">
                                    <div class="col-md-7 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="submitted_to_equip" class="bmd-label-floating input_label">Submitted By</label>
                                        <input type="text" class="form-control form_input_field form_fields" id="submitted_to_equip" name="submitted_by" value="{{$name}} - {{$email}}" readonly>
                                      </div>
                                    </div>
                                    <div class="col-md-5 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="submitted_date_equip" class="bmd-label-floating input_label">Date</label>
                                        <input type="text" class="form-control form_input_field form_fields datepicker" id="submitted_date_equip" name="date" value="{{$data->date}}">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row mt-6 input_row">
                                    <div class="col-md-7 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="send_to_equip" class="bmd-label-floating input_label">Send Mail To</label>
                                        <input type="email" class="form-control form_input_field form_fields" id="send_to_equip" name="send_mail_to" value="{{$data->send_mail_to}}">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>
                          <div class="row preview_row" style="display:none;">
                            <div class="col-md-12 mx-auto insurance_padding">
                              <div class="card preview_card">
                                <div class="card-header card-header-rose card-header-icon">
                                  <div class="card-icon">
                                    <i class="material-icons">assignment</i>
                                  </div>
                                  <h4 class="card-title">Submitted Preview</h4>
                                </div>
                                <div class="card-body">
                                  <div class="row preview_checkbox_row">
                                    <div class="col-md-12 text-center">
                                      <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input insurance_check" type="checkbox" value="add" disabled="">ADD
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                      <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input insurance_check" type="checkbox" value="delete" disabled="">DELETE
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                      <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input insurance_check" type="checkbox" value="amend" disabled="">AMEND
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>                          
                            </div>
                            <div class="col-md-12 mx-auto insurance_padding preview_page_btns_col">
                              <div class="row">
                                <div class="col-md-6 text-left save_btn_wrapper">
                                  <button type="button" class="btn btn-md btn-round btn-warning go_back ">
                                    Go Back
                                  </button>
                                </div>

                                <div class="col-md-6 text-right save_btn_wrapper">
                                  <button type="submit" class="btn btn-md btn-round btn-success confirm">
                                    Confirm
                                  </button> 
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12 text-right save_btn_wrapper">
                          <button type="button" class="btn btn-md btn-round btn-success preview_btn">
                            Preview and Send
                          </button>
                        </div>
                      </div>

                      </form>
                    </div>
                    @endif

                    @if($type == 'vehicle')
                    <div class="tab-pane inventory_sub_card insurance_tab" id="vehicle_tab">
                      <div class="row">
                        <div class="col-md-12">
                          <form method="post" action="{{ route('insurance.store') }}" class="form-horizontal">
                            @csrf
                            <input type="hidden" name="tab" value="vehicle">

                            <div class="card form_card">
                              <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                  <i class="material-icons">assignment</i>
                                </div>
                                <h4 class="card-title">Vehicle Insurance</h4>
                              </div>

                              <div class="card-body">
                                <div class="toolbar">
                                </div>
                                <div class="row">
                                  <div class="col-md-11 mx-auto">
                                    <div class="row form_header mb-5">
                                      <div class="col-md-6 mx-auto text-center">
                                        <h2>
                                          Vehicle Change Request
                                        </h2>
                                      </div>
                                      <div class="col-md-12 text-center">
                                        <p>
                                          Please fill out the following information when adding or deleting a vehicle to your coverage
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row form-top_body mx-auto">
                                      <div class="col-md-12 text-center">
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input insurance_check" type="checkbox" name="request" value="add" disabled="">ADD
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input insurance_check" type="checkbox" name="request" value="delete">DELETE
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input insurance_check" type="checkbox" name="request" value="amend">AMEND
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row form-bottom-body mt-2 input_row">
                                      <div class="col-md-12 mx-auto input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_company_name" class="bmd-label-floating input_label">Company Name</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_company_name" name="company_name" value="{{$data->company_name}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_effective_date" class="bmd-label-floating input_label">Effective Date</label>
                                          <input type="text" class="form-control form_input_field form_fields datepicker" id="vehicle_effective_date" name="effective_date" value="{{$data->effective_date}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row mt-2 mb-3">
                                      <div class="col-md-12 insurance_info">
                                        <h4>Vehicle Information : </h4>
                                      </div>
                                    </div>
                                    <div class="row mb-1 input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-check">
                                          <label class="form-check-label">
                                            <input class="form-check-input form_fields vehicle_liability" name="liability" type="checkbox" @if($data->liability == 'liability') checked @endif value="liability">Liability
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-check">
                                          <label class="form-check-label">
                                            <input class="form-check-input form_fields vehicle_physical_dam" name="damage" type="checkbox" @if($data->damage == 'damage') checked @endif value="damage" >Physical Damage
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_year" class="bmd-label-floating input_label">Year</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_year" name="year" value="{{$data->year}}">
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_make" class="bmd-label-floating input_label">Make</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_make" name="make" value="{{$data->make}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_model" class="bmd-label-floating input_label">Model</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_model" name="model" value="{{$data->model}}">
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_vin" class="bmd-label-floating input_label">VIN</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_vin" name="vin" value="{{$data->vin}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_body_type" class="bmd-label-floating input_label">Body Type</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_body_type" name="body_type" value="{{$data->body_type}}">
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_st" class="bmd-label-floating input_label">State Registered/ Titled</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_st" name="state_registered" value="{{$data->state_registered}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_unit" class="bmd-label-floating input_label">Unit</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_unit" name="unit" value="{{$data->unit}}">
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_gross_weight" class="bmd-label-floating input_label">Gross Vehicle Weight</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_gross_weight" name="gross_vehicle_weight" value="{{$data->gross_vehicle_weight}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_cost_new" class="bmd-label-floating input_label">Cost New</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_cost_new" name="cost_new" value="{{$data->cost_new}}">
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_mlg_rad" class="bmd-label-floating input_label">Milleage Radius</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_mlg_rad" name="milleage_radius" value="{{$data->milleage_radius}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="spcl_eq" class="bmd-label-floating input_label">Special Equipment (If any provide Description and Value)</label>
                                          <textarea class="form-control form_input_field form_fields" rows="3" name="special_equipment" id="spcl_eq">{{$data->special_equipment}}</textarea>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_garragin_address" class="bmd-label-floating input_label">Garraging Location Address</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_garragin_address" name="garaging_address" value="{{$data->garaging_address}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_usage" class="bmd-label-floating input_label">Usage</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_usage" name="vehicle_usage" value="{{$data->vehicle_usage}}">
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_lease_of_purchase" class="bmd-label-floating input_label">Lease or Purchase</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicle_lease_of_purchase" name="lease_of_purchase" value="{{$data->lease_of_purchase}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicel_fin_comp" class="bmd-label-floating input_label">Finance Company / Lassor Name</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicel_fin_comp" name="finance_company_name" value="{{$data->finance_company_name}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicel_fin_comp_address" class="bmd-label-floating input_label">Finance Company / Lassor Address</label>
                                          <textarea class="form-control form_input_field form_fields" rows="3" name="finance_company_address" id="vehicel_fin_comp_address">{{$data->finance_company_address}}</textarea>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper long_input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicel_fin_comp_info" class="bmd-label-floating input_label">Finance Company/Lessor Phone or Email for EPI</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="vehicel_fin_comp_info" name="finance_company_phone_email" value="{{$data->finance_company_phone_email}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="vehicle_additional" class="bmd-label-floating input_label">Additional</label>
                                          <textarea class="form-control form_input_field form_fields" rows="5" name="additional" id="vehicle_additional">{{$data->additional}}</textarea>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row mt-5">
                                      <div class="col-md-7 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="submitted_by_vehicle" class="bmd-label-floating input_label">Submitted By</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="submitted_by_vehicle" name="submitted_by" value="{{$name}} - {{$email}}" readonly>
                                        </div>
                                      </div>
                                      <div class="col-md-5 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="date_of_sub_vehicle" class="bmd-label-floating input_label">Date</label>
                                          <input type="text" class="form-control form_input_field form_fields datepicker" name="date" id="date_of_sub_vehicle" value="{{$data->date}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row mt-6 input_row">
                                      <div class="col-md-7 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="send_to_vehicle" class="bmd-label-floating input_label">Send Mail To</label>
                                          <input type="email" class="form-control form_input_field form_fields" id="send_to_vehicle" name="send_mail_to" value="{{$data->send_mail_to}}">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row preview_row" style="display:none;">
                              <div class="col-md-12 mx-auto insurance_padding">
                                <div class="card preview_card">
                                  <div class="card-header card-header-rose card-header-icon">
                                    <div class="card-icon">
                                      <i class="material-icons">assignment</i>
                                    </div>
                                    <h4 class="card-title">Submitted Preview</h4>
                                  </div>
                                  <div class="card-body">
                                    <div class="row preview_checkbox_row">
                                      <div class="col-md-12 text-center">
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input insurance_check" type="checkbox" value="add" disabled="">ADD
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input insurance_check" type="checkbox" value="delete">DELETE
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input insurance_check" type="checkbox" value="amend" disabled="">AMEND
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>                          
                              </div>
                              <div class="col-md-12 mx-auto insurance_padding preview_page_btns_col">
                                <div class="row">
                                  <div class="col-md-6 text-left save_btn_wrapper">
                                    <button type="button" class="btn btn-md btn-round btn-warning go_back ">
                                      Go Back
                                    </button>
                                  </div>
                                  <div class="col-md-6 text-right save_btn_wrapper">
                                    <button type="submit" class="btn btn-md btn-round btn-success confirm">
                                      Confirm
                                    </button>
                                    </div> 
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                        <!-- Button -->
                        <div class="col-md-12 text-right save_btn_wrapper">
                          <button type="button" class="btn btn-md btn-round btn-success preview_btn">
                            Preview and Send
                          </button>
                        </div>
                      </div>
                    </div>
                    @endif


                    @if($type == 'driver')
                    <div class="tab-pane inventory_sub_card insurance_tab" id="driver_tab">
                      <div class="row">
                        <div class="col-md-12">
                          <form method="post" action="{{ route('insurance.store') }}" class="form-horizontal">
                            @csrf
                            <input type="hidden" name="tab" value="driver">
                            
                            <div class="card form_card current">
                              <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                  <i class="material-icons">assignment</i>
                                </div>
                                <h4 class="card-title">Driver Insurance</h4>
                              </div>

                              <div class="card-body">
                                <div class="toolbar">
                                </div>
                                <div class="row">
                                  <div class="col-md-11 mx-auto">
                                    <div class="row form_header mb-5">
                                      <div class="col-md-6 mx-auto text-center">
                                        <h2>
                                          Driver Request
                                        </h2>
                                      </div>
                                      <div class="col-md-12 text-center">
                                        <p>
                                          Please fill out the following information when adding or deleting a driver to your coverage and attach a copy of the MVR
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row form-top_body mx-auto">
                                      <div class="col-md-12 text-center">
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input insurance_check" type="checkbox" name="request" value="add" disabled="">ADD
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input insurance_check" type="checkbox" name="request" value="delete">DELETE
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input insurance_check" type="checkbox" name="request" value="amend">AMEND
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="company_name" class="bmd-label-floating input_label">Company Name</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="dr_company_name" name="company_name" value="{{$data->company_name}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="company_name" class="bmd-label-floating input_label">Date Of Change</label>
                                          <input type="text" class="form-control form_input_field form_fields datepicker" id="dr_date_of_change" name="date_of_change" value="{{$data->date_of_change}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="dr_full_name" class="bmd-label-floating input_label">Full Driver Name</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="dr_full_name" name="driver_full_name" value="{{$data->driver_full_name}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="dr_dob" class="bmd-label-floating input_label">Date of Birth</label>
                                          <input type="text" class="form-control form_input_field form_fields datepicker" id="dr_dob" name="date_of_birth" value="{{$data->date_of_birth}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="dr_license_st" class="bmd-label-floating input_label">License State</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="dr_license_st" name="license_state" value="{{$data->license_state}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="dr_license_num" class="bmd-label-floating input_label">License #</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="dr_license_num" name="license" value="{{$data->license}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="dr_years_of_ex" class="bmd-label-floating input_label">Year's of Experience</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="dr_years_of_ex" name="years_of_experience" value="{{$data->years_of_experience}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="dr_years_of_cdl_ex" class="bmd-label-floating input_label">Year's of CDL Experience</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="dr_years_of_cdl_ex" name="years_of_cdl_experience" value="{{$data->years_of_cdl_experience}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="dr_duties" class="bmd-label-floating input_label">Duties</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="dr_duties" name="duties" value="{{$data->duties}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="dr_doh" class="bmd-label-floating input_label">Date of Hire</label>
                                          <input type="text" class="form-control form_input_field form_fields datepicker" id="dr_doh" name="date_of_hire" value="{{$data->date_of_hire}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-12 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="dr_additional" class="bmd-label-floating input_label">Additional</label>
                                          <textarea class="form-control form_input_field form_fields" rows="5" name="additional" id="dr_additional">{{$data->additional}}</textarea>
                                        </div> 
                                      </div>
                                    </div>
                                    <div class="row mt-5">
                                      <div class="col-md-7 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="submitted_to_driver" class="bmd-label-floating input_label">Submitted By</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="submitted_to_driver" name="submitted_by" value="{{$name}} - {{$email}}" readonly>
                                        </div>
                                      </div>
                                      <div class="col-md-5 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="date_of_sub_driver" class="bmd-label-floating input_label">Date</label>
                                          <input type="text" class="form-control form_input_field form_fields datepicker" id="date_of_sub_driver" name="date" value="{{$data->date}}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row mt-6 input_row">
                                      <div class="col-md-7 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="send_to_driver" class="bmd-label-floating input_label">Send Mail To</label>
                                          <input type="email" class="form-control form_input_field form_fields" id="send_to_driver" name="send_mail_to" value="{{$data->send_mail_to}}">
                                        </div>
                                      </div>
                                    </div>
                                    
                                  </div>
                                  
                                </div>

                              </div>
                            </div>
                            <div class="row preview_row" style="display:none;">
                              <div class="col-md-12 mx-auto insurance_padding">
                                <div class="card preview_card">
                                  <div class="card-header card-header-rose card-header-icon">
                                    <div class="card-icon">
                                      <i class="material-icons">assignment</i>
                                    </div>
                                    <h4 class="card-title">Submitted Preview</h4>
                                  </div>
                                  <div class="card-body">
                                    <div class="row preview_checkbox_row">
                                      <div class="col-md-12 text-center">
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input insurance_check" type="checkbox" value="add" disabled="">ADD
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input insurance_check" type="checkbox" value="delete" disabled="">DELETE
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                            <input class="form-check-input insurance_check" type="checkbox" value="amend" disabled="">AMEND
                                            <span class="form-check-sign">
                                              <span class="check"></span>
                                            </span>
                                          </label>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>                          
                              </div>
                              <div class="col-md-12 mx-auto insurance_padding preview_page_btns_col">
                                <div class="row">
                                  <div class="col-md-6 text-left save_btn_wrapper">
                                    <button type="button" class="btn btn-md btn-round btn-warning go_back ">
                                      Go Back
                                    </button>
                                  </div>
                                  <div class="col-md-6 text-right save_btn_wrapper">
                                    <button type="submit" class="btn btn-md btn-round btn-success confirm">
                                      Confirm
                                    </button> 
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>
                          
                        </div>
                        <!-- Button -->
                        <div class="col-md-12 text-right save_btn_wrapper">
                          <button type="button" class="btn btn-md btn-round btn-success preview_btn">
                            Preview and Send
                          </button>
                        </div>
                      </div>
                    </div>
                    @endif
                  </div>
                
                </div>
            </div>
          </div>
        </div>
      </div>


<script>

const inputs = document.querySelectorAll(".form_card input, select, textarea");
for (const el of inputs){
  el.oldValue = el.value + el.checked;
}

// Declares function and call it directly
var setEnabled;
(setEnabled = function() {
  var e = true;
  for (const el of inputs) {
    if (el.oldValue !== (el.value + el.checked)) {
      e = false;
      break;
    }
  }
  $('.preview_btn').prop("disabled", e);
})();

document.oninput = setEnabled;
document.onchange = setEnabled;
$(document).on('focus',".datepicker", function(){
  $(this).datetimepicker().on('dp.change', function (event) {
      setEnabled();
  });
});



$('.insurance_check').change(function() {

  $(".insurance_check").prop('checked',false);
  $(this).prop('checked',true);

  if($(this).attr("value") == 'delete') {
    $(".form_card input, select, textarea").not("input[name=send_mail_to], input[name=date]").attr('readonly',true);
    $(":checkbox").not("input[name=request]").bind("click", false);
  }
  else{
   $(".form_card input, select, textarea").removeAttr('readonly');
    $(":checkbox").unbind("click");
  }      

});


</script>

@endsection