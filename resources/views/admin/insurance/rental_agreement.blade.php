@extends('layouts.email')

@section('agreement_id')
    <div class="email_agreement_id_badge text-right">
      <div class="timeline-heading">
        <h4 class="badge badge-pill">RN-{{$data->id}} </h4>
      </div>
    </div>                        
  </div>
@endsection

@section('content')

        <form id="multiForm2" method="post" action="{{ route('insurance.store') }}" class="ren_agreement_form" enctype="multipart/form-data" onsubmit="return validateMyForm();">

          @csrf
          <input type="hidden" name="tab" value="rental_agreement">
          <input type="hidden" class="status" name="status" value="{{$status}}">
          <input type="hidden" name="p_key" value="{{$data->id}}">
          <input type="hidden" name="url_token" value="{{$url_token}}">


            <!-- Circles which indicates the steps of the form: -->
          <div class="row">
            <div class="col-md-12">
              <div class="card rental_agreement_card insurance_parent_tab_content" id="top_rental_card">

                <div class="card-body">
                  <div class="row mb-2">
                    <div class="col-md-12 text-center">
                      <h2 style="font-weight: 500;font-size: 1.8rem">Equipment Rental Agreement</h2>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-1 progress_container" style="text-align:center;margin-bottom:20px;">
                      <span class="stepRen step-first">
                        <i class="material-icons">done</i>
                      </span>
                      <span class="stepRen step-inner">
                        <i class="material-icons">done</i>
                      </span>
                      <span class="stepRen step-inner">
                        <i class="material-icons">done</i>
                      </span>
                      <span class="stepRen step-inner">
                        <i class="material-icons">done</i>
                      </span>
                      <span class="stepRen step-inner">
                        <i class="material-icons">done</i>
                      </span>
                      <span class="stepRen step-inner">
                        <i class="material-icons">done</i>
                      </span>                                      
                    </div>
                    <div class="col-md-11 content_container rental_ag_content_container">
                      <div class="tabRen">
                        <div class="row">
                          <div class="col-md-12 evos_field">
                            <p> This Equipment Rental Agreement ( the “Agreement” ) is made and entered on 
                                <input type="text" class="datepicker form-control ren_agreement_input input_ten form_input_field form_fields" name="entered_on" value="{{$data->entered_on}}" placeholder="Date">
                                <!-- <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_twenty" name="entered_by" value="{{$data->entered_by}}"> --> , by and between EV Oilfield Services, LLC (Lessor) and 
                                <!-- <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_forty" name="entered_between_lessor" value="{{$data->entered_between_lessor}}"> (“Lessor”) and -->
                                <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_forty" name="entered_between_lessee" value="{{$data->entered_between_lessee}}" placeholder="Company Name"> (“Lessee”) ( collectively referred to as the “Parties” ).
                            </p>
                          </div>
                          <div class="col-md-12 agree_as_follows">
                            <p> The Parties agree as follows:  </p>
                          </div>
                        </div>
                        <div class="row points_row points_row_first">
                          <div class="col-md-12">
                            <p><span class="point_head"> 1. Equipment : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              Lessor hereby rents to Lessee the following equipment : 
                            </p>
                          </div>
                          <div class="col-md-12 evos_field">
                            <textarea class="form-control form_input_field form_fields" rows="4" name="ren_ag_lesse_equipment" id="ren_ag_lesse_equipment">{{$data->ren_ag_lesse_equipment}}</textarea>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 2. Rental Term : </span> </p>
                            
                          </div>
                          <div class="col-md-12">
                            <span>
                              The rent will start on
                            </span>
                            <span>
                              <div class="form-group bmd-form-group date_input_div evos_field">
                                <input type="text" class="form-control form_input_field form_fields datepicker" id="ren_ag_start_date" name="ren_ag_start_date" placeholder="Begin Date" value="{{$data->ren_ag_start_date}}">
                              </div>
                            </span>
                            <span>
                              <!-- <input type="number" class="ren_agreement_input form-control form_input_field form_fields input_fifteen datepicker" placeholder="Begin Date"> -->
                              and will end on
                            </span>
                            <span>
                              <div class="form-group bmd-form-group date_input_div evos_field">
                                <input type="text" class="form-control form_input_field form_fields datepicker" id="ren_ag_end_date" name="ren_ag_end_date" placeholder="End Date" value="{{$data->ren_ag_end_date}}">
                              </div> 
                              (Rental Term).
                            </span>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 3. Rental Payments : </span> </p>
                            
                          </div>
                          <div class="col-md-12 evos_field"> 
                            <p>
                              <div class="p_to_inline">
                                <span>                                             
                                Lessee agrees to pay to Lessor as rent for the Equipment the amount of $
                                <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_fifteen" placeholder="" name="ren_ag_amount_rent" value="{{$data->ren_ag_amount_rent}}">
                                (“Rent”) each 
                                </span>
                                <span>
                                  <div class="input_wrapper custom_select2_col rent_time">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select contractor_select select2 form_input_field" id="" name="ren_ag_time_rent">
                                        <option class="form-select-placeholder"></option>
                                        <option value="day" @if($data->ren_ag_time_rent == 'day') selected @endif>Day</option>
                                        <option value="week" @if($data->ren_ag_time_rent == 'week') selected @endif>Week</option>
                                        <option value="month" @if($data->ren_ag_time_rent == 'month') selected @endif>Month</option>
                                      </select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label" for="contractor_select input_label">Choose Time</label>
                                    </div>
                                  </div>                                                  
                                  
                                </span>
                                <span>
                                  month in advance on the first day of each month at :
                                  <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_forty" placeholder="" name="ren_ag_address_rent" value="{{$data->ren_ag_address_rent}}"> ( address for rent payment )
                                  at any other address designated by Lessor. If the Rental Term does not start on the first
                                  day of the month or end on the last day of a month, the rent will be prorated accordingly.
                                </span>
                              </div>

                            </p>
                          </div>
                        </div>                                        
                      </div>

                      <div class="tabRen">
                        <div class="row points_row evos_field">
                          <div class="col-md-12">
                            <p><span class="point_head"> 4. Late Charges : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              If any amount under this Agreement is more than
                              <input type="number" class="ren_agreement_input form-control form_input_field form_fields input_ten" placeholder="" name="ren_ag_late_day" value="{{$data->ren_ag_late_day}}"> days late, Lessee agrees to pay a late fee of $
                              <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_fifteen" placeholder="" name="ren_ag_late_charge" value="{{$data->ren_ag_late_charge}}">  ( address for rent payment )

                            </p>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 5. Security Deposit : </span> </p>
                            
                          </div>
                          <div class="col-md-12 evos_field"> 
                            <p> 
                              Prior to taking possession of the Equipment, Lessee shall deposit with Lessor, in trust, a security deposit of $
                              <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_ten currency_input" placeholder="" name="ren_ag_security_deposit" value="{{$data->ren_ag_security_deposit}}"> as security for the performance by Lessee of the terms under this Agreement and for any damages caused by <span class="dots"> . . . </span> <span class="more"> Lessee or Lessee’s agents to the Equipment during the Rental Term. Lessor may use part or all of the security deposit to repair any damage to Equipment caused by Lessee or Lessee’s agents. However, Lessor is not just limited to the security deposit amount and Lessee remains liable for any balance. Lessee shall not apply or deduct any portion of any security deposit from the last or any month's rent. Lessee shall not use or apply any such security deposit at any time in lieu of payment of rent. If Lessee breaches any terms or conditions of this Agreement, Lessee shall forfeit any deposit, as permitted by law.
                               </span>
                            <button class="btn btn-link btn_read_more" type="button">Read More</button>                                               
                            </p>

                          </div>
                        </div>                                        
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 6. Delivery : </span> </p>
                            
                          </div>
                          <div class="col-md-12 evos_field_chk"> 
                            <p> 
                              Rental
                              <span class="form-check form-check-inline check_one">
                                <label class="form-check-label">
                                  <input class="form-check-input" name="responsible" type="checkbox" value="responsible" @if($data->responsible == 'responsible') checked @endif> Shall or
                                  <span class="form-check-sign">
                                    <span class="check"></span>
                                  </span>
                                </label>
                              </span>
                              <span class="form-check form-check-inline check_two">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="checkbox" name="responsible" value="not_responsible" @if($data->responsible == 'not_responsible') checked @endif>Shall Not
                                  <span class="form-check-sign">
                                    <span class="check"></span>
                                  </span>
                                </label>
                              </span>
                               be responsible for all expenses and costs: i) at the beginning of the Rental Term, of shipping the Equipment to Lessee’s premises and ii) at the end of the Rental Term, of shipping the Equipment back to Lessor’s premises.
                            </p>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 7. Defaults : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              If Lessee fails to perform or fulfill any obligation under this
                              Agreement, Lessee shall be in default of this Agreement. Subject to any statute,
                              ordinance or law to the contrary, Lessee shall have seven (7) days from the date of notice
                              of default by Lessor to  <span class="dots"> . . . </span>
                              <span class="more"> cure the default.
                              In the event Lessee does not cure a default, Lessor may at Lessor’s option (a) cure such default and the cost of such action may be added to Lessee’s financial obligations under this Agreement; or (b) declare Lessee in default of the Agreement. If Lessee shall become insolvent, cease to do business as a going concern or if a petition has been filed by or against Lessee under the Bankruptcy Act or similar federal or state statute, Lessor may immediately declare Lessee in default of this Agreement. In the event of default, Lessor may, as permitted by law, re-take possession of the Equipment. Lessor may, at its option, hold Lessee liable for any difference between the Rent that would have been payable under this Agreement during the balance of the unexpired term and any rent paid by any successive lessee if the Equipment is re-let minus the cost and expenses of such reletting. In the event Lessor is
                              unable to re-let the Equipment during any remaining term of this Agreement, after default
                              by Lessee, Lessor may at its option hold Lessee liable for the balance of the unpaid rent under this Agreement if this Agreement had continued in force.
                              </span>
                              <button class="btn btn-link btn_read_more" type="button">Read More</button>
                            </p>

                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 8. Possession And Surrender Of Equipment : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              Lessee shall be entitled to possession of the Equipment on the first day of the Rental Term. At the expiration of the Rental Term, Lessee shall surrender the Equipment to Lessor by delivering the Equipment to Lessor or Lessor’s agent in good condition and <span class="dots">. . .</span> 
                              <span class="more">working order, ordinary wear and tear excepted, as it was at the commencement of the Agreement.
                              </span>
                              <button class="btn btn-link btn_read_more" type="button">Read More</button>
                            </p>
                          </div>
                        </div>
                      </div>

                      <div class="tabRen">
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 9. Use Of Equipment : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              Lessee shall only use the Equipment in a careful and proper manner and will comply with all laws, rules, ordinances, statutes and orders regarding the use, maintenance of storage of the Equipment.
                            </p>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 10. Condition Of Equipment And Repair : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              Lessee or Lessee’s agent has inspected the Equipment and acknowledges that the Equipment is in good and acceptable condition.
                            </p>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 11. Maintenance, Damage And Loss : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              Lessee will, at Lessee's sole expense, keep and maintain the Equipment clean and in good working order and repair during the Rental Term. In the event the Equipment is lost or damaged beyond repair, Lessee shall pay to Lessor the replacement cost <span class="dots">. . .</span> 
                              <span class="more">of the Equipment; in addition, the obligations of this Agreement shall continue in full force and effect through the Rental Term.
                              </span>
                              <button class="btn btn-link btn_read_more" type="button">Read More</button>
                            </p>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 12. Insurance : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              Lessee shall be responsible to maintain insurance on the Equipment with losses payable to Lessor against fire, theft, collision, and other such risks as are appropriate and specified by Lessor.  Upon request by Lessor, Lessee shall provide proof of such insurance.
                            </p>
                          </div>
                        </div>                                        
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 13. Encumbrances, Taxes And Other Laws : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              Lessee shall keep the Equipment free and clear of any liens or other encumbrances, and shall not permit any act where Lessor’s title or rights may be negatively affected. Lessee shall be responsible for complying with and conforming to all laws and <span class="dots">. . .</span> 
                              <span class="more">
                                regulations relating to the possession, use or maintenance of the Equipment. Furthermore, Lessee shall promptly pay all taxes, fees, licenses and governmental charges, together with any penalties or interest thereon, relating to the possession, use or maintenance of the Equipment.
                              </span>
                              <button class="btn btn-link btn_read_more" type="button">Read More</button>
                            </p>
                          </div>
                        </div>                       
                      </div>

                      <div class="tabRen">
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 14. Lessors Representations : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              Lessor represents and warrants that he/she has the right to rent the Equipment as provided in this Agreement and that Lessee shall be entitled to quietly hold and possess the Equipment, and Lessor will not interfere with that right as long as Lessee pays the Rent in a timely manner and performs all other obligations under this Agreement.
                            </p>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 15. Ownership : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              The Equipment is and shall remain the exclusive property of Lessor.
                            </p>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 16. Severability : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              If any part or parts of this Agreement shall be held unenforceable for any reason, the remainder of this Agreement shall continue in full force and effect. If any provision of this Agreement is deemed invalid or unenforceable by any court of competent jurisdiction, and if limiting such provision would make the provision valid, then such provision shall be deemed to be construed as so limited.
                            </p>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 17. Assignment : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              Neither this Agreement nor Lessee’s rights hereunder are assignable except with Lessor’s prior, written consent.
                            </p>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 18. Binding Effect : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              The covenants and conditions contained in the Agreement shall apply to and bind the Parties and the heirs, legal representatives, successors and permitted assigns of the Parties.
                            </p>
                          </div>
                        </div>
                        <div class="row points_row evos_field">
                          <div class="col-md-12">
                            <p><span class="point_head"> 19. Governing Law : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              This Agreement shall be governed by and construed in accordance with the laws of the State of
                              <input type="text" class="form-control ren_agreement_input state_input form_input_field form_fields" name="ren_ag_governing_law" value="{{$data->ren_ag_governing_law}}">
                            </p>
                          </div>
                        </div>                      
                      </div>

                      <div class="tabRen">
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 20. Notice : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              Any notice required or otherwise given pursuant to this Agreement shall be in writing and mailed certified return receipt requested, postage prepaid, or delivered by overnight delivery service to:
                            </p>
                          </div>
                          <div class="col-md-6 evos_field">
                            <p> Lessor : </p>
                            <textarea class="form-control form_input_field form_fields" rows="3" name="lessor_agreement_notice" id="lessor_agreement">{{$data->lessor_agreement_notice}}</textarea>
                          </div>
                          <div class="col-md-6">
                            <p> Lessee : </p>
                            <textarea class="form-control form_input_field form_fields field_to_fill" rows="3" name="lesse_agreement_notice" id="lesse_agreement">{{$data->lesse_agreement_notice}}</textarea>
                          </div>
                          <div class="col-md-12">
                            <p> Either party may change such addresses from time to time by providing notice as set forth above. </p>
                          </div>                                         
                        </div>                                         
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 21. Entire Agreement : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              This Agreement constitutes the entire agreement between the Parties and supersedes any prior understanding or representation of any kind preceding the date of this Agreement. There are no other promises, conditions, understandings or <span class="dots">...</span> 
                              <span class="more">
                                >other agreements, whether oral or written, relating to the subject matter of this Agreement. This Agreement may be modified in writing and must be signed by both Lessor and Lessee.
                              </span>
                              <button class="btn btn-link btn_read_more" type="button">Read More</button>
                            </p>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 22. Cumulative Rights : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              Lessor’s and Lessee’s rights under this Agreement are cumulative, and shall not be construed as exclusive of each other unless otherwise required by law.
                            </p>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 23. Waiver : </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              The failure of either party to enforce any provisions of this Agreement shall not be deemed a waiver or limitation of that party's right to subsequently enforce and compel strict compliance with every provision of this Agreement.
                              <span class="dots"> . . </span>
                              <span class="more">The acceptance of rent by Lessor does not waive Lessor’s right to enforce any provisions of this Agreement.
                              </span>
                              <button class="btn btn-link btn_read_more" type="button">Read More<div class="ripple-container"></div></button>
                            </p>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 24. Indemnification :  </span> </p>
                            
                          </div>
                          <div class="col-md-12"> 
                            <p> 
                              Except for damages, claims or losses due to Lessor’s acts or negligence, Lessee, to the extent permitted by law, will indemnify and hold Lessor and Lessor’s property, free and harmless from any liability for losses, claims, injury to or <span class="dots">. . .</span>
                              <span class="more">death of any person, including Lessee, or for damage to property arising from Lessee using and possessing the Equipment or from the acts or omissions of any person or persons, including Lessee, using or possessing the Equipment with Lessee’s express or implied consent.
                              </span>
                              <button class="btn btn-link btn_read_more" type="button">Read More</button>
                            </p>
                          </div>
                        </div>
                        <div class="row points_row">
                          <div class="col-md-12">
                            <p><span class="point_head"> 25. Additional Terms & Conditions  </span> ( Specify “none” if there are no additional provisions ) : </p>
                            
                          </div>
                          <div class="col-md-12 evos_field">
                            <textarea class="form-control form_input_field form_fields" rows="4" name="ren_ag_additional_terms" id="ren_ag_additional_terms">{{$data->ren_ag_additional_terms}}</textarea>
                          </div>
                        </div>
                        <div class="row points_row footer_note_points_row">
                          <div class="col-md-12 text-center">
                            <span class="footer_note"> [ Remainder of Page Intentionally Left Blank ] </span>
                          </div>
                        </div>              
                      </div>

                      <div class="tabRen">
                        <div class="row points_row">
                          <div class="col-md-12"> 
                            <p> 
                              IN WITNESS WHEREOF , the parties have caused this Agreement to be executed the day and year first above written.
                            </p>
                          </div>
                        </div>
                        <div class="row points_row signature_points_row">
                          <div class="col-md-6 input_wrapper evos_field">
                            <fieldset class="custom_fieldset_2 contractor_fieldset">
                              <legend> Lessor : </legend>

                              <div class="form-group bmd-form-group mt-3">
                                <label for="model_equip" class="bmd-label-floating input_label"> Name * </label>
                                <input type="text" class="form-control form_input_field form_fields" id="lessor_ag_names" name="ren_ag_executed_lessor_name" value="{{$data->ren_ag_executed_lessor_name}}">
                              </div>

                              <div class="form-group bmd-form-group">
                                <label for="model_equip" class="bmd-label-floating input_label"> Position , If Applicatble </label>
                                <input type="text" class="form-control form_input_field form_fields" id="lessor_ag_pos" name="ren_ag_executed_lessor_position" value="{{$data->ren_ag_executed_lessor_position}}">
                              </div>
                            </fieldset>                                        
                          </div>
                          <div class="col-md-6">
                            <fieldset class="custom_fieldset_2 contractor_fieldset">
                              <legend> Lessee : </legend>

                              <div class="form-group bmd-form-group mt-3">
                                <label for="model_equip" class="bmd-label-floating input_label"> Name * </label>
                                <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="lessee_ag_names" name="ren_ag_executed_lessee_name" value="{{$data->ren_ag_executed_lessee_name}}">
                              </div>

                              <div class="form-group bmd-form-group">
                                <label for="model_equip" class="bmd-label-floating input_label"> Position , If Applicatble </label>
                                <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="lessee_ag_pos" name="ren_ag_executed_lessee_position" value="{{$data->ren_ag_executed_lessee_position}}">
                              </div>
                            </fieldset>                                              
                          </div>
                        </div>
                        <fieldset class="custom_fieldset_2 contractor_fieldset">
                          <legend> Upload Necessary Files </legend>
                          <div class="row input_row poins_row mb-3">
                            <div class="col-md-12">
                              <label for="multiple_file input_label"> Upload W9 * </label>
                              <div class="agreement_w9 agreement_upload">
                              </div>
                            </div>
                          </div>

                          <div class="row input_row poins_row mt-3 mb-5">
                            <div class="col-md-12">
                              <label for="multiple_file input_label"> Upload Insurance Certificate * </label>
                              <div class="insurance_certificate agreement_upload">
                              </div>
                            </div>
                          </div>
                        </fieldset>

                          <div class="row my-4">
                            <div class="col-md-8 mt-3 input_wrapper">
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input rental_agreement_check" id="agree_chk" type="checkbox" name="" value="1"> Upon clicking this checkbox, I agree with all terms and condition described in this agreement.
                                  <span class="form-check-sign">
                                    <span class="check"></span>
                                  </span>
                                </label>
                              </div>
                            </div>
                  <!--                             <div class="col-md-4">
                              <div class="form-group bmd-form-group">
                                <label for="signed_by" class="bmd-label-floating input_label signed_by_label"> Signed By - Your Name * </label>
                                <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="signed_by" name="signed_by" value="{{$data->signed_by}}" required="true">
                              </div>                                 
                            </div> -->                            
                          </div>

                        @if($status != 2 && $status != 4)
                        <fieldset class="custom_fieldset_2 contractor_fieldset mt-5 mb-3">
                          <legend>Sending Information</legend>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group bmd-form-group">
                              <label for="agreement_send_mail_to" class="bmd-label-floating input_label">Send Mail To</label>
                                <input type="text" class="form-control form_input_field form_fields" id="agreement_send_mail_to" name="ren_ag_send_mail_to" value="{{$data->ren_ag_send_mail_to}}" required="">
                              </div>                                          
                            </div>
                            <div class="col-md-6">
                              <div class="form-group bmd-form-group">
                              <label for="agreement_send_mail_date" class="bmd-label-floating input_label">Date</label>
                                <input type="text" class="datepicker form-control form_input_field form_fields" id="agreement_send_mail_date" name="ren_ag_send_mail_date" value="{{$data->ren_ag_send_mail_date}}" required="">
                              </div>                                          
                            </div>
                          </div>
                        </fieldset>    
                        @else
                     	  <input type="hidden" class="form-control form_input_field form_fields" id="agreement_send_mail_to" name="ren_ag_send_mail_to" value="{{$data->ren_ag_send_mail_to}}">                                                             
                        @endif
                    
                        <input type="hidden" name="submitted_by_name" value="{{ $data->submitted_by_name }}">
                        <input type="hidden" name="submitted_by_email" value="{{ $data->submitted_by_email }}">

                        @if($rn_sign)  
                        <input type="hidden" class="rn_sign" value="{{$rn_sign->url}}">
                        @endif

                        <div class="row input_row mt-2">
                          <div class="col-md-12 draw_signature_col text-center">
                              <label class="" for="">Signature (Drag Your cursor to sign below)</label>
                              <label class="sign_error_msg" style="display: none;" for="">Please Put Your Signature</label>
                              <br/>
                              <div id="sig" ></div>
                              @if($rn_sign)
                                  <input type="hidden" class="sign_input_validation" value="1">
                              @else
                                  <input type="hidden" class="sign_input_validation" value="0">
                              @endif
                              <br/>
                              <button id="clear" class="btn btn-sm btn-round btn-danger my-3">Clear Signature</button>
                              <textarea id="signature64" name="signed" style="display: none"></textarea>
                          </div>
                        </div>  

                                                                                
                      </div>

                      <div style="overflow:auto;" class="row msf_btn_holder">
                        <div class="col-md-6 save_btn_wrapper text-left" style="float:right;">
                          <a class="btn btn-round btn-warning nextPrevBtn prev" href="#top_rental_card" id="prevBtnRen" type="button" onclick="nextPrevRen(-1)">Previous</a>
                        </div>
                        <div class="col-md-6 save_btn_wrapper text-right">
                          <a class="btn custom-btn-one btn-round btn-success nextPrevBtn next" href="#top_rental_card" id="nextBtnRen" type="button" onclick="nextPrevRen(1)">Next</a>

                          <button class="btn custom-btn-one btn-round btn-success nextPrevBtn submitBtnRen submitBtn" style="display: none" id="" type="submit" onclick="">Submit</button>                                          
                        </div>
                      </div>
                    </div>
                  </div>                              
                </div>
              </div>
            </div>
          </div>

          <!-- Modal -->
          <div class="modal fade bs_modal" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Reason For The Rejection</h5>
                  <button type="button" class="close close_icon" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <textarea class="form-control summernote" style="min-width: 100%" rows="6" name="reject"></textarea>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="submit" value="reject">Submit</button>
                </div>
              </div>
            </div>
          </div>

        </form>


<script>

$(function(){
  if($('.status').val() == 2 || $('.status').val() == 4){
    $('.evos_field').find('input, textarea').attr('readonly', true);
    $('.evos_field_chk').css('pointer-events','none');
    $('.field_to_fill').each(function(){
      $(this).addClass('requiredRen')
      if($(this).hasClass('field_to_fill_validator')){
        $(this).attr('required',true)
      }
    })
  }
  if($('.status').val() == 3){
    //$('.evos_field').find('input').attr('readonly', true);
  }
});

$(document).ready(function() {
  let value = $('input[name="p_key"]').val();
  let type = 'rental_agreement_w9';
  let _token = $('input[name="_token"]').val();
  var preloaded = [];
  $.ajax({
            async: false,
            url:"{{ route('getInsuranceImage') }}",
            method:"POST",
            data:{value:value, type:type, _token:_token},
            success:function(result)
            {
              var obj = JSON.parse(result)
              $.each(obj, function(index) {
                var data = { id: obj[index].id, src: '<a href="'+obj[index].file+'" download>'+obj[index].name+'</a>' };
                preloaded.push(data);
              });

              preloadedImage(preloaded);
            }
         })
  function preloadedImage(param) {
    $('.agreement_w9').fileUploader({
      preloaded: param,
      imagesInputName:'rental_agreement_w9',
      preloadedInputName:'preloaded',
      label:'Drag & Drop files here or click to browse',
      extensions: ['.pdf','.doc','.docx','DOCX','.jpg','.jpeg','.png','.gif','.svg'],
      required: true,
      mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','image/jpeg','image/png','image/gif','image/svg+xml']
  });
}



let value2 = $('input[name="p_key"]').val();
  let type2 = 'rental_insurance_certificate';
  let _token2 = $('input[name="_token"]').val();
  var preloaded2 = [];
  $.ajax({
            async: false,
            url:"{{ route('getInsuranceImage') }}",
            method:"POST",
            data:{value:value2, type:type2, _token:_token2},
            success:function(result)
            {
              var obj = JSON.parse(result)
              $.each(obj, function(index) {
                var data = { id: obj[index].id, src: '<a href="'+obj[index].file+'" download>'+obj[index].name+'</a>' };
                preloaded2.push(data);
              });

              preloadedImage2(preloaded2);
            }
         })
  function preloadedImage2(param) {
    $('.insurance_certificate').fileUploader({
      preloaded: param,
      imagesInputName:'rental_insurance_certificate',
      preloadedInputName:'preloaded2',
      label:'Drag & Drop files here or click to browse',
      extensions: ['.pdf','.doc','.docx','DOCX','.jpg','.jpeg','.png','.gif','.svg'],
      required: true,
      mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','image/jpeg','image/png','image/gif','image/svg+xml']
  });
}


});



$('.rent_time select').on('change',function(){
  if($(this).val()!== ""){
    $(this).siblings('label').text("")
  }
})

if($('.rent_time select').val() !== ''){
    $('.rent_time select').siblings('label').text("");
}


$('#signed_by').on('focus',function(){
  $(this).siblings('label').text("Signed By")
})


$('#signed_by').on('blur',function(){
  if($(this).val()!= ''){
    $(this).siblings('label.signed_by_label').text("Signed By")
  }
  else{

    $(this).siblings('label.signed_by_label').text("Signed By - Your Name *")
  }
})


$(document).ready(function(){
  if($('input[name="preloaded2[]"]').length){
    if($('input[name="preloaded2[]"]').val()){
      $('.insurance_certificate input[name="rental_insurance_certificate[]"]').removeAttr('required')
    }
  }

  if($('input[name="preloaded[]"]').length){
    if($('input[name="preloaded[]"]').val()){
      $('.agreement_w9 input[name="rental_agreement_w9[]"]').removeAttr('required')
    }
  }

})


function validateMyForm()
{
  if($('.insurance_certificate .uploaded-file').length <= 0){
    $('.insurance_certificate input[name="rental_insurance_certificate[]"]').attr('required',true)
  }

  if($('.agreement_w9 .uploaded-file').length <= 0){
    $('.agreement_w9 input[name="rental_agreement_w9[]"]').attr('required',true)
  }

    // SIGNATURE VALIDATION on submit

  var canvas = $('canvas')[0];
  if($('.sign_input_validation').val() == "0"){
    $('.sign_error_msg').show();
    return false;
  }
  else{
    $('.sign_error_msg').hide();
    if($("#agree_chk").prop('checked') == false)
    {
      alert("You have to agree with terms & condition before submit, so please click on the checkbox");
      //e.preventDefault();
      return false;
    }
  }



  return true;
}


function isCanvasBlank(canvas) {
    var blank = document.createElement('canvas');
    blank.width = canvas.width;
    blank.height = canvas.height;

    blank.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    return canvas.toDataURL() == blank.toDataURL();
}


var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear').click(function(e) {
    e.preventDefault();
    sig.signature('clear');
    $("#signature64").val('');
    draw($(this));

    $('.sign_input_validation').val("0");
    if($('.sign_input_validation').val() == "0"){
      $('.sign_error_msg').show();
    }
    else{
      $('.sign_error_msg').hide();
    }


    // $('canvas').attr('width','100');
});


$(document).ready(function(){
if($('.rn_sign').length > 0){
var canvas = $('.kbw-signature').find('canvas')[0];
const context = canvas.getContext("2d");

const img = new Image()
img.src = $('.rn_sign').val()
img.onload = () => {
  context.drawImage(img, 0, 0)
}
}
});


$('#sig canvas').mousedown(function(){
  $(this).parents('#sig').siblings('.sign_input_validation').val("1");
    if($('.sign_input_validation').val() == "0"){
      $('.sign_error_msg').show();
    }
    else{
      $('.sign_error_msg').hide();
    }
})

// var canvas = document.getElementsByTagName('canvas')[0]
// var ctx = canvas.getContext("2d")
function draw(btn) {

  var canvas = btn.siblings('.kbw-signature').find('canvas')[0];
  var ctx = canvas.getContext("2d")
  //This line is actually not even needed...
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.rect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = 'white';
  ctx.fill();
}
$(document).ready(function(){
  // document.getElementsByTagName('canvas').width = 985.72;
  $('canvas').attr('width','985.72');
})


</script>

@endsection