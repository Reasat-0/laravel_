<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title>Driver Request</title>
</head>
<style>
	body{
		font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
	}
	.request_id_badge{
		background: #f6cb61;
		width:140px;
		height: 30px;
		line-height: 1.3;
		border-radius: 0;
		text-align: center;
		color:white;
		border-radius: 50%;
		position: fixed;
		right: -30px;
		top: -30px;
		border: 1px solid #000;
		border-radius: 15px;
	}
	.request_id_badge h4{
		padding: 6px 5px;
		font-size:12px;
		font-weight: 300;
		margin: 0;
		text-transform: uppercase;
		color: #000;
	}
	.insurance_title_secton{
		text-align: center;
	}
	.insurance_logo img{
		height: 135px;
	}
	.insurance_title_secton .title{
		font-weight: bold;
		text-transform: capitalize;
		font-style: italic;
		font-size: 21px;
		font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
		margin: 5px 0;
		margin-top: 20px;
	}
	.sub-title p{
		text-align: center;
		font-size: 15px;
	}

	.label_seciton{
		display: inline-block;
		padding: 0 15px;
	}
	.insurance_subtitle{
		text-align: center;
	}
	.checkbox-section{
		text-align: left;
		margin-left: -160px;
		margin-bottom: -10px;
		margin-top: 20px;
	}
	.label_seciton{
		text-align: center;
	}
	table{
		width: 100%;
		border-collapse: collapse;
	}
	table tr td{
		text-align: center;
		vertical-align: middle;
		padding: 5px;
		min-height: 50px;
		margin-bottom: 20px;
		line-height: 1.4;
		font-size: 1rem;
		border:1px solid red;
	}
	table tr td:first-child{
		width: 28%;
		text-align: left;
		font-weight: 700;
	}
	table tr td:last-child{
		width: 72%;
		text-align: left;
		padding-left: 20px;
	}
	.submitted-section{
		margin-top: 40px;
	}
	.submitted-section table tr td{
		width: 50%;
	}
	.submitted-section table tr td:nth-child(2n){
		padding-left: 20px;
	}

	/* NEW */
	.info-form-section .row{
		margin:10px 0;
	}
	.info-form-section .row input{
		margin-top:5px; 
		width:80%;
		border:none; 
		border-bottom:1px solid black;
		font-size:14px;
	}
	.info-form-section .row label{
		width:20%;
		font-size:14px;
		font-weight: 100;
		position: relative;
		letter-spacing: 0.5px;
	}
	.info-form-section .row textarea{
		border: none;
		border-bottom:1px solid black;
	}
	.submission-section{
		margin-top:90px;
	}
	.submission-section .row .col:first-child{
		width:60%; 
		display:inline-block;
	}
	.submission-section .row .col:last-child{
		display:inline-block;
		width:40%;
		margin-left: 20px;
	}
	.submission-section .row .col label{
		font-size:14px;
		font-weight: 100;
		position: relative;
		letter-spacing: 0.5px;
	}
	.submission-section .row .col.left_col input{
		margin-top:5px;
		width:70%;
		border: none;
		border-bottom:1px solid black;
		font-size:14px;
	}
	.submission-section .row .col.right_col input{
		margin-top:5px;
		width:70%;
		border: none;
		border-bottom:1px solid black;
		font-size:14px;
	}
	textarea,input{
		font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
		letter-spacing: 0.5px;
		font-size:14px;
		color:#2d2e2e;
	}
	.insurance_subtitle p{
		font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
		font-size: 14px;
	}
	footer {
	    position: fixed; 
	    bottom: -30px; 
	    left: 0; 
	    right: 0;
	    color: white;
	    height: 45px;
	    text-align: center;
	    /*background-color: #eff0f1;*/
	} 
	.footer-section-table{
		display: table;
		width: 100%;
		vertical-align: middle;
		table-layout: fixed;
	}
	.footer-section-row{
		display: table-row;
	}
	.footer-part{
		display: table-cell;
		height: 45px;
		vertical-align: middle;
	}
	.footer-part img{
		height: 30px;
		display: block;
		text-align: left;
	}
	.footer-part p{
		margin: 0;
		color: #676767;
		font-size: 12px;
		line-height: 1.3;
	}
			
	.mvr_file{
		margin-bottom: 2px;
		text-decoration: none;
		cursor: pointer;
		display: block;
	}
	.mbr-table tr td{
		font-size: 0.9rem;
		line-height: 1.4;
		font-weight: 600;
		border: none;
		vertical-align: top;
	}
	.mbr-table tr td:first-child{
		width: 100px;
	}
	.mbr-table tr td:last-child{
		width: calc(100% - 100px);
		padding-left: 10px;
	}
	.insurance_logo img{
		position: fixed;
		top: -40px;
		left: 0px;
		height: 80px;
	}
	.insurance_title_secton{
		margin-top: 50px;
	}
</style>
<body>
	<div class="request_id_badge">
		<h4 class="title"> Request ID : DR-{{$data->id}}</h4>
	</div>
	<div class="insurance_logo">
  		 <img src="{{ public_path() . '/assets/img/logo_ins.png' }}">
  	</div>

  <div class="insurance_pdf_main_section">
  	<div class="insurance_title_secton">
  		<h4 class="title">Driver Request</h4>
  	</div>
  	<div class="insurance_subtitle">
  		<p>Please fill out the following information when adding or deleting a driver to your coverage and attach a copy of the MVR</p>
  	</div>
  	
  	<div class="checkbox-section">
		<div class="label_seciton">
			<input type="checkbox" @if($data->request == 'add') checked @endif style="display: block;">
			<label style="display: block;">ADD</label>
		</div>
		<div class="label_seciton">
			<input type="checkbox" @if($data->request == 'delete') checked @endif style="display: block;">
			<label style="display: block;">DELETE</label>
		</div>
		<div class="label_seciton">
			<input type="checkbox" @if($data->request == 'amend') checked @endif style="display: block; ">
			<label style="display: block;">AMEND</label>
		</div>
	</div>

	<div class="info-form-section">
		<div class="row">
			<label for="company_name" style=""> Company Name :</label>
			<input type="text" value="{{$data->company_name}}" style="" />
		</div>
		<div class="row">
			<label for="company_name" style=""> Date Of Change :</label>
			<input type="text" value="{{$data->date_of_change}}" style="" />
		</div>
		<div class="row">
			<label for="company_name" style=""> Full Driver Name :</label>
			<input type="text" value="{{$data->driver_first_name}} {{$data->driver_last_name}}" style="width:78.5%" />
		</div>
		<div class="row">
			<label for="company_name" style=""> Date Of Birth :</label>
			<input type="text" value="{{$data->date_of_birth}}" style="width:82%" />
		</div>
		<div class="row">
			<label for="company_name" style=""> License State :</label>
			<input type="text" value="{{$data->license_state}}" style="width:83%" />
		</div>
		<div class="row">
			<label for="company_name" style=""> License # :</label>
			<input type="text" value="{{$data->license}}" style="width:86%" />
		</div>
		<div class="row">
			<label for="company_name" style=""> Years of Experience :</label>
			<input type="text" value="{{$data->years_of_experience}} YRS" style="width:76%" />
		</div>
		<div class="row">
			<label for="company_name" style=""> Years of CDL Experience :</label>
			<input type="text" value="{{$data->years_of_cdl_experience}} YRS" style="width:71%" />
		</div>
		<div class="row">
			<label for="company_name" style=""> Duties :</label>
			<input type="text" value="{{$data->duties}}" style="width:89%" />
		</div>
		<div class="row">
			<label for="company_name" style=""> Date Of Hire :</label>
			<input type="text" value="{{$data->date_of_hire}}" style="width:83%" />
		</div>
		<div class="row">
			<label for="company_name"> Additional Notes:</label>
			<input type="text" value="{{$data->additional}}" style="width:80%" />
		</div>
		@if(!$files->isEmpty())
			<div class="row" style="margin-top: 10px">
				<table class="mbr-table">
					<tr>
						<td>MVR File Link:</td>
						<td>@foreach($files as $rowdata)
							<a href="{{$rowdata->url}}" class="mvr_file">{{$rowdata->name}}</a>
							@endforeach
						</td>
					</tr>
				</table>
				<!-- <label for="company_name" style=""> MVR File Link:</label>
				@foreach($files as $rowdata)
				<a href="{{$rowdata->url}}" class="mvr_file">{{$rowdata->name}}</a>
				@endforeach -->
			</div>
		@endif

	</div>

	<div class="submission-section">
		<div class="row">
			<div class="col left_col" style="">
				<label for="company_name" style=""> Submitted By :</label>
				<input type="text" value="{{$data->submitted_by}}" style="" />
			</div>
			<div class="col right_col" style="">
				<label for="company_name" style=""> Date:</label>
				<input type="text" value="{{$data->date}}" style="" />
			</div>
		</div>
	</div>
  </div>

<footer>
    <div class="footer-section-table">
    	<div class="footer-section-row">
	    	<div class="footer-part footer-one">
	    		<img src="{{ public_path() . '/assets/img/logo.png' }}">
	    	</div>
	    	<div class="footer-part footer-two">
	    		<p>903 W. Industrial Ave. Midland, TX 79701</p>
	    	</div>
	    	<div class="footer-part footer-three">
	    		<p>432-253-9651</p>
	    	</div>
	    	<div class="footer-part footer-four">
	    		<p>info@evoilfieldservices.com</p>
	    	</div>
	    </div>
    </div>
</footer>

</body>
</html> 
