@extends('layouts.master')

@section('content')

        <div class="content manage-inventory-content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12 mx-auto">
                <div class="card insurance_root_card">
                  <div class="card-header card-header-tabs card-header-rose custom_card_header mang_inventory_card_header" id="insurance_header_section">
                    <div class="nav-tabs-navigation">
                      <div class="nav-tabs-wrapper">
                        <p class="nav-tabs-title">Insurance</p>
                        <ul class="nav nav-tabs" data-tabs="tabs" id="tabMenu">

                          <li class="nav-item">
                            <a class="nav-link active" href="#insurance_tab" data-toggle="tab">
                              <i class="material-icons">code</i>Direct Insurance
                              <div class="ripple-container"></div>
                            </a>
                          </li>

                          <!-- <li class="nav-item">
                            <a class="nav-link" href="#agreement_tab" data-toggle="tab">
                              <i class="material-icons">source</i>Agreements
                              <div class="ripple-container"></div>
                            </a>
                          </li> -->
                          
                          

                          <li class="nav-item">
                            <a class="nav-link" href="#manage_tab" data-toggle="tab">
                              <i class="material-icons">cloud</i> View Requests
                              <div class="ripple-container"></div>
                            </a>
                          </li>

                        </ul>
                      </div>
                    </div>
                  </div>

                  <div style="margin-top: 15px;">
                      @include('partials.messages')
                  </div>

                  <div class="excelSuccessMsg alert alert-success alert-dismissible" role="alert" style="display: none;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success:</strong> Excel Report has been downloaded.
                  </div>

                  <input type="hidden" class="submitted_by_hidden" value="{{$name}} - {{$email}}">

                  <div class="card-body manage-inventory-content-card-body">
                    <div class="tab-content insurance_parent_tab_content">
                      <div class="tab-pane active" id="insurance_tab">
                        <div class="card">
                          <div class="card-header card-header-tabs card-header-rose custom_card_header mng_view_card_header" id="insurance_tab_header">
                            <div class="nav-tabs-navigation">
                              <div class="nav-tabs-wrapper">
                                <ul class="nav nav-tabs sub-insurance-nav" data-tabs="tabs">
                                  <li class="nav-item">
                                    <a class="nav-link active" href="#equipment_tab" data-toggle="tab">
                                      <i class="material-icons">code</i>Equipment Request
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="#vehicle_tab" data-toggle="tab">
                                      <i class="material-icons">code</i>Vehicle Request
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li>
                                   <li class="nav-item">
                                    <a class="nav-link" href="#driver_tab" data-toggle="tab">
                                      <i class="material-icons">code</i>Driver Request
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>

                          <div class="tab-content">
                            <div class="tab-pane inventory_sub_card insurance_tab active" id="equipment_tab">
                              <form method="post" action="{{ route('insurance.store') }}" class="form-horizontal insurance_form">
                                @csrf
                                <input type="hidden" name="tab" value="equipment">
                        

                                <div class="row">
                                  <div class="col-md-12 mx-auto">
                                    <div class="card form_card">
                                      <div class="card-header card-header-rose card-header-icon">
                                        <div class="card-icon">
                                          <i class="material-icons">assignment</i>
                                        </div>
                                        <h4 class="card-title">Equipment Insurance</h4>
                                        <div class="row eq_search_row search_row">
                                          <div class="col-md-4 wrap searchWrapper">
                                            <div class="search">
                                              <input type="text" class="searchTerm" placeholder="Search With VIN"><input type="hidden" class="hiddenInsurance" value="equipment">
                                              <button type="button" id="equip_refresh_btn" class="search_reset_button" style="display:none;">
                                                <i class="fa fa-refresh"></i>
                                              </button>
                                              <button type="button" id="equipBtn" class="searchButton">
                                                <i class="fa fa-search"></i>
                                              </button>
                                            </div>
                                             <div class="suggestionList">
                                             </div>

                                            <div id="equip_message" style="text-align: center; margin-top: 5px; font-weight: 500"></div>
                                          </div>
                                        </div>
                                        
                                      </div>

                                      <div class="card-body">
                                        <div class="row">
                                          <div class="col-md-11 mx-auto">

                                            <div class="toolbar">
                                            </div>
                                            <div class="row form_header mb-5">
                                              <div class="col-md-12 text-center">
                                                <h2>
                                                  Equipment Request
                                                </h2>
                                              </div>
                                              <div class="col-md-12 text-center">
                                                <p>
                                                  Please fill out the following information when adding or deleting equipment to your coverage
                                                </p>
                                              </div>
                                              <div class="col-md-12 text-center add_amend_del_warning">
                                                <p style="color:#3594d0;font-weight:500; ">
                                                </p>
                                              </div>
                                            </div>
                                            <div class="row form-top_body mx-auto preview_checkbox_row">
                                              <div class="col-md-12 text-center">
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" name="request" value="add" id="equip_add_chk">Add
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" name="request" value="delete" id="equip_delete_chk" disabled="">Delete
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" name="request" value="amend" id="equip_amend_chk" disabled="">Amend
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="company_name_equip" class="bmd-label-floating input_label">Company Name</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="dr_company_name_equip" name="company_name" value="Oil Field Services" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="effective_date_equip" class="bmd-label-floating input_label">Effective Date</label>
                                                  <input type="text" class="form-control form_input_field form_fields datepicker" id="effective_date_equip" name="effective_date" value="{{date('m/d/Y')}}" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row mt-3 mb-3">
                                              <div class="col-md-12 insurance_info">
                                                <h4>Equipment Information : </h4>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="year_equip" class="bmd-label-floating input_label">Year *</label>
                                                  <input type="text" class="form-control form_input_field form_fields year_to_pick" id="year_equip" name="year" required>
                                                </div>
                                              </div>

                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="make_equip" class="bmd-label-floating input_label">Make *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="make_equip" name="make" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="model_equip" class="bmd-label-floating input_label">Model *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="model_equip" name="model" required>
                                                </div>
                                              </div>

                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vin_equip" class="bmd-label-floating input_label">VIN *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vin_equip" name="vin" required>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="row input_row">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="unit_equip" class="bmd-label-floating input_label">Unit *</label>
                                                  <input type="number" class="form-control form_input_field form_fields" id="unit_equip" name="unit" required>
                                                </div>
                                              </div>

                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="value_equip" class="bmd-label-floating input_label">Value *</label>
                                                  <input type="number" class="form-control form_input_field form_fields" id="value_equip" name="value" required>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="desc_equip_accessories" class="bmd-label-floating input_label">Description of Equipment Accessories *</label>
                                                  <textarea class="form-control form_input_field form_fields" rows="5" name="description_equipment_accessories" id="desc_equip_accessories" required></textarea>
                                                </div>
                                              </div>
                                            </div>  

                                            <!-- <div class="row input_row insurance_address_card_header">
                                              <div class="col-md-6">
                                                <h5> Garraging Location Address * </h5>
                                              </div>
                                            </div>  -->
                                            <fieldset class="custom_fieldset">
                                                <legend>Garraging Location Address</legend> 
                                              <div class="row insurance_address_card mb-2">
                                                  <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="equip_garraging_line_1" class="bmd-label-floating input_label">Address 1 *</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="equip_garraging_line_1" name="equip_garraging_address_1" required>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 input_wrapper">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="equip_garraging_line_2" class="bmd-label-floating input_label">Address 2 *</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="equip_garraging_line_2" name="equip_garraging_address_2" required>
                                                    </div>
                                                  </div>                                                                               
                                                  <div class="col-md-6 input_wrapper input_row">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="equip_garraging_city" class="bmd-label-floating input_label"> City *</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="equip_garraging_city" name="equip_garraging_city" required>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 input_wrapper input_row">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="equip_garraging_state" class="bmd-label-floating input_label">State *</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="equip_garraging_state" name="equip_garraging_state" required>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-6 input_wrapper input_row">
                                                    <div class="form-group bmd-form-group">
                                                      <label for="equip_garraging_zip" class="bmd-label-floating input_label">Zip *</label>
                                                      <input type="text" class="form-control form_input_field form_fields" id="equip_garraging_zip" name="equip_garraging_zip" required>
                                                    </div>
                                                  </div>          
                                              </div> 
                                            </fieldset>  

                                            
                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="lease_purchase_equip" class="bmd-label-floating input_label">Rental or Purchase *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="lease_purchase_equip" name="lease_purchase" required>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="finance_company_name_equip" class="bmd-label-floating input_label">Finance Company/Lessor Name *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="finance_company_name_equip" name="finance_company_name" required>
                                                </div>
                                              </div>
                                            </div>

                                            <!-- <div class="row input_row insurance_address_card_header mt-1">
                                              <div class="col-md-6">
                                                <h5> Finance Company/Lessor Address * </h5>
                                              </div>
                                            </div>                                      
                                            <div class="row insurance_address_card mb-2">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="equip_lessor_line_1" class="bmd-label-floating input_label">Address 1 *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="equip_lessor_line_1" name="equip_lessor_address_1" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="equip_lessor_line_2" class="bmd-label-floating input_label">Address 2 *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="equip_lessor_line_2" name="equip_lessor_address_2" required>
                                                </div>
                                              </div>                                                                               
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="equip_lessor_city" class="bmd-label-floating input_label"> City *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="equip_lessor_city" name="equip_lessor_city" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="equip_lessor_state" class="bmd-label-floating input_label">State *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="equip_lessor_state" name="equip_lessor_state" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="equip_lessor_zip" class="bmd-label-floating input_label">Zip *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="equip_lessor_zip" name="equip_lessor_zip" required>
                                                </div>
                                              </div>                                                                              
                                            </div> 
                                            </div>  -->
                                            <fieldset class="custom_fieldset">
                                              <legend>Finance Company/Lessor Address</legend>
                                              <div class="row insurance_address_card mb-2">
                                                <div class="col-md-6 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="equip_lessor_line_1" class="bmd-label-floating input_label">Address 1 *</label>
                                                    <input type="text" class="form-control form_input_field form_fields" id="equip_lessor_line_1" name="equip_lessor_address_1" required>
                                                  </div>
                                                </div>
                                                <div class="col-md-6 input_wrapper">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="equip_lessor_line_2" class="bmd-label-floating input_label">Address 2 *</label>
                                                    <input type="text" class="form-control form_input_field form_fields" id="equip_lessor_line_2" name="equip_lessor_address_2" required>
                                                  </div>
                                                </div>                                                                               
                                                <div class="col-md-6 input_wrapper input_row">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="equip_lessor_city" class="bmd-label-floating input_label"> City *</label>
                                                    <input type="text" class="form-control form_input_field form_fields" id="equip_lessor_city" name="equip_lessor_city" required>
                                                  </div>
                                                </div>
                                                <div class="col-md-6 input_wrapper input_row">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="equip_lessor_state" class="bmd-label-floating input_label">State *</label>
                                                    <input type="text" class="form-control form_input_field form_fields" id="equip_lessor_state" name="equip_lessor_state" required>
                                                  </div>
                                                </div>
                                                <div class="col-md-6 input_wrapper input_row">
                                                  <div class="form-group bmd-form-group">
                                                    <label for="equip_lessor_zip" class="bmd-label-floating input_label">Zip *</label>
                                                    <input type="text" class="form-control form_input_field form_fields" id="equip_lessor_zip" name="equip_lessor_zip" required>
                                                  </div>
                                                </div>                                                                              
                                              </div>
                                            </fieldset> 

                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper long_input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="company_phone_lessor_equip" class="bmd-label-floating input_label">Finance Company/Lessor Phone or Email for EPI *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="company_phone_lessor_equip" name="finance_company_phone_email" required>
                                                </div>
                                              </div>
                                            </div> 

                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="additional_note_equip" class="bmd-label-floating input_label">Additional Notes</label>
                                                  <textarea class="form-control form_input_field form_fields" rows="5" name="additional_notes" id="additional_note_equip"></textarea>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="row mt-5">
                                              <div class="col-md-7 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="submitted_to_equip" class="bmd-label-floating input_label">Submitted By</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="submitted_to_equip" name="submitted_by" value="{{$name}} - {{$email}}" readonly>
                                                </div>
                                              </div>
                                              <div class="col-md-5 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="submitted_date_equip" class="bmd-label-floating input_label">Date</label>
                                                  <input type="text" class="form-control form_input_field form_fields datepicker" id="submitted_date_equip" name="date" value="{{date('m/d/Y')}}">
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row mt-6 input_row">
                                              <div class="col-md-7 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="send_to_equip" class="bmd-label-floating input_label">Send Mail To</label>
                                                  <input type="email" class="form-control form_input_field form_fields" id="send_to_equip" name="send_mail_to" value="" required>
                                                </div>
                                              </div>
                                            </div>

                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                    <div class="row preview_row" style="display:none;">
                                      <div class="col-md-12 mx-auto insurance_padding">
                                        <div class="card preview_card equipment_preview_card">
                                          <div class="card-header card-header-rose card-header-icon">
                                            <div class="card-icon">
                                              <i class="material-icons">assignment</i>
                                            </div>
                                            <h4 class="card-title">Submitted Preview</h4>
                                          </div>
                                          <div class="card-body">
                                            <div class="row preview_checkbox_row">
                                              <div class="col-md-12 text-center">
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" value="add" >Add
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" value="delete">Delete
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" value="amend">amend
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>                          
                                      </div>
                                      <div class="col-md-12 mx-auto insurance_padding preview_page_btns_col">
                                        <div class="row">
                                          <div class="col-md-6 text-left save_btn_wrapper">
                                            <button type="button" class="btn btn-md btn-round btn-warning go_back ">
                                              Go Back
                                            </button>
                                          </div>

                                          <div class="col-md-6 text-right save_btn_wrapper">
                                            <button type="submit" class="btn btn-md btn-round btn-success confirm">
                                              Confirm
                                            </button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-12 text-right save_btn_wrapper preview_btn_wrapper">
                                    <button type="button" class="btn btn-md btn-round btn-success preview_btn">
                                      Preview and Send
                                    </button>
                                  </div>
                                </div>

                              </form>
                            </div>
                              
                            <div class="tab-pane inventory_sub_card insurance_tab" id="vehicle_tab">
                              <form method="post" action="{{ route('insurance.store') }}" class="form-horizontal insurance_form">
                                @csrf
                                <input type="hidden" name="tab" value="vehicle">
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="card form_card">
                                      <div class="card-header card-header-rose card-header-icon">
                                        <div class="card-icon">
                                          <i class="material-icons">assignment</i>
                                        </div>
                                        <h4 class="card-title">Vehicle Insurance</h4>
                                        <div class="row eq_search_row search_row">
                                          <div class="col-md-4 wrap searchWrapper">
                                            <div class="search">
                                              <input type="text" class="searchTerm" placeholder="Search With VIN">
                                              <input type="hidden" class="hiddenInsurance" value="vehicle">
                                              <button type="button" id="vehicle_refresh_btn" class="search_reset_button" style="display:none;">
                                                <i class="fa fa-refresh"></i>
                                              </button>                                        
                                              <button type="button" id="vehicleBtn" class="searchButton">
                                                <i class="fa fa-search"></i>
                                              </button>
                                            </div>

                                            <div class="suggestionList">
                                            </div>

                                            <div id="vehicle_message" style="text-align: center; margin-top: 5px; font-weight: 500"></div>
                                          </div>
                                        </div>                                  
                                      </div>

                                      <div class="card-body">
                                        <div class="toolbar">
                                        </div>
                                        <div class="row">
                                          <div class="col-md-11 mx-auto">
                                            <div class="row form_header mb-5">
                                              <div class="col-md-12 text-center">
                                                <h2>
                                                  Vehicle Change Request
                                                </h2>
                                              </div>
                                              <div class="col-md-12 text-center">
                                                <p>
                                                  Please fill out the following information when adding or deleting a vehicle to your coverage
                                                </p>
                                              </div>
                                              <div class="col-md-12 text-center add_amend_del_warning">
                                                <p style="color:#3594d0;font-weight:500; ">
                                                </p>
                                              </div>                                        
                                            </div>
                                            <div class="row form-top_body mx-auto preview_checkbox_row">
                                              <div class="col-md-12 text-center">
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" name="request" value="add" id="vehicle_add_chk">Add
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" name="request" value="delete" id="vehicle_delete_chk" disabled="">Delete
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" name="request" value="amend" id="vehicle_amend_chk" disabled="">Amend
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row form-bottom-body mt-2 input_row">
                                              <div class="col-md-12 mx-auto input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_company_name" class="bmd-label-floating input_label">Company Name</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_company_name" name="company_name" value="Oilfield Services" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_effective_date" class="bmd-label-floating input_label">Effective Date</label>
                                                  <input type="text" class="form-control form_input_field form_fields datepicker" id="vehicle_effective_date" name="effective_date" value="{{date('m/d/Y')}}" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row mt-2 mb-3">
                                              <div class="col-md-12 insurance_info">
                                                <h4>Vehicle Information : </h4>
                                              </div>
                                            </div>
                                            <div class="row mb-1 input_row">
                                              <div class="col-md-6 input_wrapper physical-liabilty-section">
                                                <div class="form-check" style="">
                                                  <h5>Liability *</h5>
                                                  <label class="form-check-label">
                                                    <input class="form-check-input form_fields vehicle_liability" name="liability" type="checkbox" value="yes">Yes
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                  <label class="form-check-label">
                                                    <input class="form-check-input vehicle_liability" name="liability" type="checkbox" value="no">No
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper physical-liabilty-section">
                                                <div class="form-check" style="">
                                                  <h5>Physical Damage *</h5>
                                                  <label class="form-check-label">
                                                    <input class="form-check-input form_fields vehicle_physical_dam" name="damage" type="checkbox" value="yes">Yes
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                  <label class="form-check-label">
                                                    <input class="form-check-input vehicle_physical_dam" name="damage" type="checkbox" value="no">No
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_year" class="bmd-label-floating input_label">Year *</label>
                                                  <input type="text" class="form-control form_input_field form_fields year_to_pick" id="vehicle_year" name="year" value="" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_make" class="bmd-label-floating input_label">Make *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_make" name="make" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_model" class="bmd-label-floating input_label">Model *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_model" name="model" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_vin" class="bmd-label-floating input_label">VIN *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_vin" name="vin" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_body_type" class="bmd-label-floating input_label">Body Type *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_body_type" name="body_type" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_st" class="bmd-label-floating input_label">State Registered/ Titled *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_st" name="state_registered" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_unit" class="bmd-label-floating input_label">Unit *</label>
                                                  <input type="number" class="form-control form_input_field form_fields" id="vehicle_unit" name="unit" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_gross_weight" class="bmd-label-floating input_label">Gross Vehicle Weight *</label>
                                                  <input type="number" class="form-control form_input_field form_fields" id="vehicle_gross_weight" name="gross_vehicle_weight" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group currency_form_group">
                                                  <label for="vehicle_cost_new" class="bmd-label-floating input_label">Cost New *</label>
                                                  <span class="currency_symbol"> $ </span>
                                                  <input type="number" class="form-control form_input_field form_fields currency_field" id="vehicle_cost_new" name="cost_new" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_mlg_rad" class="bmd-label-floating input_label">Milleage Radius *</label>
                                                  <input type="number" class="form-control form_input_field form_fields" id="vehicle_mlg_rad" name="mileage_radius" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="spcl_eq" class="bmd-label-floating input_label">Special Equipment (If any provide Description and Value) *</label>
                                                  <textarea class="form-control form_input_field form_fields" rows="3" name="special_equipment" id="spcl_eq" required></textarea>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- <div class="row input_row insurance_address_card_header">
                                              <div class="col-md-6">
                                                <h5> Garraging Location Address * </h5>
                                              </div>
                                            </div>  -->
                                            <fieldset class="custom_fieldset">
                                                <legend>Garraging Location Address</legend>            
                                                <div class="row insurance_address_card mb-2">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_garraging_line_1" class="bmd-label-floating input_label">Address 1 *</label>
                                                  <input type="text" class="form-control form_input_field form_fields garraging_address_triggerer" id="vehicle_garraging_line_1" name="vehicle_garraging_address_1" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_garraging_line_2" class="bmd-label-floating input_label">Address 2 *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_garraging_line_2" name="vehicle_garraging_address_2" required>
                                                </div>
                                              </div>                                                                               
                                              <div class="col-md-6 input_wrapper input_row">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_garraging_city" class="bmd-label-floating input_label"> City *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_garraging_city" name="vehicle_garraging_city" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper input_row">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_garraging_state" class="bmd-label-floating input_label">State *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_garraging_state" name="vehicle_garraging_state" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper input_row">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_garraging_zip" class="bmd-label-floating input_label">Zip *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_garraging_zip" name="vehicle_garraging_zip" required>
                                                </div>
                                              </div>                                                                              
                                                </div>
                                            </fieldset>


                                            <div class="row input_row">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_usage" class="bmd-label-floating input_label">Usage *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_usage" name="vehicle_usage" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_lease_of_purchase" class="bmd-label-floating input_label">Rental or Purchase *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_lease_of_purchase" name="lease_of_purchase" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicel_fin_comp" class="bmd-label-floating input_label">Finance Company / Lessor Name *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicel_fin_comp" name="finance_company_name" required>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- <div class="row input_row insurance_address_card_header mt-1">
                                              <div class="col-md-6">
                                                <h5> Finance Company / Lessor Address * </h5>
                                              </div>
                                            </div>  -->
                                            <fieldset class="custom_fieldset">
                                                <legend>Finance Company / Lessor Address</legend>
                                                <div class="row insurance_address_card mb-2">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_lessor_line_1" class="bmd-label-floating input_label">Address 1 *</label>
                                                  <input type="text" class="form-control form_input_field form_fields lessor_address_triggerer" id="vehicle_lessor_line_1" name="vehicle_lessor_address_1" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_lessor_line_2" class="bmd-label-floating input_label">Address 2 *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_lessor_line_2" name="vehicle_lessor_address_2" required>
                                                </div>
                                              </div>                                                                               
                                              <div class="col-md-6 input_wrapper input_row">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_lessor_city" class="bmd-label-floating input_label"> City *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_lessor_city" name="vehicle_lessor_city" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper input_row">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_lessor_state" class="bmd-label-floating input_label">State *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_lessor_state" name="vehicle_lessor_state" required>
                                                </div>
                                              </div>
                                              <div class="col-md-6 input_wrapper input_row">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_lessor_zip" class="bmd-label-floating input_label">Zip *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicle_lessor_zip" name="vehicle_lessor_zip" required>
                                                </div>
                                              </div>                                                                              
                                                </div>
                                            </fieldset>

                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper long_input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicel_fin_comp_info" class="bmd-label-floating input_label">Finance Company/Lessor Phone or Email for EPI *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="vehicel_fin_comp_info" name="finance_company_phone_email" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="vehicle_additional" class="bmd-label-floating input_label">Additional</label>
                                                  <textarea class="form-control form_input_field form_fields" rows="5" name="additional" id="vehicle_additional"></textarea>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row mt-5">
                                              <div class="col-md-7 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="submitted_by_vehicle" class="bmd-label-floating input_label">Submitted By</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="submitted_by_vehicle" name="submitted_by" value="{{$name}} - {{$email}}" readonly>
                                                </div>
                                              </div>
                                              <div class="col-md-5 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="date_of_sub_vehicle" class="bmd-label-floating input_label">Date</label>
                                                  <input type="text" class="form-control form_input_field form_fields datepicker" name="date" id="date_of_sub_vehicle" value="{{date('m/d/Y')}}">
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row mt-6 input_row">
                                              <div class="col-md-7 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="send_to_vehicle" class="bmd-label-floating input_label">Send Mail To</label>
                                                  <input type="email" class="form-control form_input_field form_fields" id="send_to_vehicle" name="send_mail_to" value="" required>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row preview_row" style="display:none;">
                                      <div class="col-md-12 mx-auto insurance_padding">
                                        <div class="card preview_card vehicle_preview_card">
                                          <div class="card-header card-header-rose card-header-icon">
                                            <div class="card-icon">
                                              <i class="material-icons">assignment</i>
                                            </div>
                                            <h4 class="card-title">Submitted Preview</h4>
                                          </div>
                                          <div class="card-body">
                                            <div class="row preview_checkbox_row">
                                              <div class="col-md-12 text-center">
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" value="add" >ADD
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" value="delete">DELETE
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" value="amend">AMEND
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>                          
                                      </div>
                                      <div class="col-md-12 mx-auto insurance_padding preview_page_btns_col">
                                        <div class="row">
                                          <div class="col-md-6 text-left save_btn_wrapper">
                                            <button type="button" class="btn btn-md btn-round btn-warning go_back ">
                                              Go Back
                                            </button>
                                          </div>
                                          <div class="col-md-6 text-right save_btn_wrapper">
                                             <button type="submit" class="btn btn-md btn-round btn-success confirm">
                                              Confirm
                                            </button>
                                            </div> 
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Button -->
                                  <div class="col-md-12 text-right save_btn_wrapper preview_btn_wrapper">
                                    <button type="button" class="btn btn-md btn-round btn-success preview_btn">
                                      Preview and Send
                                    </button>
                                  </div>                              
                                </div>
                              </form>
                            </div>


                            <div class="tab-pane inventory_sub_card insurance_tab" id="driver_tab">
                              <form method="post" action="{{ route('insurance.store') }}" class="form-horizontal insurance_form" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="tab" value="driver">
                                
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="card form_card current">
                                      <div class="card-header card-header-rose card-header-icon">
                                        <div class="card-icon">
                                          <i class="material-icons">assignment</i>
                                        </div>
                                        <h4 class="card-title">Driver Insurance</h4>
                                        <div class="row dr_search_row search_row">
                                          <div class="col-md-4 wrap searchWrapper">
                                            <div class="search">
                                              <input type="text" class="searchTerm" placeholder="Search With License#">
                                              <input type="hidden" class="hiddenInsurance" value="driver">
                                              <button type="button" id="driver_refresh_btn" class="search_reset_button" style="display:none;">
                                                <i class="fa fa-refresh"></i>
                                              </button>                                         
                                              <button type="button" id="driverBtn" class="searchButton">
                                                <i class="fa fa-search"></i>
                                              </button>
                                            </div>

                                            <div class="suggestionList">
                                             </div>

                                            <div id="driver_message" style="text-align: center; margin-top: 5px; font-weight: 500"></div>
                                          </div>
                                        </div>                                  
                                      </div>

                                      <div class="card-body">
                                        <div class="toolbar">
                                        </div>
                                        <div class="row">
                                          <div class="col-md-11 mx-auto">
                                            <div class="row form_header mb-5">
                                              <div class="col-md-12 mx-auto text-center">
                                                <h2>
                                                  Driver Request
                                                </h2>
                                              </div>
                                              <div class="col-md-12 text-center">
                                                <p>
                                                  Please fill out the following information when adding or deleting a driver to your coverage and attach a copy of the MVR
                                                </p>
                                              </div>
                                              <div class="col-md-12 text-center add_amend_del_warning">
                                                <p style="color:#3594d0;font-weight:500; ">
                                                </p>
                                              </div>                                        
                                            </div>
                                            <div class="row form-top_body mx-auto preview_checkbox_row">
                                              <div class="col-md-12 text-center">
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" name="request" value="add" id="driver_add_chk">ADD
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" name="request" value="delete" id="driver_delete_chk" disabled="">DELETE
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" name="request" value="amend" id="driver_amend_chk" disabled="">AMEND
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="company_name" class="bmd-label-floating input_label">Company Name</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="dr_company_name" name="company_name" value="Oilfield Services" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="company_name" class="bmd-label-floating input_label">Date Of Change</label>
                                                  <input type="text" class="form-control form_input_field form_fields datepicker" id="dr_date_of_change" name="date_of_change" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row name_row">
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="dr_full_name" class="bmd-label-floating input_label">First Name *</label>
                                                  <input type="text" class="form-control form_input_field form_fields name" id="dr_first_name" name="driver_first_name" required>
                                                </div>
                                              </div>                                       
                                              <div class="col-md-6 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="dr_full_name" class="bmd-label-floating input_label">Last Name *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="dr_last_name" name="driver_last_name" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="dr_dob" class="bmd-label-floating input_label">Date of Birth *</label>
                                                  <input type="text" class="form-control form_input_field form_fields datepicker" id="dr_dob" name="date_of_birth" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="dr_license_st" class="bmd-label-floating input_label">License State *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="dr_license_st" name="license_state" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="dr_license_num" class="bmd-label-floating input_label">License # *</label>
                                                  <input type="number" class="form-control form_input_field form_fields" id="dr_license_num" name="license" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row year_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="dr_years_of_ex" class="bmd-label-floating input_label">Year's of Experience *</label>
                                                  <input type="text" class="form-control form_input_field form_fields years_field" id="dr_years_of_ex" name="years_of_experience" maxlength="2" pattern="[0-9]" required>
                                                  <span class="years_text" style="display:none"> YRS </span>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row year_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="dr_years_of_cdl_ex" class="bmd-label-floating input_label">Year's of CDL Experience *</label>
                                                  <input type="tel" class="form-control form_input_field form_fields years_field" id="dr_years_of_cdl_ex" name="years_of_cdl_experience" maxlength="2" required>
                                                  <span class="years_text" style="display:none"> YRS </span>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="dr_duties" class="bmd-label-floating input_label">Duties *</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="dr_duties" name="duties" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="dr_doh" class="bmd-label-floating input_label">Date of Hire *</label>
                                                  <input type="text" class="form-control form_input_field form_fields datepicker" id="dr_doh" name="date_of_hire" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row input_row">
                                              <div class="col-md-12 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="dr_additional" class="bmd-label-floating input_label">Additional</label>
                                                  <textarea class="form-control form_input_field form_fields" rows="5" name="additional" id="dr_additional"></textarea>
                                                </div> 
                                              </div>
                                            </div>
                                            <!-- <div class="row input_row files_uploader_row">
                                              <div class="col-md-12 input_wrapper">
                                                <label for="multiple_file input_label"> MVR : </label>
                                                <input type="file" class="multi" accept="pdf">
                                              </div>
                                            </div> -->
                                            <div class="row input_row">
                                              <div class="col-md-12">
                                                <label for="multiple_file input_label"> MVR * </label>
                                                   <div class="input-files driver_files">
                                                     <!-- <div id="old_img" style="display: none;">
                                                      <div class="input-images">
                                                        <div class="image-uploader has-files">
                                                            <div class="uploaded">

                                                            </div>
                                                        </div>
                                                      </div>
                                                    </div> -->

                                                   </div>
                                              </div>
                                            </div>
                                            <div class="row mt-5">
                                              <div class="col-md-7 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="submitted_to_driver" class="bmd-label-floating input_label">Submitted By</label>
                                                  <input type="text" class="form-control form_input_field form_fields" id="submitted_to_driver" name="submitted_by" value="{{$name}} - {{$email}}" readonly>
                                                </div>
                                              </div>
                                              <div class="col-md-5 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="date_of_sub_driver" class="bmd-label-floating input_label">Date</label>
                                                  <input type="text" class="form-control form_input_field form_fields datepicker" id="date_of_sub_driver" name="date" value="{{date('m/d/Y')}}" required>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="row mt-6 input_row">
                                              <div class="col-md-7 input_wrapper">
                                                <div class="form-group bmd-form-group">
                                                  <label for="send_to_driver" class="bmd-label-floating input_label">Send Mail To</label>
                                                  <input type="email" class="form-control form_input_field form_fields" id="send_to_driver" name="send_mail_to" value="" required>
                                                </div>
                                              </div>
                                            </div>
                                            
                                          </div>
                                          
                                        </div>

                                      </div>
                                    </div>
                                    <div class="row preview_row" style="display:none;">
                                      <div class="col-md-12 mx-auto insurance_padding">
                                        <div class="card preview_card">
                                          <div class="card-header card-header-rose card-header-icon">
                                            <div class="card-icon">
                                              <i class="material-icons">assignment</i>
                                            </div>
                                            <h4 class="card-title">Submitted Preview</h4>
                                          </div>
                                          <div class="card-body">
                                            <div class="row preview_checkbox_row">
                                              <div class="col-md-12 text-center">
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" value="add" >ADD
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" value="delete">DELETE
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                    <input class="form-check-input insurance_check" type="checkbox" value="amend">AMEND
                                                    <span class="form-check-sign">
                                                      <span class="check"></span>
                                                    </span>
                                                  </label>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>                          
                                      </div>
                                      <div class="col-md-12 mx-auto insurance_padding preview_page_btns_col">
                                        <div class="row">
                                          <div class="col-md-6 text-left save_btn_wrapper">
                                            <button type="button" class="btn btn-md btn-round btn-warning go_back ">
                                              Go Back
                                            </button>
                                          </div>
                                          <div class="col-md-6 text-right save_btn_wrapper">
                                            <button type="submit" class="btn btn-md btn-round btn-success confirm">
                                              Confirm
                                            </button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Button -->
                                  <div class="col-md-12 text-right save_btn_wrapper preview_btn_wrapper">
                                    <button type="button" class="btn btn-md btn-round btn-success preview_btn">
                                      Preview and Send
                                    </button>
                                  </div>                             
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>

                    

                      <div class="tab-pane inventory_sub_card insurance_tab" id="manage_tab">
                        <div class="card">
                          <div class="card-header card-header-tabs card-header-rose custom_card_header mng_view_card_header" id="view_manage_tab_header">
                            <div class="nav-tabs-navigation">
                              <div class="nav-tabs-wrapper">
                                <ul class="nav nav-tabs sub-insurance-nav" data-tabs="tabs">
                                  <li class="nav-item">
                                    <a class="nav-link active" href="#view_request" data-toggle="tab">
                                      <i class="material-icons">bug_report</i>Insurance Request
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li>
                                  <!-- <li class="nav-item">
                                    <a class="nav-link" href="#view_agreement" data-toggle="tab">
                                      <i class="material-icons">code</i>Agreement Request
                                      <div class="ripple-container"></div>
                                    </a>
                                  </li> -->
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="tab-content">
                            <div class="tab-pane active" id="view_request">
                              <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                  <i class="material-icons">assignment</i>
                                </div>
                                <div class="insurance_btn_section_main">
                                  <div class="insurance_btn_section">
                                    <h4 class="card-title">Manage Request</h4>
                                    <!-- <button type="button" class="btn btn-sm btn-primary generate_report_btn">Generate Report</button> -->
                                  </div>
                                  <div class="card report_generate_section" style="display: none">
                                  
                                  <form method='post' action="{{route('generateInsuranceReport')}}">
                                    {{ csrf_field() }}

                                    <div class="card-body">
                                      <div class="row">
                                        <div class="col-md-3 second input_wrapper custom_select2_col">
                                          <div class="form-group custom-form-select">
                                            <select class="custom-select hasvalue insurace_type_report select2 form_input_field" id="insurace_type_report" name="insurace_type_report" required="true" aria-required="true">
                                              <option value="all">All</option>
                                              <option value="equipment">Equipment</option>
                                              <option value="vehicle">Vehicle</option>
                                              <option value="driver">Driver</option>
                                            </select>
                                            <div class="form-element-bar">
                                            </div>
                                            <label class="form-element-label" for="variable_data_type input_label">Request Type</label>
                                          </div>
                                        </div>
                                        <div class="col-md-3 second input_wrapper custom_select2_col">
                                          <div class="form-group custom-form-select">
                                            <select class="custom-select hasvalue insurace_time_interval select2 form_input_field" id="insurace_time_interval" name="insurace_time_interval" required="true" aria-required="true">
                                              <option value="custom">Custom</option>
                                              <option value="daily">Daily</option>
                                              <option value="weekly">Weekly</option>
                                              <option value="monthly">Monthly</option>
                                            </select>
                                            <div class="form-element-bar">
                                            </div>
                                            <label class="form-element-label" for="time_interval input_label">Duration</label>
                                          </div>
                                        </div>
                                        <div class="col-md-3">
                                          <div class="form-group bmd-form-group">
                                            <label for="insurance_report_from" class="bmd-label-floating input_label">Date Range - From</label>
                                            <input type="text" class="form-control form_input_field form_fields" id="insurance_report_from" name="insurance_report_from" value="{{date('d/m/Y', strtotime('-1 week'))}}" required>
                                          </div>
                                        </div>
                                        <div class="col-md-3">
                                          <div class="form-group bmd-form-group">
                                            <label for="insurance_report_to" class="bmd-label-floating input_label">Date Range - To</label>
                                            <input type="text" class="form-control form_input_field form_fields" id="insurance_report_to" name="insurance_report_to" value="{{date('d/m/Y')}}" required>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-3 ml-auto text-right">
                                          <button class="btn btn-info btn-round btn-sm print_insurace_btn" type="button">
                                            <span class="btn-label"><i class="material-icons">get_app</i></span>
                                            Download
                                            <div class="ripple-container"></div>
                                          </button>
                                        </div>
                                      </div>
                                      <div class="row excelDownloadMsg" style="display: none;">
                                        <div class="col-md-3 ml-auto">
                                          <b>Your download is starting...</b>
                                        </div>
                                      </div>
                                    </div>

                                  </form>
                                    
                                    <button class="btn btn-danger btn-sm btn-round insurance_report_cross">
                                      <i class="material-icons">close</i>
                                      <div class="ripple-container"></div>
                                    </button>
                                  </div>
                                </div>
                              </div>
                              <div class="card-body">
                                <div class="toolbar">
                                  <!--        Here you can write extra buttons/actions for the toolbar              -->
                                </div>
                                <div class="material-datatables insurance_request_table" style="overflow-x: unset;">
                                  <table id="insurace_report_table" class="table table-striped table-no-bordered table-hover insurance_table" cellspacing="0" width="100%" style="width:100%;">
                                    <thead>
                                      <tr>
                                        <th>Licence</th>
                                        <th>Request Id</th>
                                        <th>Name</th>
                                        <th>Request Type</th>
                                        <th>Action</th>
                                        <th>Liability</th>
                                        <th>Collision</th>                              
                                        <th>Sent</th>                                           
                                        <th style="min-width:42px;width:42px" class="text-center">Seen</th>                                                       
                                        <th class="disabled-sorting text-right" style="min-width:95px;">View</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i=1;?>
                                      @foreach($all_reqs as $rowdata)
                                      <?php 
                                      if($rowdata->getTable() == 'insurance_equipment_request'){
                                          $request_type = 'equipment';
                                          $request_id = 'EQ-'.$rowdata->id;
                                          $name = $rowdata->vin;
                                          $liability = $rowdata->liability;
                                          $liability = $rowdata->liability;
                                      }
                                      else if($rowdata->getTable() == 'insurance_vehicle_request'){
                                          $request_id = 'VH-'.$rowdata->id;
                                          $request_type = 'vehicle';
                                          $name = $rowdata->vin;
                                      }
                                      else if($rowdata->getTable() == 'insurance_driver_request'){
                                          $request_id = 'DR-'.$rowdata->id;
                                          $request_type = 'driver';
                                          $name = $rowdata->driver_first_name . ' ' . $rowdata->driver_last_name;
                                      }

                                      ?>
                                      <tr>
                                        <td>{{$rowdata->licence}}</td>
                                        <td>{{$request_id}}</td>
                                        <td>{{$name}}</td>
                                        <td>{{ucfirst($request_type)}}</td>
                                        <td>@if($rowdata->request == 'delete')
                                            <span class="ins_delete">{{strtoupper($rowdata->request)}}D</span>
                                            @elseif($rowdata->request == 'add')
                                            <span class="ins_add">{{strtoupper($rowdata->request)}}ED</span>
                                            @elseif($rowdata->request == 'amend')
                                            <span class="ins_amend"> {{strtoupper($rowdata->request)}}ED</span>
                                            @endif
                                          </td>
                                        <td class="status_td">
                                          @if($rowdata->liability == 'yes')
                                              <a href="" class="btn btn-success btn-link btn-just-icon done"><i class="material-icons">check_circle</i></a>
                                          @elseif($rowdata->liability == 'no')
                                              <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                          @else
                                              N/A
                                          @endif
                                        </td>
                                        <td class="status_td">
                                          @if($rowdata->damage == 'yes')
                                              <a href="" class="btn btn-success btn-link btn-just-icon done"><i class="material-icons">check_circle</i></a>
                                          @elseif($rowdata->damage == 'no')
                                              <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                          @else
                                              N/A
                                          @endif
                                        </td>
                                        <td>{{ \Carbon\Carbon::parse($rowdata->created_at)->timezone('America/New_York')->format('m/d/y g:i A') }}</td>
                                        <td class="status_td">
                                        @if($rowdata->status == 1)
                                          {{ \Carbon\Carbon::parse($rowdata->updated_at)->timezone('America/New_York')->format('m/d/y g:i A') }}
                                        @else
                                          <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                        @endif
                                        </td>
                                        <td class="text-right">
                                        
                                        <form action="{{ route('insurance.destroy',[$rowdata->id, 'type' => $request_type]) }}" method="POST">

                                          <a href="{{ route('insurance.show', [$rowdata->id, 'type' => $request_type]) }}" class="btn btn-link btn-info btn-just-icon view"><i class="material-icons">visibility</i></a>
                                          <!-- <a href="{{ route('insurance.edit', [$rowdata->id, 'type' => 'equipment']) }}" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a> -->


                                          @csrf
                                          @method('DELETE')

                                          <!-- <button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm('Are you sure you want to delete?');"><i class="material-icons">close</i></button> -->
                                        </form>

                                        </td>
                                      </tr>
                                      @endforeach

                      
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                            <div class="tab-pane" id="view_agreement">
                              <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                  <i class="material-icons">assignment</i>
                                </div>
                                <div class="insurance_btn_section_main">
                                  <div class="insurance_btn_section">
                                    <h4 class="card-title">Manage Agreement</h4>
                                    <button type="button" class="btn btn-sm btn-primary generate_agreement_btn">Generate Report</button>
                                  </div>
                                  <div class="card agreement_generate_section" style="display: none">
                                  
                                    <form method='post' action="">
                                      <div class="card-body">
                                        <div class="row">
                                          <div class="col-md-3 second input_wrapper custom_select2_col">
                                            <div class="form-group custom-form-select">
                                              <select class="custom-select hasvalue insurace_type_report select2 form_input_field" id="agreement_type_report" name="insurace_type_report" required="true" aria-required="true">
                                                <option value="all">All</option>
                                                <option value="subcontractor_agreement">Subcontractor Agreement</option>
                                                <option value="rental_agreement">Rental Agreement</option>
                                                <option value="owner_agreement">Owner Agreement</option>
                                              </select>
                                              <div class="form-element-bar">
                                              </div>
                                              <label class="form-element-label" for="variable_data_type input_label">Agreement Type</label>
                                            </div>
                                          </div>
                                          <div class="col-md-3 second input_wrapper custom_select2_col">
                                            <div class="form-group custom-form-select">
                                              <select class="custom-select hasvalue agreement_time_interval select2 form_input_field" id="agreement_time_interval" name="insurace_time_interval" required="true" aria-required="true">
                                                <option value="custom">Custom</option>
                                                <option value="daily">Daily</option>
                                                <option value="weekly">Weekly</option>
                                                <option value="monthly">Monthly</option>
                                              </select>
                                              <div class="form-element-bar">
                                              </div>
                                              <label class="form-element-label" for="time_interval input_label">Duration</label>
                                            </div>
                                          </div>
                                          <div class="col-md-3">
                                            <div class="form-group bmd-form-group">
                                              <label for="agreement_report_from" class="bmd-label-floating input_label">Date Range - From</label>
                                              <input type="text" class="form-control form_input_field form_fields datepicker" id="agreement_report_from" name="insurance_report_from" value="{{date('d/m/Y', strtotime('-1 week'))}}" required>
                                            </div>
                                          </div>
                                          <div class="col-md-3">
                                            <div class="form-group bmd-form-group">
                                              <label for="agreement_report_to" class="bmd-label-floating input_label">Date Range - To</label>
                                              <input type="text" class="form-control form_input_field form_fields datepicker" id="agreement_report_to" name="insurance_report_to" value="{{date('d/m/Y')}}" required>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-md-3 ml-auto text-right">
                                            <button class="btn btn-info btn-round btn-sm print_agreement_btn" type="button">
                                              <span class="btn-label"><i class="material-icons">get_app</i></span>
                                              Download
                                              <div class="ripple-container"></div>
                                            </button>
                                          </div>
                                        </div>
                                        <div class="row excelAgreementMsg" style="display: none;">
                                          <div class="col-md-3 ml-auto">
                                            <b>Your download is starting...</b>
                                          </div>
                                        </div>
                                      </div>

                                    </form>
                                    
                                    <button class="btn btn-danger btn-sm btn-round insurance_agreement_cross">
                                      <i class="material-icons">close</i>
                                      <div class="ripple-container"></div>
                                    </button>
                                  </div>
                                </div>
                              </div>
                              <div class="card-body">
                                <div class="material-datatables insurance_agreement_table_secton" style="overflow-x: unset;">
                                  <table id="insurace_agreement_table" class="table table-striped table-no-bordered table-hover agreement_table all_table" cellspacing="0" width="100%" style="width:100%;">
                                    <thead>
                                      <tr>
                                        <th>Request Id</th>
                                        <th>Name</th>
                                        <th>Request Type</th>
                                        <th>Action</th>
                                        <th>Uploaded Documents</th>
                                        <th>Sent</th>                                           
                                        <th>Seen</th><th class="disabled-sorting text-right" style="min-width:60px;">View</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i=1;?>
                                      @foreach($all_agreement as $rowdata)
                                      <?php 
                                      if($rowdata->getTable() == 'subcontractor_agreement'){
                                          $request_type = 'Subcontractor Agreement';
                                          $request_id = 'SC-'.$rowdata->id;
                                          $name = $rowdata->subcontractor_name;
                                          
                                          $request_type_show = 'subcontractor_agreement';

                                          if(isset($rowdata->getW9))
                                            if(count($rowdata->getW9))
                                              $w9 = '<span class="upload_doc w9">W9</span>';
                                          else
                                              $w9 = '';

                                           if(isset($rowdata->getInsuranceCertificate))
                                              if(count($rowdata->getInsuranceCertificate))
                                                $ins_cert = '<span class="upload_doc ins_cer">Ins. Cert.</span>';
                                            else
                                              $ins_cert = '';

                                          $uploaded_doc = $w9 . $ins_cert;
                                            
                                      }
                                      else if($rowdata->getTable() == 'rental_agreement'){
                                          $request_id = 'RN-'.$rowdata->id;
                                          $request_type = 'Rental Agreement';
                                          $name = $rowdata->ren_ag_executed_lessee_name;

                                          $request_type_show = 'rental_agreement';

                                          if(isset($rowdata->getW9))
                                            if(count($rowdata->getW9))
                                              $w9 = '<span class="upload_doc w9">W9</span>';
                                            else
                                              $w9 = '';

                                           if(isset($rowdata->getInsuranceCertificate))
                                              if(count($rowdata->getInsuranceCertificate))
                                                $ins_cert = '<span class="upload_doc ins_cer">Ins. Cert.</span>';
                                           else
                                              $ins_cert = '';

                                          $uploaded_doc = $w9 . $ins_cert;
                                      }
                                      else if($rowdata->getTable() == 'owner_agreement'){
                                          $request_id = 'OA-'.$rowdata->id;
                                          $request_type = 'Owner Agreement';
                                          $name = $rowdata->owner_name;

                                          $request_type_show = 'owner_agreement';

                                          if(isset($rowdata->getW9))
                                            if(count($rowdata->getW9))
                                              $w9 = '<span class="upload_doc w9">W9</span>';
                                          else
                                            $w9 = '';

                                           if(isset($rowdata->owner_operator_comprehensive))
                                              if(count($rowdata->owner_operator_comprehensive))
                                                $owner_operator_comprehensive = '<span class="upload_doc com_bob">Comp./Bobtail Ins.</span>';
                                           else
                                              $owner_operator_comprehensive = '';

                                           if(isset($rowdata->owner_vehicle_reg))
                                              if(count($rowdata->owner_vehicle_reg))
                                                $owner_vehicle_reg = '<span class="upload_doc veh_reg">Veh. Reg.</span>';
                                           else
                                            $owner_vehicle_reg = '';
                                           
                                           if(isset($rowdata->owner_annual_insp_cer))
                                              if(count($rowdata->owner_annual_insp_cer))
                                                $owner_annual_insp_cer = '<span class="upload_doc a_ins_cer">Ann. Insp. Cert.</span>';
                                           else
                                              $owner_annual_insp_cer = '';

                                           if(isset($rowdata->owner_vehicle_use_form))
                                              if(count($rowdata->owner_vehicle_use_form))
                                                 $owner_vehicle_use_form = '<span class="upload_doc h_veh_use">Heavy Veh. Use</span>';
                                           else
                                            $owner_vehicle_use_form = '';

                                          $uploaded_doc = $w9 . $owner_operator_comprehensive . $owner_vehicle_reg . $owner_annual_insp_cer . $owner_vehicle_use_form;
                                      }
                                      $sent_json = json_decode($rowdata->sent);
                                      $sent = $sent_json ? end($sent_json) : null;
                                      $seen_json = json_decode($rowdata->seen);
                                      $seen = $seen_json ? end($seen_json) : null;
                                      ?>
                                      <tr>
                                        <td>{{$request_id}}</td>
                                        <td>{{$name}}</td>
                                        <td>{{ucfirst($request_type)}}</td>

                                        <td>@if($rowdata->status == 1)
                                            <span class="ins_amend">Sent to 3rd Party</span>
                                            @elseif($rowdata->status == 2)
                                            <span class="ins_amend">Pending for Review</span>
                                            @elseif($rowdata->status == 3)
                                            <span class="ins_add"> Submitted</span>
                                            @elseif($rowdata->status == 4)
                                            <span class="ins_delete"> Rejected</span>
                                            @endif
                                        </td>
                                        <!-- <td>
                                          <div class="uploaded_doc_section">

                                            @if(isset($rowdata->getW9))
                                                @if(count($rowdata->getW9))
                                                <span class="upload_doc w9">W9</span>
                                                @endif
                                            @endif

                                            @if(isset($rowdata->getInsuranceCertificate))
                                                @if(count($rowdata->getInsuranceCertificate))
                                                    <span class="upload_doc ins_cer">Ins. Cert.</span>
                                                @endif
                                            @endif
                                            
                                            @if(isset($rowdata->owner_operator_comprehensive))
                                                @if(count($rowdata->owner_operator_comprehensive))
                                                <span class="upload_doc com_bob">Comp./Bobtail Ins.</span>
                                            @endif 
                                            @endif
                                            
                                            @if(isset($rowdata->owner_vehicle_reg))
                                                @if(count($rowdata->owner_vehicle_reg))
                                                <span class="upload_doc veh_reg">Veh. Reg.</span>
                                                @endif
                                            @endif

                                            @if(isset($rowdata->owner_annual_insp_cer))
                                                @if(count($rowdata->owner_annual_insp_cer))
                                            <span class="upload_doc a_ins_cer">Ann. Insp. Cert.</span>
                                                @endif
                                            @endif

                                            @if(isset($rowdata->owner_vehicle_use_form))
                                                @if(count($rowdata->owner_vehicle_use_form))
                                                    <span class="upload_doc h_veh_use">Heavy Veh. Use</span>
                                                @endif
                                            @endif

                                            @if(!isset($rowdata->getW9) && !isset($rowdata->getInsuranceCertificate) && !isset($rowdata->owner_operator_comprehensive) && !isset($rowdata->owner_vehicle_reg) && !isset($rowdata->owner_annual_insp_cer) && !isset($rowdata->owner_vehicle_use_form))
                                              <span>no files Uploaded</span>
                                            @endif

                                            
                                          </div>
                                        </td> -->
                                        <td>
                                          <div class="uploaded_doc_section">
                                            @if($uploaded_doc != '')
                                              {!! $uploaded_doc !!}
                                            @else
                                          <div class="tooltip_td"> 
                                            <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                          </div>
                                            @endif
                                          </div>
                                        </td>
                                        <td>
                                          <div class="tooltip_td_another"> 
                                          @if($rowdata->sent)
                                            <span>{{ \Carbon\Carbon::parse($sent)->timezone('America/New_York')->format('m/d/y g:i A') }}</span>
                                          
                                            @if(count($sent_json) > 1)
                                            <div class="container_tooltip">
                                              <div class="custom_tooltip">
                                                <div class="tooltip_content">
                                                  @foreach($sent_json as $row)
                                                  <span>{{ \Carbon\Carbon::parse($row)->timezone('America/New_York')->format('m/d/y g:i A') }} </span>
                                                  @endforeach
                                                </div>
                                                <div class="triangle"></div>
                                              </div>
                                              <a href="#" class="tooltip_link"><i class="material-icons">info</i></a>
                                            </div>
                                            @endif

                                          @else
                                          <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                        @endif
                                        </div>
                                        </td>
                                        <td>
                                          <div class="tooltip_td"> 
                                        @if($rowdata->seen)
                                          <span>{{ \Carbon\Carbon::parse($seen)->timezone('America/New_York')->format('m/d/y g:i A') }}</span>

                                          @if(count($seen_json) > 1)
                                          <div class="container_tooltip">
                                            <div class="custom_tooltip">
                                              <div class="tooltip_content">
                                                @foreach($seen_json as $row)
                                                <span>{{ \Carbon\Carbon::parse($row)->timezone('America/New_York')->format('m/d/y g:i A') }} </span>
                                                @endforeach
                                              </div>
                                              <div class="triangle"></div>
                                            </div>
                                            <a href="#" class="tooltip_link"><i class="material-icons">info</i></a>
                                          </div>
                                          @endif
                                                                                    
                                        @else
                                          <a href="" class="btn btn-danger btn-link btn-just-icon remove"><i class="material-icons">cancel</i></a>
                                        @endif
                                          </div>
                                        </td>
                                        <td class="text-right">
                                        
                                        <form action="{{ route('insurance.destroy',[$rowdata->id, 'type' => $request_type]) }}" method="POST">

                                          <a href="{{ route('insurance.show', [$rowdata->id, 'type' => $request_type_show]) }}" class="btn btn-link btn-info btn-just-icon view"><i class="material-icons">visibility</i></a>
                                          <!-- <a href="{{ route('insurance.edit', [$rowdata->id, 'type' => 'equipment']) }}" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a> -->


                                          @csrf
                                          @method('DELETE')

                                          <!-- <button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm('Are you sure you want to delete?');"><i class="material-icons">close</i></button> -->
                                        </form>

                                        </td>
                                      </tr>
                                      @endforeach

                      
                                    </tbody>
                                  </table>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

<script>
$(document).ready(function () {

$('.3rd_party_field').find('input, textarea').attr('readonly', true);
$('.3rd_party_field').find('select').attr('disabled', true);


var hash = document.location.hash;
if(hash){
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    history.pushState({}, '', e.target.hash);
  });

  
  var prefix = "tab_";
  if (hash) {
    $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
}
}
else{
if($('#tabMenu a[href="#{{ old('tab') }}"]').attr('href') == undefined){
    $('#tabMenu a[href="#equipment_tab"]').tab('show')  
}
else{
    $('#tabMenu a[href="#{{ old('tab') }}"]').tab('show')
}
}

var d = new Date();
var strDate = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();

$('#equip_refresh_btn').click(function(){
    $(this).parents('.form_card').find('input, textarea').not("input[name=request]").not('.hiddenInsurance').val('');
    $(this).parents('.form_card').find("input[name=company_name]").val('EV Oilfield Services');
    $(this).parents('.form_card').find("input[name=submitted_by]").val($('.submitted_by_hidden').val());
    $(this).parents('.form_card').find("input[name=send_mail_to]").val('elizabeth@mcanallywilkins.com');
    $(this).parents('.form_card').find("input[name=date]").val(strDate);
    $(this).parents('.form_card').find("input[name=effective_date]").val(strDate);

    $('#equip_delete_chk').attr('disabled', 'disabled').prop('checked', false);
    $('#equip_amend_chk').attr('disabled', 'disabled').prop('checked', false);
    $('#equip_add_chk').attr('disabled', false).prop('checked', false);

    $('#equipBtn').parents('.form_card').find('.add_amend_del_warning p').html('');

    $('#equip_message').hide();
    $(this).fadeOut();
});

$('#vehicle_refresh_btn').click(function(){
    $(this).parents('.form_card').find('input, textarea').not("input[name=request], input[name=liability], input[name=damage]").not('.hiddenInsurance').val('');
    $(this).parents('.form_card').find("input[name=company_name]").val('EV Oilfield Services');
    $(this).parents('.form_card').find("input[name=submitted_by]").val($('.submitted_by_hidden').val());
    $(this).parents('.form_card').find("input[name=send_mail_to]").val('elizabeth@mcanallywilkins.com');
    $(this).parents('.form_card').find("input[name=date]").val(strDate);
    $(this).parents('.form_card').find("input[name=effective_date]").val(strDate);
    $(this).parents('.form_card').find("input[name=liability]").prop('checked', false);
    $(this).parents('.form_card').find("input[name=damage]").prop('checked', false);


    $('#vehicle_delete_chk').attr('disabled', 'disabled').prop('checked', false);
    $('#vehicle_amend_chk').attr('disabled', 'disabled').prop('checked', false);
    $('#vehicle_add_chk').attr('disabled', false).prop('checked', false);

    $('#vehicleBtn').parents('.form_card').find('.add_amend_del_warning p').html('');

    $('#vehicle_message').hide();
    $(this).fadeOut();
});

$('#driver_refresh_btn').click(function(){
    $(this).parents('.form_card').find('input, textarea').not("input[name=request]").not('.hiddenInsurance').val('');
    $(this).parents('.form_card').find("input[name=company_name]").val('EV Oilfield Services');
    $(this).parents('.form_card').find("input[name=submitted_by]").val($('.submitted_by_hidden').val());
    $(this).parents('.form_card').find("input[name=send_mail_to]").val('elizabeth@mcanallywilkins.com');
    $(this).parents('.form_card').find("input[name=date]").val(strDate);

    $('#driver_delete_chk').attr('disabled', 'disabled').prop('checked', false);
    $('#driver_amend_chk').attr('disabled', 'disabled').prop('checked', false);
    $('#driver_add_chk').attr('disabled', false).prop('checked', false);

    $('.uploaded').empty();
    //$('.upload-text').css('display', 'flex');
    $('.input-files.driver_files .image-uploader.has-files').removeClass('has-files')
    
    $('#driverBtn').parents('.form_card').find('.add_amend_del_warning p').html('');

    $('#driver_message').hide();
    $(this).fadeOut();
});


$('.insurance_table').DataTable({
        "columnDefs": [
            {
                "targets": [0],
                "visible": false,
            },

        ],

        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "All"]
        ],
        "ordering": false,
        "responsive": false,
        "bAutoWidth": false,
        "language": {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

  var table = $('.insurance_table').DataTable();


});




$('.searchTerm').keyup(function(e){
    if(e.which != 38 && e.which!=40){
        var query = $(this).val();

        if(query != '')
        {
         let _token = $('input[name="_token"]').val();
         let type = $(this).parents('.searchWrapper').find('.hiddenInsurance').val();
         
         $.ajax({
          url:"{{ route('autocomplete') }}",
          context:this,
          method:"POST",
          data:{query:query, _token:_token, type:type},
          success:function(data){
           $(this).parents('.searchWrapper').find('.suggestionList').fadeIn();  
           $(this).parents('.searchWrapper').find('.suggestionList').html(data);

           $($(this).parents('.searchWrapper').find('.suggestionList ul.results li')[0]).children().addClass('suggestion_selected')

          }
         });
        }
        else{
            $(this).parents('.searchWrapper').find('.suggestionList').fadeOut();  
        }
      }
      // else if(e.which == 13){
      //   alert("Boolla")
      // }
});

$(document).on('click','.suggestion a', function(){  
    $(this).parents('.searchWrapper').find('.searchTerm').val($(this).text());  
    $(this).parents('.searchWrapper').find('.suggestionList').fadeOut();  
    findInsurance($(this));
});  

$('.searchButton').on('click', function(){
  findInsurance($(this));
});
$('.searchTerm').on('keypress', function(e) {
    if (e.keyCode === 13) {
        $(this).val($(this).parents('.searchWrapper').find('.suggestionList .suggestion a.suggestion_selected').text());  
        $(this).parents('.searchWrapper').find('.suggestionList').fadeOut();
        findInsurance($(this));
        e.stopPropagation()

    }
});




var preloaded = [];
function findInsurance(el){
    let type = el.parents('.searchWrapper').find('.hiddenInsurance').val();
    let value = $(el).parents('.searchWrapper').find('.searchTerm').val();
    let _token = $('input[name="_token"]').val();
    $.ajax({
        url:"{{ route('findInsurance') }}",
        context:el,
        method:"POST",
        data:{type:type, value:value, _token:_token},
        success:function(result)
        {
          if(type === 'equipment'){
            if(result != ''){
              result = JSON.parse(result);
              ///alert('okkk');
              $('#equip_message').show().html("found").css("color", "green");

              if(result.request == 'add'){
                $('#equip_add_chk').attr('disabled', 'disabled').prop('checked', false);
                $('#equip_delete_chk').attr('disabled', false).prop('checked', false);
                $('#equip_amend_chk').attr('disabled', false).prop('checked', false);

                $('#equipBtn').parents('.form_card').find('.add_amend_del_warning p').html('Your last request was add. Now, you should be able to delete or amend');

              }
              else if(result.request == 'delete'){
                $('#equip_delete_chk').attr('disabled', 'disabled').prop('checked', false);
                $('#equip_amend_chk').attr('disabled', 'disabled').prop('checked', false);
                $('#equip_add_chk').attr('disabled', false).prop('checked', false);
                
                $('#equipBtn').parents('.form_card').find('.add_amend_del_warning p').html('Your last request was delete. Now, you should be able to add only');
              }
              else if(result.request == 'amend'){
                $('#equip_delete_chk').attr('disabled', false).prop('checked', false);
                $('#equip_amend_chk').attr('disabled', false).prop('checked', false);
                $('#equip_add_chk').attr('disabled', 'disabled').prop('checked', false);

                $('#equipBtn').parents('.form_card').find('.add_amend_del_warning p').html('Your last request was amend. Now, you should be able to delete or amend');
              }

              $.each(result, function(key,valueObj){
                  $('#equipBtn').parents('.form_card').find("input[name='"+key+"']").not("input[name='request']").val(valueObj).parent().addClass('is-filled');
                  if(key == 'description_equipment_accessories'){
                    $("#desc_equip_accessories").val(valueObj).parent().addClass('is-filled');
                  }
              });
            }
            else{
              $('#equip_message').show().html("Not found").css("color", "red");
              /*$('#equipBtn').parents('.form_card').find("input, textarea").not("input[name=company_name], input[name=date]").val('');
              $('#equip_delete_chk').attr('disabled', 'disabled');
              $('#equip_amend_chk').attr('disabled', 'disabled');
              $('#equip_add_chk').attr('disabled', false);*/
            }
          }
          else if(type === 'vehicle'){
            if(result !== ''){
          
            result = JSON.parse(result);

            $('#vehicle_message').show().html("Found").css("color", "green");

            if(result.request == 'add'){
              $('#vehicle_add_chk').attr('disabled', 'disabled').prop('checked', false);
              $('#vehicle_delete_chk').attr('disabled', false).prop('checked', false);
              $('#vehicle_amend_chk').attr('disabled', false).prop('checked', false);

              $('#vehicleBtn').parents('.form_card').find('.add_amend_del_warning p').html('Your last request was add. Now, you should be able to delete or amend');
            }
            if(result.request == 'delete'){
              $('#vehicle_delete_chk').attr('disabled', 'disabled').prop('checked', false);
              $('#vehicle_amend_chk').attr('disabled', 'disabled').prop('checked', false);
              $('#vehicle_add_chk').attr('disabled', false).prop('checked', false);
              
              $('#vehicleBtn').parents('.form_card').find('.add_amend_del_warning p').html('Your last request was delete. Now, you should be able to add only');
            }
            if(result.request == 'amend'){
              $('#vehicle_delete_chk').attr('disabled', false).prop('checked', false);
              $('#vehicle_amend_chk').attr('disabled', false).prop('checked', false);
              $('#vehicle_add_chk').attr('disabled', 'disabled').prop('checked', false);

              $('#vehicleBtn').parents('.form_card').find('.add_amend_del_warning p').html('Your last request was amend. Now, you should be able to delete or amend');
            }

              $.each(result, function(key,valueObj){
                console.log(key, valueObj);

                  $('#vehicleBtn').parents('.form_card').find("input[name='"+key+"']").not("input[name='request'], input[name='liability'], input[name='damage']").val(valueObj).parent().addClass('is-filled');
                  if(key == 'liability'){
                    //lert(valueObj);
                    $('input[value='+valueObj+'].vehicle_liability').prop("checked", true);
                  }
                  if(key == 'damage'){
                      $('input[value='+valueObj+'].vehicle_physical_dam').prop("checked", true); 
                  }
                  if(key == 'special_equipment'){
                    $("#spcl_eq").val(valueObj).parent().addClass('is-filled');
                  }

              });
            }
            else{
              $('#vehicle_message').show().html("Not found").css("color", "red");
            }
          }
          else if(type === 'driver'){
            var obj = JSON.parse(result);
            var result = obj[0].result;
            if(result !== null){
              $('#driver_message').show().html("Found").css("color", "green");
              if(result.request == 'add'){
                $('#driver_add_chk').attr('disabled', 'disabled').prop('checked', false);
                $('#driver_delete_chk').attr('disabled', false).prop('checked', false);
                $('#driver_amend_chk').attr('disabled', false).prop('checked', false);

                $('#driverBtn').parents('.form_card').find('.add_amend_del_warning p').html('Your last request was add. Now, you should be able to delete or amend');
              }
              if(result.request == 'delete'){
                $('#driver_delete_chk').attr('disabled', 'disabled').prop('checked', false);
                $('#driver_amend_chk').attr('disabled', 'disabled').prop('checked', false);
                $('#driver_add_chk').attr('disabled', false).prop('checked', false);
                
                $('#driverBtn').parents('.form_card').find('.add_amend_del_warning p').html('Your last request was delete. Now, you should be able to add only');
              }
              if(result.request == 'amend'){
                $('#driver_delete_chk').attr('disabled', false).prop('checked', false);
                $('#driver_amend_chk').attr('disabled', false).prop('checked', false);
                $('#driver_add_chk').attr('disabled', 'disabled').prop('checked', false);
                
                $('#driverBtn').parents('.form_card').find('.add_amend_del_warning p').html('Your last request was amend. Now, you should be able to delete or amend');
              }

                  $.each(result, function(key,valueObj){
                      $('#driverBtn').parents('.form_card').find("input[name='"+key+"']").not("input[name='request']").val(valueObj).parent().addClass('is-filled');
                  });

                  var files = obj[0].files;
                  // $('.uploaded').html('');
                  $('.input-files.driver_files').empty();
                  $('.uploaded').empty();
                   $.each(files, function(key,valueObj){
                      var data_files = { id: valueObj.id, src: valueObj.name };
                      console.log(data_files)
                      preloaded.push(data_files);
                    });
                   preloaded_file_caller(preloaded);
                   if($('.image-uploader').length > 1){
                     $($('.image-uploader')[0]).hide();
                   }
                   
                }

                else{
                  $('#driver_message').show().html("Not found").css("color", "red");
                }
              }

            }
    })
 preloaded = [];
}


//  File Upload JS

var preloaded_file_caller = function(param){

    $('.input-files').fileUploader({
      preloaded: param,
      extensions: ['.pdf'],
      mimes: ['application/pdf'],
    //imagesInputName:'images'
    });


}





$('.insurance_check').change(function() {

  $(".insurance_check").prop('checked',false);
  $(this).prop('checked',true);

  if($(this).attr("value") == 'delete') {
    $(".form_card input, select, textarea").not("input[name=send_mail_to], input[name=date]").attr('readonly',true);
    $(":checkbox").not("input[name=request]").bind("click", false);
    $('.input-files.driver_files').css('pointer-events','none')

  }
  else{
   $(".form_card input, select, textarea").removeAttr('readonly');
    $(":checkbox").unbind("click");
    $('.input-files.driver_files').css('pointer-events','auto')
  }      

});



$(document).on('input','.searchTerm',function(){
  $(this).siblings('.search_reset_button').fadeIn();
  if($(this).val().length === 0){
    $(this).siblings('.search_reset_button').fadeOut();
  }
})

$(document).on('click', '.search_reset_button',function(){
  preloaded = [];
  //console.log(preloaded)
})






//  File Upload JS
$(function(){
  $('.input-files').fileUploader({
    preloaded: preloaded,
    extensions: ['.pdf'],
    mimes: ['application/pdf'],
    //imagesInputName:'images'
    required: true,
  });


})


$('input[type="file"].imagesInput').each(function(){
    $(this).rules('add', {
        messages: {
            required: "this FILE upload field is required"
        }
    })
});



$(window).on('load', function(){
  setTimeout(function() {
     $('.material-datatables').css('overflow-x','unset');
  }, 100);
});

var date = new Date();
var only_year = date.getFullYear();
$('.year_to_pick').yearpicker({
  endYear:only_year,
  year:null
});



$(function () {
    $('#insurance_report_from').datetimepicker({
      viewMode: 'days',
      format: 'DD/MM/YYYY',
      maxDate: moment(),
      useCurrent: false,
      icons: {
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right'
      }
    })

    $('#insurance_report_to').datetimepicker({
      viewMode: 'days',
      format: 'DD/MM/YYYY',
      maxDate: moment(),
      useCurrent: false, //Important! See issue #1075
      icons: {
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right'
      }
    })


    $("#insurance_report_from").on("dp.change", function (e) {
        $('#insurance_report_to').data("DateTimePicker").minDate(e.date);
    });
    $("#insurance_report_to").on("dp.change", function (e) {
        $('#insurance_report_from').data("DateTimePicker").maxDate(e.date);
    });
});

$('#insurance_report_from').on('blur',function(){
  if($(this)[0].value){
   $(this).parents('.form-group').addClass('is-filled');
  }else{
    $(this).parents('.form-group').removeClass('is-filled');
  }
})
$('#insurance_report_to').on('blur',function(){
  if($(this)[0].value){
   $(this).parents('.form-group').addClass('is-filled');
  }else{
    $(this).parents('.form-group').removeClass('is-filled');
  }
})



$(function () {
    $('#agreement_report_from').datetimepicker({
      viewMode: 'days',
      format: 'DD/MM/YYYY',
      maxDate: moment(),
      useCurrent: false,
      icons: {
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right'
      }
    })

    $('#agreement_report_to').datetimepicker({
      viewMode: 'days',
      format: 'DD/MM/YYYY',
      maxDate: moment(),
      useCurrent: false, //Important! See issue #1075
      icons: {
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right'
      }
    })


    $("#agreement_report_from").on("dp.change", function (e) {
        $('#agreement_report_to').data("DateTimePicker").minDate(e.date);
    });
    $("#agreement_report_to").on("dp.change", function (e) {
        $('#agreement_report_from').data("DateTimePicker").maxDate(e.date);
    });
});


//  All Agreement File Uploader

$(function(){
  $('.agreement_w9').fileUploader({
    imagesInputName: 'rental_agreement_w9',
    extensions: ['.pdf','.doc','.docx'],
    mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document']
  });
})

$(function(){
  $('.agreement_w9_2').fileUploader({
    imagesInputName: 'subcontractor_agreement_w9',
    extensions: ['.pdf','.doc','.docx'],
    mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document']
  });
})

$(function(){
  $('.insurance_certificate').fileUploader({
    imagesInputName: 'rental_insurance_certificate',
    extensions: ['.pdf','.doc','.docx'],
    mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document']    
  });
})
$(function(){
  $('.insurance_certificate_2').fileUploader({
    imagesInputName: 'subcontractor_insurance_certificate',
    extensions: ['.pdf','.doc','.docx'],
    mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document']
  });
})

$('.agreement_upload').css('pointer-events','none');

var subContractorYear = date.getFullYear();
$('#sub_agree_year').yearpicker({
  endYear:only_year,
  year:null,
  template: `<div class="yearpicker-container">
              <div class="yearpicker-header">
                  <div class="yearpicker-prev" data-view="yearpicker-prev">&lsaquo;</div>
                  <div class="yearpicker-current" data-view="yearpicker-current">SelectedYear</div>
                  <div class="yearpicker-next" data-view="yearpicker-next">&rsaquo;</div>
              </div>
              <div class="yearpicker-body">
                  <ul class="yearpicker-year sub_contractor_year" data-view="years">
                  </ul>
              </div>
          </div>`
});

$('#sub_agree_year_2').yearpicker({
  endYear:only_year,
  year:null,
  template: `<div class="yearpicker-container">
              <div class="yearpicker-header">
                  <div class="yearpicker-prev" data-view="yearpicker-prev">&lsaquo;</div>
                  <div class="yearpicker-current" data-view="yearpicker-current">SelectedYear</div>
                  <div class="yearpicker-next" data-view="yearpicker-next">&rsaquo;</div>
              </div>
              <div class="yearpicker-body">
                  <ul class="yearpicker-year sub_contractor_year2" data-view="years">
                  </ul>
              </div>
          </div>`
});

$('#owner_agreement_year').yearpicker({
  endYear:only_year,
  year:null,
  template: `<div class="yearpicker-container">
              <div class="yearpicker-header">
                  <div class="yearpicker-prev" data-view="yearpicker-prev">&lsaquo;</div>
                  <div class="yearpicker-current" data-view="yearpicker-current">SelectedYear</div>
                  <div class="yearpicker-next" data-view="yearpicker-next">&rsaquo;</div>
              </div>
              <div class="yearpicker-body">
                  <ul class="yearpicker-year owner_agreement_year" data-view="years">
                  </ul>
              </div>
          </div>`
});

$('#owner_agreement_year2').yearpicker({
  endYear:only_year,
  year:null,
  template: `<div class="yearpicker-container">
              <div class="yearpicker-header">
                  <div class="yearpicker-prev" data-view="yearpicker-prev">&lsaquo;</div>
                  <div class="yearpicker-current" data-view="yearpicker-current">SelectedYear</div>
                  <div class="yearpicker-next" data-view="yearpicker-next">&rsaquo;</div>
              </div>
              <div class="yearpicker-body">
                  <ul class="yearpicker-year owner_agreement_year2" data-view="years">
                  </ul>
              </div>
          </div>`
});


//  Owner File upload

$(function(){
  $('.owner_w9').fileUploader({
    imagesInputName: 'owner_w9',
    extensions: ['.pdf','.doc','.docx'],
    mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document']
  });
}) 

$(function(){
  $('.owner_operator_comprehensive').fileUploader({
    imagesInputName: 'owner_operator_comprehensive',
    extensions: ['.pdf','.doc','.docx'],
    mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document']
  });
})

$(function(){
  $('.owner_vehicle_reg').fileUploader({
    imagesInputName: 'owner_vehicle_reg',
    extensions: ['.pdf','.doc','.docx'],
    mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document']
  });
})

$(function(){
  $('.owner_annual_insp_cer').fileUploader({
    imagesInputName: 'owner_annual_insp_cer',
    extensions: ['.pdf','.doc','.docx'],
    mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document']
  });
})

$(function(){
  $('.owner_vehicle_use_form').fileUploader({
    imagesInputName: '',
    extensions: ['.pdf','.doc','.docx'],
    mimes: ['application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document']
  });
})  

var currency_value_validator = function(input_field,key){

    var s = String.fromCharCode(key.which);
    if(s >=0 || s<=9 ){
      // input_field.attr('maxlength','14');
      // input_field.prop('type','text');
    }
    else{
      key.preventDefault();
      alert("Please Type A Number")
    }
}


// $('.insurance_limit_currency input').on('keypress',function(e){
//   console.log("boom")
//   currency_value_validator($(this),e);
// })


// Jquery Dependency

$(".insurance_limit_currency input").on({
    keyup: function(e) {
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});

$(".currency_input").on({
    keyup: function(e) {
      console.log("currency typing")
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});



$('.rent_time select').on('change',function(){
  if($(this).val()!== ""){
    $(this).siblings('label').text("")
  }
})


// $('.agreement_info_tooltip').tooltip();   
</script>

@endsection
