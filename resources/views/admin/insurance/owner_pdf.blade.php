<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Owner Operator Agreement</title>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
body{
	font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
}
.request_id_badge{
	background: #f6cb61;
	width:140px;
	height: 30px;
	line-height: 1.3;
	border-radius: 0;
	text-align: center;
	color:white;
	border-radius: 50%;
	position: fixed;
	right: -30px;
	top: -30px;
	border: 1px solid #000;
	border-radius: 15px;
}
.request_id_badge h4{
	padding: 6px 5px;
	font-size:12px;
	font-weight: 300;
	margin: 0;
	text-transform: uppercase;
	color: #000;
}
.insurance_title_secton{
	text-align: center;
}

footer {
    position: absolute; 
    bottom: -30px; 
    left: 0; 
    right: 0;
    color: white;
    height: 45px;
    text-align: center;
} 
.footer-section-table{
	display: table;
	width: 100%;
	vertical-align: middle;
	table-layout: fixed;
}
.footer-section-row{
	display: table-row;
}
.footer-part{
	display: table-cell;
	height: 45px;
	vertical-align: middle;
}
.footer-part img{
	height: 30px;
	display: block;
	text-align: left;
}
.footer-part p{
	margin: 0;
	color: #676767;
	font-size: 12px;
	line-height: 1.3;
}
.insurance_logo img{
	position: fixed;
	top: -40px;
	left: 40%;
	height: 80px;
	transform: translate(-40%, 0);
}
.insurance_title_secton{
	margin-top: 50px;
}
.main_header h2{
	text-align: center;
	margin-top: 50px;
}
.inline-input-field{
	padding: 0 5px;
	display: inline-block;
}
.day{
	width: auto;
    display: inline-block;
}
.month,
.year{
	width: auto;
    display: inline-block;
}
p{
	font-size: 14px;
	line-height: 1.6;
}
.custom-fieldset .info-form-section .row-input {
	margin: 0;
	position: relative;
}
.custom-fieldset .info-form-section .row-input input{
	margin-top:5px; 
	width:100%;
	outline: none;
	border:none; 
	border-bottom:1px solid black;
	font-size:13px;
	height: 18px;
	margin-bottom: 5px;
	display: inline-block;
}
input{
	outline: none;
	border:none; 
	border-bottom:1px solid black;
	font-size:13px;
	height: 18px;
}
.subcontractor_table{
	width: 100%;
}
.subcontractor_table tr td{
	width: 50%;
}
.custom-fieldset{
	margin: 15px 0 10px 0;
	border-radius: 4px;
}
.custom-fieldset table{
	margin-top: 5px;
	padding: 2px;
}
.custom-fieldset legend{
	font-weight: bold;
}
.subcontractor_agre_list{
  padding-left: 30px;
}
.subcontractor_agre_list li{
    font-size: 14px;
    line-height: 1.5;
    margin-bottom: 10px;
    font-weight: bold;
}
.row-input > p{
	margin-bottom: 0;
	position: absolute;
	top: -13px;
	font-size: 12.5px;
	letter-spacing: 0.4px;
	left: 2px;
	font-weight: bold;
}
.insurance_input_field{
	max-width: 150px;
}
.sub-title{
	display: inline-block;
	text-transform: capitalize;
	text-decoration: underline;
	margin-bottom: 0px;
}
.inline-input-field{
	border: none;
	outline: none;
	border-bottom: 1px solid black;
}
.registration_list li{
	margin-bottom: 15px;
}
.registration_list li span:first-child{
	margin-left: 5px;
}
.registration_list li input{
	width: 80px;
	border: 0;
	border-bottom: 1px solid #000;
}
.info-form-section a{
	word-wrap: break-word;
	font-size: 14px;
	margin-top: -10px;
	display: inline-block;
}
.registration_list li input{
	display: inline-block;
	padding-bottom: 0px;
}
/*.file_upload_header{
	padding-bottom: 100px;
}
*/
</style>
<body>
	<header>
		<div class="request_id_badge">
			<h4 class="title">OA-{{$data->id}}</h4>
		</div>
	    <div class="insurance_logo">
			<img src="{{ public_path() . '/assets/img/logo_ins.png' }}">
		</div>
	</header>
  
	<div class="main">
		<div class="main_header">
			<h2>OWNER OPERATOR AGREEMENT</h2>
		</div>

		<div class="row">
			<div class="column">
				<p>This Owner Operator Agreement (“Agreement”) is entered into and shall be effective as of the <input type="text" name="" class="inline-input-field day" value="{{$data->owner_day}}"> day of <br> <input type="text" name="" class="inline-input-field month" value="{{$data->owner_month}}"> <input type="text" name="" class="inline-input-field year" value="{{$data->owner_year}}">(the “Effective Date”), by and among EV Oilfield Services, LLC (“Contractor”), and</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<fieldset class="custom-fieldset">
					<legend>Contract Form</legend>
					<table class="subcontractor_table">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Owner Operator</p>
										<span><input type="text" value="{{$data->owner_name}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Type of Entity</p>
										<span><input type="text" value="@if($data->owner_type_of_entity=='limited_partnership') Limited Partnership @elseif($data->owner_type_of_entity=='sole_proprietorship') Sole Proprietorship @else {{ucfirst($data->owner_type_of_entity)}} @endif" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Mailing Address</p>
										<span><input type="text" value="{{$data->owner_mailing_addr}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Physical Address</p>
										<span><input type="text" value="{{$data->owner_physical_addr}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Contact Person</p>
										<span><input type="text" value="{{$data->owner_contact_person}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Title</p>
										<span><input type="text" value="{{$data->owner_contact_person_title}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Email</p>
										<span><input type="text" value="{{$data->owner_contact_person_email}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Phone</p>
										<span><input type="text" value="{{$data->owner_contact_person_phone}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<!-- <tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Fax</p>
										<span><input type="text" value="{{$data->owner_contact_person_fax}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr> -->
					</table>
				</fieldset>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<p>Contractor and Owner Operator may be individually referred to herein as a “Party” or collectively as the “Parties.”</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">CONSPICUOUS AND FAIR NOTICE</h3>
				<p>Both parties represent to each other that --</p>
				<ul class="subcontractor_agre_list">
					<li>They have consulted an attorney concerning this agreement or, if they have not consulted an attorney, they were given the opportunity and had the ability to consult, but made an informed decision not to do so, and</li>
					<li>They fully understand their rights and obligations under this agreement.</li>
				</ul>
				<p>Now, Therefore, in consideration of the mutual promises, conditions and agreements herein contained, the sufficiency of which is acknowledged, Contractor and Subcontractor hereby agree as follows:</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">1. Scope of Agreement</h3>
				<p>Work for Hire. It is contemplated that from time to time Owner Operator will be requested by Contractor or its present or future affiliated entities to perform certain work and services . Owner Operator shall be obligated to accept requests to perform Work from Contractor during the term of this agreement. It is expressly understood and agreed that any and all Work requested by Contractor and accepted by Owner Operator shall be controlled and governed by the provisions of this Agreement. If and when Contractor desires Owner Operator to perform Work hereunder, Contractor will issue a Work order (“Work Order”) describing the Work Contractor desires Owner Operator to perform and the compensation Contractor will pay Owner Operator for the Work described in the Work order. Owner Operator shall notify Contractor in writing within five (5) business days of Owner Operator’s receipt of such Work Order if Owner Operator intends to accept the Work Order. The term “Agreement” means this Agreement as incorporated in a Work Order. Term “Contractor” includes the Contractor or any of its affiliated entities that issued the Work Order.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">2. Term and Termination</h3>
				<p>This Agreement shall remain in effect for a period of one year from the Effective Date, and from year to year thereafter, subject to the right of either party here to cancel or terminate the Agreement at any time upon not less than thirty (30) days written notice of one party to the other.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">3. Representations and Warranties</h3>
				<p>Owner Operator will furnish all necessary materials, equipment, permits, and certificates that are required for the Work. All of Owner Operator ’s materials and equipment are suitable for their intended use, and are free from all faults and defects. Owner Operator will remove and replace any defective materials or Work forthwith on notice from Contractor. Owner Operator will perform the Work entirely at Owner Operator ’s risk. Owner Operator will provide all proper and sufficient and necessary safeguards against all injuries and damage whatsoever and comply with all safety requirements imposed by law. Owner Operator shall conform to Contractor’s reasonable progress schedule. Owner Operator shall perform the Work in a professional, prompt, and diligent manner, consistent with applicable law and industry standards, without delaying or hindering Contractor’s work. If Owner Operator shall default in performance of the work or otherwise commit any act which causes delay to Contractor’s work, Owner Operator shall be liable for all losses, costs, expenses, liabilities and damages, including actual damages, consequential damages and any liquidated damages sustained by Contractor. Owner Operator will comply with all applicable federal, state and local laws, codes, ordinances, rules, regulations, orders and decrees of any government or quasi-government entity having jurisdiction over the project, the project site, the practices involved in the Work, or any subcontract work. Owner Operator, its agents and employees are properly trained and qualified to complete the Work for which they have been hired to do.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">4. Indemnification</h3>
				<p>“Claims” shall include, without limitation, any and all claims, losses, damages (including, without limitation, punitive damages and damages for bodily injury, death or property damage), causes of action, fines, penalties, enforcement proceedings, suits, and liabilities of every nature or character (including interest and all expenses of litigation, court costs, and attorneys' fees),whether or not arising in tort, contract, strict liability, under statute, or of any other character whatsoever, and whether or not caused by a legal duty. “Owner Operator Group” means Owner Operator, its parent, subsidiary and affiliated companies, and their contractors (of whatever tier), and its and their respective directors, officers, members, employees, agents, and representatives. “Contractor Group” means Contractor, its parent, subsidiary and affiliated companies, its and their co-lessees, partners, joint ventures, co-owners, contractors (other than Owner Operator), and its and their respective directors, officers, shareholders, members, employees, agents, and representatives. Owner Operator shall defend, indemnify, and hold harmless the Contractor Group for any Claims which any or all of the Contractor Group may incur in connection with any third party Claim arising out of or relating to the Owner Operator Group’s (or any member of the Owner Operator Group’s) actual or alleged negligence, willful misconduct, breach of this Agreement or violation of Law. The foregoing indemnification obligation includes, but is not limited to, any Claims asserted against the Contractor Group (or any member thereof) arising from pollution or contamination which originates above the surface of the land or water from spills of fuels, lubricants, motor oils, pipe dope, paints, solvents, cleaning solutions or other liquids, fumes and garbage which is in any manner associated with or alleged to have been associated with, resulting from or caused by Owner Operator Group’s Work, equipment or personnel.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">5. Insurance</h3>
				<p>Owner Operator, at its own cost and expense, shall procure, carry, and maintain insurance coverage satisfactory to Contractor. Owner Operator will include the following documentation: which shall include not less than the following coverage:</p>
				<!-- <ul class="subcontractor_agre_list">
					<li>General liability insurance with limits not less than $<input type="text" name="" value="" class="inline-input-field insurance_input_field"> per occurrence.</li>
					<li>Automobile Liability insurance with limits not less than $<input type="text" name="" value="" class="inline-input-field insurance_input_field"> per occurrence.</li>
					<li>Umbrella insurance with limits not less than $<input type="text" name="" value="" class="inline-input-field insurance_input_field"> per occurrence.</li>
					<li>Workers Compensation and Employers’ Liability with limits not less than $<input type="text" name="" value="" class="inline-input-field insurance_input_field">.</li>
					<li>Riggers Liability Insurance with limits not less than $<input type="text" name="" value="" class="inline-input-field insurance_input_field"> per occurrence.</li>
					<li>Motor Trade Cargo Insurance with limits not less than $<input type="text" name="" value="" class="inline-input-field insurance_input_field"> per occurrence.</li>
					<li>Installation Floater Insurance with limits not less than $<input type="text" name="" value="" class="inline-input-field insurance_input_field"> per occurrence.</li>
					<li>Any other insurance required by Contractor.</li>
				</ul> -->

				<fieldset class="custom-fieldset" style="margin-bottom: 10px !important;">
					<legend> Files </legend>
					<table class="subcontractor_table">
						<tr>
							<td>
								<div class="info-form-section" style="margin-top: -5px;">
									<p class="file_upload_header"> 1. Copy of W9 </p>
									<div class="row-input">
										@if(!$owner_w9->isEmpty())
	                                      <?php $i = 1; ?>
	                                      @foreach($owner_w9 as $rowdata)
	                                      <div><a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}} </a>
	                                      </div> 
	                                      <?php $i++; ?>
	                                      @endforeach
	                                    @else
	                                      No files uploaded
	                                    @endif 
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section" style="margin-top: -5px;">
									<p class="file_upload_header"> 2. Copy Of Owner Operator Comprehensive / Bobtail Insurance Policy </p>
									<div class="row-input">
										 @if(!$owner_operator_comprehensive->isEmpty())
                                          <?php $i = 1; ?>
                                          @foreach($owner_operator_comprehensive as $rowdata)
                                          <div><a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}} </a>
                                          </div> 
                                          <?php $i++; ?>
                                          @endforeach
                                        @else
                                          No files uploaded
                                        @endif
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section" style="margin-top: -5px;">
									<p class="file_upload_header"> 3. Copy Owner Operator Vehicle Registration </p>
									<div class="row-input">
										@if(!$owner_vehicle_reg->isEmpty())
                                          <?php $i = 1; ?>
                                          @foreach($owner_vehicle_reg as $rowdata)
                                          <div><a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}} </a>
                                          </div> 
                                          <?php $i++; ?>
                                          @endforeach
                                        @else
                                          No files uploaded
                                        @endif
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section" style="margin-top: -5px;">
									<p class="file_upload_header"> 4. Copy of DOT Annual Inspection Certificate </p>
									<div class="row-input">
										 @if(!$owner_annual_insp_cer->isEmpty())
                                          <?php $i = 1; ?>
                                          @foreach($owner_annual_insp_cer as $rowdata)
                                          <div><a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}} </a>
                                          </div> 
                                          <?php $i++; ?>
                                          @endforeach
                                        @else
                                          No files uploaded
                                        @endif
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<p class="file_upload_header"> 5. Copy of Heavy Vehicle Use Form </p>
									<div class="row-input">
										@if(!$owner_vehicle_use_form->isEmpty())
                                          <?php $i = 1; ?>
                                          @foreach($owner_vehicle_use_form as $rowdata)
                                          <div><a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}} </a></div> 
                                          <?php $i++; ?>
                                          @endforeach
                                        @else
                                          No files uploaded
                                        @endif
									</div>
								</div>
							</td>
						</tr>
					</table>
				</fieldset>

				<p>Owner Operator shall name Contractor as an additional insured on all policies of insurance. Owner Operator shall cause its insurance carrier to forward forthwith to Contractor a standard certificate of insurance such certificate shall require the insurance carrier to give Contractor thirty (30) days prior written notice of the cancellation of such insurance. Owner Operator shall not perform any work under the Agreement until a Certificate of Insurance has been delivered to Contractor. If Owner Operator fails to furnish current evidence upon demand of any insurance required hereunder, or if any insurance is cancelled or materially changed, Contractor may suspend or terminate this Agreement until insurance is obtained. The insurance coverage required herein shall in no way limit the Owner Operator’s indemnification obligations or other liability under this Agreement. </p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">6. Waiver of Subrogation</h3>
				<p>Owner Operator hereby waives any right of subrogation that it, or anyone claiming through or under it, may have against contractor, its agents, employees, or anyone for whom blakely construction company, inc. may be responsible, for any loss or damage shall have been caused by the fault or negligence of contractor. However, this waiver shall not adversely or impair any policies of insurance or prejudice the right of either party to recover thereunder. Owner Operator shall provide an endorsement from Owner Operator’s insurer that any right of subrogation is waived as against contractor and that such waiver will not impair Owner Operator's policies of insurance. Any failure or delay by contractor to require such an endorsement shall not constitute a waiver, nor shall it otherwise affect this mutual waiver of subrogation.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">7. Independent Contractor</h3>
				<p>Owner Operator shall have the status of an "independent contractor". Owner Operator shall employ and direct all persons engaged in the performance of any and all of its services under this Agreement, and such agents, servants or employees are subject to the sole control and direction of Owner Operator, and shall not be agents, servants or employees of Contractor. Owner Operator ’s agents, contractors and employees shall not be entitled to any compensation, benefits or other remuneration from Contractor. Contractor shall not have any authority to supervise or direct the manner in which Owner Operator shall perform the Work to be rendered hereunder. Owner Operator shall provide and be responsible for providing, using and maintaining all equipment, machinery and tools necessary to perform the Work. Owner Operator may accept or reject Work Orders in its discretion and may perform services for persons or entities other than Contractor.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">8. Notices</h3>
				<p>All notices and other communications pertaining to this Agreement shall be in writing and shall be deemed to have been given by a party hereto if personally delivered to the other party or if sent by certified mail, return receipt requested, postage prepaid. All notices shall be addressed as shown below:</p>

				<fieldset class="custom-fieldset" style="margin-bottom: 30px;">
					<legend>Contractor Form</legend>
					<table class="subcontractor_table ">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Contractor</p>
										<span><input type="text" value="{{$data->notice_owner_contractor_name}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Address</p>
										<span><input type="text" value="{{$data->notice_owner_contractor_address}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Phone</p>
										<span><input type="text" value="{{$data->notice_owner_contractor_phone}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Email</p>
										<span><input type="text" value="{{$data->notice_owner_contractor_email}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Attn</p>
										<span><input type="text" value="{{$data->notice_owner_contractor_attn}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>

					</table>
				</fieldset>
				<fieldset class="custom-fieldset">
					<legend>Owner Operator Form</legend>
					<table class="subcontractor_table ">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Owner Operator Name</p>
										<span><input type="text" value="{{$data->notice_owner_operator_name}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Address</p>
										<span><input type="text" value="{{$data->notice_owner_operator_address}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Phone</p>
										<span><input type="text" value="{{$data->notice_owner_operator_phone}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Email</p>
										<span><input type="text" value="{{$data->notice_owner_operator_email}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Contact</p>
										<span><input type="text" value="{{$data->notice_owner_operator_contact}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>

					</table>
				</fieldset>
					
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">9. Breach of Agreement</h3>
				<p>If Owner Operator breaches any of the terms of this agreement, then Contractor has the option, at its sole and absolute discretion, to terminate this agreement immediately, without written notice. Failure of Contractor to insist upon Owner Operator ’s performance under this Agreement or to exercise any right or privilege shall not be a waiver of any of Contractor’s rights or privileges contained herein.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">10. Record Keeping</h3>
				<p>For the purposes of permitting verification by the Contractor of any amounts paid to or claimed by the Owner Operator , the Owner Operator shall keep and preserve, for not less than two years from date of invoice all general ledgers , work orders, receipts, disbursements journals, bids, bid proposals, price lists, and supporting documentation obtained from manufacturers and suppliers in connection with the Work performed.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">11. Reports</h3>
				<p>Owner Operator shall provide to Contractor an oral report, confirmed in writing as soon as practicable, of all accidents or occurrences resulting in death or injuries to Owner Operator ’s employees, Owner Operator s, or any third parties, damage to Contractor’s property, or physical damage to the property of Contractor or any third party, arising out of or during the course of work to be performed. Owner Operator shall furnish Contractor with a copy of all reports made by Owner Operator to Owner Operator ’s insurer, governmental authorities, or to others of such accidents or occurrences.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">12. Payment Terms</h3>
				<p>Contractor agrees to pay Owner Operator for any work at the rates set forth in the applicable Work Order. Owner Operator shall provide Contractor upon the completion of any Work a signed Work Order indicating number of hours worked and certifying to the completion of the job in accordance with the terms of the Work Order (the “Ticket”). Owner Operator will receive 75% of the billed hourly rate of the job. Owner Operator is responsible for all expenses including but not limited to driver wages, fuel, repairs, and maintenance. Contactor shall pay Owner Operator within 14 days of proof / receipt of completed work. In the event Contractor has a bona fide question or dispute concerning an Owner Operator’s Ticket or a portion thereof, Contractor may withhold the disputed portion of the payment only. Contractor shall give written notice of any disputed amounts to Owner Operator specifying the reasons therefore within sixty (60) days after receipt of the applicable Ticket. Tickets submitted later than thirty (30) days will not be considered for reimbursement. Contractor shall not pay or reimburse Owner Operator for any travel or other expenses unless Contractor approves such expenses in advance and in writing.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">13. Audit</h3>
				<p>Owner Operator shall maintain a true and correct set of records pertaining to all work performed under each work order, including supporting documentation, for two (2) years following completion of the Work. Contractor may, at its expense require Owner Operator at any time within said two-year period to furnish sufficient evidence, with documentary support, to enable Contractor to verify the correctness and accuracy of payments to Owner Operator. Contractor may, following written notice to Owner Operator audit any and all records of Owner Operator relating to the work performed by or on behalf of Owner Operator hereunder, and all payments made in regard thereto, in order to verify the accuracy and compliance with this provision; provided however, Contractor shall not have the right to inspect or audit Owner Operator ’s trade secrets or any proprietary information. If Contractor’s examination discloses that Owner Operator ’s tickets to Contractor were in error, Owner Operator will immediately pay to Contractor any amounts overpaid by Contractor, plus interest from the date of the error at the lesser of one percent (1%) per month or the maximum rate allowed by law.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">14. No delay</h3>
				<p>In no case (unless otherwise directed in writing by the Contractor) shall any claim or dispute, whether or not involving litigation, permit the Owner Operator to delay or discontinue any of the work hereunder, but rather the Owner Operator shall diligently pursue the work hereunder while awaiting the resolution of any dispute or claim; provided, however, that the Contractor shall not withhold, pending the resolution of any claim or dispute, the payment of any undisputed sum otherwise due Owner Operator under this Agreement.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">15. Drugs, Alcohol & Firearms</h3>
				<p>To help ensure a safe, productive work environment, Contractor prohibits the use, transportation and possession of firearms, drugs and/or controlled substances, drug paraphernalia and alcoholic beverages. Illegal drugs shall include, but not be limited to, marijuana, amphetamines, barbiturates, opiates, cocaine, codeine, morphine, hallucinogenic substances (LSD) and any similar drugs and/or chemical synthetics deemed hazardous by Contractor. Such prohibitions shall apply to Owner Operator and its employees, agents, servants and Owner Operator’s employees, agents, contractors and invitees shall abide the more stringent of Owner Operator’s or Contractor’s drug, alcohol and firearm policy. Contractor may request that Owner Operator carry out drug and alcohol tests of its employees and/or that Owner Operator carry out reasonable searches of individuals, their personal effects and vehicles when entering on and leaving assigned premises at any time, at scheduled times, or at random. Individuals found in violation shall be removed from the premises by Owner Operator immediately. Submission to such a search is strictly voluntary, however, refusal may be cause for not allowing that individual on the well site or Contractor’s other premises. Owner Operator shall (1) test at Owner Operator’s expense any individual involved in or related to an accident or injury within twelve (12) hours of such accident or injury and (2) submit to Contractor any drug or alcohol test results for any individual involved in or related to an accident or injury on Contractor’s premises. It is Owner Operator ’s responsibility to notify its employees, contractors, Owner Operators, agents and invitees of this prohibition, the provisions of this paragraph and its enforcement and obtain any acknowledgement or release from any person in order to comply with this provision and applicable law.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">16. Miscellaneous</h3><br><br>
				<h3 class="sub-title">A. Governing Law</h3>
				<p>The laws of the State of Texas shall apply and govern the validity, interpretation, and performance of this Agreement. The Parties waive any right to trial by jury in any action arising out of or relating to this Agreement or the Parties’ relationship.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">B. Severability</h3>
				<p>If any part of this Agreement contravenes any applicable statutes, regulations, rules, or common law requirements, then, to the extent of and only to the extent of such contravention, such part shall be severed from this Agreement and deemed non-binding while all other parts of this Agreement shall remain binding.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">C. Amendment</h3>
				<p>This Agreement may not be modified or amended unless it is in writing and signed by both Parties.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">D. Assignment</h3>
				<p>Neither Party shall assign all or any part of its rights or obligations under this Agreement without the prior written consent from the other Party.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">E. Compliance with Laws</h3>
				<p>Owner Operator agrees to comply with all laws, statutes, codes, rules, and regulations, which are now or may hereafter become applicable to Work covered by this Agreement or arising out of the performance of such operations.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">F. Headings</h3>
				<p>The headings, sub-headings, and other subdivisions of this Agreement are inserted for convenience only. The Parties do not intend them to be an aid in legal construction.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">G. Authority to Sign and Bind</h3>
				<p>By their signatures below, Owner Operator represents that the person signing this Agreement has the authority to execute same on behalf of Owner Operator and to bind Subcontract to the obligations set forth herein.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">H. Registration & Fees</h3>
				<p>Owner Operator Additional Charges:</p>
				<ul class="registration_list">
					<li>Admin Fee :<span>$</span><span><input type="text" value="{{$data->owner_admin_fee}}" name="" /></span> ( Monthly )</li>

					<li>Training (If Required / Applicable):<span>$</span><span><input type="text" value="{{$data->owner_training_fee}}" name="" /></span> ( One Time Fee )</li>

					<li>New Truck Set Up Fee:<span>$</span><span><input type="text" value="{{$data->owner_truck_set_up_fee}}" name="" /></span> ( One Time Fee )</li>

					<li>MVR:<span>$</span><span><input type="text" value="{{$data->owner_mvr_fee}}" name="" /></span> ( One Time Fee )</li>

					<li>Decals:<span>$</span><span><input type="text" value="{{$data->owner_decals_fee}}" name="" /></span> ( One Time Fee )</li>

					<li> Required PPE ( Gas Monitor ):<span>$</span><span><input type="text" value="{{$data->owner_ppe_fee}}" name="" /></span></li>

					<li>GPS Installation:<span>$</span><span><input type="text" value="{{$data->owner_gps_installation_fee}}" name="" /></span> ( One Time Fee )</li>

					<li>DOT Cab Card:<span>$</span><span><input type="text" value="{{$data->owner_dot_cab_card_fee}}" name="" /></span> ( One Time Fee )</li>

					<li>GPS Monitoring:<span>$</span><span><input type="text" value="{{$data->owner_gps_monitoring_fee}}" name="" /></span> ( Monthly )</li>

					<li>Insurance (Per Truck / Tank):<span>$</span><span><input type="text" value="{{$data->owner_ins_per_truck_fee}}" name="" /></span> ( Weekly )</li>

					<li>Drug Testing:<span>$</span><span><input type="text" value="{{$data->owner_drug_testing_fee}}" name="" /></span> ( One Time Fee )</li>

					<li>Field Tickets:<span>$</span><span><input type="text" value="{{$data->field_tickets_fee}}" name="" /></span> /Book </li>
				</ul>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<p>Executed this <input type="text" name="" class="inline-input-field day" value="{{$data->executed_day_owner}}"> day of <input type="text" name="" class="inline-input-field month" value="{{$data->executed_month_owner}}"> <input type="text" name="" class="inline-input-field year" value="{{$data->executed_year_owner}}"></p>
			</div>
		</div>

		<div class="row">
			<div class="column">			
				<fieldset class="custom-fieldset" style="margin-bottom: 30px !important;">
					<legend>Contractor</legend>
					<table class="subcontractor_table">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Contractor</p>
										<span><input type="text" value="{{$data->executed_owner_contractor_name}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Contractor By</p>
										<span><input type="text" value="{{$data->executed_owner_contractor_by}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Title</p>
										<span><input type="text" value="{{$data->executed_owner_contractor_title}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Date</p>
										<span><input type="text" value="{{$data->executed_owner_contractor_date}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p>Initial</p>
										<span><input type="text" value="{{$data->executed_owner_contractor_initial}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</fieldset>

				<fieldset class="custom-fieldset" style="margin-bottom: 30px !important;">
					<legend>Owner Operator</legend>
					<table class="subcontractor_table">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Owner Operator</p>
										<span><input type="text" value="{{$data->executed_owner_operator_name}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> By</p>
										<span><input type="text" value="{{$data->executed_owner_operator_by}}" name=""/></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Title</p>
										<input type="text" value="{{$data->executed_owner_operator_title}}" name="" />
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Date</p>
										<input type="text" value="{{$data->executed_owner_operator_date}}" name="" />
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Initial</p>
										<span><input type="text" value="{{$data->executed_owner_operator_initial}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</fieldset>


				<div class="row">
					<br><br><br><br><br><br><br><br><br>
					<div class="column">
						@if($owner_sign)
			                <fieldset class="custom-fieldset" style="margin-top: 30px !important;">
			                <legend>Signature</legend>
			                    <img src="{{ public_path() . '/storage/files/'. $owner_sign->name }}" width="100%">
			                </fieldset>
			            @endif
	        		</div>
	        	</div>

			</div>
		</div>

	</div>

	<footer>
	    <div class="footer-section-table">
	    	<div class="footer-section-row">
		    	<div class="footer-part footer-one">
		    		<img src="{{ public_path() . '/assets/img/logo.png' }}">
		    	</div>
		    	<div class="footer-part footer-two">
		    		<p>903 W. Industrial Ave. Midland, TX 79701</p>
		    	</div>
		    	<div class="footer-part footer-three">
		    		<p></i>432-253-9651</p>
		    	</div>
		    	<div class="footer-part footer-four">
		    		<p>info@evoilfieldservices.com</p>
		    	</div>
		    </div>
	    </div>
	</footer>

</body>
</html>
