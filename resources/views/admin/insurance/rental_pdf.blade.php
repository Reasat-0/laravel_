<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title>Rental Agreement</title>
</head>
<style>
	body{
		font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
	}
	/* Header Secction CSS */
	.request_id_badge{
		background: #f6cb61;
		width:140px;
		height: 30px;
		line-height: 1.3;
		border-radius: 0;
		text-align: center;
		color:white;
		border-radius: 50%;
		position: absolute;
		right: -30px;
		top: -30px;
		border: 1px solid #000;
		border-radius: 15px;
	}
	.request_id_badge h4{
		padding: 6px 5px;
		font-size:12px;
		font-weight: 300;
		margin: 0;
		text-transform: uppercase;
		color: #000;
	}
	.insurance_logo{
		text-align: center;
	}
	.insurance_logo img{
		height: 100px;
		margin-top:-40px;
	}
	.rental_agreement_title_secton{
		text-align: center;
	}
	.rental_agreement_title_secton .title{
		font-weight: bold;
		text-transform: capitalize;
		font-size: 22px;
		font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
		margin: 0;
		line-height: 1;
		margin-top: 40px;
	}
	/* end of Footer Secction CSS */

	/* Footer Secction CSS */
	footer {
	    position: absolute; 
	    bottom: -30px; 
	    left: 0; 
	    right: 0;
	    color: white;
	    height: 45px;
	    text-align: center;
	    /*background-color: #eff0f1;*/
	} 
	.footer-section-table{
		display: table;
		width: 100%;
		vertical-align: middle;
		table-layout: fixed;
	}
	.footer-section-row{
		display: table-row;
	}
	.footer-part{
		display: table-cell;
		height: 45px;
		vertical-align: middle;
	}
	.footer-part img{
		height: 30px;
		display: block;
		text-align: left;
	}
	.footer-part p{
		margin: 0;
		color: #676767;
		font-size: 12px;
		line-height: 1.3;
	}
	/* end Footer Secction CSS */

	/* Content Section CSS */
	.rental_agreement_pdf_content_section p{
		font-size:14px;
		line-height: 24px;
	}
	.points_row_first{
		margin-bottom: 20px;
	}
	.point_head_holder_para{
		margin-bottom:-5px;
		text-decoration: underline;
	}
	.point_head{
		font-size:18px;
		font-weight: 700;
	}
	.input_ten{
	  width:15%;
	}
	.input_fifteen{
	  	width:15%;
	}
	.input_twenty{
	  	width:20%;
	}
	.input_thirty{
	  	width:30%;
	}
	.input_forty{
	  	width:40%;
	}
	table{
	  	width:100%;
	}
	td{
		width:50%;
	}
	.text_area,input{
		border:0;
		border-bottom: 1px solid black;
		width:100%;
		font-size:14px;
		color:#0e0e0ef0;
	}
	.check_one input,
	.check_two input{
		border:0;
	}
	.footer_note_points_row{
		margin-top:20px;
		margin-bottom:30px;
	}
	.footer_note_points_row .footer_note{
		font-size:15px;
		font-weight: 700;
	}
	fieldset{
		padding-bottom:30px;
		padding-top:10px;
	}
	legend{
		font-weight: 700;
		font-size:18px;
	}
	.uploaded-file div {
		font-size:14px;
		color: #333232;
	}
	.check_one{
		display: inline-block;
		position: absolute;
		top:0px;
	}
	.check_two{
		display: inline-block;
		position: absolute;
		top:0px;
	}
	.check_box_wrapper-first{
		display: inline-block;
		width:52px;
		height: 15px;
		position: relative;

	}
	.check_box_wrapper-last{
		display: inline-block;
		width:15px;
		height: 15px;
		position: relative;
	}
	.shall_or{
		font-size:14px;
		line-height: 1.6;
	}
	.shall_or_lease{
		line-height: 0;
	}
	.info_div{
		margin-bottom: 30px;
	}
	fieldset p{
		font-weight: 700;
	}
	/* end of Main Secction CSS */
	.bold_paragraph_header{
		margin-bottom: -5px
	}
	.bold_paragraph_header span input{
		color:#222323;
		font-weight: 400;
		padding-left:0px;
	}

	.bold_paragraph_header span input.null_input{
		margin-top:10px;
	}
</style>


<body>
	<div class="request_id_badge">
		<h4 class="title"> RN-{{$data->id}} </h4>
	</div>
	<div class="insurance_logo">
		<img src="{{ public_path() . '/assets/img/logo_ins.png' }}">
	</div>
	<div class="rental_agreement_pdf_main_section">  	
		<div class="rental_agreement_title_secton">
			<h4 class="title">EQUIPMENT RENTAL AGREEMENT</h4>
		</div>

		<div class="col-md-11 rental_agreement_pdf_content_section">
		  <div class="tabRen">
		    <div class="row">
		      <div class="col-md-12">
		        <p> This Equipment Rental Agreement ( the “Agreement” ) is made and entered on 
		            <input type="text" class="form-control ren_agreement_input input_twenty form_input_field form_fields" value="{{$data->entered_on}}" > ,<br />
		            <!-- <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_twenty" value="{{$data->entered_by}}" >  --> by and between EV Oilfield Services,LLC (Lessor ) and
		            <!-- <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_forty" value="{{$data->entered_between_lessor}}"> (“Lessor”) and <br /> -->
		            <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_forty" value="{{$data->entered_between_lessee}}" > <br /> (“Lessee”) ( collectively referred to as the “Parties” ).
		        </p>
		      </div>
		      <div class="col-md-12 agree_as_follows">
		        <p> The Parties agree as follows:  </p>
		      </div>
		    </div>
		    <div class="row points_row points_row_first">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 1. Equipment : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          Lessor hereby rents to Lessee the following equipment : 
		        </p>
		      </div>
		      <div class="col-md-12">
		        <input type="text" class="text_area" value="{{$data->ren_ag_lesse_equipment}}" />
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 2. Rental Term : </span> </p>
		      </div>
		      <div class="col-md-12">
		        <p>
		          The rent will start on
					<input type="text" class="form-control ren_agreement_input input_twenty form_input_field form_fields" value="{{$data->ren_ag_start_date}}"> 
		          <!-- <input type="number" class="ren_agreement_input form-control form_input_field form_fields input_fifteen datepicker" placeholder="Begin Date"> -->
		          and will end on
					<input type="text" class="form-control ren_agreement_input input_twenty form_input_field form_fields" value="{{$data->ren_ag_end_date}}"> 
		          (Rental Term).
		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 3. Rental Payments : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p>
		          Lessee agrees to pay to Lessor as rent for the Equipment the amount of $
		          <input type="text" class="form-control ren_agreement_input input_twenty form_input_field form_fields" value="{{$data->ren_ag_amount_rent}}"> 
		          (“Rent”) each <br />
		          <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_ten" placeholder="" value="{{$data->ren_ag_time_rent}}"> 
		          in advance on the first day of each month at :
		          <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_forty" placeholder="" value="{{$data->ren_ag_address_rent}}"> ( address for rent payment )
		          at any other address designated by Lessor. If the Rental Term does not start on the first
		          day of the month or end on the last day of a month, the rent will be prorated accordingly.

		        </p>
		      </div>
		    </div>                                        
		  </div>

		  <div class="tabRen">
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 4. Late Charges : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          If any amount under this Agreement is more than
		          <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_ten" placeholder="" value="{{$data->ren_ag_late_day}}"> days late, Lessee agrees to pay a late fee of $
		          <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_twenty" placeholder="" value="{{$data->ren_ag_late_charge}}">

		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 5. Security Deposit : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          Prior to taking possession of the Equipment, Lessee shall deposit with Lessor, in trust, a security deposit of $ <br />
		          <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_twenty" placeholder="" value="{{$data->ren_ag_security_deposit}}"> as security for the performance by Lessee of the terms under this Agreement and for any damages caused by Lessee or Lessee’s agents to the Equipment during the Rental Term. Lessor may use part or all of the security deposit to repair any damage to Equipment caused by Lessee or Lessee’s agents. However, Lessor is not just limited to the security deposit amount and Lessee remains liable for any balance. Lessee shall not apply or deduct any portion of any security deposit from the last or any month's rent. Lessee shall not use or apply any such security deposit at any time in lieu of payment of rent. If Lessee breaches any terms or conditions of this Agreement, Lessee shall forfeit any deposit, as permitted by law.                                               
		        </p>

		      </div>
		    </div>                                        
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 6. Delivery : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          
		          <div class="check_box_wrapper-first">
		          <span class="shall_or shall_or_lease">Rental</span>
		          <span class="form-check form-check-inline check_one">
		            <label class="form-check-label">
		              <input class="form-check-input" name="responsible" type="checkbox" value="" @if($data->responsible == 'responsible') checked @endif>
		              <span class="form-check-sign">
		                <span class="check"></span>
		              </span>
		            </label>
		          </span>
		          </div>
		          <span class="shall_or"> Shall Or</span>
		          <div class="check_box_wrapper-last">
		          <span class="form-check form-check-inline check_two">
		            <label class="form-check-label">
		              <input class="form-check-input" type="checkbox" name="responsible" value="" @if($data->responsible == 'not_responsible') checked @endif>	              
		              <span class="form-check-sign">
		                <span class="check"></span>
		              </span>
		            </label>		            
		          </span>
		      </div>
		      	<span class="shall_or">
		           Shall Not be responsible for all expenses and costs: i) at the beginning of the Rental Term, of shipping the Equipment to Lessee’s premises and ii) at the end of the Rental Term, of shipping the Equipment back to Lessor’s premises.
		        </span>			          

		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 7. Defaults : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          If Lessee fails to perform or fulfill any obligation under this
		          Agreement, Lessee shall be in default of this Agreement. Subject to any statute,
		          ordinance or law to the contrary, Lessee shall have seven (7) days from the date of notice
		          of default by Lessor to cure the default.
		          In the event Lessee does not cure a default, Lessor may at Lessor’s option (a) cure such default and the cost of such action may be added to Lessee’s financial obligations under this Agreement; or (b) declare Lessee in default of the Agreement. If Lessee shall become insolvent, cease to do business as a going concern or if a petition has been filed by or against Lessee under the Bankruptcy Act or similar federal or state statute, Lessor may immediately declare Lessee in default of this Agreement. In the event of default, Lessor may, as permitted by law, re-take possession of the Equipment. Lessor may, at its option, hold Lessee liable for any difference between the Rent that would have been payable under this Agreement during the balance of the unexpired term and any rent paid by any successive lessee if the Equipment is re-let minus the cost and expenses of such reletting. In the event Lessor is
		          unable to re-let the Equipment during any remaining term of this Agreement, after default
		          by Lessee, Lessor may at its option hold Lessee liable for the balance of the unpaid rent under this Agreement if this Agreement had continued in force.
		        </p>

		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 8. Possession And Surrender Of Equipment : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          Lessee shall be entitled to possession of the Equipment on the first day of the Rental Term. At the expiration of the Rental Term, Lessee shall surrender the Equipment to Lessor by delivering the Equipment to Lessor or Lessor’s agent in good condition and working order, ordinary wear and tear excepted, as it was at the commencement of the Agreement.
		        </p>
		      </div>
		    </div>
		  </div>

		  <div class="tabRen">
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 9. Use Of Equipment : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          Lessee shall only use the Equipment in a careful and proper manner and will comply with all laws, rules, ordinances, statutes and orders regarding the use, maintenance of storage of the Equipment.
		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 10. Condition Of Equipment And Repair : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          Lessee or Lessee’s agent has inspected the Equipment and acknowledges that the Equipment is in good and acceptable condition.
		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 11. Maintenance, Damage And Loss : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          Lessee will, at Lessee's sole expense, keep and maintain the Equipment clean and in good working order and repair during the Rental Term. In the event the Equipment is lost or damaged beyond repair, Lessee shall pay to Lessor the replacement cost of the Equipment; in addition, the obligations of this Agreement shall continue in full force and effect through the Rental Term.
		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 12. Insurance : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          Lessee shall be responsible to maintain insurance on the Equipment with losses payable to Lessor against fire, theft, collision, and other such risks as are appropriate and specified by Lessor.  Upon request by Lessor, Lessee shall provide proof of such insurance.
		        </p>
		      </div>
		    </div>                                        
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 13. Encumbrances, Taxes And Other Laws : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          Lessee shall keep the Equipment free and clear of any liens or other encumbrances, and shall not permit any act where Lessor’s title or rights may be negatively affected. Lessee shall be responsible for complying with and conforming to all laws and regulations relating to the possession, use or maintenance of the Equipment. Furthermore, Lessee shall promptly pay all taxes, fees, licenses and governmental charges, together with any penalties or interest thereon, relating to the possession, use or maintenance of the Equipment.
		        </p>
		      </div>
		    </div>                       
		  </div>

		  <div class="tabRen">
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 14. Lessors Representations : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          Lessor represents and warrants that he/she has the right to rent the Equipment as provided in this Agreement and that Lessee shall be entitled to quietly hold and possess the Equipment, and Lessor will not interfere with that right as long as Lessee pays the Rent in a timely manner and performs all other obligations under this Agreement.
		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 15. Ownership : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          The Equipment is and shall remain the exclusive property of Lessor.
		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 16. Severability : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          If any part or parts of this Agreement shall be held unenforceable for any reason, the remainder of this Agreement shall continue in full force and effect. If any provision of this Agreement is deemed invalid or unenforceable by any court of competent jurisdiction, and if limiting such provision would make the provision valid, then such provision shall be deemed to be construed as so limited.
		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 17. Assignment : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          Neither this Agreement nor Lessee’s rights hereunder are assignable except with Lessor’s prior, written consent.
		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 18. Binding Effect : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          The covenants and conditions contained in the Agreement shall apply to and bind the Parties and the heirs, legal representatives, successors and permitted assigns of the Parties.
		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 19. Governing Law : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          This Agreement shall be governed by and construed in accordance with the laws of the State of <br />
		          <span>
		          	<input type="text" class="input_forty" value="{{$data->ren_ag_governing_law}}">
		          </span>
		        </p>
		      </div>
		    </div>                      
		  </div>

		  <div class="tabRen">
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 20. Notice : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          Any notice required or otherwise given pursuant to this Agreement shall be in writing and mailed certified return receipt requested, postage prepaid, or delivered by overnight delivery service to:
		        </p>
		      </div>
		      <table>
		      <td class="col-5">
		        <p> Lessor : </p>
		        <input type="text" style="width: 95%" value="{{$data->lessor_agreement_notice}}" />
		      </td>
		      <td class="col-5">
		        <p> Lessee : </p>
		        <input type="text" style="width: 95%" value="{{$data->lesse_agreement_notice}}">
		      </td>
		  	  </table>
		      <div class="col-md-12">
		        <p> Either party may change such addresses from time to time by providing notice as set forth above. </p>
		      </div>                                         
		    </div>                                         
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 21. Entire Agreement : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          This Agreement constitutes the entire agreement between the Parties and supersedes any prior understanding or representation of any kind preceding the date of this Agreement. There are no other promises, conditions, understandings or other agreements, whether oral or written, relating to the subject matter of this Agreement. This Agreement may be modified in writing and must be signed by both Lessor and Lessee.
		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 22. Cumulative Rights : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          Lessor’s and Lessee’s rights under this Agreement are cumulative, and shall not be construed as exclusive of each other unless otherwise required by law.
		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 23. Waiver : </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          The failure of either party to enforce any provisions of this Agreement shall not be deemed a waiver or limitation of that party's right to subsequently enforce and compel strict compliance with every provision of this Agreement.The acceptance of rent by Lessor does not waive Lessor’s right to enforce any provisions of this Agreement.
		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class="point_head_holder_para"><span class="point_head"> 24. Indemnification :  </span> </p>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          Except for damages, claims or losses due to Lessor’s acts or negligence, Lessee, to the extent permitted by law, will indemnify and hold Lessor and Lessor’s property, free and harmless from any liability for losses, claims, injury to or death of any person, including Lessee, or for damage to property arising from Lessee using and possessing the Equipment or from the acts or omissions of any person or persons, including Lessee, using or possessing the Equipment with Lessee’s express or implied consent.
		        </p>
		      </div>
		    </div>
		    <div class="row points_row">
		      <div class="col-md-12">
		        <p class=""><span class="point_head" style="text-decoration: underline;"> 25. Additional Terms & Conditions : </span> ( Specify “none” if there are no additional provisions ) </p>
		      </div>
		      <div class="col-md-12">
		        <input type="text" class="text_area" value="{{$data->ren_ag_additional_terms}}" />
		      </div>
		    </div>
		    <div class="row points_row footer_note_points_row">
		      <div class="col-md-12" style="text-align: center;">
		        <span class="footer_note"> [ Remainder of Page Intentionally Left Blank ] </span>
		      </div>
		      <div class="col-md-12"> 
		        <p> 
		          IN WITNESS WHEREOF , the parties have caused this Agreement to be executed the day and year first above written.
		        </p>
		      </div>		      
		    </div>              
		  </div>

		  <div class="tabRen">
		    <div class="row points_row">
		    </div>
		    <div class="row points_row signature_points_row info_div">
			    <table>
			      <td>
			        <fieldset class="custom_fieldset_2 contractor_fieldset">
			          <legend> Lessor : </legend>

			          <div class="form-group bmd-form-group mt-3">
			            <p class="bold_paragraph_header"> Name:  <br />
			            	<span>
			            	<input type="text" class="form-control form_input_field form_fields" id="lessor_ag_pos" name="lessor_ag_names" style="width: 100%" value="{{$data->ren_ag_executed_lessor_name}}">
			            	</span>
			            </p>
			          </div>

			          <div class="form-group bmd-form-group">
			            <p class="bold_paragraph_header"> Position , If Applicatble <br />
			            <span>
			            	<input type="text" class="form-control form_input_field form_fields" id="lessor_ag_pos" name="lessor_ag_names" style="width: 100%" value="{{$data->ren_ag_executed_lessor_position}}">
						</span>
						</p>
			          </div>
			        </fieldset>                                        
			      </td>
			      <td>
			        <fieldset class="custom_fieldset_2 contractor_fieldset">
			          <legend> Lessee : </legend>

			          <div class="form-group bmd-form-group mt-3">
			            <p class="bold_paragraph_header"> Name : <br />
			            <span>
			            	<input type="text" class="form-control form_input_field form_fields" id="lessor_ag_pos" name="lessor_ag_names" style="width: 100%" value="{{$data->ren_ag_executed_lessee_name}}">
						</span>
						 </p>
			          </div>

			          <div class="form-group bmd-form-group">
			            <p class="bold_paragraph_header"> Position , If Applicatble <br />
			            <span>
			            	<input type="text" class="" id="" name="lessor_ag_names" style="width: 100%" value="{{$data->ren_ag_executed_lessee_position}}">
						</span>
						</p>
			          </div>
			        </fieldset>                                              
			      </td>
			    </table>
		    </div>
		    <fieldset class="custom_fieldset_2 contractor_fieldset info_div">
		      <legend> Necessary Files </legend>
		      <div class="row input_row poins_row mb-3">
		      	<table>
		        <td class="col-md-12">
			            <p> W9 </p>
			            <div class="uploaded-file">
			            @foreach($agreement_files as $rowdata)
			            	<div> <a href="{{$rowdata->url}}">{{$rowdata->name}}</a> </div>
			            @endforeach
			            </div>
		        </td>
		        <td style="border-left:2px solid gray; padding-left:10px">
			            <p> Insurance Certificate </p>
			            <div class="uploaded-file">
			            @foreach($insurance_files as $rowdata)
			            	<div> <a href="{{$rowdata->url}}">{{$rowdata->name}}</a> </div>
			            @endforeach
			            </div>
		        </td>
		        </table>		        
		      </div>
		    </fieldset>

			@if($rn_sign)
	                <fieldset class="custom-fieldset" style="margin-bottom: 30px !important;">
	                <legend>Signature</legend>
	                    <img src="{{ public_path() . '/storage/files/'. $rn_sign->name }}" width="100%">
	                </fieldset>
	        @endif                                                                                                                                                             
		  </div>
		</div>
	</div>
	<footer>
		<div class="footer-section-table">
			<div class="footer-section-row">
		    	<div class="footer-part footer-one">
		    		<img src="{{ public_path() . '/assets/img/logo.png' }}">
		    	</div>
		    	<div class="footer-part footer-two">
		    		<p>903 W. Industrial Ave. Midland, TX 79701</p>
		    	</div>
		    	<div class="footer-part footer-three">
		    		<p></i>432-253-9651</p>
		    	</div>
		    	<div class="footer-part footer-four">
		    		<p>info@evoilfieldservices.com</p>
		    	</div>
		    </div>
		</div>
	</footer>

</body>
</html>