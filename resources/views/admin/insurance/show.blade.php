@extends('layouts.master')

@section('content')

        <div class="content manage-inventory-content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12 mx-auto card_holder_box">
                
                
              <div class="card edit_preview_parent_card view_tab_root_card">
                <div class="card-header card-header-tabs card-header-rose custom_card_header mang_inventory_card_header" id="insurance_header_section">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <ul class="nav nav-tabs" data-tabs="tabs" id="tabMenu">
                        <li class="nav-item">
                          <a class="nav-link active" href="#profile" data-toggle="tab">
                            <i class="material-icons">visibility</i> View
                            <div class="ripple-container"></div>
                          </a>
                        </li>                        
                        <li class="nav-item">
                          <a class="nav-link" href="{{ route('insurance.index') }}#insurance_tab">
                            <i class="material-icons">bug_report</i>Direct Insurance
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="{{ route('insurance.index') }}#agreement_tab">
                            <i class="material-icons">code</i>Agreements
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="{{ route('insurance.index') }}#manage_tab">
                            <i class="material-icons">cloud</i> View Requests
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>

                <div style="margin-top: 15px;">
                      @include('partials.messages')
                </div>

                @if($type == 'equipment')
                  <div class="card-body col-md-12 mx-auto increased_margin_t_cardbody">
                    <div class="row preview_row">
                      <div class="col-md-12 mx-auto insurance_padding">
                        <div class="card preview_card">
                          <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                              <i class="material-icons">assignment</i>
                            </div>
                            <h4 class="card-title">Submitted Request</h4>
                            <div class="row preview_req_id_badge_row">
                              <div class="col-md-4 wrap">
                                <div class="preview_req_id_badge text-right">
                                  <div class="timeline-heading">
                                    <h4 class="badge badge-pill service_add">Request Id : EQ-{{$data->id}} </h4>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card-body">
                            <div class="row preview_checkbox_row">
                              <div class="col-md-12 text-center">
                                <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                    <input class="form-check-input insurance_check" type="checkbox" @if($data->request == 'add') checked @endif value="add" disabled>Add
                                    <span class="form-check-sign">
                                      <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                    <input class="form-check-input insurance_check" type="checkbox" @if($data->request == 'delete') checked @endif value="delete" disabled>Delete
                                    <span class="form-check-sign">
                                      <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                    <input class="form-check-input insurance_check" type="checkbox" @if($data->request == 'amend') checked @endif value="amend" disabled>Amend
                                    <span class="form-check-sign">
                                      <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                              </div>
                            </div>

                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Company Name<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->company_name}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Effective Date<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->effective_date}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Year<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->year}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Make<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->make}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Model <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->model}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Vin <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->vin}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Unit <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->unit}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Value <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->value}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Description Of Equipment Accessories <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->description_equipment_accessories}} </p>
                                 </div>
                            </div>
                            <fieldset class="custom_fieldset">
                              <legend> Garraging Location Address </legend>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> Address-1 <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->equip_garraging_address_1}} </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> Address-2 <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->equip_garraging_address_2}} </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> City <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->equip_garraging_city}} </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> State <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->equip_garraging_state}} </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> Zip <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->equip_garraging_zip}} </p>
                                   </div>
                              </div>                                
                            </fieldset>                                                                            
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Lease Or Purchase <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->lease_purchase}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Finance Company/Lessor Name  <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->finance_company_name}} </p>
                                 </div>
                            </div>
                            <fieldset class="custom_fieldset">
                              <legend> Finance Company/Lessor Address </legend>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> Address-1 <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->equip_lessor_address_1}}  </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> Address-2 <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->equip_lessor_address_2}}  </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> City <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->equip_lessor_city}} </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> State <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->equip_lessor_state}}  </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> Zip <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->equip_lessor_zip}} </p>
                                   </div>
                              </div>                                
                            </fieldset> 
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Finance Company/Lessor Phone Or Email For EP <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->finance_company_phone_email}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Additional Notes <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->additional_notes}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Submitted By<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->submitted_by}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Date<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->date}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Send Email To<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->send_mail_to}} </p>
                                 </div>
                            </div>


                          </div>
                        </div>                          
                      </div>
                    </div>
                  </div>
                @endif

                @if($type == 'vehicle')
                  <div class="card-body col-md-12 mx-auto increased_margin_t_cardbody">
                    <div class="row preview_row">
                      <div class="col-md-12 mx-auto insurance_padding">
                        <div class="card preview_card">
                          <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                              <i class="material-icons">assignment</i>
                            </div>
                            <h4 class="card-title">Submitted Request</h4>
                            <div class="row preview_req_id_badge_row">
                              <div class="col-md-4 wrap">
                                <div class="preview_req_id_badge text-right">
                                  <div class="timeline-heading">
                                    <h4 class="badge badge-pill service_add">Request Id : VH-{{$data->id}} </h4>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card-body">
                            <div class="row preview_checkbox_row">
                              <div class="col-md-12 text-center">
                                <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                    <input class="form-check-input insurance_check" type="checkbox" @if($data->request == 'add') checked @endif value="add" disabled>Add
                                    <span class="form-check-sign">
                                      <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                    <input class="form-check-input insurance_check" type="checkbox" @if($data->request == 'delete') checked @endif value="delete" disabled>Delete
                                    <span class="form-check-sign">
                                      <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                    <input class="form-check-input insurance_check" type="checkbox" @if($data->request == 'amend') checked @endif value="amend" disabled>Amend
                                    <span class="form-check-sign">
                                      <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                              </div>
                            </div>

                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Company Name<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->company_name}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Effective Date<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->effective_date}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                              <label class="col-sm-4 col-form-label">Liability<span> : </span></label>
                              <div class="col-sm-8 view_vehicle_liability_dam_col" style="margin-top: 15px;">
                                <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" @if($data->liability == 'yes') checked @endif value="" disabled="disabled">Yes
                                    <span class="form-check-sign">
                                      <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                  <input class="form-check-input" type="checkbox" @if($data->liability == 'no') checked @endif value="" disabled="disabled">No
                                  <span class="form-check-sign">
                                    <span class="check"></span>
                                  </span>
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                              <label class="col-sm-4 col-form-label">Physical Damage <span> : </span></label>
                              <div class="col-sm-8 view_vehicle_liability_dam_col" style="margin-top: 15px; ">
                                <div class="form-check form-check-inline">
                                   <label class="form-check-label">
                                   <input class="form-check-input" type="checkbox" @if($data->damage == 'yes') checked @endif value="" disabled="disabled">Yes
                                   <span class="form-check-sign">
                                        <span class="check"></span>
                                   </span>
                                   </label>
                                </div>
                                <div class="form-check form-check-inline">
                                   <label class="form-check-label">
                                   <input class="form-check-input" type="checkbox" @if($data->damage == 'no') checked @endif value="" disabled="disabled">No
                                   <span class="form-check-sign">
                                        <span class="check"></span>
                                   </span>
                                   </label>
                                </div>
                              </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Year <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->year}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Make <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->make}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Model <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->model}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Vin<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->vin}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Body Type <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->body_type}} </p>
                                 </div>
                            </div>

                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">State Registered<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->state_registered}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Unit<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->unit}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Gross Vehicle Weight<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->gross_vehicle_weight}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Cost New<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> $ {{$data->cost_new}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Mileage Radius<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->mileage_radius}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Special Equipment<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->special_equipment}} </p>
                                 </div>
                            </div>
                            <fieldset class="custom_fieldset">
                              <legend> Garraging Location Address </legend>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> Address-1 <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->vehicle_garraging_address_1}}  </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> Address-2 <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->vehicle_garraging_address_2}}  </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> City <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->vehicle_garraging_city}} </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> State <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->vehicle_garraging_state}}  </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> Zip <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->vehicle_garraging_zip}} </p>
                                   </div>
                              </div>                                
                            </fieldset>                            
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Usage<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->vehicle_usage}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Lease Or Purchase<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->lease_of_purchase}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Finance Company / Lessor Name<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->finance_company_name}} </p>
                                 </div>
                            </div>
                            <fieldset class="custom_fieldset">
                              <legend> Finance Company / Lessor Address </legend>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> Address-1 <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->vehicle_lessor_address_1}}  </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> Address-2 <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->vehicle_lessor_address_2}}  </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> City <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->vehicle_lessor_city}} </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> State <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->vehicle_lessor_state}}  </p>
                                   </div>
                              </div>
                              <div class="row preview_input_holder_rows">
                                   <label class="col-sm-4 col-form-label"> Zip <span> : </span></label>
                                   <div class="col-sm-8" style="margin-top: 2px; ">
                                   <p> {{$data->vehicle_lessor_zip}} </p>
                                   </div>
                              </div>                                
                            </fieldset>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Finance Company/Lessor Phone Or Email For EP <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->finance_company_phone_email}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Additional <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->additional}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Submitted By<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->submitted_by}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Date<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->date}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Send Email To<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->send_mail_to}} </p>
                                 </div>
                            </div>


                          </div>
                        </div>                          
                      </div>
                    </div>
                  </div>
                @endif


                @if($type == 'driver')
                  <div class="card-body col-md-12 mx-auto increased_margin_t_cardbody">
                    <div class="row preview_row">
                      <div class="col-md-12 mx-auto insurance_padding">
                        <div class="card preview_card">
                          <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                              <i class="material-icons">assignment</i>
                            </div>
                            <h4 class="card-title">Submitted Data</h4>
                            <div class="row preview_req_id_badge_row">
                              <div class="col-md-4 wrap">
                                <div class="preview_req_id_badge text-right">
                                  <div class="timeline-heading">
                                    <h4 class="badge badge-pill service_add">Request Id : DR-{{$data->id}} </h4>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card-body">
                            <div class="row preview_checkbox_row">
                              <div class="col-md-12 text-center">
                                <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                    <input class="form-check-input insurance_check" type="checkbox" @if($data->request == 'add') checked @endif value="add" disabled>Add
                                    <span class="form-check-sign">
                                      <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                    <input class="form-check-input insurance_check" type="checkbox" @if($data->request == 'delete') checked @endif value="delete" disabled>Delete
                                    <span class="form-check-sign">
                                      <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                                <div class="form-check form-check-inline">
                                  <label class="form-check-label">
                                    <input class="form-check-input insurance_check" type="checkbox" @if($data->request == 'amend') checked @endif value="amend" disabled>Amend
                                    <span class="form-check-sign">
                                      <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                              </div>
                            </div>

                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Company Name<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->company_name}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Date Of Change<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->date_of_change}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Full Driver Name<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->driver_first_name}} {{$data->driver_last_name}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Date of Birth<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->date_of_birth}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">License State <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->license_state}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">License # <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->license}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Year's Of Experience <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->years_of_experience}} YRS </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Year's Of CDL Experience <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->years_of_cdl_experience}} YRS </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Duties<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->duties}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Date Of Hire <span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->date_of_hire}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Additional<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->additional}} </p>
                                 </div>
                            </div>

                          @if(!$files->isEmpty())
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Mvr File<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 18px; ">
                                 @foreach($files as $rowdata)
                                  <a href="{{$rowdata->url}}" style="margin-right: 10px;text-decoration: none; cursor: pointer;">{{$rowdata->name}}</a>
                                  @endforeach
                                 </div>
                            </div>
                          @endif

                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Submitted By<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->submitted_by}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Date<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->date}} </p>
                                 </div>
                            </div>
                            <div class="row preview_input_holder_rows">
                                 <label class="col-sm-4 col-form-label">Send Email To<span> : </span></label>
                                 <div class="col-sm-8" style="margin-top: 2px; ">
                                 <p> {{$data->send_mail_to}} </p>
                                 </div>
                            </div>


                          </div>
                        </div>                          
                      </div>
                    </div>
                  </div>
                @endif



                @if($type == 'subcontractor_agreement')              
                  <div class="card-body show_agreement_card_body">
                    <form id="regForm" method="post" action="{{ route('insurance.store') }}" class="sub_agreement_form" enctype="multipart/form-data">
                    
                    @csrf
                    <input type="hidden" name="tab" value="subcontractor_agreement">
                    <input type="hidden" class="status" name="status" value="3">
                    <input type="hidden" name="p_key" value="{{$data->id}}">
                        <!-- Circles which indicates the steps of the form: -->
                      <div class="card subcontractor_agreement_card_section">

                          <div class="card-header card-header-rose card-header-icon user_sub_card" id="top_subcontracor_card">
                              <div class="card-icon">
                                <i class="material-icons">people_outline</i>
                              </div>
                              <h4 class="card-title">Subcontractor Agreement</h4>

                            <div class="col-md-12 text-center">
                              <div class="credit_app_id_badge text-right agreement_badge">
                                <div class="timeline-heading">
                                  <h4 class="badge badge-pill"> SC-{{$data->id}} </h4>
                                </div>
                              </div>
                            @if($data->status == 3)
                              <span class="text-info agreement_pdf_download_span">
                                <a target="_blank" href="{{ route('agreementPdf', [$data->id, 'type' => 'subcontractor'] ) }}" class="btn btn-link btn-twitter">
                                  <i class="material-icons">save_alt</i>
                                Subcontractor Agreement PDF
                                </a>  
                              </span> 
                            @endif
                            </div>

                          </div>
                        <!-- <div class="card-header card-header-rose card-header-icon">
                          <div class="card-icon">
                            <i class="material-icons">assignment</i>
                          </div>
                          <h4 class="card-title">Subcontractor Agreement</h4>
                        </div> -->
                        <div class="card-body subcontractor_agreement_card_body">
                          <div class="row">
                            <div class="col-md-12 text-center">
                              <h2 class="main-title">SUBCONTRACTOR AGREEMENT</h2>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-1 progress_container" style="text-align:center;margin-bottom:20px;">
                              <span class="step step-first active">
                                <i class="material-icons">done</i>
                              </span>
                              <span class="step step-inner">
                                <i class="material-icons">done</i>
                              </span>
                              <span class="step step-inner">
                                <i class="material-icons">done</i>
                              </span>
                              <span class="step step-inner">
                                <i class="material-icons">done</i>
                              </span>
                              <span class="step step-inner">
                                <i class="material-icons">done</i>
                              </span>
                              <span class="step step-last">
                                <i class="material-icons">done</i>
                              </span>
                              <span class="step">
                                <i class="material-icons">done</i>
                              </span>
                              <span class="step">
                                <i class="material-icons">done</i>
                              </span>
                            </div>
                            <div class="col-md-11 content_container">
                              <div class="tab">
                                <div class="row input_row">
                                  <div class="col-md-12">
                                    <div>
                                      <span>This Subcontractor Agreement (“Agreement”) is entered into and shall be effective as of the</span>
                                      <span>
                                        <div class="form-group bmd-form-group day day-month-year">
                                          <input type="number" class="form-control form_input_field form_fields field_to_fill" id="sub_agree_day" name="day" placeholder="Day" min="1" max="31" value="{{$data->day}}">
                                        </div>
                                      </span>
                                      <span>day of</span> 
                                      <span>

                                        <div class="input_wrapper custom_select2_col month">
                                          <div class="form-group custom-form-select">
                                            <select class="custom-select contractor_select select2 form_input_field field_to_fill" id="subContractoMonth " name="month">
                                              <option class="form-select-placeholder"></option>
                                              <option value="january" @if($data->month == 'january') selected @endif>January</option>
                                              <option value="february" @if($data->month == 'february') selected @endif>February</option>
                                              <option value="march" @if($data->month == 'march') selected @endif>March</option>
                                              <option value="april" @if($data->month == 'april') selected @endif>April</option>
                                              <option value="may" @if($data->month == 'may') selected @endif>May</option>
                                              <option value="june" @if($data->month == 'june') selected @endif>June</option>
                                              <option value="july" @if($data->month == 'july') selected @endif>July</option>
                                              <option value="august" @if($data->month == 'august') selected @endif>August</option>
                                              <option value="september" @if($data->month == 'september') selected @endif>September</option>
                                              <option value="october" @if($data->month == 'october') selected @endif>October</option>
                                              <option value="october" @if($data->month == 'october') selected @endif>November</option>
                                              <option value="december" @if($data->month == 'december') selected @endif>December</option>
                                            </select>
                                            <div class="form-element-bar">
                                            </div>
                                            <label class="form-element-label" for="contractor_select input_label">Enter a month</label>
                                          </div>
                                        </div>
                                      </span>
                                      <span>
                                        <div class="form-group bmd-form-group year day-month-year">
                                          <input type="number" class="form-control form_input_field form_fields field_to_fill" id="sub_agree_year" name="year" placeholder="Year *" value="{{$data->year}}">
                                      </div>
                                      </span>
                                      <span>
                                         (the “Effective Date”), by and among EV Oilfield Services, LLC (“Contractor”), and
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <fieldset class="custom_fieldset_2 contractor_fieldset">
                                  <legend>Contact Form</legend>
                                  <div class="row input_row">
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_name" class="bmd-label-floating input_label">Subcontractor *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="subcontractor_name" name="subcontractor_name" value="{{$data->subcontractor_name}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper custom_select2_col">
                                      <div class="form-group custom-form-select">
                                        <select class="custom-select contractor_select select2 form_input_field field_to_fill" id="contractor_select" name="type_of_entity">
                                          <option class="form-select-placeholder"></option>
                                          <option value="corporation" @if($data->type_of_entity == 'corporation') selected @endif>Corporation</option>
                                          <option value="llc" @if($data->type_of_entity == 'llc') selected @endif>LLC</option>
                                          <option value="limited_partnership" @if($data->type_of_entity == 'limited_partnership') selected @endif>Limited Partnership</option>
                                          <option value="sole_proprietorship" @if($data->type_of_entity == 'sole_proprietorship') selected @endif>Sole Proprietorship</option>
                                        </select>
                                        <div class="form-element-bar">
                                        </div>
                                        <label class="form-element-label" for="contractor_select input_label">Type of Entity *</label>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="mailing_addr" class="bmd-label-floating input_label">Mailing Address *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="mailing_addr" name="mailing_addr" value="{{$data->mailing_addr}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="physical_addr" class="bmd-label-floating input_label">Physical Address (if different from mailing Address) *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="physical_addr" name="physical_addr" value="{{$data->physical_addr}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contact_person" class="bmd-label-floating input_label">Contact Person *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="contact_person" name="contact_person" value="{{$data->contact_person}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contact_person_title" class="bmd-label-floating input_label">Title *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="contact_person_title" name="contact_person_title" value="{{$data->contact_person_title}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contact_person_email" class="bmd-label-floating input_label">Email *</label>
                                          <input type="email" class="form-control form_input_field form_fields field_to_fill" id="contact_person_email" name="contact_person_email" value="{{$data->contact_person_email}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contact_person_phone" class="bmd-label-floating input_label">Phone *</label>
                                          <input type="text" class="form-control form_input_field form_fields mobile field_to_fill" id="contact_person_phone" name="contact_person_phone" value="{{$data->contact_person_phone}}">
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contact_person_fax" class="bmd-label-floating input_label">Fax *</label>
                                          <input type="text" class="form-control form_input_field form_fields mobile field_to_fill" id="contact_person_fax" name="contact_person_fax" value="{{$data->contact_person_fax}}">
                                        </div>
                                    </div> -->
                                  </div>
                                </fieldset>
                                <div class="row agrement_tab_content">
                                  <div class="col-md-12">
                                    <p>Contractor and Subcontractor may be individually referred to herein as a “Party” or collectively as the “Parties.” </p>
                                  </div>
                                </div>
                                <div class="row agrement_tab_content">
                                  <div class="col-md-12">
                                    <h4 class="title">CONSPICUOUS AND FAIR NOTICE</h4>
                                    <!-- <hr class="center-diamond">-->                                    
                                    <p>Both parties represent to each other that --</p>
                                    <ul class="subcontractor_agre_list bold">
                                      <li>
                                        They have consulted an attorney concerning this agreement or, if they have not consulted an attorney, they were given the opportunity and had the ability to consult, but made an informed decision not to do so, and
                                      </li>
                                      <li>
                                        They fully understand their rights and obligations under this agreement.
                                      </li> 
                                    </ul>
                                    <p>Now, Therefore, in consideration of the mutual promises, conditions and agreements herein contained, the sufficiency of which is acknowledged, Contractor and Subcontractor hereby agree as follows:</p>
                                  </div>
                                </div>
                              </div>

                              <div class="tab">
                                <div class="row agrement_tab_content">
                                  <div class="col-md-12">
                                    <div class="section_head_content">
                                      <h4 class="sub-title">1. Scope of Agreement</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>
                                        Work for Hire. It is contemplated that from time to time Subcontractor will be requested by Contractor or its present or future affiliated entities to perform certain work and services (“Work”) .<span class="dots">...</span> <span class="more">Neither Contractor nor its affiliates shall be obligated to request Subcontractor to perform any Work, and Subcontractor shall not be obligated to accept requests to perform Work from either Contractor or its affiliates, but it is expressly understood and agreed that any and all Work requested by Contractor or its affiliates and accepted by Subcontractor shall be controlled and governed by the provisions of this Agreement. If and when Contractor desires Subcontractor to perform Work hereunder, Contractor will issue a Work order describing the Work Contractor desires Subcontractor to perform and the compensation Contractor will pay Subcontractor for the Work described in the Work order.  Subcontractor shall notify Contractor in writing within five (5) business days of Subcontractor’s receipt of such Work order if Subcontractor intends to accept the Work order. The term “Agreement” means this Agreement as incorporated in a work order and the term “Contractor” as used herein shall mean the Contractor or affiliated entity that issued the work order.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>

                                    <div class="section_head_content">
                                       <h4 class="sub-title">2. Term and Termination</h4>
                                       <!-- <hr class="center-diamond"> -->
                                       <p>This Agreement shall remain in effect for a period of one year from the Effective Date, and from year to year thereafter, subject to the right of either party here to cancel or terminate the Agreement at any time upon not less than thirty (30) days written notice of one party to the other.</p>
                                    </div>

                                    <div class="section_head_content">
                                      <h4 class="sub-title">3. Representations and Warranties</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>Subcontractor will furnish all necessary materials, equipment, permits, and certificates that are required for the Work. All of Subcontractor’s materials and equipment are suitable for their intended use, and are free from all faults and defects. Subcontractor will remove and replace any defective materials <span class="dots">...</span><span class="more"> or Work forthwith on notice from Contractor.Subcontractor will perform the Work entirely at Subcontractor’s risk. Subcontractor will provide all proper and sufficient and necessary safeguards against all injuries and damage whatsoever, and to comply with all safety requirements imposed by law. Subcontractor shall conform to Contractor’s reasonable progress schedule. Subcontractor shall perform the Work in a professional, prompt, and diligent manner, consistent with applicable law and industry standards, without delaying or hindering Contractor’s work. If Subcontractor shall default in performance of the work or otherwise commit any act which causes delay to Contractor’s work, Subcontractor shall be liable for all losses, costs, expenses, liabilities and damages, including actual damages, consequential damages and any liquidated damages sustained by Contractor. Subcontractor will comply with all applicable federal, state and local laws, codes, ordinances, rules, regulations, orders and decrees of any government or quasi-government entity having jurisdiction over the project, the project site, the practices involved in the Work, or any subcontract work. Subcontractor, its agents and employees are properly trained and qualified to complete the Work for which they have been hired to do.</span>
                                      <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>

                                    <div class="section_head_content">
                                      <h4 class="sub-title">4. Indemnification</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>
                                        “Claims” shall include, without limitation, any and all claims, losses, damages (including, without limitation, punitive damages and damages for bodily injury, death or property damage ), causes of action, fines, penalties, enforcement proceedings, suits, and liabilities of every nature or character (including interest and all expenses of litigation, court costs, and attorneys' fees), <span class="dots">...</span><span class="more">whether or not arising in tort, contract, strict liability, under statute, or of any other character whatsoever, and whether or not caused by a legal duty. “Subcontractor Group” means subcontractor, its parent, subsidiary and affiliated companies, and their contractors (of whatever tier), and its and their respective directors, officers, employees, agents, and representatives. “Contractor Group” means Contractor, its parent, subsidiary and affiliated companies, its and their co-lessees, partners, joint ventures, co-owners, contractors (other than Subcontractor), and its and their respective directors, officers, employees, agents, and representatives.Subcontractor shall defend, indemnify, and hold harmless the Contractor Group for any Claims which any or all of the Contractor Group may incur in connection with any third party Claim arising out of or relating to the Subcontractor Group’s (or any member of the Subcontractor Group’s) actual or alleged negligence, willful misconduct, breach of this Agreement or violation of Law.  The foregoing indemnification obligation includes any Claims asserted against the Contractor Group (or any member thereof) arising from pollution or contamination which originates above the surface of the land or water from spills of fuels, lubricants, motor oils, pipe dope, paints, solvents, cleaning solutions or other liquids, fumes and garbage which is in any manner associated with or alleged to have been associated with, resulting from or caused by Subcontractor Group’s Work, equipment or personnel </span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="tab">
                                <div class="row agrement_tab_content padding-content">
                                  <div class="col-md-12">
                                    <div class="section_head_content">
                                      <h4 class="sub-title">5. Insurance</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>
                                        Subcontractor, at its own cost and expense, shall procure, carry, and maintain insurance coverage satisfactory to Contractor, which shall include not less than the following coverage:
                                      </p>
                                      <ul class="subcontractor_agre_list evos_field">
                                        <li>General liability insurance with limits not less than $
                                          <span>
                                            <div class="form-group bmd-form-group insurance_limit_currency">
                                              <input type="text" class="form-control form_input_field form_fields required" id="general_liability_limit" name="general_liability_limit" placeholder="" value="{{$data->general_liability_limit}}">
                                          </div>
                                          </span>
                                        per occurrence.</li>
                                        <li>Automobile Liability insurance with limits not less than $
                                        <span>
                                            <div class="form-group bmd-form-group insurance_limit_currency">
                                              <input type="text" class="form-control form_input_field form_fields required" id="automobile_liability_limit" name="automobile_liability_limit" placeholder="" value="{{$data->automobile_liability_limit}}">
                                          </div>
                                        </span>
                                        per occurrence.</li>
                                        <li>Umbrella insurance with limits not less than $
                                        <span>
                                          <div class="form-group bmd-form-group insurance_limit_currency">
                                            <input type="text" class="form-control form_input_field form_fields required" id="umbrella_limit" name="umbrella_limit" placeholder="" value="{{$data->umbrella_limit}}">
                                          </div>
                                        </span>
                                        per occurrence.</li>
                                        <li>Workers Compensation and Employers’ Liability with limits not less than
                                        <span>
                                          <div class="form-group bmd-form-group insurance_limit_currency">
                                            <input type="text" class="form-control form_input_field form_fields required" id="employer_liability_limit" name="employer_liability_limit" placeholder="" value="{{$data->employer_liability_limit}}">
                                          </div>
                                        </span>                                            
                                        </li>
                                        <li>Riggers Liability Insurance with limits not less than 
                                        <span>
                                          <div class="form-group bmd-form-group insurance_limit_currency">
                                            <input type="text" class="form-control form_input_field form_fields required" id="riggers_liability_limit" name="riggers_liability_limit" placeholder="" value="{{$data->riggers_liability_limit}}">
                                          </div>
                                        </span> 
                                        per occurrence.</li>
                                        <li>Motor Trade Cargo Insurance with limits not less than
                                        <span>
                                          <div class="form-group bmd-form-group insurance_limit_currency">
                                            <input type="text" class="form-control form_input_field form_fields required" id="motor_trade_limit" name="motor_trade_limit" placeholder="" value="{{$data->motor_trade_limit}}">
                                          </div>
                                        </span> 
                                        per occurrence.</li>
                                        <li>Installation Floater Insurance with limits not less than 
                                        <span>
                                          <div class="form-group bmd-form-group insurance_limit_currency">
                                            <input type="text" class="form-control form_input_field form_fields required" id="installation_floter_limit" name="installation_floater_limit" placeholder="" value="{{$data->installation_floater_limit}}">
                                          </div>
                                        </span>
                                        per occurrence.</li>
                                        <li>Any other insurance required by Contractor.</li>
                                      </ul>
                                    </div>
                                    <div class="section_head_content">
                                      <p>Subcontractor shall name Contractor as an additional insured on all policies of insurance. Subcontractor shall cause its insurance carrier to forward forthwith to Contractor a standard certificate of insurance such certificate shall require the insurance carrier to give Contractor written thirty (30) days prior notice of the cancellation <span class="dots">...</span><span class="more"> of such insurance. Subcontractor shall not perform any work under the Agreement until a Certificate of Insurance has been delivered to Contractor. If Subcontractor fails to furnish current evidence upon demand of any insurance required hereunder, or if any insurance is cancelled or materially changed, Contractor may suspend or terminate this Agreement until insurance is obtained. The insurance coverage required herein shall in no way limit the Subcontractor’s indemnification obligations or other  liability under this Agreement.</span>
                                      <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">6. Waiver of Subrogation</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>
                                        Subcontractor hereby waives any right of subrogation that it, or anyone claiming through or under it, may have against contractor, its agents, employees, or anyone for <span class="dots">...</span><span class="more"> whom blakely construction company, inc. may be responsible, for any loss or damage shall have been caused by the fault or negligence of contractor. However, this waiver shall not adversely or impair any policies of insurance or prejudice the right of either party to recover thereunder. Subcontractor shall provide an endorsement from subcontractor’s subcontractor or subcontractors that any right of subrogation is waived as against contractor and that such waiver will not impair subcontractor's policies of insurance. Any failure or delay by contractor to require such an endorsement shall not constitute a waiver, nor shall it otherwise affect this mutual waiver of subrogation.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button>  
                                      </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">7. Independent Contractor</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>
                                        Subcontractor shall have the status of an "independent contractor". Subcontractor shall employ and direct all persons engaged in the performance<span class="dots">...</span> <span class="more"> of any and all of its services under this Agreement, and such agents, servants or employees are subject to the sole control and direction of Subcontractor, and shall not be agents, servants or employees of Contractor. Subcontractor’s agents, contractors and employees shall not be entitled to any compensation, benefits or other remuneration from Contractor. Contractor shall not have any authority to supervise or direct the manner in which Subcontractor shall perform the services to be rendered hereunder. Subcontractor shall provide and be responsible for providing, using and maintaining all equipment, machinery and tools necessary to perform the Work.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>


                              <div class="tab">
                                <div class="row agrement_tab_content">
                                  <div class="col-md-12">
                                    
                                    <div class="section_head_content">
                                      <h4 class="sub-title">8. Notices</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>
                                        All notices and other communications pertaining to this Agreement shall be in writing and shall be deemed to have been given by a party hereto <span class="dots">...</span> <span class="more">if personally delivered to the other party or if sent by certified mail, return receipt requested, postage prepaid. All notices shall be addressed as shown below</span>
                                         <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    
                                  </div>
                                </div>
                                <fieldset class="custom_fieldset_2 contractor_fieldset evos_field">
                                  <legend>Contractor Form</legend>
                                  <div class="row input_row">
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_name_2" class="bmd-label-floating input_label">Contractor</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="contractor_name_2" name="notice_contractor_name" value="{{$data->notice_contractor_name}}">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_address" class="bmd-label-floating input_label">Address</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="contractor_address" name="notice_contractor_address" value="{{$data->notice_contractor_address}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_phone" class="bmd-label-floating input_label">Phone</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="contractor_phone" name="notice_contractor_phone" value="{{$data->notice_contractor_phone}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_email" class="bmd-label-floating input_label">Email</label>
                                          <input type="email" class="form-control form_input_field form_fields" id="contractor_email" name="notice_contractor_email" value="{{$data->notice_contractor_email}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_attn" class="bmd-label-floating input_label">Attn</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="contractor_attn" name="notice_contractor_attn" value="{{$data->notice_contractor_attn}}">
                                        </div>
                                    </div>
                                  </div>
                                </fieldset>
                                <fieldset class="custom_fieldset_2 contractor_fieldset">
                                  <legend>Subcontractor Form</legend>
                                  <div class="row input_row">
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_name_2" class="bmd-label-floating input_label">Subcontractor Name *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="subcontractor_name_2" name="notice_subcontractor_name" value="{{$data->notice_subcontractor_name}}">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_address" class="bmd-label-floating input_label">Address *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="subcontractor_address" name="notice_subcontractor_address" value="{{$data->notice_subcontractor_address}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_phone" class="bmd-label-floating input_label">Phone *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill mobile" id="subcontractor_phone" name="notice_subcontractor_phone" value="{{$data->notice_subcontractor_phone}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_email" class="bmd-label-floating input_label">Email *</label>
                                          <input type="email" class="form-control form_input_field form_fields field_to_fill" id="subcontractor_email" name="notice_subcontractor_email" value="{{$data->notice_subcontractor_email}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_contact" class="bmd-label-floating input_label">Contact *</label>
                                          <input type="text" class="form-control form_input_field form_fields mobile field_to_fill" id="subcontractor_contact" name="notice_subcontractor_contact" value="{{$data->notice_subcontractor_contact}}">
                                        </div>
                                    </div>
                                  </div>
                                </fieldset>
                              </div>


                              <div class="tab">
                                
                                <div class="row agrement_tab_content">
                                  <div class="col-md-12">
                                    <div class="section_head_content">
                                      <h4 class="sub-title">9. Breach of Agreement</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>
                                        If Subcontractor breaches any of the terms of this agreement, then Contractor has the option, at its sole and absolute discretion, to terminate this agreement immediately, without written notice.<span class="dots">...</span> <span class="more">Failure of Contractor to insist upon Subcontractor’s performance under this Agreement or to exercise any right or privilege shall not be a waiver of any of Contractor’s rights or privileges contained herein</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">10. Record Keeping</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>
                                        For the purposes of permitting verification by the Contractor of any amounts paid to or claimed by the Subcontractor, the Subcontractor shall keep and preserve, for not less than two years from date of invoice all general<span class="dots">...</span> <span class="more">ledgers, work orders, receipts, disbursements journals, bids, bid proposals, price lists, and supporting documentation obtained from manufacturers and suppliers in connection with the Work performed.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">11. Reports</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>
                                        Subcontractor shall provide to Contractor an oral report, confirmed in writing as soon as practicable, of all accidents or occurrences resulting in death or injuries to Subcontractor’s employees, subcontractors, or any third parties, damage<span class="dots">...</span> <span class="more">to Contractor’s property, or physical damage to the property of Contractor or any third party, arising out of or during the course of work to be performed. Subcontractor shall furnish Contractor with a copy of all reports made by Subcontractor to Subcontractor’s insurer, governmental authorities, or to others of such accidents or occurrences.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">12. Payment Terms</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p> Contractor agrees to pay Subcontractor for any work at the rates set forth in the applicable Work order. Subcontractor shall promptly invoice Contractor upon the completion of any Work, but in any <span class="dots">...</span> <span class="more">event shall invoice Contractor (i) at least once every thirty (30) days and (ii) within ten (10) days following completion of all of the Work under any applicable Work order or purchase order, and (iii) in the unlikely event that Work is completed without an applicable work order or purchase order, within ten (10) days of completion of such Work at Subcontractor’s best rates in the geographical area where the Work was completed. Contractor shall remit full payment of all undisputed portions of any and all invoices in full in United States funds by check, bank draft, money order, or bank/wire transfer payable to Contractor at its offices (or bank account) within sixty (60) days following actual receipt of Subcontractor’s invoice. In the event Contractor has a bona fide question or dispute concerning a Subcontractor invoice or a portion thereof, Contractor may withhold the disputed portion of the payment only. Contractor shall give written notice of any disputed amounts to Subcontractor specifying the reasons therefore within sixty (60) days after receipt of the applicable invoice. Invoices submitted later than one hundred twenty (120) days will not be considered for reimbursement.Contractor shall not pay or reimburse Subcontractor for any travel or other expenses unless Contractor approves such expenses in advance and in writing. </span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>

                                    <div class="section_head_content">
                                      <h4 class="sub-title">13. Audit</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>
                                        Subcontractor shall maintain, and shall cause any of Subcontractor’s Subcontractors to maintain, a true and correct set of records pertaining to all work performed under each work order, including supporting <span class="dots">...</span> <span class="more">documentation, for two (2) years following completion of the Work. Contractor may, at its expense require Subcontractor, or any of Subcontractor’s Subcontractors, at any time within said two-year period to furnish sufficient evidence, with documentary support, to enable Contractor to verify the correctness and accuracy of payments to Subcontractor or such Subcontractors. Contractor may, following written notice to Subcontractor or such Subcontractor, audit any and all records of Subcontractor and any Subcontractor relating to the work performed by or on behalf of Subcontractor hereunder, and all payments made in regard thereto, in order to verify the accuracy and compliance with this provision; provided however, Contractor shall not have the right to inspect or audit Subcontractor’s trade secrets or any proprietary information. If Contractor’s examination discloses that Subcontractor’s invoices to Contractor were in error, Subcontractor will immediately pay to Contractor any amounts overpaid by Contractor, plus interest from the date of the error at the lesser of one percent (1%) per month or the maximum rate allowed by law.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    
                                  </div>
                                </div>
                              </div>


                              <div class="tab">
                                
                                <div class="row input_row agrement_tab_content">
                                  <div class="col-md-12">
                                    
                                    
                                    <div class="section_head_content">
                                      <h4 class="sub-title">14. No delay</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>
                                        In no case (unless otherwise directed in writing by the Contractor) shall any claim or dispute, whether or not involving litigation, permit the Subcontractor to delay or discontinue any of the work hereunder, but rather the <span class="dots">...</span> <span class="more">Subcontractor shall diligently pursue the work hereunder while awaiting the resolution of any dispute or claim; provided, however, that the Contractor shall not withhold, pending the resolution of any claim or dispute, the payment of any undisputed sum otherwise due Subcontractor under this Agreement.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">15. Drugs, Alcohol & Firearms</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>
                                        To help ensure a safe, productive work environment, Contractor prohibits the use, transportation and possession of firearms, drugs and/or controlled substances, drug paraphernalia and alcoholic beverages. Illegal drugs shall include, but not be limited <span class="dots">...</span> <span class="more">to, marijuana, amphetamines, barbiturates, opiates, cocaine, codeine, morphine, hallucinogenic substances (LSD) and any similar drugs and/or chemical synthetics deemed hazardous by Contractor. Such prohibitions shall apply to Subcontractor and its employees, agents, servants and subcontractors. Subcontractor’s employees, agents, contractors and invitees shall abide the more stringent of Subcontractor’s or Contractor’s drug, alcohol and firearm policy. Contractor may request that Subcontractor carry out drug and alcohol tests of its employees and/or that Subcontractor carry out reasonable searches of individuals, their personal effects and vehicles when entering on and leaving assigned premises at any time, at scheduled times, or at random. Individuals found in violation shall be removed from the premises by Subcontractor immediately. Submission to such a search is strictly voluntary, however, refusal may be cause for not allowing that individual on the well site or Contractor’s other premises. Subcontractor shall (1) test at Subcontractor’s expense any individual involved in or related to an accident or injury within twelve (12) hours of such accident or injury and (2) submit to Contractor any drug or alcohol test results for any individual involved in or related to an accident or injury on Contractor’s premises. It is Subcontractor’s responsibility to notify its employees, contractors, subcontractors, agents and invitees of this prohibition, the provisions of this paragraph and its enforcement and obtain any acknowledgement or release from any person in order to comply with this provision and applicable law.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">16. Miscellaneous</h4>
                                      <!-- <hr class="center-diamond"> -->
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">A. Governing Law</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>The laws of the State of Texas shall apply and govern the validity, interpretation, and performance of this Agreement. The Parties waive any right to trial by jury in any action arising out of or relating to this Agreement or the Parties’ relationship </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">B. Severability</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>If any part of this Agreement contravenes any applicable statutes, regulations, rules, or common law requirements, then, to the extent of and only to the extent of such contravention, such part shall be severed from this Agreement and deemed non-binding while all other parts of this Agreement shall remain binding.</p>
                                    </div>

                                  </div>
                                </div>
                              </div>

                              <div class="tab">
                                <div class="row input_row agrement_tab_content">
                                  <div class="col-md-12">
                                    <div class="section_head_content">
                                      <h4 class="sub-title">C. Amendment</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>This Agreement may not be modified or amended unless it is in writing and signed by both Parties. </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">D. Assignment</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>Neither Party shall assign all or any part of its rights or obligations under this Agreement without the prior written consent from the other Party. </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">E. Compliance with Laws</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>Subcontractor agrees to comply with all laws, statutes, codes, rules, and regulations, which are now or may hereafter become applicable to Work covered by this Agreement or arising out of the performance of such operations. </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">F. Headings</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>The headings, sub-headings, and other subdivisions of this Agreement are inserted for convenience only. The Parties do not intend them to be an aid in legal construction. </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">G. Authority to Sign and Bind</h4>
                                      <!-- <hr class="center-diamond"> -->
                                      <p>By their signatures below, Subcontractor represents that the person signing this Agreement has the authority to execute same on behalf of Subcontractor and to bind Subcontract to the obligations set forth herein. </p>
                                    </div>

                                  </div>
                                </div>
                              </div>

                              <div class="tab">
                                <div class="row input_row">
                                  <div class="col-md-12">
                                    <div>
                                      <span>Executed this</span>
                                      <span>
                                        <div class="form-group bmd-form-group day day-month-year">
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="sub_agree_day_2" name="executed_day" placeholder="Day" value="{{$data->executed_day}}" max="31">
                                        </div>
                                      </span>
                                      <span>day of</span> 
                                      <span>
                                        <div class="input_wrapper custom_select2_col month">
                                          <div class="form-group custom-form-select">
                                            <select class="custom-select contractor_select select2 form_input_field is-filled field_to_fill" id="subContractoMonth2" name="executed_month">
                                            <option class="form-select-placeholder"></option>
                                            <option value="january" @if($data->executed_month == 'january') selected @endif>January</option>
                                                <option value="february" @if($data->executed_month == 'february') selected @endif>February</option>
                                                <option value="march" @if($data->executed_month == 'march') selected @endif>March</option>
                                                <option value="april" @if($data->executed_month == 'april') selected @endif>April</option>
                                                <option value="may" @if($data->executed_month == 'may') selected @endif>May</option>
                                                <option value="june" @if($data->executed_month == 'june') selected @endif>June</option>
                                                <option value="july" @if($data->executed_month == 'july') selected @endif>July</option>
                                                <option value="august" @if($data->executed_month == 'august') selected @endif>August</option>
                                                <option value="september" @if($data->executed_month == 'september') selected @endif>September</option>
                                                <option value="october" @if($data->executed_month == 'october') selected @endif>October</option>
                                                <option value="october" @if($data->executed_month == 'october') selected @endif>November</option>
                                                <option value="december" @if($data->executed_month == 'december') selected @endif>December</option>
                                            </select>
                                            <div class="form-element-bar">
                                            </div>
                                            <label class="form-element-label" for="contractor_select input_label">Enter a month</label>
                                          </div>
                                        </div>
                                      </span>
                                      <span>
                                        <div class="form-group bmd-form-group year day-month-year">
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="sub_agree_year_2" name="executed_year" placeholder="Year" value="{{$data->executed_year}}">
                                      </div>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <fieldset class="custom_fieldset_2 contractor_fieldset evos_field">
                                  <legend>Contractor</legend>
                                  <div class="row input_row">
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="Contractor_name_3" class="bmd-label-floating input_label">Contractor</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="Contractor_name_3" name="executed_contractor_name" value="{{$data->executed_contractor_name}}">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_by" class="bmd-label-floating input_label">Contractor By</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="contractor_by" name="executed_contractor_by" value="{{$data->executed_contractor_by}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_title" class="bmd-label-floating input_label">Title</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="contractor_title" name="executed_contractor_title" value="{{$data->executed_contractor_title}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_date" class="bmd-label-floating input_label">Date *</label>
                                          <input type="text" class="form-control form_input_field form_fields datepicker" id="contractor_date" name="executed_contractor_date" value="{{$data->executed_contractor_date}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_initial" class="bmd-label-floating input_label">Contractor Initial *</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="contractor_initial" name="contractor_initial" value="{{$data->contractor_initial}}">
                                        </div>
                                    </div>
                                    
                                  </div>
                                </fieldset>

                                <fieldset class="custom_fieldset_2 contractor_fieldset">
                                  <legend>Sub Contractor</legend>
                                  <div class="row input_row">
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="Subcontractor_name_3" class="bmd-label-floating input_label">Sub Contractor *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="Subcontractor_name_3" name="executed_subcontractor_name" value="{{$data->executed_subcontractor_name}}">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_by" class="bmd-label-floating input_label">Sub Contractor By *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="subcontractor_by" name="executed_subcontractor_by" value="{{$data->executed_subcontractor_by}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_title" class="bmd-label-floating input_label">Title *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="subcontractor_title" name="executed_subcontractor_title" value="{{$data->executed_subcontractor_title}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_date" class="bmd-label-floating input_label">Date *</label>
                                          <input type="text" class="form-control form_input_field form_fields datepicker field_to_fill field_to_fill_validator" id="subcontractor_date" name="executed_subcontractor_date" value="{{$data->executed_subcontractor_date}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_initial" class="bmd-label-floating input_label">Subcontractor Initial *</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="subcontractor_initial" name="subcontractor_initial" value="{{$data->subcontractor_initial}}">
                                        </div>
                                    </div>
                                    
                                  </div>
                                </fieldset>

                                <fieldset class="custom_fieldset_2 contractor_fieldset info_div necessary_files_fieldset">
                                  <legend> Necessary Files </legend>
                                  <div class="row input_row poins_row mb-3">
                                    <table>
                                      <td class="" style="padding-left:10px">
                                            <p class="necessary_files_head_p"> W9 </p>
                                            <hr>
                                            <div class="uploaded-file">
                                            <?php $i = 1; ?>
                                            @if(!$agreement_files->isEmpty())
                                            @foreach($agreement_files as $rowdata)
                                              <div> <a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}}</a> </div>
                                            <?php $i++; ?>
                                            @endforeach
                                            @else
                                            <span>No files Uploaded</span>
                                            @endif
                                            </div>
                                      </td>
                                      <td style="border-left:2px solid gray; padding-left:10px">
                                            <p class="necessary_files_head_p"> Insurance Certificate </p>
                                            <hr>
                                            <div class="uploaded-file">
                                            <?php $i =1; ?>
                                            @if(!$insurance_files->isEmpty())
                                            @foreach($insurance_files as $rowdata)
                                              <div> <a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}}</a> </div>
                                            <?php $i++; ?>
                                            @endforeach
                                            @else
                                            <span>No files Uploaded</span>
                                            @endif
                                            </div>
                                      </td>
                                    </table>            
                                  </div>
                                </fieldset>

                                @if($sc_sign)
                                <div class="row input_row mt-2" style="margin-bottom: 15px;">
                                  <div class="col-md-12 text-center">
                                      <b>Signature</b>
                                      <br/>
                                      <div id="sig" style="border: 2px solid #DDD">
                                        <img src="{{$sc_sign->url}}">
                                      </div>
                                  </div>
                                </div>
                                @else
                                  <div class="row input_row mt-2" style="margin-bottom: 15px;">
                                    <div class="col-md-12 text-center">
                                        <b>No Signature Uploaded</b>
                                    </div>
                                  </div>
                                @endif

                              </div>



                              <div style="overflow:auto;" class="row msf_btn_holder">
                                <div class="col-md-4 save_btn_wrapper text-left" style="float:right;">
                                  <a class="btn btn-round btn-warning nextPrevBtnOne prev" type="button" href="#top_subcontracor_card" id="prevBtn" onclick="nextPrev(-1)">Previous</a>
                                </div>
                                <div class="col-md-8 save_btn_wrapper text-right">
                                  <a class="btn custom-btn-one btn-round btn-success nextPrevBtnOne next nextBtnAgreement" href="#top_subcontracor_card" type="button" id="nextBtn" onclick="nextPrev(1)">Next</a>

                                @if($data->status == 2)
                                <button class="btn btn-round btn-danger  nextPrevBtnOne submitBtn nextBtnAgreement" style="display: none" type="button" id="" onclick="" data-toggle="modal" data-target="#rejectModal">Reject</button>

                                <button class="btn custom-btn-one btn-round btn-success nextPrevBtnOne submitBtn nextBtnAgreement submit_to_insurance_btn" style="display: none" type="button" id="" onclick="" data-toggle="modal" data-target="#submitModal">Submit to Insurance</button>
                                @endif
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!-- Modal -->
                      <div class="modal fade bs_modal" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel"><u>Reason For The Rejection</u></h5>
                              <button type="button" class="close close_icon" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <textarea class="form-control summernote" style="min-width: 100%" rows="6" name="reject"></textarea>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary" name="submit" value="reject">Submit</button>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!-- Modal -->
                      <div class="modal fade bs_modal" id="submitModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel"><u>Submit to Insurance</u></h5>
                              <button type="button" class="close close_icon" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <fieldset class="custom_fieldset_2 contractor_fieldset mt-2 mb-3">
                                  <legend>Sending Information</legend>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                      <label for="agreement_send_mail_to" class="bmd-label-floating input_label">Send Mail To Insurance</label>
                                        <input type="email" class="form-control form_input_field form_fields" id="agreement_send_mail_to" name="submitted_to_insurance" value="elizabeth@mcanallywilkins.com" required="">
                                      </div>                                          
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                      <label for="agreement_send_mail_to" class="bmd-label-floating input_label">Send Mail To 3rd Party</label>
                                        <input type="email" class="form-control form_input_field form_fields" id="agreement_send_mail_to" name="agreement_send_mail_to" value="{{$data->agreement_send_mail_to}}" required="">
                                      </div>                                          
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                      <label for="agreement_send_mail_date" class="bmd-label-floating input_label">Date</label>
                                        <input type="text" class="datepicker form-control form_input_field form_fields" id="agreement_send_mail_date" name="submitted_date_insurance" value="{{date('m/d/Y')}}" required="">
                                      </div>                                          
                                    </div>
                                  </div>
                                </fieldset>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary" name="submit" value="submit_to_insurance">Submit</button>
                            </div>
                          </div>
                        </div>
                      </div>

                    </form>
                  </div>
                @endif


                @if($type == 'rental_agreement')
                  <div class="card-body show_agreement_card_body">
                    <form id="multiForm2" method="post" action="{{ route('insurance.store') }}" class="ren_agreement_form" enctype="multipart/form-data">
              
                    @csrf
                    <input type="hidden" name="tab" value="rental_agreement">
                    <input type="hidden" class="status" name="status" value="3">
                    <input type="hidden" name="p_key" value="{{$data->id}}">

                        <!-- Circles which indicates the steps of the form: -->
                      <div class="row">
                        <div class="col-md-12">
                          <div class="card rental_agreement_card">
                            <div class="card-header card-header-rose card-header-icon user_sub_card" id="top_rental_card">
                                <div class="card-icon">
                                  <i class="material-icons">people_outline</i>
                                </div>
                                <h4 class="card-title">Rental Agreement</h4>

                                <div class="col-md-12 text-center">
                                  <div class="credit_app_id_badge text-right agreement_badge">
                                    <div class="timeline-heading">
                                      <h4 class="badge badge-pill"> RN-{{$data->id}} </h4>
                                    </div>
                                  </div>
                                @if($data->status == 3)
                                  <span class="text-info agreement_pdf_download_span">
                                    <a target="_blank" href="{{ route('agreementPdf', [$data->id, 'type' => 'rental'] ) }}" class="btn btn-link btn-twitter">
                                      <i class="material-icons">save_alt</i>
                                    Rental Agreement PDF
                                    </a>  
                                  </span>
                                @endif
                                </div>

                            </div>

                            <div class="card-body">
                              <div class="row mb-2">
                                <div class="col-md-12 text-center">
                                  <h2>Equipment Rental Agreement</h2>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-1 progress_container" style="text-align:center;margin-bottom:20px;">
                                  <span class="stepRen step-first">
                                    <i class="material-icons">done</i>
                                  </span>
                                  <span class="stepRen step-inner">
                                    <i class="material-icons">done</i>
                                  </span>
                                  <span class="stepRen step-inner">
                                    <i class="material-icons">done</i>
                                  </span>
                                  <span class="stepRen step-inner">
                                    <i class="material-icons">done</i>
                                  </span>
                                  <span class="stepRen step-inner">
                                    <i class="material-icons">done</i>
                                  </span>
                                  <span class="stepRen step-inner">
                                    <i class="material-icons">done</i>
                                  </span>                                      
                                </div>
                                <div class="col-md-11 content_container rental_ag_content_container">
                                  <div class="tabRen">
                                    <div class="row">
                                      <div class="col-md-12 evos_field">
                                        <p> This Equipment Rental Agreement ( the “Agreement” ) is made and entered on 
                                            <input type="text" class="datepicker form-control ren_agreement_input input_ten form_input_field form_fields" name="entered_on" value="{{$data->entered_on}}" placeholder="Date"> ,
                                            <!-- <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_twenty" name="entered_by" value="{{$data->entered_by}}"> -->  by and between EV Oilfield Services, LLC (Lessor) and 
                                            <!-- <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_forty" name="entered_between_lessor" value="{{$data->entered_between_lessor}}"> (“Lessor”) and -->
                                            <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_forty" name="entered_between_lessee" value="{{$data->entered_between_lessee}}" placeholder="Company Name"> (“Lessee”) ( collectively referred to as the “Parties” ).
                                        </p>
                                      </div>
                                      <div class="col-md-12 agree_as_follows">
                                        <p> The Parties agree as follows:  </p>
                                      </div>
                                    </div>
                                    <div class="row points_row points_row_first">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 1. Equipment : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          Lessor hereby rents to Lessee the following equipment : 
                                        </p>
                                      </div>
                                      <div class="col-md-12 evos_field">
                                        <textarea class="form-control form_input_field form_fields" rows="4" name="ren_ag_lesse_equipment" id="ren_ag_lesse_equipment">{{$data->ren_ag_lesse_equipment}}</textarea>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 2. Rental Term : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12">
                                        <span>
                                          The rent will start on
                                        </span>
                                        <span>
                                          <div class="form-group bmd-form-group date_input_div evos_field">
                                            <input type="text" class="form-control form_input_field form_fields datepicker" id="ren_ag_start_date" name="ren_ag_start_date" placeholder="Begin Date" value="{{$data->ren_ag_start_date}}">
                                          </div>
                                        </span>
                                        <span>
                                          <!-- <input type="number" class="ren_agreement_input form-control form_input_field form_fields input_fifteen datepicker" placeholder="Begin Date"> -->
                                          and will end on
                                        </span>
                                        <span>
                                          <div class="form-group bmd-form-group date_input_div evos_field">
                                            <input type="text" class="form-control form_input_field form_fields datepicker" id="ren_ag_end_date" name="ren_ag_end_date" placeholder="End Date" value="{{$data->ren_ag_end_date}}">
                                          </div> 
                                          (Rental Term).
                                        </span>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 3. Rental Payments : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12 evos_field"> 
                                        <p>
                                          <div class="p_to_inline">
                                            <span>                                             
                                            Lessee agrees to pay to Lessor as rent for the Equipment the amount of $
                                            <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_fifteen" placeholder="" name="ren_ag_amount_rent" value="{{$data->ren_ag_amount_rent}}">
                                            (“Rent”) each 
                                            </span>
                                            <span>
                                              <div class="input_wrapper custom_select2_col rent_time">
                                                <div class="form-group custom-form-select">
                                                  <select class="custom-select contractor_select select2 form_input_field" id="" name="ren_ag_time_rent">
                                                    <option class="form-select-placeholder"></option>
                                                    <option value="day" @if($data->ren_ag_time_rent == 'day') selected @endif>Day</option>
                                                    <option value="week" @if($data->ren_ag_time_rent == 'week') selected @endif>Week</option>
                                                    <option value="month" @if($data->ren_ag_time_rent == 'month') selected @endif>Month</option>
                                                  </select>
                                                  <div class="form-element-bar">
                                                  </div>
                                                  <label class="form-element-label" for="contractor_select input_label">Choose Time</label>
                                                </div>
                                              </div>                                                  
                                              
                                            </span>
                                            <span>
                                              month in advance on the first day of each month at :
                                              <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_forty" placeholder="" name="ren_ag_address_rent" value="{{$data->ren_ag_address_rent}}"> ( address for rent payment )
                                              at any other address designated by Lessor. If the Rental Term does not start on the first
                                              day of the month or end on the last day of a month, the rent will be prorated accordingly.
                                            </span>
                                          </div>

                                        </p>
                                      </div>
                                    </div>                                        
                                  </div>

                                  <div class="tabRen">
                                    <div class="row points_row evos_field">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 4. Late Charges : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          If any amount under this Agreement is more than
                                          <input type="number" class="ren_agreement_input form-control form_input_field form_fields input_ten" placeholder="" name="ren_ag_late_day" value="{{$data->ren_ag_late_day}}"> days late, Lessee agrees to pay a late fee of $
                                          <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_fifteen" placeholder="" name="ren_ag_late_charge" value="{{$data->ren_ag_late_charge}}">  ( address for rent payment )

                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 5. Security Deposit : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          Prior to taking possession of the Equipment, Lessee shall deposit with Lessor, in trust, a security deposit of $
                                          <input type="text" class="ren_agreement_input form-control form_input_field form_fields input_ten currency_input" placeholder="" name="ren_ag_security_deposit" value="{{$data->ren_ag_security_deposit}}"> as security for the performance by Lessee of the terms under this Agreement and for any damages caused by <span class="dots"> . . . </span> <span class="more"> Lessee or Lessee’s agents to the Equipment during the Rental Term. Lessor may use part or all of the security deposit to repair any damage to Equipment caused by Lessee or Lessee’s agents. However, Lessor is not just limited to the security deposit amount and Lessee remains liable for any balance. Lessee shall not apply or deduct any portion of any security deposit from the last or any month's rent. Lessee shall not use or apply any such security deposit at any time in lieu of payment of rent. If Lessee breaches any terms or conditions of this Agreement, Lessee shall forfeit any deposit, as permitted by law.
                                           </span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button>                                               
                                        </p>

                                      </div>
                                    </div>                                        
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 6. Delivery : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12 evos_field_chk"> 
                                        <p> 
                                          Rental
                                          <span class="form-check form-check-inline check_one">
                                            <label class="form-check-label">
                                              <input class="form-check-input" name="responsible" type="checkbox" value="responsible" @if($data->responsible == 'responsible') checked @endif> Shall or
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </span>
                                          <span class="form-check form-check-inline check_two">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" name="responsible" value="not_responsible" @if($data->responsible == 'not_responsible') checked @endif>Shall Not
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </span>
                                           be responsible for all expenses and costs: i) at the beginning of the Rental Term, of shipping the Equipment to Lessee’s premises and ii) at the end of the Rental Term, of shipping the Equipment back to Lessor’s premises.
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 7. Defaults : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          If Lessee fails to perform or fulfill any obligation under this
                                          Agreement, Lessee shall be in default of this Agreement. Subject to any statute,
                                          ordinance or law to the contrary, Lessee shall have seven (7) days from the date of notice
                                          of default by Lessor to  <span class="dots"> . . . </span>
                                          <span class="more"> cure the default.
                                          In the event Lessee does not cure a default, Lessor may at Lessor’s option (a) cure such default and the cost of such action may be added to Lessee’s financial obligations under this Agreement; or (b) declare Lessee in default of the Agreement. If Lessee shall become insolvent, cease to do business as a going concern or if a petition has been filed by or against Lessee under the Bankruptcy Act or similar federal or state statute, Lessor may immediately declare Lessee in default of this Agreement. In the event of default, Lessor may, as permitted by law, re-take possession of the Equipment. Lessor may, at its option, hold Lessee liable for any difference between the Rent that would have been payable under this Agreement during the balance of the unexpired term and any rent paid by any successive lessee if the Equipment is re-let minus the cost and expenses of such reletting. In the event Lessor is
                                          unable to re-let the Equipment during any remaining term of this Agreement, after default
                                          by Lessee, Lessor may at its option hold Lessee liable for the balance of the unpaid rent under this Agreement if this Agreement had continued in force.
                                          </span>
                                          <button class="btn btn-link btn_read_more" type="button">Read More</button>
                                        </p>

                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 8. Possession And Surrender Of Equipment : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          Lessee shall be entitled to possession of the Equipment on the first day of the Rental Term. At the expiration of the Rental Term, Lessee shall surrender the Equipment to Lessor by delivering the Equipment to Lessor or Lessor’s agent in good condition and <span class="dots">. . .</span> 
                                          <span class="more">working order, ordinary wear and tear excepted, as it was at the commencement of the Agreement.
                                          </span>
                                          <button class="btn btn-link btn_read_more" type="button">Read More</button>
                                        </p>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="tabRen">
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 9. Use Of Equipment : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          Lessee shall only use the Equipment in a careful and proper manner and will comply with all laws, rules, ordinances, statutes and orders regarding the use, maintenance of storage of the Equipment.
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 10. Condition Of Equipment And Repair : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          Lessee or Lessee’s agent has inspected the Equipment and acknowledges that the Equipment is in good and acceptable condition.
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 11. Maintenance, Damage And Loss : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          Lessee will, at Lessee's sole expense, keep and maintain the Equipment clean and in good working order and repair during the Rental Term. In the event the Equipment is lost or damaged beyond repair, Lessee shall pay to Lessor the replacement cost <span class="dots">. . .</span> 
                                          <span class="more">of the Equipment; in addition, the obligations of this Agreement shall continue in full force and effect through the Rental Term.
                                          </span>
                                          <button class="btn btn-link btn_read_more" type="button">Read More</button>
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 12. Insurance : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          Lessee shall be responsible to maintain insurance on the Equipment with losses payable to Lessor against fire, theft, collision, and other such risks as are appropriate and specified by Lessor.  Upon request by Lessor, Lessee shall provide proof of such insurance.
                                        </p>
                                      </div>
                                    </div>                                        
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 13. Encumbrances, Taxes And Other Laws : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          Lessee shall keep the Equipment free and clear of any liens or other encumbrances, and shall not permit any act where Lessor’s title or rights may be negatively affected. Lessee shall be responsible for complying with and conforming to all laws and <span class="dots">. . .</span> 
                                          <span class="more">
                                            regulations relating to the possession, use or maintenance of the Equipment. Furthermore, Lessee shall promptly pay all taxes, fees, licenses and governmental charges, together with any penalties or interest thereon, relating to the possession, use or maintenance of the Equipment.
                                          </span>
                                          <button class="btn btn-link btn_read_more" type="button">Read More</button>
                                        </p>
                                      </div>
                                    </div>                       
                                  </div>

                                  <div class="tabRen">
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 14. Lessors Representations : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          Lessor represents and warrants that he/she has the right to rent the Equipment as provided in this Agreement and that Lessee shall be entitled to quietly hold and possess the Equipment, and Lessor will not interfere with that right as long as Lessee pays the Rent in a timely manner and performs all other obligations under this Agreement.
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 15. Ownership : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          The Equipment is and shall remain the exclusive property of Lessor.
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 16. Severability : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          If any part or parts of this Agreement shall be held unenforceable for any reason, the remainder of this Agreement shall continue in full force and effect. If any provision of this Agreement is deemed invalid or unenforceable by any court of competent jurisdiction, and if limiting such provision would make the provision valid, then such provision shall be deemed to be construed as so limited.
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 17. Assignment : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          Neither this Agreement nor Lessee’s rights hereunder are assignable except with Lessor’s prior, written consent.
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 18. Binding Effect : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          The covenants and conditions contained in the Agreement shall apply to and bind the Parties and the heirs, legal representatives, successors and permitted assigns of the Parties.
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row evos_field">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 19. Governing Law : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          This Agreement shall be governed by and construed in accordance with the laws of the State of
                                          <input type="text" class="form-control ren_agreement_input state_input form_input_field form_fields" name="ren_ag_governing_law" value="{{$data->ren_ag_governing_law}}">
                                        </p>
                                      </div>
                                    </div>                      
                                  </div>

                                  <div class="tabRen">
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 20. Notice : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          Any notice required or otherwise given pursuant to this Agreement shall be in writing and mailed certified return receipt requested, postage prepaid, or delivered by overnight delivery service to:
                                        </p>
                                      </div>
                                      <div class="col-md-6 evos_field">
                                        <p> Lessor : </p>
                                        <textarea class="form-control form_input_field form_fields" rows="3" name="lessor_agreement_notice" id="lessor_agreement">{{$data->lessor_agreement_notice}}</textarea>
                                      </div>
                                      <div class="col-md-6">
                                        <p> Lessee : </p>
                                        <textarea class="form-control form_input_field form_fields field_to_fill" rows="3" name="lesse_agreement_notice" id="lesse_agreement">{{$data->lesse_agreement_notice}}</textarea>
                                      </div>
                                      <div class="col-md-12">
                                        <p> Either party may change such addresses from time to time by providing notice as set forth above. </p>
                                      </div>                                         
                                    </div>                                         
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 21. Entire Agreement : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          This Agreement constitutes the entire agreement between the Parties and supersedes any prior understanding or representation of any kind preceding the date of this Agreement. There are no other promises, conditions, understandings or <span class="dots">...</span> 
                                          <span class="more">
                                            >other agreements, whether oral or written, relating to the subject matter of this Agreement. This Agreement may be modified in writing and must be signed by both Lessor and Lessee.
                                          </span>
                                          <button class="btn btn-link btn_read_more" type="button">Read More</button>
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 22. Cumulative Rights : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          Lessor’s and Lessee’s rights under this Agreement are cumulative, and shall not be construed as exclusive of each other unless otherwise required by law.
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 23. Waiver : </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          The failure of either party to enforce any provisions of this Agreement shall not be deemed a waiver or limitation of that party's right to subsequently enforce and compel strict compliance with every provision of this Agreement.
                                          <span class="dots"> . . </span>
                                          <span class="more">The acceptance of rent by Lessor does not waive Lessor’s right to enforce any provisions of this Agreement.
                                          </span>
                                          <button class="btn btn-link btn_read_more" type="button">Read More<div class="ripple-container"></div></button>
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 24. Indemnification :  </span> </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12"> 
                                        <p> 
                                          Except for damages, claims or losses due to Lessor’s acts or negligence, Lessee, to the extent permitted by law, will indemnify and hold Lessor and Lessor’s property, free and harmless from any liability for losses, claims, injury to or <span class="dots">. . .</span>
                                          <span class="more">death of any person, including Lessee, or for damage to property arising from Lessee using and possessing the Equipment or from the acts or omissions of any person or persons, including Lessee, using or possessing the Equipment with Lessee’s express or implied consent.
                                          </span>
                                          <button class="btn btn-link btn_read_more" type="button">Read More</button>
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row">
                                      <div class="col-md-12">
                                        <p><span class="point_head"> 25. Additional Terms & Conditions  </span> ( Specify “none” if there are no additional provisions ) : </p>
                                        <!-- <hr class="center-diamond"> -->
                                      </div>
                                      <div class="col-md-12 evos_field">
                                        <textarea class="form-control form_input_field form_fields" rows="4" name="ren_ag_additional_terms" id="ren_ag_additional_terms">{{$data->ren_ag_additional_terms}}</textarea>
                                      </div>
                                    </div>
                                    <div class="row points_row footer_note_points_row">
                                      <div class="col-md-12 text-center">
                                        <span class="footer_note"> [ Remainder of Page Intentionally Left Blank ] </span>
                                      </div>
                                    </div>              
                                  </div>

                                  <div class="tabRen">
                                    <div class="row points_row">
                                      <div class="col-md-12"> 
                                        <p> 
                                          IN WITNESS WHEREOF , the parties have caused this Agreement to be executed the day and year first above written.
                                        </p>
                                      </div>
                                    </div>
                                    <div class="row points_row signature_points_row">
                                      <div class="col-md-6 input_wrapper evos_field">
                                        <fieldset class="custom_fieldset_2 contractor_fieldset">
                                          <legend> Lessor : </legend>

                                          <div class="form-group bmd-form-group mt-3">
                                            <label for="model_equip" class="bmd-label-floating input_label"> Name * </label>
                                            <input type="text" class="form-control form_input_field form_fields" id="lessor_ag_names" name="ren_ag_executed_lessor_name" value="{{$data->ren_ag_executed_lessor_name}}">
                                          </div>

                                          <div class="form-group bmd-form-group">
                                            <label for="model_equip" class="bmd-label-floating input_label"> Position , If Applicatble </label>
                                            <input type="text" class="form-control form_input_field form_fields" id="lessor_ag_pos" name="ren_ag_executed_lessor_position" value="{{$data->ren_ag_executed_lessor_position}}">
                                          </div>
                                        </fieldset>                                        
                                      </div>
                                      <div class="col-md-6">
                                        <fieldset class="custom_fieldset_2 contractor_fieldset">
                                          <legend> Lessee : </legend>

                                          <div class="form-group bmd-form-group mt-3">
                                            <label for="model_equip" class="bmd-label-floating input_label"> Name * </label>
                                            <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="lessee_ag_names" name="ren_ag_executed_lessee_name" value="{{$data->ren_ag_executed_lessee_name}}">
                                          </div>

                                          <div class="form-group bmd-form-group">
                                            <label for="model_equip" class="bmd-label-floating input_label"> Position , If Applicatble </label>
                                            <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="lessee_ag_pos" name="ren_ag_executed_lessee_position" value="{{$data->ren_ag_executed_lessee_position}}">
                                          </div>
                                        </fieldset>                                              
                                      </div>
                                    </div>
                                    

                                  <fieldset class="custom_fieldset_2 contractor_fieldset info_div necessary_files_fieldset">
                                    <legend> Necessary Files </legend>
                                    <div class="row input_row poins_row mb-3">
                                      <table>
                                      <td class="" style="padding-left:10px">
                                            <p class="necessary_files_head_p"> W9 </p>
                                            <hr>
                                            <div class="uploaded-file">
                                            <?php $i = 1; ?>
                                            @if(!$agreement_files->isEmpty())
                                            @foreach($agreement_files as $rowdata)
                                              <div> <a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}}</a> </div>
                                            <?php $i++; ?>
                                            @endforeach
                                            @else
                                              <span>No files Uploaded</span>
                                            @endif
                                            </div>
                                      </td>
                                      <td style="border-left:2px solid gray; padding-left:10px">
                                            <p class="necessary_files_head_p"> Insurance Certificate </p>
                                            <hr>
                                            <div class="uploaded-file">
                                            <?php $i =1; ?>
                                            @if(!$insurance_files->isEmpty())
                                            @foreach($insurance_files as $rowdata)
                                              <div> <a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}}</a> </div>
                                            <?php $i++; ?>
                                            @endforeach
                                            @else
                                              <span>No files Uploaded</span>
                                            @endif
                                            </div>
                                      </td>
                                      </table>            
                                    </div>
                                  </fieldset>

                                   @if($rn_sign)
                                <div class="row input_row mt-2" style="margin-bottom: 15px;">
                                  <div class="col-md-12 text-center">
                                      <b>Signature</b>
                                      <br/>
                                      <div id="sig" style="border: 2px solid #DDD">
                                        <img src="{{$rn_sign->url}}">
                                      </div>
                                  </div>
                                </div>
                                @else
                                  <div class="row input_row mt-2" style="margin-bottom: 15px;">
                                    <div class="col-md-12 text-center">
                                        <b>No Signature Uploaded</b>
                                    </div>
                                  </div>
                                @endif
                                                                                  
                                  </div>

                                  <div style="overflow:auto;" class="row msf_btn_holder">
                                    <div class="col-md-4 save_btn_wrapper text-left" style="float:right;">
                                      <a class="btn btn-round btn-warning nextPrevBtn prev" id="prevBtnRen" type="button" href="#top_rental_card" onclick="nextPrevRen(-1)">Previous</a>
                                    </div>
                                    <div class="col-md-8 save_btn_wrapper text-right">
                                      <a class="btn custom-btn-one btn-round btn-success nextPrevBtn next" href="#top_rental_card" id="nextBtnRen" type="button" onclick="nextPrevRen(1)">Next</a>

                                    @if($data->status == 2)
                                      <button class="btn btn-round btn-danger nextPrevBtnOne submitBtnRen submitBtn" style="display: none" type="button" id="" onclick="" data-toggle="modal" data-target="#rejectModal">Reject</button>

                                      <button class="btn custom-btn-one btn-round btn-success nextPrevBtnOne submitBtnRen submitBtn submit_to_insurance_btn" style="display: none" type="button" id="" onclick="" data-toggle="modal" data-target="#submitModal">Submit to Insurance</button>  
                                    @endif

                                    </div>
                                  </div>
                                </div>
                              </div>                              
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- Modal -->
                      <div class="modal fade bs_modal" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel"><u>Reason For The Rejection</u></h5>
                              <button type="button" class="close close_icon" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <textarea class="form-control summernote" style="min-width: 100%" rows="6" name="reject"></textarea>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary" name="submit" value="reject">Submit</button>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!-- Modal -->
                    <div class="modal fade bs_modal" id="submitModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"><u>Submit to Insurance</u></h5>
                            <button type="button" class="close close_icon" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <fieldset class="custom_fieldset_2 contractor_fieldset mt-2 mb-3">
                                <legend>Sending Information</legend>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group bmd-form-group">
                                    <label for="agreement_send_mail_to" class="bmd-label-floating input_label">Send Mail To Insurance</label>
                                      <input type="email" class="form-control form_input_field form_fields" id="agreement_send_mail_to" name="submitted_to_insurance" value="elizabeth@mcanallywilkins.com" required="">
                                    </div>                                          
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group bmd-form-group">
                                    <label for="agreement_send_mail_to" class="bmd-label-floating input_label">Send Mail To 3rd Party</label>
                                      <input type="email" class="form-control form_input_field form_fields" id="agreement_send_mail_to" name="ren_ag_send_mail_to" value="{{$data->ren_ag_send_mail_to}}" required="">
                                    </div>                                          
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group bmd-form-group">
                                    <label for="agreement_send_mail_date" class="bmd-label-floating input_label">Date</label>
                                      <input type="text" class="datepicker form-control form_input_field form_fields" id="agreement_send_mail_date" name="submitted_date_insurance" value="{{date('m/d/Y')}}" required="">
                                    </div>                                          
                                  </div>
                                </div>
                              </fieldset>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" name="submit" value="submit_to_insurance">Submit</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    </form>
                  </div>

                @endif



                @if($type == 'owner_agreement')
                <div class="card-body show_agreement_card_body">
                  <form id="multiForm3" method="post" action="{{ route('insurance.store') }}" class="owner_operator_agreement_form" enctype="multipart/form-data">

                       @csrf
                        <input type="hidden" name="tab" value="owner_agreement">
                        <input type="hidden" class="status" name="status" value="3">
                        <input type="hidden" name="p_key" value="{{$data->id}}">

                        <!-- Circles which indicates the steps of the form: -->
                      <div class="card subcontractor_agreement_card_section" id="top_owner_card">
                        <div class="card-header card-header-rose card-header-icon">
                          <div class="card-icon">
                            <i class="material-icons">assignment</i>
                          </div>
                          <h4 class="card-title">Owner Agreement</h4>

                          <div class="col-md-12 text-center">
                            <div class="credit_app_id_badge text-right agreement_badge">
                              <div class="timeline-heading">
                                <h4 class="badge badge-pill"> OA-{{$data->id}} </h4>
                              </div>
                            </div>

                          @if($data->status == 3)
                            <span class="text-info agreement_pdf_download_span">
                              <a target="_blank" href="{{ route('agreementPdf', [$data->id, 'type' => 'owner_operator'] ) }}" class="btn btn-link btn-twitter">
                                <i class="material-icons">save_alt</i>
                              Owner Operator Agreement PDF
                              </a>  
                            </span>
                          @endif

                          </div>

                        </div>
                        <div class="card-body subcontractor_agreement_card_body">
                          <div class="row">
                            <div class="col-md-12 text-center">
                              <h2 class="main-title">OWNER OPERATOR AGREEMENT</h2>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-1 progress_container" style="text-align:center;margin-bottom:20px;">
                              <span class="stepOwner step-first active">
                                <i class="material-icons">done</i>
                              </span>
                              <span class="stepOwner step-inner">
                                <i class="material-icons">done</i>
                              </span>
                              <span class="stepOwner step-inner">
                                <i class="material-icons">done</i>
                              </span>
                              <span class="stepOwner step-inner">
                                <i class="material-icons">done</i>
                              </span>
                              <span class="stepOwner step-inner">
                                <i class="material-icons">done</i>
                              </span>
                              <span class="stepOwner step-last">
                                <i class="material-icons">done</i>
                              </span>
                              <span class="stepOwner">
                                <i class="material-icons">done</i>
                              </span>
                              <!-- <span class="stepOwner">
                                <i class="material-icons">done</i>
                              </span> -->
                              <span class="stepOwner">
                                <i class="material-icons">done</i>
                              </span>
                            </div>
                            <div class="col-md-11 content_container">

                              <div class="tabOwner">
                                <div class="row input_row">
                                  <div class="col-md-12">
                                    <div>
                                      <span>This Owner Operator Agreement (“Agreement”) is entered into and shall be effective as of the</span>
                                      <span>
                                        <div class="form-group bmd-form-group day day-month-year">
                                          <input type="number" class="form-control form_input_field form_fields field_to_fill" id="sub_agree_day" name="owner_day" value="{{$data->owner_day}}" placeholder="Day" min="1" max="31">
                                        </div>
                                      </span>
                                      <span>day of</span> 
                                      <span>

                                        <div class="input_wrapper custom_select2_col month">
                                          <div class="form-group custom-form-select">
                                            <select class="custom-select contractor_select select2 form_input_field field_to_fill" id="subContractoMonth" name="owner_month">
                                              <option value="january" @if($data->owner_month == 'january') selected @endif>January</option>
                                                <option value="february" @if($data->owner_month == 'february') selected @endif>February</option>
                                                <option value="march" @if($data->owner_month == 'march') selected @endif>March</option>
                                                <option value="april" @if($data->owner_month == 'april') selected @endif>April</option>
                                                <option value="may" @if($data->owner_month == 'may') selected @endif>May</option>
                                                <option value="june" @if($data->owner_month == 'june') selected @endif>June</option>
                                                <option value="july" @if($data->owner_month == 'july') selected @endif>July</option>
                                                <option value="august" @if($data->owner_month == 'august') selected @endif>August</option>
                                                <option value="september" @if($data->owner_month == 'september') selected @endif>September</option>
                                                <option value="october" @if($data->owner_month == 'october') selected @endif>October</option>
                                                <option value="october" @if($data->owner_month == 'october') selected @endif>November</option>
                                                <option value="december" @if($data->owner_month == 'december') selected @endif>December</option>
                                            </select>
                                            <div class="form-element-bar">
                                            </div>
                                            <label class="form-element-label" for="contractor_select input_label">Enter a month</label>
                                          </div>
                                        </div>
                                      </span>
                                      <span>
                                        <div class="form-group bmd-form-group year day-month-year">
                                          <input type="number" class="form-control form_input_field form_fields field_to_fill" id="sub_agree_year" name="owner_year" value="{{$data->year}}" placeholder="Year *">
                                      </div>
                                      </span>
                                      <span>
                                         (the “Effective Date”), by and among EV Oilfield Services, LLC (“Contractor”), and
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <fieldset class="custom_fieldset_2 contractor_fieldset">
                                  <legend>Contract Form</legend>
                                  <div class="row input_row">
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="owner_name" class="bmd-label-floating input_label">Owner Operator *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="owner_name" name="owner_name" value="{{$data->owner_name}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper custom_select2_col">
                                      <div class="form-group custom-form-select">
                                        <select class="custom-select contractor_select select2 form_input_field field_to_fill" id="owner_select" name="owner_type_of_entity">
                                          <option class="form-select-placeholder"></option>
                                          <option value="corporation" @if($data->owner_type_of_entity == 'corporation') selected @endif>Corporation</option>
                                          <option value="llc" @if($data->owner_type_of_entity == 'llc') selected @endif>LLC</option>
                                          <option value="limited_partnership" @if($data->owner_type_of_entity == 'limited_partnership') selected @endif>Limited Partnership</option>
                                          <option value="sole_proprietorship" @if($data->owner_type_of_entity == 'sole_proprietorship') selected @endif>Sole Proprietorship</option>
                                        </select>
                                        <div class="form-element-bar">
                                        </div>
                                        <label class="form-element-label" for="contractor_select input_label">Type of Entity *</label>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="owner_mailing_addr" class="bmd-label-floating input_label">Mailing Address *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="owner_mailing_addr" name="owner_mailing_addr" value="{{$data->owner_mailing_addr}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="physical_addr" class="bmd-label-floating input_label">Physical Address (if different from mailing Address) *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="owner_physical_addr" name="owner_physical_addr" value="{{$data->owner_physical_addr}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contact_person" class="bmd-label-floating input_label">Contact Person *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="onwer_contact_person" name="owner_contact_person" value="{{$data->owner_contact_person}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contact_person_title" class="bmd-label-floating input_label">Title *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="owner_contact_person_title" name="owner_contact_person_title" value="{{$data->owner_contact_person_title}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="owner_contact_person_email" class="bmd-label-floating input_label">Email *</label>
                                          <input type="email" class="form-control form_input_field form_fields field_to_fill" id="owner_contact_person_email" name="owner_contact_person_email" value="{{$data->owner_contact_person_email}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="owner_contact_person_phone" class="bmd-label-floating input_label">Phone *</label>
                                          <input type="text" class="form-control form_input_field form_fields mobile field_to_fill" id="owner_contact_person_phone" name="owner_contact_person_phone" value="{{$data->owner_contact_person_phone}}">
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contact_person_fax" class="bmd-label-floating input_label">Fax *</label>
                                          <input type="text" class="form-control form_input_field form_fields mobile field_to_fill" id="owner_contact_person_fax" name="owner_contact_person_fax" value="{{$data->owner_contact_person_fax}}">
                                        </div>
                                    </div> -->
                                  </div>
                                </fieldset>
                                <div class="row agrement_tab_content">
                                  <div class="col-md-12">
                                    <p>Contractor and Owner Operator may be individually referred to herein as a “Party” or collectively as the “Parties.” </p>
                                  </div>
                                </div>
                                <div class="row agrement_tab_content">
                                  <div class="col-md-12">
                                    <h4 class="title">CONSPICUOUS AND FAIR NOTICE</h4>
                                    
                                    <p>Both parties represent to each other that --</p>
                                    <ul class="subcontractor_agre_list bold">
                                      <li>
                                        They have consulted an attorney concerning this agreement or, if they have not consulted an attorney, they were given the opportunity and had the ability to consult, but made an informed decision not to do so, and
                                      </li>
                                      <li>
                                        They fully understand their rights and obligations under this agreement.
                                      </li> 
                                    </ul>
                                    <p>Now, Therefore, in consideration of the mutual promises, conditions and agreements herein contained, the sufficiency of which is acknowledged, Contractor and Subcontractor hereby agree as follows:</p>
                                  </div>
                                </div>
                              </div>

                              <div class="tabOwner">
                                <div class="row agrement_tab_content">
                                  <div class="col-md-12">
                                    <div class="section_head_content">
                                      <h4 class="sub-title">1. Scope of Agreement</h4>
                                      
                                      <p>
                                        Work for Hire. It is contemplated that from time to time Owner Operator will be requested by Contractor or its present or future affiliated entities to perform certain work and services  .<span class="dots">...</span> <span class="more">Owner Operator shall be obligated to accept requests to perform Work from Contractor during the term of this agreement.  It is expressly understood and agreed that any and all Work requested by Contractor and accepted by Owner Operator  shall be controlled and governed by the provisions of this Agreement. If and when Contractor desires Owner Operator  to perform Work hereunder, Contractor will issue a Work order (“Work Order”) describing the Work Contractor desires Owner Operator  to perform and the compensation Contractor will pay Owner Operator  for the Work described in the Work order. Owner Operator shall notify Contractor in writing within five (5) business days of Owner Operator’s receipt of such Work Order if Owner Operator intends to accept the Work Order.  The term “Agreement” means this Agreement as incorporated in a Work Order. Term “Contractor” includes the Contractor or any of its affiliated entities that issued the Work Order.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>

                                    <div class="section_head_content">
                                       <h4 class="sub-title">2. Term and Termination</h4>
                                       
                                       <p>This Agreement shall remain in effect for a period of one year from the Effective Date, and from year to year thereafter, subject to the right of either party here to cancel or terminate the Agreement at any time upon not less than thirty (30) days written notice of one party to the other.</p>
                                    </div>

                                    <div class="section_head_content">
                                      <h4 class="sub-title">3. Representations and Warranties</h4>
                                      
                                      <p>Owner Operator will furnish all necessary materials, equipment, permits, and certificates that are required for the Work. All of Owner Operator ’s materials and equipment are suitable for their intended use, and are free from all faults and defects. Owner Operator will remove and replace any defective materials <span class="dots">...</span><span class="more"> or Work forthwith on notice from Contractor. Owner Operator will perform the Work entirely at Owner Operator ’s risk. Owner Operator will provide all proper and sufficient and necessary safeguards against all injuries and damage whatsoever and comply with all safety requirements imposed by law. Owner Operator shall conform to Contractor’s reasonable progress schedule. Owner Operator shall perform the Work in a professional, prompt, and diligent manner, consistent with applicable law and industry standards, without delaying or hindering Contractor’s work. If Owner Operator  shall default in performance of the work or otherwise commit any act which causes delay to Contractor’s work, Owner Operator  shall be liable for all losses, costs, expenses, liabilities and damages, including actual damages, consequential damages and any liquidated damages sustained by Contractor. Owner Operator will comply with all applicable federal, state and local laws, codes, ordinances, rules, regulations, orders and decrees of any government or quasi-government entity having jurisdiction over the project, the project site, the practices involved in the Work, or any subcontract work. Owner Operator, its agents and employees are properly trained and qualified to complete the Work for which they have been hired to do.</span>
                                      <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>

                                    <div class="section_head_content">
                                      <h4 class="sub-title">4. Indemnification</h4>
                                      
                                      <p>
                                        “Claims” shall include, without limitation, any and all claims, losses, damages (including, without limitation, punitive damages and damages for bodily injury, death or property damage), causes of action, fines, penalties, enforcement proceedings, suits, and liabilities of every nature or character (including interest and all expenses of litigation, court costs, and attorneys' fees),<span class="dots">...</span><span class="more">whether or not arising in tort, contract, strict liability, under statute, or of any other character whatsoever, and whether or not caused by a legal duty. “Owner Operator Group” means Owner Operator, its parent, subsidiary and affiliated companies, and their contractors (of whatever tier), and its and their respective directors, officers, members, employees, agents, and representatives. “Contractor Group” means Contractor, its parent, subsidiary and affiliated companies, its and their co-lessees, partners, joint ventures, co-owners, contractors (other than Owner Operator), and its and their respective directors, officers, shareholders, members, employees, agents, and representatives.  Owner Operator  shall defend, indemnify, and hold harmless the Contractor Group for any Claims which any or all of the Contractor Group may incur in connection with any third party Claim arising out of or relating to the Owner Operator  Group’s (or any member of the Owner Operator  Group’s) actual or alleged negligence, willful misconduct, breach of this Agreement or violation of Law.  The foregoing indemnification obligation includes, but is not limited to, any Claims asserted against the Contractor Group (or any member thereof) arising from pollution or contamination which originates above the surface of the land or water from spills of fuels, lubricants, motor oils, pipe dope, paints, solvents, cleaning solutions or other liquids, fumes and garbage which is in any manner associated with or alleged to have been associated with, resulting from or caused by Owner Operator  Group’s Work, equipment or personnel. </span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="tabOwner">
                                <div class="row agrement_tab_content padding-content">
                                  <div class="col-md-12">
                                    <div class="section_head_content">
                                      <h4 class="sub-title">5. Insurance</h4>
                                      
                                      <p>
                                        Owner Operator, at its own cost and expense, shall procure, carry, and maintain insurance coverage satisfactory to Contractor.  Owner Operator will include the following documentation: which shall include not less than the following coverage:
                                      </p>

                                      <fieldset class="custom_fieldset_2 contractor_fieldset info_div necessary_files_fieldset">
                                        <legend> Files </legend>
                                        <div class="row input_row poins_row mb-3">
                                          <table>
                                            <td class="" style="padding-left:10px">
                                                  <p class="necessary_files_head_p"> 1. Copy of W9  * </p>
                                                  <hr>
                                                  <div class="uploaded-file">
                                                     
                                                    @if(!$owner_w9->isEmpty())
                                                      <?php $i = 1; ?>
                                                      @foreach($owner_w9 as $rowdata)
                                                      <div><a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}} </a></div> 
                                                      <?php $i++; ?>
                                                      @endforeach
                                                    @else
                                                      No files uploaded
                                                    @endif 
                                                  <!-- <span>No files Uploaded</span> -->
                                                  </div>
                                            </td>
                                            <td style="border-left:2px solid gray; padding-left:10px">
                                                  <p class="necessary_files_head_p"> 2. Copy Of Owner Operator Comprehensive / Bobtail Insurance Policy *  </p>
                                                  <hr>
                                                  <div class="uploaded-file">
                                                
                                                    @if(!$owner_operator_comprehensive->isEmpty())
                                                      <?php $i = 1; ?>
                                                      @foreach($owner_operator_comprehensive as $rowdata)
                                                      <div><a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}} </a></div> 
                                                      <?php $i++; ?>
                                                      @endforeach
                                                    @else
                                                      No files uploaded
                                                    @endif
                                                 
                                                  <!-- <span>No files Uploaded</span> -->
                                                  </div>
                                            </td>
                                          </table>            
                                        </div>
                                        <div class="row input_row poins_row mb-3">
                                          <table>
                                            <td class="" style="padding-left:10px">
                                                  <p class="necessary_files_head_p"> 3. Copy Owner Operator Vehicle Registration *  </p>
                                                  <hr>
                                                  <div class="uploaded-file">
                                                    <div>
                                                    @if(!$owner_vehicle_reg->isEmpty())
                                                      <?php $i = 1; ?>
                                                      @foreach($owner_vehicle_reg as $rowdata)
                                                      <div><a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}} </a></div> 
                                                      <?php $i++; ?>
                                                      @endforeach
                                                    @else
                                                      No files uploaded
                                                    @endif
                                                    </div>
                                                  <!-- <span>No files Uploaded</span> -->
                                                  </div>
                                            </td>
                                            <td style="border-left:2px solid gray; padding-left:10px">
                                                  <p class="necessary_files_head_p"> 4. Copy of DOT Annual Inspection Certificate * </p>
                                                  <hr>
                                                  <div class="uploaded-file">
                                                    <div> 
                                                    @if(!$owner_annual_insp_cer->isEmpty())
                                                      <?php $i = 1; ?>
                                                      @foreach($owner_annual_insp_cer as $rowdata)
                                                      <div><a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}} </a></div> 
                                                      <?php $i++; ?>
                                                      @endforeach
                                                    @else
                                                      No files uploaded
                                                    @endif
                                                    </div>
                                                  <!-- <span>No files Uploaded</span> -->
                                                  </div>
                                            </td>
                                          </table>            
                                        </div>
                                        <div class="row input_row poins_row mb-3">
                                          <table>
                                            <td class="" style="padding-left:10px">
                                                  <p class="necessary_files_head_p">  5. Copy of Heavy Vehicle Use Form  *  </p>
                                                  <hr>
                                                  <div class="uploaded-file">
                                                    <div> 
                                                    @if(!$owner_vehicle_use_form->isEmpty())
                                                      <?php $i = 1; ?>
                                                      @foreach($owner_vehicle_use_form as $rowdata)
                                                      <div><a href="{{$rowdata->url}}">{{$i}}. {{$rowdata->name}} </a></div> 
                                                      <?php $i++; ?>
                                                      @endforeach
                                                    @else
                                                      No files uploaded
                                                    @endif
                                                    </div>
                                                  <!-- <span>No files Uploaded</span> -->
                                                  </div>
                                            </td>
                                            <td style="border-left:2px solid gray; padding-left:10px">
                                            </td>
                                          </table>            
                                        </div>
                                      </fieldset>

                                    </div>
                                    <div class="section_head_content">
                                      <p>Owner Operator shall name Contractor as an additional insured on all policies of insurance. Owner Operator shall cause its insurance carrier to forward forthwith to <span class="dots">...</span><span class="more">Contractor a standard certificate of insurance such certificate shall require the insurance carrier to give Contractor thirty (30) days prior written notice of the cancellation of such insurance. Owner Operator shall not perform any work under the Agreement until a Certificate of Insurance has been delivered to Contractor. If Owner Operator fails to furnish current evidence upon demand of any insurance required hereunder, or if any insurance is cancelled or materially changed, Contractor may suspend or terminate this Agreement until insurance is obtained. The insurance coverage required herein shall in no way limit the Owner Operator’s indemnification obligations or other liability under this Agreement.</span>
                                      <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="tabOwner">
                                <div class="row agrement_tab_content">
                                  <div class="col-md-12">

                                    <div class="section_head_content">
                                      <h4 class="sub-title">6. Waiver of Subrogation</h4>
                                      
                                      <p>
                                        Owner Operator hereby waives any right of subrogation that it, or anyone claiming through or under it, may have against contractor, its agents, employees, or anyone for <span class="dots">...</span><span class="more"> whom blakely construction company, inc. may be responsible, for any loss or damage shall have been caused by the fault or negligence of contractor. However, this waiver shall not adversely or impair any policies of insurance or prejudice the right of either party to recover thereunder. Owner Operator shall provide an endorsement from Owner Operator’s insurer that any right of subrogation is waived as against contractor and that such waiver will not impair Owner Operator's policies of insurance. Any failure or delay by contractor to require such an endorsement shall not constitute a waiver, nor shall it otherwise affect this mutual waiver of subrogation.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button>  
                                      </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">7. Independent Contractor</h4>
                                      
                                      <p>
                                        Owner Operator shall have the status of an "independent contractor".  Owner Operator  shall employ and direct all persons engaged in the performance <span class="dots">...</span> <span class="more"> of any and all of its services under this Agreement, and such agents, servants or employees are subject to the sole control and direction of Owner Operator, and shall not be agents, servants or employees of Contractor. Owner Operator ’s agents, contractors and employees shall not be entitled to any compensation, benefits or other remuneration from Contractor. Contractor shall not have any authority to supervise or direct the manner in which Owner Operator shall perform the Work to be rendered hereunder. Owner Operator shall provide and be responsible for providing, using and maintaining all equipment, machinery and tools necessary to perform the Work.  Owner Operator may accept or reject Work Orders in its discretion and may perform services for persons or entities other than Contractor.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    
                                    <div class="section_head_content">
                                      <h4 class="sub-title">8. Notices</h4>
                                      
                                      <p>
                                        All notices and other communications pertaining to this Agreement shall be in writing and shall be deemed to have been given by a party hereto <span class="dots">...</span> <span class="more">if personally delivered to the other party or if sent by certified mail, return receipt requested, postage prepaid. All notices shall be addressed as shown below: </span>
                                         <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    
                                  </div>
                                </div>
                                <fieldset class="custom_fieldset_2 contractor_fieldset evos_field">
                                  <legend>Contractor Form</legend>
                                  <div class="row input_row">
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_name_2" class="bmd-label-floating input_label">Contractor</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="owner_contractor_name_2" name="notice_owner_contractor_name" value="{{$data->notice_owner_contractor_name}}">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_address" class="bmd-label-floating input_label">Address</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="owner_contractor_address" name="notice_owner_contractor_address" value="{{$data->notice_owner_contractor_address}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_phone" class="bmd-label-floating input_label">Phone</label>
                                          <input type="text" class="form-control form_input_field form_fields mobile" id="owner_contractor_phone" name="notice_owner_contractor_phone" value="{{$data->notice_owner_contractor_phone}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_email" class="bmd-label-floating input_label">Email</label>
                                          <input type="email" class="form-control form_input_field form_fields" id="owner_notice_contractor_email" name="notice_owner_contractor_email" value="{{$data->notice_owner_contractor_email}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="contractor_attn" class="bmd-label-floating input_label">Attn</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="owner_contractor_attn" name="notice_owner_contractor_attn" value="{{$data->notice_owner_contractor_attn}}">
                                        </div>
                                    </div>
                                  </div>
                                </fieldset>
                                <fieldset class="custom_fieldset_2 contractor_fieldset">
                                  <legend>Owner Operator Form</legend>
                                  <div class="row input_row">
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_name_2" class="bmd-label-floating input_label">Owner Operator Name *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="owner_operator_name_2" name="notice_owner_operator_name" value="{{$data->notice_owner_operator_name}}">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_address" class="bmd-label-floating input_label">Address *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="owner_operator_address" name="notice_owner_operator_address" value="{{$data->notice_owner_operator_address}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_phone" class="bmd-label-floating input_label">Phone *</label>
                                          <input type="text" class="form-control form_input_field form_fields mobile field_to_fill" id="notice_owner_operator_phone" name="notice_owner_operator_phone" value="{{$data->notice_owner_operator_phone}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_email" class="bmd-label-floating input_label">Email *</label>
                                          <input type="email" class="form-control form_input_field form_fields field_to_fill" id="notice_owner_operator_email" name="notice_owner_operator_email" value="{{$data->notice_owner_operator_email}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="notice_owner_operator_contact" class="bmd-label-floating input_label">Contact Person*</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill" id="notice_owner_operator_contact" name="notice_owner_operator_contact" value="{{$data->notice_owner_operator_contact}}">
                                        </div>
                                    </div>
                                  </div>
                                </fieldset>
                              </div>

                              <div class="tabOwner">
                                
                                <div class="row agrement_tab_content">
                                  <div class="col-md-12">
                                    <div class="section_head_content">
                                      <h4 class="sub-title">9. Breach of Agreement</h4>
                                      
                                      <p>
                                        If Owner Operator breaches any of the terms of this agreement, then Contractor has the option, at its sole and absolute discretion, to terminate this agreement immediately, without written notice.<span class="dots">...</span> <span class="more">Failure of Contractor to insist upon Owner Operator ’s performance under this Agreement or to exercise any right or privilege shall not be a waiver of any of Contractor’s rights or privileges contained herein.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">10. Record Keeping</h4>
                                     
                                      <p>
                                        For the purposes of permitting verification by the Contractor of any amounts paid to or claimed by the Owner Operator , the Owner Operator shall keep and preserve, for not less than two years from date of invoice all general ledgers <span class="dots">...</span> <span class="more">, work orders, receipts, disbursements journals, bids, bid proposals, price lists, and supporting documentation obtained from manufacturers and suppliers in connection with the Work performed.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">11. Reports</h4>
                                      <p>
                                        Owner Operator  shall provide to Contractor an oral report, confirmed in writing as soon as practicable, of all accidents or occurrences resulting in death or injuries to Owner Operator ’s employees, Owner Operator s, or any third parties, damage <span class="dots">...</span> <span class="more">to Contractor’s property, or physical damage to the property of Contractor or any third party, arising out of or during the course of work to be performed. Owner Operator shall furnish Contractor with a copy of all reports made by Owner Operator to Owner Operator ’s insurer, governmental authorities, or to others of such accidents or occurrences.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">12. Payment Terms</h4>
                                      
                                      <p> Contractor agrees to pay Owner Operator for any work at the rates set forth in the applicable Work Order. Owner Operator shall provide Contractor upon the completion of any Work a signed Work <span class="dots">...</span> <span class="more">Order indicating number of hours worked and certifying to the completion of the job in accordance with the terms of the Work Order (the “Ticket”).  Owner Operator will receive 75% of the billed hourly rate of the job.  Owner Operator is responsible for all expenses including but not limited to driver wages, fuel, repairs, and maintenance.  Contactor shall pay Owner Operator within 14 days of proof / receipt of completed work.  In the event Contractor has a bona fide question or dispute concerning an Owner Operator’s Ticket or a portion thereof, Contractor may withhold the disputed portion of the payment only. Contractor shall give written notice of any disputed amounts to Owner Operator specifying the reasons therefore within sixty (60) days after receipt of the applicable Ticket. Tickets submitted later than thirty (30) days will not be considered for reimbursement. Contractor shall not pay or reimburse Owner Operator for any travel or other expenses unless Contractor approves such expenses in advance and in writing.   </span>
                                      <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    
                                    <div class="section_head_content">
                                      <h4 class="sub-title">13. Audit</h4>
                                      
                                      <p>
                                        Owner Operator  shall maintain a true and correct set of records pertaining to all work performed under each work order, including supporting <span class="dots">...</span> <span class="more">documentation, for two (2) years following completion of the Work. Contractor may, at its expense require Owner Operator at any time within said two-year period to furnish sufficient evidence, with documentary support, to enable Contractor to verify the correctness and accuracy of payments to Owner Operator. Contractor may, following written notice to Owner Operator audit any and all records of Owner Operator relating to the work performed by or on behalf of Owner Operator hereunder, and all payments made in regard thereto, in order to verify the accuracy and compliance with this provision; provided however, Contractor shall not have the right to inspect or audit Owner Operator ’s trade secrets or any proprietary information. If Contractor’s examination discloses that Owner Operator ’s tickets to Contractor were in error, Owner Operator will immediately pay to Contractor any amounts overpaid by Contractor, plus interest from the date of the error at the lesser of one percent (1%) per month or the maximum rate allowed by law.</span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="tabOwner">
                                
                                <div class="row input_row agrement_tab_content">
                                  <div class="col-md-12">
                                    
                                    <div class="section_head_content">
                                      <h4 class="sub-title">14. No delay</h4>
                                     
                                      <p>
                                        In no case (unless otherwise directed in writing by the Contractor) shall any claim or dispute, whether or not involving litigation, permit the Owner Operator  to delay or discontinue any of the work hereunder, but rather the <span class="dots">...</span> <span class="more">Owner Operator shall diligently pursue the work hereunder while awaiting the resolution of any dispute or claim; provided, however, that the Contractor shall not withhold, pending the resolution of any claim or dispute, the payment of any undisputed sum otherwise due Owner Operator under this Agreement. </span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">15. Drugs, Alcohol & Firearms</h4>
                                      
                                      <p>
                                        To help ensure a safe, productive work environment, Contractor prohibits the use, transportation and possession of firearms, drugs and/or controlled substances, drug paraphernalia and alcoholic beverages. Illegal drugs shall include, but not be limited <span class="dots">...</span> <span class="more">to, marijuana, amphetamines, barbiturates, opiates, cocaine, codeine, morphine, hallucinogenic substances (LSD) and any similar drugs and/or chemical synthetics deemed hazardous by Contractor. Such prohibitions shall apply to Owner Operator  and its employees, agents, servants and Owner Operator’s employees, agents, contractors and invitees shall abide the more stringent of Owner Operator’s or Contractor’s drug, alcohol and firearm policy. Contractor may request that Owner Operator carry out drug and alcohol tests of its employees and/or that Owner Operator carry out reasonable searches of individuals, their personal effects and vehicles when entering on and leaving assigned premises at any time, at scheduled times, or at random. Individuals found in violation shall be removed from the premises by Owner Operator immediately. Submission to such a search is strictly voluntary, however, refusal may be cause for not allowing that individual on the well site or Contractor’s other premises. Owner Operator shall (1) test at Owner Operator’s expense any individual involved in or related to an accident or injury within twelve (12) hours of such accident or injury and (2) submit to Contractor any drug or alcohol test results for any individual involved in or related to an accident or injury on Contractor’s premises. It is Owner Operator ’s responsibility to notify its employees, contractors, Owner Operators, agents and invitees of this prohibition, the provisions of this paragraph and its enforcement and obtain any acknowledgement or release from any person in order to comply with this provision and applicable law. </span>
                                        <button class="btn btn-link btn_read_more" type="button">Read More</button> 
                                      </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">16. Miscellaneous</h4>
                                      
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">A. Governing Law</h4>
                                      
                                      <p>The laws of the State of Texas shall apply and govern the validity, interpretation, and performance of this Agreement. The Parties waive any right to trial by jury in any action arising out of or relating to this Agreement or the Parties’ relationship. </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">B. Severability</h4>
                                      
                                      <p>If any part of this Agreement contravenes any applicable statutes, regulations, rules, or common law requirements, then, to the extent of and only to the extent of such contravention, such part shall be severed from this Agreement and deemed non-binding while all other parts of this Agreement shall remain binding. </p>
                                    </div>

                                  </div>
                                </div>
                              </div>

                              <div class="tabOwner">
                                <div class="row input_row agrement_tab_content">
                                  <div class="col-md-12">
                                    <div class="section_head_content">
                                      <h4 class="sub-title">C. Amendment</h4>
                                      
                                      <p>This Agreement may not be modified or amended unless it is in writing and signed by both Parties. </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">D. Assignment</h4>
                                      
                                      <p>Neither Party shall assign all or any part of its rights or obligations under this Agreement without the prior written consent from the other Party.</p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">E. Compliance with Laws</h4>
                                     
                                      <p>Owner Operator agrees to comply with all laws, statutes, codes, rules, and regulations, which are now or may hereafter become applicable to Work covered by this Agreement or arising out of the performance of such operations. </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">F. Headings</h4>
                                     
                                      <p>The headings, sub-headings, and other subdivisions of this Agreement are inserted for convenience only. The Parties do not intend them to be an aid in legal construction. </p>
                                    </div>
                                    <div class="section_head_content">
                                      <h4 class="sub-title">G. Authority to Sign and Bind</h4>
                                     
                                      <p>By their signatures below, Owner Operator represents that the person signing this Agreement has the authority to execute same on behalf of Owner Operator and to bind Subcontract to the obligations set forth herein. </p>
                                    </div>

                                    <div class="section_head_content owner_fees_section_head_content">
                                      <h4 class="sub-title">H. Registration & Fees </h4>
                                      
                                      <p>
                                        Owner Operator Additional Charges:
                                      </p>
                                      <ul class="subcontractor_agre_list evos_field">
                                        <div class="row">
                                          <div class="col-md-6">
                                            <li> Admin Fee :  $
                                              <span>
                                                <div class="form-group bmd-form-group insurance_limit_currency">
                                                  <input type="text" class="form-control form_input_field form_fields required" id="owner_admin_fee" name="owner_admin_fee" placeholder="" value="{{$data->owner_admin_fee}}">
                                                </div>
                                              </span>

                                            ( Monthly ) </li>
                                            <li>  New Truck Set Up Fee:  $
                                            <span>
                                                <div class="form-group bmd-form-group insurance_limit_currency">
                                                  <input type="text" class="form-control form_input_field form_fields required" id="owner_truck_set_up_fee" name="owner_truck_set_up_fee" placeholder="" value="{{$data->owner_truck_set_up_fee}}">
                                                </div>
                                            </span>
                                            ( One Time Fee )</li>
                                            <li>  Decals:   $
                                            <span>
                                                <div class="form-group bmd-form-group insurance_limit_currency">
                                                  <input type="text" class="form-control form_input_field form_fields required" id="owner_decals_fee" name="owner_decals_fee" placeholder="" value="{{$data->owner_decals_fee}}">
                                                </div>
                                            </span>
                                            ( One Time Fee )</li>
                                            <li> GPS Installation :   $
                                            <span>
                                              <div class="form-group bmd-form-group insurance_limit_currency">
                                                <input type="text" class="form-control form_input_field form_fields required" id="owner_gps_installation_fee" name="owner_gps_installation_fee" placeholder="" value="{{$data->owner_gps_installation_fee}}">
                                              </div>
                                            </span>
                                            ( One Time Fee )</li>
                                            <li>GPS Monitoring : $
                                            <span>
                                              <div class="form-group bmd-form-group insurance_limit_currency">
                                                <input type="text" class="form-control form_input_field form_fields required" id="owner_gps_monitoring_fee" name="owner_gps_monitoring_fee" placeholder="" value="{{$data->owner_gps_monitoring_fee}}">
                                              </div>
                                            </span> 
                                            ( Monthly )</li>
                                            <li> Drug Testing : $
                                            <span>
                                              <div class="form-group bmd-form-group insurance_limit_currency">
                                                <input type="text" class="form-control form_input_field form_fields required" id="owner_drug_testing_fee" name="owner_drug_testing_fee" placeholder="" value="{{$data->owner_drug_testing_fee}}">
                                              </div>
                                            </span> 
                                            ( One Time Fee )</li>
                                          </div>
                                          <div class="col-md-6">
                                            <li> Training (If Required / Applicable): $ 
                                            <span>
                                              <div class="form-group bmd-form-group insurance_limit_currency">
                                                <input type="text" class="form-control form_input_field form_fields required" id="owner_training_fee" name="owner_training_fee" placeholder="" value="{{$data->owner_training_fee}}">
                                              </div>
                                            </span>
                                            ( One Time Fee )</li>

                                            <li> MVR: $ 
                                            <span>
                                              <div class="form-group bmd-form-group insurance_limit_currency">
                                                <input type="text" class="form-control form_input_field form_fields required" id="owner_mvr_fee" name="owner_mvr_fee" placeholder="" value="{{$data->owner_mvr_fee}}">
                                              </div>
                                            </span>
                                            ( One Time Fee )</li>

                                            <li> Required PPE ( Gas Monitor ): $
                                            <span>
                                              <div class="form-group bmd-form-group insurance_limit_currency">
                                                <input type="text" class="form-control form_input_field form_fields required" id="owner_ppe_fee" name="owner_ppe_fee" placeholder="" value="{{$data->owner_ppe_fee}}">
                                              </div>
                                            </span>
                                            </li>

                                            <li> DOT Cab Card: $ 
                                            <span>
                                              <div class="form-group bmd-form-group insurance_limit_currency">
                                                <input type="text" class="form-control form_input_field form_fields required" id="owner_dot_cab_card_fee" name="owner_dot_cab_card_fee" placeholder="" value="{{$data->owner_dot_cab_card_fee}}">
                                              </div>
                                            </span>
                                            ( One Time Fee )</li>

                                            <li> Insurance (Per Truck / Tank): $ 
                                            <span>
                                              <div class="form-group bmd-form-group insurance_limit_currency">
                                                <input type="text" class="form-control form_input_field form_fields required" id="owner_ins_per_truck_fee" name="owner_ins_per_truck_fee" placeholder="" value="{{$data->owner_ins_per_truck_fee}}">
                                              </div>
                                            </span>
                                            ( Weekly )</li>                                               
                                            <li> Field Tickets: $ 
                                            <span>
                                              <div class="form-group bmd-form-group insurance_limit_currency">
                                                <input type="text" class="form-control form_input_field form_fields required" id="field_tickets_fee" name="field_tickets_fee" placeholder="" value="{{$data->field_tickets_fee}}">
                                              </div>
                                            </span>
                                           / Book</li>
                                         </div>
                                       </div>
                                      </ul>
                                    </div>

                                  </div>
                                </div>
                              </div>

                              <!-- <div class="tabOwner">
                                <div class="row"> 
                                    <div class="col-md-12"> 

                                    </div>
                                </div>
                              </div> -->

                              <div class="tabOwner">
                                <div class="row input_row">
                                  <div class="col-md-12">
                                    <div>
                                      <span>Executed this</span>
                                      <span>
                                        <div class="form-group bmd-form-group day day-month-year">
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="sub_agree_day_2" name="executed_day_owner" placeholder="Day" max="31" value="{{$data->executed_day_owner}}">
                                        </div>
                                      </span>
                                      <span>day of</span> 
                                      <span>
                                        <div class="input_wrapper custom_select2_col month">
                                          <div class="form-group custom-form-select">
                                            <select class="custom-select contractor_select select2 form_input_field field_to_fill field_to_fill_validator" id="subContractoMonth2" name="executed_month_owner">
                                              <option class="form-select-placeholder"></option>
                                              <option value="january" @if($data->executed_month_owner == 'january') selected @endif>January</option>
                                                <option value="february" @if($data->executed_month_owner == 'february') selected @endif>February</option>
                                                <option value="march" @if($data->executed_month_owner == 'march') selected @endif>March</option>
                                                <option value="april" @if($data->executed_month_owner == 'april') selected @endif>April</option>
                                                <option value="may" @if($data->executed_month_owner == 'may') selected @endif>May</option>
                                                <option value="june" @if($data->executed_month_owner == 'june') selected @endif>June</option>
                                                <option value="july" @if($data->executed_month_owner == 'july') selected @endif>July</option>
                                                <option value="august" @if($data->executed_month_owner == 'august') selected @endif>August</option>
                                                <option value="september" @if($data->executed_month_owner == 'september') selected @endif>September</option>
                                                <option value="october" @if($data->executed_month_owner == 'october') selected @endif>October</option>
                                                <option value="october" @if($data->executed_month_owner == 'october') selected @endif>November</option>
                                                <option value="december" @if($data->executed_month_owner == 'december') selected @endif>December</option>
                                            </select>
                                            <div class="form-element-bar">
                                            </div>
                                            <label class="form-element-label" for="contractor_select input_label">Enter a month</label>
                                          </div>
                                        </div>
                                      </span>
                                      <span>
                                        <div class="form-group bmd-form-group year day-month-year">
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="owner_agree_year_2" name="executed_year_owner" value="{{$data->executed_year_owner}}" placeholder="Year">
                                      </div>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <fieldset class="custom_fieldset_2 contractor_fieldset evos_field">
                                  <legend>Contractor</legend>
                                  <div class="row input_row">
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="owner_agreement_name_3" class="bmd-label-floating input_label">Contractor</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="owner_agreement_name_3" name="executed_owner_contractor_name" value="{{$data->executed_owner_contractor_name}}">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="owner_contractor_by" class="bmd-label-floating input_label">Contractor By</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="owner_contractor_by" name="executed_owner_contractor_by" value="{{$data->executed_owner_contractor_by}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="owner_contractor_title" class="bmd-label-floating input_label">Title</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="owner_contractor_title" name="executed_owner_contractor_title" value="{{$data->executed_owner_contractor_title}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="owner_contractor_date" class="bmd-label-floating input_label">Date *</label>
                                          <input type="text" class="form-control form_input_field form_fields datepicker" id="owner_contractor_date" name="executed_owner_contractor_date" value="{{$data->executed_owner_contractor_date}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="owner_contractor_initial" class="bmd-label-floating input_label">Contractor Initial *</label>
                                          <input type="text" class="form-control form_input_field form_fields" id="owner_contractor_initial" name="executed_owner_contractor_initial" value="{{$data->executed_owner_contractor_initial}}">
                                        </div>
                                    </div>
                                    
                                  </div>
                                </fieldset>

                                <fieldset class="custom_fieldset_2 contractor_fieldset">
                                  <legend>Owner Operator</legend>
                                  <div class="row input_row">
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="executed_owner_name" class="bmd-label-floating input_label">Owner Operator *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="executed_owner_name" name="executed_owner_operator_name" value="{{$data->executed_owner_operator_name}}">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="executed_owner_by" class="bmd-label-floating input_label">By *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="by" name="executed_owner_operator_by" value="{{$data->executed_owner_operator_by}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_title" class="bmd-label-floating input_label">Title *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="owner_title" name="executed_owner_operator_title" value="{{$data->executed_owner_operator_title}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_date" class="bmd-label-floating input_label">Date *</label>
                                          <input type="text" class="form-control form_input_field form_fields datepicker field_to_fill field_to_fill_validator" id="executed_owner_date" name="executed_owner_operator_date" value="{{$data->executed_owner_operator_date}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcontractor_initial" class="bmd-label-floating input_label">Owner Operator's Initial *</label>
                                          <input type="text" class="form-control form_input_field form_fields field_to_fill field_to_fill_validator" id="owner_initial" name="executed_owner_operator_initial" value="{{$data->executed_owner_operator_initial}}">
                                        </div>
                                    </div>
                                    
                                  </div>
                                </fieldset>
  
                                @if($owner_sign)
                                <div class="row input_row mt-2" style="margin-bottom: 15px;">
                                  <div class="col-md-12 text-center">
                                      <b>Signature</b>
                                      <br/>
                                      <div id="sig" style="border: 2px solid #DDD">
                                        <img src="{{$owner_sign->url}}">
                                      </div>
                                  </div>
                                </div>
                                @else
                                  <div class="row input_row mt-2" style="margin-bottom: 15px;">
                                    <div class="col-md-12 text-center">
                                        <b>No Signature Uploaded</b>
                                    </div>
                                  </div>
                                @endif

                              </div>

                              <div style="overflow:auto;" class="row msf_btn_holder">
                                <div class="col-md-4 save_btn_wrapper text-left" style="float:right;">
                                  <a class="btn btn-round btn-warning nextPrevBtnOne prev" href="#top_owner_card" type="button" id="prevBtnOwner" onclick="nextPrevOwner(-1)">Previous</a>
                                </div>
                                <div class="col-md-8 save_btn_wrapper text-right">
                                  <a class="btn custom-btn-one btn-round btn-success nextPrevBtnOne next nextBtnAgreement" href="#top_owner_card" type="button" id="nextBtnOwner" onclick="nextPrevOwner(1)">Next</a>
                                  @if($data->status == 2)
                                  <button class="btn btn-round btn-danger nextPrevBtnOne submitBtnOwner nextBtnAgreement" style="display: none" type="button" data-toggle="modal" data-target="#rejectModal">Reject</button>
                                  <button class="btn custom-btn-one btn-round btn-success nextPrevBtnOne submitBtnOwner nextBtnAgreement" style="display: none" type="button" data-toggle="modal" data-target="#submitModal">Submit</button>
                                  @endif
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>


                      <!-- Modal -->
                    <div class="modal fade bs_modal" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"><u>Reason For The Rejection</u></h5>
                            <button type="button" class="close close_icon" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <textarea class="form-control summernote" style="min-width: 100%" rows="6" name="reject"></textarea>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" name="submit" value="reject">Submit</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!-- Modal -->
                  <div class="modal fade bs_modal" id="submitModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel"><u>Submit to Insurance</u></h5>
                          <button type="button" class="close close_icon" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <fieldset class="custom_fieldset_2 contractor_fieldset mt-2 mb-3">
                              <legend>Sending Information</legend>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group bmd-form-group">
                                  <label for="agreement_send_mail_to" class="bmd-label-floating input_label">Send Mail To Insurance</label>
                                    <input type="email" class="form-control form_input_field form_fields" id="agreement_send_mail_to" name="submitted_to_insurance" value="elizabeth@mcanallywilkins.com" required="">
                                  </div>                                          
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group bmd-form-group">
                                  <label for="agreement_send_mail_to" class="bmd-label-floating input_label">Send Mail To 3rd Party</label>
                                    <input type="email" class="form-control form_input_field form_fields" id="agreement_send_mail_to" name="agreement_send_mail_to" value="{{$data->agreement_send_mail_to}}" required="">
                                  </div>                                          
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group bmd-form-group">
                                  <label for="agreement_send_mail_date" class="bmd-label-floating input_label">Date</label>
                                    <input type="text" class="datepicker form-control form_input_field form_fields" id="agreement_send_mail_date" name="submitted_date_insurance" value="{{date('m/d/Y')}}" required="">
                                  </div>                                          
                                </div>
                              </div>
                            </fieldset>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary" name="submit" value="submit_to_insurance">Submit</button>
                        </div>
                      </div>
                    </div>
                  </div>

                    </form>
                </div>
                @endif



              </div>



              </div>
            </div>
          </div>
        </div>


<script>
/*  $(function(){
      $('.sub_agreement_form, .ren_agreement_form').find('input, textarea').attr('readonly', true);
  });*/



$('.month select').on('change',function(){
  $(this).siblings('label').text('')
})

if($('.month select').val()!=""){
  $('.month select').siblings('label').text("");
}


$(document).ready(function() {

  //month label hide
  if($('#subContractoMonth').val() !== ''){
    $('#subContractoMonth').parent().children('label').text('');
  }
  if($('#subContractoMonth2').val() !== ''){
    $('#subContractoMonth2').parent().children('label').text('');
  }

  if($('#contractor_select').val() !== ''){
    $('#contractor_select').addClass( "hasvalue" );
  }

  $('.custom-select.select2').each(function(){
    if($(this).val() !== ""){
      $(this).addClass("hasvalue")
    }
  })

  //year picker 
var date = new Date();
var only_year = date.getFullYear();



  $('#sub_agree_year').yearpicker({
  endYear:only_year,
  year:null,
  template: `<div class="yearpicker-container">
              <div class="yearpicker-header">
                  <div class="yearpicker-prev" data-view="yearpicker-prev">&lsaquo;</div>
                  <div class="yearpicker-current" data-view="yearpicker-current">SelectedYear</div>
                  <div class="yearpicker-next" data-view="yearpicker-next">&rsaquo;</div>
              </div>
              <div class="yearpicker-body">
                  <ul class="yearpicker-year sub_contractor_year" data-view="years">
                  </ul>
              </div>
          </div>`
});

$('#sub_agree_year_2').yearpicker({
  endYear:only_year,
  year:null,
  template: `<div class="yearpicker-container">
              <div class="yearpicker-header">
                  <div class="yearpicker-prev" data-view="yearpicker-prev">&lsaquo;</div>
                  <div class="yearpicker-current" data-view="yearpicker-current">SelectedYear</div>
                  <div class="yearpicker-next" data-view="yearpicker-next">&rsaquo;</div>
              </div>
              <div class="yearpicker-body">
                  <ul class="yearpicker-year sub_contractor_year2" data-view="years">
                  </ul>
              </div>
          </div>`
});

});



$(document).ready(function() {
  $('.summernote').summernote();
});


$('.rent_time select').on('change',function(){
  if($(this).val()!== ""){
    $(this).siblings('label').text("")
  }
})

if($('.rent_time select').val() !== ''){
    $('.rent_time select').siblings('label').text("");
}



</script>
@endsection