<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Subcontractor Agreement</title>
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style>
body{
	font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
}
.request_id_badge{
	background: #f6cb61;
	width:140px;
	height: 30px;
	line-height: 1.3;
	border-radius: 0;
	text-align: center;
	color:white;
	border-radius: 50%;
	position: fixed;
	right: -30px;
	top: -30px;
	border: 1px solid #000;
	border-radius: 15px;
}
.request_id_badge h4{
	padding: 6px 5px;
	font-size:12px;
	font-weight: 300;
	margin: 0;
	text-transform: uppercase;
	color: #000;
}
.insurance_title_secton{
	text-align: center;
}

footer {
    position: absolute; 
    bottom: -30px; 
    left: 0; 
    right: 0;
    color: white;
    height: 45px;
    text-align: center;
} 
.footer-section-table{
	display: table;
	width: 100%;
	vertical-align: middle;
	table-layout: fixed;
}
.footer-section-row{
	display: table-row;
}
.footer-part{
	display: table-cell;
	height: 45px;
	vertical-align: middle;
}
.footer-part img{
	height: 30px;
	display: block;
	text-align: left;
}
.footer-part p{
	margin: 0;
	color: #676767;
	font-size: 12px;
	line-height: 1.3;
}
.insurance_logo img{
	position: fixed;
	top: -40px;
	left: 40%;
	height: 80px;
	transform: translate(-40%, 0);
}
.insurance_title_secton{
	margin-top: 50px;
}
.main_header h2{
	text-align: center;
	margin-top: 50px;
}
.inline-input-field{
	padding: 0 5px;
	display: inline-block;
}
.day{
	width: auto;
	display: inline-block;
}
.month,
.year{
	width: auto;
	display: inline-block;
}
p{
	font-size: 14px;
	line-height: 1.6;
}
.custom-fieldset .info-form-section .row-input {
	margin: 0;
	position: relative;
}
.custom-fieldset .info-form-section .row-input input{
	margin-top:5px; 
	width:100%;
	outline: none;
	border:none; 
	border-bottom:1px solid black;
	font-size:13px;
	height: 18px;
	margin-bottom: 10px;
	display: inline-block;
}
input{
	outline: none;
	border:none; 
	border-bottom:1px solid black;
	font-size:13px;
	height: 18px;
}
.subcontractor_table{
	width: 100%;
}
.subcontractor_table tr td{
	width: 50%;
}
.custom-fieldset{
	margin: 15px 0 10px 0;
	border-radius: 4px;
}
.custom-fieldset table{
	margin-top: 5px;
	padding: 2px;
}
.custom-fieldset legend{
	font-weight: bold;
}
.subcontractor_agre_list{
  padding-left: 30px;
}
.subcontractor_agre_list li{
    font-size: 14px;
    line-height: 1.5;
    margin-bottom: 10px;
    font-weight: bold;
}
.row-input > p{
	margin-bottom: 0;
	position: absolute;
	top: -13px;
	font-size: 12.5px;
	letter-spacing: 0.4px;
	left: 2px;
	font-weight: bold;
}
.insurance_input_field{
	max-width: 150px;
}
.sub-title{
	display: inline-block;
	text-transform: capitalize;
	text-decoration: underline;
	margin-bottom: 0px;
}
.inline-input-field{
	border: none;
	outline: none;
	border-bottom: 1px solid black;
}

</style>
<body>
	<header>
		<div class="request_id_badge">
			<h4 class="title">SC-{{$data->id}}</h4>
		</div>
	    <div class="insurance_logo">
			<img src="{{ public_path() . '/assets/img/logo_ins.png' }}">
		</div>
	</header>
  
	<div class="main">
		<div class="main_header">
			<h2>SUBCONTRACTOR AGREEMENT</h2>
		</div>

		<div class="row">
			<div class="column">
				<p>This Subcontractor Agreement (“Agreement”) is entered into and shall be effective as of the <input type="text" name="" class="inline-input-field day" value="{{$data->day}}"> day of <br> <input type="text" name="" class="inline-input-field month" value="{{$data->month}}"> <input type="text" name="" class="inline-input-field year" value="{{$data->year}}">(the “Effective Date”), by and among EV Oilfield Services, LLC (“Contractor”), and</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<fieldset class="custom-fieldset">
					<legend>Contract Form</legend>
					<table class="subcontractor_table">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Subcontractor</p>
										<span><input type="text" value="{{$data->subcontractor_name}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Type of Entity</p>
										<span><input type="text" value="@if($data->type_of_entity=='limited_partnership') Limited Partnership @elseif($data->type_of_entity=='sole_proprietorship') Sole Proprietorship @else {{ucfirst($data->type_of_entity)}} @endif" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Mailing Address</p>
										<span><input type="text" value="{{$data->mailing_addr}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Physical Address</p>
										<span><input type="text" value="{{$data->physical_addr}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Contact Person</p>
										<span><input type="text" value="{{$data->contact_person}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Title</p>
										<span><input type="text" value="{{$data->contact_person_title}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Email</p>
										<span><input type="text" value="{{$data->contact_person_email}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Phone</p>
										<span><input type="text" value="{{$data->contact_person_phone}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<!-- <tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Fax</p>
										<span><input type="text" value="{{$data->contact_person_fax}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr> -->
					</table>
				</fieldset>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<p>Contractor and Subcontractor may be individually referred to herein as a “Party” or collectively as the “Parties.”</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">CONSPICUOUS AND FAIR NOTICE</h3>
				<p>Both parties represent to each other that --</p>
				<ul class="subcontractor_agre_list">
					<li>They have consulted an attorney concerning this agreement or, if they have not consulted an attorney, they were given the opportunity and had the ability to consult, but made an informed decision not to do so, and</li>
					<li>They fully understand their rights and obligations under this agreement.</li>
				</ul>
				<p>Now, Therefore, in consideration of the mutual promises, conditions and agreements herein contained, the sufficiency of which is acknowledged, Contractor and Subcontractor hereby agree as follows:</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">1. Scope of Agreement</h3>
				<p>Work for Hire. It is contemplated that from time to time Subcontractor will be requested by Contractor or its present or future affiliated entities to perform certain work and services (“Work”) . Neither Contractor nor its affiliates shall be obligated to request Subcontractor to perform any Work, and Subcontractor shall not be obligated to accept requests to perform Work from either Contractor or its affiliates, but it is expressly understood and agreed that any and all Work requested by Contractor or its affiliates and accepted by Subcontractor shall be controlled and governed by the provisions of this Agreement. If and when Contractor desires Subcontractor to perform Work hereunder, Contractor will issue a Work order describing the Work Contractor desires Subcontractor to perform and the compensation Contractor will pay Subcontractor for the Work described in the Work order. Subcontractor shall notify Contractor in writing within five (5) business days of Subcontractor’s receipt of such Work order if Subcontractor intends to accept the Work order. The term “Agreement” means this Agreement as incorporated in a work order and the term “Contractor” as used herein shall mean the Contractor or affiliated entity that issued the work order.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">2. Term and Termination</h3>
				<p>This Agreement shall remain in effect for a period of one year from the Effective Date, and from year to year thereafter, subject to the right of either party here to cancel or terminate the Agreement at any time upon not less than thirty (30) days written notice of one party to the other.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">3. Representations and Warranties</h3>
				<p>Subcontractor will furnish all necessary materials, equipment, permits, and certificates that are required for the Work. All of Subcontractor’s materials and equipment are suitable for their intended use, and are free from all faults and defects. Subcontractor will remove and replace any defective materials or Work forthwith on notice from Contractor.Subcontractor will perform the Work entirely at Subcontractor’s risk. Subcontractor will provide all proper and sufficient and necessary safeguards against all injuries and damage whatsoever, and to comply with all safety requirements imposed by law. Subcontractor shall conform to Contractor’s reasonable progress schedule. Subcontractor shall perform the Work in a professional, prompt, and diligent manner, consistent with applicable law and industry standards, without delaying or hindering Contractor’s work. If Subcontractor shall default in performance of the work or otherwise commit any act which causes delay to Contractor’s work, Subcontractor shall be liable for all losses, costs, expenses, liabilities and damages, including actual damages, consequential damages and any liquidated damages sustained by Contractor. Subcontractor will comply with all applicable federal, state and local laws, codes, ordinances, rules, regulations, orders and decrees of any government or quasi-government entity having jurisdiction over the project, the project site, the practices involved in the Work, or any subcontract work. Subcontractor, its agents and employees are properly trained and qualified to complete the Work for which they have been hired to do. </p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">4. Indemnification</h3>
				<p>“Claims” shall include, without limitation, any and all claims, losses, damages (including, without limitation, punitive damages and damages for bodily injury, death or property damage ), causes of action, fines, penalties, enforcement proceedings, suits, and liabilities of every nature or character (including interest and all expenses of litigation, court costs, and attorneys' fees), whether or not arising in tort, contract, strict liability, under statute, or of any other character whatsoever, and whether or not caused by a legal duty. “Subcontractor Group” means subcontractor, its parent, subsidiary and affiliated companies, and their contractors (of whatever tier), and its and their respective directors, officers, employees, agents, and representatives. “Contractor Group” means Contractor, its parent, subsidiary and affiliated companies, its and their co-lessees, partners, joint ventures, co-owners, contractors (other than Subcontractor), and its and their respective directors, officers, employees, agents, and representatives.Subcontractor shall defend, indemnify, and hold harmless the Contractor Group for any Claims which any or all of the Contractor Group may incur in connection with any third party Claim arising out of or relating to the Subcontractor Group’s (or any member of the Subcontractor Group’s) actual or alleged negligence, willful misconduct, breach of this Agreement or violation of Law. The foregoing indemnification obligation includes any Claims asserted against the Contractor Group (or any member thereof) arising from pollution or contamination which originates above the surface of the land or water from spills of fuels, lubricants, motor oils, pipe dope, paints, solvents, cleaning solutions or other liquids, fumes and garbage which is in any manner associated with or alleged to have been associated with, resulting from or caused by Subcontractor Group’s Work, equipment or personnel.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">5. Insurance</h3>
				<p>Subcontractor, at its own cost and expense, shall procure, carry, and maintain insurance coverage satisfactory to Contractor, which shall include not less than the following coverage:</p>
				<ul class="subcontractor_agre_list">
					<li>General liability insurance with limits not less than $<input type="text" name="" value="{{$data->general_liability_limit}}" class="inline-input-field insurance_input_field"> per occurrence.</li>
					<li>Automobile Liability insurance with limits not less than $<input type="text" name="" value="{{$data->automobile_liability_limit}}" class="inline-input-field insurance_input_field"> per occurrence.</li>
					<li>Umbrella insurance with limits not less than $<input type="text" name="" value="{{$data->umbrella_limit}}" class="inline-input-field insurance_input_field"> per occurrence.</li>
					<li>Workers Compensation and Employers’ Liability with limits not less than $<input type="text" name="" value="{{$data->employer_liability_limit}}" class="inline-input-field insurance_input_field">.</li>
					<li>Riggers Liability Insurance with limits not less than $<input type="text" name="" value="{{$data->riggers_liability_limit}}" class="inline-input-field insurance_input_field"> per occurrence.</li>
					<li>Motor Trade Cargo Insurance with limits not less than $<input type="text" name="" value="{{$data->motor_trade_limit}}" class="inline-input-field insurance_input_field"> per occurrence.</li>
					<li>Installation Floater Insurance with limits not less than $<input type="text" name="" value="{{$data->installation_floater_limit}}" class="inline-input-field insurance_input_field"> per occurrence.</li>
					<li>Any other insurance required by Contractor.</li>
				</ul>
				<p>Subcontractor shall name Contractor as an additional insured on all policies of insurance. Subcontractor shall cause its insurance carrier to forward forthwith to Contractor a standard certificate of insurance such certificate shall require the insurance carrier to give Contractor written thirty (30) days prior notice of the cancellation of such insurance. Subcontractor shall not perform any work under the Agreement until a Certificate of Insurance has been delivered to Contractor. If Subcontractor fails to furnish current evidence upon demand of any insurance required hereunder, or if any insurance is cancelled or materially changed, Contractor may suspend or terminate this Agreement until insurance is obtained. The insurance coverage required herein shall in no way limit the Subcontractor’s indemnification obligations or other liability under this Agreement. </p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">6. Waiver of Subrogation</h3>
				<p>Subcontractor hereby waives any right of subrogation that it, or anyone claiming through or under it, may have against contractor, its agents, employees, or anyone for whom blakely construction company, inc. may be responsible, for any loss or damage shall have been caused by the fault or negligence of contractor. However, this waiver shall not adversely or impair any policies of insurance or prejudice the right of either party to recover thereunder. Subcontractor shall provide an endorsement from subcontractor’s subcontractor or subcontractors that any right of subrogation is waived as against contractor and that such waiver will not impair subcontractor's policies of insurance. Any failure or delay by contractor to require such an endorsement shall not constitute a waiver, nor shall it otherwise affect this mutual waiver of subrogation.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">7. Independent Contractor</h3>
				<p>Subcontractor shall have the status of an "independent contractor". Subcontractor shall employ and direct all persons engaged in the performance of any and all of its services under this Agreement, and such agents, servants or employees are subject to the sole control and direction of Subcontractor, and shall not be agents, servants or employees of Contractor. Subcontractor’s agents, contractors and employees shall not be entitled to any compensation, benefits or other remuneration from Contractor. Contractor shall not have any authority to supervise or direct the manner in which Subcontractor shall perform the services to be rendered hereunder. Subcontractor shall provide and be responsible for providing, using and maintaining all equipment, machinery and tools necessary to perform the Work.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">8. Notices</h3>
				<p>All notices and other communications pertaining to this Agreement shall be in writing and shall be deemed to have been given by a party hereto if personally delivered to the other party or if sent by certified mail, return receipt requested, postage prepaid. All notices shall be addressed as shown below</p>

				<fieldset class="custom-fieldset" style="margin-bottom: 30px;">
					<legend>Contractor Form</legend>
					<table class="subcontractor_table ">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Contractor</p>
										<span><input type="text" value="{{$data->notice_contractor_name}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Address</p>
										<span><input type="text" value="{{$data->notice_contractor_address}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Phone</p>
										<span><input type="text" value="{{$data->notice_contractor_phone}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Email</p>
										<span><input type="text" value="{{$data->notice_contractor_email}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Attn</p>
										<span><input type="text" value="{{$data->notice_contractor_attn}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>

					</table>
				</fieldset>

				<fieldset class="custom-fieldset">
					<legend>Subcontractor Form</legend>
					<table class="subcontractor_table ">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Subcontractor</p>
										<span><input type="text" value="{{$data->notice_subcontractor_name}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Address</p>
										<span><input type="text" value="{{$data->notice_subcontractor_address}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Phone</p>
										<span><input type="text" value="{{$data->notice_subcontractor_phone}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Email</p>
										<span><input type="text" value="{{$data->notice_contractor_email}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Contact</p>
										<span><input type="text" value="{{$data->notice_subcontractor_contact}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>

					</table>
				</fieldset>
					
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">9. Breach of Agreement</h3>
				<p>If Subcontractor breaches any of the terms of this agreement, then Contractor has the option, at its sole and absolute discretion, to terminate this agreement immediately, without written notice. Failure of Contractor to insist upon Subcontractor’s performance under this Agreement or to exercise any right or privilege shall not be a waiver of any of Contractor’s rights or privileges contained herein.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">10. Record Keeping</h3>
				<p>For the purposes of permitting verification by the Contractor of any amounts paid to or claimed by the Subcontractor, the Subcontractor shall keep and preserve, for not less than two years from date of invoice all general ledgers, work orders, receipts, disbursements journals, bids, bid proposals, price lists, and supporting documentation obtained from manufacturers and suppliers in connection with the Work performed.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">11. Reports</h3>
				<p>Subcontractor shall provide to Contractor an oral report, confirmed in writing as soon as practicable, of all accidents or occurrences resulting in death or injuries to Subcontractor’s employees, subcontractors, or any third parties, damage to Contractor’s property, or physical damage to the property of Contractor or any third party, arising out of or during the course of work to be performed. Subcontractor shall furnish Contractor with a copy of all reports made by Subcontractor to Subcontractor’s insurer, governmental authorities, or to others of such accidents or occurrences. </p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">12. Payment Terms</h3>
				<p>Contractor agrees to pay Subcontractor for any work at the rates set forth in the applicable Work order. Subcontractor shall promptly invoice Contractor upon the completion of any Work, but in any event shall invoice Contractor (i) at least once every thirty (30) days and (ii) within ten (10) days following completion of all of the Work under any applicable Work order or purchase order, and (iii) in the unlikely event that Work is completed without an applicable work order or purchase order, within ten (10) days of completion of such Work at Subcontractor’s best rates in the geographical area where the Work was completed. Contractor shall remit full payment of all undisputed portions of any and all invoices in full in United States funds by check, bank draft, money order, or bank/wire transfer payable to Contractor at its offices (or bank account) within sixty (60) days following actual receipt of Subcontractor’s invoice. In the event Contractor has a bona fide question or dispute concerning a Subcontractor invoice or a portion thereof, Contractor may withhold the disputed portion of the payment only. Contractor shall give written notice of any disputed amounts to Subcontractor specifying the reasons therefore within sixty (60) days after receipt of the applicable invoice. Invoices submitted later than one hundred twenty (120) days will not be considered for reimbursement.Contractor shall not pay or reimburse Subcontractor for any travel or other expenses unless Contractor approves such expenses in advance and in writing.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">13. Audit</h3>
				<p>Subcontractor shall maintain, and shall cause any of Subcontractor’s Subcontractors to maintain, a true and correct set of records pertaining to all work performed under each work order, including supporting documentation, for two (2) years following completion of the Work. Contractor may, at its expense require Subcontractor, or any of Subcontractor’s Subcontractors, at any time within said two-year period to furnish sufficient evidence, with documentary support, to enable Contractor to verify the correctness and accuracy of payments to Subcontractor or such Subcontractors. Contractor may, following written notice to Subcontractor or such Subcontractor, audit any and all records of Subcontractor and any Subcontractor relating to the work performed by or on behalf of Subcontractor hereunder, and all payments made in regard thereto, in order to verify the accuracy and compliance with this provision; provided however, Contractor shall not have the right to inspect or audit Subcontractor’s trade secrets or any proprietary information. If Contractor’s examination discloses that Subcontractor’s invoices to Contractor were in error, Subcontractor will immediately pay to Contractor any amounts overpaid by Contractor, plus interest from the date of the error at the lesser of one percent (1%) per month or the maximum rate allowed by law. </p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">14. No delay</h3>
				<p>In no case (unless otherwise directed in writing by the Contractor) shall any claim or dispute, whether or not involving litigation, permit the Subcontractor to delay or discontinue any of the work hereunder, but rather the Subcontractor shall diligently pursue the work hereunder while awaiting the resolution of any dispute or claim; provided, however, that the Contractor shall not withhold, pending the resolution of any claim or dispute, the payment of any undisputed sum otherwise due Subcontractor under this Agreement. </p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">15. Drugs, Alcohol & Firearms</h3>
				<p>To help ensure a safe, productive work environment, Contractor prohibits the use, transportation and possession of firearms, drugs and/or controlled substances, drug paraphernalia and alcoholic beverages. Illegal drugs shall include, but not be limited to, marijuana, amphetamines, barbiturates, opiates, cocaine, codeine, morphine, hallucinogenic substances (LSD) and any similar drugs and/or chemical synthetics deemed hazardous by Contractor. Such prohibitions shall apply to Subcontractor and its employees, agents, servants and subcontractors. Subcontractor’s employees, agents, contractors and invitees shall abide the more stringent of Subcontractor’s or Contractor’s drug, alcohol and firearm policy. Contractor may request that Subcontractor carry out drug and alcohol tests of its employees and/or that Subcontractor carry out reasonable searches of individuals, their personal effects and vehicles when entering on and leaving assigned premises at any time, at scheduled times, or at random. Individuals found in violation shall be removed from the premises by Subcontractor immediately. Submission to such a search is strictly voluntary, however, refusal may be cause for not allowing that individual on the well site or Contractor’s other premises. Subcontractor shall (1) test at Subcontractor’s expense any individual involved in or related to an accident or injury within twelve (12) hours of such accident or injury and (2) submit to Contractor any drug or alcohol test results for any individual involved in or related to an accident or injury on Contractor’s premises. It is Subcontractor’s responsibility to notify its employees, contractors, subcontractors, agents and invitees of this prohibition, the provisions of this paragraph and its enforcement and obtain any acknowledgement or release from any person in order to comply with this provision and applicable law. </p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">16. Miscellaneous</h3><br>
				<h3 class="sub-title">A. Governing Law</h3>
				<p>The laws of the State of Texas shall apply and govern the validity, interpretation, and performance of this Agreement. The Parties waive any right to trial by jury in any action arising out of or relating to this Agreement or the Parties’ relationship. </p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">B. Severability</h3>
				<p>If any part of this Agreement contravenes any applicable statutes, regulations, rules, or common law requirements, then, to the extent of and only to the extent of such contravention, such part shall be severed from this Agreement and deemed non-binding while all other parts of this Agreement shall remain binding.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">C. Amendment</h3>
				<p>This Agreement may not be modified or amended unless it is in writing and signed by both Parties.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">D. Assignment</h3>
				<p>Neither Party shall assign all or any part of its rights or obligations under this Agreement without the prior written consent from the other Party.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">E. Compliance with Laws</h3>
				<p>Subcontractor agrees to comply with all laws, statutes, codes, rules, and regulations, which are now or may hereafter become applicable to Work covered by this Agreement or arising out of the performance of such operations.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">F. Headings</h3>
				<p>The headings, sub-headings, and other subdivisions of this Agreement are inserted for convenience only. The Parties do not intend them to be an aid in legal construction.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<h3 class="sub-title">G. Authority to Sign and Bind</h3>
				<p>By their signatures below, Subcontractor represents that the person signing this Agreement has the authority to execute same on behalf of Subcontractor and to bind Subcontract to the obligations set forth herein.</p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<p>Executed this <input type="text" name="" class="inline-input-field day" value="{{$data->executed_day}}"> day of <input type="text" name="" class="inline-input-field month" value="{{$data->executed_month}}"> <input type="text" name="" class="inline-input-field year" value="{{$data->executed_year}}"></p>
			</div>
		</div>

		<div class="row">
			<div class="column">
				<br><br><br><br><br><br>
				<fieldset class="custom-fieldset" style="margin-bottom: 30px !important;">
					<legend>Contractor</legend>
					<table class="subcontractor_table">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Contractor</p>
										<span><input type="text" value="{{$data->executed_contractor_name}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Contractor By</p>
										<span><input type="text" value="{{$data->executed_contractor_by}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Title</p>
										<span><input type="text" value="{{$data->executed_contractor_title}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Date</p>
										<span><input type="text" value="{{$data->executed_contractor_date}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p>Initial</p>
										<span><input type="text" value="{{$data->contractor_initial}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</fieldset>

				<fieldset class="custom-fieldset" style="margin-bottom: 30px !important;">
					<legend>Sub Contractor</legend>
					<table class="subcontractor_table">
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Sub Contractor</p>
										<span><input type="text" value="{{$data->executed_subcontractor_name}}" name="" /></span>
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Sub Contractor By</p>
										<span><input type="text" value="{{$data->executed_subcontractor_by}}" name=""/></span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Title</p>
										<input type="text" value="{{$data->executed_subcontractor_title}}" name="" />
									</div>
								</div>
							</td>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Date</p>
										<input type="text" value="{{$data->executed_subcontractor_date}}" name="" />
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="info-form-section">
									<div class="row-input">
										<p> Initial</p>
										<span><input type="text" value="{{$data->subcontractor_initial}}" name="" /></span>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</fieldset>

			<fieldset class="custom_fieldset contractor_fieldset info_div">
		      <legend> Necessary Files </legend>
		      <div class="row input_row poins_row mb-3">
		      	<table>
		        <td class="col-md-12">
			            <p> W9 </p>
			            <div class="uploaded-file">
			            @foreach($agreement_files as $rowdata)
			            	<div> <a href="{{$rowdata->url}}">{{$rowdata->name}}</a> </div>
			            @endforeach
			            </div>
		        </td>
		        <td style="border-left:2px solid gray; padding-left:10px">
			            <p> Insurance Certificate </p>
			            <div class="uploaded-file">
			            @foreach($insurance_files as $rowdata)
			            	<div> <a href="{{$rowdata->url}}">{{$rowdata->name}}</a> </div>
			            @endforeach
			            </div>
		        </td>
		        </table>		        
		      </div>
		    </fieldset>

		    @if($sc_sign)
	                <fieldset class="custom-fieldset" style="margin-bottom: 30px !important;">
	                <legend>Signature</legend>
	                    <img src="{{ public_path() . '/storage/files/'. $sc_sign->name }}" width="100%">
	                </fieldset>
	        @endif

			</div>
		</div>

	</div>

	<footer>
	    <div class="footer-section-table">
	    	<div class="footer-section-row">
		    	<div class="footer-part footer-one">
		    		<img src="{{ public_path() . '/assets/img/logo.png' }}">
		    	</div>
		    	<div class="footer-part footer-two">
		    		<p>903 W. Industrial Ave. Midland, TX 79701</p>
		    	</div>
		    	<div class="footer-part footer-three">
		    		<p></i>432-253-9651</p>
		    	</div>
		    	<div class="footer-part footer-four">
		    		<p>info@evoilfieldservices.com</p>
		    	</div>
		    </div>
	    </div>
	</footer>

</body>
</html>
