@extends('layouts.master')

@section('content')

  <div class="content add_vendor_content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 mx-auto">             
          <div class="card vendor_root_card">
            <div class="card-header card-header-tabs card-header-rose custom_card_header">
              <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                  <p class="nav-tabs-title">Add Vendor</p>
                  <ul class="nav nav-tabs" data-tabs="tabs" id="tabMenu">
                    <!-- <li class="nav-item">
                      <a class="nav-link" href="#quickbooks_vendor" data-toggle="tab">
                        <i class="material-icons">account_circle</i> Quickbooks
                        <div class="ripple-container"></div>
                      <div class="ripple-container"></div></a>
                    </li> -->
                    <li class="nav-item">
                      <a class="nav-link show active" href="#custom_vendor" data-toggle="tab">
                        <i class="material-icons">account_circle</i> Manual
                        <div class="ripple-container"></div>
                      <div class="ripple-container"></div></a>
                    </li>
                    <!-- <li class="nav-item">
                      <a class="nav-link show" href="#pre_trip_report" data-toggle="tab">
                        <i class="material-icons">account_circle</i> Pre-Trip Report
                        <div class="ripple-container"></div>
                      <div class="ripple-container"></div></a>
                    </li>

                    <li class="nav-item">
                      <a class="nav-link show" href="#jobs_site_analysis" data-toggle="tab">
                        <i class="material-icons">account_circle</i> Jobs Site Hazard analysis
                        <div class="ripple-container"></div>
                      <div class="ripple-container"></div></a>
                    </li> -->


                  </ul>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="tab-content">
                <div class="tab-pane tab_pane_card" id="quickbooks_vendor">
                  <div class="card lead_card">
                    <div class="card-header card-header-rose card-header-text lead_card_header user_sub_card">
                      <div class="card-icon">
                          <i class="material-icons">supervised_user_circle</i>
                      </div>
                      <!-- <div class="card-text"> -->
                        <h4 class="card-title">QUICKBOOKS DETAILS</h4>
                    </div>
                    <div class="card-body vendor_form_details">
                      <div style="margin-top: 15px;">
                          @include('partials.messages')
                      </div>
                      
                      <div class="card-body">
                            
                              <?php
                                $authUrl = $authUrl;
                              ?>

                              <a class="imgLink" href="#" onclick="oauth.loginPopup()"><img src="{{ asset('assets/img/quickbooks_connect_btn.png') }}" width="178" /></a>

                              <p>If there is no access token or the access token is invalid, click the <b>Connect to QuickBooks</b> button</p>


                              <pre id="accessToken" style="background-color:#efefef;overflow-x:scroll"">
                                <?php
                                  $displayString = isset($accessTokenJson) ? $accessTokenJson : "No Access Token Generated Yet";
                                  echo json_encode($displayString, JSON_PRETTY_PRINT); 
                                ?>
                              </pre>
                              
                              <hr />

                              @if(isset($accessTokenJson))
                                  <h5><b>Import new Vendors (Incremental)</b>                
                                    <button  type="button" class="btn btn-success btn-sm" onclick="apiCall.getVendor()">Import Vendor
                                    </button>
                                  </h5>

                                  <p id="loading_text" style="display: none;">Vendor data is importing from Quickbooks for review, Please wait.</p>

                                  <img src="{{asset('assets/img/loader.gif')}}" id="loader" style="display: none;">
                                  <p id="apiCall"></p>
                                  <hr />

                                  
                                  <div class="form-group bmd-form-group">
                                    <div class="togglebutton admin-role-toggle">
                                      
                                    <span style="color: black; font-size: 15px;">Synchronize Vendor data</span>
                                      <label style="color: black; font-size: 15px; font-weight:400 ">
                                          <input type="checkbox" name="" checked="" id="syncCheck">
                                           From Quickbooks to EVOS <span class="toggle"></span> From EVOS to Quickbooks
                                      </label>

                                      <button  type="button" class="btn btn-success btn-sm syncbtn" onclick="sync()" style="margin-left: 10px;">Synchronize</button>

                                    </div>
                                  </div>

                                  <p id="loading_text_sync" style="display: none;">Vendor data is synchronizing with Quickbooks, Please wait.</p>
                                  <img src="{{asset('assets/img/loader.gif')}}" id="loader_sync" style="display: none;">
                                  <p id="apiCall_sync"></p>

                                  <p id="loading_text_qb_sync" style="display: none;">Quickbooks is synchronizing with Evo vendors, Please wait.</p>
                                  <img src="{{asset('assets/img/loader.gif')}}" id="loader_qb_sync" style="display: none;">
                                  <p id="apiCall_qb_sync"></p>

                              @endif
                          </div>

                    </div>
                  </div>
                </div>

                <div class="tab-pane tab_pane_card active" id="custom_vendor">
                  <div class="card lead_card">
                    <div class="card-header card-header-rose card-header-text lead_card_header user_sub_card">
                      <div class="card-icon">
                          <i class="material-icons">supervised_user_circle</i>
                      </div>
                      <!-- <div class="card-text"> -->
                        <h4 class="card-title">Vendor DETAILS</h4>
                    </div>
                    <div class="card-body vendor_form_details">
                      <div style="margin-top: 15px;">
                          @include('partials.messages')
                      </div>
                      <form method="post" action="{{ route('vendors.store') }}" class="form-horizontal" enctype="multipart/form-data" onsubmit="return validateMyForm();">
                        @csrf


                        <div class="card customer_top_card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Basic</h4>
                                  </div>
                                </div>
                                <div class="card-body ">

                                  <div class="row">
                                    <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="company_name" class="bmd-label-floating input_label">Company Name</label>
                                          <input type="text" class="form-control form_input_field" id="company_name" name="companyName">
                                        </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="display_name" class="bmd-label-floating input_label">Display Name</label>
                                          <input type="text" class="form-control form_input_field" id="display_name" name="displayName" required="true" aria-required="true">
                                        </div>
                                    </div>
                                  </div>
                                  
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="title" class="bmd-label-floating input_label">Title</label>
                                          <input type="text" class="form-control form_input_field" id="title" name="title">
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="given_name" class="bmd-label-floating input_label">Given Name *</label>
                                          <input type="text" class="form-control form_input_field" id="given_name" name="givenName">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="middle_name" class="bmd-label-floating input_label">Middle Name</label>
                                          <input type="text" class="form-control form_input_field" id="middle_name" name="middleName">
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="family_name" class="bmd-label-floating input_label">Family Name</label>
                                          <input type="text" class="form-control form_input_field" id="family_name" name="familyName">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="suffix" class="bmd-label-floating input_label">Suffix</label>
                                          <input type="text" class="form-control form_input_field" id="suffix" name="suffix">
                                        </div>
                                      </div>
                                    </div>

                                </div>
                        </div>


                        <div class="card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Phone & Email</h4>
                                  </div>
                                </div>
                                <div class="card-body ">
                                  
                                    <div class="row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="primaryPhone_freeFormNumber" class="bmd-label-floating input_label">Free Form No</label>
                                          <input type="text" class="form-control form_input_field" id="primaryPhone_freeFormNumber" name="primaryPhone_freeFormNumber">
                                        </div>
                                      </div>
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="primaryEmailAddr_addess" class="bmd-label-floating input_label">Email Address</label>
                                          <input type="email" class="form-control form_input_field" id="primaryEmailAddr_addess" name="primaryEmailAddr_addess" >
                                        </div>
                                      </div>
                                    </div>

                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="mobile" class="bmd-label-floating input_label">Mobile</label>
                                          <input type="text" class="form-control form_input_field" id="mobile" name="mobile">
                                        </div>
                                      </div>
                                      <!-- <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="fax" class="bmd-label-floating input_label">Fax</label>
                                          <input type="text" class="form-control form_input_field" id="fax" name="fax">
                                        </div>
                                      </div> -->
                                    </div>
                                    <div class="row input_row">
                                      <div class="col-md-6 input_wrapper">
                                        <div class="form-group bmd-form-group">
                                          <label for="alternatePhone" class="bmd-label-floating input_label">Other No</label>
                                          <input type="text" class="form-control form_input_field" id="alternatePhone" name="alternatePhone">
                                        </div>
                                      </div>
                                    </div>

                                </div>
                              </div>


                              <div class="card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Billing Address</h4>
                                  </div>
                                </div>

                                <div class="card-body ">
                                  <div class="row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_line1" class="bmd-label-floating input_label">Line 1</label>
                                        <input type="text" class="form-control form_input_field" id="billAddr_line1" name="billAddr_line1">
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_line2" class="bmd-label-floating input_label">Line 2</label>
                                        <input type="text" class="form-control form_input_field" id="billAddr_line2" name="billAddr_line2">
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <div class="row input_row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_city" class="bmd-label-floating input_label">City</label>
                                        <input type="text" class="form-control form_input_field" id="billAddr_city" name="billAddr_city">
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_country" class="bmd-label-floating input_label">Country</label>
                                        <input type="text" class="form-control form_input_field" id="billAddr_country" name="billAddr_country">
                                      </div>
                                    </div>
                                    
                                  </div>
                                  <div class="row input_row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Country SubDivision Code</label>
                                        <input type="text" class="form-control form_input_field" id="billAddr_countrySubDivisionCode" name="billAddr_countrySubDivisionCode">
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="billAddr_postalCode" class="bmd-label-floating input_label">Postal Code</label>
                                        <input type="text" class="form-control form_input_field" id="billAddr_postalCode" name="billAddr_postalCode">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>


                              <div class="card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Others</h4>
                                  </div>
                                </div>

                                <div class="card-body ">
                                  
                                  <div class="row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="taxIdentifier" class="bmd-label-floating input_label">Tax Identifier</label>
                                        <input type="text" class="form-control form_input_field" id="taxIdentifier" name="taxIdentifier">
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="accountNum" class="bmd-label-floating input_label">Account Number</label>
                                        <input type="text" class="form-control form_input_field" id="accountNum" name="accountNum">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="webAddr" class="bmd-label-floating input_label">Web Address</label>
                                        <input type="url" class="form-control form_input_field" id="webAddr" name="webAddr">
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="notes" class="bmd-label-floating input_label">Notes</label>
                                        <input type="text" class="form-control form_input_field" id="notes" name="notes">
                                      </div>
                                    </div>
                                  </div>

                                </div>
                              </div>


                        <div class="card">
                          <div class="card-header card-header-rose card-header-text sub_card">
                            <div class="card-text card_text_title">
                                <h4 class="card-title">Identity Section</h4>
                            </div>
                          </div>

                          <div class="card-body">
                            <div class="row">

                              <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group">
                                    <label for="reg_number_1" class="bmd-label-floating input_label">Registration Number</label>
                                    <input type="text" class="form-control form_input_field" id="reg_number_1" name="reg_number">
                                  </div>
                              </div>
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group">
                                    <label for="lis_number_1" class="bmd-label-floating input_label">License Number</label>
                                    <input type="text" class="form-control form_input_field" id="lis_number_1" name="license_number">
                                  </div>
                                </div>

                            </div>
                            

                            <div class="row">
                              <div class="col-md-12">
                                <h4 class="title">Image</h4>
                                <div class="input-images">
                                  
                                </div>
                                <div id="genModal" class="inv_img_modal">
                                  <span class="gen_close inv_img_close">×</span>
                                  <img class="inv_modal_content" id="gen_modal_img">
                                  <div id="caption"></div>
                                </div>
                              </div>
                            </div>

                                                
                          </div>
                        </div>


                        <!-- Manual Section  -->
                        <div class="card resource_section_card">
                          <div class="card-header card-header-rose card-header-text sub_card">
                            <div class="card-text card_text_title">
                                <h4 class="card-title">Resource Section</h4>
                            </div>
                          </div>

                          <div class="card-body">
                            <div class="row resource_row">
                              <div class="col-md-3 justify-content-center d-flex align-items-center resource_toggle_div">
                                  <div class="togglebutton admin-role-toggle resource_toggle_section ">
                                      <label>
                                          <input class="resource_id" type="checkbox" name="" checked="">
                                          Existing <span class="toggle"></span> New
                                      </label>
                                  </div>
                              </div>
                              <div class="col-md-9 resource_toggle_content_div">
                                  <div class="row" id="existing_resource">
                                    <div class="col-md-4 input_wrapper top_input_wrapper custom_select2_col">
                                      <div class="form-group custom-form-select">
                                        <select class="custom-select select2" id="vendor_resource_name_select">
                                            <option class="form-select-placeholder"></option>
                                          @foreach($resources as $rowdata)
                                            <option value="{{$rowdata->resource_name}}" r_id = "{{$rowdata->id}}" r_code ="{{$rowdata->resource_code}}" >{{$rowdata->resource_name}}</option>
                                          @endforeach
                                        </select>
                                        <div class="form-element-bar">
                                        </div>
                                        <label class="form-element-label input_label" id="vendor_resource_name_existing" for="resource_name">Resource Name</label>
                                      </div>
                                    </div>
                                    <div class="col-md-4 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                          <label for="resource_code_exist" class="bmd-label-floating input_label">Resource Code</label>
                                          <input type="text" class="form-control form_input_field" id="vendor_resource_code_existing" name="res_code">
                                      </div>
                                    </div>
                                    <div class="col-md-4 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                          <label for="model_no_exist" class="bmd-label-floating input_label">Model Number</label>
                                          <input type="text" class="form-control form_input_field" id="vendor_model_no_exist" name="model_no">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row" id="new_resource" style="display: none">
                                    <div class="col-md-4 input_wrapper top_input_wrapper">
                                      <div class="form-group bmd-form-group">
                                          <label for="resource_name_new" class="bmd-label-floating input_label">Resource Name</label>
                                          <input type="text" class="form-control form_input_field" id="resource_name_new" name="res_name_new">
                                      </div>
                                    </div>
                                    <div class="col-md-4 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                          <label for="resource_code_new" class="bmd-label-floating input_label">Resource Code</label>
                                          <input type="text" class="form-control form_input_field" id="resource_code_new" name="res_code_new">
                                      </div>
                                    </div>
                                    <div class="col-md-4 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                          <label for="model_no_new" class="bmd-label-floating input_label">Model Number</label>
                                          <input type="text" class="form-control form_input_field" id="model_no_new"  name="model_no_new">
                                      </div>
                                    </div>
                                    <!-- <div class="col-md-1">
                                      <a href="#" class="btn btn-link btn-danger btn-just-icon remove disabled"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                                    </div> -->
                                  </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-check add_variable_checkbox">
                                  <label class="form-check-label variable_lebel">
                                    <input class="form-check-input variable_checkbox" type="checkbox" value="">Do you want to add variables?
                                    <span class="form-check-sign">
                                      <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                              </div>
                            </div>

                            <!-- Variable Section -->

                            <div class="card variable_card" style="display: none;">
                              <div class="card-header card-header-rose card-header-text sub_card">
                                <div class="card-text card_text_title">
                                    <h4 class="card-title">Variables</h4>
                                </div>
                              </div>
                              <div class="card-body variable_card_body">
                                <div class="row top_variable_row input_row" id="var_row_create">
                                  <div class="col-md-4 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="field_name" class="bmd-label-floating input_label">Name Of the Field</label>
                                      <input type="text" class="form-control form_input_field" name="var_name[]" aria-required="true">
                                    </div>
                                  </div>
                                  <div class="col-md-4 input_wrapper">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select fa" id="" name="var_type[]" aria-required="true">
                                        <option class="form-select-placeholder"></option>
                                        <option class="" value="input">&#xf044; Input Field</option>
                                        <option class="" value="checkbox">&#xf14a; Checkbox</option>
                                        <option class="" value="datetime">&#xf073; Date & time</option>
                                        <option class="" value="radio">&#xf192; Radio</option>
                                      </select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label top_select_input_label" for="var_type">Select Field Type</label>
                                    </div>
                                  </div>
                                  <div class="col-md-3 d-flex align-items-center justify-content-center variable_toggle">
                                    <div class="togglebutton">
                                        <label>
                                            <input type="checkbox" name="enable_disable[]" class="enable_disable">
                                            Disable <span class="toggle"></span> Enable
                                        </label>
                                    </div>
                                  </div>
                                  <div class="col-md-1 variable_cross_btn">
                                    <a href="#" class="btn btn-link btn-danger btn-just-icon remove disabled"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                                  </div>
                                </div>
                                <div class="row add_variable_btn_wrapper">
                                  <div class="col-md-12 text-right">
                                    <button class="btn btn-info btn-round btn-sm add_variable_btn" type="button">
                                      <span class="btn-label">
                                        <i class="material-icons">add</i>
                                      </span>
                                      Add More
                                      <div class="ripple-container"></div>
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-12 text-right add_vendor_resource_btn_wrapper">
                                <button class="btn btn-warning btn-round btn-sm add_vendor_resource_btn" type="button">
                                  Add Resources
                                  <div class="ripple-container"></div>
                                </button>
                              </div>
                            </div>

                            <!-- Showing The correspondence resource table -->
                            
                            <div class="row" id="resource_table_row" style="display: none">
                              <div class="col-md-12 mx-auto">
                                <div class="table-responsive table-sales">
                                  <table class="table resource_viewer_table">
                                    <thead>
                                        <tr class="table-success">
                                          <th>Resource Name</th>
                                          <th>Resource Code</th>
                                          <th>Model Number</th>
                                          <th>Variables</th>
                                          <th>Edit</th>
                                          <th class="disabled-sorting text-right">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table_body vendor_res_table_body">
                                      
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                              <!-- Edit Modal -->
                              <div id="myEditModal" class="modal" style="display: none;">
                                <div class="card modal-content edit_resource_card">
                                  <div class="card-header card-header-rose card-header-text" style="width:100%">
                                    <div class="card-icon">
                                        <i class="material-icons">edit</i>
                                    </div>
                                    <!-- <div class="card-text"> -->
                                      <h4 class="card-title">Edit Resources</h4>
                                  </div>
                                  <div class="card-body edit_resource_card_body mt-5">
                                    <div class="row">
                                      <div class="col-md-4 new_resource_col" style="display:none;">
                                        <div class="form-group bmd-form-group">
                                          <label for="resource_code_exist" class="bmd-label-floating input_label">Resource Name</label>
                                          <input type="text" class="form-control form_input_field" value="" name="edit_res_name">
                                        </div>
                                      </div>
                                      <div class="col-md-4 input_wrapper existing_resource_col" style="display:none;">
                                        <div class="form-group custom-form-select">
                                          <select class="custom-select hasvalue" id="vendor_resource_name_select">
                                              <option class="form-select-placeholder"></option>
                                            @foreach($resources as $rowdata)
                                              <option value="{{$rowdata->resource_name}}" r_id = "{{$rowdata->id}}" r_code ="{{$rowdata->resource_code}}" >{{$rowdata->resource_name}}</option>
                                            @endforeach
                                          </select>
                                          <div class="form-element-bar">
                                          </div>
                                          <label class="form-element-label input_label" id="vendor_resource_name_existing" for="resource_name">Resource Name</label>
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                        <div class="form-group bmd-form-group">
                                          <label for="resource_code_exist" class="bmd-label-floating input_label">Resource Code</label>
                                          <input type="text" class="form-control form_input_field" value="" name="edit_res_code">
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                        <div class="form-group bmd-form-group">
                                          <label for="resource_code_exist" class="bmd-label-floating input_label">Model Number</label>
                                          <input type="text" class="form-control form_input_field" value="" name="edit_model_no">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-10 mx-auto">
                                        <div class="card edit_resource_variable_card" style="display:none">
                                          <div class="card-header card-header-rose card-header-text">
                                            <div class="card-icon">
                                                <i class="material-icons">edit</i>
                                            </div>
                                            <h4 class="card-title">Variables</h4>
                                          </div>
                                          <div class="card-body edit_resource_variable_card_body">
                                            
                                            
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-12 text-right">
                                        <button class="btn btn-success update_resource_btn btn-round" type="button"> Update </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <span class="close">×</span>
                              </div>

                              <!-- The Modal -->
                              <div id="myModal" class="modal" style="display: none;">
                                <div class="card modal-content variable_table_card">
                                  <div class="card-header card-header-rose card-header-text">
                                    <div class="card-icon">
                                        <i class="material-icons">ballot</i>
                                    </div>
                                    <!-- <div class="card-text"> -->
                                      <h4 class="card-title">Variables Selected</h4>
                                  </div>
                                  <div class="card-body">
                                    <div class="table-responsive table-sales variable_table_wrapper">  
                                      <table class="table variable_table">
                                        <thead class="text-success">
                                          <tr>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Status</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          
                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                </div>

                                <span class="close">×</span>
                              </div>
                            </div>
                              
                          </div>

                        </div>


                        <div class="card quickbooks_update_checking_card">
                                <div class="card-header card-header-rose card-header-text sub_card">
                                  <div class="card-text card_text_title">
                                    <h4 class="card-title">Quickbooks</h4>
                                  </div>
                                </div>

                                <div class="card-body">
                                  
                                  <div class="row">
                                    <div class="col-md-12 input_wrapper">
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input singleCustomerQB" type="checkbox" name="singleCustomerQB" value="1"> Check here If you want to update data to Quickbooks as well
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                        </div>




                        <!-- Save Btn -->
                        <div class="form-group bmd-form-group save_btn_wrapper text-right">
                          <button type="submit" class="btn save_btn custom-btn-one">Save</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>

                <div class="tab-pane" id="pre_trip_report">
                  <div class="card pre_trip_report_card">
                    <div class="card-header card-header-rose card-header-text lead_card_header user_sub_card">
                      <div class="card-icon">
                          <i class="material-icons">supervised_user_circle</i>
                      </div>
                        <h4 class="card-title">Pre-Trip Report</h4>
                    </div>
                    <div class="card-body">

                      <div class="row">
                        <div class="col-md-8 ml-auto mr-auto pre_trip_report_header">
                          <div class="row">
                            <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group">
                                  <label for="pre_trip_date_report" class="bmd-label-floating input_label">Date</label>
                                  <input type="text" class="form-control form_input_field datepicker" id="pre_trip_date_report" name="pre_trip_date_report">
                                </div>
                            </div>
                            <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group">
                                  <label for="tructor_truck_no" class="bmd-label-floating input_label">Tructor/Truck No</label>
                                  <input type="text" class="form-control form_input_field" id="tructor_truck_no" name="tructor_truck_no">
                                </div>
                            </div>
                          </div>
                          <div class="row pre_trip_report_remark">
                            <label class="col-md-12 text-center">
                              <span>Note</span>
                              CHECK ANY DEFECTIVE ITEM AND GIVE DETAILS UNDER "REMARKS"
                            </label>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-12">
                          <div class="card odometer_card">
                            <div class="card-header card-header-rose card-header-text sub_card">
                              <div class="card-text card_text_title">
                                <h4 class="card-title">Odometer</h4>
                              </div>
                            </div>
                            <div class="card-body">
                              <div class="row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group">
                                    <label for="odometer_start" class="bmd-label-floating input_label">Odometer Start</label>
                                    <input type="text" class="form-control form_input_field " id="odometer_start" name="odometer_start">
                                  </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group">
                                    <label for="odometer_end" class="bmd-label-floating input_label">Odometer Ending</label>
                                    <input type="text" class="form-control form_input_field " id="odometer_end" name="odometer_end">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="col-md-12">
                          <div class="card odometer_card">
                            <div class="card-header card-header-rose card-header-text sub_card">
                              <div class="card-text card_text_title">
                                <h4 class="card-title">Basic Equipments</h4>
                              </div>
                            </div>
                            <div class="card-body">

                              <div class="row">
                                <div class="col-md-4">
                                  <ul class="equipments_list">
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Air Compressor
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Air Lines
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Battery    
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Body       
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Brake Accessories           
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Brakes, Parking        
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Brakes, Service     
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Clutch
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Coupling Devices   
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Defroster/Heater     
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Drive Line   
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Engine
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Exhaust
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                  </ul>
                                </div>
                                <div class="col-md-4">
                                  <ul class="equipments_list">
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Fifth Wheel
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Frame and Assembly
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Front Axle
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Fuel Tanks
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Generator
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Horn
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Lights
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                      <ul class="equipments_sublist">
                                        <li>
                                          <div class="form-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Head - Stop
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="form-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Tail - Dash
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="form-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Turn Indicators
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                      </ul>
                                    </li>
                                    
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Mirros
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Muffler
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Oil Pressure
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                  </ul>
                                </div>
                                <div class="col-md-4">
                                  <ul class="equipments_list">
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Radiator
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Rear End
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Reflectors
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Suspension System  
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Starter
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Steering
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Tachograph
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Tires
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Tire Chains
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Trasnsmission
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Wheels and Rims
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Windows
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Windshield Wipers
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <div class="row safety-row">
                                <div class="col-md-4">
                                  <h4 class="equipments_sublist_title">Safety Equipment</h4>
                                  <ul class="equipments_list">
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Fire Extinguisher (2)
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Reflective Triangles
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Flags - Flares - First Aid Kit
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Spare Bulbs & Fuses  
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Spare Seal Beam
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                  </ul>
                                </div>
                                <div class="col-md-4">
                                  <h4 class="equipments_sublist_title">Safety PPE</h4>
                                  <ul class="equipments_list">
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Hard Hat & Gloves
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Safety Glasses / Goggles
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Boots & FRC's
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Hearing Protection
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">H2S and 4/5 Gas Monitor
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Job / Task Specific PPE
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                  </ul>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-12">
                          <div class="card odometer_card">
                            <div class="card-header card-header-rose card-header-text sub_card">
                              <div class="card-text card_text_title">
                                <h4 class="card-title">Other Equipments</h4>
                              </div>
                            </div>
                            <div class="card-body">

                              <div class="row input_row" style="margin-bottom: 10px;">
                                <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="trailer_no" class="bmd-label-floating input_label">Trailer's No(S)</label>
                                      <input type="text" class="form-control form_input_field" id="trailer_no" name="trailer_no">
                                    </div>
                                </div>
                              </div>

                              <div class="row input_row">
                                <div class="col-md-4">
                                  <ul class="equipments_list">
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Brake Connections
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Brakes
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Coupling Devices    
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Coupling (King) Pin    
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Doors           
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                  </ul>
                                </div>

                                <div class="col-md-4">
                                  <ul class="equipments_list">
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Hitch
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Landing Gear
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Lights- All
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Roof 
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Suspension System    
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                  </ul>
                                </div>

                                <div class="col-md-4">
                                  <ul class="equipments_list">
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Tarpaulin
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Tires
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Wheels and Rims
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Paperwork - Permits
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Other  
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                  </ul>
                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-12">
                          <div class="card odometer_card">
                            <div class="card-header card-header-rose card-header-text sub_card">
                              <div class="card-text card_text_title">
                                <h4 class="card-title">Others</h4>
                              </div>
                            </div>
                            <div class="card-body">

                              <div class="row">
                                <div class="col-md-12 input_wrapper">
                                  <div class="form-group bmd-form-group">
                                    <label for="report_remarks" class="bmd-label-floating input_label">Remarks</label>
                                    <textarea class="form-control form_input_field form_fields" rows="5" name="report_remarks" id="report_remarks"></textarea>
                                  </div>
                                </div>
                              </div>

                              <fieldset class="custom_fieldset">
                                <legend>Time count</legend>
                                <div class="row">
                                  <div class="col-md-4 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="time_in" class="bmd-label-floating input_label">Time In</label>
                                      <input type="text" class="form-control form_input_field timepicker" id="time_in" name="time_in">
                                    </div>
                                  </div>
                                  <div class="col-md-4 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="time_out" class="bmd-label-floating input_label">Time Out</label>
                                      <input type="text" class="form-control form_input_field timepicker" id="time_out" name="time_out">
                                    </div>
                                  </div>
                                  <div class="col-md-4 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="total_hour" class="bmd-label-floating input_label">Total Hours</label>
                                      <input type="text" class="form-control form_input_field" id="total_hour" name="total_hour">
                                    </div>
                                  </div>
                                </div>
                              </fieldset>

                              <div class="row condition_row_pri_trip input_row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input class="form-check-input" type="checkbox" value="">Condition of The Above Vehicle is Satisfactory
                                      <span class="form-check-sign">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="driver_signature1" class="bmd-label-floating input_label">Driver's Signature</label>
                                      <input type="text" class="form-control form_input_field" id="driver_signature1" name="driver_signature1">
                                    </div>
                                </div>
                              </div>

                              <div class="row defect_correction_row input_row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input class="form-check-input" type="checkbox" value="">Above Defects Corrected  
                                      <span class="form-check-sign">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input class="form-check-input" type="checkbox" value="">Above Defects Need Not Be Corrected For Safe Operation Of Vehicle
                                      <span class="form-check-sign">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-12">
                          <div class="card odometer_card">
                            <div class="card-header card-header-rose card-header-text sub_card">
                              <div class="card-text card_text_title">
                                <h4 class="card-title">Signature</h4>
                              </div>
                            </div>
                            <div class="card-body">

                              <div class="row input_row" style="margin-bottom: 10px;">

                                <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="driver_signature2" class="bmd-label-floating input_label">Driver's Signature</label>
                                      <input type="text" class="form-control form_input_field datepicker" id="driver_signature2" name="driver_signature2">
                                    </div>
                                </div>

                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group">
                                    <label for="driver_signature_date" class="bmd-label-floating input_label">Date</label>
                                    <input type="text" class="form-control form_input_field datepicker" id="driver_signature_date" name="driver_signature_date">
                                  </div>
                                </div>

                              </div>

                              <div class="row input_row" style="margin-bottom: 10px;">
                                <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group">
                                      <label for="mechanic_signature" class="bmd-label-floating input_label">Mechanic's Signature</label>
                                      <input type="text" class="form-control form_input_field" id="mechanic_signature" name="mechanic_signature">
                                    </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group">
                                    <label for="machanic_signature_date" class="bmd-label-floating input_label">Date</label>
                                    <input type="text" class="form-control form_input_field datepicker" id="machanic_signature_date" name="machanic_signature_date">
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="tab-pane" id="jobs_site_analysis">
                  <form id="jobs_site_analysis_form">
                    <div class="card pre_trip_report_card" id="top_pre_trip_report_card">
                      <div class="card-header card-header-rose card-header-text lead_card_header user_sub_card">
                        <div class="card-icon">
                            <i class="material-icons">supervised_user_circle</i>
                        </div>
                          <h4 class="card-title">Jobs Site Hazard analysis</h4>
                      </div>
                      <div class="card-body">
                        <div class="row">
                          <div class="col-md-1 progress_container jobs_site_analysis_arrow" style="text-align:center;margin-bottom:20px;">
                            <span class="stepJobSiteAnalysis step-first active">
                              <i class="material-icons">done</i>
                            </span>
                            <span class="stepJobSiteAnalysis step-inner">
                              <i class="material-icons">done</i>
                            </span>
                            <span class="stepJobSiteAnalysis step-inner">
                              <i class="material-icons">done</i>
                            </span>
                            <span class="stepJobSiteAnalysis step-inner">
                              <i class="material-icons">done</i>
                            </span>
                          </div>

                          <div class="col-md-11 jobs_site_analysis_content">
                            <div class="tabJobSiteAnalysis">
                              <div class="card">
                                <div class="card-body">
                                  <div class="row input_row">

                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="prepared_by" class="bmd-label-floating input_label">Prepared By</label>
                                        <input type="text" class="form-control form_input_field" id="prepared_by" name="prepared_by">
                                      </div>
                                    </div>

                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="crew" class="bmd-label-floating input_label">Crew</label>
                                        <input type="text" class="form-control form_input_field" id="crew" name="crew">
                                      </div>
                                    </div>

                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="customer_well" class="bmd-label-floating input_label">Customer / Well</label>
                                        <input type="text" class="form-control form_input_field" id="customer_well" name="customer_well">
                                      </div>
                                    </div>

                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="jobs_site_analysis_time" class="bmd-label-floating input_label">Time</label>
                                        <input type="text" class="form-control form_input_field timepicker" id="jobs_site_analysis_time" name="jobs_site_analysis_time">
                                      </div>
                                    </div>

                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="jobs_site_analysis_date" class="bmd-label-floating input_label">Date</label>
                                        <input type="text" class="form-control form_input_field datepicker" id="jobs_site_analysis_date" name="jobs_site_analysis_date">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row input_row check_job_site_row">
                                    <div class="col-md-12">
                                      <div class="form-check jobs_site_analysis_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value=""> Check Job Site Surface Equipment
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                      <div class="form-check jobs_site_analysis_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value=""> Check Job Site Pressure Ratings
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                      <div class="form-check jobs_site_analysis_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value=""> Hazardous Gasses or Fluids (H2S, Acid, etc.)
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row jobs_site_analysis_row">
                                    <div class="col-md-12">
                                     <div class="service_represent">
                                      <span class="inline-block-span">E&V Oilfield Services Representative </span>
                                      <div class="form-check jobs_site_analysis_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value=""> Verify all safety policies will be followed and adhered to throughout the job. (Stop Work Authority)
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                     </div>

                                     <div class="service_represent">
                                      <span class="inline-block-span">E&V Oilfield Services Representative </span> 
                                      <div class="form-check jobs_site_analysis_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Verified that valves and wellhead are designated to be used for this job site
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                     </div>

                                     <div class="service_represent">
                                      <span class="inline-block-span">E&V Oilfield Services Representative </span>
                                      <div class="form-check jobs_site_analysis_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">has reviewd customer and 3rd party supplied equipment safe to use: i.e. rig stairs, ladders, light towers. 
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                     </div>

                                     <div class="service_represent">
                                      <span>E&V Oilfield Services Representative </span>
                                      <div class="form-check jobs_site_analysis_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">has communicated the requirement for 100% tie off over 4 foot heights. 
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                     </div>
                                    </div>
                                  </div>

                                  <div class="row physical_hazard_discus_row">
                                    <div class="headeing_with_chekbox">
                                      <h3>Physical Hazards Discussed</h3>
                                      <div class="form-check jobs_site_analysis_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value=""> Job Assignment &#8226; Equipment Operations &#8226; HouseKeeping
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row physical_hazard_discus_content">
                                    <div class="col-md-4">
                                      <ul class="physical_hazard_discus_list">
                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Cranes
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>

                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Extreme Temperatures
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>

                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Elevated Work Areas
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>

                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Fails
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Lightning
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                      </ul>
                                    </div>
                                    <div class="col-md-4">
                                      <ul class="physical_hazard_discus_list">
                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Overhead Loads
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Overhead Utilities
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Pinch Points
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Rat Holes, Slick Spots, Cellars
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Spiders, Snakes, Wasps
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                      </ul>
                                    </div>
                                    <div class="col-md-4">
                                      <ul class="physical_hazard_discus_list">
                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Slips, Trips, Fails
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Trapped Pressure
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Wind speed
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Wind Direction
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                        <li>
                                          <div class="form-check physical_hazard_discus_check form-check-inline">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">WireLine / Lubricator
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </li>
                                      </ul>
                                    </div>
                                    
                                  </div>

                                </div>
                              </div>
                            </div>
                            <div class="tabJobSiteAnalysis">
                              <div class="row emergency_procedure_dis_row">
                                <h4 class="sub-title">Emergency Procedures Discussed</h4>
                                <div class="col-md-12">
                                  <div class="form-check emergency_procedure_check form-check-inline">
                                    <label class="form-check-label">
                                      <input class="form-check-input" type="checkbox" value="">Emergency Numbers
                                      <span class="form-check-sign">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>

                                  <div class="form-check emergency_procedure_check form-check-inline">
                                    <label class="form-check-label">
                                      <input class="form-check-input" type="checkbox" value="">Identified Designated Safe Areas
                                      <span class="form-check-sign">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>

                                  <div class="form-check emergency_procedure_check form-check-inline">
                                    <label class="form-check-label">
                                      <input class="form-check-input" type="checkbox" value="">Location of Cell Phone, First Aid Kit, Eye Wash, Monitors
                                      <span class="form-check-sign">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>

                                  


                                </div>
                              </div>

                              <div class="row codition_hazard_policy_row">
                                <h4 class="sub-title">
                                  Condition, Hazards, Policy
                                </h4>
                              </div>

                              <div class="row codition_hazard_policy_content input_row">
                                <div class="col-md-4 input_wrapper">
                                  <ul class="physical_hazard_discus_list">
                                    
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Energized Fluids
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Equipment Grounding
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>

                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Heater Fires
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    
                                  </ul>
                                </div>
                                <div class="col-md-4 input_wrapper">
                                  <ul class="physical_hazard_discus_list">
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Hot Work; Welding/ Fueling
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Locked Gates
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>

                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Lock-out / Tag-out
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    
                                  </ul>
                                </div>
                                <div class="col-md-4 input_wrapper">
                                  <ul class="physical_hazard_discus_list">
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Primary Meeting Area
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Secondary Meeting Area
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    
                                  </ul>
                                </div>
                               
                              </div>

                              <div class="row job-hazard-analysis-row">
                                <h4 class="sub-title">*** Communication: Be sure that individual activities are clean to each person on location ***</h4>
                                <h5 class="job_hazard_sub_title">Job Hazard analysis: PPE Required</h5>
                              </div>
                              <div class="row job_hazard_analysis_content input_row">
                                <div class="col-md-4 input_wrapper">
                                  <ul class="physical_hazard_discus_list">
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Chemical Suit
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Chemical Aprons
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Face Shield
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Fail Protection
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Fire Retardant Clothing
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Goggles
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                  </ul>
                                </div>
                                <div class="col-md-4 input_wrapper">
                                  <ul class="physical_hazard_discus_list">
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Hand Protection
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Hard Hat
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>

                                     <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Hering Protection
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Impact Gloves
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Personal H2S Monitor
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                  </ul>
                                </div>
                                <div class="col-md-4 input_wrapper">
                                  <ul class="physical_hazard_discus_list">
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Respirtor
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Rubber Boots
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Rubber Gloves
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Safety Harness
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="form-check physical_hazard_discus_check form-check-inline">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" value="">Steel Toe Boots
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>
                                        </label>
                                      </div>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>

                            <div class="tabJobSiteAnalysis">
                              
                              <div class="row jsa-step-table-row">
                                <div class="col-md-12 ">
                                  <table class="table jsa-step-table">
                                    <thead>
                                      <tr>
                                        <th>JSA Steps</th>
                                        <th>Potential Accidents or Hazards</th>
                                        <th>Recommended Safe Actions to Mitigate Hazards</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Additonal Step
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Check PPE for all the crew
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Mobilization from location
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Mobilization from yard
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Spotting
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Transport/ Load/ Unload
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Blind Spots, Road Conditions
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Check Location Equipment
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Highway, Country / lease Roads
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Third-Party Equipment
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Use Spotters
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Vehicle Accidents, Directions
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          
                                        </td>
                                        <td>
                                          <p>
                                            Be careful at yard. Proper PPE, 3 Point contact, pretrip vehicle. Check trucks, chemicals, supplies, perform tailgate meeting. Discuss Convoy, route, seatbelts, following distance, road hazards. No Cell phones or tect while driving, follow speed limits. Lease Road max. 25 mph, beware of guards and livestock. Report all vehicle Accidents, spills, incidents, Near Miss, Injuries. Blind Spots, Road Conditions, Dust, other hazards. Clean Vehicle windows, lights, loose trash or unsecared items. 
                                          </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Crew JSA Meeting
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Rig Up
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Rig Down
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Site Check
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </td>

                                        <td>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Caught In-Between
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Elevated Work Area
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Overhead Loads
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Poor Walking / Working Surface
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Pinch Points, Trips and Falls
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Rat holes, Slick spots, cellars
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Standard Hazards
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </td>

                                        <td>
                                          <p>
                                            Inspect work areas, check for hazards, work in teams, proper tools. Assign jobs, number of lines for iron and hoses, valves, pop-offs. Good Hand / Foot placements, watch slips / trips / falls, crane signals, carefull lifting iron / hoses, wire placement, proper tools, tag lines. Communicate tasks, tie off 4', H2S Monitors, Clear of hammering. Team work/ lift, proper PPE, oil-threads, equipment placement. Fire extingushers out, caution tape around red zone, PPSI sign out. If no railing -4" tie off required, harness, certified machine operators. 
                                          </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Crew JSA Meeting
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Roll / Acid Check Chemicals
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Test Lines / Pressure / and Kills
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          
                                          
                                        </td>
                                        <td>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Distraction, Chemicals, Valves
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Secondary Personnel
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Stay in Designated Areas
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Trapped Pressure
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </td>
                                        <td>
                                          <p>
                                            Check rigging of lines, kicks, pick up unused iron/hoses/tools. Pressure up, stay clear, check for leaks on lines, valves, well-head and equipment, communicate to supervisor, stay clear of lines. Check valves, tanks, contain spills, don't overfill tank/tub.
                                          </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Crew JSA Meeting
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Perform Job
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Distraction, Chemicals, Valves
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Stay in Designated Areas
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Wireline Operations
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Wireline - Lubricator Down
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </td>
                                        <td>
                                          <p>
                                            Wear radio/ headphones, stay clear wireline and 3rd party operations. Release pressure before opening valve, contain any spills. Stay in designated areas, perform jobs safely, communicate hazards, pressure, rate, density, entering/ exiting red zones. 
                                          </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Identify Hazards
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Site Check
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          
                                        </td>
                                        <td>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Check Location Equipment
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Emergency Metting Place
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Red Zone Area - caution Tape
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </td>
                                        <td>
                                          <p>
                                            Housekeeping, fire extinguishers, chemicals, lights, wash trailer. Plan route directions out, meetings areas, designated drivers. Check location: holes - cellar - slick areas, equipment, use spotters. 
                                          </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Crew JSA Meeting
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Sign JSA Sheet
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Distractions
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Heat - cold; Hydration
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                          
                                          <div class="form-check jsa-step-table-check">
                                            <label class="form-check-label">
                                              <input class="form-check-input" type="checkbox" value="">Standard Hazards
                                              <span class="form-check-sign">
                                                <span class="check"></span>
                                              </span>
                                            </label>
                                          </div>
                                        </td>
                                        <td>
                                          <p>
                                            Communicate all hazards with crew and third parties. Identify those authorized to enter and work in red zone areas.
                                          </p>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>

                            </div>

                            <div class="tabJobSiteAnalysis">
                              <div class="row">
                                <div class="col-md-12 tailgate_safety_meet_tbl_res">
                                  <table class="table tailgate_safety_meet_table">
                                    <thead>
                                      <tr>
                                        <th colspan="3">E&V Oilfield Services</th>
                                        <th colspan="4">
                                          <h4>TailGate Safety Meeting</h4>
                                          <p>Invite All Personnel on location and discuss job Procedure</p>
                                        </th>
                                      </tr>
                                    </thead>
                                    <tr>
                                      <td colspan="3">Customer:</td>
                                      <td colspan="2">Supervisor:</td>
                                      <td colspan="2">Date:</td>
                                    </tr>
                                    <tr>
                                      <td colspan="3">Well Name & Number:</td>
                                      <td colspan="2">Crew:</td>
                                      <td colspan="2">Data Van #:</td>
                                    </tr>
                                    <tr>
                                      <td colspan="3">Customer Representative:</td>
                                      <td colspan="2">Max Pressure:</td>
                                      <td colspan="2">Max Rate:</td>
                                    </tr>
                                    <tr>
                                      <td colspan="3">Nearest Hospital:</td>
                                      <td colspan="2">Recorded By:</td>
                                      <td colspan="2">Number of Stages:</td>
                                    </tr>
                                    <tr>
                                      <th></th>
                                      <th colspan="2">Employee Print Name</th>
                                      <th>Employee Signature</th>
                                      <th>Hard Hat Color</th>
                                      <th class="mentor_green_hard_hat">Mentor for Green Hard Hat</th>
                                      <th>Company Name</th>
                                    </tr>
                                    <tr>
                                      <td>1</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>2</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>3</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>4</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>5</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>6</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>7</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>8</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>9</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>10</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>

                                    <tr>
                                      <td>11</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>12</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>13</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>14</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>15</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>16</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>17</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>18</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>19</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>20</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>

                                    <tr>
                                      <td>21</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>22</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>23</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>24</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>25</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>26</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>27</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>28</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>29</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>30</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>

                                    <tr>
                                      <td>31</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>32</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>33</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>34</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>35</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>36</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>37</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>38</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>39</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>
                                    <tr>
                                      <td>40</td>
                                      <td colspan="2"></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>E&V Oilfield Service</td>
                                    </tr>

                                  </table>

                                </div>
                                <div class="col-md-12 text-center">
                                  <p class="tailgate-jst-form-footer">HSE Form 136 JSA - TailGate Meeting Sign in Sheet</p>
                                </div>
                              </div>
                            </div>
                           

                            <div style="overflow:auto;" class="row msf_btn_holder">
                              <div class="col-md-6 save_btn_wrapper text-left" style="float:right;">
                                <a class="btn btn-round btn-warning prevJobanlysisbtn prev" href="#top_pre_trip_report_card" type="button" id="prevBtnJobSiteAnalysis" onclick="nextPrevJobSiteAnalysis(-1)">Previous</a>
                              </div>
                              <div class="col-md-6 save_btn_wrapper text-right">
                                <a class="btn custom-btn-one btn-round btn-success nextPrevBtnOne next nextJobanlysisbtn" href="#top_pre_trip_report_card" type="button" id="nextBtnJobSiteAnalysis" onclick="nextPrevJobSiteAnalysis(1)">Next</a>
                                <button class="btn custom-btn-one btn-round btn-success nextPrevBtnOne submitBtnJobSiteAnalysis nextJobanlysisbtn" style="display: none" type="submit" onclick="">Submit</button>
                              </div>
                            </div>

                          </div>


                        </div>
                      </div>
                    </div>
                  </form>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>





<script>
  $(document).ready(function() {

    $('.resource_id').change(function() {
      //alert('okk');
      if (this.checked) {
          $('#existing_resource').show();
          $('#new_resource').hide();
      }
      else{
          $('#new_resource').show();
          $('#existing_resource').hide();
      }        
    });

    $('.variable_checkbox').change(function(){
      if(this.checked){
        $('.variable_card').fadeIn('5000','swing');
      }else{
        $('.variable_card').fadeOut('3000','swing', function(){
          $('.variable_card_body .var_input_row').remove();
          $('.top_variable_row input[type=text]').val('');
          $('.top_variable_row .input_wrapper .form-group').removeClass('is-filled');
          $('.top_variable_row .custom-select').removeClass('hasvalue').find('option:first').prop('selected', true);
          $('.top_variable_row .enable_disable')[0].checked = false;
        });

      }
    })



    $( ".custom-select" ).change(function() {
      if(this.value.length > 0){
          $( this ).addClass( "hasvalue" );
      }
    })

  });



  $( "#vendor_resource_name_select" ).change(function() {
      //alert($('option:selected', this).attr('r_id'));

      $('#vendor_resource_code_existing').val($('option:selected', this).attr('r_code'));
      $('#vendor_resource_code_existing').parent().addClass('is-filled');
      $('#vendor_resource_code_existing').attr('readonly', true);
    })


  $('.input-images').imageUploader({
      imagesInputName:'images',
      label:'Drag & Drop files here or click to browse'
  }); 




        var url = '<?php echo $authUrl; ?>';

        var OAuthCode = function(url) {

            this.loginPopup = function (parameter) {
                this.loginPopupUri(parameter);
            }

            this.loginPopupUri = function (parameter) {

                // Launch Popup
                var parameters = "location=1,width=800,height=650";
                parameters += ",left=" + (screen.width - 800) / 2 + ",top=" + (screen.height - 650) / 2;

                var win = window.open(url, 'connectPopup', parameters);
                var pollOAuth = window.setInterval(function () {
                    try {

                        if (win.document.URL.indexOf("code") != -1) {
                            window.clearInterval(pollOAuth);
                            win.close();
                            location.reload();
                        }
                    } catch (e) {
                        console.log(e)
                    }
                }, 100);
            }
        }


        var apiCall = function() {
            this.getVendor = function() {
                $.ajax({
                      url:"{{ route('getQbVendor') }}",
                      method:"GET",
                      cache: false,
                      beforeSend: function(){
                          $('#loading_text').show();
                          $('#loader').show();
                      },
                      complete: function(){
                          $('#loading_text').hide();
                          $('#loader').hide();
                      },
                      success:function(result)
                      {
                       $('#apiCall').html(result);
                      }
                   })
            }           

        }


          function sync(){
            if($('#syncCheck').prop("checked") == true){
              synchronizeQbVendor();
            }
            else{
              synchronizeEvosVendor();
            }
          }  

          function synchronizeEvosVendor() {
                $.ajax({
                      url:"{{ route('synchronizeEvosVendor') }}",
                      method:"GET",
                      cache: false,
                      beforeSend: function(){
                          $('#loading_text_sync').show();
                          $('#loader_sync').show();
                      },
                      complete: function(){
                          $('#loading_text_sync').hide();
                          $('#loader_sync').hide();
                      },
                      success:function(result)
                      {
                       $('#apiCall_sync').html(result);
                      }
                   })
            }

            function synchronizeQbVendor() {
                $.ajax({
                      url:"{{ route('synchronizeQbVendor') }}",
                      method:"GET",
                      cache: false,
                      beforeSend: function(){
                          $('#loading_text_qb_sync').show();
                          $('#loader_qb_sync').show();
                      },
                      complete: function(){
                          $('#loading_text_qb_sync').hide();
                          $('#loader_qb_sync').hide();
                      },
                      success:function(result)
                      {
                       $('#apiCall_qb_sync').html(result);
                      }
                   })
            } 

      $('.singleCustomerQB').change(function() {
          if($(this).is(":checked")) {
              $.ajax({
                        url:"{{ route('singleCustomerQB') }}",
                        method:"GET",
                        cache: false,
                        success:function(result)
                        {
                         if(result == 'connect'){
                            alert('Please click on connect to quickbooks button from Quickbooks Tab to get access token');
                            $('.singleCustomerQB').prop("checked", false);
                         }
                         else if(result == 'expire'){
                          alert('Your access token is expired, Please click on Connect to Quickbooks button from QUickbooks tab');
                          $('.singleCustomerQB').prop("checked", false);
                         }
                        
                        }
                     })
          }       
      });



        var oauth = new OAuthCode(url);
        var apiCall = new apiCall();


function validateMyForm()
{
  if($('#vendor_resource_name_select').val() !== '' || $('#vendor_resource_code_existing').val() !== '' || $('#vendor_model_no_exist').val() !== '' || $('#resource_name_new').val() !== '' || $('#resource_code_new').val() !== '' || $('#model_no_new').val() !== '')
  { 
    alert("Please click add resources button first to add your resources");
    return false;
  }

  return true;
}

$(document).ready(function () {

if($('#tabMenu a[href="#{{ old('tab') }}"]').attr('href') == undefined){
    $('#tabMenu a[href="#quickbooks_vendor"]').tab('show')  
}
else{
    $('#tabMenu a[href="#{{ old('tab') }}"]').tab('show')
}

});


</script>



@endsection           




