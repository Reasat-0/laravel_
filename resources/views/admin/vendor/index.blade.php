@extends('layouts.master')

@section('content')      


<div class="content">
  <div class="content manage-vendor-content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
              <div class="card vendor_root_card">
                <div class="card-header card-header-tabs card-header-rose custom_card_header mang_customer_header_card">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <p class="nav-tabs-title">Manage Vendors</p>
                      <ul class="nav nav-tabs" data-tabs="tabs">
                        <!-- <li class="nav-item">
                          <a class="nav-link active show" href="#quickbooks_vendor" data-toggle="tab">
                            <i class="material-icons">bug_report</i> Quickbooks
                            <div class="ripple-container"></div>
                          <div class="ripple-container"></div></a>
                        </li> -->
                        <li class="nav-item">
                          <a class="nav-link active" href="#custom_vendor" data-toggle="tab">
                            <i class="material-icons">code</i> Manual
                            <div class="ripple-container"></div>
                          <div class="ripple-container"></div></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane" id="quickbooks_vendor">
                      <div class="card">

                      @if(request()->temp == 'true')
                        <div class="row">
                          <div class="col-md-6">
                            <div class="card-header card-header-rose card-header-icon">
                              <div class="card-icon temp_card_icon">
                                <i class="material-icons">assignment</i>
                              </div>
                              @if(request()->temp == 'true') 
                              <?php
                                $temp = " (Temporary data for review)";
                              ?>
                              @else
                              <?php
                                $temp = "";
                              ?>
                              @endif
                              <h4 class="card-title temporary_heading_title">Quickbooks {{$temp}}</h4>
                            </div>
                          </div>

                       
                          <div class="col-md-6 text-right quickbook-btn-section">
                            <button type="button" class="btn btn-success save_btn" onclick="insertQbCustomer()">Save permanently</button>

                            <button  type="button" class="btn btn-danger btn_red" onclick="discardQbCustomer()">Discard</button>
                          </div>
                        </div>
                      @else
                        <div class="card-header card-header-rose card-header-icon icon_box_wrapper user_sub_card">
                            <div class="card-icon">
                              <i class="material-icons">assignment</i>
                            </div>
                            <h4 class="card-title">Quickbooks</h4>
                          </div>
                      @endif

                      <p id="qbCustomer" style="padding:15px;">{!! Session::get('message') !!}</p>

                        <div style="margin-top: 15px;">
                            @include('partials.messages')
                        </div>

                        <div class="card-body">
                          <div class="toolbar">
                          </div>
                          <div class="material-datatables vendor_datatables" style="overflow-x: hidden">
                            <table id="all_vendors" class="table table-striped table-no-bordered table-hover all_table" cellspacing="0" width="100%" style="width:100%;">
                              <thead>
                                <tr>
                                  <th>Display Name</th>
                                  <th>Email</th>
                                  <th>Phone</th>
                                  <th class="disabled-sorting text-right">Actions</th>
                                </tr>
                              </thead>

                              <tbody>
                                @if(request()->temp == 'true') 
                                <?php
                                  $vendors = $vendors_quickbooks_temp;
                                ?>
                                @else
                                <?php
                                  $vendors = $vendors_quickbooks;
                                ?>
                                @endif
                                @foreach($vendors as $rowdata)
                                  <tr>  
                                      <td> {{$rowdata->displayName}} </td>
                                      <td> {{$rowdata->primaryEmailAddr_addess}} </td>
                                      <td> {{$rowdata->primaryPhone_freeFormNumber}} </td>
                                      <td class="text-right">
                                        
                                        <form action="{{ route('vendors.destroy', $rowdata->id) }}" method="POST">
                                            @if(request()->temp == 'true') 
                                              <a href="{{ route('vendors.show',$rowdata->id) }}" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">remove_red_eye</i></a>
                                            @else
                                              <a href="{{ route('vendors.edit',$rowdata->id) }}" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a>
                                            @endif
                                         
                                            @csrf
                                            @method('DELETE')
                            
                                            <button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm('Are you sure you want to delete?');"><i class="material-icons">close</i></button>
                                        </form>
                                      
                                      </td>
                                  </tr>
                                @endforeach
                              
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- end content-->
                      </div>
                    </div>
                    <div class="tab-pane active" id="custom_vendor">
                      <div class="card">
                        <div class="card-header card-header-rose card-header-icon user_sub_card">
                          <div class="card-icon">
                            <i class="material-icons">supervised_user_circle</i>
                          </div>
                          <h4 class="card-title">Manual</h4>
                        </div>

                        <div style="margin-top: 15px;">
                            @include('partials.messages')
                        </div>

                        <div class="card-body">
                          <div class="toolbar">
                          </div>
                          <div class="material-datatables vendor_datatables" style="overflow-x: hidden">
                            <table id="all_manual_vendors" class="table table-striped table-no-bordered table-hover all_table" cellspacing="0" width="100%" style="width:100%;">
                              <thead>
                                <tr>
                                  <th>Display Name</th>
                                  <th>Email</th>
                                  <th>Phone</th>
                                  <th class="disabled-sorting text-right">Actions</th>
                                </tr>
                              </thead>

                              <tbody>
                                @foreach($vendors_manual as $rowdata)
                                  <tr>  
                                      <td> {{$rowdata->displayName}} </td>
                                      <td> {{$rowdata->primaryEmailAddr_addess}} </td>
                                      <td> {{$rowdata->primaryPhone_freeFormNumber}} </td>
                                      <td class="text-right">
                                        
                                        <form action="{{ route('vendors.destroy', $rowdata->id) }}" method="POST">
                                            <a href="{{ route('vendors.edit',$rowdata->id) }}" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a>
                                         
                                            @csrf
                                            @method('DELETE')
                            
                                            <button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm('Are you sure you want to delete?');"><i class="material-icons">close</i></button>
                                        </form>
                                      
                                      </td>
                                  </tr>
                                @endforeach
                              
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- end content-->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
  </div>
</div>

<script>
    function insertQbCustomer(){
    $.ajax({
                      url:"{{ route('insertQbVendor') }}",
                      method:"GET",
                      cache: false,
                      beforeSend: function(){
                          $('#loading_text').show();
                          $('#loader').show();
                      },
                      complete: function(){
                          $('#loading_text').hide();
                          $('#loader').hide();
                      },
                      success:function(result)
                      {
                       //$(".all_table").load(location.href + " .all_table");
                       location.reload();
                       //$('#qbCustomer').html(result);
                      }
                   })
  }

  function discardQbCustomer(){
    $.ajax({
                      url:"{{ route('discardQbVendor') }}",
                      method:"GET",
                      cache: false,
                      beforeSend: function(){
                          $('#loading_text').show();
                          $('#loader').show();
                      },
                      complete: function(){
                          $('#loading_text').hide();
                          $('#loader').hide();
                      },
                      success:function(result)
                      {
                       //$(".all_table").load(location.href + " .all_table");
                       location.reload();
                       //$('#qbCustomer').html(result);
                      }
                   })
  }

  $(window).on('load', function(){
    setTimeout(function() {
       $('.material-datatables').css('overflow-x','unset');
       $('.vendor_datatables table thead th:last-child').css('width','100px');
    }, 100);
  });
</script>

@endsection