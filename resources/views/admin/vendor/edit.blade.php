@extends('layouts.master')

@section('content')

<div class="content edit_vendor_content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 mx-auto">             
        <div class="card lead_card">
          <div class="card-header card-header-tabs card-header-rose">
            <div class="nav-tabs-navigation">
              <div class="nav-tabs-wrapper">
                
                <ul class="nav nav-tabs" data-tabs="tabs">
                  <li class="nav-item">
                    <a class="nav-link active" href="#profile" data-toggle="tab">
                      <i class="material-icons">ballot</i> Edit Vendor
                      <div class="ripple-container"></div>
                    </a>
                  </li>
                  
                </ul>
              </div>
            </div>
          </div>
          <div class="card-body vendor_form_details">
            <div style="margin-top: 15px;">
              @include('partials.messages')
            </div>
            <form method="post" action="{{ route('vendors.update', $vendor->id) }}" class="form-horizontal" enctype="multipart/form-data" onsubmit="return validateMyForm();">
              @csrf
              @method('PUT')
              
              <input type="hidden" name="vendor_key" value="{{$vendor->id}}">

              <div class="card card_modified">
                          <div class="card-header card-header-rose card-header-text sub_card">
                            <div class="card-text card_text_title">
                              <h4 class="card-title">Basic</h4>
                            </div>
                          </div>
                          <div class="card-body ">
                            <div class="row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="company_name" class="bmd-label-floating input_label">Company Name</label>
                                  <input type="text" class="form-control form_input_field" id="company_name" name="companyName" value="{{$vendor->companyName}}">
                                </div>
                              </div>
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="display_name" class="bmd-label-floating input_label ">Display Name</label>
                                  <input type="text" class="form-control form_input_field" id="display_name" name="displayName" value="{{$vendor->displayName}}" required="true" aria-required="true">
                                </div>
                              </div>
                            </div>
                            
                            <div class="row input_row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="title" class="bmd-label-floating input_label">Title</label>
                                  <input type="text" class="form-control form_input_field" id="title" name="title" value="{{$vendor->title}}">
                                </div>
                              </div>
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="given_name" class="bmd-label-floating input_label">Given Name *</label>
                                  <input type="text" class="form-control form_input_field" id="given_name" name="givenName" value="{{$vendor->givenName}}">
                                </div>
                              </div>
                            </div>
                            <div class="row input_row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="middle_name" class="bmd-label-floating input_label">Middle Name</label>
                                  <input type="text" class="form-control form_input_field" id="middle_name" name="middleName" value="{{$vendor->middleName}}">
                                </div>
                              </div>
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="family_name" class="bmd-label-floating input_label">Family Name</label>
                                  <input type="text" class="form-control form_input_field" id="family_name" name="familyName" value="{{$vendor->familyName}}">
                                </div>
                              </div>
                            </div>
                            <div class="row input_row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="suffix" class="bmd-label-floating input_label">Suffix</label>
                                  <input type="text" class="form-control form_input_field" id="suffix" name="suffix" value="{{$vendor->suffix}}">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>


                <div class="card">
                          <div class="card-header card-header-rose card-header-text sub_card">
                            <div class="card-text card_text_title">
                              <h4 class="card-title">Phone & Email</h4>
                            </div>
                          </div>
                          <div class="card-body ">
                            <div class="row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="primaryPhone_freeFormNumber" class="bmd-label-floating input_label">Phone</label>
                                  <input type="text" class="form-control form_input_field" id="primaryPhone_freeFormNumber" name="primaryPhone_freeFormNumber" value="{{$vendor->primaryPhone_freeFormNumber}}">
                                </div>
                              </div>
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="primaryEmailAddr_addess" class="bmd-label-floating input_label">Email Address</label>
                                  <input type="email" class="form-control form_input_field" id="primaryEmailAddr_addess" name="primaryEmailAddr_addess" value="{{$vendor->primaryEmailAddr_addess}}">
                                </div>
                              </div>
                            </div>

                            <div class="row input_row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="mobile" class="bmd-label-floating input_label">Mobile</label>
                                  <input type="text" class="form-control form_input_field" id="mobile" name="mobile" value="{{$vendor->mobile}}">
                                </div>
                              </div>
                              <!-- <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="fax" class="bmd-label-floating input_label">Fax</label>
                                  <input type="text" class="form-control form_input_field" id="fax" name="fax" value="{{$vendor->fax}}">
                                </div>                              
                              </div> -->
                            </div>
                            <div class="row input_row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="alternatePhone" class="bmd-label-floating input_label">Other no</label>
                                  <input type="text" class="form-control form_input_field" id="alternatePhone" name="alternatePhone" value="{{$vendor->alternatePhone}}">
                                </div>
                              </div>
                            </div>

                          </div>

                        </div>

                  <div class="card">
                            <div class="card-header card-header-rose card-header-text sub_card">
                              <div class="card-text card_text_title">
                                <h4 class="card-title">Billing Address</h4>
                              </div>
                            </div>

                            <div class="card-body ">
                              <div class="row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="billAddr_line1" class="bmd-label-floating input_label">Line 1</label>
                                    <input type="text" class="form-control form_input_field" id="billAddr_line1" name="billAddr_line1" value="{{$vendor->billAddr_line1}}">
                                  </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="billAddr_line2" class="bmd-label-floating input_label">Line 2</label>
                                    <input type="text" class="form-control form_input_field" id="billAddr_line2" name="billAddr_line2" value="{{$vendor->billAddr_line2}}">
                                  </div>
                                </div>
                              </div>
                              
                              <div class="row input_row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="billAddr_city" class="bmd-label-floating input_label">City</label>
                                    <input type="text" class="form-control form_input_field" id="billAddr_city" name="billAddr_city" value="{{$vendor->billAddr_city}}">
                                  </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="billAddr_country" class="bmd-label-floating input_label">Country</label>
                                    <input type="text" class="form-control form_input_field" id="billAddr_country" name="billAddr_country" value="{{$vendor->billAddr_country}}">
                                  </div>
                                </div>
                              </div>
                              <div class="row input_row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="billAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Country SubDivision Code</label>
                                    <input type="text" class="form-control form_input_field" id="billAddr_countrySubDivisionCode" name="billAddr_countrySubDivisionCode" value="{{$vendor->billAddr_countrySubDivisionCodeo}}">
                                  </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="billAddr_postalCode" class="bmd-label-floating input_label">Postal Code</label>
                                    <input type="text" class="form-control form_input_field" id="billAddr_postalCode" name="billAddr_postalCode" value="{{$vendor->billAddr_postalCode}}">
                                  </div>
                                </div>
                              </div>
                            </div>

                        </div>


                    <div class="card">
                            <div class="card-header card-header-rose card-header-text sub_card">
                              <div class="card-text card_text_title">
                                <h4 class="card-title">Others</h4>
                              </div>
                            </div>

                            <div class="card-body ">
                              
                              <div class="row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="taxIdentifier" class="bmd-label-floating input_label">Tax Identifier</label>
                                    <input type="url" class="form-control form_input_field" id="taxIdentifier" name="taxIdentifier" value="{{$vendor->taxIdentifier}}">
                                  </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="accountNum" class="bmd-label-floating input_label">Account Number</label>
                                    <input type="text" class="form-control form_input_field" id="accountNum" name="accountNum" value="{{$vendor->accountNum}}">
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="webAddr" class="bmd-label-floating input_label">Web Address</label>
                                    <input type="url" class="form-control form_input_field" id="webAddr" name="webAddr" value="{{$vendor->webAddr}}">
                                  </div>
                                </div>
                                <div class="col-md-6 input_wrapper">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="notes" class="bmd-label-floating input_label">Notes</label>
                                    <input type="text" class="form-control form_input_field" id="notes" name="notes" value="{{$vendor->notes}}">
                                  </div>
                                </div>
                              </div>

                            </div>

                        </div>

              <div class="card">
                <div class="card-header card-header-rose card-header-text sub_card">
                  <div class="card-text card_text_title">
                      <h4 class="card-title">Identity Section</h4>
                  </div>
                </div>
                <div class="card-body">

                  <div class="row">

                    <div class="col-md-6 input_wrapper">
                        <div class="form-group bmd-form-group is-filled">
                          <label for="reg_number" class="bmd-label-floating input_label">Registration Number</label>
                          <input type="text" class="form-control form_input_field" id="reg_number" name="reg_number" value="{{$vendor->reg_number}}">
                        </div>
                    </div>
                    <div class="col-md-6 input_wrapper">
                        <div class="form-group bmd-form-group is-filled">
                          <label for="lis_number" class="bmd-label-floating input_label">License Number</label>
                          <input type="text" class="form-control form_input_field" id="lis_number" name="license_number" value="{{$vendor->license_number}}">
                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <h4 class="title">Image</h4>
                      <div class="input-images edit_section_img"></div>
                      <div id="genModal" class="inv_img_modal">
                        <span class="gen_close inv_img_close">×</span>
                        <img class="inv_modal_content" id="gen_modal_img">
                        <div id="caption"></div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

              <div class="card resource_section_card">
                <div class="card-header card-header-rose card-header-text sub_card">
                  <div class="card-text card_text_title">
                      <h4 class="card-title">Resource Section</h4>
                  </div>
                </div>


                <div class="card-body">
                  <div class="row resource_row">
                    <div class="col-md-3 justify-content-center d-flex align-items-center resource_toggle_div">
                        <div class="togglebutton admin-role-toggle resource_toggle_section ">
                            <label>
                                <input class="resource_id" type="checkbox" name="" checked="">
                                Existing <span class="toggle"></span> New
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9 resource_toggle_content_div">
                        <div class="row" id="existing_resource">
                          <div class="col-md-4 input_wrapper top_input_wrapper">
                            <div class="form-group custom-form-select">
                              <select class="custom-select" id="vendor_resource_name_select">
                                  <option class="form-select-placeholder"></option>
                                @foreach($resources as $rowdata)
                                  <option value="{{$rowdata->resource_name}}" r_id = "{{$rowdata->id}}" r_code ="{{$rowdata->resource_code}}" >{{$rowdata->resource_name}}</option>
                                @endforeach
                              </select>
                              <div class="form-element-bar">
                              </div>
                              <label class="form-element-label input_label" id="vendor_resource_name_existing" for="resource_name">Resource Name</label>
                            </div>
                          </div>
                          <div class="col-md-4 input_wrapper">
                            <div class="form-group bmd-form-group">
                                <label for="resource_code_exist" class="bmd-label-floating input_label">Resource Code</label>
                                <input type="text" class="form-control form_input_field" id="vendor_resource_code_existing" name="res_code">
                            </div>
                          </div>
                          <div class="col-md-4 input_wrapper">
                            <div class="form-group bmd-form-group">
                                <label for="model_no_exist" class="bmd-label-floating input_label">Model Number</label>
                                <input type="text" class="form-control form_input_field" id="vendor_model_no_exist" name="model_no">
                            </div>
                          </div>
                        </div>

                        <div class="row" id="new_resource" style="display: none">
                          <div class="col-md-4 input_wrapper top_input_wrapper">
                            <div class="form-group bmd-form-group">
                                <label for="resource_name_new" class="bmd-label-floating input_label">Resource Name</label>
                                <input type="text" class="form-control form_input_field" id="resource_name_new" name="res_name_new">
                            </div>
                          </div>
                          <div class="col-md-4 input_wrapper">
                            <div class="form-group bmd-form-group">
                                <label for="resource_code_new" class="bmd-label-floating input_label">Resource Code</label>
                                <input type="text" class="form-control form_input_field" id="resource_code_new" name="res_code_new">
                            </div>
                          </div>
                          <div class="col-md-4 input_wrapper">
                            <div class="form-group bmd-form-group">
                                <label for="model_no_new" class="bmd-label-floating input_label">Model Number</label>
                                <input type="text" class="form-control form_input_field" id="model_no_new"  name="model_no_new">
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-check add_variable_checkbox">
                        <label class="form-check-label variable_lebel">
                          <input class="form-check-input variable_checkbox" type="checkbox" value="">Do you want to add variables?
                          <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>

                  <!-- Variable Section -->

                  <div class="card variable_card" style="display: none;">
                    <div class="card-header card-header-rose card-header-text sub_card">
                      <div class="card-text card_text_title">
                          <h4 class="card-title">Variables</h4>
                      </div>
                    </div>
                    <div class="card-body variable_card_body">
                      
                      <div class="row top_variable_row input_row">
                        <div class="col-md-4 input_wrapper">
                          <div class="form-group bmd-form-group">
                            <label for="field_name" class="bmd-label-floating input_label">Name Of the Field</label>
                            <input type="text" class="form-control form_input_field" name="var_name[]" aria-required="true">
                          </div>
                        </div>
                        <div class="col-md-4 input_wrapper">
                          <div class="form-group custom-form-select">
                            <select class="custom-select fa" id="" name="var_type[]" aria-required="true">
                              <option class="form-select-placeholder"></option>
                              <option class="" value="input">&#xf044; Input Field</option>
                              <option class="" value="checkbox">&#xf14a; Checkbox</option>
                              <option class="" value="datetime">&#xf073; Date & time</option>
                              <option class="" value="radio">&#xf192; Radio</option>
                            </select>
                            <div class="form-element-bar">
                            </div>
                            <label class="form-element-label input_label top_select_input_label" for="var_type">Select Field Type</label>
                          </div>
                        </div>
                        <div class="col-md-3 d-flex align-items-center justify-content-center variable_toggle">
                          <div class="togglebutton">
                              <label>
                                  <input type="checkbox" name="enable_disable[]" class="enable_disable">
                                  Disable <span class="toggle"></span> Enable
                              </label>
                          </div>
                        </div>
                        <div class="col-md-1 variable_cross_btn">
                          <a href="#" class="btn btn-link btn-danger btn-just-icon remove disabled"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                        </div>
                        <!-- <div class="col-md-2 text-center">
                          <a href="#" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                        </div> -->
                      </div>
                      <div class="row add_variable_btn_wrapper">
                        <div class="col-md-12 text-right">
                          <button class="btn btn-info btn-round btn-sm add_variable_btn" type="button">
                            <span class="btn-label">
                              <i class="material-icons">add</i>
                            </span>
                            Add More
                            <div class="ripple-container"></div>
                          </button>
                        </div>
                      </div>

                      
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12 text-right add_vendor_resource_btn_wrapper">
                      <button class="btn btn-warning btn-round btn-sm add_vendor_resource_btn" type="button">
                        Add Resources
                        <div class="ripple-container"></div>
                      </button>
                    </div>
                  </div>

                  <!-- Showing The correspondence resource table -->
                  
                   <div class="row" id="resource_table_row">
                      <div class="col-md-12 mx-auto">
                        <div class="table-responsive table-sales">
                          <table class="table resource_viewer_table">
                            <thead>
                                <tr class="table-success">
                                  <th>Resource Name</th>
                                  <th>Resource Code</th>
                                  <th>Model Number</th>
                                  <th>Variables</th>
                                  <th>Edit</th>
                                  <th class="disabled-sorting text-right">delete</th>
                                </tr>
                            </thead>
                            <tbody class="table_body vendor_res_table_body">
                                <?php $i=0; ?>
                                @foreach($vendor_resources as $rowdata)
                                 <?php
                                  $variables = $rowdata->getVariables($rowdata->id);
                                 ?>
                                  <tr>
                                  <input type="hidden" name="r_key[]" value="{{$rowdata->id}}">

                                  <input type="hidden" name="resource_id[]" value="{{$rowdata->resource_id}}">
                                  <input type="hidden" name="resource_name[]" value="{{$rowdata->resource_name}}">
                                  <input type="hidden" name="resource_code[]" value="{{$rowdata->resource_code}}">
                                  <input type="hidden" name="resource_model_no[]" value="{{$rowdata->resource_model_no}}">
                                  <input type="hidden" name="row_index[]" value="{{$i}}">
                                  
                                      <?php $arr = []; ?>
                                  @foreach($variables as $variable)
                                    <input type="hidden" name="var_key{{$i}}[]" value="{{$variable->id}}">
                                    <input type="hidden" class="variable_input_hidden" name="var_name0{{$i}}[]" value="{{$variable->var_name}}">
                                    <input type="hidden" class="variable_input_hidden" name="var_type0{{$i}}[]" value="{{$variable->var_type}}">
                                    <input type="hidden" class="variable_input_hidden" name="var_status0{{$i}}[]" value="{{$variable->enable_disable}}">
                                      <?php
                                        $arr[] = array(
                                        'var_name' => $variable->var_name,
                                        'var_type' => $variable->var_type,
                                        'var_status' => $variable->enable_disable == 1 ? 'Enabled' : 'Disabled',
                                        );
                                      ?>
                                  @endforeach
                                      <?php $json = json_encode($arr); ?>

                                    <td>{{$rowdata->resource_name}}</td>
                                    <td>{{$rowdata->resource_code}}</td>
                                    <td>{{$rowdata->resource_model_no}}</td>
                                    <td>
                                      @if(!$variables->isEmpty())
                                      <button class="btn btn-sm btn-info stored_variable_td_btn" type="button" value="{{$rowdata->id}}">Show</button>
                                      @else
                                      N/A
                                      @endif
                                    </td>
                                    <td style="display: none;"> {{$json}} </td>
                                    @if($rowdata->resource_id == 0)
                                    <td style="display: none;"> new </td>
                                    @else
                                    <td style="display: none;"> existing </td>
                                    @endif

                                    <td> <a href="#" id="" name="" class="btn btn-link btn-success btn-just-icon resource_edit"><i class="material-icons">edit</i></a> </td>
                                    
                                    <td class="text-right"> 
                                      <a href="#" id="vendor_resource_delete_btn_" name="" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">close</i></a>
                                    </td>
                                  </tr>
                                  <?php $i++; ?>
                                @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!-- The Modal -->
                      @foreach($vendor_resources as $rowdata)
                                 <?php
                                  $variables = $rowdata->getVariables($rowdata->id);
                                 ?>
                      <div id="stored_var_myModal{{$rowdata->id}}" class="modal stored_var_modal" style="display: none;">
                        
                        <div class="card modal-content variable_table_card">
                          <div class="card-header card-header-rose card-header-text">
                            <div class="card-icon">
                                <i class="material-icons">ballot</i>
                            </div>
                            <!-- <div class="card-text"> -->
                              <h4 class="card-title">Variables Selected</h4>
                          </div>
                          <div class="card-body">
                            <div class="table-responsive table-sales variable_table_wrapper">  
                              <table class="table variable_table2">
                                <thead class="text-success">
                                  <tr>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <!-- @foreach($variables as $variable)
                                    <tr>
                                      <td>{{$variable->var_name}}</td>
                                      <td>{{$variable->var_type}}</td>
                                      <td>@if($variable->enable_disable == 1) enabled @else disabled @endif</td>
                                    </tr>
                                  @endforeach -->

                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                       
                        <span class="close">×</span>
                        <!-- 
                        <img class="modal-content" id="img01">
                        <div id="caption"></div> -->
                      </div>
                       @endforeach

                       <!-- Edit Modal -->
                      <div id="myEditModal" class="modal" style="display: none;">
                        <div class="card modal-content edit_resource_card">
                          <div class="card-header card-header-rose card-header-text" style="width:100%">
                            <div class="card-icon">
                                <i class="material-icons">edit</i>
                            </div>
                            <!-- <div class="card-text"> -->
                              <h4 class="card-title">Edit Resources</h4>
                          </div>
                          <div class="card-body edit_resource_card_body mt-5">
                            <div class="row">
                              <div class="col-md-4 new_resource_col" style="display:none;">
                                <div class="form-group bmd-form-group">
                                  <label for="resource_code_exist" class="bmd-label-floating input_label">Resource Name</label>
                                  <input type="text" class="form-control form_input_field" value="" name="edit_res_name">
                                </div>
                              </div>
                              <div class="col-md-4 input_wrapper existing_resource_col" style="display:none;">
                                <div class="form-group custom-form-select">
                                  <select class="custom-select hasvalue edit_modal_res_name" id="vendor_resource_name_select">
                                      <option class="form-select-placeholder"></option>
                                    @foreach($resources as $rowdata)
                                      <option value="{{$rowdata->resource_name}}" r_id = "{{$rowdata->id}}" r_code ="{{$rowdata->resource_code}}" >{{$rowdata->resource_name}}</option>
                                    @endforeach
                                  </select>
                                  <div class="form-element-bar">
                                  </div>
                                  <label class="form-element-label input_label" id="vendor_resource_name_existing" for="resource_name">Resource Name</label>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group bmd-form-group">
                                  <label for="resource_code_exist" class="bmd-label-floating input_label">Resource Code</label>
                                  <input type="text" class="form-control form_input_field edit_modal_res_code" value="" name="edit_res_code">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group bmd-form-group">
                                  <label for="resource_code_exist" class="bmd-label-floating input_label">Model Number</label>
                                  <input type="text" class="form-control form_input_field" value="" name="edit_model_no">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-10 mx-auto">
                                <div class="card edit_resource_variable_card" style="display:none">
                                  <div class="card-header card-header-rose card-header-text">
                                    <div class="card-icon">
                                        <i class="material-icons">edit</i>
                                    </div>
                                    <h4 class="card-title">Variables</h4>
                                  </div>
                                  <div class="card-body edit_resource_variable_card_body">
                                    
                                    
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12 text-right">
                                <button class="btn btn-success update_resource_btn btn-round" type="button"> Update </button>
                              </div>
                            </div>
                          </div>
                        </div>

                        <span class="close">×</span>
                      </div>

                      <!-- The Modal -->
                      <div id="myModal" class="modal" style="display: none;">
                        
                        <div class="card modal-content variable_table_card">
                          <div class="card-header card-header-rose card-header-text">
                            <div class="card-icon">
                                <i class="material-icons">ballot</i>
                            </div>
                            <!-- <div class="card-text"> -->
                              <h4 class="card-title">Variables Selected</h4>
                          </div>
                          <div class="card-body">
                            <div class="table-responsive table-sales variable_table_wrapper">  
                              <table class="table variable_table">
                                <thead class="text-success">
                                  <tr>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                  </tr>
                                </thead>
                                <tbody>



                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>

                        <span class="close">×</span>
                        <!-- 
                        <img class="modal-content" id="img01">
                        <div id="caption"></div> -->
                      </div>
                      
                    </div>
                    
                </div>
                

              </div>



              <div class="card quickbooks_update_checking_card">
                              <div class="card-header card-header-rose card-header-text sub_card">
                                <div class="card-text card_text_title">
                                  <h4 class="card-title">Quickbooks</h4>
                                </div>
                              </div>

                              <div class="card-body">
                                
                                <div class="row">
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-check">
                                      <label class="form-check-label">
                                        <input class="form-check-input singleCustomerQB" type="checkbox" name="singleCustomerQB" value="1" @if($vendor->quickbooks == 1) checked @endif> Check here If you want to update data to Quickbooks as well
                                        <span class="form-check-sign">
                                          <span class="check"></span>
                                        </span>
                                      </label>
                                    </div>
                                  </div>
                                </div>

                              </div>
                        </div>

            


              <div class="form-group bmd-form-group save_btn_wrapper text-right">
                <button type="submit" class="btn save_btn custom-btn-one">Update</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>








<script>
  $(document).ready(function() {


    $('.resource_id').change(function() {
      //alert('okk');
      if (this.checked) {
          $('#existing_resource').show();
          $('#new_resource').hide();
      }
      else{
          $('#new_resource').show();
          $('#existing_resource').hide();
      }        
    });

    $('.variable_checkbox').change(function(){
      if(this.checked){
        $('.variable_card').fadeIn('5000','swing');
      }else{
        $('.variable_card').fadeOut('3000','swing', function(){
          $('.variable_card_body .var_input_row').remove();
          $('.top_variable_row input[type=text]').val('');
          $('.top_variable_row .input_wrapper .form-group').removeClass('is-filled');
          $('.top_variable_row .custom-select').removeClass('hasvalue').find('option:first').prop('selected', true);
          $('.top_variable_row .enable_disable')[0].checked = false;
        });

      }
    })

    $( ".custom-select" ).change(function() {
      if(this.value.length > 0){
          $( this ).addClass( "hasvalue" );
      }
    })


  });


      $( "#vendor_resource_name_select" ).change(function() {
      //alert($('option:selected', this).attr('r_id'));

      $('#vendor_resource_code_existing').val($('option:selected', this).attr('r_code'));
      $('#vendor_resource_code_existing').parent().addClass('is-filled');
      $('#vendor_resource_code_existing').attr('readonly', true);
    })

      $( ".edit_modal_res_name" ).change(function() {
      //alert($('option:selected', this).attr('r_id'));

      $('.edit_modal_res_code').val($('option:selected', this).attr('r_code'));
      $('.edit_modal_res_code').parent().addClass('is-filled');
     // $('.edit_modal_res_code').attr('readonly', true);
    })



let value = $('input[name="vendor_key"]').val();
let type = 'vendor';
let _token = $('input[name="_token"]').val();
var preloaded = [];
$.ajax({
            async: false,
            url:"{{ route('getImage') }}",
            method:"POST",
            data:{value:value, type:type, _token:_token},
            success:function(result)
            {
              var obj = JSON.parse(result)
              $.each(obj, function(index) {
                var data = { id: obj[index].id, src: obj[index].file };
                preloaded.push(data);
              });

              preloadedImage(preloaded);
            }
         })

function preloadedImage(param) {

    $('.input-images').imageUploader({
      preloaded: param,
      imagesInputName:'images',
      preloadedInputName:'preloaded',
      label:'Drag & Drop files here or click to browse'
  }); 

}


$('.singleCustomerQB').change(function() {
          if($(this).is(":checked")) {
              $.ajax({
                        url:"{{ route('singleCustomerQB') }}",
                        method:"GET",
                        cache: false,
                        success:function(result)
                        {
                         if(result == 'connect'){
                            alert('Please click on connect to quickbooks button from to get access token');
                            $('.singleCustomerQB').prop("checked", false);
                         }
                         else if(result == 'expire'){
                          alert('Your access token is expired, Please click on Connect to Quickbooks button');
                          $('.singleCustomerQB').prop("checked", false);
                         }
                        
                        }
                     })
          }       
      });


if($('#resource_table_row tbody tr').length == 0) {
  $('#resource_table_row').css('display','none');
}

function validateMyForm()
{
  if($('#vendor_resource_name_select').val() !== '' || $('#vendor_resource_code_existing').val() !== '' || $('#vendor_model_no_exist').val() !== '' || $('#resource_name_new').val() !== '' || $('#resource_code_new').val() !== '' || $('#model_no_new').val() !== '')
  { 
    alert("Please click add resources button first to add your resources");
    return false;
  }

  return true;
}


/* ------ Stored Td Editing -----*/

$('.resource_stored_edit').click(function(){
  $('#myEditModal').show();
})

</script>


@endsection



              