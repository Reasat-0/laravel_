
@extends('layouts.master')

@section('content')
        <div class="content add_product_content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12 mx-auto">
                
                
                <div class="card add_inv_service_root_card">
                  <div class="card-header card-header-tabs card-header-rose custom_card_header long_card_header creat_inv_pro_card_header">
                    <div class="nav-tabs-navigation">
                      <div class="nav-tabs-wrapper">
                        <p class="nav-tabs-title">Add Service</p>
                        <ul class="nav nav-tabs" data-tabs="tabs" id="tabMenu">
                          <li class="nav-item">
                            <a class="nav-link" href="#profile" data-toggle="tab">
                              <i class="material-icons">bug_report</i> Category
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#messages" data-toggle="tab">
                              <i class="material-icons">code</i> Sub Category
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#variable" data-toggle="tab">
                              <i class="material-icons">code</i> Variable
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#settings" data-toggle="tab">
                              <i class="material-icons">cloud</i> Service
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div style="margin-top: 15px;">
                      @include('partials.messages')
                  </div>

                  <div class="card-body">
                    <div class="tab-content">
                      <div class="tab-pane" id="profile">
                        <div class="card card_modified">
                          <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card">
                            <div class="card-text">
                              <h4 class="card-title box_label_title">Add Category</h4>
                            </div>
                          </div>
                          <div class="card-body ">
                            <form method="post" action="{{ route('products.store') }}" class="form-horizontal">
                            @csrf
                            <input type="hidden" name="tab" value="category">
                            <input type="hidden" name="type" value="product">
                              <div class="form-group bmd-form-group">
                                <label for="category_name" class="bmd-label-floating input_label"> Category Name *</label>
                                <input type="text" class="form-control form_input_field" id="category_name" name="category_name" required="true" aria-required="true">
                              </div>
                              <div class="form-group bmd-form-group save_btn_wrapper text-right">
                                <button type="submit" class="btn custom-btn-one save_btn">Save</button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane sub_holder" id="messages">
                        <div class="card card_modified">
                          <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card">
                            <div class="card-text">
                              <h4 class="card-title box_label_title">Add Sub Category</h4>
                            </div>
                          </div>
                          <div class="card-body ">
                            <form method="post" action="{{ route('products.store') }}" class="form-horizontal">

                            @csrf
                            <input type="hidden" name="tab" value="subcategory">
                            <input type="hidden" name="type" value="product">

                                  <div class="row">
                                    <div class="col-md-6 custom_select2_col">
                                      <div class="form-group custom-form-select">
                                        <select class="custom-select select2" id="pro-sub-category-custom-select" name="cat_id" required="true" aria-required="true">
                                          <option class="form-select-placeholder"></option>
                                          @foreach($cat->where('type', 'product') as $rowdata)
                                          <option value="{{$rowdata->id}}">{{$rowdata->category_name}}</option>
                                          @endforeach
                                        </select>
                                        <div class="form-element-bar">
                                        </div>
                                        <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Category</label>
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="subcategory_name" class="bmd-label-floating input_label">Sub Category Name *</label>
                                        <input type="text" class="form-control form_input_field" id="subcategory_name" name="subcategory_name" required="true" aria-required="true">
                                      </div>
                                    </div>
                                  </div>
                                <div class="form-group bmd-form-group save_btn_wrapper text-right">
                                  <button type="submit" class="btn custom-btn-one save_btn">Save</button>
                                </div>
                            </form>
                          </div>
                        </div>
                      </div>

                      <div class="tab-pane sub_holder" id="variable">
                              <div class="card card_modified">
                                <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card">
                                  <div class="card-text">
                                    <h4 class="card-title box_label_title">Add Variables</h4>
                                  </div>
                                </div>
                                <div class="card-body variable_card_body_main_section">
                                  <form method="post" action="{{ route('products.store') }}" class="form-horizontal">

                                    @csrf
                                    <input type="hidden" name="tab" value="variable">
                                    <input type="hidden" name="type" value="product">

                                        <div class="row variable_first_row">
                                          <div class="col-md-5 input_wrapper">
                                            <div class="form-group bmd-form-group">
                                              <label for="variable_name" class="bmd-label-floating input_label">Name of Variable *</label>
                                              <input type="text" class="form-control form_input_field" id="variable_name" name="variable_name" required="true" aria-required="true">
                                            </div>
                                          </div>
                                        </div>

                                        <div class="row var_param_row">
                                          
                                          <div class="col-md-3 input_wrapper first">
                                            <div class="form-group bmd-form-group">
                                              <label for="parameter_name" class="bmd-label-floating input_label">Name of parameter *</label>
                                              <input type="text" class="form-control form_input_field" id="parameter_name" name="var_param_name[]" required="true" aria-required="true">
                                            </div>
                                          </div>

                                          <div class="col-md-2 second input_wrapper custom_select2_col">
                                            <div class="form-group custom-form-select">
                                              <select class="custom-select var_data_type select2 form_input_field" id="var_data_type" name="var_param_data_type[]" required="true" aria-required="true">
                                                <option class="form-select-placeholder"></option>
                                                <option value="decimal">Decimal</option>
                                                <option value="integer">Integer</option>
                                                <option value="datetime">DateTime</option>
                                                <option value="string">String</option>
                                                <option value="percentage">Percentage</option>
                                              </select>
                                              <div class="form-element-bar">
                                              </div>
                                              <label class="form-element-label" for="variable_data_type input_label">Data Type</label>
                                            </div>
                                          </div>

                                          <div class="col-md-3 d-flex align-items-center  var_param_value_wrapper input_wrapper">
                                            <div class="togglebutton admin-role-toggle priceclass_toggle_section">
                                                <label>
                                                  <input type="hidden" class="var_param_value_type" name="var_param_value_type[]" value="0">
                                                    <input class="var_param_toggle price_class_toggle" type="checkbox" name="" checked="">
                                                    Predefined <span class="toggle"></span> UserDefined
                                                </label>
                                            </div>
                                          </div>

                                          <div class="col-md-3 input_wrapper four">
                                            <div class="form-group bmd-form-group">
                                              <label for="parameter_name" class="bmd-label-floating input_label">Value </label>
                                              <input type="text" class="form-control form_input_field" id="var_param_value" name="var_param_value[]">
                                            </div>
                                          </div>

                                          <div class="col-md-1 var_cross_btn">
                                              <a href="#" class="btn btn-link btn-danger btn-just-icon remove disabled"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                                          </div>

                                        </div>

                                        <div class="row add_variable_btn_wrapper new_var_wrapper">
                                            <div class="col-md-12 text-right">
                                              <button class="btn btn-info btn-round btn-sm add_var_btn" type="button">
                                                <span class="btn-label">
                                                  <i class="material-icons">add</i>
                                                </span>
                                                Add Parameter
                                                <div class="ripple-container"></div>
                                              </button>
                                            </div>
                                        </div>



                                      <div class="form-group bmd-form-group save_btn_wrapper text-right">
                                        <button type="submit" class="btn custom-btn-one save_btn">Save</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>


                      <div class="tab-pane sub_holder tab_pane_card" id="settings">
                        <div class="card">
                          <div class="card-header card-header-rose card-header-text lead_card_header user_sub_card">
                            <div class="card-icon">
                                <i class="material-icons">people_outline</i>
                            </div>
                            <!-- <div class="card-text"> -->
                              <h4 class="card-title">SERVICE DETAILS</h4>
                          </div>
                          <div class="card-body">
                            <form method="post" action="{{ route('products.store') }}" class="form-horizontal" enctype="multipart/form-data" onsubmit="return validateMyForm();">
                                    
                              @csrf
                              <input type="hidden" name="tab" value="product">
                              <div class="card">
                                <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card">
                                  <div class="card-text">
                                    <h4 class="card-title box_label_title">General Section</h4>
                                  </div>
                                </div>
                                <div class="card-body ">
                                  <div class="row">
                                    <div class="col-md-6 custom_select2_col">
                                      <div class="form-group custom-form-select">
                                        <select class="custom-select cat_product select2" id="pro-category-custom-select" name="cat_id" required="true" aria-required="true">
                                          <option class="form-select-placeholder"></option>
                                          @foreach($cat->where('type', 'product') as $rowdata)
                                          <option value="{{$rowdata->id}}">{{$rowdata->category_name}}</option>
                                          @endforeach
                                        </select>
                                        <div class="form-element-bar">
                                        </div>
                                        <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Category</label>
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper custom_select2_col">
                                      <div class="form-group custom-form-select">
                                        <select class="custom-select subcat_product select2" id="sub-category-custom-select" name="subcat_id" required="true" aria-required="true">
                                        </select>
                                        <div class="form-element-bar">
                                        </div>
                                        <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Sub Category</label>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row input_row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="product_name" class="bmd-label-floating input_label">Name of the product *</label>
                                        <input type="text" class="form-control form_input_field" id="product_name" name="product_name" required="true" aria-required="true">
                                      </div>
                                    </div>
                                    
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="sku" class="bmd-label-floating input_label">SKU *</label>
                                        <input type="text" class="form-control form_input_field" id="sku" name="sku" required="true" aria-required="true">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row input_row">
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="web_short_des" class="bmd-label-floating input_label textarea_label">Web Short Description</label>
                                        <textarea class="form-control form_input_field" rows="4" name="web_short_des" id="pro_web_short_des"></textarea>
                                      </div>
                                    </div>
                                    <div class="col-md-6 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="web_long_des" class="bmd-label-floating input_label textarea_label">Web Long Description</label>
                                        <textarea class="form-control form_input_field" rows="4" name="web_long_des" id="pro_web_long_des"></textarea>
                                      </div>
                                    </div>    
                                  </div>
                                  <!-- Image row -->
                                  <div class="row">
                                      <div class="col-md-12">
                                          <h4 class="title">Image</h4>
                                          <div class="input-images"></div>
                                          <div id="genModal" class="inv_img_modal">
                                            <span class="gen_close inv_img_close">×</span>
                                            <img class="inv_modal_content" id="gen_modal_img">
                                            <div id="caption"></div>
                                          </div>
                                      </div>
                                  </div>
                                        
                                          
                                          
                                  <!-- IMage row -->
                                </div>
                              </div>
                              <div class="card">
                                <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card">
                                  <div class="card-text">
                                    <h4 class="card-title box_label_title">Add Resources Section</h4>
                                  </div>
                                </div>
                                <div class="card-body">
                                  <div class="row input_row">
                                    <div class="col-md-3 input_wrapper custom_select2_col">
                                      <div class="form-group custom-form-select">
                                        <select class="custom-select cat product_small_text select2" id="category-custom-select" name="">
                                          <option class="form-select-placeholder"></option>
                                          @foreach($cat->where('type', 'inventory') as $rowdata)
                                          <option value="{{$rowdata->id}}">{{$rowdata->category_name}}</option>
                                          @endforeach
                                        </select>
                                        <div class="form-element-bar">
                                        </div>
                                        <label class="form-element-label input_label product_small_label top_select_input_label" for="category_product_select">Category</label>
                                      </div>
                                    </div>
                                    <div class="col-md-3 input_wrapper custom_select2_col">
                                      <div class="form-group custom-form-select">
                                        <select class="custom-select subcat product_small_text select2" id="sub_category_product_select" name="">                           
                                          
                                        </select>
                                        <div class="form-element-bar">
                                        </div>
                                        <label class="form-element-label input_label product_small_label top_select_input_label" for="sub_category_product_select">Sub Category</label>
                                      </div>
                                    </div>
                                    <div class="col-md-3 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="resource_name" class="bmd-label-floating input_label product_small_label">Resource Name</label>
                                        <input type="text" class="form-control form_input_field product_small_text " id="resource_name" name="" disabled="">
                                      </div>
                                    </div>
                                    <div class="col-md-3 input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <label for="resource_code" class="bmd-label-floating input_label product_small_label">Resource Code</label>
                                        <input type="text" class="form-control form_input_field product_small_text" id="resource_code" name="" disabled="">
                                      </div>
                                    </div>

                                    <input type="hidden" name="" id="unit_price">
                                    <input type="hidden" name="" id="inventory_id">
                                  </div>

                                  <div class="row">
                                    <div class="col-md-12 resource_adder_wrapper">
                                      <div class="form-group bmd-form-group save_btn_wrapper" style="text-align: right">
                                         <button type="button" class="btn btn-round btn-warning btn-sm add_inventory">Add Resources<div class="ripple-container"></div></button>
                                       </div>
                                    </div>
                                  </div>
                                  <!-- Showing The corresponds -->
                                  <div class="row" id="product_viewer_row" style="display: none;">
                                      <div class="col-md-12 mx-auto">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="table-responsive table-sales">
                                              <table class="table product_viewer_table">
                                                <thead>
                                                    <tr class="table-success">
                                                      <th>Resource Name</th>
                                                      <th>Unit Price (per hour)</th>
                                                      <th class="disabled-sorting text-right">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="table_body">
                                                  
                                                </tbody>
                                              </table>
                                              <div id="myEditModal" class="modal" style="display: none;">
                                                <div class="card modal-content edit_resource_card" style="width:50%">
                                                  <div class="card-header card-header-rose card-header-text" style="width:100%">
                                                    <div class="card-icon">
                                                        <i class="material-icons">edit</i>
                                                    </div>
                                                    <!-- <div class="card-text"> -->
                                                      <h4 class="card-title">Edit Resources</h4>
                                                  </div>
                                                  <div class="card-body edit_product_card_body mt-5">
                                                    <div class="row">
                                                      <div class="col-md-6" style="">
                                                        <div class="form-group bmd-form-group">
                                                          <label for="resource_name" class="bmd-label-floating input_label">Resource Name</label>
                                                          <input type="text" class="form-control form_input_field" value="" name="edit_pro_name">
                                                        </div>
                                                      </div>
                                                      <div class="col-md-6 input_wrapper" style="">
                                                        <div class="form-group bmd-form-group">
                                                          <label for="unit_price" class="bmd-label-floating input_label">Unit Price ( $ )</label>
                                                          <input type="text" class="form-control form_input_field" value="" name="edit_unit_price">
                                                        </div>
                                                      </div>
                                                    </div>
                                                    <div class="row">
                                                      <div class="col-md-12 text-right">
                                                        <button class="btn btn-success update_product_btn btn-round" type="button"> Update </button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                                <span class="close">×</span>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                  <!-- end of corresponds -->
                                   
                                </div>
                              </div>


                              <!-- Variable Section -->

                              <div class="card">
                                <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card inv_sub_card_img_zindex">
                                  <div class="card-text">
                                    <h4 class="card-title">Variable Section</h4>
                                  </div>
                                </div>
                                <div class="card-body variable_card_body variable_selection_card_body">
                                  <div class="row top_variable_row input_row" id="var_row_create">
                                    <div class="col-md-6 input_wrapper custom_select2_col">
                                      <div class="form-group custom-form-select">
                                        <select class="custom-select varSelection select2 var_blank_it" id="variable_selection_dw" name="">
                                          <option class="form-select-placeholder"></option>
                                          @foreach($variables as $rowdata)
                                            <option value="{{$rowdata->id}}">{{$rowdata->variable_name}}</option>
                                          @endforeach
                                        </select>
                                        <div class="form-element-bar">
                                        </div>
                                        <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Select Variable</label>
                                      </div>
                                    </div>
                                    <div class="col-md-6 text-right variable_adder_btn_wrapper">
                                      <button class="btn btn-info btn-round btn-sm variable_adder_btn" type="button">
                                        <span class="btn-label">
                                          <i class="material-icons">add</i>
                                        </span>
                                        Add
                                        <div class="ripple-container"></div>
                                      </button>                                              
                                    </div>
                                  </div>
                                  <div class="card product_quotaion_table_card variable_selection_table_card" style="display:none">
                                    <div class="card-body table_card_wrapper">
                                      <div class="card variable_table_header">
                                        <div class="quotation_table_section">
                                          <div class="quotation_table_row">
                                            <div class="var_col">
                                              <p>Variable Name</p>
                                            </div>
                                            <div class="quote_col single_resource_main">
                                              <div class="all_single_resounce">
                                                <div class="single_resource_col">
                                                  <p>Parameters</p>
                                                </div>
                                                <div class="single_resource_col">
                                                 <p>Data Type</p>
                                                </div>
                                                <div class="single_resource_col">
                                                 <p>Status</p>
                                                </div>
                                                <div class="single_resource_col">
                                                 <p>Cost</p>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>                                                
                                      </div>

                                      <div class="var_section">

                                      </div>
                                      

                                    </div>                                            
                                  </div>                    
                                </div>
                              </div>


                              <div class="form-group bmd-form-group save_btn_wrapper text-right">
                                <button type="submit" class="btn custom-btn-one save_btn">Save<div class="ripple-container"></div></button>
                              </div>
                            </form>
                          </div>
                        </div>
                    </div>
                    </div>
                  </div>
                </div>



              </div>
            </div>
          </div>
        </div>


<script>
  $(document).ready(function() {
    
    $( ".custom-select" ).change(function() {
      if(this.value.length > 0){
          $( this ).addClass( "hasvalue" );
      }
    })


    $('.cat_product').change(function(){
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getSubcategory') }}",
            method:"POST",
            data:{value:value, _token:_token, type:'product'},
            success:function(result)
            {
             $('.subcat_product').html(result);
            }
         })
      }
     });

    $('.cat').change(function(){
      if($('#resource_name').val() != '' || $('#resource_code').val() != ''){
        $('#resource_name').val('');
        $('#resource_name').parent().removeClass('is-filled');
        $('#resource_code').val('');
        $('#resource_code').parent().removeClass('is-filled');
      }

      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getSubcategory') }}",
            method:"POST",
            data:{value:value, _token:_token, type: 'inventory'},
            success:function(result)
            {
             $('.subcat').html(result);
            }
         })
      }
     });



        $('.subcat').change(function(){
      //alert($(this).val());
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getResourceInfoBySubcat') }}",
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result)
            {
              var obj = JSON.parse(result)
              //alert(obj[0].resource_name);
              //alert(obj[0].resource_code);
             if(obj[0].resource_name != null){
              $('#resource_name').parent().addClass('is-filled'); 
              //$('#resource_name').attr('readonly', true);
             }
             else{
              $('#resource_name').parent().removeClass('is-filled'); 
              //$('#resource_name').attr('readonly', false);
             }
             if(obj[0].resource_code != null){
              $('#resource_code').parent().addClass('is-filled'); 
              //$('#resource_code').attr('readonly', true);
             }
             else{
              $('#resource_code').parent().removeClass('is-filled'); 
              //$('#resource_code').attr('readonly', false);
             }
             
             $('#resource_name').val(obj[0].resource_name);
             $('#resource_code').val(obj[0].resource_code);
             $('#unit_price').val(obj[0].price_per_hour_service);
             $('#inventory_id').val(obj[0].inventory_id);
            }
         })
      }
     });


/*    $("form").submit(function () {

        var this_master = $(this);

        this_master.find('input[type="checkbox"]').each( function () {
            var checkbox_this = $(this);


            if( checkbox_this.is(":checked") == true ) {
                checkbox_this.attr('value','1');
            } else {
                checkbox_this.prop('checked',true);
                //DONT' ITS JUST CHECK THE CHECKBOX TO SUBMIT FORM DATA    
                checkbox_this.attr('value','0');
            }
        })
    })*/


  });

$('.input-images').imageUploader({imagesInputName:'images'});

function validateMyForm()
{
  if($('#resource_name').val() !== '')
  { 
    alert("Please click add resources button first to add your resources");
    return false;
  }

  return true;
}



$('.var_param_toggle').change(function() {
    if(this.checked){
      $(this).parent().find('.var_param_value_type').val(0);
      $(this).parents('.var_param_row').find('#var_param_value').attr('readonly', false).val('');
    }
    else{
      $(this).parent().find('.var_param_value_type').val(1);
      $(this).parents('.var_param_row').find('#var_param_value').attr('readonly', true).val('');
    }

});

$('.var_data_type').change(function() {
    if($(this).val() == 'decimal'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'number').prop('step', '0.01');
    }
    else if($(this).val() == 'integer'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'number').prop('step', false);
    }
    else if($(this).val() == 'datetime'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'text').prop('step', false);
    }
    else if($(this).val() == 'string'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'text').prop('step', false);
    }
    else if($(this).val() == 'percentage'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'text').prop('step', false);
    }
});


$(document).on('click','.variable_adder_btn', function(){
  let var_id = $('.varSelection').val();
  let _token = $('input[name="_token"]').val();

  $.ajax({
            url:"{{ route('addVariableSection') }}",
            method:"POST",
            data:{var_id:var_id, _token:_token},
            success:function(result)
            { 
              $('.var_section').append(result);
              $('.variable_selection_table_card').show();
            }
         })
  $('.custom_select2_col select.var_blank_it').val(null).trigger('change');
  $('#variable_selection_dw').removeClass('hasvalue')
});


$(document).ready(function () {
  //alert($('#tabMenu a[href="#{{ old('tab') }}"]').attr('href'));
  if($('#tabMenu a[href="#{{ old('tab') }}"]').attr('href') == undefined){
      $('#tabMenu a[href="#profile"]').tab('show')  
  }
  else{
      $('#tabMenu a[href="#{{ old('tab') }}"]').tab('show')
}

});

</script>

@endsection


