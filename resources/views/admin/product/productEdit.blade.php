@extends('layouts.master')

@section('content')

        <div class="content edit_product_content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12 mx-auto">
                  <div class="card">
                    <div class="card-header card-header-tabs card-header-rose">
                      <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                          <ul class="nav nav-tabs" data-tabs="tabs">
                            <li class="nav-item">
                              <a class="nav-link active" href="#profile" data-toggle="tab">
                                <i class="material-icons">ballot</i> Edit Service
                                <div class="ripple-container"></div>
                              </a>
                            </li>
                            
                          </ul>
                        </div>
                      </div>
                    </div>

                    <div style="margin-top: 15px;">
                        @include('partials.messages')
                    </div>

                    <div class="card-body">
                      <form method="post" action="{{ route('products.update', $product->id) }}" class="form-horizontal" enctype="multipart/form-data" onsubmit="return validateMyForm();">
                          
                          @csrf
                          @method('PUT')

                        <input type="hidden" name="product_key" value="{{$product->id}}">

                        <div class="card">
                          <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card">
                            <div class="card-text">
                              <h4 class="card-title box_label_title">General Section</h4>
                            </div>
                          </div>

                          <div class="card-body increased_margin_t_cardbody_proincust">
                          
                            <input type="hidden" name="tab" value="product">
                            <input type="hidden" name="p_key" value="{{$product->id}}">

                            <div class="row sub_holder">
                              <div class="col-md-6 custom_select2_col">
                                <div class="form-group custom-form-select">
                                  <select class="custom-select cat_product hasvalue select2" id="edit-category-custom-select" name="cat_id" required="true" aria-required="true">
                                    <option class="form-select-placeholder"></option>
                                    @foreach($cat->where('type', 'product') as $rowdata)
                                    <option value="{{$rowdata->id}}" @if($rowdata->id == $product->cat_id) selected @endif>{{$rowdata->category_name}}</option>
                                    @endforeach
                                  </select>
                                  <div class="form-element-bar">
                                  </div>
                                  <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Category</label>
                                </div>
                              </div>
                              <div class="col-md-6 input_wrapper custom_select2_col">
                                <div class="form-group custom-form-select">
                                  <select class="custom-select subcat_product hasvalue select2" id="sub-category-custom-select" name="subcat_id" required="true" aria-required="true">
                                    @foreach($subcat as $rowdata)
                                          <option value="{{$rowdata->id}}" @if($rowdata->id == $product->subcat_id) selected @endif>{{$rowdata->subcategory_name}}</option>
                                      @endforeach
                                  </select>
                                  <div class="form-element-bar">
                                  </div>
                                  <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Sub Category</label>
                                </div>
                              </div>
                            </div>

                            <div class="row input_row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="product_name" class="bmd-label-floating input_label">Name of the product *</label>
                                  <input type="text" class="form-control form_input_field" id="product_name" name="product_name" required="true" aria-required="true" value="{{$product->product_name}}">
                                </div>
                              </div>
                              
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group is-filled">
                                  <label for="sku" class="bmd-label-floating input_label">SKU *</label>
                                  <input type="text" class="form-control form_input_field" id="sku" name="sku" required="true" aria-required="true" value="{{$product->sku}}">
                                </div>
                              </div>
                            </div>
                            <div class="row input_row">
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group">
                                  <label for="web_short_des" class="bmd-label-floating input_label textarea_label">Web Short Description</label>
                                  <textarea class="form-control form_input_field" rows="4" name="web_short_des" id="pro_web_short_des">{{$product->web_short_des}}</textarea>
                                </div>
                              </div>
                              <div class="col-md-6 input_wrapper">
                                <div class="form-group bmd-form-group">
                                  <label for="web_long_des" class="bmd-label-floating input_label textarea_label">Web Long Description</label>
                                  <textarea class="form-control form_input_field" rows="4" name="web_long_des" id="pro_web_long_des">{{$product->web_long_des}}</textarea>
                                </div>
                              </div>                              
                            </div>
                            

                            <div class="row">
                              <div class="col-md-12">
                                <h4 class="title">Image</h4>
                                <div class="input-images edit_section_img"></div>
                                <div id="genModal" class="inv_img_modal">
                                  <span class="gen_close inv_img_close">×</span>
                                  <img class="inv_modal_content" id="gen_modal_img">
                                  <div id="caption"></div>
                                </div>
                              </div>
                            </div>
                            
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card">
                            <div class="card-text">
                              <h4 class="card-title box_label_title">Add Resources Section</h4>
                            </div>
                          </div>
                          <div class="card-body">
                            <div class="row input_row">
                              <div class="col-md-3 input_wrapper custom_select2_col">
                                <div class="form-group custom-form-select">
                                  <select class="custom-select cat product_small_text select2" id="category-custom-select" name="">
                                          <option class="form-select-placeholder"></option>
                                          @foreach($cat->where('type', 'inventory') as $rowdata)
                                          <option value="{{$rowdata->id}}">{{$rowdata->category_name}}</option>
                                          @endforeach
                                  </select>
                                  <div class="form-element-bar">
                                  </div>
                                  <label class="form-element-label input_label product_small_label top_select_input_label" for="category_product_select">Category</label>
                                </div>
                              </div>
                              <div class="col-md-3 input_wrapper custom_select2_col">
                                <div class="form-group custom-form-select">
                                  <select class="custom-select subcat product_small_text select2" id="sub_category_product_select" name="">                           
                                    
                                  </select>
                                  <div class="form-element-bar">
                                  </div>
                                  <label class="form-element-label input_label product_small_label top_select_input_label" for="sub_category_product_select">Sub Category</label>
                                </div>
                              </div>
                              <div class="col-md-3 input_wrapper">
                                <div class="form-group bmd-form-group">
                                  <label for="resource_name" class="bmd-label-floating input_label product_small_label">Resource Name</label>
                                  <input type="text" class="form-control form_input_field product_small_text" id="resource_name" name="" disabled="">
                                </div>
                              </div>
                              <div class="col-md-3 input_wrapper">
                                <div class="form-group bmd-form-group">
                                  <label for="resource_code" class="bmd-label-floating input_label product_small_label">Resource Code</label>
                                  <input type="text" class="form-control form_input_field product_small_text" id="resource_code" name="" disabled="">
                                </div>
                              </div>
                              <input type="hidden" name="" id="unit_price">
                              <input type="hidden" name="" id="inventory_id"> 
                            </div>

                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group bmd-form-group save_btn_wrapper" style="text-align: right">
                                   <button type="button" class="btn add_inventory btn-sm btn-warning btn-round">Add Resources<div class="ripple-container"></div></button>
                                 </div>
                              </div>
                            </div>



                            <!-- Showing The corresponds -->
                            
                            <div class="row" id="product_viewer_row">
                                <div class="col-md-12 mx-auto">
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="table-responsive table-sales">
                                        <table class="table product_viewer_table">
                                          <thead>
                                              <tr class="table-success">
                                                <th>Resource Name</th>
                                                <th>Unit Price</th>
                                                <th class="disabled-sorting text-right">Actions</th>
                                              </tr>
                                          </thead>
                                          <tbody class="table_body">
                                            @foreach($product_inventory as $rowdata)
                                              <tr>
                                                <input type="hidden" name="inventory_id[]" id="inventory_id" class="inventory_id_check" value="{{$rowdata->inventory->id}}">
                                                <td>{{$rowdata->inventory->resource_name}}</td>
                                                <td>$ {{$rowdata->inventory->price_per_hour_service}}</td>
                                                <td class="text-right"> <a href="#" id="product_delete_btn_" name="" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">close</i></a></td>
                                              </tr>
                                            @endforeach
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                              <!-- end of corresponds -->
                          </div>
                        </div>


                        <!-- Variable Section -->
                        <div class="card">
                          <div class="card-header card-header-rose card-header-text custom_content_title_wrapper sub_card inv_sub_card_img_zindex">
                            <div class="card-text">
                              <h4 class="card-title">Variable Section</h4>
                            </div>
                          </div>
                          <div class="card-body variable_card_body variable_selection_card_body">
                          
                          <span id="var_tbl"></span>

                            <div class="row top_variable_row input_row" id="var_row_create">
                              <div class="col-md-6 input_wrapper custom_select2_col">
                                <div class="form-group custom-form-select">
                                  <select class="custom-select varSelection select2 var_blank_it" id="variable_selection_dw">
                                    <option class="form-select-placeholder"></option>
                                    @foreach($variables as $rowdata)
                                    <option value="{{$rowdata->id}}">{{$rowdata->variable_name}}</option>
                                    @endforeach
                                  </select>
                                  <div class="form-element-bar">
                                  </div>
                                  <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Select Variable</label>
                                </div>
                              </div>
                              <div class="col-md-6 text-right">
                                <button class="btn btn-info btn-round btn-sm variable_adder_btn" type="button">
                                  <span class="btn-label">
                                    <i class="material-icons">add</i>
                                  </span>
                                  Add
                                  <div class="ripple-container"></div>
                                </button>                                              
                              </div>
                            </div>
                            <div class="card product_quotaion_table_card variable_selection_table_card">
                              <div class="card-body table_card_wrapper">
                                <div class="card variable_table_header">
                                  <div class="quotation_table_section">
                                    <div class="quotation_table_row">
                                      <div class="var_col">
                                        <p>Variable Name</p>
                                      </div>
                                      <div class="quote_col single_resource_main">
                                        <div class="all_single_resounce">
                                          <div class="single_resource_col">
                                            <p>Parameters</p>
                                          </div>
                                          <div class="single_resource_col">
                                           <p>Data Type</p>
                                          </div>
                                          <div class="single_resource_col">
                                           <p>Status</p>
                                          </div>
                                          <div class="single_resource_col">
                                           <p>Value</p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>                                                
                                </div>

                            <div class="var_section">
                              @foreach($variable_section as $rowdata)
                              <?php
                                $variable_row = App\Models\Variable::where('id', $rowdata->var_id)->first();
                              ?>
                                <div class="card single_card_for_single_service variable_single_card">
                                  <div class="quotation_table_main_section variable_table_main_section">
                                    <div class="quotation_table_section">
                                      <div class="quotation_table_row variable_table_row">
                                        <div class="quote_col">
                                          <input type="hidden" name="var_section_key[]" value="{{$rowdata->id}}">
                                          <input type="hidden" name="var_id[]" value="{{$rowdata->var_id}}">
                                          <p>{{$variable_row->variable_name}}</p>
                                        </div>
                                        <div class="quote_col single_resource_main">
                                        <?php $var_params = App\Models\VariableParameter::where('var_id', $rowdata->var_id)->get(); ?>
                                        @foreach($var_params as $rowdata)
                                          <div class="all_single_resounce">
                                            <div class="single_resource_col price_select_wrapper">
                                              <p>{{$rowdata->var_param_name}}</p>
                                            </div>
                                            <div class="single_resource_col">
                                              <p> {{$rowdata->var_param_data_type}} </p>
                                            </div>
                                            <div class="single_resource_col">
                                              <p> @if($rowdata->var_param_value_type == 0) PreDefined @else UserDefined @endif</p>
                                            </div>
                                            <div class="single_resource_col colTotal">
                                              {{$rowdata->var_param_value}}
                                            </div>
                                          </div>
                                        @endforeach
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="cross">
                                    <a href="#" class="btn btn-danger btn-round">
                                      <i class="material-icons">close</i>
                                    </a>
                                  </div>
                                </div>
                              @endforeach
                            </div>

                              </div>                                            
                            </div>
                          </div>
                        </div>


                        <div class="form-group bmd-form-group save_btn_wrapper text-right">
                          <button type="submit" class="btn custom-btn-one save_btn">Update<div class="ripple-container"></div></button>
                        </div>
                      </form>
                    </div>


                  </div>
                </div>
                     
              </div>
  
            </div>
        </div>
<!--         </div>
      </div> -->


<script>
  $(document).ready(function() {
    
    $( ".custom-select" ).change(function() {
      if(this.value.length > 0){
          $( this ).addClass( "hasvalue" );
      }
    })


    $('.cat_product').change(function(){
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getSubcategory') }}",
            method:"POST",
            data:{value:value, _token:_token, 'type':'product'},
            success:function(result)
            {
             $('.subcat_product').html(result);
            }
         })
      }
     });

    $('.cat').change(function(){
      if($('#resource_name').val() != '' || $('#resource_code').val() != ''){
        $('#resource_name').val('');
        $('#resource_name').parent().removeClass('is-filled');
        $('#resource_code').val('');
        $('#resource_code').parent().removeClass('is-filled');
      }

      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getSubcategory') }}",
            method:"POST",
            data:{value:value, _token:_token, 'type':'inventory'},
            success:function(result)
            {
             $('.subcat').html(result);
            }
         })
      }
     });



        $('.subcat').change(function(){
      //alert($(this).val());
      if($(this).val() != ''){
        let value = $(this).val();
        let _token = $('input[name="_token"]').val();

         $.ajax({
            url:"{{ route('getResourceInfoBySubcat') }}",
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result)
            {
              var obj = JSON.parse(result)
              //alert(obj[0].resource_name);
              //alert(obj[0].resource_code);
             if(obj[0].resource_name != null){
              $('#resource_name').parent().addClass('is-filled'); 
              //$('#resource_name').attr('readonly', true);
             }
             else{
              $('#resource_name').parent().removeClass('is-filled'); 
              //$('#resource_name').attr('readonly', false);
             }
             if(obj[0].resource_code != null){
              $('#resource_code').parent().addClass('is-filled'); 
             //$('#resource_code').attr('readonly', true);
             }
             else{
              $('#resource_code').parent().removeClass('is-filled'); 
              //$('#resource_code').attr('readonly', false);
             }
             
             $('#resource_name').val(obj[0].resource_name);
             $('#resource_code').val(obj[0].resource_code);
             $('#unit_price').val(obj[0].price_per_hour_service);
             $('#inventory_id').val(obj[0].inventory_id);
            }
         })
      }
     });


      $("form").submit(function () {

          var this_master = $(this);

          this_master.find('input[type="checkbox"]').each( function () {
              var checkbox_this = $(this);


              if( checkbox_this.is(":checked") == true ) {
                  checkbox_this.attr('value','1');
              } else {
                  checkbox_this.prop('checked',true);
                  //DONT' ITS JUST CHECK THE CHECKBOX TO SUBMIT FORM DATA    
                  checkbox_this.attr('value','0');
              }
          })
      })


  });



let value = $('input[name="product_key"]').val();
let type = 'service';
let _token = $('input[name="_token"]').val();
var preloaded = [];
$.ajax({
            async: false,
            url:"{{ route('getImage') }}",
            method:"POST",
            data:{value:value, type:type, _token:_token},
            success:function(result)
            {
              var obj = JSON.parse(result)
              $.each(obj, function(index) {
                var data = { id: obj[index].id, src: obj[index].file };
                preloaded.push(data);
              });

              preloadedImage(preloaded);
            }
         })

function preloadedImage(param) {

    $('.input-images').imageUploader({
      preloaded: param,
      imagesInputName:'images',
      preloadedInputName:'preloaded',
      label:'Drag & Drop files here or click to browse'
  }); 

}

function validateMyForm()
{
  if($('#resource_name').val() !== '')
  { 
    alert("Please click add resources button first to add your resources");
    return false;
  }

  return true;
}


$(document).on('click','.variable_adder_btn', function(){
  let var_id = $('.varSelection').val();
  let _token = $('input[name="_token"]').val();

  $.ajax({
            url:"{{ route('addVariableSection') }}",
            method:"POST",
            data:{var_id:var_id, _token:_token},
            success:function(result)
            { 
              $('.var_section').append(result);
              $('.variable_selection_table_card').show();
            }
         })
  $('.custom_select2_col select.var_blank_it').val(null).trigger('change');
  $('#variable_selection_dw').removeClass('hasvalue')
});

</script>

@endsection
