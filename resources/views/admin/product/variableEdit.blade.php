@extends('layouts.master')

@section('content')

        <div class="content">
          <div class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12 mx-auto card_holder_box">
                  
                  
                  <div class="card">
                  <div class="card-header card-header-tabs card-header-rose">
                    <div class="nav-tabs-navigation">
                      <div class="nav-tabs-wrapper">
                        
                        <ul class="nav nav-tabs" data-tabs="tabs">
                          <li class="nav-item">
                            <a class="nav-link active" href="#profile" data-toggle="tab">
                              <i class="material-icons">ballot</i> Edit Variable
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                          
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div style="margin-top: 15px;">
                      @include('partials.messages')
                  </div>

                  <div class="card-body">
                     
                        <div class="card edit_category_inner_card">
                          <div class="card-header card-header-rose card-header-text">
                            
                          </div>

                          <div class="card-body col-md-12 mx-auto increased_margin_t_cardbody variable_card_body_main_section">
                            <form method="post" action="{{ route('products.update', $variable->id) }}" class="form-horizontal">
                            @csrf
                            @method('PUT')

                            <input type="hidden" name="tab" value="variable">
                            <input type="hidden" name="p_key" value="{{$variable->id}}">

                                <div class="row variable_first_row">
                                          <div class="col-md-5 input_wrapper">
                                            <div class="form-group bmd-form-group">
                                              <label for="variable_name" class="bmd-label-floating input_label">Name of Variable *</label>
                                              <input type="text" class="form-control form_input_field" id="variable_name" name="variable_name" required="true" aria-required="true" value="{{$variable->variable_name}}">
                                            </div>
                                          </div>
                                        </div>

                                      @foreach($var_params as $rowdata)
                                        <div class="row var_param_row">
                                        <input type="hidden" name="var_param_key[]" value="{{$rowdata->id}}">
                                          <div class="col-md-3 input_wrapper first">
                                            <div class="form-group bmd-form-group">
                                              <label for="parameter_name" class="bmd-label-floating input_label">Name of parameter *</label>
                                              <input type="text" class="form-control form_input_field" id="parameter_name" name="var_param_name[]" required="true" aria-required="true" value="{{$rowdata->var_param_name}}">
                                            </div>
                                          </div>

                                          <div class="col-md-2 custom_select2_col second input_wrapper">
                                            <div class="form-group custom-form-select">
                                              <select class="custom-select hasvalue select2 form_input_field var_data_type" id="var_data_type" name="var_param_data_type[]" required="true" aria-required="true">
                                                <option class="form-select-placeholder"></option>
                                                <option value="decimal" @if($rowdata->var_param_data_type == 'decimal') selected @endif>Decimal</option>
                                                <option value="integer" @if($rowdata->var_param_data_type == 'integer') selected @endif>Integer</option>
                                                <option value="datetime" @if($rowdata->var_param_data_type == 'datetime') selected @endif>DateTime</option>
                                                <option value="string" @if($rowdata->var_param_data_type == 'string') selected @endif>String</option>
                                                <option value="percentage" @if($rowdata->var_param_data_type == 'percentage') selected @endif>Percentage</option>
                                              </select>
                                              <div class="form-element-bar">
                                              </div>
                                              <label class="form-element-label" for="variable_data_type input_label">Data Type</label>
                                            </div>
                                          </div>

                                          <div class="col-md-3 d-flex align-items-center var_param_value_wrapper">
                                            <div class="togglebutton admin-role-toggle priceclass_toggle_section">
                                                <label>
                                                  <input type="hidden" class="var_param_value_type" name="var_param_value_type[]" value="{{$rowdata->var_param_value_type}}">
                                                    <input class="var_param_toggle price_class_toggle" type="checkbox" name="" @if($rowdata->var_param_value_type == 0) checked @endif>
                                                    Predefined <span class="toggle"></span> UserDefined
                                                </label>
                                            </div>
                                          </div>

                                          <div class="col-md-3 input_wrapper four">
                                            <div class="form-group bmd-form-group">
                                              <label for="parameter_name" class="bmd-label-floating input_label">Value </label>
                                              <input type="text" class="form-control form_input_field" id="var_param_value" name="var_param_value[]" value="{{$rowdata->var_param_value}}" @if($rowdata->var_param_value_type == 1) readOnly @endif>
                                            </div>
                                          </div>

                                          <div class="col-md-1 var_cross_btn">
                                              <a href="#" class="btn btn-link btn-danger btn-just-icon remove var_param_dlt_btn"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                                          </div>

                                        </div>
                                      @endforeach

                                        <div class="row add_variable_btn_wrapper new_var_wrapper">
                                            <div class="col-md-12 text-right">
                                              <button class="btn btn-info btn-round btn-sm add_var_btn" type="button">
                                                <span class="btn-label">
                                                  <i class="material-icons">add</i>
                                                </span>
                                                Add Parameter
                                                <div class="ripple-container"></div>
                                              </button>
                                            </div>
                                        </div>
                              <div class="form-group bmd-form-group save_btn_wrapper text-right">
                                <button type="submit" class="btn save_btn custom-btn-one">Update</button>
                              </div>
                          </form>
                          </div>
                      </div>


                     
                  </div>
                </div>
  
  
  
                </div>
              </div>
          </div>
        </div>
      </div>

<script>
$('.var_param_toggle').change(function() {
    if(this.checked){
      $(this).parent().find('.var_param_value_type').val(0);
      $(this).parents('.var_param_row').find('#var_param_value').attr('readonly', false).val('');
    }
    else{
      $(this).parent().find('.var_param_value_type').val(1);
      $(this).parents('.var_param_row').find('#var_param_value').attr('readonly', true).val('');
    }

});

$('.var_data_type').change(function() {
    if($(this).val() == 'decimal'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'number').prop('step', '0.01');
    }
    else if($(this).val() == 'integer'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'number').prop('step', false);
    }
    else if($(this).val() == 'datetime'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'text').prop('step', false);
    }
    else if($(this).val() == 'string'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'text').prop('step', false);
    }
    else if($(this).val() == 'percentage'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'text').prop('step', false);
    }
});
</script>
@endsection