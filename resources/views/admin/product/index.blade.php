@extends('layouts.master')

@section('content')

	<div class="content manage-product-content">    
        <div class="container-fluid">
            <div class="row">
              <div class="col-md-12 mx-auto">
                	<div class="card manage_inv_ser_root_card">
	                  <div class="card-header card-header-tabs card-header-rose custom_card_header mang_inventory_card_header inv_mang_card_tab_header">
	                    <div class="nav-tabs-navigation">
	                      <div class="nav-tabs-wrapper">
	                        <p class="nav-tabs-title">Manage Service</p>
	                        <ul class="nav nav-tabs" data-tabs="tabs" id="tabMenu">
	                          <li class="nav-item">
	                            <a class="nav-link" href="#product_category_management" data-toggle="tab">
	                              <i class="material-icons">bug_report</i> Category Management
	                              <div class="ripple-container"></div>
	                            </a>
	                          </li>
	                          <li class="nav-item">
	                            <a class="nav-link" href="#product_sub_category_management" data-toggle="tab">
	                              <i class="material-icons">code</i> Sub Category Management
	                              <div class="ripple-container"></div>
	                            </a>
	                          </li>
	                          <li class="nav-item">
	                            <a class="nav-link" href="#variable_management" data-toggle="tab">
	                              <i class="material-icons">code</i> Variable Management
	                              <div class="ripple-container"></div>
	                            </a>
	                          </li>
	                          <li class="nav-item">
	                            <a class="nav-link" href="#product_management" data-toggle="tab">
	                              <i class="material-icons">cloud</i> Added Services
	                              <div class="ripple-container"></div>
	                            </a>
	                          </li>
	                        </ul>
	                      </div>
	                    </div>
	                  </div>

	                  <div class="alert alert-success alert-dismissible var_msg_wrapper" role="alert" style="display: none;">
                              <!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
                              <strong class="var_msg"></strong>
                     </div>

	                  <div class="card-body manage-product-content-card-body">
	                    <div class="tab-content">
	                      <div class="tab-pane inventory_sub_card" id="product_category_management">
	                        <div class="card">
	                          <div class="card-header card-header-rose card-header-icon icon_box_wrapper">
	                            <div class="card-icon">
	                              <i class="material-icons">assignment</i>
	                            </div>
	                            <h4 class="card-title">Category</h4>
	                          </div>

	                          <div style="margin-top: 15px;">
	                            @include('partials.messages')
	                          </div>

	                          <div class="card-body">
	                            <div class="toolbar">
	                              <!--        Here you can write extra buttons/actions for the toolbar              -->
	                            </div>
	                            <div class="material-datatables product-cat-datatable" style="overflow-x: hidden;">
	                              <table id="product_category_table" class="table table-striped table-no-bordered table-hover all_table" cellspacing="0" width="100%" style="width:100%;">
	                                <thead>
	                                  <tr>
	                                    <th>Category Name</th>
	                                    <th class="disabled-sorting text-right">Actions</th>
	                                  </tr>
	                                </thead>
	                                <tbody>

	                                	@foreach($cat as $rowdata)
		                                <tr>
		                                    <td>{{$rowdata->category_name}}</td>
	                                		<td class="text-right">

	                                		<form action="{{ route('products.destroy',[$rowdata->id, 'type' => 'cat']) }}" method="POST">

		                                		<a href="{{ route('products.edit', [$rowdata->id, 'type' => 'cat']) }}" class="btn btn-link btn-success btn-just-icon edit">
												  <i class="material-icons">edit</i>
												  <div class="ripple-container"></div>
												</a>

												@csrf
                                      			@method('DELETE')
												<button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm('Are you sure you want to delete?');">
		  											<i class="material-icons">close</i>
		  										</button>
		  									</form>
	                                		</td>
	                                	</tr>
	                                	@endforeach
	                                </tbody>
	                              </table>
	                            </div>
	                          </div>
	                        </div>
	                      </div>
	                      <div class="tab-pane inventory_sub_card" id="product_sub_category_management">
	                        <div class="card">
	                          <div class="card-header card-header-rose card-header-icon">
	                            <div class="card-icon">
	                              <i class="material-icons">assignment</i>
	                            </div>
	                            <h4 class="card-title">Sub Category</h4>
	                          </div>

	                          <div style="margin-top: 15px;">
	                            @include('partials.messages')
	                          </div>

	                          <div class="card-body">
	                            <div class="toolbar">
	                            </div>
	                            <div class="material-datatables product-subcat-datatable" style="overflow-x: hidden;">
	                              <table id="product_sub_cat_table" class="table table-striped table-no-bordered table-hover all_table" cellspacing="0" width="100%" style="width:100%;">
	                                <thead>
	                                  <tr>
	                                    <th>Category Name</th>
	                                    <th>Sub Category Name</th>
	                                    <th class="disabled-sorting text-right">Actions</th>
	                                  </tr>
	                                </thead>
	                                <tbody>

	                                @foreach($subcat as $rowdata)
		                                <tr>
		                                    <td>@if($rowdata->category){{$rowdata->category->category_name}}@endif</td>
		                                    <td>{{$rowdata->subcategory_name}}</td>
		                                	<td class="text-right">

		                                	<form action="{{ route('products.destroy',[$rowdata->id, 'type' => 'subcat']) }}" method="POST">

		                                		<a href="{{ route('products.edit', [$rowdata->id, 'type' => 'subcat']) }}" class="btn btn-link btn-success btn-just-icon edit">
												  <i class="material-icons">edit</i>
												  <div class="ripple-container"></div>
												</a>

											@csrf
                                      		@method('DELETE')
												<button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm('Are you sure you want to delete?');">
		  											<i class="material-icons">close</i>
		  										</button>
		  									</form>
		                                	</td>
	                                	</tr>
	                                @endforeach
	                                
	                                </tbody>
	                              </table>
	                            </div>
	                          </div>
	                        </div>
	                      </div>

	                      <div class="tab-pane inventory_sub_card" id="variable_management">
	                        <div class="card">
	                          <div class="card-header card-header-rose card-header-icon">
	                            <div class="card-icon">
	                              <i class="material-icons">assignment</i>
	                            </div>
	                            <h4 class="card-title">Variables</h4>
	                          </div>

	                          <div style="margin-top: 15px;">
	                            @include('partials.messages')
	                          </div>

	                          <div class="card-body">
	                            <div class="toolbar">
	                            </div>
	                            <div class="material-datatables product-subcat-datatable variable_table_management" style="overflow-x: hidden;">
	                              <table id="service_variable_mgt_table" class="table table-striped table-no-bordered table-hover all_table variable_table" cellspacing="0" width="100%" style="width:100%;">
	                                <thead>
	                                  <tr>
	                                    <th>Variable Name</th>
	                                    <th>Status</th>
	                                    <th class="disabled-sorting text-right">Actions</th>
	                                  </tr>
	                                </thead>
	                                <tbody>

	                                @foreach($variables as $rowdata)
		                                <tr>
		                                    <td>{{$rowdata->variable_name}}</td>
		                                    <td>
		                                      <span class="togglebutton">
		                                        <label>
		                                            <input type="checkbox" name="" class="enable_disable" var_key="{{$rowdata->id}}" value="{{$rowdata->enable_disable}}" @if($rowdata->enable_disable == 1) checked @endif>
                                            				Disable <span class="toggle"></span> Enable
		                                        </label>
		                                      </span>
		                                    </td>
		                                	<td class="text-right">

		                                	<form action="{{ route('products.destroy',[$rowdata->id, 'type' => 'variable']) }}" method="POST">

		                                		<a href="{{ route('products.edit', [$rowdata->id, 'type' => 'variable']) }}" class="btn btn-link btn-success btn-just-icon edit">
												  <i class="material-icons">edit</i>
												  <div class="ripple-container"></div>
												</a>

											@csrf
                                      		@method('DELETE')
												<button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm('Are you sure you want to delete?');">
		  											<i class="material-icons">close</i>
		  										</button>
		  									</form>
		                                	</td>
	                                	</tr>
	                                @endforeach
	                                
	                                </tbody>
	                              </table>
	                            </div>
	                          </div>
	                        </div>
	                      </div>

	                      <div class="tab-pane inventory_sub_card" id="product_management">
	                        <div class="card">
	                          <div class="card-header card-header-rose card-header-icon">
	                            <div class="card-icon">
	                              <i class="material-icons">assignment</i>
	                            </div>
	                            <h4 class="card-title">Services</h4>
	                          </div>

	                          <div style="margin-top: 15px;">
	                            @include('partials.messages')
	                          </div>

	                          <div class="card-body">
	                            <div class="toolbar">
	                              <!--        Here you can write extra buttons/actions for the toolbar              -->
	                            </div>
	                            <div class="material-datatables product-man-datatable" style="overflow-x: hidden;">
	                              <table id="manage_product_table" class="table table-striped table-no-bordered table-hover all_table" cellspacing="0" width="100%" style="width:100%;">
	                                <thead>
	                                  <tr>
	                                    <th>Category</th>
	                                    <th>Subcategory</th>
	                                    <th>Product Name</th>
	                                    <th>SKU</th>                                   
	                                    <th class="disabled-sorting text-right">Actions</th>
	                                  </tr>
	                                </thead>
	                                <tbody>

	                                @foreach($products as $rowdata)
	                                	<tr>
	                                		<td>@if($rowdata->category){{$rowdata->category->category_name}}@endif</td>
	                                		<td>@if($rowdata->subcategory){{$rowdata->subcategory->subcategory_name}}@endif</td>
	                                		<td>{{$rowdata->product_name}}</td>
	                                		<td>{{$rowdata->sku}}</td>

		                                	<td class="text-right">

		                                	<form action="{{ route('products.destroy',[$rowdata->id, 'type' => 'product']) }}" method="POST">

		                                		<a href="{{ route('products.edit', [$rowdata->id, 'type' => 'product']) }}" class="btn btn-link btn-success btn-just-icon edit">
												  <i class="material-icons">edit</i>
												  <div class="ripple-container"></div>
												</a>

											@csrf
                                      		@method('DELETE')
												<button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm('Are you sure you want to delete?');">
		  											<i class="material-icons">close</i>
		  										</button>
		                                	</td>
	                                	</tr>
	                                @endforeach
	                                </tbody>
	                              </table>
	                            </div>
	                          </div>
	                        </div>
	                      </div>
	                    </div>
	                  </div>
                	</div>                              
              </div>
            </div>
        </div>
    </div>


<script>
	$(window).on('load', function(){
    setTimeout(function() {
       $('.material-datatables').css('overflow-x','unset');
       $('.product-cat-datatable table thead th:last-child').css('width','100px');
       $('.product-subcat-datatable table thead th:last-child').css('width','100px');
       $('.product-man-datatable table thead th:last-child').css('width','100px'); 
    }, 100);
  });

$('.enable_disable').change(function() {
    let value = $(this).prop("checked");
    let var_key = $(this).attr('var_key');
    let _token = $('input[name="_token"]').val();

    $.ajax({
            url:"{{ route('variableEnableDisable') }}",
            method:"POST",
            data:{var_key:var_key, _token:_token, value:value},
            success:function(result)
            { 
                if(result == 0){
                    $('.var_msg').html('Success: Variable has been disabled');
                    $('.var_msg_wrapper').show();
                }
                else{
                    $('.var_msg').html('Success: Variable has been enabled');
                    $('.var_msg_wrapper').show();
                }
            }
    })

});

$(document).ready(function () {

if($('#tabMenu a[href="#{{ old('tab') }}"]').attr('href') == undefined){
    $('#tabMenu a[href="#product_management"]').tab('show')  
}
else{
    $('#tabMenu a[href="#{{ old('tab') }}"]').tab('show')
}

});

</script>

@endsection
