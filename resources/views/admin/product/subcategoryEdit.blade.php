@extends('layouts.master')

@section('content')

        <div class="content">
          <div class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12 mx-auto">
                  
                  
                  <div class="card">
                  <div class="card-header card-header-tabs card-header-rose">
                    <div class="nav-tabs-navigation">
                      <div class="nav-tabs-wrapper">
                        
                        <ul class="nav nav-tabs" data-tabs="tabs">
                          <li class="nav-item">
                            <a class="nav-link active" href="#profile" data-toggle="tab">
                              <i class="material-icons">ballot</i> Edit Sub Category
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                          
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div style="margin-top: 15px;">
                      @include('partials.messages')
                  </div>

                  <div class="card-body">
                     
                        <div class="card edit_category_inner_card">
                          <div class="card-header card-header-rose card-header-text">
                            
                          </div>

                          <div class="card-body col-md-12 mx-auto increased_margin_t_cardbody">
                            <form method="post" action="{{ route('products.update', $subcat->id) }}" class="form-horizontal">
                            @csrf
                            @method('PUT')

                            <input type="hidden" name="tab" value="subcategory">
                            <input type="hidden" name="p_key" value="{{$subcat->id}}">

                                <div class="row sub_holder">
                                  <div class="col-md-6 custom_select2_col">
                                    <div class="form-group custom-form-select">
                                      <select class="custom-select hasvalue select2" id="category-custom-select" name="cat_id">
                                        <option class="form-select-placeholder"></option>
                                        @foreach($cat as $rowdata)
                                          <option value="{{$rowdata->id}}" @if($rowdata->id == $subcat->cat_id) selected @endif>{{$rowdata->category_name}}</option>
                                        @endforeach
                                      </select>
                                      <div class="form-element-bar">
                                      </div>
                                      <label class="form-element-label input_label top_select_input_label" for="category-custom-select">Category</label>
                                    </div>
                                  </div>
                                  <div class="col-md-6 input_wrapper">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label for="subcategory_name" class="bmd-label-floating input_label">Sub Category Name *</label>
                                      <input type="text" class="form-control form_input_field" id="category_name" name="subcategory_name" required="true" aria-required="true" value="{{$subcat->subcategory_name}}">
                                    </div>
                                  </div>
                                </div>
                              <div class="form-group bmd-form-group save_btn_wrapper text-right">
                                <button type="submit" class="btn custom-btn-one save_btn">Update</button>
                              </div>
                          </form>
                          </div>
                      </div>


                     
                  </div>
                </div>
  
  
  
                </div>
              </div>
          </div>
        </div>
      </div>

@endsection