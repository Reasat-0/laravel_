@if (Session::has('success'))
	
	<div class="alert alert-success alert-dismissible" role="alert">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Success:</strong> {{ Session::get('success') }}
	</div>

@endif


@if (Session::has('error'))
	
	<div class="alert alert-danger alert-dismissible" role="alert">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Error:</strong> {{ Session::get('error') }}
	</div>

@endif


@if (count($errors) > 0)

	<div class="alert alert-danger alert-dismissible" role="alert">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Errors:</strong>
		<ul>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach  
		</ul>
	</div>

@endif

@if (Session::has('message'))
	
	<div class="alert alert-success alert-dismissible" role="alert">
		<!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
		{{ Session::get('message') }}
	</div>

@endif