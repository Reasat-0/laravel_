<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToInsuranceVehicleRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insurance_vehicle_request', function (Blueprint $table) {
            $table->string('vehicle_garraging_address_1')->nullable()->after('special_equipment');
            $table->string('vehicle_garraging_address_2')->nullable()->after('vehicle_garraging_address_1');
            $table->string('vehicle_garraging_city')->nullable()->after('vehicle_garraging_address_2');
            $table->string('vehicle_garraging_state')->nullable()->after('vehicle_garraging_city');
            $table->string('vehicle_garraging_zip')->nullable()->after('vehicle_garraging_state');
            
            $table->string('vehicle_lessor_address_1')->nullable()->after('finance_company_name');
            $table->string('vehicle_lessor_address_2')->nullable()->after('vehicle_lessor_address_1');
            $table->string('vehicle_lessor_city')->nullable()->after('vehicle_lessor_address_2');
            $table->string('vehicle_lessor_state')->nullable()->after('vehicle_lessor_city');
            $table->string('vehicle_lessor_zip')->nullable()->after('vehicle_lessor_state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insurance_vehicle_request', function (Blueprint $table) {
            //
        });
    }
}
