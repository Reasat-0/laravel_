<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceLocationAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_location_address', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->integer('parent_id');
            $table->string('shipAddr_line1')->nullable();
            $table->string('shipAddr_line2')->nullable();
            $table->string('shipAddr_city')->nullable();
            $table->string('shipAddr_country')->nullable();
            $table->string('shipAddr_countrySubDivisionCode')->nullable();
            $table->string('shipAddr_postalCode')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_location_address');
    }
}
