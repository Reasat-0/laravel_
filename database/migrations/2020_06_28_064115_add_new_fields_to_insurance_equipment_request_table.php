<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToInsuranceEquipmentRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insurance_equipment_request', function (Blueprint $table) {
            $table->string('equip_garraging_address_1')->nullable()->after('description_equipment_accessories');
            $table->string('equip_garraging_address_2')->nullable()->after('equip_garraging_address_1');
            $table->string('equip_garraging_city')->nullable()->after('equip_garraging_address_2');
            $table->string('equip_garraging_state')->nullable()->after('equip_garraging_city');
            $table->string('equip_garraging_zip')->nullable()->after('equip_garraging_state');
            
            $table->string('equip_lessor_address_1')->nullable()->after('finance_company_name');
            $table->string('equip_lessor_address_2')->nullable()->after('equip_lessor_address_1');
            $table->string('equip_lessor_city')->nullable()->after('equip_lessor_address_2');
            $table->string('equip_lessor_state')->nullable()->after('equip_lessor_city');
            $table->string('equip_lessor_zip')->nullable()->after('equip_lessor_state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insurance_equipment_request', function (Blueprint $table) {
            //
        });
    }
}
