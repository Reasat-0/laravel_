<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('companyName')->nullable();
            $table->string('displayName')->nullable();            
            $table->string('title')->nullable();
            $table->string('givenName')->nullable();
            $table->string('middleName')->nullable();
            $table->string('familyName')->nullable();
            $table->string('suffix')->nullable();
            
            $table->string('primaryPhone_freeFormNumber')->nullable();
            $table->string('primaryEmailAddr_addess')->nullable();
            $table->string('mobile')->nullable();
            $table->string('fax')->nullable();
            $table->string('alternatePhone')->nullable();

            $table->string('billAddr_line1')->nullable();
            $table->string('billAddr_line2')->nullable();
            $table->string('billAddr_city')->nullable();
            $table->string('billAddr_country')->nullable();
            $table->string('billAddr_countrySubDivisionCode')->nullable();
            $table->string('billAddr_postalCode')->nullable();

            $table->string('taxIdentifier')->nullable();
            $table->string('accountNum')->nullable();
            $table->string('webAddr')->nullable();
            $table->string('notes')->nullable();

            $table->string('reg_number')->nullable();
            $table->string('license_number')->nullable();

            $table->integer('quickbooks_id')->nullable();
            $table->integer('syncToken')->nullable();
            $table->tinyInteger('quickbooks')->default(0);
            $table->string('manual')->nullable();
            $table->tinyInteger('temp')->default(0);
            $table->tinyInteger('updated')->default(0);

            $table->softDeletes();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
