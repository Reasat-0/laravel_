<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return redirect()->route('home');
});*/

Auth::routes();

// Route::get('/test',function(){
// 	return App\User::find(1)->profile;
// });
// Route::get('/', 'HomeController@index')->name('home');
// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/oil_field', 'HomeController@home')->name('home');


Route::get('/', 'HomeController@home')->name('home');
Route::get('/home', 'HomeController@home')->name('home');
Route::get('/oil_field', 'HomeController@home')->name('home');


Route::resource('/users', 'admin\UserController');
Route::get('/myProfile', 'admin\UserController@myProfile')->name('myProfile');
Route::post('/myProfile', 'admin\UserController@myProfileUpdate')->name('myProfileUpdate');

Route::resource('/settings', 'admin\SettingsController');

Route::resource('/inventory', 'admin\InventoryController');
Route::post('/getSubcategory', 'admin\InventoryController@getSubcategory')->name('getSubcategory');
Route::post('/getInventoryInfo', 'admin\InventoryController@getInventoryInfo')->name('getInventoryInfo');
Route::post('/getResourceInfoBySubcat', 'admin\InventoryController@getResourceInfoBySubcat')->name('getResourceInfoBySubcat');
Route::post('/getVariablesBySubcat', 'admin\InventoryController@getVariablesBySubcat')->name('getVariablesBySubcat');
Route::post('/getImagesBySubcat', 'admin\InventoryController@getImagesBySubcat')->name('getImagesBySubcat');
Route::post('/getUnitDetailsBySubcat', 'admin\InventoryController@getUnitDetailsBySubcat')->name('getUnitDetailsBySubcat');
Route::post('/unitProperties', 'admin\InventoryController@unitProperties')->name('unitProperties');
Route::post('/unitPropertiesDlt', 'admin\InventoryController@unitPropertiesDlt')->name('unitPropertiesDlt');


Route::resource('/customers', 'admin\CustomerController');
Route::get('/qbCallback', 'admin\CustomerController@qbCallback');
Route::get('/qbCustomer', 'admin\CustomerController@qbCustomer')->name('qbCustomer');
Route::get('/qbRefreshToken', 'admin\CustomerController@qbRefreshToken')->name('qbRefreshToken');
Route::post('/importQbData', 'admin\CustomerController@importQbData')->name('importQbData');
Route::get('/synchronizeCustomer', 'admin\CustomerController@synchronizeCustomer')->name('synchronizeCustomer');
Route::get('/synchronizeQb', 'admin\CustomerController@synchronizeQb')->name('synchronizeQb');
Route::get('/insertQbCustomer', 'admin\CustomerController@insertQbCustomer')->name('insertQbCustomer');
Route::get('/discardQbCustomer', 'admin\CustomerController@discardQbCustomer')->name('discardQbCustomer');
Route::get('/singleCustomerQB', 'admin\CustomerController@singleCustomerQB')->name('singleCustomerQB');

Route::get('/creditApp/{id}/edit', 'admin\CustomerController@creditAppEdit')->name('creditAppEdit');
Route::get('/creditApp/{id}', 'admin\CustomerController@creditAppShow')->name('creditAppShow');
Route::post('/creditAppUpdate', 'admin\CustomerController@creditAppUpdate')->name('creditAppUpdate');
Route::get('/creditAppDelete/{id}', 'admin\CustomerController@creditAppDelete')->name('creditAppDelete');
Route::get('creditApp/accessCode/{token}', 'admin\CustomerController@creditAppToken');
Route::post('creditApp', 'admin\CustomerController@creditAppCustomer')->name('creditAppCustomer');
Route::post('creditAppUpdateForCustomer', 'admin\CustomerController@creditAppUpdateForCustomer')->name('creditAppUpdateForCustomer');
Route::get('creditAppMessage', 'admin\CustomerController@creditAppMessage')->name('creditAppMessage');
Route::get('creditAppPdf/{id}', 'admin\CustomerController@creditAppPdf')->name('creditAppPdf');



Route::resource('/products', 'admin\ProductController');

Route::resource('/vendors', 'admin\VendorController');
Route::get('/getQbVendor', 'admin\VendorController@getQbVendor')->name('getQbVendor');
Route::get('/synchronizeEvosVendor', 'admin\VendorController@synchronizeEvosVendor')->name('synchronizeEvosVendor');
Route::get('/synchronizeQbVendor', 'admin\VendorController@synchronizeQbVendor')->name('synchronizeQbVendor');
Route::get('/insertQbVendor', 'admin\VendorController@insertQbVendor')->name('insertQbVendor');
Route::get('/discardQbVendor', 'admin\VendorController@discardQbVendor')->name('discardQbVendor');


Route::post('/getImage', 'admin\VendorController@getImage')->name('getImage');

Route::resource('/quotation', 'admin\QuotationController');

Route::post('/getService', 'admin\QuotationController@getService')->name('getService');

Route::post('/getServiceResourceInfo', 'admin\QuotationController@getServiceResourceInfo')->name('getServiceResourceInfo');
Route::post('/getRentalResourceInfo', 'admin\QuotationController@getRentalResourceInfo')->name('getRentalResourceInfo');
Route::post('/getAddress', 'admin\QuotationController@getAddress')->name('getAddress');

Route::post('/addVariableSection', 'admin\InventoryController@addVariableSection')->name('addVariableSection');

Route::post('/variableEnableDisable', 'admin\InventoryController@variableEnableDisable')->name('variableEnableDisable');


Route::post('/getCustomerDetails', 'admin\QuotationController@getCustomerDetails')->name('getCustomerDetails');
Route::post('/updateCustomerQuoteInfo', 'admin\QuotationController@updateCustomerQuoteInfo')->name('updateCustomerQuoteInfo');

Route::resource('/insurance', 'admin\InsuranceController');
Route::get('insurance/accessCode/{token}', 'admin\InsuranceController@insuranceToken');
Route::post('insurance/pdf', 'admin\InsuranceController@getPdf')->name('getPdf');
Route::post('findInsurance', 'admin\InsuranceController@findInsurance')->name('findInsurance');
Route::post('generateInsuranceReport', 'admin\InsuranceController@generateInsuranceReport')->name('generateInsuranceReport');
Route::post('agreement', 'admin\InsuranceController@agreement')->name('agreement');
Route::get('agreementMessage', 'admin\InsuranceController@agreementMessage')->name('agreementMessage');
Route::post('getInsuranceImage', 'admin\InsuranceController@getInsuranceImage')->name('getInsuranceImage');
Route::post('generateAgreementReport', 'admin\InsuranceController@generateAgreementReport')->name('generateAgreementReport');
Route::get('agreementPdf/{id}', 'admin\InsuranceController@agreementPdf')->name('agreementPdf');
Route::post('/autocomplete', 'admin\InsuranceController@autocomplete')->name('autocomplete');



Route::post('deleteInventoryUnit', 'admin\InventoryController@deleteInventoryUnit')->name('deleteInventoryUnit');

Route::get('test', 'admin\InventoryController@test')->name('test');

Route::get('agreement', 'admin\InsuranceController@agreement')->name('agreement');

Route::get('subcontractor_pdf', 'admin\InsuranceController@subcontractor_pdf')->name('subcontractor_pdf');
Route::get('rental_pdf', 'admin\InsuranceController@rental_pdf')->name('rental_pdf');
Route::get('owner_pdf', 'admin\InsuranceController@owner_pdf')->name('owner_pdf');

Route::get('creditApplication', 'admin\CustomerController@creditApplication')->name('creditApplication');
Route::get('creditApplicationEmail', 'admin\CustomerController@creditApplicationEmail')->name('creditApplicationEmail');
Route::get('creditApplicationPdf', 'admin\CustomerController@creditApplicationPdf')->name('creditApplicationPdf');

Route::resource('/jobChecklist', 'admin\JobChecklistController');


Route::get('/users/make_admin/{id}','admin\UserController@make_admin')->name('make.admin');
Route::get('/users/make_agent/{id}','admin\UserController@make_agent')->name('make.agent');






