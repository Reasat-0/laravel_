@extends('layouts.master')

@section('content')

        <div class="content">
          <div class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-12 mx-auto card_holder_box">
                  
                  
                  <div class="card">
                  <div class="card-header card-header-tabs card-header-rose">
                    <div class="nav-tabs-navigation">
                      <div class="nav-tabs-wrapper">
                        
                        <ul class="nav nav-tabs" data-tabs="tabs">
                          <li class="nav-item">
                            <a class="nav-link active" href="#profile" data-toggle="tab">
                              <i class="material-icons">ballot</i> Edit Category
                              <div class="ripple-container"></div>
                            </a>
                          </li>
                          
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div style="margin-top: 15px;">
                      @include('partials.messages')
                  </div>

                  <div class="card-body">
                     
                        <div class="card">
                          <div class="card-header card-header-rose card-header-text">
                            
                          </div>

                          <div class="card-body col-md-10 mx-auto increased_margin_t_cardbody">
                            <form method="post" action="{{ route('inventory.update', $cat->id) }}" class="form-horizontal">

                            @csrf
                            @method('PUT')

                            <input type="hidden" name="tab" value="category">
                            <input type="hidden" name="p_key" value="{{$cat->id}}">

                                  <div class="form-group bmd-form-group is-filled">
                                    <label for="category_name" class="bmd-label-floating input_label"> Category Name *</label>
                                    <input type="text" class="form-control form_input_field" id="category_name" required="true" aria-required="true" value="{{$cat->category_name}}" name="category_name">
                                  </div>
                                <div class="form-group bmd-form-group save_btn_wrapper">
                                  <button type="submit" class="btn save_btn custom-btn-one">Update</button>
                                </div>
                            </form>
                          </div>
                      </div>


                     
                  </div>
                </div>
  
  
  
                </div>
              </div>
          </div>
        </div>
      </div>


@endsection