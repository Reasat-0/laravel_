-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 24, 2021 at 05:18 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oil_field_service`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_url`
--

CREATE TABLE `app_url` (
  `id` int(11) NOT NULL,
  `url` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_url`
--

INSERT INTO `app_url` (`id`, `url`, `created_at`, `updated_at`) VALUES
(1, 'http://localhost:8000', '2020-07-20 11:19:57', '2020-07-20 11:19:57');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Trucks', 'inventory', '2020-08-13 00:53:25', '2020-08-13 00:53:25');

-- --------------------------------------------------------

--
-- Table structure for table `credit_application`
--

CREATE TABLE `credit_application` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `company_name` varchar(191) DEFAULT NULL,
  `client_telephone` varchar(191) DEFAULT NULL,
  `company_email` varchar(191) DEFAULT NULL,
  `client_contact_no` varchar(191) DEFAULT NULL,
  `client_address` varchar(191) DEFAULT NULL,
  `client_city` varchar(191) DEFAULT NULL,
  `client_state` varchar(191) DEFAULT NULL,
  `client_zip` varchar(191) DEFAULT NULL,
  `client_business_type` varchar(191) DEFAULT NULL,
  `client_business_date` varchar(191) DEFAULT NULL,
  `client_years_address` varchar(191) DEFAULT NULL,
  `client_no_employee` varchar(191) DEFAULT NULL,
  `type_of_organization` varchar(191) DEFAULT NULL,
  `party_charge_acts` varchar(191) DEFAULT NULL,
  `po_required` varchar(191) DEFAULT NULL,
  `terms_are` varchar(191) DEFAULT NULL,
  `person_accepting_term` varchar(191) DEFAULT NULL,
  `title` varchar(191) DEFAULT NULL,
  `social_tax_number` varchar(191) DEFAULT NULL,
  `primary_bank_used` varchar(191) DEFAULT NULL,
  `bank_telephone` varchar(191) DEFAULT NULL,
  `bank_representatative` varchar(191) DEFAULT NULL,
  `types_of_account` varchar(191) DEFAULT NULL,
  `date_opened` varchar(191) DEFAULT NULL,
  `avg_checking_balance` varchar(191) DEFAULT NULL,
  `hi_balance` varchar(191) DEFAULT NULL,
  `avg_balance` varchar(191) DEFAULT NULL,
  `db_rating` varchar(191) DEFAULT NULL,
  `loan_outstanding` varchar(191) DEFAULT NULL,
  `current_loan_balance` varchar(191) DEFAULT NULL,
  `hi_balance2` varchar(191) DEFAULT NULL,
  `avg_balance2` varchar(191) DEFAULT NULL,
  `payment_history` varchar(191) DEFAULT NULL,
  `bank_credit_rating` varchar(191) DEFAULT NULL,
  `sic_code` varchar(191) DEFAULT NULL,
  `comments` varchar(191) DEFAULT NULL,
  `cr_suplier_name1` varchar(191) DEFAULT NULL,
  `cr_type_of_business1` varchar(191) DEFAULT NULL,
  `cr_suplier_addr1` varchar(191) DEFAULT NULL,
  `cr_suplier_contact1` varchar(191) DEFAULT NULL,
  `cr_suplier_telephone1` varchar(191) DEFAULT NULL,
  `cr_suplier_fax1` varchar(191) DEFAULT NULL,
  `cr_credit_limit1` varchar(191) DEFAULT NULL,
  `cr_date_acct_opened1` varchar(191) DEFAULT NULL,
  `cr_account_average1` varchar(191) DEFAULT NULL,
  `cr_account_high1` varchar(191) DEFAULT NULL,
  `cr_avg_day_pay1` varchar(191) DEFAULT NULL,
  `cr_terms1` varchar(191) DEFAULT NULL,
  `cr_rating1` varchar(191) DEFAULT NULL,
  `cr_express_comments1` varchar(191) DEFAULT NULL,
  `cr_suplier_name2` varchar(191) DEFAULT NULL,
  `cr_type_of_business2` varchar(191) DEFAULT NULL,
  `cr_suplier_addr2` varchar(191) DEFAULT NULL,
  `cr_suplier_contact2` varchar(191) DEFAULT NULL,
  `cr_suplier_telephone2` varchar(191) DEFAULT NULL,
  `cr_suplier_fax2` varchar(191) DEFAULT NULL,
  `cr_credit_limit2` varchar(191) DEFAULT NULL,
  `cr_date_acct_opened2` varchar(191) DEFAULT NULL,
  `cr_account_average2` varchar(191) DEFAULT NULL,
  `cr_account_high2` varchar(191) DEFAULT NULL,
  `cr_avg_day_pay2` varchar(191) DEFAULT NULL,
  `cr_terms2` varchar(191) DEFAULT NULL,
  `cr_rating2` varchar(191) DEFAULT NULL,
  `cr_express_comments2` varchar(191) DEFAULT NULL,
  `cr_suplier_name3` varchar(191) DEFAULT NULL,
  `cr_type_of_business3` varchar(191) DEFAULT NULL,
  `cr_suplier_addr3` varchar(191) DEFAULT NULL,
  `cr_suplier_contact3` varchar(191) DEFAULT NULL,
  `cr_suplier_telephone3` varchar(191) DEFAULT NULL,
  `cr_suplier_fax3` varchar(191) DEFAULT NULL,
  `cr_credit_limit3` varchar(191) DEFAULT NULL,
  `cr_date_acct_opened3` varchar(191) DEFAULT NULL,
  `cr_account_average3` varchar(191) DEFAULT NULL,
  `cr_account_high3` varchar(191) DEFAULT NULL,
  `cr_avg_day_pay3` varchar(191) DEFAULT NULL,
  `cr_terms3` varchar(191) DEFAULT NULL,
  `cr_rating3` varchar(191) DEFAULT NULL,
  `cr_express_comments3` varchar(191) DEFAULT NULL,
  `customer_sign_date` varchar(191) DEFAULT NULL,
  `ca_approved_by` varchar(191) DEFAULT NULL,
  `ca_credit_limit` varchar(191) DEFAULT NULL,
  `ca_terms` varchar(191) DEFAULT NULL,
  `ca_source_code` varchar(191) DEFAULT NULL,
  `ca_client_account_no` varchar(191) DEFAULT NULL,
  `ca_completed_by` varchar(191) DEFAULT NULL,
  `ca_checked_by` varchar(191) DEFAULT NULL,
  `ca_date` varchar(191) DEFAULT NULL,
  `customer_send_mail_to` varchar(191) DEFAULT NULL,
  `customer_send_mail_date` varchar(191) DEFAULT NULL,
  `submitted_by_email` varchar(191) DEFAULT NULL,
  `submitted_by_name` varchar(191) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `access_code` varchar(191) DEFAULT NULL,
  `url_token` varchar(191) DEFAULT NULL,
  `reject` text DEFAULT NULL,
  `sent` text DEFAULT NULL,
  `seen` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_application`
--

INSERT INTO `credit_application` (`id`, `customer_id`, `company_name`, `client_telephone`, `company_email`, `client_contact_no`, `client_address`, `client_city`, `client_state`, `client_zip`, `client_business_type`, `client_business_date`, `client_years_address`, `client_no_employee`, `type_of_organization`, `party_charge_acts`, `po_required`, `terms_are`, `person_accepting_term`, `title`, `social_tax_number`, `primary_bank_used`, `bank_telephone`, `bank_representatative`, `types_of_account`, `date_opened`, `avg_checking_balance`, `hi_balance`, `avg_balance`, `db_rating`, `loan_outstanding`, `current_loan_balance`, `hi_balance2`, `avg_balance2`, `payment_history`, `bank_credit_rating`, `sic_code`, `comments`, `cr_suplier_name1`, `cr_type_of_business1`, `cr_suplier_addr1`, `cr_suplier_contact1`, `cr_suplier_telephone1`, `cr_suplier_fax1`, `cr_credit_limit1`, `cr_date_acct_opened1`, `cr_account_average1`, `cr_account_high1`, `cr_avg_day_pay1`, `cr_terms1`, `cr_rating1`, `cr_express_comments1`, `cr_suplier_name2`, `cr_type_of_business2`, `cr_suplier_addr2`, `cr_suplier_contact2`, `cr_suplier_telephone2`, `cr_suplier_fax2`, `cr_credit_limit2`, `cr_date_acct_opened2`, `cr_account_average2`, `cr_account_high2`, `cr_avg_day_pay2`, `cr_terms2`, `cr_rating2`, `cr_express_comments2`, `cr_suplier_name3`, `cr_type_of_business3`, `cr_suplier_addr3`, `cr_suplier_contact3`, `cr_suplier_telephone3`, `cr_suplier_fax3`, `cr_credit_limit3`, `cr_date_acct_opened3`, `cr_account_average3`, `cr_account_high3`, `cr_avg_day_pay3`, `cr_terms3`, `cr_rating3`, `cr_express_comments3`, `customer_sign_date`, `ca_approved_by`, `ca_credit_limit`, `ca_terms`, `ca_source_code`, `ca_client_account_no`, `ca_completed_by`, `ca_checked_by`, `ca_date`, `customer_send_mail_to`, `customer_send_mail_date`, `submitted_by_email`, `submitted_by_name`, `status`, `access_code`, `url_token`, `reject`, `sent`, `seen`, `created_at`, `updated_at`) VALUES
(6, 373, 'xxc', '(432) 682-7582', 'accounts@abanienergy.com', 'ad das', '505 N Big Spring St # 500', 'Midland', '1323', '79701', '123', '08/12/2020', '3123', '45', 'Others', '312', 'Yes', '313', 'da sa', 'asddas', 'dasd', 'dasd', '(131) 231-2313', 'd asd', 'asd', '08/18/2020', '123', '123', '3123', '3dwqd', 'Unsecured', '213', '23', '312', 'Good', 'Fair', 'we wqe', 'sa sadas', 'qweq', 'adasd a', 'ewqe', 'asdsad as', '(312) 312-3123', '(123) 123-1231', '12', '08/12/2020', '12', '12', '12', 'wsad das d', 'Good', 'da sdasd a', 'da s', 'asdasd', 'das', 'sadasd', '(123) 123-1231', '(312) 312-3123', '12', '08/03/2020', '21', '12', '12', '08/29/2020', 'Excellent', 'ewqeqeqweq', 'eqweqw', 'asdasd', 'eqwe', 'dasdas', '(123) 123-1312', '(123) 123-1231', '233', '08/11/2020', '123', '123', '3123', 'dssad', 'Fair', 'dada s', '08/29/2020', 'asd', '1321', 'dasd', 'eqe', 'dasd', 'dada', 'qeq565', '08/28/2020', 'looklike.himu@gmail.com', '08/29/2020', 'eg.himel@gmail.com', 'test1', 3, '195181', '6-JRQLVES1NBC2YPOUFD85TAI0XKM763', NULL, '[\"2020-08-29T11:38:41.533576Z\",\"2020-08-29T11:54:50.995752Z\",\"2020-08-29T11:58:07.352369Z\",\"2020-09-08T06:18:22.680067Z\",\"2020-09-08T06:19:01.307903Z\",\"2020-09-08T06:29:34.041742Z\"]', '[\"2020-08-29T11:45:55.895210Z\",\"2020-08-29T11:50:26.968287Z\",\"2020-08-29T11:55:46.706267Z\",\"2020-08-29T11:58:47.320919Z\",\"2020-09-08T06:21:07.861663Z\",\"2020-09-08T06:30:15.696245Z\",\"2020-09-08T06:34:15.522580Z\",\"2020-09-08T06:35:19.127994Z\",\"2020-09-08T06:38:09.921653Z\",\"2020-09-08T07:10:29.838819Z\",\"2020-09-08T07:11:08.438574Z\"]', '2020-08-29 05:38:41', '2020-09-08 01:11:08'),
(7, 372, NULL, '(940) 202-8640', 'geraldg@aaawellservice.us', NULL, '6802 7222, N County Rd 1150', 'Midland', NULL, '79705', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '432,432,432,434.00', '123,445,678.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'geraldg@aaawellservice.us', '08/30/2020', 'm.reasat38@gmail.com', 'test1', 1, '941369', '7-B5PA1C7NGIV2TKUM3LQ94H8ODX6FJY', NULL, '[\"2020-08-30T11:09:09.689182Z\"]', NULL, '2020-08-30 05:09:09', '2020-08-30 05:09:09'),
(9, 381, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'looklike.himu@gmail.com', '08/30/2020', 'eg.himel@gmail.com', 'test1', 1, '714840', '9-5SPQMKR2ZXWG93VE0BNUIAJ46F817T', NULL, '[\"2020-08-30T12:17:37.610307Z\"]', NULL, '2020-08-30 06:17:37', '2020-08-30 06:17:37'),
(10, 344, 'Agility Energy Inc.', '(801) 878-4710', 'dping@agilityenergyinc.com', 'dsad sadsa d', '4807 S. County Rd.', 'Midland', 'sfaf', '79706', 'afaf', '08/16/2020', '13', '12', 'Others', 'asd', 'Yes', 'qeq', 'asdd', 'dasd', 'dasd', 'qwe', '(123) 123-1231', 'dada', 'adas', '08/12/2020', '133.00', '12.00', '12.00', 'asd s', 'Unsecured', '123.00', '1,231.00', '123.00', 'Good', 'Fair', '123asde', 'asd sads ad', 'd asd', 'dasd', 'd sadsa', 'adsa das', '(312) 312-3123', '(321) 312-3123', '123.00', '08/18/2020', '123.00', '12.00', '12', 'das das', 'Fair', 'dsada a', 'asdasda', 'asdasd', 'eqweq', 'sadasd', '(123) 123-1231', '(123) 123-1231', '123.00', '08/18/2020', '31.00', '12.00', '12', 'asdsa s', 'Good', 'd asdsad sa', 'das d', 'werwer', 'asd', 'sdsdfs sd', '(123) 123-1312', '(123) 123-1231', '12.00', '08/25/2020', '12.00', '12.00', '12', 'sa as d d', 'Poor', 'd sad s', '08/18/2020', 'wewq', '123,123,123,123.00', 'qweq', 'eqe', 'eqweq', 'dada', 'qeq', '08/31/2020', 'looklike.himu@gmail.com', '08/31/2020', 'eg.himel@gmail.com', 'test1', 3, '375984', '10-1XP69JWVO8NCUQALM0SDI4KRHZT2F5', NULL, '[\"2020-08-31T09:34:33.940939Z\",\"2020-08-31T09:39:21.027227Z\"]', '[\"2020-08-31T09:35:14.088323Z\",\"2020-08-31T09:39:44.004710Z\"]', '2020-08-31 03:34:33', '2020-08-31 03:39:44'),
(11, 352, 'testc', '(888) 256-5325', 'eqeq@asd.com', 'safa', '325 Pronto Ave', 'Odessa', 'sfaf', '79762', 'afaf', '08/25/2020', 'asfa', '2', 'LLC', '112321', 'No', 'qeq', 'asdd', 'dasd', 'dasd', 'qwe', '(131) 231-2313', 'dada', 'asd', '08/18/2020', '312,312,312.00', '313.00', '123.00', '3dwqd', 'Secured', '133.00', '1,231.00', '1,233.00', 'Good', 'Fair', 'we wqe', 'wwcer we', 'asdas', 'asd', 'asd', 'asda', '(312) 312-3123', '1231231231231223', '123.00', '08/04/2020', '123.00', '12.00', '2', 'das das', 'Good', 'dsada a', '12313', '131', 'asda', 'sadasd', '(123) 123-1231', '(312) 312-3123', '123.00', '08/03/2020', '31.00', '13,231.00', '2', '08/24/2020', 'Excellent', 'ewqeqeqweq', 'eqweqw', 'werwer', 'dasd', 'sdsdfs sd', '(123) 123-1312', '(123) 123-1231', '133.00', '08/18/2020', '1,331.00', '13.00', '1', 'sa as d d', 'Good', 'dasd', '08/17/2020', 'wewq', '123,123,123,123.00', 'qweq', 'eqe', 'eqweq', 'dada', 'qeq', '08/25/2020', 'looklike.himu@gmail.com', '08/31/2020', 'eg.himel@gmail.com', 'test1', 3, '670455', '11-YXLEFIQ7STOWV3RKGCJH2491DUM5AN', NULL, '[\"2020-08-31T13:40:11.651641Z\",\"2020-08-31T13:55:01.375002Z\",\"2020-08-31T13:56:59.141216Z\"]', '[\"2020-08-31T13:40:29.822166Z\",\"2020-08-31T13:41:16.532390Z\",\"2020-08-31T13:41:35.783470Z\",\"2020-08-31T13:43:05.877702Z\",\"2020-08-31T13:43:50.153994Z\",\"2020-08-31T13:51:31.994768Z\",\"2020-08-31T13:55:21.845809Z\",\"2020-08-31T13:57:15.328227Z\"]', '2020-08-31 07:40:11', '2020-08-31 07:57:15'),
(12, 349, 'testc', '(432) 694-1994', 'lbradford@agri-empresa.com', 'safa', '2907 South N County Rd 1250,', 'Midland', 'sfaf', '79706', 'afaf', '08/10/2020', '13', '2', 'Individual Proprietor', '112321', 'Yes', 'qeq', 'asdd', 'dasd', '1231', 'qwe', '(123) 123-1231', 'dada', 'sad das', '08/19/2020', '12,321.00', '313.00', '123.00', 'asd', 'Unsecured', '133.00', '1,231.00', '1,233.00', 'Good', 'Fair', '213', 'f sdfsd fsdf', 'asdas', 'asd', 'asd', 'asda', '(312) 312-3123', '1231231231231223', '123.00', '08/17/2020', '123.00', '12.00', '12', 'asd', 'Good', 'asd sad', '12313', '131', 'asda', 'sadasd', '(123) 123-1231', '(312) 312-3123', '123.00', '08/26/2020', '31.00', '13,231.00', '21', 'asdsa s', 'Poor', 'asdsadsad', 'eqweqw', 'werwer', 'dasd', 'sdsdfs sd', '(123) 123-1312', '(123) 123-1231', '13.00', '08/18/2020', '123.00', '13.00', '13', 'd sad', 'Excellent', 'd sad', '08/11/2020', 'wewq', '123,123,123,123.00', 'qweq', 'eqe', 'eqweq', 'dada', 'qeq', '08/18/2020', 'looklike.himu@gmail.com', '08/31/2020', 'eg.himel@gmail.com', 'test1', 3, '952402', '12-L2P3ZNXCUY65OTS0KGBFI9AEDRJ8W4', NULL, '[\"2020-08-31T14:14:27.733162Z\",\"2020-08-31T15:53:08.426463Z\",\"2020-08-31T15:56:33.523135Z\"]', '[\"2020-08-31T15:39:33.787524Z\",\"2020-08-31T15:53:28.296939Z\",\"2020-08-31T15:54:22.374518Z\",\"2020-08-31T15:56:52.883216Z\"]', '2020-08-31 08:14:27', '2020-08-31 09:56:52'),
(13, 376, NULL, '(303) 501-7836', 'brandon.yeager@basinfluids.com', NULL, '4317 E. Utah PL', 'Denver', NULL, '80222', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'looklike.himu@gmail.com', '08/31/2020', 'eg.himel@gmail.com', 'test1', 1, '243847', '13-HPZ0CYBA83W71VTQ9SIMU5RN2KFGL4', NULL, '[\"2020-08-31T14:16:05.624615Z\"]', NULL, '2020-08-31 08:16:05', '2020-08-31 08:16:05'),
(14, 355, 'Basin Pump Down Services, LLC.', '(432) 203-0614', 'fgarcia@basinpumpdown.com', NULL, 'P.O. BOX 80550', 'MIDLAND', NULL, '79706', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5678131231', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'looklike.himu@gmail.com', '08/31/2020', 'eg.himel@gmail.com', 'test1', 1, '233801', '14-GYA8ZKUCBME7Q2S41LO9XJ6PN5TIH0', NULL, '[\"2020-08-31T15:24:07.493484Z\"]', NULL, '2020-08-31 09:24:07', '2020-08-31 09:24:07'),
(15, 361, NULL, '(405) 317-7161', 'alice.castillo@silverbacktransport.net', NULL, '1405 Redbird Circle', 'Graham', NULL, '76450', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'looklike.himu@gmail.com', '08/31/2020', 'eg.himel@gmail.com', 'test1', 1, '313869', '15-PHJ4YT7G3CSOMX6R09A8INKB2EDV5Z', NULL, '[\"2020-08-31T15:39:08.173607Z\"]', NULL, '2020-08-31 09:39:08', '2020-08-31 09:39:08'),
(16, 339, 'weqwe', '(432) 201-8921', 'tracy@knoxsupply.net', 'safa', '10 Desta Dr. Suite 240W', 'Midland', 'asd', '79705', 'afaf', '09/06/2020', '13', '2', 'Corporation', '112321', 'Yes', 'qeq', '131da', 'dasd', '931-23-1231', 'qwe', '(131) 231-2313', 'dada', 'asd', '09/08/2020', '312,312,312.00', '313.00', '123.00', 'asd', 'Unsecured', '133.00', '1,231.00', '1,233.00', 'Fair', 'Fair', 'we wqe', 'sad sadsa', 'asdas', 'asd', 'asd', 'asdsad as', '(312) 312-3123', NULL, '123.00', '09/20/2020', '123.00', '12.00', '2', 'wsad das d', 'Fair', 'fsds', '12313', '131', 'asda', 'sadasd', '(123) 123-1231', NULL, '123.00', '09/14/2020', '31.00', '13,231.00', '2', 'adas dsa', 'Good', 'd asdsad sa', 'eqweqw', 'werwer', 'dasd', 'sdsdfs sd', '(123) 123-1312', NULL, '133.00', '09/04/2020', '1,331.00', '13.00', '3', 'ads ad', 'Good', 'gdfgd', '09/01/2020', 'wewq', '127,889.00', 'qweq', NULL, '12312', 'dada', 'qeq', '09/08/2020', 'looklike.himu@gmail.com', '09/07/2020', 'eg.himel@gmail.com', 'test1', 4, '319891', '16-CNAZTK7SV60R53DFYJUPELHMB124WI', '<p>1. wdasdsad</p>', '[\"2020-09-07T05:29:04.965831Z\",\"2020-09-07T11:52:00.385572Z\"]', '[\"2020-09-07T05:29:57.097579Z\",\"2020-09-07T11:29:35.284457Z\",\"2020-09-07T11:29:57.988842Z\",\"2020-09-07T11:42:33.764799Z\",\"2020-09-07T11:52:19.942948Z\",\"2020-09-07T11:52:47.791253Z\",\"2020-09-07T11:53:30.215964Z\",\"2020-09-07T11:54:00.094988Z\",\"2020-09-07T11:57:29.704295Z\",\"2020-09-07T11:58:43.792014Z\"]', '2020-09-06 23:29:04', '2020-09-07 05:58:43'),
(17, 332, 'Bos Solutions', '(432) 567-7712', 'sandra.ramirez@bos-solutions.com', 'safa', '8916 W. CR 127', 'Midland', 'sfaf', '79706', 'afaf', '09/13/2020', '13', '2', 'Partnership', '112321', 'No', '313', 'asdd', 'dasd', '1231', 'qwe', '(131) 231-2313', 'dada', 'asd', '09/01/2020', '312,312,312.00', '313.00', '123.00', 'asd', 'Secured', '133.00', '1,231.00', '1,233.00', 'Fair', 'Fair', 'sadad', 'sad ad', 'asdas', 'asd', 'asd', 'asda', '(312) 312-3123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12313', '131', 'asda', 'sadasd', '(123) 123-1231', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eqweqw', 'werwer', 'dasd', 'sdsdfs sd', '(123) 123-1312', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '09/04/2020', NULL, NULL, NULL, NULL, '(432) 567-7712', NULL, NULL, NULL, 'looklike.himu@gmail.com', '09/07/2020', 'eg.himel@gmail.com', 'test1', 2, '635183', '17-K89I4Q7EJ21L0UXHPAZOGTBMFW3S6V', NULL, '[\"2020-09-07T12:25:18.915115Z\"]', '[\"2020-09-07T12:25:50.906572Z\",\"2020-09-07T12:27:34.315609Z\",\"2020-09-07T12:28:05.027086Z\",\"2020-09-07T12:31:02.109003Z\",\"2020-09-07T12:46:33.608925Z\",\"2020-09-07T12:47:38.794336Z\",\"2020-09-07T12:49:52.225555Z\",\"2020-09-07T12:50:07.313279Z\",\"2020-09-07T12:51:01.374045Z\",\"2020-09-07T12:51:57.892543Z\",\"2020-09-07T12:53:25.156425Z\",\"2020-09-07T12:54:55.680088Z\",\"2020-09-07T12:55:54.240070Z\",\"2020-09-07T13:13:11.339818Z\",\"2020-09-07T13:13:42.144889Z\",\"2020-09-07T13:14:18.320290Z\"]', '2020-09-07 06:25:18', '2020-09-07 07:15:45'),
(20, 371, 'testc', '(432) 694-0063', 'jhight@byrdoilfield.com', 'safa', '3020 Pease Trail', 'Midland', 'sfaf', '79706', 'afaf', '09/09/2020', 'asfa', '3', 'LLC', '112321', 'Yes', 'qeq', '131da', 'dasd', '2321', 'qwe', '(131) 231-2313', 'dada', 'asd', '09/07/2020', '312,312,312.00', '313.00', '123.00', 'asd', 'Secured', '133.00', '1,231.00', '1,233.00', 'Good', 'Fair', 'sadad', 'df sa dsa', 'asdas', 'asd', 'asd', 'asda', '(312) 312-3123', NULL, '123.00', '09/08/2020', '123.00', '12.00', '2', 'asd', 'Good', 'dasda da', '12313', '131', 'asda', 'sadasd', '(123) 123-1231', NULL, '123.00', '09/08/2020', '31.00', '13,231.00', '2', '08/24/2020', 'Good', 'ewqeqeqweq', 'eqweqw', 'werwer', 'dasd', 'sdsdfs sd', '(123) 123-1312', NULL, '133.00', '09/08/2020', '1,331.00', '13.00', '2', 'ads ad', 'Good', 'eqw eqw', '09/08/2020', 'wewq', '6,789.00', 'qweq', NULL, '(432) 694-0063', 'dada', 'qeq', '09/01/2020', 'looklike.himu@gmail.com', '09/08/2020', 'eg.himel@gmail.com', 'test1', 3, '952001', '20-THJCWV1LRF3KIN6BDO7QX0A542UMGZ', NULL, '[\"2020-09-08T13:14:32.005639Z\",\"2020-09-08T13:16:55.161595Z\",\"2020-09-08T13:19:06.239943Z\",\"2020-09-08T13:20:14.789698Z\"]', '[\"2020-09-08T13:15:02.812898Z\",\"2020-09-08T13:17:10.968050Z\",\"2020-09-08T13:19:26.023139Z\",\"2020-09-08T13:21:27.934263Z\"]', '2020-09-08 07:14:32', '2020-09-08 07:21:27'),
(21, 368, 'Marvel Studios', '(132) 321-2322', 'dsfasdf@gmail.com', 'fsdfdf dfsfsdf', '449 East Private Road 42', 'Monahans', 'dfsfsdfdsf', '79756', 'dfsdff', '09/09/2020', 'fdsf', '12', 'Others', 'dsfsf', 'Yes', 'dsfsd', 'dfdsdsf', '454', '1231', 'dffdsf', '(213) 334-3342', 'ffdfs', 'dfgf', '09/09/2020', '3,434,324.00', '432,432,432,434.00', '123,445,678.00', '23', 'Secured', '34,324.00', '34,324,234,234.00', '345,435.00', 'Good', 'Fair', '32432', 'sfdfdf', 'safdfds', 'fdsfdfd', 'dfsf', 'dfdfsd', '(243) 243-2432', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dsff', 'dfdfds', 'fdsff', 'fdsdf', '(234) 323-3432', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dsfsdf', 'dfsdf', 'dfdsf', 'fdsf', '(233) 343-4343', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '09/14/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'r.muntasir38@gmail.com', '09/09/2020', 'eg.himel@gmail.com', 'test1', 2, '843359', '21-HL3OSYF5EUDJMX6C2I4AK7RTPWQ0GN', NULL, '[\"2020-09-09T06:30:47.099353Z\"]', '[\"2020-09-09T06:31:36.284381Z\"]', '2020-09-09 00:30:47', '2020-09-09 00:34:03');

-- --------------------------------------------------------

--
-- Table structure for table `credit_application_history`
--

CREATE TABLE `credit_application_history` (
  `id` int(11) NOT NULL,
  `credit_app_id` int(11) DEFAULT NULL,
  `credit_limit` varchar(191) DEFAULT NULL,
  `ca_terms` varchar(191) DEFAULT NULL,
  `date` varchar(191) DEFAULT NULL,
  `access_code` varchar(191) DEFAULT NULL,
  `url_token` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_application_history`
--

INSERT INTO `credit_application_history` (`id`, `credit_app_id`, `credit_limit`, `ca_terms`, `date`, `access_code`, `url_token`, `created_at`, `updated_at`) VALUES
(1, 20, '6,889.00', 'qweq 234324324 test', '09/14/2020', NULL, NULL, '2020-09-08 07:20:14', '2020-09-08 07:20:14');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `givenName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middleName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `familyName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suffix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `displayName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `companyName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `businessNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primaryPhone_deviceType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primaryPhone_countryCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primaryPhone_areaCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primaryPhone_exchangeCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primaryPhone_extension` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primaryPhone_freeFormNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alternatePhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primaryEmailAddr_addess` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primaryEmailAddr_default` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_line1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_line2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_countryCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_countrySubDivisionCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_postalCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_postalCodeSuffix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_long` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_contact_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_line1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_line2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_countryCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_countrySubDivisionCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_postalCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_postalCodeSuffix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_long` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lease_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rig_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `afe_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_contact_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rig_contact_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_man_contact_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otherAddr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `altContactName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `webAddr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otherContactInfo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quickbooks_id` int(11) DEFAULT NULL,
  `syncToken` int(11) DEFAULT NULL,
  `quickbooks` tinyint(4) NOT NULL DEFAULT 0,
  `credit_app_creation` tinyint(4) NOT NULL DEFAULT 0,
  `manual` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `temp` tinyint(4) NOT NULL DEFAULT 0,
  `updated` tinyint(4) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `givenName`, `middleName`, `familyName`, `suffix`, `account_no`, `displayName`, `companyName`, `title`, `businessNumber`, `organization`, `primaryPhone_deviceType`, `primaryPhone_countryCode`, `primaryPhone_areaCode`, `primaryPhone_exchangeCode`, `primaryPhone_extension`, `primaryPhone_freeFormNumber`, `alternatePhone`, `mobile`, `fax`, `primaryEmailAddr_addess`, `primaryEmailAddr_default`, `billAddr_line1`, `billAddr_line2`, `billAddr_city`, `billAddr_country`, `billAddr_countryCode`, `billAddr_countrySubDivisionCode`, `billAddr_postalCode`, `billAddr_postalCodeSuffix`, `billAddr_lat`, `billAddr_long`, `billing_phone`, `billing_email`, `billing_contact_name`, `shipAddr_line1`, `shipAddr_line2`, `shipAddr_city`, `shipAddr_country`, `shipAddr_countryCode`, `shipAddr_countrySubDivisionCode`, `shipAddr_postalCode`, `shipAddr_postalCodeSuffix`, `shipAddr_lat`, `shipAddr_long`, `lease_name`, `rig_number`, `afe_number`, `site_contact_phone`, `rig_contact_email`, `company_man_contact_name`, `latitude`, `longitude`, `otherAddr`, `contactName`, `altContactName`, `webAddr`, `otherContactInfo`, `notes`, `quickbooks_id`, `syncToken`, `quickbooks`, `credit_app_creation`, `manual`, `temp`, `updated`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Test Given', 'Mid Test', NULL, NULL, NULL, 'Test Traders', 'test', 'Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(234) 324-3243', '(124) 432-4323', NULL, NULL, 'sfdfdfsd@gmail.com', NULL, '620 N Grant St. 1200', 'gfdgdfgs', 'Odessa', 'Ector', NULL, 'fdsfsdfds', '79706', NULL, NULL, NULL, '(453) 453-4543', 'fsdf@gmail.com', 'dsfsddsf', 'dsfsdfdsf', 'dfdsf', 'dfsdf', 'dsfdsfsdf', NULL, 'dfdsf', 'fdsfds', NULL, NULL, NULL, 'thtrgjtyhjah', '425dsvsda3154', '5325erwerewr', '(432) 453-5353', '4sdfdsf@gmail.com', 'sFdda', '12', '43', NULL, NULL, NULL, 'http://www.sfscfsdf.com', NULL, 'dsfsdf', NULL, NULL, 0, 0, '0', 0, 0, NULL, '2021-01-15 00:12:25', '2021-01-15 00:12:25');

-- --------------------------------------------------------

--
-- Table structure for table `email_configuration`
--

CREATE TABLE `email_configuration` (
  `id` int(10) UNSIGNED NOT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mailer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_host` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auto_tls` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `port_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authentication` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `smtp_password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_configuration`
--

INSERT INTO `email_configuration` (`id`, `from_name`, `from_email`, `mailer`, `smtp_host`, `auto_tls`, `port_no`, `authentication`, `smtp_user`, `smtp_password`, `created_at`, `updated_at`) VALUES
(1, 'Evos Team', 'evosfield@gmail.com', 'other', 'smtp.gmail.com', 'ssl', '465', 'on', 'evosfield@gmail.com', 'Trial@123456', '2020-06-07 23:52:49', '2020-06-11 10:50:28');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qb_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `type`, `parent_id`, `name`, `url`, `qb_id`, `created_at`, `updated_at`) VALUES
(1, 'vendor', 18, 'lpp.jpg', 'http://localhost:8000/storage/images/1587016536lpp.jpg', NULL, NULL, NULL),
(2, 'vendor', 18, 'screenshot.png', 'http://localhost:8000/storage/images/1587016536screenshot.png', NULL, NULL, NULL),
(3, 'vendor', 18, 'Untitled.png', 'http://localhost:8000/storage/images/1587016536Untitled.png', NULL, NULL, NULL),
(8, 'vendor', 18, 'a1.PNG', 'http://localhost:8000/storage/images/1587017478a1.PNG', NULL, NULL, NULL),
(9, 'vendor', 19, 'a1.PNG', 'http://localhost:8000/storage/images/1587017541a1.PNG', NULL, NULL, NULL),
(13, 'vendor', 20, 'a1.PNG', 'http://localhost:8000/storage/images/1587031904a1.PNG', NULL, NULL, NULL),
(14, 'vendor', 20, 'a2.PNG', 'http://localhost:8000/storage/images/1587031904a2.PNG', NULL, NULL, NULL),
(15, 'vendor', 20, '38897655_2102695313097539_9220072664452825088_n.jpg', 'http://localhost:8000/storage/images/158703190438897655_2102695313097539_9220072664452825088_n.jpg', NULL, NULL, NULL),
(16, 'vendor', 20, 'a1.PNG', 'http://localhost:8000/storage/images/1587031904a1.PNG', NULL, NULL, NULL),
(17, 'vendor', 20, 'a2.PNG', 'http://localhost:8000/storage/images/1587031904a2.PNG', NULL, NULL, NULL),
(18, 'vendor', 21, 'customer_1599633243_21.png', 'http://localhost:8000/storage/files/customer_1599633243_21.png', NULL, NULL, '2020-09-09 00:34:03'),
(19, 'vendor', 21, 'customer_1599633243_21.png', 'http://localhost:8000/storage/files/customer_1599633243_21.png', NULL, NULL, '2020-09-09 00:34:03'),
(20, 'vendor', 21, 'customer_1599633243_21.png', 'http://localhost:8000/storage/files/customer_1599633243_21.png', NULL, NULL, '2020-09-09 00:34:03'),
(21, 'vendor', 21, 'customer_1599633243_21.png', 'http://localhost:8000/storage/files/customer_1599633243_21.png', NULL, NULL, '2020-09-09 00:34:03'),
(22, 'vendor', 21, 'customer_1599633243_21.png', 'http://localhost:8000/storage/files/customer_1599633243_21.png', NULL, NULL, '2020-09-09 00:34:03'),
(23, 'vendor', 21, 'customer_1599633243_21.png', 'http://localhost:8000/storage/files/customer_1599633243_21.png', NULL, NULL, '2020-09-09 00:34:03'),
(24, 'vendor', 23, 'Untitled3.png', 'http://localhost:8000/storage/images/1587032256Untitled3.png', NULL, NULL, NULL),
(25, 'vendor', 23, 'Untitled4.png', 'http://localhost:8000/storage/images/1587032256Untitled4.png', NULL, NULL, NULL),
(26, 'vendor', 23, 'Untitled3.png', 'http://localhost:8000/storage/images/1587032256Untitled3.png', NULL, NULL, NULL),
(27, 'vendor', 23, 'Untitled4.png', 'http://localhost:8000/storage/images/1587032256Untitled4.png', NULL, NULL, NULL),
(28, 'vendor', 23, '38897655_2102695313097539_9220072664452825088_n.jpg', 'http://localhost:8000/storage/images/158703225638897655_2102695313097539_9220072664452825088_n.jpg', NULL, NULL, NULL),
(29, 'vendor', 46, '38897655_2102695313097539_9220072664452825088_n.jpg', 'http://localhost:8000/storage/images/158703734238897655_2102695313097539_9220072664452825088_n.jpg', NULL, NULL, NULL),
(30, 'vendor', 46, 'a1.PNG', 'http://localhost:8000/storage/images/1587037342a1.PNG', NULL, NULL, NULL),
(31, 'vendor', 46, 'a2.PNG', 'http://localhost:8000/storage/images/1587037342a2.PNG', NULL, NULL, NULL),
(32, 'vendor', 47, 'lpp.jpg', 'http://localhost:8000/storage/images/1587037367lpp.jpg', NULL, NULL, NULL),
(33, 'vendor', 47, 'screenshot.png', 'http://localhost:8000/storage/images/1587037367screenshot.png', NULL, NULL, NULL),
(34, 'vendor', 47, 'Untitled.png', 'http://localhost:8000/storage/images/1587037367Untitled.png', NULL, NULL, NULL),
(35, 'inventory', 148, '38897655_2102695313097539_9220072664452825088_n.jpg', 'http://localhost:8000/storage/images/158703990338897655_2102695313097539_9220072664452825088_n.jpg', NULL, NULL, NULL),
(36, 'inventory', 148, 'a1.PNG', 'http://localhost:8000/storage/images/1587039903a1.PNG', NULL, NULL, NULL),
(37, 'inventory', 148, 'a2.PNG', 'http://localhost:8000/storage/images/1587039903a2.PNG', NULL, NULL, NULL),
(38, 'vendor', 49, 'lpp.jpg', 'http://localhost:8000/storage/images/1587215365lpp.jpg', NULL, NULL, NULL),
(41, 'vendor', 49, 'Untitled4.png', 'http://localhost:8000/storage/images/1587215976Untitled4.png', NULL, NULL, NULL),
(42, 'vendor', 49, 'Capture26.PNG', 'http://localhost:8000/storage/images/1587215976Capture26.PNG', NULL, NULL, NULL),
(43, 'inventory_unit', 171, 'a2.PNG', 'http://localhost:8000/storage/images/1587216868a2.PNG', NULL, NULL, NULL),
(44, 'inventory_unit', 171, 'a1.PNG', 'http://localhost:8000/storage/images/1587216868a1.PNG', NULL, NULL, NULL),
(45, 'service', 9, 'Untitled.png', 'http://localhost:8000/storage/images/1587275382Untitled.png', NULL, NULL, NULL),
(47, 'service', 9, 'Capture26.PNG', 'http://localhost:8000/storage/images/1587275382Capture26.PNG', NULL, NULL, NULL),
(48, 'service', 9, 'a2.PNG', 'http://localhost:8000/storage/images/1587275749a2.PNG', NULL, NULL, NULL),
(51, 'service', 9, 'Untitled1.png', 'http://localhost:8000/storage/images/1587277350Untitled1.png', NULL, NULL, NULL),
(52, 'service', 9, 'Untitled4.png', 'http://localhost:8000/storage/images/1587277350Untitled4.png', NULL, NULL, NULL),
(53, 'service', 9, 'Capture.PNG', 'http://localhost:8000/storage/images/1587277350Capture.PNG', NULL, NULL, NULL),
(59, 'inventory', 149, 'a2.PNG', 'http://localhost:8000/storage/images/1587283333a2.PNG', NULL, NULL, NULL),
(60, 'inventory', 149, 'a1.PNG', 'http://localhost:8000/storage/images/1587283333a1.PNG', NULL, NULL, NULL),
(62, 'inventory_unit', 172, '38897655_2102695313097539_9220072664452825088_n.jpg', 'http://localhost:8000/storage/images/158728405238897655_2102695313097539_9220072664452825088_n.jpg', NULL, NULL, NULL),
(67, 'inventory', 149, 'screenshot.png', 'http://localhost:8000/storage/images/1587284171screenshot.png', NULL, NULL, NULL),
(68, 'inventory_unit', 172, 'lpp.jpg', 'http://localhost:8000/storage/images/1587284171lpp.jpg', NULL, NULL, NULL),
(69, 'inventory', 149, 'Untitled4.png', 'http://localhost:8000/storage/images/1587284420Untitled4.png', NULL, NULL, NULL),
(70, 'inventory', 149, '38897655_2102695313097539_9220072664452825088_n.jpg', 'http://localhost:8000/storage/images/158728442038897655_2102695313097539_9220072664452825088_n.jpg', NULL, NULL, NULL),
(71, 'inventory_unit', 172, 'Screenshot.png', 'http://localhost:8000/storage/images/1587284420Screenshot.png', NULL, NULL, NULL),
(72, 'inventory_unit', 173, 'screenshot.png', 'http://localhost:8000/storage/images/1587284645screenshot.png', NULL, NULL, NULL),
(73, 'inventory_unit', 173, 'lpp.jpg', 'http://localhost:8000/storage/images/1587284645lpp.jpg', NULL, NULL, NULL),
(74, 'inventory_unit', 173, 'a1.PNG', 'http://localhost:8000/storage/images/1587284645a1.PNG', NULL, NULL, NULL),
(76, 'service', 10, 'Capture25.PNG', 'http://localhost:8000/storage/images/1587285489Capture25.PNG', NULL, NULL, NULL),
(77, 'service', 10, 'a2.PNG', 'http://localhost:8000/storage/images/1587285528a2.PNG', NULL, NULL, NULL),
(78, 'service', 10, 'Untitled.png', 'http://localhost:8000/storage/images/1587285528Untitled.png', NULL, NULL, NULL),
(79, 'service', 10, 'a1.PNG', 'http://localhost:8000/storage/images/1587285528a1.PNG', NULL, NULL, NULL),
(80, 'vendor', 50, 'Untitled.png', 'http://localhost:8000/storage/images/1587288401Untitled.png', NULL, NULL, NULL),
(82, 'vendor', 50, 'lpp.jpg', 'http://localhost:8000/storage/images/1587288401lpp.jpg', NULL, NULL, NULL),
(83, 'vendor', 50, '38897655_2102695313097539_9220072664452825088_n.jpg', 'http://localhost:8000/storage/images/158728844738897655_2102695313097539_9220072664452825088_n.jpg', NULL, NULL, NULL),
(84, 'vendor', 50, 'a1.PNG', 'http://localhost:8000/storage/images/1587288447a1.PNG', NULL, NULL, NULL),
(85, 'service', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(86, 'service', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(87, 'service', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(88, 'vendor', 51, 'lpp.jpg', 'http://localhost:8000/storage/images/1587297759lpp.jpg', NULL, NULL, NULL),
(90, 'vendor', 51, '38897655_2102695313097539_9220072664452825088_n.jpg', 'http://localhost:8000/storage/images/158729780338897655_2102695313097539_9220072664452825088_n.jpg', NULL, NULL, NULL),
(91, 'vendor', 51, 'a1.PNG', 'http://localhost:8000/storage/images/1587297803a1.PNG', NULL, NULL, NULL),
(92, 'inventory', 22, 'Untitled.png', 'http://localhost:8000/storage/images/1587299004Untitled.png', NULL, NULL, NULL),
(94, 'inventory', 22, 'lpp.jpg', 'http://localhost:8000/storage/images/1587299004lpp.jpg', NULL, NULL, NULL),
(97, 'inventory_unit', 174, 'a1.PNG', 'http://localhost:8000/storage/images/1587299004a1.PNG', NULL, NULL, NULL),
(100, 'inventory_unit', 174, 'Capture25.PNG', 'http://localhost:8000/storage/images/1587299344Capture25.PNG', NULL, NULL, NULL),
(101, 'service', 11, 'rental_sign_1597759319_11.png', 'http://localhost:8000/storage/files/rental_sign_1597759319_11.png', NULL, NULL, '2020-08-18 08:01:59'),
(102, 'service', 11, 'rental_sign_1597759319_11.png', 'http://localhost:8000/storage/files/rental_sign_1597759319_11.png', NULL, NULL, '2020-08-18 08:01:59'),
(103, 'service', 11, 'rental_sign_1597759319_11.png', 'http://localhost:8000/storage/files/rental_sign_1597759319_11.png', NULL, NULL, '2020-08-18 08:01:59'),
(104, 'service', 11, 'rental_sign_1597759319_11.png', 'http://localhost:8000/storage/files/rental_sign_1597759319_11.png', NULL, NULL, '2020-08-18 08:01:59'),
(105, 'vendor', 52, 'a2.PNG', 'http://localhost:8000/storage/images/1587369965a2.PNG', NULL, NULL, NULL),
(106, 'vendor', 52, 'a1.PNG', 'http://localhost:8000/storage/images/1587369965a1.PNG', NULL, NULL, NULL),
(107, 'vendor', 52, '38897655_2102695313097539_9220072664452825088_n.jpg', 'http://localhost:8000/storage/images/158736996538897655_2102695313097539_9220072664452825088_n.jpg', NULL, NULL, NULL),
(108, 'inventory', 22, 'a2.PNG', 'http://localhost:8000/storage/images/1587372837a2.PNG', NULL, NULL, NULL),
(110, 'inventory_unit', 174, 'Capture26.PNG', 'http://localhost:8000/storage/images/1587372837Capture26.PNG', NULL, NULL, NULL),
(111, 'inventory', 13, 'a2.PNG', 'http://localhost:8000/storage/images/1587383256a2.PNG', NULL, NULL, NULL),
(112, 'inventory', 13, 'a1.PNG', 'http://localhost:8000/storage/images/1587383256a1.PNG', NULL, NULL, NULL),
(114, 'inventory_unit', 175, 'screenshot.png', 'http://localhost:8000/storage/images/1587383256screenshot.png', NULL, NULL, NULL),
(115, 'inventory_unit', 175, '38897655_2102695313097539_9220072664452825088_n.jpg', 'http://localhost:8000/storage/images/158738330538897655_2102695313097539_9220072664452825088_n.jpg', NULL, NULL, NULL),
(116, 'service', 12, 'rental_sign_1597766707_12.png', 'http://localhost:8000/storage/files/rental_sign_1597766707_12.png', NULL, NULL, '2020-08-18 10:05:07'),
(117, 'service', 12, 'rental_sign_1597766707_12.png', 'http://localhost:8000/storage/files/rental_sign_1597766707_12.png', NULL, NULL, '2020-08-18 10:05:07'),
(118, 'inventory', 22, 'a1.PNG', 'http://localhost:8000/storage/images/1587387274a1.PNG', NULL, NULL, NULL),
(119, 'vendor', 53, 'Screenshot (2).png', 'http://localhost:8000/storage/images/1587645926Screenshot (2).png', NULL, NULL, NULL),
(120, 'vendor', 53, 'Untitled.png', 'http://localhost:8000/storage/images/1587645926Untitled.png', NULL, NULL, NULL),
(121, 'vendor', 53, 'a2.PNG', 'http://localhost:8000/storage/images/1587645926a2.PNG', NULL, NULL, NULL),
(122, 'vendor', 53, 'a1.PNG', 'http://localhost:8000/storage/images/1587645926a1.PNG', NULL, NULL, NULL),
(123, 'vendor', 53, 'Screenshot (5).png', 'http://localhost:8000/storage/images/1587645926Screenshot (5).png', NULL, NULL, NULL),
(126, 'vendor', 54, 'a2.PNG', 'http://localhost:8000/storage/images/1587647282a2.PNG', NULL, NULL, NULL),
(127, 'vendor', 54, 'a1.PNG', 'http://localhost:8000/storage/images/1587647282a1.PNG', NULL, NULL, NULL),
(128, 'vendor', 54, 'Screenshot (4).png', 'http://localhost:8000/storage/images/1587647282Screenshot (4).png', NULL, NULL, NULL),
(129, 'vendor', 55, 'a1.PNG', 'http://localhost:8000/storage/images/1587800515a1.PNG', NULL, NULL, NULL),
(130, 'vendor', 55, 'Screenshot(7).png', 'http://localhost:8000/storage/images/1587800515Screenshot(7).png', NULL, NULL, NULL),
(131, 'vendor', 55, 'Screensnew.png', 'http://localhost:8000/storage/images/1587800515Screensnew.png', NULL, NULL, NULL),
(132, 'vendor', 56, 'a2.PNG', 'http://localhost:8000/storage/images/1587801954a2.PNG', NULL, NULL, NULL),
(133, 'vendor', 56, 'a1.PNG', 'http://localhost:8000/storage/images/1587801954a1.PNG', NULL, NULL, NULL),
(134, 'vendor', 59, 'a2.PNG', 'http://localhost:8000/storage/images/1587803542a2.PNG', NULL, NULL, NULL),
(135, 'vendor', 59, 'a1.PNG', 'http://localhost:8000/storage/images/1587803542a1.PNG', NULL, NULL, NULL),
(136, 'vendor', 59, 'Screenshot.png', 'http://localhost:8000/storage/images/1587803542Screenshot.png', NULL, NULL, NULL),
(137, 'vendor', 59, 'Screenshot.png', 'http://localhost:8000/storage/images/1587803542Screenshot.png', NULL, NULL, NULL),
(138, 'vendor', 59, 'Screensnew.png', 'http://localhost:8000/storage/images/1587803542Screensnew.png', NULL, NULL, NULL),
(139, 'inventory', 25, 'screenshot.png', 'http://localhost:8000/storage/images/1587804015screenshot.png', NULL, NULL, NULL),
(140, 'inventory', 25, 'Screenshot.png', 'http://localhost:8000/storage/images/1587804015Screenshot.png', NULL, NULL, NULL),
(141, 'inventory', 25, 'Screenshot.png', 'http://localhost:8000/storage/images/1587804015Screenshot.png', NULL, NULL, NULL),
(142, 'inventory', 25, 'Screensnew.png', 'http://localhost:8000/storage/images/1587804015Screensnew.png', NULL, NULL, NULL),
(143, 'inventory_unit', 176, 'a1.PNG', 'http://localhost:8000/storage/images/1587804015a1.PNG', NULL, NULL, NULL),
(147, 'inventory_unit', 176, 'Screenshot.png', 'http://localhost:8000/storage/images/1587804208Screenshot.png', NULL, NULL, NULL),
(148, 'inventory_unit', 176, 'Screensnew.png', 'http://localhost:8000/storage/images/1587804208Screensnew.png', NULL, NULL, NULL),
(149, 'inventory_unit', 176, 'screenshot.png', 'http://localhost:8000/storage/images/1587804232screenshot.png', NULL, NULL, NULL),
(150, 'inventory', 19, 'screenshot.png', 'http://localhost:8000/storage/images/1587804368screenshot.png', NULL, NULL, NULL),
(151, 'inventory', 19, 'Screenshot.png', 'http://localhost:8000/storage/images/1587804368Screenshot.png', NULL, NULL, NULL),
(152, 'inventory', 19, 'Screensnew.png', 'http://localhost:8000/storage/images/1587804368Screensnew.png', NULL, NULL, NULL),
(162, 'inventory_unit', 177, 'screenshot.png', 'http://localhost:8000/storage/images/15878054641screenshot.png', NULL, NULL, NULL),
(163, 'inventory_unit', 177, '38897655_2102695313097539_9220072664452825088_n.jpg', 'http://localhost:8000/storage/images/1587805464238897655_2102695313097539_9220072664452825088_n.jpg', NULL, NULL, NULL),
(164, 'inventory_unit', 177, 'Screenshot.png', 'http://localhost:8000/storage/images/15878054643Screenshot.png', NULL, NULL, NULL),
(165, 'inventory_unit', 177, 'Screenshot.png', 'http://localhost:8000/storage/images/15878054644Screenshot.png', NULL, NULL, NULL),
(166, 'inventory_unit', 177, 'Screensnew.png', 'http://localhost:8000/storage/images/15878054645Screensnew.png', NULL, NULL, NULL),
(168, 'vendor', 60, 'Screenshot.png', 'http://localhost:8000/storage/images/15878058242Screenshot.png', NULL, NULL, NULL),
(169, 'vendor', 60, 'Screenshot.png', 'http://localhost:8000/storage/images/15878058243Screenshot.png', NULL, NULL, NULL),
(170, 'vendor', 60, 'Screensnew.png', 'http://localhost:8000/storage/images/15878058244Screensnew.png', NULL, NULL, NULL),
(171, 'vendor', 61, 'lpp.jpg', 'http://localhost:8000/storage/images/15878835261lpp.jpg', NULL, NULL, NULL),
(172, 'vendor', 61, 'screenshot.png', 'http://localhost:8000/storage/images/15878835262screenshot.png', NULL, NULL, NULL),
(173, 'vendor', 61, 'Untitled.png', 'http://localhost:8000/storage/images/15878835263Untitled.png', NULL, NULL, NULL),
(176, 'vendor', 62, 'Screenshot (2).png', 'http://localhost:8000/storage/images/15878963613Screenshot (2).png', NULL, NULL, NULL),
(178, 'vendor', 62, 'Untitled4.png', 'http://localhost:8000/storage/images/15878975382Untitled4.png', NULL, NULL, NULL),
(179, 'vendor', 62, 'lpp.jpg', 'http://localhost:8000/storage/images/15878983651lpp.jpg', NULL, NULL, NULL),
(180, 'vendor', 62, 'screenshot.png', 'http://localhost:8000/storage/images/15878983652screenshot.png', NULL, NULL, NULL),
(182, 'vendor', 62, 'Untitled1.png', 'http://localhost:8000/storage/images/15878998651Untitled1.png', NULL, NULL, NULL),
(183, 'vendor', 62, 'Untitled3.png', 'http://localhost:8000/storage/images/15878998652Untitled3.png', NULL, NULL, NULL),
(184, 'inventory', 150, 'Untitled.png', 'http://localhost:8000/storage/images/15879041551Untitled.png', NULL, NULL, NULL),
(187, 'inventory_unit', 178, 'Untitled1.png', 'http://localhost:8000/storage/images/15879041551Untitled1.png', NULL, NULL, NULL),
(188, 'inventory_unit', 178, 'Untitled4.png', 'http://localhost:8000/storage/images/15879041552Untitled4.png', NULL, NULL, NULL),
(191, 'inventory_unit', 178, 'Screenshot (3).png', 'http://localhost:8000/storage/images/15879051621Screenshot (3).png', NULL, NULL, NULL),
(195, 'inventory_unit', 178, 'Screenshot (7).png', 'http://localhost:8000/storage/images/15879052961Screenshot (7).png', NULL, NULL, NULL),
(196, 'inventory_unit', 178, 'Screenshot (2).png', 'http://localhost:8000/storage/images/15879052962Screenshot (2).png', NULL, NULL, NULL),
(197, 'service', 13, '38897655_2102695313097539_9220072664452825088_n.jpg', 'http://localhost:8000/storage/images/1587905829138897655_2102695313097539_9220072664452825088_n.jpg', NULL, NULL, NULL),
(198, 'service', 13, 'Screenshot (2).png', 'http://localhost:8000/storage/images/15879058292Screenshot (2).png', NULL, NULL, NULL),
(201, 'service', 13, 'Untitled4.png', 'http://localhost:8000/storage/images/15879059621Untitled4.png', NULL, NULL, NULL),
(202, 'service', 13, 'Untitled.png', 'http://localhost:8000/storage/images/15879059622Untitled.png', NULL, NULL, NULL),
(203, 'inventory_unit', 179, 'Screenshot (3).png', 'http://localhost:8000/storage/images/15879095921Screenshot (3).png', NULL, NULL, NULL),
(204, 'inventory_unit', 179, 'Screens new.png', 'http://localhost:8000/storage/images/15879095922Screens new.png', NULL, NULL, NULL),
(208, 'inventory_unit', 180, 'Untitled.png', 'http://localhost:8000/storage/images/15879115351Untitled.png', NULL, NULL, NULL),
(209, 'inventory_unit', 180, 'screenshot.png', 'http://localhost:8000/storage/images/15879115352screenshot.png', NULL, NULL, NULL),
(210, 'inventory_unit', 180, 'lpp.jpg', 'http://localhost:8000/storage/images/15879115353lpp.jpg', NULL, NULL, NULL),
(212, 'inventory', 102, 'Untitled.png', 'http://localhost:8000/storage/images/15879619351Untitled.png', NULL, NULL, NULL),
(214, 'inventory', 102, 'lpp.jpg', 'http://localhost:8000/storage/images/15879619353lpp.jpg', NULL, NULL, NULL),
(216, 'inventory_unit', 181, 'Untitled2.png', 'http://localhost:8000/storage/images/15879619352Untitled2.png', NULL, NULL, NULL),
(217, 'inventory', 102, 'Screenshot (2).png', 'http://localhost:8000/storage/images/15879620801Screenshot (2).png', NULL, NULL, NULL),
(218, 'inventory_unit', 181, 'Screenshot (2).png', 'http://localhost:8000/storage/images/15879620801Screenshot (2).png', NULL, NULL, NULL),
(219, 'inventory_unit', 181, 'Screens new.png', 'http://localhost:8000/storage/images/15879620802Screens new.png', NULL, NULL, NULL),
(220, 'inventory_unit', 102, 'Untitled4.png', 'http://localhost:8000/storage/images/15879640521Untitled4.png', NULL, NULL, NULL),
(221, 'inventory_unit', 102, 'screenshot.png', 'http://localhost:8000/storage/images/15879640522screenshot.png', NULL, NULL, NULL),
(222, 'vendor', 63, 'Screenshot(4).png', 'http://localhost:8000/storage/images/15879648871Screenshot(4).png', NULL, NULL, NULL),
(223, 'vendor', 63, 'Screens new.png', 'http://localhost:8000/storage/images/15879648872Screens new.png', NULL, NULL, NULL),
(224, 'inventory', 151, 'lpp.jpg', 'http://localhost:8000/storage/images/15880720651lpp.jpg', NULL, NULL, NULL),
(228, 'inventory', 151, 'Untitled.png', 'http://localhost:8000/storage/images/15880721131Untitled.png', NULL, NULL, NULL),
(229, 'inventory', 151, 'Untitled4.png', 'http://localhost:8000/storage/images/15880731011Untitled4.png', NULL, NULL, NULL),
(230, 'inventory_unit', 182, 'Screenshot (3).png', 'http://localhost:8000/storage/images/15880731011Screenshot (3).png', NULL, NULL, NULL),
(231, 'inventory_unit', 182, 'Untitled1.png', 'http://localhost:8000/storage/images/15880731012Untitled1.png', NULL, NULL, NULL),
(232, 'vendor', 64, 'Screens new.png', 'http://localhost:8000/storage/images/15880731881Screens new.png', NULL, NULL, NULL),
(234, 'vendor', 64, 'Screenshot (3) - Copy.png', 'http://localhost:8000/storage/images/15880731893Screenshot (3) - Copy.png', NULL, NULL, NULL),
(235, 'vendor', 64, 'screenshot.png', 'http://localhost:8000/storage/images/15880732201screenshot.png', NULL, NULL, NULL),
(236, 'vendor', 64, 'lpp.jpg', 'http://localhost:8000/storage/images/15880732202lpp.jpg', NULL, NULL, NULL),
(237, 'service', 14, 'screenshot.png', 'http://localhost:8000/storage/images/15880733791screenshot.png', NULL, NULL, NULL),
(238, 'service', 14, 'lpp.jpg', 'http://localhost:8000/storage/images/15880733792lpp.jpg', NULL, NULL, NULL),
(239, 'inventory_unit', 183, 'screenshot.png', 'http://localhost:8000/storage/images/15880960771screenshot.png', NULL, NULL, NULL),
(241, 'inventory', 150, 'screenshot.png', 'http://localhost:8000/storage/images/15880961801screenshot.png', NULL, NULL, NULL),
(242, 'inventory_unit', 183, 'Untitled4.png', 'http://localhost:8000/storage/images/15880961801Untitled4.png', NULL, NULL, NULL),
(243, 'vendor', 65, 'screenshot.png', 'http://localhost:8000/storage/images/15882249331screenshot.png', NULL, NULL, NULL),
(244, 'vendor', 65, 'lpp.jpg', 'http://localhost:8000/storage/images/15882249332lpp.jpg', NULL, NULL, NULL),
(245, 'vendor', 27, 'screenshot.png', 'http://localhost:8000/storage/images/15884986221screenshot.png', NULL, NULL, NULL),
(246, 'vendor', 27, 'lpp.jpg', 'http://localhost:8000/storage/images/15884986222lpp.jpg', NULL, NULL, NULL),
(248, 'vendor', 32, 'Untitled.png', 'http://localhost:8000/storage/images/15884990222Untitled.png', NULL, NULL, NULL),
(250, 'vendor', 32, 'lpp.jpg', 'http://localhost:8000/storage/images/15885074751lpp.jpg', NULL, NULL, NULL),
(251, 'vendor', 32, 'screenshot.png', 'http://localhost:8000/storage/images/15885077681screenshot.png', NULL, NULL, NULL),
(252, 'vendor', 33, 'Untitled.png', 'http://localhost:8000/storage/images/15885101051Untitled.png', NULL, NULL, NULL),
(253, 'vendor', 33, 'screenshot.png', 'http://localhost:8000/storage/images/15885101052screenshot.png', NULL, NULL, NULL),
(254, 'vendor', 41, 'Untitled.png', 'http://localhost:8000/storage/images/15886483441Untitled.png', NULL, NULL, NULL),
(255, 'vendor', 41, 'screenshot.png', 'http://localhost:8000/storage/images/15886483442screenshot.png', NULL, NULL, NULL),
(256, 'vendor', 43, 'Untitled4.png', 'http://localhost:8000/storage/images/15886795031Untitled4.png', NULL, NULL, NULL),
(258, 'vendor', 44, 'Untitled.png', 'http://localhost:8000/storage/images/15886795751Untitled.png', NULL, NULL, NULL),
(259, 'vendor', 44, 'screenshot.png', 'http://localhost:8000/storage/images/15886795752screenshot.png', NULL, NULL, NULL),
(260, 'vendor', 45, 'Untitled.png', 'http://localhost:8000/storage/images/15886798201Untitled.png', NULL, NULL, NULL),
(261, 'vendor', 45, 'lpp.jpg', 'http://localhost:8000/storage/images/15886798202lpp.jpg', NULL, NULL, NULL),
(262, 'vendor', 46, 'Untitled.png', 'http://localhost:8000/storage/images/15886798961Untitled.png', NULL, NULL, NULL),
(263, 'vendor', 46, 'screenshot.png', 'http://localhost:8000/storage/images/15886798962screenshot.png', NULL, NULL, NULL),
(264, 'vendor', 47, 'Untitled.png', 'http://localhost:8000/storage/images/15886799351Untitled.png', NULL, NULL, NULL),
(265, 'vendor', 47, 'screenshot.png', 'http://localhost:8000/storage/images/15886799352screenshot.png', NULL, NULL, NULL),
(266, 'vendor', 48, 'screenshot.png', 'http://localhost:8000/storage/images/15886802031screenshot.png', NULL, NULL, NULL),
(267, 'vendor', 48, 'lpp.jpg', 'http://localhost:8000/storage/images/15886802032lpp.jpg', NULL, NULL, NULL),
(268, 'vendor', 56, 'Screenshot (3) - Copy.png', 'http://localhost:8000/storage/images/15886822221Screenshot (3) - Copy.png', NULL, NULL, NULL),
(269, 'vendor', 56, 'Screenshot (1).png', 'http://localhost:8000/storage/images/15886822282Screenshot (1).png', NULL, NULL, NULL),
(270, 'vendor', 56, 'Screens new.png', 'http://localhost:8000/storage/images/15886822333Screens new.png', NULL, NULL, NULL),
(271, 'vendor', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(274, 'vendor', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(275, 'vendor', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(276, 'vendor', 5, 'customer_1598681813_5.png', 'http://localhost:8000/storage/files/customer_1598681813_5.png', NULL, NULL, '2020-08-29 00:16:53'),
(277, 'vendor', 5, 'customer_1598681813_5.png', 'http://localhost:8000/storage/files/customer_1598681813_5.png', NULL, NULL, '2020-08-29 00:16:53'),
(278, 'inventory', 10, 'Clipboard - June 4, 2020 7_32 PM.png', 'http://localhost:8000/storage/images/15915303051Clipboard - June 4, 2020 7_32 PM.png', NULL, NULL, NULL),
(279, 'driver-insurance', 3, 'Change_Request_Form_-_Vehicle.pdf', 'https://evostage.wikimesh.com/storage/files/15930762671Change_Request_Form_-_Vehicle.pdf', NULL, NULL, NULL),
(280, 'driver-insurance', 1, 'Change_Request_Form_-_Vehicle.pdf', 'https://evostage.wikimesh.com/storage/files/15931012821Change_Request_Form_-_Vehicle.pdf', NULL, NULL, NULL),
(281, 'driver-insurance', 1, 'MVR- MOSES CHEGE.pdf', 'https://evostage.wikimesh.com/storage/files/15934534341MVR- MOSES CHEGE.pdf', NULL, NULL, NULL),
(282, 'driver-insurance', 2, 'MVR- BOBBY AGUERO.pdf', 'https://evostage.wikimesh.com/storage/files/15934537711MVR- BOBBY AGUERO.pdf', NULL, NULL, NULL),
(283, 'driver-insurance', 3, 'MVR- ALEJANDRO MENDOZA.pdf', 'https://evostage.wikimesh.com/storage/files/15934539761MVR- ALEJANDRO MENDOZA.pdf', NULL, NULL, NULL),
(284, 'driver-insurance', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(286, 'driver-insurance', 5, 'customer_1598681813_5.png', 'http://localhost:8000/storage/files/customer_1598681813_5.png', NULL, NULL, '2020-08-29 00:16:53'),
(287, 'driver-insurance', 6, 'owner_sign_1597766467_6.png', 'http://localhost:8000/storage/files/owner_sign_1597766467_6.png', NULL, NULL, '2020-08-18 10:01:07'),
(288, 'inventory_unit', 190, 'brandon-morgan-3qucB7U2l7I-unsplash.jpg', 'https://evostage.wikimesh.com/storage/images/15939467911brandon-morgan-3qucB7U2l7I-unsplash.jpg', NULL, NULL, NULL),
(289, 'driver-insurance', 7, 'MVR- Chance Sherrod.pdf', 'https://evostage.wikimesh.com/storage/files/15941284661MVR- Chance Sherrod.pdf', NULL, NULL, NULL),
(290, 'driver-insurance', 8, 'Change_Request_Form_-_Driver.pdf', 'http://localhost:8000/storage/files/15952658391Change_Request_Form_-_Driver.pdf', NULL, NULL, NULL),
(325, 'subcontractor_agreement_w9', 1, '15964336241Subcontractor_Agreement_ (1).docx', 'http://localhost:8000/storage/files/1596956754115964336241Subcontractor_Agreement_ (1).docx', NULL, NULL, NULL),
(327, 'subcontractor_insurance_certificate', 1, 'Truck_Driver_Application.docx', 'http://localhost:8000/storage/files/15969567541Truck_Driver_Application.docx', NULL, NULL, NULL),
(329, 'subcontractor_agreement_w9', 3, 'equipment_lease_agreement (1).docx', 'http://localhost:8000/storage/files/15970582901equipment_lease_agreement (1).docx', NULL, NULL, NULL),
(330, 'subcontractor_insurance_certificate', 3, '15963832841equipment_lease_agreement.docx', 'http://localhost:8000/storage/files/1597058290115963832841equipment_lease_agreement.docx', NULL, NULL, NULL),
(331, 'subcontractor_agreement_w9', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(332, 'subcontractor_agreement_w9', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(335, 'subcontractor_agreement_w9', 5, 'customer_1598681813_5.png', 'http://localhost:8000/storage/files/customer_1598681813_5.png', NULL, NULL, '2020-08-29 00:16:53'),
(336, 'subcontractor_insurance_certificate', 5, 'customer_1598681813_5.png', 'http://localhost:8000/storage/files/customer_1598681813_5.png', NULL, NULL, '2020-08-29 00:16:53'),
(339, 'rental_agreement_w9', 3, '15970582901equipment_lease_agreement (1).docx', 'http://localhost:8000/storage/files/1597147843115970582901equipment_lease_agreement (1).docx', NULL, NULL, NULL),
(340, 'rental_insurance_certificate', 3, 'equipment_lease_agreement (1).docx', 'http://localhost:8000/storage/files/15971478431equipment_lease_agreement (1).docx', NULL, NULL, NULL),
(343, 'rental_agreement_w9', 2, 'equipment_lease_agreement (1).docx', 'http://localhost:8000/storage/files/15971513131equipment_lease_agreement (1).docx', NULL, NULL, NULL),
(344, 'rental_agreement_w9', 2, '15970582901equipment_lease_agreement (1).docx', 'http://localhost:8000/storage/files/1597151313215970582901equipment_lease_agreement (1).docx', NULL, NULL, NULL),
(345, 'rental_insurance_certificate', 2, 'equipment_lease_agreement (1).docx', 'http://localhost:8000/storage/files/15971513131equipment_lease_agreement (1).docx', NULL, NULL, NULL),
(351, 'subcontractor_agreement_w9', 9, 'Truck_Driver_Application.docx', 'http://localhost:8000/storage/files/15971528791Truck_Driver_Application.docx', NULL, NULL, NULL),
(352, 'subcontractor_insurance_certificate', 9, '15971519912equipment_lease_agreement (1) (1).docx', 'http://localhost:8000/storage/files/1597152879115971519912equipment_lease_agreement (1) (1).docx', NULL, NULL, NULL),
(356, 'subcontractor_agreement_w9', 10, '15971519912equipment_lease_agreement (1).docx', 'http://localhost:8000/storage/files/1597156448115971519912equipment_lease_agreement (1).docx', NULL, NULL, NULL),
(357, 'subcontractor_insurance_certificate', 10, '1597151991115964336241Subcontractor_Agreement_ (1) (1).docx', 'http://localhost:8000/storage/files/159715644811597151991115964336241Subcontractor_Agreement_ (1) (1).docx', NULL, NULL, NULL),
(361, 'rental_agreement_w9', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(362, 'rental_insurance_certificate', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(365, 'subcontractor_insurance_certificate', 11, 'rental_sign_1597759319_11.png', 'http://localhost:8000/storage/files/rental_sign_1597759319_11.png', NULL, NULL, '2020-08-18 08:01:59'),
(366, 'subcontractor_agreement_w9', 11, 'rental_sign_1597759319_11.png', 'http://localhost:8000/storage/files/rental_sign_1597759319_11.png', NULL, NULL, '2020-08-18 08:01:59'),
(367, 'inventory', 1, 'kitten-small.png', 'http://localhost:8000/storage/images/15973035551kitten-small.png', NULL, NULL, NULL),
(368, 'inventory_unit', 1, 'brandon-morgan-3qucB7U2l7I-unsplash.jpg', 'http://localhost:8000/storage/images/15973035551brandon-morgan-3qucB7U2l7I-unsplash.jpg', NULL, NULL, NULL),
(369, 'inventory_unit', 1, 'kitten-small.png', 'http://localhost:8000/storage/images/15973035552kitten-small.png', NULL, NULL, NULL),
(370, 'inventory_unit', 2, 'vetted_law_updated (1).jpg', 'http://localhost:8000/storage/images/15973036951vetted_law_updated (1).jpg', NULL, NULL, NULL),
(371, 'rental_agreement_w9', 9, '15971519912equipment_lease_agreement (1) (1).docx', 'http://localhost:8000/storage/files/1597319153115971519912equipment_lease_agreement (1) (1).docx', NULL, NULL, NULL),
(372, 'rental_insurance_certificate', 9, '15970582901equipment_lease_agreement (1).docx', 'http://localhost:8000/storage/files/1597319153115970582901equipment_lease_agreement (1).docx', NULL, NULL, NULL),
(377, 'subcontractor_agreement_w9', 14, 'Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', 'http://localhost:8000/storage/files/15976620711Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', NULL, NULL, NULL),
(378, 'subcontractor_insurance_certificate', 14, '15971519912equipment_lease_agreement (1).docx', 'http://localhost:8000/storage/files/1597662071115971519912equipment_lease_agreement (1).docx', NULL, NULL, NULL),
(379, 'sc_sign', 14, 'sc_sign_1597662071_14', 'http://localhost:8000/storage/files/sc_sign_1597662071_14.png', NULL, '2020-08-17 05:01:11', '2020-08-17 05:01:11'),
(386, 'owner_w9', 1, '15971519912equipment_lease_agreement (1).docx', 'http://localhost:8000/storage/files/1597735548115971519912equipment_lease_agreement (1).docx', NULL, NULL, NULL),
(387, 'owner_w9', 1, '1597151991115964336241Subcontractor_Agreement_ (1) (1).docx', 'http://localhost:8000/storage/files/159773554821597151991115964336241Subcontractor_Agreement_ (1) (1).docx', NULL, NULL, NULL),
(388, 'owner_operator_comprehensive', 1, '15971519912equipment_lease_agreement (1) (1).docx', 'http://localhost:8000/storage/files/1597735548115971519912equipment_lease_agreement (1) (1).docx', NULL, NULL, NULL),
(389, 'owner_annual_insp_cer', 1, 'Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', 'http://localhost:8000/storage/files/15977355481Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', NULL, NULL, NULL),
(390, 'owner_vehicle_reg', 1, '1597151991115964336241Subcontractor_Agreement_ (1) (1).docx', 'http://localhost:8000/storage/files/159773554811597151991115964336241Subcontractor_Agreement_ (1) (1).docx', NULL, NULL, NULL),
(391, 'owner_vehicle_use_form', 1, '15971519912equipment_lease_agreement (1).docx', 'http://localhost:8000/storage/files/1597735548115971519912equipment_lease_agreement (1).docx', NULL, NULL, NULL),
(392, 'owner_sign', 1, 'owner_sign_1597735549_1', 'http://localhost:8000/storage/files/owner_sign_1597735549_1.png', NULL, '2020-08-18 01:25:49', '2020-08-18 01:25:49'),
(393, 'owner_sign', 3, 'owner_sign_1597743697_3.png', 'http://localhost:8000/storage/files/owner_sign_1597743697_3.png', NULL, '2020-08-18 03:41:37', '2020-08-18 03:41:37'),
(394, 'owner_w9', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(395, 'owner_operator_comprehensive', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(396, 'owner_annual_insp_cer', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(397, 'owner_vehicle_reg', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(398, 'owner_vehicle_use_form', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(399, 'owner_sign', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, '2020-08-18 07:18:45', '2020-08-18 07:30:57'),
(400, 'owner_w9', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(401, 'owner_operator_comprehensive', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(402, 'owner_annual_insp_cer', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(403, 'owner_vehicle_reg', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(404, 'owner_vehicle_use_form', 4, 'owner_sign_1597757457_4.png', 'http://localhost:8000/storage/files/owner_sign_1597757457_4.png', NULL, NULL, '2020-08-18 07:30:57'),
(405, 'subcontractor_agreement_w9', 17, 'sc_sign_1597758260_17.png', 'http://localhost:8000/storage/files/sc_sign_1597758260_17.png', NULL, NULL, '2020-08-18 07:44:20'),
(406, 'subcontractor_insurance_certificate', 17, 'sc_sign_1597758260_17.png', 'http://localhost:8000/storage/files/sc_sign_1597758260_17.png', NULL, NULL, '2020-08-18 07:44:20'),
(407, 'sc_sign', 17, 'sc_sign_1597758260_17.png', 'http://localhost:8000/storage/files/sc_sign_1597758260_17.png', NULL, '2020-08-18 07:36:24', '2020-08-18 07:44:20'),
(414, 'rental_agreement_w9', 11, 'rental_sign_1597759319_11.png', 'http://localhost:8000/storage/files/rental_sign_1597759319_11.png', NULL, NULL, '2020-08-18 08:01:59'),
(415, 'rental_insurance_certificate', 11, 'rental_sign_1597759319_11.png', 'http://localhost:8000/storage/files/rental_sign_1597759319_11.png', NULL, NULL, '2020-08-18 08:01:59'),
(416, 'rental_sign', 11, 'rental_sign_1597759319_11.png', 'http://localhost:8000/storage/files/rental_sign_1597759319_11.png', NULL, '2020-08-18 07:57:32', '2020-08-18 08:01:59'),
(417, 'owner_w9', 6, 'owner_sign_1597766467_6.png', 'http://localhost:8000/storage/files/owner_sign_1597766467_6.png', NULL, NULL, '2020-08-18 10:01:07'),
(418, 'owner_operator_comprehensive', 6, 'owner_sign_1597766467_6.png', 'http://localhost:8000/storage/files/owner_sign_1597766467_6.png', NULL, NULL, '2020-08-18 10:01:07'),
(419, 'owner_annual_insp_cer', 6, 'owner_sign_1597766467_6.png', 'http://localhost:8000/storage/files/owner_sign_1597766467_6.png', NULL, NULL, '2020-08-18 10:01:07'),
(420, 'owner_vehicle_reg', 6, 'owner_sign_1597766467_6.png', 'http://localhost:8000/storage/files/owner_sign_1597766467_6.png', NULL, NULL, '2020-08-18 10:01:07'),
(422, 'owner_sign', 6, 'owner_sign_1597766467_6.png', 'http://localhost:8000/storage/files/owner_sign_1597766467_6.png', NULL, '2020-08-18 09:59:29', '2020-08-18 10:01:07'),
(423, 'owner_vehicle_use_form', 6, 'owner_sign_1597766467_6.png', 'http://localhost:8000/storage/files/owner_sign_1597766467_6.png', NULL, NULL, '2020-08-18 10:01:07'),
(424, 'rental_agreement_w9', 12, 'rental_sign_1597766707_12.png', 'http://localhost:8000/storage/files/rental_sign_1597766707_12.png', NULL, NULL, '2020-08-18 10:05:07'),
(425, 'rental_insurance_certificate', 12, 'rental_sign_1597766707_12.png', 'http://localhost:8000/storage/files/rental_sign_1597766707_12.png', NULL, NULL, '2020-08-18 10:05:07'),
(426, 'rental_sign', 12, 'rental_sign_1597766707_12.png', 'http://localhost:8000/storage/files/rental_sign_1597766707_12.png', NULL, '2020-08-18 10:04:02', '2020-08-18 10:05:07'),
(427, 'subcontractor_agreement_w9', 20, '15971519912equipment_lease_agreement (1) (1).docx', 'http://localhost:8000/storage/files/1597845684115971519912equipment_lease_agreement (1) (1).docx', NULL, NULL, NULL),
(428, 'subcontractor_insurance_certificate', 20, '1597151991115964336241Subcontractor_Agreement_ (1) (1).docx', 'http://localhost:8000/storage/files/159784568411597151991115964336241Subcontractor_Agreement_ (1) (1).docx', NULL, NULL, NULL),
(429, 'sc_sign', 20, 'sc_sign_1597845684_20.png', 'http://localhost:8000/storage/files/sc_sign_1597845684_20.png', NULL, '2020-08-19 08:01:24', '2020-08-19 08:01:24'),
(430, 'rental_agreement_w9', 13, '15971519912equipment_lease_agreement (1) (1).docx', 'http://localhost:8000/storage/files/1597845793115971519912equipment_lease_agreement (1) (1).docx', NULL, NULL, NULL),
(431, 'rental_insurance_certificate', 13, '1597151991115964336241Subcontractor_Agreement_ (1) (1).docx', 'http://localhost:8000/storage/files/159784579311597151991115964336241Subcontractor_Agreement_ (1) (1).docx', NULL, NULL, NULL),
(432, 'rental_sign', 13, 'rental_sign_1597845793_13.png', 'http://localhost:8000/storage/files/rental_sign_1597845793_13.png', NULL, '2020-08-19 08:03:13', '2020-08-19 08:03:13'),
(433, 'owner_w9', 8, '1597151991115964336241Subcontractor_Agreement_ (1) (1).docx', 'http://localhost:8000/storage/files/159784594511597151991115964336241Subcontractor_Agreement_ (1) (1).docx', NULL, NULL, NULL),
(434, 'owner_operator_comprehensive', 8, 'Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', 'http://localhost:8000/storage/files/15978459451Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', NULL, NULL, NULL),
(436, 'owner_vehicle_reg', 8, '15971519912equipment_lease_agreement (1) (1).docx', 'http://localhost:8000/storage/files/1597845945115971519912equipment_lease_agreement (1) (1).docx', NULL, NULL, NULL),
(437, 'owner_sign', 8, 'owner_sign_1597845945_8', 'http://localhost:8000/storage/files/owner_sign_1597845945_8.png', NULL, '2020-08-19 08:05:45', '2020-08-19 08:05:45'),
(438, 'owner_annual_insp_cer', 8, '15971519912equipment_lease_agreement (1).docx', 'http://localhost:8000/storage/files/1597846034115971519912equipment_lease_agreement (1).docx', NULL, NULL, NULL),
(439, 'subcontractor_agreement_w9', 21, 'customer_1599633243_21.png', 'http://localhost:8000/storage/files/customer_1599633243_21.png', NULL, NULL, '2020-09-09 00:34:03'),
(440, 'subcontractor_insurance_certificate', 21, 'customer_1599633243_21.png', 'http://localhost:8000/storage/files/customer_1599633243_21.png', NULL, NULL, '2020-09-09 00:34:03'),
(441, 'sc_sign', 21, 'customer_1599633243_21.png', 'http://localhost:8000/storage/files/customer_1599633243_21.png', NULL, '2020-08-19 08:34:46', '2020-09-09 00:34:03'),
(442, 'owner_w9', 9, 'signature.png', 'http://localhost:8000/storage/files/15979338291signature.png', NULL, NULL, NULL),
(443, 'owner_operator_comprehensive', 9, 'Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', 'http://localhost:8000/storage/files/15979338301Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', NULL, NULL, NULL),
(444, 'owner_operator_comprehensive', 9, 'signature.png', 'http://localhost:8000/storage/files/15979338302signature.png', NULL, NULL, NULL),
(445, 'owner_annual_insp_cer', 9, 'Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', 'http://localhost:8000/storage/files/15979338301Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', NULL, NULL, NULL),
(446, 'owner_vehicle_reg', 9, '15971519912equipment_lease_agreement (1) (1).docx', 'http://localhost:8000/storage/files/1597933830115971519912equipment_lease_agreement (1) (1).docx', NULL, NULL, NULL),
(447, 'owner_vehicle_use_form', 9, 'Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', 'http://localhost:8000/storage/files/15979338301Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', NULL, NULL, NULL),
(448, 'owner_sign', 9, 'owner_sign_1597933830_9.png', 'http://localhost:8000/storage/files/owner_sign_1597933830_9.png', NULL, '2020-08-20 08:30:30', '2020-08-20 08:30:30'),
(449, 'owner_w9', 10, 'EV_Company_Credit_Application_-_EVOS.docx', 'http://localhost:8000/storage/files/15981854051EV_Company_Credit_Application_-_EVOS.docx', NULL, NULL, NULL),
(450, 'owner_operator_comprehensive', 10, 'EV_Company_Credit_Application_-_EVOS.docx', 'http://localhost:8000/storage/files/15981854051EV_Company_Credit_Application_-_EVOS.docx', NULL, NULL, NULL),
(451, 'owner_annual_insp_cer', 10, '1597151991115964336241Subcontractor_Agreement_ (1).docx', 'http://localhost:8000/storage/files/159818540611597151991115964336241Subcontractor_Agreement_ (1).docx', NULL, NULL, NULL),
(452, 'owner_vehicle_reg', 10, 'Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', 'http://localhost:8000/storage/files/15981854061Sub_Contractor_and_Rental_Agreement_Email_Copy.docx', NULL, NULL, NULL),
(453, 'owner_sign', 10, 'owner_sign_1598185406_10.png', 'http://localhost:8000/storage/files/owner_sign_1598185406_10.png', NULL, '2020-08-23 06:23:26', '2020-08-23 06:23:26'),
(454, 'customer_sign', 5, 'customer_1598681813_5.png', 'http://localhost:8000/storage/files/customer_1598681813_5.png', NULL, '2020-08-28 21:55:34', '2020-08-29 00:16:53'),
(455, 'customer_sign', 6, 'customer_1598701992_6.png', 'http://localhost:8000/storage/files/customer_1598701992_6.png', NULL, '2020-08-29 05:53:12', '2020-08-29 05:53:12'),
(456, 'customer_sign', 7, 'customer_1598706496_7.png', 'http://localhost:8000/storage/files/customer_1598706496_7.png', NULL, '2020-08-29 07:08:16', '2020-08-29 07:08:16'),
(457, 'customer_sign', 10, 'customer_1598866717_10.png', 'http://localhost:8000/storage/files/customer_1598866717_10.png', NULL, '2020-08-31 03:38:37', '2020-08-31 03:38:37'),
(458, 'customer_sign', 11, 'customer_1598882014_11.png', 'http://localhost:8000/storage/files/customer_1598882014_11.png', NULL, '2020-08-31 07:53:34', '2020-08-31 07:53:34'),
(459, 'customer_sign', 12, 'customer_1598888574_12.png', 'http://localhost:8000/storage/files/customer_1598888574_12.png', NULL, '2020-08-31 09:42:54', '2020-08-31 09:42:54'),
(460, 'inventory_unit', 4, 'signature.png', 'http://localhost:8000/storage/images/15989647321signature.png', NULL, NULL, NULL),
(462, 'inventory_unit', 4, 'kitten-small.png', 'http://localhost:8000/storage/images/15990280701kitten-small.png', NULL, NULL, NULL),
(463, 'inventory_unit', 6, 'brandon-morgan-3qucB7U2l7I-unsplash.jpg', 'http://localhost:8000/storage/images/15990315481brandon-morgan-3qucB7U2l7I-unsplash.jpg', NULL, NULL, NULL),
(464, 'inventory_unit', 6, 'kitten-small.png', 'http://localhost:8000/storage/images/15990315482kitten-small.png', NULL, NULL, NULL),
(465, 'inventory_unit', 7, 'logo_new.png', 'http://localhost:8000/storage/images/15990366531logo_new.png', NULL, NULL, NULL),
(466, 'inventory_unit', 7, 'Signature (1).jpg', 'http://localhost:8000/storage/images/15990366532Signature (1).jpg', NULL, NULL, NULL),
(467, 'inventory_unit', 2, 'Signature (1).jpg', 'http://localhost:8000/storage/images/15990441811Signature (1).jpg', NULL, NULL, NULL),
(468, 'inventory', 2, 'signature.png', 'http://localhost:8000/storage/images/15990455031signature.png', NULL, NULL, NULL),
(469, 'inventory_unit', 8, 'kitten-small.png', 'http://localhost:8000/storage/images/15990456461kitten-small.png', NULL, NULL, NULL),
(470, 'customer_sign', 21, 'customer_1599633243_21.png', 'http://localhost:8000/storage/files/customer_1599633243_21.png', NULL, '2020-09-09 00:31:59', '2020-09-09 00:34:03');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_driver_request`
--

CREATE TABLE `insurance_driver_request` (
  `id` int(11) NOT NULL,
  `request` varchar(191) DEFAULT NULL,
  `company_name` varchar(191) DEFAULT NULL,
  `date_of_change` varchar(191) DEFAULT NULL,
  `driver_full_name` varchar(191) DEFAULT NULL,
  `driver_first_name` varchar(191) DEFAULT NULL,
  `driver_last_name` varchar(191) DEFAULT NULL,
  `date_of_birth` varchar(191) DEFAULT NULL,
  `license_state` varchar(191) DEFAULT NULL,
  `license` varchar(191) DEFAULT NULL,
  `years_of_experience` varchar(191) DEFAULT NULL,
  `years_of_cdl_experience` varchar(191) DEFAULT NULL,
  `duties` varchar(191) DEFAULT NULL,
  `date_of_hire` varchar(191) DEFAULT NULL,
  `additional` text DEFAULT NULL,
  `submitted_by` varchar(191) DEFAULT NULL,
  `date` varchar(191) DEFAULT NULL,
  `send_mail_to` varchar(191) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `url_token` varchar(191) DEFAULT NULL,
  `token` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurance_driver_request`
--

INSERT INTO `insurance_driver_request` (`id`, `request`, `company_name`, `date_of_change`, `driver_full_name`, `driver_first_name`, `driver_last_name`, `date_of_birth`, `license_state`, `license`, `years_of_experience`, `years_of_cdl_experience`, `duties`, `date_of_hire`, `additional`, `submitted_by`, `date`, `send_mail_to`, `status`, `url_token`, `token`, `created_at`, `updated_at`) VALUES
(1, 'add', 'EV Oilfield Services', '06/29/2020', 'MOSES', 'MOSES', NULL, '12/12/1980', 'WASHINGTON', 'WDL14231853B', 'N/A', 'N/A', 'SUPERVISOR', '06/29/2020', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '06/29/2020', 'elizabeth@mcanallywilkins.com', 1, '1-YC1WBOK4DP50MZ72EJV3HARTSUL86N', '532884', '2020-06-29 15:57:14', '2020-06-30 15:15:04'),
(2, 'add', 'EV Oilfield Services', '06/29/2020', 'BOBBY AGUERO', 'BOBBY', 'AGUERO', '10/25/1995', 'TEXAS', '37923490', '2 ', '2', 'DRIVER', '06/29/2020', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '06/29/2020', 'elizabeth@mcanallywilkins.com', 1, '2-VCLPR13N9GIJHXBA6YKT7UO048ZEFM', '117378', '2020-06-29 16:02:51', '2020-06-30 14:12:33'),
(3, 'add', 'EV Oilfield Services', '06/29/2020', 'ALEJANDRO MENDOZA', 'ALEJANDRO', 'MENDOZA', '05/17/1959', 'TEXAS', '07804803', '6 ', '6', 'DRIVER', '06/29/2020', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '06/29/2020', 'elizabeth@mcanallywilkins.com', 1, '3-Y2GI7EO3BDRXJ10HZCFMWLNSK4VPQ5', '261727', '2020-06-29 16:06:16', '2020-06-30 13:32:30'),
(4, 'add', 'EV Oilfield Services', '06/29/2020', 'JORGE VARGAS', 'JORGE', 'VARGAS', '07/20/1976', 'NEW MEXICO', '100134632', 'N/A (NON CDL DRIVER)', 'N/A', 'SWAMPER', '06/06/2020', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '06/29/2020', 'elizabeth@mcanallywilkins.com', 1, '4-U3AKNXG069S24EOIHYBR8VQ5ZM1PFD', '626429', '2020-06-29 16:12:21', '2020-06-30 13:25:01'),
(6, 'delete', 'EV Oilfield Services', '06/29/2020', 'BOBBY AGUERO', 'BOBBY', 'AGUERO\r\n', '10/25/1995', 'TEXAS', '37923490', '2 ', '2', 'DRIVER', '06/29/2020', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '07/02/2020', 'elizabeth@mcanallywilkins.com', 1, '6-XZ6OC2KBEWP8N0HG7IS4RMYFTDJAQ5', '856057', '2020-07-02 13:05:04', '2020-07-02 14:08:18'),
(7, 'add', 'EV Oilfield Services', '07/07/2020', 'Chance Sherrod', 'Chance', 'Sherrod', '03/13/1986', 'TX', '14766364', '18', 'N/A', 'SALES REPRESENTATIVE', '07/07/2020', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '07/07/2020', 'elizabeth@mcanallywilkins.com', 1, '7-EPHS1YW8G63RO7XFC925JTMKIVLAZ0', '125882', '2020-07-07 11:27:46', '2020-07-07 11:48:54'),
(8, 'add', 'EV Oilfield Services', '07/01/2020', NULL, '123', '31232', '07/22/2020', '123', '3123', '31', '23', '3123123', '07/14/2020', NULL, 'test1 - test1@gmail.com', '07/20/2020', 'looklike.himu@gmail.com', 0, '8-ICWLJRG0FUNVBZA7E28SMDY5P4OX1H', '724051', '2020-07-20 11:23:59', '2020-07-20 11:23:59');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_equipment_request`
--

CREATE TABLE `insurance_equipment_request` (
  `id` int(11) NOT NULL,
  `request` varchar(191) DEFAULT NULL,
  `company_name` varchar(191) DEFAULT NULL,
  `effective_date` varchar(191) DEFAULT NULL,
  `year` varchar(191) DEFAULT NULL,
  `make` varchar(191) DEFAULT NULL,
  `model` varchar(191) DEFAULT NULL,
  `vin` varchar(191) DEFAULT NULL,
  `unit` varchar(191) DEFAULT NULL,
  `value` varchar(191) DEFAULT NULL,
  `description_equipment_accessories` text DEFAULT NULL,
  `equip_garraging_address_1` varchar(191) DEFAULT NULL,
  `equip_garraging_address_2` varchar(191) DEFAULT NULL,
  `equip_garraging_city` varchar(191) DEFAULT NULL,
  `equip_garraging_state` varchar(191) DEFAULT NULL,
  `equip_garraging_zip` varchar(191) DEFAULT NULL,
  `garaging_location_address` text DEFAULT NULL,
  `lease_purchase` varchar(191) DEFAULT NULL,
  `finance_company_name` varchar(191) DEFAULT NULL,
  `equip_lessor_address_1` varchar(191) DEFAULT NULL,
  `equip_lessor_address_2` varchar(191) DEFAULT NULL,
  `equip_lessor_city` varchar(191) DEFAULT NULL,
  `equip_lessor_state` varchar(191) DEFAULT NULL,
  `equip_lessor_zip` varchar(191) DEFAULT NULL,
  `finance_company_address` text DEFAULT NULL,
  `finance_company_phone_email` varchar(191) DEFAULT NULL,
  `additional_notes` text DEFAULT NULL,
  `submitted_by` varchar(191) DEFAULT NULL,
  `date` varchar(191) DEFAULT NULL,
  `send_mail_to` varchar(191) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `token` varchar(191) DEFAULT NULL,
  `url_token` varchar(191) DEFAULT NULL,
  `created_at` varchar(191) DEFAULT NULL,
  `updated_at` varchar(191) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurance_equipment_request`
--

INSERT INTO `insurance_equipment_request` (`id`, `request`, `company_name`, `effective_date`, `year`, `make`, `model`, `vin`, `unit`, `value`, `description_equipment_accessories`, `equip_garraging_address_1`, `equip_garraging_address_2`, `equip_garraging_city`, `equip_garraging_state`, `equip_garraging_zip`, `garaging_location_address`, `lease_purchase`, `finance_company_name`, `equip_lessor_address_1`, `equip_lessor_address_2`, `equip_lessor_city`, `equip_lessor_state`, `equip_lessor_zip`, `finance_company_address`, `finance_company_phone_email`, `additional_notes`, `submitted_by`, `date`, `send_mail_to`, `status`, `token`, `url_token`, `created_at`, `updated_at`) VALUES
(6, 'add', 'EV OILFIELD SERVICES', '06/15/2020', '2020', 'KENWORTH T880 W', 'HYDROVAC', '1NKZX4TX2LJ342839', '7618', '515,000', 'N/A', NULL, NULL, NULL, NULL, NULL, 'E COUNTY RD 140 Midland, TX 79706', 'lease', 'ELITE EQUIPMENT RENTAL', NULL, NULL, NULL, NULL, NULL, '244 WEST HIGHWAY 40 ROOSEVELT, UT 84066', '(432) 250-2412 OR (435) 722-2236 TMUTH@ELITERENTALS.NET', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '06/15/2020', 'elizabeth@mcanallywilkins.com', 1, '838266', '6-WATGRYF5ZSE7IM0VDNJ64HPB291CO3', '2020-06-15 20:10:55', '2020-06-15 20:10:55'),
(12, 'add', 'EV Oilfield Services', '06/30/2020', '1980', 'PROC', 'TANK', 'TR205693', '7218 T', '10,000', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'MHOLGUIN@EVOILFIELDSERVICES.COM', 'N/A', 'Mayra Holguin - mholguin@evoilfieldservices.com', '06/30/2020', 'elizabeth@mcanallywilkins.com', 1, '839495', '12-UXJQ4L9AB1F30IGE7KOMH6WVNP2SCD', '2020-06-30 19:06:56', '2020-06-30 19:16:27'),
(15, 'add', 'EV Oilfield Services', '07/07/2020', '1997', 'TRAILER', 'FONT FB TRAILER', '13N148300V1575666', '7518', '15000', 'FLAT BED TRAILER', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'N/A', 'Mayra Holguin - mholguin@evoilfieldservices.com', '07/07/2020', 'elizabeth@mcanallywilkins.com', 1, '866330', '15-7EVTWY58HI6GDRMJS1UXNLZOAQFK23', '2020-07-07 14:32:56', '2020-07-07 14:41:26'),
(16, 'add', 'EV Oilfield Services', '07/16/2020', '2020', '3123', '313', '3123', '123', '3123', 'Aweeq qwewqe qwe eq', 'wqe qe wqeqwe', 'eqwe qweqwe', 'eqwe qeqweqe', 'eqwe', '1312', NULL, 'lease', 'sadas sda', 'qeqw', '123', 'dasd', '3123', '3123', NULL, '312312', 'Aadas asdsad', 'test1 - test1@gmail.com', '07/16/2020', 'looklike.himu@gmail.com', 1, '662617', '16-Y3584AOFCPKMID6QEB1ZHJUTWNRSV0', '2020-07-16 07:49:23', '2020-07-16 07:49:51'),
(17, NULL, 'EV Oilfield Services', '07/07/2020', '1997', 'TRAILER', 'FONT FB TRAILER', '13N148300V1575666', '7518', '15000', 'FLAT BED TRAILER', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '07/07/2020', 'elizabeth@mcanallywilkins.com', 0, '796411', '17-CKEGQXL1SFVI2RTHMJ5Z6U4A7BY8D9', '2020-08-23 04:11:39', '2020-08-23 04:11:39');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_vehicle_request`
--

CREATE TABLE `insurance_vehicle_request` (
  `id` int(11) NOT NULL,
  `request` varchar(191) DEFAULT NULL,
  `company_name` varchar(191) DEFAULT NULL,
  `effective_date` varchar(191) DEFAULT NULL,
  `liability` varchar(191) DEFAULT NULL,
  `damage` varchar(191) DEFAULT NULL,
  `year` varchar(191) DEFAULT NULL,
  `make` varchar(191) DEFAULT NULL,
  `model` varchar(191) DEFAULT NULL,
  `vin` varchar(191) DEFAULT NULL,
  `body_type` varchar(191) DEFAULT NULL,
  `state_registered` varchar(191) DEFAULT NULL,
  `unit` varchar(191) DEFAULT NULL,
  `gross_vehicle_weight` varchar(191) DEFAULT NULL,
  `cost_new` varchar(191) DEFAULT NULL,
  `mileage_radius` varchar(191) DEFAULT NULL,
  `special_equipment` text DEFAULT NULL,
  `vehicle_garraging_address_1` varchar(191) DEFAULT NULL,
  `vehicle_garraging_address_2` varchar(191) DEFAULT NULL,
  `vehicle_garraging_city` varchar(191) DEFAULT NULL,
  `vehicle_garraging_state` varchar(191) DEFAULT NULL,
  `vehicle_garraging_zip` varchar(191) DEFAULT NULL,
  `garaging_address` varchar(191) DEFAULT NULL,
  `vehicle_usage` varchar(191) DEFAULT NULL,
  `lease_of_purchase` varchar(191) DEFAULT NULL,
  `finance_company_name` varchar(191) DEFAULT NULL,
  `vehicle_lessor_address_1` varchar(191) DEFAULT NULL,
  `vehicle_lessor_address_2` varchar(191) DEFAULT NULL,
  `vehicle_lessor_city` varchar(191) DEFAULT NULL,
  `vehicle_lessor_state` varchar(191) DEFAULT NULL,
  `vehicle_lessor_zip` varchar(191) DEFAULT NULL,
  `finance_company_address` text DEFAULT NULL,
  `finance_company_phone_email` varchar(191) DEFAULT NULL,
  `additional` text DEFAULT NULL,
  `submitted_by` varchar(191) DEFAULT NULL,
  `date` varchar(191) DEFAULT NULL,
  `send_mail_to` varchar(191) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `url_token` varchar(191) DEFAULT NULL,
  `token` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurance_vehicle_request`
--

INSERT INTO `insurance_vehicle_request` (`id`, `request`, `company_name`, `effective_date`, `liability`, `damage`, `year`, `make`, `model`, `vin`, `body_type`, `state_registered`, `unit`, `gross_vehicle_weight`, `cost_new`, `mileage_radius`, `special_equipment`, `vehicle_garraging_address_1`, `vehicle_garraging_address_2`, `vehicle_garraging_city`, `vehicle_garraging_state`, `vehicle_garraging_zip`, `garaging_address`, `vehicle_usage`, `lease_of_purchase`, `finance_company_name`, `vehicle_lessor_address_1`, `vehicle_lessor_address_2`, `vehicle_lessor_city`, `vehicle_lessor_state`, `vehicle_lessor_zip`, `finance_company_address`, `finance_company_phone_email`, `additional`, `submitted_by`, `date`, `send_mail_to`, `status`, `url_token`, `token`, `created_at`, `updated_at`) VALUES
(3, 'add', 'EV OILFIELD SERVICES', '06/16/2020', 'liability', NULL, '1979', 'FRUEHAUF', 'DES-M20/32', 'FRV30911', 'BELLY DUMP TRAILER', 'TX', '118 T', 'UNKNOWN', '20,000', '50', 'N/A', NULL, NULL, NULL, NULL, NULL, '903 W. INDUSTRIAL MIDLAND TX 79701', 'TO WORK WITH BELLY DUMP', 'PURCHASED', 'N/A', NULL, NULL, NULL, NULL, NULL, 'N/A', 'N/A', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '06/16/2020', 'elizabeth@mcanallywilkins.com', 1, '3-K8L417STENO6FI9MZBDJCGHVWR0AQU', '139426', '2020-06-16 18:50:18', '2020-06-16 18:50:19'),
(5, 'add', 'Ev Oilfield Services', '06/24/2020', 'yes', 'yes', '2020', 'F150', 'FORD', '1FTEW1E56LKD17286', 'PICKUP', 'TX', '53', '4,800', '50,000', '50', 'N/A', NULL, NULL, NULL, NULL, NULL, '903 W. INDUSTRIAL', 'SUPERVISORS', 'PURCHASED', 'FORD MOTOR CREDIT', NULL, NULL, NULL, NULL, NULL, 'PO BOX 105704 ATLANTA GA 30348', '1800-727-7000', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '06/24/2020', 'elizabeth@mcanallywilkins.com', 1, '5-IMKX1P8ZGSLWVOH9FNR6AUQ507Y34E', '553128', '2020-06-24 18:49:17', '2020-06-24 18:52:04'),
(6, 'add', 'Ev Oilfield Services', '06/24/2020', 'yes', 'yes', '2020', 'FORD', 'F150', '1FTEW1E56LKD17305', 'PICKUP', 'TX', '53', '4,800', '50,000', '50', 'N/A', NULL, NULL, NULL, NULL, NULL, '903 W. INDUSTRIAL MIDLAND TX 79701', 'SUPERVISORS', 'PURCHASE', 'FORD MOTOR CREDIT', NULL, NULL, NULL, NULL, NULL, 'PO BOX 105704 ATLANTA GA 30348', '1800-727-7000', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '06/24/2020', 'elizabeth@mcanallywilkins.com', 1, '6-KCJ7BN36Z8DGV02S91MQRYFU4PAI5L', '637805', '2020-06-24 18:58:21', '2020-06-24 19:12:49'),
(8, 'add', 'EV Oilfield Services', '7/7/2020', 'yes', 'yes', '2015', 'INTERNATIONAL', 'TRUCK', '3HAZZAAL2FL647289', 'TRUCK', 'TX', '54', '80000', '12000', '50', 'roustabout truck', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'ROUSTABOUT WORK', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'N/A', 'Mayra Holguin - mholguin@evoilfieldservices.com', '7/7/2020', 'elizabeth@mcanallywilkins.com', 1, '8-W4JBP0O2A7ZM16CDKRIHFNUQVSG39T', '707014', '2020-07-07 13:19:26', '2020-07-07 13:26:54'),
(9, 'add', 'EV Oilfield Services', '07/07/2020', 'yes', 'yes', '2006', 'INTERNATIONAL', 'TRUCK', '2HSCNAPR16C224766', 'TRUCK- TRACTOR', 'TX', '1918', '80000', '25000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'N/A', 'OWNER- EIGHT FOUR ENTERPRISES LLC', 'N/A', 'N/', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'N/A', 'Mayra Holguin - mholguin@evoilfieldservices.com', '07/07/2020', 'elizabeth@mcanallywilkins.com', 1, '9-KWN4HCD2JI0O5VMA9GBFQLP1ESZX8T', '169003', '2020-07-07 14:18:37', '2020-07-07 14:19:43'),
(10, 'add', 'EV Oilfield Services', '07/07/2020', 'yes', 'no', '1995', 'PION', 'TRL', '1P9TF422351260574', 'TANK TRAILER', 'TX', '1918', '16000', '10000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'OWNER OPERATOR', 'OWNER- EIGHT FOUR ENTERPRISES LLC', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'N/A', 'Mayra Holguin - mholguin@evoilfieldservices.com', '07/07/2020', 'elizabeth@mcanallywilkins.com', 1, '10-RFY5TOHLQ9V6SJ8EUIP1DNBAXM703G', '403277', '2020-07-07 14:23:01', '2020-07-07 14:23:48'),
(11, 'add', 'EV Oilfield Services', '07/09/2020', 'yes', 'no', '2006', 'TROX', 'TRL', '1T9TA432661867415', 'TANK', 'TX', '3718', '40000', '15000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '07/09/2020', 'elizabeth@mcanallywilkins.com', 1, '11-EYSIOVW7JN968DA1LU4BQ3XPKRCHMF', '748098', '2020-07-09 11:48:53', '2020-07-09 12:10:40'),
(12, 'add', 'EV Oilfield Services', '07/09/2020', 'yes', 'no', '2002', 'DRAGON', 'TANK', '1UNST41232L023604', 'TN', 'TX', '7418', '20000', '15000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'N/A', 'Mayra Holguin - mholguin@evoilfieldservices.com', '07/09/2020', 'elizabeth@mcanallywilkins.com', 1, '12-K8RXMAIGHYONLJT0PUVE3ZQ2FSBD15', '907228', '2020-07-09 16:34:25', '2020-07-09 16:52:11'),
(13, 'add', 'EV Oilfield Services', '07/17/2020', 'yes', 'no', '1978', 'DOWN', 'N/A', '178019', 'TANK-TRAILER', 'TX', '1118', '12500', '8000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mayraholguin20@yahoo.com', 'N/A', 'Mayra Holguin - mholguin@evoilfieldservices.com', '07/17/2020', 'elizabeth@mcanallywilkins.com', 1, '13-WONHLVS7E9RQ4XU0MY8APF2DTBKCZG', '682479', '2020-07-17 18:21:19', '2020-07-20 12:00:32'),
(14, 'add', 'EV Oilfield Services', '07/20/2020', 'yes', 'yes', '2020', 'FORD', 'F150', '1FTEW1E54LKD17304', 'PICKUP', 'TX', '54', '7000', '50000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'SUPERVISORS', 'PURCHASE', 'ROGERS FORD SALES', '4200 W WALL', 'N/A', 'MIDLAND', 'TX', '79703', NULL, '432-694-8801', 'N/A', 'Mayra Holguin - mholguin@evoilfieldservices.com', '07/20/2020', 'elizabeth@mcanallywilkins.com', 1, '14-RAOJGXMI0Z21PV938UHYCSETWLK4NB', '604721', '2020-07-20 13:30:09', '2020-07-20 13:48:33'),
(17, 'add', 'EV Oilfield Services', '08/05/2020', 'yes', 'yes', '2020', 'INTERNATIONAL', 'TRUCK', '2HSCESBR17C429521', 'TRUCK- TRACTOR', 'tx', '3518', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'mIDLAND', 'TEXAS', '79706', NULL, 'VACUUM TRUCK', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/05/2020', 'elizabeth@mcanallywilkins.com', 1, '17-VDB6AMH7JOLG5314CXPSQ8WUTK09EI', '429965', '2020-08-05 14:04:19', '2020-08-05 14:57:46'),
(18, 'add', 'EV Oilfield Services', '08/10/2020', 'yes', 'yes', '2012', 'FREIGHTLINER', 'CAI', '2HSCESBR17C429521', 'TRUCK- TRACTOR', 'TX', '3518', '80000', '35000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/10/2020', 'elizabeth@mcanallywilkins.com', 1, '18-OB17CRSFVJ4IXGK0QN23P9AZDU86EY', '791768', '2020-08-10 18:09:35', '2020-08-10 18:15:20'),
(19, 'amend', 'EV Oilfield Services', '08/10/2020', 'yes', 'yes', '2007', 'INTERNATIONAL', 'N/A', '2HSCESBR17C429521', 'TRUCK- TRACTOR', 'TX', '3518', '80000', '35000', '50', 'WATER HAULING', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/10/2020', 'elizabeth@mcanallywilkins.com', 1, '19-D918SM4TUIVZHRAGQN0KW6P2CO3EXB', '390294', '2020-08-10 18:12:06', '2020-08-10 18:33:26'),
(20, 'add', 'EV Oilfield Services', '08/10/2020', 'yes', 'no', '2012', 'FREIGHTLINER', 'CAI', '1FUJGLDR8CSBD7228', 'TRUCK- TRACTOR', 'TX', '3718', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/10/2020', 'elizabeth@mcanallywilkins.com', 1, '20-RUMKTSCZE30QHV7XL46PGDN1BOIA9W', '295658', '2020-08-10 18:15:57', '2020-08-10 18:21:21'),
(21, 'add', 'EV Oilfield Services', '08/10/2020', 'yes', 'no', '2006', 'FREIGHTLINER', 'COL', '1FUJA6CKX6LN84313', 'TRUCK- TRACTOR', 'TX', '7018', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/10/2020', 'elizabeth@mcanallywilkins.com', 1, '21-PTNHMG63Y4FXE8UKVQOSJ1CLIZ5W07', '422359', '2020-08-10 18:21:31', '2020-08-10 18:36:53'),
(22, 'add', 'EV Oilfield Services', '08/10/2020', 'yes', 'yes', '2000', 'FREIGHTLINER', 'N/A', '1FUPCSEB4YDB82829', 'TRUCK- TRACTOR', 'TX', '7218', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/10/2020', 'elizabeth@mcanallywilkins.com', 1, '22-27DMV56TP4IF0GARU9W8NX3ELBOJH1', '596007', '2020-08-10 18:25:17', '2020-08-10 18:27:52'),
(23, 'add', 'EV Oilfield Services', '08/10/2020', 'yes', 'no', '2014', 'FREIGHTLINER', 'CAS', '1FUJGHDV2ELFR5052', 'TRUCK- TRACTOR', 'TX', '7418', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/10/2020', 'elizabeth@mcanallywilkins.com', 1, '23-WM4F3EG6VCBOTYK29PSQAD70INUX51', '536056', '2020-08-10 18:28:40', '2020-08-10 18:38:16'),
(24, 'add', 'EV Oilfield Services', '08/10/2020', 'yes', 'no', '2014', 'FREIGHTLINER', 'CAS', '1FUJGHDV4ELFR5053', 'TRUCK- TRACTOR', 'TX', '7518', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/10/2020', 'elizabeth@mcanallywilkins.com', 1, '24-6FXPLVTR2M9B84YU7C0I5GSWQD3HAO', '707805', '2020-08-10 18:31:48', '2020-08-10 18:45:45'),
(25, 'add', 'EV Oilfield Services', '08/10/2020', 'yes', 'no', '2014', 'FREIGHTLINER', 'CAS', '1FUJGHDV4ELFR5053', 'TRUCK- TRACTOR', 'TX', '7518', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/10/2020', 'elizabeth@mcanallywilkins.com', 1, '25-4ESJQN38K7LR1B2WPMYVXCDHAU6TOF', '405267', '2020-08-10 18:31:48', '2020-08-10 18:44:49'),
(26, 'add', 'EV Oilfield Services', '08/10/2020', 'yes', 'no', '2010', 'FREIGHTLINER', 'N/A', '1FUJBBCK6ADAG5718', 'TRUCK- TRACTOR', 'TX', '1118', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/10/2020', 'elizabeth@mcanallywilkins.com', 1, '26-UQ8KO0BF1GTRMI6WL7VXAJ2SDH4C5Z', '827376', '2020-08-10 18:36:34', '2020-08-10 18:43:59'),
(27, 'add', 'EV Oilfield Services', '08/10/2020', 'yes', 'no', '1998', 'FREIGHTLINER', 'N/A', '1FUYDCXB5WL889278', 'TRUCK- TRACTOR', 'TX', '74', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '78706', NULL, 'VACUUM TRUCK', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/10/2020', 'elizabeth@mcanallywilkins.com', 1, '27-XSLT9A3RNKUB4Y2MWV1E0HP6QO7FDJ', '882719', '2020-08-10 18:39:43', '2020-08-10 18:43:00'),
(28, 'add', 'EV Oilfield Services', '08/10/2020', 'yes', 'no', '2019', 'PETERBUILT', 'N/A', '1NPCXPEX8KD435134', 'HYDROVAC', 'TX', '1018', '80000', '500000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'EXCAVATION', 'RENTAL', 'KAISER RENTAL', '2550 E. BIJOU AVE', 'N/A', 'FT. MORGAN', 'CO', '80701', NULL, '970-542-1975', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/10/2020', 'elizabeth@mcanallywilkins.com', 1, '28-F26B547ZM0GXJDE1NHCLW9AOS3IPYU', '301675', '2020-08-10 19:35:09', '2020-08-10 19:54:04'),
(29, 'add', 'EV Oilfield Services', '08/12/2020', 'yes', 'no', '1998', 'FREIGHTLINER', 'N/A', '1FUYDCXB5WL889278', 'TRUCK- TRACTOR', 'TX', '74', '80000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/12/2020', 'elizabeth@mcanallywilkins.com', 1, '29-0U1BVA7KE4OYTHMX8RC29D6JQL3GZW', '763644', '2020-08-12 16:43:39', '2020-08-12 17:06:46'),
(30, 'add', 'EV Oilfield Services', '08/12/2020', 'yes', 'no', '2019', 'PETERBUILT', 'HYDROVAC', '1NPCXPEX8KD435134', 'TRUCK- TRACTOR', 'TX', '1018', '80000', '500000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'EXCAVATION, SUPER SUCKER', 'PURCHASE', 'KAISER RENTAL', '2550 E BIJOU AVE', 'N/A', 'FT MORGAN', 'CO', '80701', NULL, '970-5421975', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/12/2020', 'elizabeth@mcanallywilkins.com', 1, '30-9FT2NXOJ0R1VIZPMLGAEQ3YKS6784C', '515217', '2020-08-12 16:52:09', '2020-08-12 17:07:06'),
(31, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2019', 'PETERBUILT', 'HYDROVAC', '1NPCXPEXXKD435135', 'TRUCK- TRACTOR', 'TX', '2318', '80000', '50000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'EXCAVATION, SUPER SUCKER', 'PURCHASE', 'KAISER RENTAL', '2550 E BIJOU AVE', 'N/A', 'FT MORGAN', 'CO', '80701', NULL, '970-5421975', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '31-Q56K7ZDBS932XT0M1UVLOPIGAERY8F', '273752', '2020-08-13 13:43:33', '2020-08-17 11:11:15'),
(32, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '1995', 'FREIGHTLINER', 'N/A', '1FUYDDYB2SP764813', 'TRUCK- TRACTOR', 'TX', '118', '80000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '32-90VJTRESBCIWPO45HN1GLA3DZX6U8K', '387836', '2020-08-13 13:49:37', '2020-08-13 14:06:20'),
(33, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2015', 'INTERNATIONAL', 'N/A', '3HAZZAAL2FL647289', 'TRUCK', 'TX', '54', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140E', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'ROUSTABOUT WORK', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '33-XM8AGNC6ZFVIUHK1E3T2PL5JR0WBD7', '572830', '2020-08-13 14:02:02', '2020-08-13 14:03:08'),
(34, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2007', 'FREIGHTLINER', 'N/A', '1FVACWDD17HY11365', 'TRUCK', 'TX', '52', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'ROUSTABOUT WORK', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '34-I4FP698XWDQ5C32TNUORJ7YK0EHAZB', '173179', '2020-08-13 14:23:48', '2020-08-17 11:08:28'),
(35, 'add', 'EV Oilfield Services', '8/13/2020', 'yes', 'no', '2007', 'INTERNATIONAL', 'N/A', '2HSCNAPR87C401881', 'TRUCK- TRACTOR', 'TX', '2118', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'OWNER OPERATOR', 'ZION lOGISTICTS - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '8/13/2020', 'elizabeth@mcanallywilkins.com', 1, '35-0Y4LFC7QEKHAD6WTM3NI29JRP8BUSZ', '163376', '2020-08-13 14:31:55', '2020-08-16 17:43:34'),
(36, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2006', 'FREIGHTLINER', 'N/A', '1FUJA6CK26LW47193', 'TRUCK- TRACTOR', 'TX', '4418', '80000', '30000', '50', 'n/a', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'OWNER OPERATOR', 'ZION lOGISTICS - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'duplicate', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '36-BD52GACTVQKMJSOPR7UH83FI9W64N1', '988261', '2020-08-13 14:36:10', '2020-08-13 16:32:26'),
(37, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2009', 'FREIGHTLINER', 'N/A', '1FUJA6CK29DAE5679', 'TRUCK- TRACTOR', 'TX', '4918', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'OWNER OPERATOR', 'BROWN PANDA - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '37-B7FINA4KWOPG1EXZ3SU0H5DRTCJ2L8', '370433', '2020-08-13 14:41:12', '2020-08-13 16:32:44'),
(38, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2013', 'INTERNATIONAL', 'N/A', '3HSDJSJRXDN309101', 'TRUCK- TRACTOR', 'TX', '6518', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'OWNER OPERATOR', 'ORC WAY- OWNER OPERAROT', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '38-BKPO2RJFCAY371HZIX8SU4WMNQETGD', '399668', '2020-08-13 16:26:24', '2020-08-13 16:28:20'),
(39, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2005', 'INTERNATIONAL', 'N/A', '2HSCNAPR55C007929', 'TRUCK- TRACTOR', 'TX', '1818', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'OWNER OPERATOR', 'EIGHTFOUR - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '39-X1DIP3SNE9ZUAQ5VBCMR82G4FY7HJO', '823524', '2020-08-13 16:31:07', '2020-08-13 16:33:41'),
(40, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2006', 'INTERNATIONAL', 'N/A', '2HSCNAPR16C224766', 'TRUCK- TRACTOR', 'TX', '1918', '80000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'OWNER OPERATOR', 'EIGHTFOUR - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '40-3N7F6YAVWUCEKMG0PBHQ85RTJXLI2Z', '547039', '2020-08-13 16:34:07', '2020-08-13 17:39:39'),
(41, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2011', 'EAGLE', 'N/A', '1D9ST422XBV661138', 'TRUCK- TRACTOR', 'TX', '3518', '40000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '41-6KJ0V37FULPQMACDBYZS9XWH1NIG25', '711932', '2020-08-13 17:25:38', '2020-08-13 17:39:24'),
(42, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '1979', 'TRIM', 'N/A', 'P31TV61344', 'TANK', 'TX', '3718', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '42-1P2JXKVIOG0SF7YANQUHMR6DW8TZCB', '811127', '2020-08-13 17:36:34', '2020-08-13 17:37:22'),
(43, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2007', 'DRAGON', 'N/A', 'P31TV61344', 'TANK', 'Tx', '7018', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '43-Z1DIY065XSHMF749JE2LCTGVKO8UPR', '114603', '2020-08-13 18:04:24', '2020-08-13 18:19:12'),
(44, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '1980', 'PROC', 'N/A', 'TR205693', 'TANK', 'TX', '7218', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '44-KH7V8WTJFG5LAUXZRME61NC4QS9IPO', '383033', '2020-08-13 18:09:17', '2020-08-13 18:20:27'),
(45, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2002', 'DRAGON', 'N/A', '1UNST41232L023604', 'TANK', 'TX', '7418', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '45-J2KWR7HAI14EVOBSGXTZ8Q0NF95CLP', '167843', '2020-08-13 18:13:37', '2020-08-13 18:21:03'),
(46, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '1997', 'FONT', 'N/A', '13N148300V1575666', 'FLATBED TRAILER', 'TX', '7418', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'HAUL PIPES', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '46-7UQJGDA910I36FKWE5O8YXMC4HB2VT', '114430', '2020-08-13 18:17:33', '2020-08-13 18:22:08'),
(47, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2006', 'TROX', 'N/A', '1T9TA432661867415', 'TANK', 'TX', '1118', '40000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '47-05DQE89H4YPUVJR3IBTSLKAMF6ZGX2', '293137', '2020-08-13 18:20:28', '2020-08-13 20:28:36'),
(48, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2020', 'PROFAB', 'N/A', '3P9KT4027C1033160', 'TANKER', 'TX', '0074', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '48-8X5EBLWOGC2QPD31ZSM9JN7IA0RH6U', '910280', '2020-08-13 18:23:45', '2020-08-13 20:17:40'),
(49, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '1979', 'FRUE', 'N/A', 'FRV30911', 'BELLY TRAILER', 'TX', '118', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'HAUL LOOSE MATERIALS', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '49-18WM293LPIHZD4V7JCXG0TANKUYQFE', '143290', '2020-08-13 18:27:19', '2020-08-13 20:23:24'),
(50, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '1979', 'FRUE', 'N/A', 'FRV30911', 'BELLY TRAILER', 'TX', '118', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'HAUL LOOSE MATERIALS', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '50-C86YPBMJ9NI5RSOL1HUZTK723XDWAV', '265785', '2020-08-13 18:27:21', '2020-08-13 20:24:21'),
(51, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2008', 'DRAGON', 'N/A', '1UNST42237L063705', 'TANKER', 'TX', '2118', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'OWNER OPERATOR', 'ZION lOGISTICS - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '51-U2WTMQ4FHL9BJ05PDK7SEV6XGIRA8O', '492881', '2020-08-13 18:33:57', '2020-08-13 20:20:32'),
(52, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2002', 'PION', 'N/A', '1P9TA432021260478', 'TANKER', 'TX', '4418', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'OWNER OPERATOR', 'ZION lOGISTICS - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '52-LJFMZQCHIOG1XPBUYN76V2T9ASE5DW', '137851', '2020-08-13 18:36:56', '2020-08-13 20:19:32'),
(53, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2012', 'PROCO', 'N/A', '1P9VN3627CK359006', 'TANKER', 'TX', '4918', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'OWNER OPERATOR', 'ZION lOGISTICS - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '53-KQNGSOU5PZWDBLI2HV734M18XCE9TR', '680468', '2020-08-13 18:39:57', '2020-08-13 20:18:42'),
(54, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2007', 'DRAGON', 'N/A', '1UNST422X7L045640', 'TANKER', 'TX', '6518', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '54-RESKTCIPL2YMJANV6BZO1XD4QGUF08', '515619', '2020-08-13 18:43:22', '2020-08-13 20:29:54'),
(55, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2007', 'DRPU', 'N/A', '1UNST42277L052593', 'TANKER', 'TX', '1818', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'OWNER OPERATOR', 'EIGHTFOUR - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '55-U2DVK6AOCHT7N3X1IF9EWMPBY80QGJ', '431014', '2020-08-13 18:46:22', '2020-08-13 20:14:45'),
(56, 'add', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '1995', 'PIONEER', 'N/A', '1P9TF4223S1260574', 'TANKER', 'TX', '1918', '20000', '20000', '50', 'N/A', 'E COUNTY RD', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'OWNER OPERATOR', 'EIGHTFOUR - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'Mholguin@evoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '56-9U85ASTYNLBO2KHMQ1VZ7E3X4FIRWD', '933231', '2020-08-13 18:51:12', '2020-08-13 20:13:20'),
(57, 'amend', 'EV Oilfield Services', '08/13/2020', 'yes', 'no', '2007', 'FREIGHTLINER', 'N/A', '1FVACWDD17HY11365', 'TRUCK', 'TX', '52', '80000', '30000', '50', 'NONE', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'ROUSTABOUT WORK', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/13/2020', 'elizabeth@mcanallywilkins.com', 1, '57-VFM8CWRZ0UEDSH31GJIK7T65YQPXA9', '465141', '2020-08-13 19:24:59', '2020-08-13 19:56:27'),
(58, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2012', 'SILVERADO', 'CHEVROLET', '1GCRKSE70CZ247134', 'PICK-UP', 'TX', '12', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'POWERWASH', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mayraholguin20@yahoo.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '58-1DYZFM62UGWRJL49XBESNACH3O0TKP', '168349', '2020-08-14 12:26:49', '2020-08-14 12:46:30'),
(59, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2012', 'SILVERADO', 'CHEVROLET', '1GCRKSE70CZ247134', 'PICK-UP', 'TX', '12', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'POWERWASH', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mayraholguin20@yahoo.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '59-U51QXZY3I68DPEWT0LV2C9HORKMBFJ', '273712', '2020-08-14 12:26:52', '2020-08-14 12:49:03'),
(60, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2014', 'RAM', 'DODGE', '3C63RRGL2EG310717', 'PICKUP', 'TX', '13', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'PM SERVICE', '30000', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mayraholguin20@yahoo.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '60-ZTP2AMR198X5N47CBE3IKGV0OWDSHL', '518436', '2020-08-14 12:34:18', '2020-08-14 12:49:29'),
(61, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2015', 'DODGE', 'RAM', '1C6RR6LM6FS755947', 'PICKUP', 'TX', '16', '20000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'SERVICE', 'PURCHASE', 'N/A', 'N/AN', 'N/AN', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '61-B7LQGJXV0RYK9EZTS34ADC8OU26FI5', '187259', '2020-08-14 12:41:55', '2020-08-14 12:54:37'),
(62, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2013', 'SILVERADO', 'CHRVROLET', '1GBOCVCG9DF173360', 'PICKUP', 'TX', '17', '20000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'POWERWASH', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '62-OVI3ZFD8T750B6P41JU2ELSHKXYWAC', '344050', '2020-08-14 12:45:08', '2020-08-14 12:57:00'),
(63, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2013', 'RAM', 'DODGE', '3C7WRLEL2DG552656', 'PICKUP', 'TX', '18', '20000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'PM SERVICE', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '63-XIMS78VG3AN0HYTPFB4QC92WD6KLZ5', '348353', '2020-08-14 12:48:34', '2020-08-14 12:58:49'),
(64, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2010', 'SILVERADO', 'CHEVROLET', '3GCRCSE01AG199561', 'PICKUP', 'TX', '20', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'POWERWASH', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '64-SZV6ICP4B8LAT3RXHJ5UE7QOYFKG2D', '604186', '2020-08-14 12:52:37', '2020-08-14 13:10:33'),
(65, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2013', 'SILVERADO', 'CHEVROLET', '1GC1CVCG4DF115477', 'PICKUP', 'TX', '23', '20000', '25000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'POWERWASH', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '65-Q18WH6IOYM0CP5ZXURB47L23KDEAFV', '857774', '2020-08-14 12:56:44', '2020-08-14 13:18:54'),
(66, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2015', 'SILVERADO', 'CHEVROLET', '1GC1CUEG9FF177631', 'PICKUP', 'TX', '24', '20000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'POWERWASH', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'MIDLAND', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '66-2XVKCSZ8P19LUGTIO3EBRW4A6HM5FY', '737820', '2020-08-14 13:00:49', '2020-08-14 13:19:40'),
(67, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2011', 'SILVERADO', 'CHEVROLET', '1GC1CVCG6BF190369', 'PICKUP', 'TX', '28', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'SERVICE', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '67-SX1JPMY0Q2VG7Z4E9KWBCT6FINRA8L', '812411', '2020-08-14 13:06:48', '2020-08-14 13:31:59'),
(68, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2012', 'SILVERADO', 'CHEVROLET', '1GC1CVCG9CF155620', 'PICKUP', 'TX', '32', '20000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'SERVICE', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '68-P7Z1GJSEXUMN2K4H8CVWFB60ILTYAR', '605021', '2020-08-14 13:12:55', '2020-08-14 13:52:24'),
(69, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2012', '2500 SILVERADO', 'CHEVROLET', '1GB2KVCG2CZ135767', 'PICKUP', 'Tx', '33', '20000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'SERVICE', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '69-3D86M5FHR1GVJ7O4BQ0ECLNISYATZ9', '111197', '2020-08-14 13:21:12', '2020-08-14 13:54:30'),
(70, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2018', 'SILVERADO', 'CHEVROLET', '3GCUKREC6JG391896', 'PICKUP', 'TX', '35', '20000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'PM SERVICE', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'MIDLAND', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '70-95WBC0VZM4P6K3UF8Y2DJGHAINE71Q', '754981', '2020-08-14 13:25:27', '2020-08-14 13:55:33'),
(71, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2018', 'SILVERADO', 'CHEVROLET', '3GCUKREC3JG253507', 'PICKUP', 'TX', '36', '20000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'SUPERVISOR', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '71-Z21LMYUR9OPKJNQ7DT06SB8XW3GC5A', '341072', '2020-08-14 13:34:00', '2020-08-14 13:41:39'),
(72, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2013', 'FORD', 'F150', '1FTFW1ET7DFB84311', 'PICKUP', 'TX', '40', '10000', '20000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'SERVICE', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '72-N7Z4Q9TBXYKOAI06RGW3F28JUEC5P1', '944371', '2020-08-14 13:44:03', '2020-08-14 13:56:55'),
(73, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2018', 'FORD', 'VAN', '1FBZX2ZG7JKA95617', 'VAN', 'TX', '41', '10000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'SERVICE', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '73-3G7AS8RMJ19XWTPNFQ0BHYZU52CKVI', '355896', '2020-08-14 14:24:08', '2020-08-14 14:29:44'),
(74, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2019', 'FORD', 'F250', '1FT7W2BT9KED36705', 'PK', 'TX', '42', '10000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'SUPERVISORS', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '74-TVS3XE08L2WYODNR96PKZHC4BAJUGI', '984217', '2020-08-14 14:27:08', '2020-08-14 14:36:58'),
(75, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2018', 'FORD', 'F250', '1FT7W2BT6KED31414', 'PK', 'TX', '43', '10000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'SUPERVISOR', 'PURCHASE', 'N/A', 'N/A', 'N/AN', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '75-Z8S2YL9I0FXD7T64WKAQV3NUJGPMEC', '379270', '2020-08-14 14:30:00', '2020-08-14 14:38:00'),
(76, 'add', 'EV Oilfield Services', '08/14/2020', 'yes', 'yes', '2019', 'FORD', 'F250', '1FT7W2BT2KED26873', 'PK', 'TX', '44', '10000', '30000', '50', 'N/A', 'E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'SUPERVISOR', 'PURCHASE', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'DUPLICATE', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/14/2020', 'elizabeth@mcanallywilkins.com', 1, '76-BNAMIX3FESWVRH41D6TY7KUO59LQ0G', '827165', '2020-08-14 14:40:47', '2020-08-14 17:06:58'),
(77, 'add', 'EV Oilfield Services', '08/17/2020', 'yes', 'no', '2013', 'KW', 'T60', '1XKAD49X7DR329395', 'TRUCK- TRACTOR', 'TX', '7818', '80000', '30000', '50', 'N/A', '1719 E. COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'OWNER OPERATOR', 'DUSTY ROADS - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/17/2020', 'elizabeth@mcanallywilkins.com', 1, '77-FYMS9N2H53AEVRIKP4CL7ZTG1JQ80U', '429278', '2020-08-17 12:18:33', '2020-08-17 12:27:18'),
(78, 'add', 'EV Oilfield Services', '08/17/2020', 'yes', 'no', '2012', 'JACK', '130', '1J9TA4327C1364627', 'TANK', 'TX', '7818', '20000', '20000', '50', 'N/A', '1719 e. COUNTY ROAD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'OWNER OPERATOR', 'DUSTY ROADS TRUCKING', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/17/2020', 'elizabeth@mcanallywilkins.com', 1, '78-NFMOQZVWLU8B5TYXJ07H412KPIR9GD', '873449', '2020-08-17 12:33:10', '2020-08-17 12:33:38'),
(79, 'add', 'EV Oilfield Services', '08/17/2020', 'yes', 'no', '2000', 'FREIGHTLINER', 'N/A', '1FUYDXYB5YLF71494', 'TRUCK- TRACTOR', 'TX', '7918', '80000', '30000', '50', 'N/A', '1719 e. COUNTY RD', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'VACUUM TRUCK', 'OWNER OPERATOR', 'ALAED NOA - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/17/2020', 'elizabeth@mcanallywilkins.com', 1, '79-CR7NEXVHI9PZM5AY6Q1FGTK4J3SOUW', '849832', '2020-08-17 16:48:57', '2020-08-17 17:33:05'),
(80, 'add', 'EV Oilfield Services', '08/17/2020', 'yes', 'no', '2007', 'EAGLE', 'TN', '5AGEV42287S457527', 'TANK', 'TX', '7918', '20000', '30000', '50', 'N/A', '1719 E COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'OWNER OPERATOR', 'ALAED NOA - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/17/2020', 'elizabeth@mcanallywilkins.com', 1, '80-Q5VRHZ7OKFPMYT918GACJ4NUIL6EWB', '238072', '2020-08-17 16:51:29', '2020-08-17 17:31:48'),
(81, 'add', 'EV Oilfield Services', '08/18/2020', 'yes', 'no', '2013', 'INTERNATIONAL', 'N/A', '3A9K402W4DJ080037', 'TANK', 'TX', '8018', '20000', '20000', '50', 'N/A', '1917 E COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'OWNER OPERATOR', 'CEPEDA & CHACON TRUCKING', 'N/A', 'N/A', 'MIDLAND', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'NEW', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/18/2020', 'elizabeth@mcanallywilkins.com', 1, '81-70I8V5LJHCWQG3DEXF9PM4K12UTORS', '955431', '2020-08-18 14:29:49', '2020-08-18 14:33:15'),
(82, 'add', 'EV Oilfield Services', '08/18/2020', 'yes', 'no', '2005', 'FREIGHTLINER', 'N/A', '1FUJBBCG45LN55535', 'TRUCK- TRACTOR', 'TX', '8018', '80000', '30000', '50', 'N/A', '1719 E COUNTY RD 140', 'N/A', 'Midland', 'TX', '79706', NULL, 'WATER HAULING', 'OWNER OPERATOR', 'CEPEDA & CHACON TRUCKING', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'NEW', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/18/2020', 'elizabeth@mcanallywilkins.com', 1, '82-KADO125VYMR9T0Z3CFWU4PNSIEHGXJ', '993385', '2020-08-18 14:33:33', '2020-08-18 14:35:15'),
(83, 'add', 'EV Oilfield Services', '08/18/2020', 'yes', 'no', '2005', 'FREIGHTLINER', 'N/A', '1FUJBBCG45LN55535', 'TRUCK- TRACTOR', 'TX', '8018', '80000', '30000', '50', 'N/A', '1719 E COUNTY RD 140', 'N/A', 'Midland', 'TX', '79706', NULL, 'WATER HAULING', 'OWNER OPERATOR', 'CEPEDA & CHACON TRUCKING', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', 'NEW', 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/18/2020', 'elizabeth@mcanallywilkins.com', 1, '83-IK6MCYNOF3XASG7598E0UZ1RHTQJW2', '839644', '2020-08-18 14:33:34', '2020-08-18 14:36:10'),
(84, 'add', 'EV Oilfield Services', '08/19/2020', 'yes', 'no', '2006', 'FREIGHTLINER', 'N/A', '1FUJA6AV96LV56676', 'TRUCK- TRACTOR', 'TX', '8118', '80000', '30000', '50', 'N/A', '1719 E COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'OWNER OPERATOR', 'aLVIO AGUILERA - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/19/2020', 'elizabeth@mcanallywilkins.com', 1, '84-KP4NQTDWUYO9E3SVH01FGJ2RI7CMX6', '888974', '2020-08-19 12:50:22', '2020-08-19 13:00:14'),
(85, 'add', 'EV Oilfield Services', '08/19/2020', 'yes', 'no', '2011', 'SHOP MADE', 'N/A', 'TR203034', 'TANK', 'TX', '8118', '20000', '20000', '50', 'N/A', '1719 E COUNTY RD 140', 'N/A', 'MIDLAND', 'TX', '79706', NULL, 'WATER HAULING', 'OWNER OPERATOR', 'ALVIO AGUILERA - OWNER OPERATOR', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', NULL, 'mholguin@eandvoilfieldservices.com', NULL, 'Mayra Holguin - mholguin@evoilfieldservices.com', '08/19/2020', 'elizabeth@mcanallywilkins.com', 1, '85-OHR0AJWXV2K36ZB8NP5UM9ELCG41IQ', '166425', '2020-08-19 12:53:02', '2020-08-19 13:05:58');

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE `inventories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(10) UNSIGNED NOT NULL,
  `subcat_id` int(10) UNSIGNED NOT NULL,
  `total_unit` int(11) NOT NULL,
  `available_unit` int(11) NOT NULL,
  `allocated_unit` int(11) NOT NULL,
  `resource_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resource_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pricing_variable` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_per_hour_service` decimal(12,2) DEFAULT NULL,
  `price_per_day_service` decimal(12,2) DEFAULT NULL,
  `price_per_week_service` decimal(12,2) DEFAULT NULL,
  `price_per_month_service` decimal(12,2) DEFAULT NULL,
  `price_per_hour_rental` decimal(12,2) DEFAULT NULL,
  `price_per_day_rental` decimal(12,2) DEFAULT NULL,
  `price_per_week_rental` decimal(12,2) DEFAULT NULL,
  `price_per_month_rental` decimal(12,2) DEFAULT NULL,
  `model_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_short_des` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_long_des` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `common_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serial_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `cat_id`, `subcat_id`, `total_unit`, `available_unit`, `allocated_unit`, `resource_name`, `resource_code`, `pricing_variable`, `price_per_hour_service`, `price_per_day_service`, `price_per_week_service`, `price_per_month_service`, `price_per_hour_rental`, `price_per_day_rental`, `price_per_week_rental`, `price_per_month_rental`, `model_num`, `web_short_des`, `web_long_des`, `common_image`, `serial_image`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 9, 5, 'VT', 'VT', '', '200.00', NULL, NULL, NULL, '80.00', NULL, NULL, NULL, 'VTM', NULL, NULL, NULL, NULL, '2020-08-13 01:25:55', '2020-09-02 05:03:46'),
(2, 1, 2, 4, 1, 2, 'rs', 'rs', '', '200.00', NULL, NULL, NULL, '80.00', NULL, NULL, NULL, 'rsm222', 'hgjg', 'bbmbnm dsadasdsa', NULL, NULL, '2020-09-01 06:51:11', '2020-09-02 05:18:23');

-- --------------------------------------------------------

--
-- Table structure for table `inventories_old`
--

CREATE TABLE `inventories_old` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(10) UNSIGNED NOT NULL,
  `subcat_id` int(10) UNSIGNED NOT NULL,
  `inventory_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upc_vin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_unit` int(11) NOT NULL,
  `available_unit` int(11) NOT NULL,
  `allocated_unit` int(11) NOT NULL,
  `resource_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resource_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pricing_variable` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `additional_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_short_des` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_long_des` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `common_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serial_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inventories_old`
--

INSERT INTO `inventories_old` (`id`, `cat_id`, `subcat_id`, `inventory_name`, `upc_vin`, `total_unit`, `available_unit`, `allocated_unit`, `resource_name`, `resource_code`, `pricing_variable`, `price`, `additional_info`, `model_num`, `web_short_des`, `web_long_des`, `common_image`, `serial_image`, `created_at`, `updated_at`) VALUES
(10, 4, 4, '1 - VTW', '1HJB6GLJI2QDWYCYB', 8, 8, 0, 'Vacuum Truck', 'VT80', 'Per Hour', '80.00', 'EVOS Internal Vaccum Truck', 'Freightliner', 'This Model Freightliner 120 BBL Vacuum Truck', 'This Model Freightliner 120 BBL Vacuum Truck is the perfect truck for your toughest jobs!  It can remove an amazing 30 gallons of material an hour or deliver up to 120 BBL of liquid to your location.  Lets schedule your next service request today!.', 'https://evostage.wikimesh.com/storage/images/1586445476_resourceImg.png', 'https://evostage.wikimesh.com/storage/images/1586445488_serialImg.png', '2020-03-23 11:43:21', '2020-04-09 13:37:13'),
(11, 4, 4, '2 - VTS', '35HQW87Z7YAFYK87N', 8, 8, 0, 'Vacuum Truck', 'VT80', 'Per Hour', '80.00', NULL, 'Freightliner', 'This Model Freightliner 120 BBL Vacuum Truck', 'This Model Freightliner 120 BBL Vacuum Truck is the perfect truck for your toughest jobs!  It can remove an amazing 30 gallons of material an hour or deliver up to 120 BBL of liquid to your location.  Lets schedule your next service request today!.', 'https://evostage.wikimesh.com/storage/images/1586445476_resourceImg.png', NULL, '2020-03-23 11:44:25', '2020-04-09 13:21:59'),
(12, 4, 4, '3 - VTR', 'QQRFDV6A49ZK1EQQV', 8, 8, 0, 'Vacuum Truck', 'VT80', 'Per Hour', '80.00', NULL, 'Freightliner', 'This Model Freightliner 120 BBL Vacuum Truck', 'This Model Freightliner 120 BBL Vacuum Truck is the perfect truck for your toughest jobs!  It can remove an amazing 30 gallons of material an hour or deliver up to 120 BBL of liquid to your location.  Lets schedule your next service request today!.', 'https://evostage.wikimesh.com/storage/images/1586445476_resourceImg.png', NULL, '2020-03-23 11:54:05', '2020-04-09 13:21:59'),
(13, 5, 5, '1 - HVO', '3FL7YWL1SLJKQEA1E', 2, 2, 0, 'Hydrovac', 'HV250', 'Per Hour', '250.00', NULL, 'Peterbilt Model 2019', 'Model 2019 Peterbilt 50 BBL Tank For Liquids, 50 BBL Tank For Sludge | Solids', 'This 2019 Model Peterbilt 50 BBL Hydrovac Tank is the perfect Super Suction Truck for your toughest jobs!  It can remove an amazing 50 BBL of just about any type of material an hour or deliver up to 50 BBL of liquid to your location.  Lets schedule your next service request today!', 'https://evostage.wikimesh.com/storage/images/1586447096_resourceImg.JPG', NULL, '2020-03-23 11:56:30', '2020-04-09 13:44:56'),
(14, 5, 5, '2 - HVT', 'AD1V7LSNG3LZ2KLK3', 2, 2, 0, 'Hydrovac', 'HV250', 'Per Hour', '250.00', NULL, 'Peterbilt Model 2019', 'Model 2019 Peterbilt 50 BBL Tank For Liquids, 50 BBL Tank For Sludge | Solids', 'This 2019 Model Peterbilt 50 BBL Hydrovac Tank is the perfect Super Suction Truck for your toughest jobs!  It can remove an amazing 50 BBL of just about any type of material an hour or deliver up to 50 BBL of liquid to your location.  Lets schedule your next service request today!', 'https://evostage.wikimesh.com/storage/images/1586447096_resourceImg.JPG', NULL, '2020-03-23 11:57:12', '2020-04-09 13:44:56'),
(15, 5, 5, '3 - HVU', '49JTEC9S4IKGDANMJ', 2, 2, 0, 'Hydrovac', 'HV250', 'Per Hour', '250.00', NULL, 'Peterbilt Model 2019', 'Model 2019 Peterbilt 50 BBL Tank For Liquids, 50 BBL Tank For Sludge | Solids', 'This 2019 Model Peterbilt 50 BBL Hydrovac Tank is the perfect Super Suction Truck for your toughest jobs!  It can remove an amazing 50 BBL of just about any type of material an hour or deliver up to 50 BBL of liquid to your location.  Lets schedule your next service request today!', 'https://evostage.wikimesh.com/storage/images/1586447096_resourceImg.JPG', NULL, '2020-03-23 11:58:09', '2020-04-09 13:44:56'),
(16, 6, 6, '1 - PWO', '1DVQYWNVH4HTWPSPB', 10, 10, 0, 'Power Washin Wand', 'PWW', 'Per Hour', '90.00', NULL, 'PWMobile', 'Power Washing Service', 'We offer the most comprehensive power washing of your truck, rig, mat, or just about anything that needs to be Power Wash Clean!  We\'ll take care to protect what\'s valuable while ensuring all the dirt and grime is power washed away.', 'https://evostage.wikimesh.com/storage/images/1586457214_resourceImg.JPG', NULL, '2020-03-23 12:01:22', '2020-04-09 16:33:34'),
(17, 6, 6, '2 - PWT', 'J45NNLJMTFDF9C7DS', 10, 10, 0, 'Power Washin Wand', 'PWW', 'Per Hour', '90.00', NULL, 'PWMobile', 'Power Washing Service', 'We offer the most comprehensive power washing of your truck, rig, mat, or just about anything that needs to be Power Wash Clean!  We\'ll take care to protect what\'s valuable while ensuring all the dirt and grime is power washed away.', 'https://evostage.wikimesh.com/storage/images/1586457214_resourceImg.JPG', NULL, '2020-03-23 12:02:13', '2020-04-09 16:33:34'),
(18, 6, 6, '3 - PWU', 'WQ6EQXK3MF4Y5S99P', 10, 10, 0, 'Power Washin Wand', 'PWW', 'Per Hour', '90.00', NULL, 'PWMobile', 'Power Washing Service', 'We offer the most comprehensive power washing of your truck, rig, mat, or just about anything that needs to be Power Wash Clean!  We\'ll take care to protect what\'s valuable while ensuring all the dirt and grime is power washed away.', 'https://evostage.wikimesh.com/storage/images/1586457214_resourceImg.JPG', NULL, '2020-03-23 12:04:56', '2020-04-09 16:33:34'),
(19, 7, 7, '1 - SVROO', '463017410578', 10, 10, 0, 'Service Operator', 'SVRO', 'Per Hour', '60.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:06:18', '2020-03-23 12:06:18'),
(20, 7, 7, '2 - SVROT', '331042592561', 10, 10, 0, 'Service Operator', 'SVRO', 'Per Hour', '60.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:06:54', '2020-03-23 12:06:54'),
(21, 7, 7, '3 - SVROU', '914401343123', 10, 10, 0, 'Service Operator', 'SVRO', 'Per Hour', '60.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:07:32', '2020-03-23 12:07:32'),
(22, 8, 8, '1 - RABTO', 'TTCPV2DE3T5QR4VJU', 10, 10, 0, 'Roustabout Truck', 'RABT', 'Per Hour', '120.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:08:32', '2020-03-23 12:08:32'),
(23, 8, 8, '2 - RABTT', '48NIZUBE2TR2LESDB', 10, 10, 0, 'Roustabout Truck', 'RABT', 'Per Hour', '120.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:09:03', '2020-03-23 12:09:03'),
(24, 8, 8, '3 - RABTU', 'GZUK6VYN1G6U7C53I', 10, 10, 0, 'Roustabout Truck', 'RABT', 'Per Hour', '120.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:09:39', '2020-03-23 12:09:39'),
(25, 9, 9, '1 - BHOEO', '922212493818', 10, 10, 0, 'Backhoe', 'BHOE', 'Per Hour', '85.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:10:40', '2020-03-23 12:10:40'),
(26, 9, 9, '2 - BHOET', '425809654522', 10, 10, 0, 'Backhoe', 'BHOE', 'Per Hour', '85.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:11:19', '2020-03-23 12:11:19'),
(27, 9, 9, '3 - BHOEU', '854779060245', 10, 10, 0, 'Backhoe', 'BHOE', 'Per Hour', '85.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:11:52', '2020-03-23 12:11:52'),
(28, 51, 52, '1 - WTRKO', '3JVE8LZXEJQL222N4', 10, 10, 0, 'Winch Truck', 'WTRK', 'Per Hour', '110.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:13:03', '2020-03-23 12:13:03'),
(29, 51, 52, '2 - WTRKT', 'VHID6GK18H7Q4UFWY', 10, 10, 0, 'Winch Truck', 'WTRK', 'Per Hour', '110.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:13:43', '2020-03-23 12:13:43'),
(30, 51, 52, '3 - WTRKU', '63CSR4N9PMM9VW4J1', 10, 10, 0, 'Winch Truck', 'WTRK', 'Per Hour', '110.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:14:16', '2020-03-23 12:14:16'),
(31, 11, 11, '1 - CP4X4O', '387534779106', 10, 10, 0, 'Containment Pad 4\' x 4\'', 'CP4X4', 'Per Day', '3.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:16:20', '2020-03-23 12:16:20'),
(32, 11, 11, '2 - CP4X4T', '522622326092', 10, 10, 0, 'Containment Pad 4\' x 4\'', 'CP4X4', 'Per Day', '3.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:17:05', '2020-03-23 12:17:05'),
(33, 11, 11, '3 - CP4X4U', '768539626991', 10, 10, 0, 'Containment Pad 4\' x 4\'', 'CP4X4', 'Per Day', '3.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:17:35', '2020-03-23 12:17:35'),
(34, 12, 12, '1 - CP5X5O', '668509276180', 10, 10, 0, 'Containment Pad 5\' x 5\'', 'CP5X5', 'Per Day', '4.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:19:04', '2020-03-23 12:19:04'),
(35, 12, 12, '2 - CP5X5T', '968261974944', 10, 10, 0, 'Containment Pad 5\' x 5\'', 'CP5X5', 'Per Day', '4.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:19:41', '2020-03-23 12:19:41'),
(36, 12, 12, '3 - CP5X5U', '493003642070', 10, 10, 0, 'Containment Pad 5\' x 5\'', 'CP5X5', 'Per Day', '4.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:20:17', '2020-03-23 12:20:17'),
(37, 13, 13, '1 - CP5X10O', '524469981807', 10, 10, 0, 'Containment Pad 5\' x 10\'', 'CP5X10', 'Per Day', '9.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:21:50', '2020-03-23 12:21:50'),
(38, 13, 13, '2 - CP5X10T', '289932438402', 10, 10, 0, 'Containment Pad 5\' x 10\'', 'CP5X10', 'Per Day', '9.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:22:48', '2020-03-23 12:22:48'),
(39, 13, 13, '3 - CP5X10U', '556259718134', 10, 10, 0, 'Containment Pad 5\' x 10\'', 'CP5X10', 'Per Day', '9.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:23:21', '2020-03-23 12:23:21'),
(40, 14, 14, '1 - CP8X8O', '186890197351', 10, 10, 0, 'Containment Pad 8\' x 8\'', 'CP8X8', 'Per Day', '9.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:24:17', '2020-03-23 12:24:17'),
(41, 14, 14, '2 - CP8X8T', '770974099417', 10, 10, 0, 'Containment Pad 8\' x 8\'', 'CP8X8', 'Per Day', '9.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:26:13', '2020-03-23 12:26:13'),
(42, 14, 14, '3 - CP8X8U', '638982537540', 10, 10, 0, 'Containment Pad 8\' x 8\'', 'CP8X8', 'Per Day', '9.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:27:09', '2020-03-23 12:27:09'),
(43, 15, 16, '1 - CP12X10O', '950137823485', 10, 10, 0, 'Containment Pad 12\' x 10\'', 'CP12X10', 'Per Day', '11.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:28:41', '2020-03-23 12:28:41'),
(44, 15, 16, '2 - CP12X10T', '122199790231', 10, 10, 0, 'Containment Pad 12\' x 10\'', 'CP12X10', 'Per Day', '11.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:29:22', '2020-03-23 12:29:22'),
(45, 15, 16, '3 - CP12X10U', '776843103116', 10, 10, 0, 'Containment Pad 12\' x 10\'', 'CP12X10', 'Per Day', '11.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:29:55', '2020-03-23 12:29:55'),
(46, 16, 17, '1 - CP12X12O', '605457705705', 10, 10, 0, 'Containment Pad 12\' x 12\'', 'CP12X12', 'Per Day', '14.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:30:48', '2020-03-23 12:30:48'),
(47, 16, 17, '2 - CP12X12T', '625359414546', 10, 10, 0, 'Containment Pad 12\' x 12\'', 'CP12X12', 'Per Day', '14.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:31:29', '2020-03-23 12:31:29'),
(48, 16, 17, '3 - CP12X12U', '454927006407', 10, 10, 0, 'Containment Pad 12\' x 12\'', 'CP12X12', 'Per Day', '14.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:31:56', '2020-03-23 12:31:56'),
(49, 17, 18, '1 - CP12X20O', '684403685851', 10, 10, 0, 'Containment Pad 12\' x 20\'', 'CP12X20', 'Per Day', '15.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:32:49', '2020-03-23 12:32:49'),
(50, 17, 18, '2 - CP12X20T', '718029868316', 10, 10, 0, 'Containment Pad 12\' x 20\'', 'CP12X20', 'Per Day', '15.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:33:27', '2020-03-23 12:33:27'),
(51, 17, 18, '3 - CP12X20U', '470936344505', 10, 10, 0, 'Containment Pad 12\' x 20\'', 'CP12X20', 'Per Day', '15.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:33:51', '2020-03-23 12:33:51'),
(52, 18, 19, '1 - CP12X24O', '417514880295', 10, 10, 0, 'Containment Pad 12 x 24\'', 'CP12X24', 'Per Day', '16.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:34:46', '2020-03-23 12:34:46'),
(53, 18, 19, '2 - CP12X24T', '517151637739', 10, 10, 0, 'Containment Pad 12 x 24\'', 'CP12X24', 'Per Day', '16.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:35:12', '2020-03-23 12:35:12'),
(54, 18, 19, '3 - CP12X24U', '976088917200', 10, 10, 0, 'Containment Pad 12 x 24\'', 'CP12X24', 'Per Day', '16.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:35:36', '2020-03-23 12:35:36'),
(55, 19, 20, '1 - CP12X30O', '206030134030', 10, 10, 0, 'Containment Pad 12\' x 30\'', 'CP12X30', 'Per Day', '20.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:37:03', '2020-03-23 12:37:03'),
(56, 19, 20, '2 - CP12X30T', '670996885652', 10, 10, 0, 'Containment Pad 12\' x 30\'', 'CP12X30', 'Per Day', '20.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:37:28', '2020-03-23 12:37:28'),
(57, 19, 20, '3 - CP12X30U', '360991979833', 10, 10, 0, 'Containment Pad 12\' x 30\'', 'CP12X30', 'Per Day', '20.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:37:53', '2020-03-23 12:37:53'),
(58, 20, 21, '1 - CP12X36O', '431109163161', 10, 10, 0, 'Containment Pad 12\' x 36\'', 'CP12X36', 'Per Day', '24.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:38:39', '2020-03-23 12:38:39'),
(59, 20, 21, '2 - CP12X36T', '326140364953', 10, 10, 0, 'Containment Pad 12\' x 36\'', 'CP12X36', 'Per Day', '24.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:39:04', '2020-03-23 12:39:04'),
(60, 20, 21, '3 - CP12X36U', '695050269731', 10, 10, 0, 'Containment Pad 12\' x 36\'', 'CP12X36', 'Per Day', '24.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:39:45', '2020-03-23 12:39:45'),
(61, 21, 22, '1 - CP12X40O', '509036649442', 10, 10, 0, 'Containment Pad 12\' x 40\'', 'CP12X40', 'Per Day', '26.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:40:39', '2020-03-23 12:40:39'),
(62, 21, 22, '2 - CP12X40T', '877306346680', 10, 10, 0, 'Containment Pad 12\' x 40\'', 'CP12X40', 'Per Day', '26.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:41:04', '2020-03-23 12:41:04'),
(63, 21, 22, '3 - CP12X40U', '947887413035', 10, 10, 0, 'Containment Pad 12\' x 40\'', 'CP12X40', 'Per Day', '26.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:41:29', '2020-03-23 12:41:29'),
(64, 22, 23, '1 - CP12X50O', '765723726457', 10, 10, 0, 'Containment Pad 12\' x 50\'', 'CP12X50', 'Per Day', '30.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:42:12', '2020-03-23 12:42:12'),
(65, 22, 23, '2 - CP12X50T', '944297981315', 10, 10, 0, 'Containment Pad 12\' x 50\'', 'CP12X50', 'Per Day', '30.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:42:38', '2020-03-23 12:42:38'),
(66, 22, 23, '3 - CP12X50U', '743891560840', 10, 10, 0, 'Containment Pad 12\' x 50\'', 'CP12X50', 'Per Day', '30.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:43:06', '2020-03-23 12:43:06'),
(67, 23, 24, '1 - CP12X50X12O', '908056139010', 10, 10, 0, 'Containment Pad 12\' x 50\' x 12\'', 'CP12X50X12', 'Per Day', '39.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:43:48', '2020-03-23 12:43:48'),
(68, 23, 24, '2 - CP12X50X12T', '191679636171', 10, 10, 0, 'Containment Pad 12\' x 50\' x 12\'', 'CP12X50X12', 'Per Day', '39.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:44:09', '2020-03-23 12:44:09'),
(69, 23, 24, '3 - CP12X50X12U', '322157414787', 10, 10, 0, 'Containment Pad 12\' x 50\' x 12\'', 'CP12X50X12', 'Per Day', '39.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:44:40', '2020-03-23 12:44:40'),
(70, 24, 25, '1 - CP12X60O', '465107744125', 10, 10, 0, 'Containment Pad 12\' x 60\'', 'CP12X60', 'Per Day', '35.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:45:31', '2020-03-23 12:45:31'),
(71, 24, 25, '2 - CP12X60T', '133906239633', 10, 10, 0, 'Containment Pad 12\' x 60\'', 'CP12X60', 'Per Day', '35.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:45:54', '2020-03-23 12:45:54'),
(72, 24, 25, '3 - CP12X60U', '403670282661', 10, 10, 0, 'Containment Pad 12\' x 60\'', 'CP12X60', 'Per Day', '35.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:46:18', '2020-03-23 12:46:18'),
(73, 25, 26, '1 - CP15X60O', '320994404963', 10, 10, 0, 'Containment Pad 15\' x 60\'', 'CP15X60', 'Per Day', '49.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:47:23', '2020-03-23 12:47:23'),
(74, 25, 26, '2 - CP15X60T', '995457364317', 10, 10, 0, 'Containment Pad 15\' x 60\'', 'CP15X60', 'Per Day', '49.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:47:55', '2020-03-23 12:47:55'),
(75, 26, 27, '1 - CP12X70O', '442383285124', 10, 10, 0, 'Containment Pad 12\' x 70\'', 'CP12X70', 'Per Day', '49.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:48:49', '2020-03-23 12:48:49'),
(76, 26, 27, '2 - CP12X70T', '892169909284', 10, 10, 0, 'Containment Pad 12\' x 70\'', 'CP12X70', 'Per Day', '49.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:49:12', '2020-03-23 12:49:12'),
(77, 26, 27, '3 - CP12X70U', '100071195302', 10, 10, 0, 'Containment Pad 12\' x 70\'', 'CP12X70', 'Per Day', '49.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:49:43', '2020-03-23 12:49:43'),
(78, 27, 28, '1 - CP12X80O', '868633163100', 10, 10, 0, 'Containment Pad 12\' x 80\'', 'CP12X80', 'Per Day', '56.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:50:58', '2020-03-23 12:50:58'),
(79, 27, 28, '2 - CP12X80T', '972857020956', 10, 10, 0, 'Containment Pad 12\' x 80\'', 'CP12X80', 'Per Day', '56.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:51:22', '2020-03-23 12:51:22'),
(80, 27, 28, '3 - CP12X80U', '221873945742', 10, 10, 0, 'Containment Pad 12\' x 80\'', 'CP12X80', 'Per Day', '56.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:51:46', '2020-03-23 12:51:46'),
(81, 28, 29, '1 - CP15X80O', '684334779627', 10, 10, 0, 'Containment Pad 15\' x 80', 'CP15X80', 'Per Day', '75.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:52:38', '2020-03-23 12:52:38'),
(82, 28, 29, '2 - CP15X80T', '631217058491', 10, 10, 0, 'Containment Pad 15\' x 80', 'CP15X80', 'Per Day', '75.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:53:07', '2020-03-23 12:53:07'),
(83, 28, 29, '3 - CP15X80U', '271893584647', 10, 10, 0, 'Containment Pad 15\' x 80', 'CP15X80', 'Per Day', '75.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:53:34', '2020-03-23 12:53:34'),
(84, 29, 30, '1 - CP31X51X6O', '460163127308', 10, 10, 0, 'Containment Pad 31\' x 51\' x 6\'', 'CP31X51X6', 'Per Day', '81.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:54:18', '2020-03-23 12:54:18'),
(85, 29, 30, '2 - CP31X51X6T', '961576265804', 10, 10, 0, 'Containment Pad 31\' x 51\' x 6\'', 'CP31X51X6', 'Per Day', '81.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:54:41', '2020-03-23 12:54:41'),
(86, 29, 30, '3 - CP31X51X6U', '308857947931', 10, 10, 0, 'Containment Pad 31\' x 51\' x 6\'', 'CP31X51X6', 'Per Day', '81.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:55:05', '2020-03-23 12:55:05'),
(87, 30, 31, '1 - CP31X51X12O', '506531722265', 10, 10, 0, 'Containment Pad 31\' x 51\' x 12\'', 'CP31X51X12', 'Per Day', '91.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:55:54', '2020-03-23 12:55:54'),
(88, 30, 31, '2 - CP31X51X12T', '247074762051', 10, 10, 0, 'Containment Pad 31\' x 51\' x 12\'', 'CP31X51X12', 'Per Day', '91.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:56:30', '2020-03-23 12:56:30'),
(89, 30, 31, '3 - CP31X51X12U', '425166832827', 10, 10, 0, 'Containment Pad 31\' x 51\' x 12\'', 'CP31X51X12', 'Per Day', '91.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:56:58', '2020-03-23 12:56:58'),
(90, 31, 32, '1 - RRURDO', '115023573567', 10, 10, 0, 'Restraint Rig up & Down', 'RRURD', 'Per Day', '0.45', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:57:55', '2020-03-23 12:57:55'),
(91, 31, 32, '2 - RRURDT', '420519378921', 10, 10, 0, 'Restraint Rig up & Down', 'RRURD', 'Per Day', '0.45', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:58:24', '2020-03-23 12:58:24'),
(92, 31, 32, '3 - RRURDU', '447459318476', 10, 10, 0, 'Restraint Rig up & Down', 'RRURD', 'Per Day', '0.45', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:58:53', '2020-03-23 12:58:53'),
(93, 32, 33, '1 - ACDPRO', '726633563485', 10, 10, 0, 'Acid Pump Rental', 'ACDPR', 'Per Month', '4500.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 12:59:59', '2020-03-23 12:59:59'),
(94, 32, 33, '2 - ACDPRT', '613968332144', 10, 10, 0, 'Acid Pump Rental', 'ACDPR', 'Per Month', '4500.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:00:33', '2020-03-23 13:00:33'),
(95, 32, 33, '3 - ACDPRU', '876707725160', 10, 10, 0, 'Acid Pump Rental', 'ACDPR', 'Per Month', '4500.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:01:07', '2020-03-23 13:01:07'),
(96, 33, 34, '1 - DSTPNLRO', 'BFXWWSMQBKN7NU1M9', 10, 10, 0, 'Distrobution Panel Rental', 'DSTPNLR', 'Per Month', '2000.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:02:04', '2020-03-23 13:02:04'),
(97, 33, 34, '2 - DSTPNLRT', '250435954386', 10, 10, 0, 'Distrobution Panel Rental', 'DSTPNLR', 'Per Month', '2000.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:02:47', '2020-03-23 13:02:47'),
(98, 33, 34, '3 - DSTPNLRU', 'ZS4HHICRAP2ZP4I3S', 10, 10, 0, 'Distrobution Panel Rental', 'DSTPNLR', 'Per Month', '2000.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:03:30', '2020-03-23 13:03:30'),
(99, 34, 35, '1 - CFGLPRO', 'CZT6QQKGTB8IDBKP6', 10, 10, 0, 'Centrifugal Pump Rental', 'CFGLPR', 'Per Day', '50.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:04:47', '2020-03-23 13:04:47'),
(100, 34, 35, '2 - CFGLPRT', 'FLSGZLDGXUPSR4FH7', 10, 10, 0, 'Centrifugal Pump Rental', 'CFGLPR', 'Per Day', '50.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:05:21', '2020-03-23 13:05:21'),
(101, 34, 35, '3 - CFGLPRU', 'PZEV4PVRLL4YJP4XA', 10, 10, 0, 'Centrifugal Pump Rental', 'CFGLPR', 'Per Day', '50.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:05:50', '2020-03-23 13:05:50'),
(102, 35, 36, '1 - LTWRRO', '2A96EL88GW2NDHP4R', 10, 10, 0, 'Light Tower Rental', 'LTWRR', 'Per Day', '50.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:07:32', '2020-03-23 13:07:32'),
(103, 35, 36, '2 - LTWRRT', 'J2ADBPBCVYMZGS8MK', 10, 10, 0, 'Light Tower Rental', 'LTWRR', 'Per Day', '50.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:08:09', '2020-03-23 13:08:09'),
(104, 35, 36, '3 - LTWRRU', '6BVQGGYD6CUXWPKLK', 10, 10, 0, 'Light Tower Rental', 'LTWRR', 'Per Day', '50.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:08:54', '2020-03-23 13:08:54'),
(105, 36, 37, '1 - CWLKPRKRO', '3EN2UKZH1AW53N7PT', 10, 10, 0, 'Catwalk & Pipe Racks', 'CWLKPRKR', 'Per Month', '2700.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:09:43', '2020-03-23 13:09:43'),
(106, 36, 37, '2 - CWLKPRKRT', 'ABXDL84794DZBVR98', 10, 10, 0, 'Catwalk & Pipe Racks', 'CWLKPRKR', 'Per Month', '2700.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:10:15', '2020-03-23 13:10:15'),
(107, 37, 38, '1 - TTGBKTO', '520524660808', 10, 10, 0, 'Trash Trailer Grease Bucket', 'TTGBKT', 'Per Day', '750.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:11:14', '2020-03-23 13:11:14'),
(108, 37, 38, '2 - TTGBKTT', '979719485013', 10, 10, 0, 'Trash Trailer Grease Bucket', 'TTGBKT', 'Per Day', '750.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:11:41', '2020-03-23 13:11:41'),
(109, 37, 38, '3 - TTGBKTU', '962215381801', 10, 10, 0, 'Trash Trailer Grease Bucket', 'TTGBKT', 'Per Day', '750.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:12:08', '2020-03-23 13:12:08'),
(110, 38, 39, '1 - TTSHLGO', '496575505876', 10, 10, 0, 'Trash Trailer Sand Hauling', 'TTSHLG', 'Per Day', '6.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:12:58', '2020-03-23 13:12:58'),
(111, 38, 39, '2 - TTSHLGT', '461218507610', 10, 10, 0, 'Trash Trailer Sand Hauling', 'TTSHLG', 'Per Day', '6.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:13:27', '2020-03-23 13:13:27'),
(112, 38, 39, '3 - TTSHLGU', '918049324391', 10, 10, 0, 'Trash Trailer Sand Hauling', 'TTSHLG', 'Per Day', '6.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:14:04', '2020-03-23 13:14:04'),
(113, 39, 40, '1 - TTDUMPO', '7PNDYFWWQTFBY4C4C', 10, 10, 0, 'Trash Trailer Transport Dump Fee', 'TTDUMP', 'Per Transport', '225.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:15:05', '2020-03-23 13:15:05'),
(114, 39, 40, '2 - TTDUMP', '969W3XS9MV981EJ1X', 10, 10, 0, 'Trash Trailer Transport Dump Fee', 'TTDUMP', 'Per Transport', '225.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:15:35', '2020-03-23 13:15:35'),
(115, 39, 40, '3 - TTDUMPU', 'NZJP42VSW2RWXLQAJ', 10, 10, 0, 'Trash Trailer Transport Dump Fee', 'TTDUMP', 'Per Transport', '225.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:16:10', '2020-03-23 13:16:10'),
(116, 40, 41, '1 - TTTONWO', '885932058400', 10, 10, 0, 'Trash Trailer Ton Weight', 'TTTONW', 'Per Ton Over 8 Tons', '75.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:17:31', '2020-03-23 13:17:31'),
(117, 40, 41, '2 - TTTONWT', '109927787927', 10, 10, 0, 'Trash Trailer Ton Weight', 'TTTONW', 'Per Ton Over 8 Tons', '75.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:18:03', '2020-03-23 13:18:03'),
(118, 40, 41, '3 - TTTONWU', '202637408929', 10, 10, 0, 'Trash Trailer Ton Weight', 'TTTONW', 'Per Ton Over 8 Tons', '75.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:18:54', '2020-03-23 13:18:54'),
(119, 41, 42, '1 - TTOVR8TW', '114285394174', 10, 10, 0, 'Trash Trailer Over 8 Tons', 'TTOVR8TW', 'Per Ton Over 8 Tons', '25.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:19:40', '2020-03-23 13:19:40'),
(120, 41, 42, '2 - TTOVR8TWT', '115906966463', 10, 10, 0, 'Trash Trailer Over 8 Tons', 'TTOVR8TW', 'Per Ton Over 8 Tons', '25.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:20:11', '2020-03-23 13:20:11'),
(121, 42, 43, '1 - POC3O', '247337317017', 10, 10, 0, 'Pump Oil Change 300', 'POC3', '# of 300', '880.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:21:02', '2020-03-23 13:21:02'),
(122, 42, 43, '2 - POC3T', '192880303644', 10, 10, 0, 'Pump Oil Change 300', 'POC3', '# of 300', '880.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:21:35', '2020-03-23 13:21:35'),
(123, 42, 43, '3 - POC3U', '384704036805', 10, 10, 0, 'Pump Oil Change 300', 'POC3', '# of 300', '880.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:22:05', '2020-03-23 13:22:05'),
(124, 43, 44, '1 - POC36O', '650369337742', 10, 10, 0, 'Pump Oil Cghange 300 | 600', 'POC36', '# of 300 | 600', '1030.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:23:29', '2020-03-23 13:23:29'),
(125, 43, 44, '2 - POC36T', '157233666742', 10, 10, 0, 'Pump Oil Cghange 300 | 600', 'POC36', '# of 300 | 600', '1030.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:24:03', '2020-03-23 13:24:03'),
(126, 44, 45, '1 - POC369T', '937191204058', 10, 10, 0, 'Pump Oil change 300 | 600 | 900', 'POC369', '# of 300 | 600 | 900', '1280.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:24:55', '2020-03-23 13:24:55'),
(127, 44, 45, '2 - POC369O', '758381408575', 10, 10, 0, 'Pump Oil change 300 | 600 | 900', 'POC369', '# of 300 | 600 | 900', '1280.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:26:24', '2020-03-23 13:26:24'),
(128, 44, 45, '3 - POC369U', '507203748129', 10, 10, 0, 'Pump Oil change 300 | 600 | 900', 'POC369', '# of 300 | 600 | 900', '1280.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:27:05', '2020-03-23 13:27:05'),
(129, 45, 46, '1 - TOC3O', 'FS291ARQHE3RKE94G', 10, 10, 0, 'Tractor Oil Change 300', 'TOC3', '# of 300', '75.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:28:19', '2020-03-23 13:28:19'),
(130, 45, 46, '2 - TOC3T', '52XWYFFQNLDXU4MHY', 10, 10, 0, 'Tractor Oil Change 300', 'TOC3', '# of 300', '75.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:28:48', '2020-03-23 13:28:48'),
(131, 46, 47, '1 - TOC6O', '974MW5XS2PM4DLKMR', 10, 10, 0, 'Tractor Oil change 600', 'TOC6', '# of 600', '400.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:29:52', '2020-03-23 13:29:52'),
(132, 46, 47, '2 - TOC6T', '4NQBAJRVZ7A7IRDSY', 10, 10, 0, 'Tractor Oil change 600', 'TOC6', '# of 600', '400.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:30:24', '2020-03-23 13:30:24'),
(133, 46, 47, '3 - TOC6U', 'QZK7L9PQM79FGAL2A', 10, 10, 0, 'Tractor Oil change 600', 'TOC6', '# of 600', '400.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:31:13', '2020-03-23 13:31:13'),
(134, 47, 48, '1 - TOC9O', 'HQ818TYD3PFMWG1YB', 10, 10, 0, 'Tractor Oil change 900', 'TOC9', '# of 900', '600.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:31:57', '2020-03-23 13:31:57'),
(135, 47, 48, '2 - TOC9T', 'AXP9RHF1417RSESG4', 10, 10, 0, 'Tractor Oil change 900', 'TOC9', '# of 900', '600.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:32:29', '2020-03-23 13:32:29'),
(136, 47, 48, '3 - TOC9U', 'S9FV89ZNTRS4K93UA', 10, 10, 0, 'Tractor Oil change 900', 'TOC9', '# of 900', '600.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:33:07', '2020-03-23 13:33:07'),
(137, 48, 49, '1 - SBOCO', '240251620490', 10, 10, 0, 'Sandbox Oil Change (300 Sandbox King)', 'SBOC', '# of 300 Sandbox Kings', '250.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:34:03', '2020-03-23 13:34:03'),
(138, 48, 49, '2 - SBOCT', '909551439742', 10, 10, 0, 'Sandbox Oil Change (300 Sandbox King)', 'SBOC', '# of 300 Sandbox Kings', '250.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:34:33', '2020-03-23 13:34:33'),
(139, 48, 49, '3 - SBOCU', '565924504438', 10, 10, 0, 'Sandbox Oil Change (300 Sandbox King)', 'SBOC', '# of 300 Sandbox Kings', '250.00', NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-23 13:35:08', '2020-03-23 13:35:08');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_unit_details`
--

CREATE TABLE `inventory_unit_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `inventory_id` int(10) UNSIGNED NOT NULL,
  `serial_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upc_vin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additional_info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serial_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inventory_unit_details`
--

INSERT INTO `inventory_unit_details` (`id`, `inventory_id`, `serial_name`, `upc_vin`, `additional_info`, `properties`, `serial_image`, `created_at`, `updated_at`) VALUES
(1, 1, 'VT', NULL, NULL, '[{\"key\":\"model name\",\"type\":\"string\",\"unit_param_id\":\"1\",\"value\":\"peterbilt 2019\"},{\"key\":\"liability\",\"type\":\"checkbox\",\"unit_param_id\":\"2\",\"value\":\"no\"},{\"key\":\"collision\",\"type\":\"checkbox\",\"unit_param_id\":\"3\",\"value\":\"no\"},{\"key\":\"service date\",\"type\":\"datetime\",\"unit_param_id\":\"4\",\"value\":\"08\\/12\\/2020\"},{\"key\":\"lic plate\",\"type\":\"string\",\"unit_param_id\":\"5\",\"value\":\"2sad34asdfd\"},{\"key\":\"gps\",\"type\":\"string\",\"unit_param_id\":\"6\",\"value\":\"16dt3423d\"},{\"key\":\"VIN number\",\"type\":\"vin_number\",\"unit_param_id\":\"7\",\"value\":\"1XRTMB2I1P8SGEQAH\"}]', NULL, '2020-08-13 01:25:55', '2020-08-13 02:00:17'),
(2, 1, 'VT', NULL, NULL, '[{\"key\":\"model name\",\"type\":\"string\",\"unit_param_id\":\"1\",\"value\":\"peterbilt 2012\"},{\"key\":\"liability\",\"type\":\"checkbox\",\"unit_param_id\":\"2\",\"value\":\"no\"}]', NULL, '2020-08-13 01:28:15', '2020-09-02 04:56:21'),
(3, 2, 'rs', NULL, NULL, '[{\"key\":\"param1\",\"type\":\"string\",\"unit_param_id\":\"8\",\"value\":\"test\"},{\"key\":\"param2\",\"type\":\"checkbox\",\"unit_param_id\":\"9\",\"value\":\"no\"},{\"key\":\"param3\",\"type\":\"checkbox\",\"unit_param_id\":\"10\",\"value\":\"yes\"},{\"key\":\"param4\",\"type\":\"decimal\",\"unit_param_id\":\"11\",\"value\":\"123.23\"},{\"key\":\"param6\",\"type\":\"integer\",\"unit_param_id\":\"12\",\"value\":null},{\"key\":\"param7\",\"type\":\"string\",\"unit_param_id\":\"13\",\"value\":null},{\"key\":\"param8\",\"type\":\"checkbox\",\"unit_param_id\":\"14\"},{\"key\":\"param9\",\"type\":\"datetime\",\"unit_param_id\":\"15\",\"value\":null},{\"key\":\"param10\",\"type\":\"checkbox\",\"unit_param_id\":\"16\"}]', NULL, '2020-09-01 06:51:11', '2020-09-03 11:30:23'),
(4, 2, 'rs111', NULL, NULL, '[{\"key\":\"param1\",\"type\":\"string\",\"unit_param_id\":\"8\",\"value\":\"test2222\"},{\"key\":\"param2\",\"type\":\"checkbox\",\"unit_param_id\":\"9\",\"value\":\"yes\"},{\"key\":\"param3\",\"type\":\"checkbox\",\"unit_param_id\":\"10\",\"value\":\"yes\"},{\"key\":\"param4\",\"type\":\"decimal\",\"unit_param_id\":\"11\",\"value\":\"80.56\"},{\"key\":\"param6\",\"type\":\"integer\",\"unit_param_id\":\"12\",\"value\":\"122\"}]', NULL, '2020-09-01 06:52:12', '2020-09-02 00:27:50'),
(6, 2, 'RSSS', NULL, NULL, '[{\"key\":\"param1\",\"type\":\"string\",\"unit_param_id\":\"8\",\"value\":\"das sa\"},{\"key\":\"param2\",\"type\":\"checkbox\",\"unit_param_id\":\"9\",\"value\":\"no\"},{\"key\":\"param3\",\"type\":\"checkbox\",\"unit_param_id\":\"10\",\"value\":\"no\"},{\"key\":\"param4\",\"type\":\"decimal\",\"unit_param_id\":\"11\",\"value\":\"123\"},{\"key\":\"param6\",\"type\":\"integer\",\"unit_param_id\":\"12\",\"value\":\"122\"}]', NULL, '2020-09-02 01:25:48', '2020-09-02 01:25:48'),
(8, 2, 'rs4', NULL, NULL, '[{\"key\":\"param1\",\"type\":\"string\",\"unit_param_id\":\"8\",\"value\":\"test2222\"},{\"key\":\"param2\",\"type\":\"checkbox\",\"unit_param_id\":\"9\",\"value\":\"yes\"},{\"key\":\"param3\",\"type\":\"checkbox\",\"unit_param_id\":\"10\",\"value\":\"yes\"},{\"key\":\"param4\",\"type\":\"decimal\",\"unit_param_id\":\"11\",\"value\":\"12\"},{\"key\":\"param6\",\"type\":\"integer\",\"unit_param_id\":\"12\",\"value\":\"234\"}]', NULL, '2020-09-02 05:20:46', '2020-09-02 05:20:46');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_04_06_084205_create_vendors_table', 2),
(4, '2020_04_08_123005_add_reg_license_no_to_vendors_table', 3),
(5, '2020_04_13_115027_create_variable_table', 4),
(6, '2020_04_13_115506_create_variables_table', 5),
(7, '2020_04_14_104502_create_vendor_resources_table', 6),
(8, '2020_04_14_105930_create_vendor_resources_table', 7),
(9, '2020_04_15_111627_create_images_table', 8),
(10, '2020_04_21_113627_create_service_location_address_table', 9),
(11, '2020_05_03_055116_create_vendors_table', 10),
(12, '2020_05_03_074135_create_vendors_table', 11),
(13, '2020_06_28_064115_add_new_fields_to_insurance_equipment_request_table', 12),
(14, '2020_06_28_065558_add_new_fields_to_insurance_vehicle_request_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `open_invoice_settings`
--

CREATE TABLE `open_invoice_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `public_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secret_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `owner_agreement`
--

CREATE TABLE `owner_agreement` (
  `id` int(11) NOT NULL,
  `owner_day` varchar(191) DEFAULT NULL,
  `owner_month` varchar(191) DEFAULT NULL,
  `owner_year` varchar(191) DEFAULT NULL,
  `owner_name` varchar(191) DEFAULT NULL,
  `owner_type_of_entity` varchar(191) DEFAULT NULL,
  `owner_mailing_addr` varchar(191) DEFAULT NULL,
  `owner_physical_addr` varchar(191) DEFAULT NULL,
  `owner_contact_person` varchar(191) DEFAULT NULL,
  `owner_contact_person_title` varchar(191) DEFAULT NULL,
  `owner_contact_person_email` varchar(191) DEFAULT NULL,
  `owner_contact_person_phone` varchar(191) DEFAULT NULL,
  `owner_contact_person_fax` varchar(191) DEFAULT NULL,
  `notice_owner_contractor_name` varchar(191) DEFAULT NULL,
  `notice_owner_contractor_address` varchar(191) DEFAULT NULL,
  `notice_owner_contractor_phone` varchar(191) DEFAULT NULL,
  `notice_owner_contractor_email` varchar(191) DEFAULT NULL,
  `notice_owner_contractor_attn` varchar(191) DEFAULT NULL,
  `notice_owner_operator_name` varchar(191) DEFAULT NULL,
  `notice_owner_operator_address` varchar(191) DEFAULT NULL,
  `notice_owner_operator_phone` varchar(191) DEFAULT NULL,
  `notice_owner_operator_email` varchar(191) DEFAULT NULL,
  `notice_owner_operator_contact` varchar(191) DEFAULT NULL,
  `owner_admin_fee` varchar(191) DEFAULT NULL,
  `owner_truck_set_up_fee` varchar(191) DEFAULT NULL,
  `owner_decals_fee` varchar(191) DEFAULT NULL,
  `owner_gps_installation_fee` varchar(191) DEFAULT NULL,
  `owner_gps_monitoring_fee` varchar(191) DEFAULT NULL,
  `owner_drug_testing_fee` varchar(191) DEFAULT NULL,
  `owner_training_fee` varchar(191) DEFAULT NULL,
  `owner_mvr_fee` varchar(191) DEFAULT NULL,
  `owner_ppe_fee` varchar(191) DEFAULT NULL,
  `owner_dot_cab_card_fee` varchar(191) DEFAULT NULL,
  `owner_ins_per_truck_fee` varchar(191) DEFAULT NULL,
  `field_tickets_fee` varchar(191) DEFAULT NULL,
  `executed_day_owner` varchar(191) DEFAULT NULL,
  `executed_month_owner` varchar(191) DEFAULT NULL,
  `executed_year_owner` varchar(191) DEFAULT NULL,
  `executed_owner_contractor_name` varchar(191) DEFAULT NULL,
  `executed_owner_contractor_by` varchar(191) DEFAULT NULL,
  `executed_owner_contractor_title` varchar(191) DEFAULT NULL,
  `executed_owner_contractor_date` varchar(191) DEFAULT NULL,
  `executed_owner_contractor_initial` varchar(191) DEFAULT NULL,
  `executed_owner_operator_name` varchar(191) DEFAULT NULL,
  `executed_owner_operator_by` varchar(191) DEFAULT NULL,
  `executed_owner_operator_title` varchar(191) DEFAULT NULL,
  `executed_owner_operator_date` varchar(191) DEFAULT NULL,
  `executed_owner_operator_initial` varchar(191) DEFAULT NULL,
  `submitted_by` varchar(191) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `agreement_send_mail_to` varchar(191) DEFAULT NULL,
  `agreement_send_mail_date` varchar(191) DEFAULT NULL,
  `submitted_by_name` varchar(191) DEFAULT NULL,
  `submitted_by_email` varchar(191) DEFAULT NULL,
  `signed_by` varchar(191) DEFAULT NULL,
  `submitted_to_insurance` varchar(191) DEFAULT NULL,
  `submitted_date_insurance` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `access_code` varchar(191) DEFAULT NULL,
  `url_token` varchar(191) DEFAULT NULL,
  `evos_access_code` varchar(191) DEFAULT NULL,
  `evos_url_token` varchar(191) DEFAULT NULL,
  `reject` text DEFAULT NULL,
  `sent` text DEFAULT NULL,
  `seen` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owner_agreement`
--

INSERT INTO `owner_agreement` (`id`, `owner_day`, `owner_month`, `owner_year`, `owner_name`, `owner_type_of_entity`, `owner_mailing_addr`, `owner_physical_addr`, `owner_contact_person`, `owner_contact_person_title`, `owner_contact_person_email`, `owner_contact_person_phone`, `owner_contact_person_fax`, `notice_owner_contractor_name`, `notice_owner_contractor_address`, `notice_owner_contractor_phone`, `notice_owner_contractor_email`, `notice_owner_contractor_attn`, `notice_owner_operator_name`, `notice_owner_operator_address`, `notice_owner_operator_phone`, `notice_owner_operator_email`, `notice_owner_operator_contact`, `owner_admin_fee`, `owner_truck_set_up_fee`, `owner_decals_fee`, `owner_gps_installation_fee`, `owner_gps_monitoring_fee`, `owner_drug_testing_fee`, `owner_training_fee`, `owner_mvr_fee`, `owner_ppe_fee`, `owner_dot_cab_card_fee`, `owner_ins_per_truck_fee`, `field_tickets_fee`, `executed_day_owner`, `executed_month_owner`, `executed_year_owner`, `executed_owner_contractor_name`, `executed_owner_contractor_by`, `executed_owner_contractor_title`, `executed_owner_contractor_date`, `executed_owner_contractor_initial`, `executed_owner_operator_name`, `executed_owner_operator_by`, `executed_owner_operator_title`, `executed_owner_operator_date`, `executed_owner_operator_initial`, `submitted_by`, `status`, `agreement_send_mail_to`, `agreement_send_mail_date`, `submitted_by_name`, `submitted_by_email`, `signed_by`, `submitted_to_insurance`, `submitted_date_insurance`, `created_at`, `updated_at`, `access_code`, `url_token`, `evos_access_code`, `evos_url_token`, `reject`, `sent`, `seen`) VALUES
(1, '2', 'january', '2020', 'qw', 'llc', 'qwe@asdsa.com', 'asdad', 'dasd', 'sdsa', 'asdas@sadsa.com', NULL, '(312) 321-3123', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sdsad', '31232', '(312) 312-3123', '1312@sadasd.com', '13213', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', 'may', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/18/2020', 'EVOS', '23123', '31231', '3123', '08/18/2020', '3123', NULL, 2, 'looklike.himu@gmail.com', '08/18/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-17 23:23:24', '2020-08-18 02:50:47', '971315', '1-ZO4GERQU7SN8WXM9FPLCVYJ1AIKH3B', NULL, NULL, '<p>1. eqeqwe</p><p>2. saddasddasd</p>', '[\"2020-08-18T05:23:24.036507Z\",\"2020-08-18T08:42:55.748749Z\"]', '[\"2020-08-18T06:48:14.340990Z\",\"2020-08-18T06:48:59.260115Z\",\"2020-08-18T06:49:19.288002Z\",\"2020-08-18T06:49:57.384490Z\",\"2020-08-18T08:43:23.644651Z\",\"2020-08-18T08:44:13.004467Z\",\"2020-08-18T08:47:04.762982Z\",\"2020-08-18T08:50:32.490735Z\"]'),
(2, NULL, NULL, '2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/18/2020', 'EVOS', NULL, NULL, NULL, NULL, NULL, NULL, 1, 'looklike.himu@gmail.com', '08/18/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-18 01:15:37', '2020-08-18 01:15:37', '197825', '2-XWCHS9TDG65L4ABJ1ZNM3FPU0Q82KE', NULL, NULL, NULL, '[\"2020-08-18T07:15:37.359664Z\"]', NULL),
(3, '2', 'february', '2020', 'wqe', 'llc', 'eqwe@asdas.com', 'asdad', 'sdasd', 'dsadsa', 'asdas@asdsa.com', '(312) 321-3123', '(312) 312-3123', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'eqweq', '123123', '(312) 321-3123', 'asdas@dsadas.com', 'dasdsad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/18/2020', 'EVOS', '23123', '31231', '3123', '08/19/2020', '3123', NULL, 3, 'looklike.himu@gmail.com', '08/18/2020', 'test1', 'eg.himel@gmail.com', NULL, 'looklike.himu@gmail.com', '08/18/2020', '2020-08-18 02:52:04', '2020-08-18 05:15:18', '822680', '3-DKJBTQ837CVU2MWIH96RLF5XPOSAZ0', NULL, NULL, NULL, '[\"2020-08-18T08:52:04.958009Z\",\"2020-08-18T09:57:31.433375Z\",\"2020-08-18T10:26:33.882742Z\"]', '[\"2020-08-18T08:53:08.103858Z\",\"2020-08-18T09:39:21.688468Z\",\"2020-08-18T09:58:14.799231Z\",\"2020-08-18T09:58:47.857286Z\",\"2020-08-18T10:07:23.227647Z\",\"2020-08-18T10:07:35.229444Z\",\"2020-08-18T10:08:00.304146Z\",\"2020-08-18T10:08:25.888624Z\",\"2020-08-18T10:08:37.573006Z\",\"2020-08-18T10:15:11.567325Z\",\"2020-08-18T10:24:47.547459Z\",\"2020-08-18T10:27:50.457612Z\",\"2020-08-18T10:54:50.272879Z\",\"2020-08-18T10:55:14.690057Z\",\"2020-08-18T10:56:12.223083Z\",\"2020-08-18T10:58:00.836763Z\",\"2020-08-18T11:01:55.399717Z\",\"2020-08-18T11:02:25.126287Z\",\"2020-08-18T11:03:04.904874Z\",\"2020-08-18T11:05:13.343844Z\",\"2020-08-18T11:06:18.748194Z\",\"2020-08-18T11:07:48.036584Z\",\"2020-08-18T11:11:20.870455Z\",\"2020-08-18T11:11:44.994959Z\",\"2020-08-18T11:13:16.249469Z\",\"2020-08-18T11:14:39.544197Z\",\"2020-08-18T11:15:18.293790Z\"]'),
(4, '2', 'february', '2020', 'sad', 'corporation', 'sadsa@asd.com', 'wsdsad', 'asd', '23123312312', 'asdsa@asdsad.com', '(123) 123-1231', '(312) 321-3123', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sa', 'sdsad', '(123) 123-1331', 'asdsa@asda.com', 'SADSAD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', 'january', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/18/2020', 'EVOS', 'dasd', 'adasd', 'asdas', '08/04/2020', 'aaaa', NULL, 3, 'looklike.himu@gmail.com', '08/18/2020', 'test1', 'eg.himel@gmail.com', NULL, 'looklike.himu@gmail.com', '08/18/2020', '2020-08-18 07:01:19', '2020-08-18 07:32:10', '452846', '4-56EUKZ0WVH3QY1CTLSDNOB79G2M8FP', NULL, NULL, NULL, '[\"2020-08-18T13:01:19.121223Z\",\"2020-08-18T13:19:53.047632Z\",\"2020-08-18T13:31:49.237120Z\"]', '[\"2020-08-18T13:02:11.585567Z\",\"2020-08-18T13:08:46.982611Z\",\"2020-08-18T13:12:36.185147Z\",\"2020-08-18T13:13:05.758054Z\",\"2020-08-18T13:14:15.534357Z\",\"2020-08-18T13:16:06.460133Z\",\"2020-08-18T13:20:21.366033Z\",\"2020-08-18T13:27:15.953447Z\",\"2020-08-18T13:29:45.557321Z\",\"2020-08-18T13:32:10.947160Z\"]'),
(5, NULL, NULL, '2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/18/2020', 'EVOS', NULL, NULL, NULL, NULL, NULL, NULL, 1, 'looklike.himu@gmail.com', '08/18/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-18 08:37:47', '2020-08-18 08:37:47', '554461', '5-9SQBDREN0OPHG4UYX1WM7KTF623IJV', NULL, NULL, NULL, '[\"2020-08-18T14:37:47.619099Z\"]', NULL),
(6, '1', 'february', '2020', 'wqe', 'llc', 'qwe@sdaas.com', 'asdasd', 'qsadsa', '3123', 'ewq@dasda.com', '(123) 123-1231', '(312) 312-3123', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'wqe', 'qwe@sdaas.com', '(123) 123-1231', 'ewq@dasda.com', 'qsadsa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/18/2020', 'EVOS', 'dasd', 'adasd', 'asdas', '08/11/2020', 'aaaa', NULL, 3, 'looklike.himu@gmail.com', '08/18/2020', 'test1', 'eg.himel@gmail.com', NULL, 'looklike.himu@gmail.com', '08/18/2020', '2020-08-18 09:56:55', '2020-08-18 10:02:03', '320835', '6-F8VXDHEI1OZ4GB5KRNSW6ULM73PJC2', NULL, NULL, NULL, '[\"2020-08-18T15:56:55.285661Z\",\"2020-08-18T16:00:02.552702Z\",\"2020-08-18T16:01:47.204511Z\"]', '[\"2020-08-18T15:57:48.225781Z\",\"2020-08-18T16:00:21.328428Z\",\"2020-08-18T16:02:03.646214Z\"]'),
(7, NULL, NULL, '2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/19/2020', 'EVOS', NULL, NULL, NULL, NULL, NULL, NULL, 1, 'looklike.himu@gmail.com', '08/19/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-19 06:31:26', '2020-08-19 06:40:24', '388067', '7-X9NBOCWQZHAKLDIJS6UVME7P3401RG', NULL, NULL, NULL, '[\"2020-08-19T12:31:26.842918Z\"]', '[\"2020-08-19T12:40:24.436956Z\"]'),
(8, '2', 'january', '2020', 'JHJJ', 'corporation', 'JJDD@ASD.COM', 'sadsd', 'QWEWQ', 'asd', 'sadsad@asdsa.com', '(312) 312-3123', '(312) 312-3123', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'JHJJ', 'JJDD@ASD.COM', '(312) 312-3123', 'sadsad@asdsa.com', 'QWEWQ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', 'january', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/19/2020', NULL, 'eqwe', 'ewqe', 'qwe', '08/03/2020', 'eqwe', NULL, 2, 'looklike.himu@gmail.com', '08/19/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-19 08:03:45', '2020-08-19 08:07:14', '930634', '8-JIP8DMG26NS49OZBH3XVF0T5LA1YE7', NULL, NULL, '<p>gn,n ,nb</p>', '[\"2020-08-19T14:03:45.285577Z\",\"2020-08-19T14:06:08.995770Z\"]', '[\"2020-08-19T14:04:02.416576Z\",\"2020-08-19T14:06:31.160763Z\"]'),
(9, '2', 'january', '2020', 'wqeqw', 'llc', 'dasd@asda.com', 'adasd', 'ad', 'asdasd', 'ada@asda.com', '(312) 312-3123', '(312) 313-1231', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'wqeqw', 'dasd@asda.com', '(312) 312-3123', 'ada@asda.com', 'ad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', 'january', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/20/2020', NULL, 'dasd', 'adasd', 'asdas', '08/19/2020', 'aaaa', NULL, 3, 'looklike.himu@gmail.com', '08/20/2020', 'test1', 'eg.himel@gmail.com', NULL, 'looklike.himu@gmail.com', '08/20/2020', '2020-08-20 08:23:48', '2020-08-20 08:43:27', '624278', '9-2N5Y6ZELB1UKVXDAOFIQMRC98TJ0PS', NULL, NULL, NULL, '[\"2020-08-20T14:23:48.242072Z\",\"2020-08-20T14:31:18.821539Z\",\"2020-08-20T14:33:17.726827Z\"]', '[\"2020-08-20T14:24:23.448990Z\",\"2020-08-20T14:25:58.999524Z\",\"2020-08-20T14:31:35.648887Z\",\"2020-08-20T14:34:43.486967Z\",\"2020-08-20T14:40:36.997597Z\",\"2020-08-20T14:41:13.210227Z\",\"2020-08-20T14:43:27.594315Z\"]'),
(10, '2', 'february', '2020', 'wqeqw', 'llc', 'dasd@asda.com', 'adasd', 'ad', 'asdasd', 'ada@asda.com', '(312) 312-3123', '(312) 313-1231', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'wqeqw', 'dasd@asda.com', '(312) 312-3123', 'ada@asda.com', 'ad', '50.00', '500.00', '180.00', '200.00', '45.00', '90.00', '350.00', '25.00', '100.00', '40.00', '175.00', '20.00', '12', 'march', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/23/2020', NULL, 'dasd', 'adasd', 'asdas', '08/25/2020', 'aaaa', NULL, 3, 'looklike.himu@gmail.com', '08/23/2020', 'test1', 'eg.himel@gmail.com', NULL, 'looklike.himu@gmail.com', '08/23/2020', '2020-08-23 06:10:35', '2020-08-23 09:04:09', '617200', '10-YO69HG5XT7RS1P43LJCA8MKBVWQFE2', NULL, NULL, NULL, '[\"2020-08-23T12:10:35.387304Z\",\"2020-08-23T13:16:24.927652Z\"]', '[\"2020-08-23T12:11:15.109746Z\",\"2020-08-23T12:11:57.411085Z\",\"2020-08-23T12:12:44.628599Z\",\"2020-08-23T12:21:10.019347Z\",\"2020-08-23T13:16:50.717681Z\",\"2020-08-23T13:18:28.214798Z\",\"2020-08-23T13:37:38.221856Z\",\"2020-08-23T13:38:09.260426Z\",\"2020-08-23T13:38:43.686106Z\",\"2020-08-23T13:53:13.098311Z\",\"2020-08-23T13:55:28.940670Z\",\"2020-08-23T13:56:45.885068Z\",\"2020-08-23T14:23:42.755544Z\",\"2020-08-23T14:28:37.917666Z\",\"2020-08-23T14:32:16.327414Z\",\"2020-08-23T15:04:09.298905Z\"]'),
(11, '2', 'january', '2020', 'asdsa', 'corporation', 'dasd@asda.com', 'adasd', 'ad', 'asdasd', 'ada@asda.com', '(312) 312-3123', '(312) 313-1231', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'asdsa', 'dasd@asda.com', '(312) 312-3123', 'ada@asda.com', 'ad', '50.00', '500.00', '180.00', '200.00', '45.00', '90.00', '350.00', '25.00', '100.00', '40.00', '175.00', '20.00', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '09/03/2020', NULL, 'dasd', 'adasd', 'asdas', '09/09/2020', 'aaaa', NULL, 3, 'eg.himel@gmail.com', '09/03/2020', 'test1', 'eg.himel@gmail.com', NULL, 'himel.biswas@asciisys.com', '09/03/2020', '2020-09-03 10:22:40', '2020-09-03 11:11:59', '558067', '11-ST0E9KJIQNPVM1B3FCWZH6R7XG4LD2', NULL, NULL, NULL, '[\"2020-09-03T16:22:40.373215Z\",\"2020-09-03T16:26:06.428103Z\",\"2020-09-03T16:29:27.461895Z\",\"2020-09-03T17:11:59.335482Z\"]', '[\"2020-09-03T16:23:29.786769Z\",\"2020-09-03T16:24:11.582817Z\",\"2020-09-03T16:27:19.618464Z\",\"2020-09-03T16:28:16.937269Z\",\"2020-09-03T16:28:29.166792Z\"]'),
(12, '2', 'april', '2020', 'wqeqw', 'limited_partnership', 'dasd@asda.com', 'adasd', 'ad', 'asdasd', 'ada@asda.com', '(312) 312-3123', NULL, 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'wqeqw', 'dasd@asda.com', '(312) 312-3123', 'ada@asda.com', 'ad', '50.00', '500.00', '180.00', '200.00', '45.00', '90.00', '350.00', '25.00', '100.00', '40.00', '175.00', '20.00', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '09/30/2020', 'EVOS', 'dasd', 'adasd', 'asdas', '09/14/2020', 'aaaa', NULL, 2, 'looklike.himu@gmail.com', '09/30/2020', 'test1', 'eg.himel@gmail.com', NULL, 'elizabeth@mcanallywilkins.com', '09/30/2020', '2020-09-30 00:45:28', '2020-09-30 01:06:33', '392109', '12-QGNP27O5MWK4LHCUV8I1DSATR3FXJ9', NULL, NULL, NULL, '[\"2020-09-30T06:45:28.399633Z\",\"2020-09-30T06:56:33.334544Z\"]', '[\"2020-09-30T06:52:15.501034Z\",\"2020-09-30T06:53:18.982765Z\",\"2020-09-30T06:54:25.907721Z\",\"2020-09-30T06:56:58.630806Z\"]'),
(13, '12', 'january', '2020', 'wqeqw sfsdfsd', 'corporation', 'dasd@asda.com', 'adasd', 'ad', 'asdasd', 'ada@asda.com', '(312) 312-3123', NULL, 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'wqeqw sfsdfsd', 'dasd@asda.com', '(312) 312-3123', 'ada@asda.com', 'ad', '50.00', '500.00', '180.00', '200.00', '45.00', '90.00', '350.00', '25.00', '100.00', '40.00', '175.00', '20.00', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '09/30/2020', NULL, 'dasd', 'adasd', 'asdas', '09/08/2020', 'aaaa', NULL, 2, 'looklike.himu@gmail.com', '09/30/2020', 'test1', 'eg.himel@gmail.com', NULL, 'elizabeth@mcanallywilkins.com', '09/30/2020', '2020-09-30 01:46:01', '2020-09-30 01:55:12', '567104', '13-8XYO6MIRWUA57K3HCDBNFGSZTP1409', NULL, NULL, NULL, '[\"2020-09-30T07:46:01.763900Z\",\"2020-09-30T07:51:05.673477Z\"]', '[\"2020-09-30T07:46:26.688765Z\",\"2020-09-30T07:51:27.301639Z\"]'),
(14, '2', 'february', '2020', 'wqeqw', 'corporation', 'dasd@asda.com', 'adasd', 'ad', 'asdasd', 'ada@asda.com', '(312) 312-3123', NULL, 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'wqeqw', 'dasd@asda.com', '(312) 312-3123', 'ada@asda.com', 'ad', '50.00', '500.00', '180.00', '200.00', '45.00', '90.00', '350.00', '25.00', '100.00', '40.00', '175.00', '20.00', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '09/30/2020', 'EVOS', 'dasd', 'adasd', 'asdas', '09/15/2020', 'aaaa', NULL, 2, 'looklike.himu@gmail.com', '09/30/2020', 'test1', 'eg.himel@gmail.com', NULL, 'elizabeth@mcanallywilkins.com', '09/30/2020', '2020-09-30 05:12:31', '2020-09-30 06:02:28', '268965', '14-8OD7CKW6ZG34EIQY1NB5UXMFTS9VHJ', NULL, NULL, NULL, '[\"2020-09-30T11:12:31.276123Z\"]', '[\"2020-09-30T11:13:12.748998Z\",\"2020-09-30T11:14:14.799537Z\"]'),
(15, '2', 'january', '2020', 'wqeqw', 'corporation', 'dasd@asda.com', 'adasd', 'ad', 'asdasd', 'ada@asda.com', '(312) 312-3123', NULL, 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'wqeqw', 'dasd@asda.com', '(312) 312-3123', 'ada@asda.com', 'ad', '50.00', '500.00', '180.00', '200.00', '45.00', '90.00', '350.00', '25.00', '100.00', '40.00', '175.00', '20.00', '12', 'january', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '09/30/2020', NULL, 'dasd', 'adasd', '3123', '09/15/2020', 'aaaa', NULL, 2, 'looklike.himu@gmail.com', '09/30/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-09-30 05:21:49', '2020-09-30 05:24:37', '516547', '15-VEH4IWANBDJ6TUPM28L7GQ0OYS1XKZ', NULL, NULL, NULL, '[\"2020-09-30T11:21:49.178428Z\"]', '[\"2020-09-30T11:22:12.223304Z\"]'),
(16, '2', 'january', '2020', 'wqeqw', 'corporation', 'dasd@asda.com', 'adasd', 'ad', 'asdasd', 'ada@asda.com', '(312) 312-3123', NULL, 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'wqeqw', 'dasd@asda.com', '(312) 312-3123', 'ada@asda.com', 'ad', '50.00', '500.00', '180.00', '200.00', '45.00', '90.00', '350.00', '25.00', '100.00', '40.00', '175.00', '20.00', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '09/30/2020', 'EVOS', 'dasd', 'adasd', 'asdas', '09/07/2020', 'aaaa', NULL, 2, 'looklike.himu@gmail.com', '09/30/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-09-30 05:33:59', '2020-09-30 05:52:04', '373530', '16-YP5H687WAQF9O1JX2SB4VNMZUGKT0L', NULL, NULL, NULL, '[\"2020-09-30T11:33:59.465796Z\"]', '[\"2020-09-30T11:34:16.989024Z\",\"2020-09-30T11:35:51.218796Z\",\"2020-09-30T11:44:39.608057Z\",\"2020-09-30T11:46:03.812174Z\",\"2020-09-30T11:48:14.293675Z\",\"2020-09-30T11:50:12.502303Z\"]');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('test1@gmail.com', '$2y$10$g1.88ts1iFm5dZ6s.hdBkuUGjxWHJDThE.cr8.18wKJaHZWusEK2.', '2020-03-24 14:37:51'),
('looklike.himu@gmail.com', '$2y$10$UTrvbw9QqCU/tO5yq2eG9.Cq.Xrd7HbD4thbnXnSDc4EW/Aek/wvK', '2020-06-08 06:54:37'),
('baulsuphal@gmail.com', '$2y$10$TjCyFq5YA3uRkWBz2mqIvuLINKN1FDzMBFhxnVVRA1DEAreyuzF4e', '2020-06-25 10:30:22');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL,
  `subcat_id` int(11) NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `web_short_des` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_long_des` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `cat_id`, `subcat_id`, `product_name`, `sku`, `web_short_des`, `web_long_des`, `image`, `created_at`, `updated_at`) VALUES
(4, 49, 50, 'edad', 'dsadasd', 'dssad', 'sdaasd', 'http://localhost:8000/storage/images/1586776368_productImg.png', '2020-04-13 05:12:48', '2020-04-29 23:41:26'),
(5, 49, 50, '3rwerwe', 'werwerw', 'dsdf', 'fdsfdsf', 'http://localhost:8000/storage/images/1586927387_productImg.png', '2020-04-14 23:09:47', '2020-05-10 02:27:41'),
(6, 49, 50, '3rwerwe', 'werwerw', 'dsdf', 'fdsfdsf', 'http://localhost:8000/storage/images/1586927414_productImg.png', '2020-04-14 23:10:14', '2020-05-31 03:33:38'),
(7, 49, 50, '3rwerwe', 'werwerw', 'xzczxc', NULL, 'http://localhost:8000/storage/images/1586927653_productImg.png', '2020-04-14 23:14:13', '2020-05-19 01:45:52'),
(8, 52, 53, 'cvtytyty', 'yuyt', NULL, NULL, '', '2020-04-18 23:49:27', '2020-04-18 23:49:27'),
(9, 52, 53, 'cvtytyty', 'yuyt', NULL, NULL, '', '2020-04-18 23:49:41', '2020-04-19 00:22:30'),
(10, 52, 53, '3rwerwe', 'werwerw', 'nnn', 'uiuiui', '', '2020-04-19 02:38:09', '2020-04-19 02:38:48'),
(11, 49, 50, '3rwerwe', 'werwerw', NULL, NULL, '', '2020-04-19 06:30:04', '2020-04-19 06:30:39'),
(12, 49, 50, 'p31', 'p31', NULL, NULL, '', '2020-04-20 06:22:26', '2020-04-20 06:22:26'),
(13, 52, 53, 'cvcvcvcs312312', '1231', NULL, NULL, '', '2020-04-26 06:57:09', '2020-04-26 06:59:22'),
(14, 52, 53, '3rwerweadasdas', 'werwerw', NULL, NULL, '', '2020-04-28 05:29:39', '2020-04-28 05:29:39'),
(15, 5, 50, 'text productnnnnnmmmm', 'nmmnm', NULL, NULL, '', '2020-05-27 07:56:35', '2020-05-27 07:56:35'),
(16, 49, 50, 'text productnnnnnmmmm', 'nmmnm', NULL, NULL, '', '2020-05-27 07:56:51', '2020-05-27 07:59:10'),
(17, 49, 50, 'text productwewqe', '13123', NULL, NULL, '', '2020-05-31 03:07:59', '2020-05-31 03:07:59'),
(18, 49, 50, 'test p1', 'tp1', NULL, NULL, '', '2020-05-31 06:41:31', '2020-06-05 07:55:03'),
(19, 52, 53, 'new test product', 'nps', NULL, NULL, '', '2020-06-01 02:48:07', '2020-06-01 02:48:07');

-- --------------------------------------------------------

--
-- Table structure for table `product_inventories`
--

CREATE TABLE `product_inventories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_settings`
--

CREATE TABLE `quickbooks_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_secret` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oauth_redirect_uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `environment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quickbooks_settings`
--

INSERT INTO `quickbooks_settings` (`id`, `client_id`, `client_secret`, `oauth_redirect_uri`, `environment`, `created_at`, `updated_at`) VALUES
(1, 'AB9mvbzrqCYHKMJ1zGUDtBCtWpgxhS3Ogyl8YMcfIn5c7P8SyM', 'DbFGfbWUQr6YBViyUkCfqZQIc6Hdjs62nlSlZNp3', 'https://evostage.wikimesh.com/qbCallback', 'Production', '2020-03-08 05:09:12', '2020-03-29 01:39:24');

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE `quotation` (
  `id` int(11) NOT NULL,
  `quote_ticket` varchar(191) DEFAULT NULL,
  `date` varchar(191) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `total_gross_cost` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quotation`
--

INSERT INTO `quotation` (`id`, `quote_ticket`, `date`, `customer_id`, `total_gross_cost`, `created_at`, `updated_at`) VALUES
(1, '1958-T5UF', '06/16/2020', 5, '150', '2020-06-16 13:12:41', '2020-06-16 13:12:41');

-- --------------------------------------------------------

--
-- Table structure for table `quote_item`
--

CREATE TABLE `quote_item` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_type` varchar(191) DEFAULT NULL,
  `service_location_address_id` int(11) DEFAULT NULL,
  `service_location_address_type` varchar(191) DEFAULT NULL,
  `item_total` int(11) DEFAULT NULL,
  `var_total` varchar(191) DEFAULT NULL,
  `gross_total` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quote_item`
--

INSERT INTO `quote_item` (`id`, `quote_id`, `item_id`, `item_type`, `service_location_address_id`, `service_location_address_type`, `item_total`, `var_total`, `gross_total`, `created_at`, `updated_at`) VALUES
(1, 1, 10, 'rental', NULL, NULL, NULL, '150', '150', '2020-06-16 13:12:41', '2020-06-16 13:12:41');

-- --------------------------------------------------------

--
-- Table structure for table `quote_item_details`
--

CREATE TABLE `quote_item_details` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) DEFAULT NULL,
  `quote_item_id` int(11) DEFAULT NULL,
  `resource_id` int(11) DEFAULT NULL,
  `resource_price` varchar(191) DEFAULT NULL,
  `resource_unit` int(11) DEFAULT NULL,
  `resource_duration` int(11) DEFAULT NULL,
  `item_detail_total` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quote_item_details`
--

INSERT INTO `quote_item_details` (`id`, `quote_id`, `quote_item_id`, `resource_id`, `resource_price`, `resource_unit`, `resource_duration`, `item_detail_total`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 10, NULL, NULL, NULL, NULL, '2020-06-16 13:12:41', '2020-06-16 13:12:41');

-- --------------------------------------------------------

--
-- Table structure for table `quote_variable`
--

CREATE TABLE `quote_variable` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) DEFAULT NULL,
  `quote_item_type` varchar(191) DEFAULT NULL,
  `quote_item_id` int(11) DEFAULT NULL,
  `var_name` varchar(191) DEFAULT NULL,
  `var_type` varchar(191) DEFAULT NULL,
  `var_type_value` varchar(191) DEFAULT NULL,
  `item_var_total` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quote_variable`
--

INSERT INTO `quote_variable` (`id`, `quote_id`, `quote_item_type`, `quote_item_id`, `var_name`, `var_type`, `var_type_value`, `item_var_total`, `created_at`, `updated_at`) VALUES
(1, 1, 'inventory', 1, 'Fresh Water', NULL, NULL, NULL, '2020-06-16 13:12:41', '2020-06-16 13:12:41'),
(2, 1, 'inventory', 1, 'Tank Wash', NULL, NULL, NULL, '2020-06-16 13:12:41', '2020-06-16 13:12:41'),
(3, 1, 'inventory', 1, 'Disposal Fee - Liquid', NULL, NULL, NULL, '2020-06-16 13:12:41', '2020-06-16 13:12:41'),
(4, 1, 'inventory', 1, 'Service Operator', NULL, NULL, NULL, '2020-06-16 13:12:41', '2020-06-16 13:12:41'),
(5, 1, 'inventory', 1, 'Schedule', NULL, NULL, NULL, '2020-06-16 13:12:41', '2020-06-16 13:12:41');

-- --------------------------------------------------------

--
-- Table structure for table `quote_var_parameters`
--

CREATE TABLE `quote_var_parameters` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) DEFAULT NULL,
  `quote_item_var_id` int(11) DEFAULT NULL,
  `var_param_name` varchar(191) DEFAULT NULL,
  `var_param_data_type` varchar(191) DEFAULT NULL,
  `var_param_value_type` varchar(191) DEFAULT NULL,
  `var_param_value` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quote_var_parameters`
--

INSERT INTO `quote_var_parameters` (`id`, `quote_id`, `quote_item_var_id`, `var_param_name`, `var_param_data_type`, `var_param_value_type`, `var_param_value`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Cost - Per Barrel', NULL, '0', '1.50', '2020-06-16 13:12:41', '2020-06-16 13:12:41'),
(2, 1, 1, 'Amount / Qty ( Barrel )', NULL, '1', '100', '2020-06-16 13:12:41', '2020-06-16 13:12:41'),
(3, 1, 2, 'Cost - Per Tank', NULL, '0', '292.50', '2020-06-16 13:12:41', '2020-06-16 13:12:41'),
(4, 1, 2, 'Amount / Qty ( Tank )', NULL, '1', NULL, '2020-06-16 13:12:41', '2020-06-16 13:12:41'),
(5, 1, 3, 'Cost - Per Barrel', NULL, '0', '1.15', '2020-06-16 13:12:41', '2020-06-16 13:12:41'),
(6, 1, 3, 'Amount / Qty ( Barrel )', NULL, '1', NULL, '2020-06-16 13:12:41', '2020-06-16 13:12:41'),
(7, 1, 4, 'Cost Per hour', NULL, '0', '45', '2020-06-16 13:12:41', '2020-06-16 13:12:41'),
(8, 1, 4, 'Total Hours', NULL, '1', NULL, '2020-06-16 13:12:41', '2020-06-16 13:12:41'),
(9, 1, 5, 'Service Date', NULL, '1', NULL, '2020-06-16 13:12:41', '2020-06-16 13:12:41');

-- --------------------------------------------------------

--
-- Table structure for table `rental_agreement`
--

CREATE TABLE `rental_agreement` (
  `id` int(11) NOT NULL,
  `entered_on` varchar(191) DEFAULT NULL,
  `entered_by` varchar(191) DEFAULT NULL,
  `entered_between_lessor` varchar(191) DEFAULT NULL,
  `entered_between_lessee` varchar(191) DEFAULT NULL,
  `ren_ag_lesse_equipment` text DEFAULT NULL,
  `ren_ag_start_date` varchar(191) DEFAULT NULL,
  `ren_ag_end_date` varchar(191) DEFAULT NULL,
  `ren_ag_amount_rent` varchar(191) DEFAULT NULL,
  `ren_ag_time_rent` varchar(191) DEFAULT NULL,
  `ren_ag_address_rent` varchar(191) DEFAULT NULL,
  `ren_ag_late_day` varchar(191) DEFAULT NULL,
  `ren_ag_late_charge` varchar(191) DEFAULT NULL,
  `ren_ag_security_deposit` varchar(191) DEFAULT NULL,
  `responsible` varchar(191) DEFAULT NULL,
  `ren_ag_governing_law` varchar(191) DEFAULT NULL,
  `lessor_agreement_notice` text DEFAULT NULL,
  `lesse_agreement_notice` text DEFAULT NULL,
  `ren_ag_additional_terms` text DEFAULT NULL,
  `ren_ag_executed_lessor_name` varchar(191) DEFAULT NULL,
  `ren_ag_executed_lessor_position` varchar(191) DEFAULT NULL,
  `ren_ag_executed_lessee_name` varchar(191) DEFAULT NULL,
  `ren_ag_executed_lessee_position` varchar(191) DEFAULT NULL,
  `ren_ag_send_mail_to` varchar(191) DEFAULT NULL,
  `ren_ag_send_mail_date` varchar(191) DEFAULT NULL,
  `submitted_by_name` varchar(191) DEFAULT NULL,
  `submitted_by_email` varchar(191) DEFAULT NULL,
  `signed_by` varchar(191) DEFAULT NULL,
  `submitted_to_insurance` varchar(191) DEFAULT NULL,
  `submitted_date_insurance` varchar(191) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `reject` text DEFAULT NULL,
  `access_code` varchar(191) DEFAULT NULL,
  `url_token` varchar(191) DEFAULT NULL,
  `sent` text DEFAULT NULL,
  `seen` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_agreement`
--

INSERT INTO `rental_agreement` (`id`, `entered_on`, `entered_by`, `entered_between_lessor`, `entered_between_lessee`, `ren_ag_lesse_equipment`, `ren_ag_start_date`, `ren_ag_end_date`, `ren_ag_amount_rent`, `ren_ag_time_rent`, `ren_ag_address_rent`, `ren_ag_late_day`, `ren_ag_late_charge`, `ren_ag_security_deposit`, `responsible`, `ren_ag_governing_law`, `lessor_agreement_notice`, `lesse_agreement_notice`, `ren_ag_additional_terms`, `ren_ag_executed_lessor_name`, `ren_ag_executed_lessor_position`, `ren_ag_executed_lessee_name`, `ren_ag_executed_lessee_position`, `ren_ag_send_mail_to`, `ren_ag_send_mail_date`, `submitted_by_name`, `submitted_by_email`, `signed_by`, `submitted_to_insurance`, `submitted_date_insurance`, `status`, `reject`, `access_code`, `url_token`, `sent`, `seen`, `created_at`, `updated_at`) VALUES
(1, '08/09/2020', NULL, NULL, 'qweq', 'fsf sfs fsdf sdf sdfsdfs', '08/03/2020', '08/07/2020', '313123', 'week', '903 West Industrial Ave. Midland, TX 79701', '6777', NULL, '3,397,979.00', 'not_responsible', 'khjkhkhj kmk;lk', '903 West Industrial Ave. Midland, TX 79701', 'dasd adasd dsas dsadas', 'jljlkj lkj lkjlkjl', 'adsds', 'dasdsa', 'dasdsad', 'asdd adasd', 'looklike.himu@gmail.com', '08/09/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, 3, NULL, '344681', '1-1ZASCH8PYNI2WJOFT5Q6R9KGX3B0VD', '[\"2020-08-09T09:07:27.422608Z\",\"2020-08-09T09:16:46.435086Z\",\"2020-08-09T09:21:33.349199Z\"]', '[\"2020-08-09T09:13:08.395951Z\",\"2020-08-09T09:17:57.287475Z\",\"2020-08-09T09:18:48.023659Z\",\"2020-08-09T09:19:35.476151Z\",\"2020-08-09T09:21:52.481606Z\",\"2020-08-09T09:23:44.240177Z\",\"2020-08-09T09:23:56.152901Z\",\"2020-08-09T09:58:20.784822Z\"]', '2020-08-09 03:07:27', '2020-08-09 03:58:20'),
(2, '08/11/2020', NULL, NULL, 'qweq', 'dada das dasd asd asd ad asd as dasd', '08/11/2020', '08/13/2020', '2,213,213,123.00', 'month', '903 West Industrial Ave. Midland, TX 79701', '112', '31,231,231.00', '13,132,131.00', 'not_responsible', 'wqwe wqewqe', '903 West Industrial Ave. Midland, TX 79701', 'qwewqeqw', '3123 123 123 123123', 'wdqwdqwe', 'eqweqwe', 'eqwe', 'eqwewqe', 'looklike.himu@gmail.com', '08/11/2020', 'test1', 'eg.himel@gmail.com', 'eqweq', 'himel.biswas@asciisys.com', '08/10/2020', 3, NULL, '655392', '2-28GD49SLNYEA6K75RWTHIOX1U0ZCFB', '[\"2020-08-11T10:05:09.851658Z\",\"2020-08-11T12:31:36.913929Z\",\"2020-08-11T13:06:46.995466Z\",\"2020-08-11T13:12:11.192437Z\"]', '[\"2020-08-11T10:05:37.983885Z\",\"2020-08-11T10:06:16.789851Z\",\"2020-08-11T10:08:07.076568Z\",\"2020-08-11T10:09:11.790676Z\",\"2020-08-11T10:09:55.144006Z\",\"2020-08-11T10:11:52.718723Z\",\"2020-08-11T12:31:59.838434Z\",\"2020-08-11T13:07:09.776305Z\",\"2020-08-11T13:12:41.116764Z\"]', '2020-08-11 04:05:09', '2020-08-11 07:12:41'),
(3, '08/12/2020', NULL, NULL, 'qweq', 'weqweqw', '08/11/2020', '08/12/2020', '2,213,213,123.00', 'month', '903 West Industrial Ave. Midland, TX 79701', '12', '13,132,131.00', '13,132,131.00', 'not_responsible', 'wqwe wqewqe', '903 West Industrial Ave. Midland, TX 79701', 'er erwe rwe r ewr ewrewr', 'wwqewqe', 'wdqwdqwe', 'eqweqwe', '23213', 'asdsadasd', 'looklike.himu@gmail.com', '08/11/2020', 'test1', 'eg.himel@gmail.com', 'asdsad dsada', NULL, NULL, 2, NULL, '852007', '3-G82ZKHENF1I0Q6ARDB5LSOP43MV7TJ', '[\"2020-08-11T12:00:20.536505Z\"]', '[\"2020-08-11T12:09:01.438626Z\"]', '2020-08-11 06:00:20', '2020-08-11 06:10:43'),
(4, '08/11/2020', NULL, NULL, 'qweq', 'sadsad sadsad asdas das', '08/03/2020', '08/10/2020', '2,213,213,123.00', 'week', '903 West Industrial Ave. Midland, TX 79701', '2', '312,312,312.00', '1,231,313.00', 'not_responsible', 'wqwe wqewqe', '903 West Industrial Ave. Midland, TX 79701', 'qwrq rqerew', 'rwer werw rwer', 'wdqwdqwe', 'eqweqwe', 'sdfsf', 'asdsadasd', 'looklike.himu@gmail.com', '08/11/2020', 'test1', 'eg.himel@gmail.com', 'cvvv', 'looklike.himu@gmail.com', '08/11/2020', 3, NULL, '715847', '4-5XCP7WL0I8KN2ZEOVFY3G4Q6BDS9JT', '[\"2020-08-11T14:49:37.022463Z\",\"2020-08-11T15:02:24.341050Z\",\"2020-08-11T15:04:58.628024Z\"]', '[\"2020-08-11T14:50:00.182304Z\",\"2020-08-11T14:52:33.535044Z\",\"2020-08-11T15:00:06.962724Z\",\"2020-08-11T15:02:42.485835Z\",\"2020-08-11T15:05:49.244355Z\"]', '2020-08-11 08:49:37', '2020-08-11 09:05:49'),
(5, '08/11/2020', NULL, NULL, 'qweq', 'safa asdasd', '08/11/2020', '08/05/2020', '1,313.00', 'week', '903 West Industrial Ave. Midland, TX 79701', '31231', '12,313.00', '12,312,313.00', 'not_responsible', NULL, '903 West Industrial Ave. Midland, TX 79701', NULL, 'rwerwr', 'wdqwdqwe', 'qweqw', NULL, NULL, 'looklike.himu@gmail.com', '08/11/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, 1, NULL, '992378', '5-R0LPQ6XGVFM849ZAE3DT2JUWICYOHB', '[\"2020-08-11T15:08:44.337630Z\"]', '[\"2020-08-11T15:10:04.467893Z\"]', '2020-08-11 09:08:44', '2020-08-11 09:10:04'),
(6, '08/13/2020', NULL, NULL, 'qweq', 'adas dsadsad', '08/12/2020', '08/14/2020', '2,213,213,123.00', 'week', '903 West Industrial Ave. Midland, TX 79701', '2', '13,132,131.00', '1,231,313.00', 'not_responsible', 'wqwe wqewqe', '903 West Industrial Ave. Midland, TX 79701', NULL, 'das das dasd', 'wdqwdqwe', 'eqweqwe', NULL, NULL, 'looklike.himu@gmail.com', '08/13/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, 2, NULL, '381265', '6-3QN0EIA48U7L2OMSZ5FVJXRD96GK1B', '[\"2020-08-13T11:38:17.992818Z\"]', NULL, '2020-08-13 05:38:17', '2020-08-13 05:38:17'),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', NULL, NULL, NULL, NULL, NULL, NULL, 'looklike.himu@gmail.com', '08/13/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, 2, NULL, '354684', '7-EZMLGPH3X921BYSTO0IFA4KDVCUN58', '[\"2020-08-13T11:40:18.335293Z\"]', NULL, '2020-08-13 05:40:18', '2020-08-13 05:40:18'),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', NULL, NULL, NULL, NULL, NULL, NULL, 'looklike.himu@gmail.com', '08/13/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, 1, NULL, '753877', '8-4VUBP2MX9AELSNOGZFY0CIQ51HJ63K', '[\"2020-08-13T11:43:53.298701Z\"]', NULL, '2020-08-13 05:43:53', '2020-08-13 05:43:53'),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', 'asd asdasd', NULL, NULL, NULL, 'dadadsa', 'dsadas', 'looklike.himu@gmail.com', '08/13/2020', 'test1', 'eg.himel@gmail.com', 'sdsad', NULL, NULL, 2, NULL, '727476', '9-CNATZ436P1H7EGX8D5QUSFBRIJL9V2', '[\"2020-08-13T11:44:35.820713Z\"]', '[\"2020-08-13T11:45:13.304045Z\"]', '2020-08-13 05:44:35', '2020-08-13 05:45:53'),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', NULL, NULL, NULL, NULL, NULL, NULL, 'looklike.himu@gmail.com', '08/17/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, 1, NULL, '503563', '10-G14CXSH90OLPZUQAJ5NKTYF3DI7E6B', '[\"2020-08-17T12:54:22.348803Z\"]', '[\"2020-08-17T12:54:55.055641Z\",\"2020-08-17T13:14:13.149456Z\",\"2020-08-17T13:15:23.212905Z\",\"2020-08-17T13:16:55.821213Z\",\"2020-08-17T13:19:10.035772Z\",\"2020-08-17T13:21:25.948034Z\",\"2020-08-17T13:21:44.514627Z\",\"2020-08-17T13:21:56.640272Z\",\"2020-08-17T14:18:57.242021Z\",\"2020-08-17T14:21:30.659122Z\",\"2020-08-17T14:22:24.598921Z\",\"2020-08-17T14:23:30.959282Z\",\"2020-08-17T14:23:41.086439Z\",\"2020-08-17T14:24:55.716968Z\",\"2020-08-17T14:25:05.868942Z\",\"2020-08-17T14:26:21.242715Z\",\"2020-08-17T14:27:01.235033Z\",\"2020-08-17T14:27:29.804496Z\"]', '2020-08-17 06:54:22', '2020-08-17 08:27:29'),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', 'dasdsad', NULL, NULL, NULL, 'awdea', 'dasd', 'looklike.himu@gmail.com', '08/18/2020', 'test1', 'eg.himel@gmail.com', 'dasdsad', 'looklike.himu@gmail.com', '08/18/2020', 3, NULL, '199977', '11-PW9TUEQB6XO51MAS4GCDNK0V2R38ZY', '[\"2020-08-18T13:47:04.461876Z\",\"2020-08-18T14:00:12.360344Z\",\"2020-08-18T14:02:32.931042Z\"]', '[\"2020-08-18T13:54:05.461035Z\",\"2020-08-18T13:55:06.892969Z\",\"2020-08-18T14:00:29.670017Z\",\"2020-08-18T14:01:13.825678Z\",\"2020-08-18T14:01:45.705116Z\",\"2020-08-18T14:02:49.081804Z\"]', '2020-08-18 07:47:04', '2020-08-18 08:02:49'),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', 'sdfds', NULL, NULL, NULL, 'qr', 'rwer', 'looklike.himu@gmail.com', '08/18/2020', 'test1', 'eg.himel@gmail.com', NULL, 'looklike.himu@gmail.com', '08/18/2020', 3, NULL, '527906', '12-N28TIR4WK0GJZO1CVFM9LUQ5X6SDEP', '[\"2020-08-18T16:03:05.248783Z\",\"2020-08-18T16:04:26.244844Z\",\"2020-08-18T16:05:29.354435Z\"]', '[\"2020-08-18T16:03:29.466204Z\",\"2020-08-18T16:04:41.507797Z\",\"2020-08-18T16:05:50.195764Z\"]', '2020-08-18 10:03:05', '2020-08-18 10:05:50'),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', 'HHHHH', NULL, NULL, NULL, 'JJ', 'JJJ', 'looklike.himu@gmail.com', '08/19/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, 2, NULL, '390356', '13-48X7LJGES5KY3VIMWFBCT9ZHANU2QO', '[\"2020-08-19T14:02:22.916591Z\"]', '[\"2020-08-19T14:02:42.465263Z\"]', '2020-08-19 08:02:22', '2020-08-19 08:03:13'),
(14, '09/14/2020', NULL, NULL, 'qweq', 'sdf dfdsfsdf', '09/15/2020', '09/24/2020', '31231', 'month', '903 West Industrial Ave. Midland, TX 79701', '3', '13,132,131.00', '1,231,313.00', 'not_responsible', 'wqwe wqewqe', '903 West Industrial Ave. Midland, TX 79701', 'ads asdasdsad', 'fsdfsdfs sfd fsd', NULL, NULL, 'asdas', 'dasdsa', 'looklike.himu@gmail.com', '09/03/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, 2, NULL, '157008', '14-91M3CH4JTAKVQXFRWG87I25BYOPZL0', '[\"2020-09-03T16:02:48.375451Z\",\"2020-09-03T16:11:13.375892Z\"]', '[\"2020-09-03T16:08:23.483895Z\",\"2020-09-03T16:11:31.250334Z\"]', '2020-09-03 10:02:48', '2020-09-03 10:11:45'),
(15, '09/07/2020', NULL, NULL, 'qweq', 'dad asd sad', '09/03/2020', '09/10/2020', '3123', 'day', '903 West Industrial Ave. Midland, TX 79701', '12', '1222', '1,231,313.00', 'responsible', NULL, '903 West Industrial Ave. Midland, TX 79701', 'fsadf f', 'sdf sdfsd', 'fsdfsd', 'sdf sdf', 'adasd', 'sad sa', 'looklike.himu@gmail.com', '09/03/2020', 'test1', 'eg.himel@gmail.com', NULL, 'himel.biswas@asciisys.com', '09/03/2020', 3, NULL, '296672', '15-I1FP7YERKVS269UX43OLMZNHABJ08T', '[\"2020-09-03T16:15:19.369752Z\",\"2020-09-03T16:17:42.137078Z\",\"2020-09-03T16:19:08.750570Z\"]', '[\"2020-09-03T16:15:38.351262Z\",\"2020-09-03T16:18:04.244029Z\",\"2020-09-03T16:19:46.476082Z\",\"2020-09-03T16:21:16.067751Z\"]', '2020-09-03 10:15:19', '2020-09-03 10:21:16'),
(16, '09/13/2020', NULL, NULL, 'qweq', 'w erewr w', '09/06/2020', '09/09/2020', '1,313.00', 'month', '903 West Industrial Ave. Midland, TX 79701', '3', '12323', '13,123.00', 'responsible', 'q eqweqwe', '903 West Industrial Ave. Midland, TX 79701', 'sada dsa', 'erw  ewrew rw', 'eqqwe', 'ewqeqwe', 'qwewq qw', 'eqwee wq', 'looklike.himu@gmail.com', '09/13/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, 4, '<p>1. adasd sadasd sad&nbsp;</p>', '561648', '16-5GAFXDTRKU2B07PZWSEY6JIC9H4O18', '[\"2020-09-13T04:09:48.697092Z\",\"2020-09-13T04:17:54.405246Z\"]', '[\"2020-09-13T04:12:31.960868Z\"]', '2020-09-12 22:09:48', '2020-09-12 22:17:54'),
(17, '09/06/2020', NULL, NULL, 'qweq', 'e eqq q', '09/07/2020', '09/07/2020', '1,313.00', 'day', '903 West Industrial Ave. Midland, TX 79701', '3', '1222', '13,123.00', 'responsible', 'wqwe wqewqe', '903 West Industrial Ave. Midland, TX 79701', 'dad ada', 'hhk h hkj', 'wdqwdqwe', 'eqweqwe', 'ds  ad d', 's aa a sa', 'looklike.himu@gmail.com', '09/30/2020', 'test1', 'eg.himel@gmail.com', NULL, 'elizabeth@mcanallywilkins.com', '09/30/2020', 2, NULL, '957402', '17-48MGWOC3H2ZIQE9P0JUDRBT6FVSNX5', '[\"2020-09-30T05:49:00.648342Z\",\"2020-09-30T06:40:08.410865Z\"]', '[\"2020-09-30T05:49:48.339045Z\",\"2020-09-30T06:25:55.648089Z\",\"2020-09-30T06:26:33.601082Z\",\"2020-09-30T06:29:03.695573Z\",\"2020-09-30T06:41:15.108772Z\"]', '2020-09-29 23:49:00', '2020-09-30 00:41:54'),
(18, '09/14/2020', NULL, NULL, 'qweq', 'da cd sdds', '09/15/2020', '09/14/2020', '3112', 'month', '903 West Industrial Ave. Midland, TX 79701', NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', 'dad ada', NULL, NULL, NULL, 'ds  ad d', 's aa a sa', 'looklike.himu@gmail.com', NULL, NULL, NULL, NULL, 'elizabeth@mcanallywilkins.com', '09/30/2020', 0, NULL, NULL, NULL, NULL, NULL, '2020-09-30 00:32:21', '2020-09-30 00:32:21'),
(19, '09/06/2020', NULL, NULL, 'qweq', 'q qwwq qw', '09/07/2020', '09/12/2020', '2,213', 'month', '903 West Industrial Ave. Midland, TX 79701', NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', 'dad ada', NULL, NULL, NULL, 'ds  ad d', 's aa a sa', 'looklike.himu@gmail.com', NULL, NULL, NULL, NULL, 'elizabeth@mcanallywilkins.com', '09/30/2020', 0, NULL, NULL, NULL, NULL, NULL, '2020-09-30 00:33:10', '2020-09-30 00:33:10'),
(20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', NULL, NULL, NULL, NULL, NULL, '903 West Industrial Ave. Midland, TX 79701', 'ads ad', NULL, NULL, NULL, 'sdfsf', 'asdsadasd', 'looklike.himu@gmail.com', '09/30/2020', 'test1', 'eg.himel@gmail.com', NULL, 'elizabeth@mcanallywilkins.com', '09/30/2020', 4, '<p>hjghg</p>', '711937', '20-1CWPHYB02XINU6RG758K4FVTEJSAZD', '[\"2020-09-30T12:01:30.575787Z\",\"2020-10-30T13:15:35.807162Z\"]', '[\"2020-09-30T12:01:47.259586Z\",\"2020-09-30T13:12:31.703199Z\"]', '2020-09-30 06:01:30', '2020-09-30 07:15:35'),
(21, '09/21/2020', NULL, NULL, 'qweq', 'hhjgjh j', '09/14/2020', '09/14/2020', '2,213,213,123.00', 'day', '903 West Industrial Ave. Midland, TX 79701', '3', '1222', '13,123.00', 'not_responsible', 'asdsa dsadasd', '903 West Industrial Ave. Midland, TX 79701', 'sffss', NULL, 'wdqwdqwe', 'eqweqwe', 'sdfsf', 'asdsadasd', 'looklike.himu@gmail.com', '09/30/2020', 'test1', 'eg.himel@gmail.com', NULL, 'eg.himel@gmail.com', '09/30/2020', 3, NULL, '413474', '21-8SXQJA4IPRCO2W0DU3GBNMYE96K5VL', '[\"2020-09-30T13:14:50.330803Z\",\"2020-09-30T13:29:53.085767Z\"]', '[\"2020-09-30T13:26:29.894014Z\"]', '2020-09-30 07:14:50', '2020-09-30 07:29:53');

-- --------------------------------------------------------

--
-- Table structure for table `resource_parameters`
--

CREATE TABLE `resource_parameters` (
  `id` int(11) NOT NULL,
  `subcat_id` int(11) DEFAULT NULL,
  `unit_param_name` varchar(191) DEFAULT NULL,
  `unit_param_data_type` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resource_parameters`
--

INSERT INTO `resource_parameters` (`id`, `subcat_id`, `unit_param_name`, `unit_param_data_type`, `created_at`, `updated_at`) VALUES
(1, 1, 'model name', 'string', '2020-08-13 07:25:27', '2020-08-13 07:25:27'),
(2, 1, 'liability', 'checkbox', '2020-08-13 07:25:27', '2020-08-13 07:25:27'),
(11, 2, 'param4', 'decimal', '2020-09-03 06:18:25', '2020-09-03 06:18:25'),
(10, 2, 'param3', 'checkbox', '2020-09-03 06:18:25', '2020-09-03 06:18:25'),
(9, 2, 'param2', 'checkbox', '2020-09-03 06:18:25', '2020-09-03 06:18:25'),
(8, 2, 'param1', 'string', '2020-09-03 06:18:25', '2020-09-03 06:18:25'),
(12, 2, 'param6', 'integer', '2020-09-03 06:18:25', '2020-09-03 06:18:25'),
(13, 2, 'param7', 'string', '2020-09-03 06:18:25', '2020-09-03 06:18:25'),
(14, 2, 'param8', 'checkbox', '2020-09-03 06:18:25', '2020-09-03 06:18:25'),
(15, 2, 'param9', 'datetime', '2020-09-03 06:18:25', '2020-09-03 06:18:25'),
(16, 2, 'param10', 'checkbox', '2020-09-03 06:18:25', '2020-09-03 06:18:25');

-- --------------------------------------------------------

--
-- Table structure for table `service_location_address`
--

CREATE TABLE `service_location_address` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `shipAddr_line1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_line2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_countrySubDivisionCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipAddr_postalCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lease_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rig_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `afe_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_contact_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rig_contact_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_man_contact_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_location_address`
--

INSERT INTO `service_location_address` (`id`, `type`, `parent_id`, `shipAddr_line1`, `shipAddr_line2`, `shipAddr_city`, `shipAddr_country`, `shipAddr_countrySubDivisionCode`, `shipAddr_postalCode`, `lease_name`, `rig_number`, `afe_number`, `site_contact_phone`, `rig_contact_email`, `company_man_contact_name`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'Corral Canyon 8-32 Fed 103H', 'Akita 803', 'DD.2017.04601', NULL, 'akita803@xtoenergy.com', NULL, NULL, NULL, '2020-07-08 11:16:28', '2020-07-08 11:42:05'),
(2, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'Blalock 3181NH', NULL, 'DD.2019.06841', '432-236-3210', 'justin_kinslow@xtoenergy.com', 'Justin Kinslow', NULL, NULL, '2020-07-08 11:16:28', '2020-07-08 11:42:05'),
(4, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'Corral Canyon 8-32 Fed 162H', 'Precision 568', 'DD.2019.03188', NULL, 'Precision568@xtoenergy.com', 'Mike Turner', NULL, NULL, '2020-07-08 11:20:45', '2020-07-08 11:42:05'),
(5, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'Big Eddy Unit 29E Vador 107H', 'Precision 595', 'DD.2019.03076', NULL, 'precision595@xtoenergy.com', 'John Elrades', NULL, NULL, '2020-07-08 11:23:00', '2020-07-08 11:42:05'),
(6, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'Muy Wayno Recycle Facility', NULL, 'DD.2017.01917', NULL, NULL, 'Larry Powers', NULL, NULL, '2020-07-08 11:24:56', '2020-07-08 11:42:05'),
(7, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'Icarus C2-5-8 WB3 #803H', 'HP 518', 'DD.2019.08637', NULL, NULL, 'Josh Davis', NULL, NULL, '2020-07-08 11:27:05', '2020-07-08 11:42:05'),
(8, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'Charles Midkiff 4712 Pad', NULL, 'DD.2015.00278', NULL, NULL, 'Scotty Alms', NULL, NULL, '2020-07-08 11:28:21', '2020-07-08 11:42:05'),
(9, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'Gunpowder 21-16 WB1 #801H', NULL, '1880051001 (Cost Center)', NULL, NULL, NULL, NULL, NULL, '2020-07-08 11:30:17', '2020-07-08 11:42:05'),
(10, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'PLU 25 Brushy Draw 104H', NULL, 'DD.2017.03807', NULL, 'zachary_corbin@xtoenergy.com', 'Zach Corbin', NULL, NULL, '2020-07-08 11:38:07', '2020-07-08 11:42:05'),
(11, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'PLU 25 Brushy Draw 124H', NULL, 'DD.2017.03801', NULL, 'zachary_corbin@xtoenergy.com', 'Zach Corbin', NULL, NULL, '2020-07-08 11:38:07', '2020-07-08 11:42:05'),
(12, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'PLU 25 Brushy Draw 703H', NULL, 'DD.2017.03791', NULL, 'zachary_corbin@xtoenergy.com', 'Zach Corbin', NULL, NULL, '2020-07-08 11:38:07', '2020-07-08 11:42:05'),
(13, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'PLU 25 Brushy Draw 152H', NULL, 'DD.2017.03806', NULL, 'zachary_corbin@xtoenergy.com', 'Zach Corbin', NULL, NULL, '2020-07-08 11:38:07', '2020-07-08 11:42:05'),
(14, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'PLU 25 Brushy Draw 162H', NULL, 'DD.2017.03799', NULL, 'zachary_corbin@xtoenergy.com', 'Zach Corbin', NULL, NULL, '2020-07-08 11:38:07', '2020-07-08 11:42:05'),
(15, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'PLU 25 Brushy Draw 202H', NULL, 'DD.2019.02516', NULL, 'zachary_corbin@xtoenergy.com', 'Zach Corbin', NULL, NULL, '2020-07-08 11:38:07', '2020-07-08 11:42:05'),
(16, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'PLU 25 Brushy Draw 121H', NULL, 'DD.2017.03798', NULL, 'zachary_corbin@xtoenergy.com', 'Zach Corbin', NULL, NULL, '2020-07-08 11:38:07', '2020-07-08 11:42:05'),
(17, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'PLU 25 Brushy Draw 901H', NULL, 'DD.2017.03794', NULL, 'zachary_corbin@xtoenergy.com', 'Zach Corbin', NULL, NULL, '2020-07-08 11:38:07', '2020-07-08 11:42:05'),
(18, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'PLU 25 Brushy Draw 153H', NULL, 'DD.2017.03800', NULL, 'zachary_corbin@xtoenergy.com', 'Zach Corbin', NULL, NULL, '2020-07-08 11:38:07', '2020-07-08 11:42:05'),
(19, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'PLU 25 Brushy Draw 903H', NULL, 'DD.2017.03795', NULL, 'zachary_corbin@xtoenergy.com', 'Zach Corbin', NULL, NULL, '2020-07-08 11:38:07', '2020-07-08 11:42:05'),
(20, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'PLU 25 Brushy Draw 103H', NULL, 'DD.2019.02518', NULL, 'zachary_corbin@xtoenergy.com', 'Zach Corbin', NULL, NULL, '2020-07-08 11:38:07', '2020-07-08 11:42:05'),
(21, 'customer', 357, NULL, NULL, NULL, NULL, NULL, NULL, 'PLU 25 Brushy Draw 701H', NULL, 'DD.2017.03790', NULL, 'zachary_corbin@xtoenergy.com', 'Zach Corbin', NULL, NULL, '2020-07-08 11:38:07', '2020-07-08 11:42:05'),
(22, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Garrett 17-8 Pad 1 (182, 284) DRL', NULL, 'OPD20037', NULL, 'fruiz@eeronline.com', 'Fidel Ruiz', NULL, NULL, '2020-07-08 11:46:09', '2020-07-13 21:35:16'),
(23, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Welch 44 #1', NULL, NULL, NULL, NULL, 'Dale Sullins', NULL, NULL, '2020-07-08 11:46:09', '2020-07-13 21:35:16'),
(24, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'University #7-GA #5', NULL, NULL, NULL, NULL, 'Dale Sullins', NULL, NULL, '2020-07-08 11:46:09', '2020-07-13 21:35:16'),
(25, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'McAlpine Grisham 4738-2', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 11:46:09', '2020-07-13 21:35:16'),
(26, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Parker 47 #3', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 11:46:09', '2020-07-13 21:35:16'),
(27, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Koonce North A 39-34 #112', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 11:46:09', '2020-07-13 21:35:16'),
(28, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Nutt 35 #3', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 11:46:09', '2020-07-13 21:35:16'),
(29, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Fred Hall Unit #2', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 11:46:09', '2020-07-13 21:35:16'),
(30, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'TXL 1 #2', NULL, NULL, NULL, NULL, 'Gregorio Estrada', NULL, NULL, '2020-07-08 11:47:39', '2020-07-13 21:35:16'),
(31, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'TXL 41 #1', NULL, NULL, NULL, NULL, 'Gregorio Estrada', NULL, NULL, '2020-07-08 11:47:39', '2020-07-13 21:35:16'),
(32, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Davidson 2403', NULL, NULL, NULL, NULL, 'Gregorio Estrada', NULL, NULL, '2020-07-08 11:47:39', '2020-07-13 21:35:16'),
(33, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Ricker B #1', NULL, NULL, NULL, NULL, 'Gregorio Estrada', NULL, NULL, '2020-07-08 11:47:39', '2020-07-13 21:35:16'),
(34, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Bradford 44 #2', NULL, NULL, NULL, NULL, 'Gregorio Estrada', NULL, NULL, '2020-07-08 11:47:39', '2020-07-13 21:35:16'),
(35, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Windham D #1', NULL, NULL, NULL, NULL, 'Gregorio Estrada', NULL, NULL, '2020-07-08 11:47:39', '2020-07-13 21:35:16'),
(36, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Scharbauer 103 #1', NULL, NULL, NULL, NULL, 'Oscar Rodriguez', NULL, NULL, '2020-07-08 11:48:14', '2020-07-13 21:35:16'),
(37, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Parks 36-5', NULL, NULL, NULL, NULL, 'Oscar Rodriguez', NULL, NULL, '2020-07-08 11:48:14', '2020-07-13 21:35:16'),
(38, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Davis Walter R-44 #4', NULL, 'OWM20451', NULL, NULL, 'Bill Bennis', NULL, NULL, '2020-07-08 11:49:14', '2020-07-13 21:35:16'),
(39, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Woody 6-35 Well #113', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 11:49:14', '2020-07-13 21:35:16'),
(40, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Gault 5-44 #262', NULL, '19019', NULL, 'tricks@eeronline.com', 'Todd Ricks', NULL, NULL, '2020-07-08 11:50:04', '2020-07-13 21:35:16'),
(41, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Anguish #1', NULL, NULL, NULL, NULL, 'Dale Sullins', NULL, NULL, '2020-07-08 11:51:19', '2020-07-13 21:35:16'),
(42, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Northwest Midland SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 11:51:54', '2020-07-09 12:31:20'),
(43, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Garrett 17-8 FAC', NULL, 'JOB #34.2109.20', NULL, 'fruiz@eeronline.com', 'Fidel Ruiz', NULL, NULL, '2020-07-08 11:54:04', '2020-07-13 21:35:16'),
(44, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Davidson 12 #6', NULL, NULL, NULL, 'brodriguez@eeronline.com', 'Brenda Rodriguez', NULL, NULL, '2020-07-08 11:54:04', '2020-07-13 21:35:16'),
(45, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Dickenson SWD #1', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 11:54:39', '2020-07-09 12:31:20'),
(46, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Barrow 26 SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 11:55:04', '2020-07-09 12:31:20'),
(47, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Douthit M&N Water Line FY20-00381', NULL, 'JOB #34.7066.20', NULL, 'brodriguez@eeronline.com', 'Brenda Rodriguez', NULL, NULL, '2020-07-08 11:57:21', '2020-07-13 21:35:16'),
(48, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Patterson #5', NULL, NULL, NULL, NULL, 'Dale Sullins', NULL, NULL, '2020-07-08 11:57:21', '2020-07-13 21:35:16'),
(49, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Hazelwood 41-32 #H281, H184, A282) DRL', NULL, 'JOB #34.2519.20', NULL, 'tgriffin@eeronline.com', 'Travis Griffin', NULL, NULL, '2020-07-08 11:57:21', '2020-07-13 21:35:16'),
(50, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Sadie Waddell 4 #4', NULL, NULL, NULL, 'brodriguez@eeronline.com', 'Brenda Rodriguez', NULL, NULL, '2020-07-08 12:00:05', '2020-07-13 21:35:16'),
(51, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Rhea 1-6 Pad 1 #211', NULL, '20010', NULL, 'tricks@eeronline.com', 'Todd Ricks', NULL, NULL, '2020-07-08 12:00:05', '2020-07-13 21:35:16'),
(52, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Rhea 1-6 #221', NULL, '20012', NULL, 'tricks@eeronline.com', 'Todd Ricks', NULL, NULL, '2020-07-08 12:00:05', '2020-07-13 21:35:16'),
(53, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Rhea 1-6 #112', NULL, '20009', NULL, 'tricks@eeronline.com', 'Todd Ricks', NULL, NULL, '2020-07-08 12:00:05', '2020-07-13 21:35:16'),
(54, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Ellen McDowell SPCC', NULL, NULL, NULL, 'jwillingham@eeronline.com', 'Jamie Willingham', NULL, NULL, '2020-07-08 12:00:05', '2020-07-13 21:35:16'),
(55, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Nance 30 #4', NULL, NULL, NULL, 'brodriguez@eeronline.com', 'Brenda Rdoriguez', NULL, NULL, '2020-07-08 12:02:57', '2020-07-13 21:35:16'),
(56, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Holcomb 39 A #3', NULL, NULL, NULL, 'brodriguez@eeronline.com', 'Brenda Rodriguez', NULL, NULL, '2020-07-08 12:02:57', '2020-07-13 21:35:16'),
(57, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'University 48-5 #3X', NULL, NULL, NULL, NULL, 'Dale Sullins', NULL, NULL, '2020-07-08 12:02:57', '2020-07-13 21:35:16'),
(58, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Greer Thomas 3-44 WSL Pad 1 (251, 271) DRL', NULL, 'JOB #34.2531.20', NULL, 'jthorn@eeronline.com', 'John Thorn', NULL, NULL, '2020-07-08 12:02:57', '2020-07-13 21:35:16'),
(59, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Dickenson 18-7 Pad 4 #131', NULL, 'ODR19041', NULL, 'tricks@eeronline.com', 'Todd Ricks', NULL, NULL, '2020-07-08 12:05:09', '2020-07-13 21:35:16'),
(60, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Bryant Ranch 23', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 12:05:35', '2020-07-09 12:31:20'),
(61, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Bryant Ranch SWD 44 #1', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 12:08:39', '2020-07-09 12:31:20'),
(62, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Greenwood SWD 1D', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 12:08:39', '2020-07-09 12:31:20'),
(63, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Allar SWD 1D', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 12:08:39', '2020-07-09 12:31:20'),
(64, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Tex Harvey SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 12:08:39', '2020-07-09 12:31:20'),
(65, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'JL McMaster FY20-00317', NULL, 'JOB #34.7054', NULL, 'brodriguez@eeronline.com', 'Brenda Rodriguez', NULL, NULL, '2020-07-08 12:13:32', '2020-07-13 21:35:16'),
(66, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Carbon 12-13 WSL Pad 2 (281) DRL', NULL, 'ODR20097', NULL, 'jthorn@eeronline.com', 'John Thorn', NULL, NULL, '2020-07-08 12:13:32', '2020-07-13 21:35:16'),
(67, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Harvard 46 #2', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 12:13:32', '2020-07-13 21:35:16'),
(68, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'University H 18G #7', NULL, NULL, NULL, NULL, 'Dale Sullins', NULL, NULL, '2020-07-08 12:13:32', '2020-07-13 21:35:16'),
(69, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Gault 5-44 Pad 1 #221R) DRL', NULL, 'ODR20188', NULL, 'jthorn@eeronline.com', 'John Thorn', NULL, NULL, '2020-07-08 12:13:32', '2020-07-13 21:35:16'),
(70, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Perro Rojo 20 #4', NULL, NULL, NULL, NULL, 'Oscar Rodriguez', NULL, NULL, '2020-07-08 12:13:32', '2020-07-13 21:35:16'),
(71, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'WTH 24-13 Pad 4 (271, 281, 282) DRL', NULL, 'OPD19042', NULL, 'fruiz@eeronline.com', 'Fidel Ruiz', NULL, NULL, '2020-07-08 12:13:32', '2020-07-13 21:35:16'),
(72, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Mae Weaver BTY #1, #2, #3', NULL, NULL, NULL, 'tgriffin@eeronline.com', 'Travis Griffin', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(73, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Kraken 10-3 Pad 2 (East) #251', NULL, '19272', NULL, 'tricks@eeronline.com', 'Todd Ricks', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(74, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Ball Lease Well #3', NULL, NULL, NULL, NULL, 'Dale Sullins', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(75, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Darla 39-1', NULL, NULL, NULL, NULL, 'Gregorio Estrada', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(76, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Powell 17 #3', NULL, NULL, NULL, NULL, 'Greogrio Estrada', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(77, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Harvard #1', NULL, NULL, NULL, NULL, 'Gregorio Estrada', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(78, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, '8608 JV - P Braun #1', NULL, NULL, NULL, NULL, 'Gregorio Estrada', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(79, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Nail Ranch 1-122 Unit 2-8UA', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(80, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'WTH 22-15 C #231', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(81, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Quinn 21 #1', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(82, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'WTH 22-15 D #242', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(83, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'WTH 22-15 A #212', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(84, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Strain 20 #1', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(85, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Koonce 46 #2', NULL, NULL, NULL, NULL, 'Jaime Moreno', NULL, NULL, '2020-07-08 12:18:55', '2020-07-13 21:35:16'),
(86, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Slaughter 79 #2', NULL, NULL, NULL, 'belliot@eeronline.com', 'Brent Elliot', NULL, NULL, '2020-07-08 12:20:03', '2020-07-13 21:35:16'),
(87, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Bryant Ranch 10-1 SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 12:20:34', '2020-07-09 12:31:20'),
(88, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Rio Lavaca 28-48 ESL Pad #2 (112, 113, 222, 123) DRL', NULL, 'ODR20006, ODR20004', NULL, 'jthorn@eeronline.com', 'John Thorn', NULL, NULL, '2020-07-08 12:22:54', '2020-07-13 21:35:16'),
(89, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Jones 83 #3', NULL, NULL, NULL, NULL, 'Jesse Villegas', NULL, NULL, '2020-07-08 12:22:54', '2020-07-13 21:35:16'),
(90, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Hickman C Well #2', NULL, NULL, NULL, NULL, 'Dale Sullins', NULL, NULL, '2020-07-08 12:22:54', '2020-07-13 21:35:16'),
(91, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Rio Lavaca 28-48C #232', '140', 'ODR19035', NULL, 'bconnell@eeronline.com', 'Bobby Connell', NULL, NULL, '2020-07-08 12:25:57', '2020-07-13 21:35:16'),
(92, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Rhea 1-6 Unit 1 #231', 'Precision 596', 'ODR20015', NULL, 'precision596@eeronline.com', 'Bobby Connell', NULL, NULL, '2020-07-08 12:25:57', '2020-07-13 21:35:16'),
(93, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Rio Lavaca 28-48 A #112', '227', 'ODR20001', NULL, NULL, 'D.M. Green', NULL, NULL, '2020-07-08 12:29:40', '2020-07-13 21:35:16'),
(94, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Rio Llano 11-10 Pad 1 East #174', NULL, '19004', NULL, 'tricks@eeronline.com', 'Todd Ricks', NULL, NULL, '2020-07-08 12:29:40', '2020-07-13 21:35:16'),
(95, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Rio Llano 11-10 Pad 1 West', NULL, 'ODR19012', NULL, 'tricks@eeronline.com', 'Todd Ricks', NULL, NULL, '2020-07-08 12:29:40', '2020-07-13 21:35:16'),
(96, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Rio Llano 11-10 Pad 1 #273', NULL, 'ODR19005', NULL, 'tricks@eeronline.com', 'Todd Ricks', NULL, NULL, '2020-07-08 12:29:40', '2020-07-13 21:35:16'),
(97, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Rio Llano 11-10 Pad 1 #182', NULL, 'ODR19002', NULL, 'tricks@eeronline.com', 'Todd Ricks', NULL, NULL, '2020-07-08 12:29:40', '2020-07-13 21:35:16'),
(98, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Tarzan SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 12:30:09', '2020-07-09 12:31:20'),
(102, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Theodore #1 SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:24:35', '2020-07-09 12:31:20'),
(103, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Bryant Ranch SWD #37', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:24:35', '2020-07-09 12:31:20'),
(104, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Salt Lake SWD #1', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:24:35', '2020-07-09 12:31:20'),
(105, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Flower Grove SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:24:35', '2020-07-09 12:31:20'),
(106, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Martin County Water Asset', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:26:16', '2020-07-09 12:31:20'),
(107, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'McMorris 18 #1 SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:26:16', '2020-07-09 12:31:20'),
(108, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Yater SWD #2', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:26:16', '2020-07-09 12:31:20'),
(109, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Midland International SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:26:58', '2020-07-09 12:31:20'),
(110, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Midland National Bank SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:26:58', '2020-07-09 12:31:20'),
(111, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'All Red SWD #1', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:26:58', '2020-07-09 12:31:20'),
(112, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Brown SWD #1', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:28:55', '2020-07-09 12:31:20'),
(113, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Big Gulp SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:28:55', '2020-07-09 12:31:20'),
(114, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Riptide Recycle Facility SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:28:55', '2020-07-09 12:31:20'),
(115, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Tex Harvey 15 SWD #1', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:28:55', '2020-07-09 12:31:20'),
(116, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Gordon SWD #2', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:28:55', '2020-07-09 12:31:20'),
(117, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Sunflower SWD #1', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:28:55', '2020-07-09 12:31:20'),
(118, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'West Knott SWD #1', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:28:55', '2020-07-09 12:31:20'),
(119, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Liberty Bell SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-08 15:28:55', '2020-07-09 12:31:20'),
(120, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'BT Hale 12 #3- LOE', NULL, 'JOB #34.2731.91', NULL, 'tgriffin@eeronline.com', 'Travis Griffin', NULL, NULL, '2020-07-08 15:30:00', '2020-07-13 21:35:16'),
(121, 'customer', 373, NULL, NULL, NULL, NULL, NULL, NULL, 'Bay LTD Yard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-08 15:43:04', '2020-08-29 05:35:25'),
(122, 'customer', 352, NULL, NULL, NULL, NULL, NULL, NULL, 'Pronto Yard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-08 15:44:35', '2020-08-29 06:24:14'),
(123, 'customer', 329, NULL, NULL, 'Odessa', NULL, NULL, NULL, 'Renegade Yard', NULL, NULL, '432-305-7341', NULL, 'Patrick Fry', NULL, NULL, '2020-07-08 15:47:25', '2020-07-08 16:35:49'),
(124, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Bandit 29 State COM Well #504Y', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 15:47:25', '2020-07-08 16:35:49'),
(125, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Heart Throb 17 State #706H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:18:48', '2020-07-08 16:35:49'),
(126, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Heart Throb 17 State #705H', NULL, '110898', NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:18:48', '2020-07-08 16:35:49'),
(127, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Heart Throb 17 State #701H', NULL, '109127', NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:20:51', '2020-07-08 16:35:49'),
(128, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'HP 651 McKnight 29 #4H', NULL, '112199', NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:20:51', '2020-07-08 16:35:49'),
(129, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'HP 651 McKnight 29 #5H', NULL, '112200', NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:20:51', '2020-07-08 16:35:49'),
(130, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Joaquin Unit #3H', NULL, '110999', NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:20:51', '2020-07-08 16:35:49'),
(131, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Noah Brunson A Unit #3H', 'HP 1208', '107831', NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:20:51', '2020-07-08 16:35:49'),
(132, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Durango 2 State COM #704H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:22:02', '2020-07-08 16:35:49'),
(133, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Durango 2 State COM #703H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:22:02', '2020-07-08 16:35:49'),
(134, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Durango 2 State COM #706H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:22:02', '2020-07-08 16:35:49'),
(135, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Durango 2 State COM #708H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:22:54', '2020-07-08 16:35:49'),
(136, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Durango 2 State COM #701H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:22:54', '2020-07-08 16:35:49'),
(137, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Durango 2 State COM #702H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:22:54', '2020-07-08 16:35:49'),
(138, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Dragon 36 CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:23:54', '2020-07-08 16:35:49'),
(139, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Audacious 19 FED #721H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:23:54', '2020-07-08 16:35:49'),
(140, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Convoy Central CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:23:54', '2020-07-08 16:35:49'),
(141, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Fearless 23 CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:23:54', '2020-07-08 16:35:49'),
(142, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Green Drake 16 CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:26:14', '2020-07-08 16:35:49'),
(143, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Taipan BST State #1H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:26:14', '2020-07-08 16:35:49'),
(144, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Hearns 34 CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:26:14', '2020-07-08 16:35:49'),
(145, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Audacious 19 CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:26:14', '2020-07-08 16:35:49'),
(146, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Dauntless #713H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:26:14', '2020-07-08 16:35:49'),
(147, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Dauntless #715H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:27:41', '2020-07-08 16:35:49'),
(148, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Dauntless #734H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:27:41', '2020-07-08 16:35:49'),
(149, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Taipan-Adder 31 CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:27:41', '2020-07-08 16:35:49'),
(150, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Taipan 31 #502H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:27:41', '2020-07-08 16:35:49'),
(151, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Taipan 31 #503H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:27:41', '2020-07-08 16:35:49'),
(152, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Quijote 2 State COM #711H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:29:22', '2020-07-08 16:35:49'),
(153, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Valiant 24 FED COM #710H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:29:22', '2020-07-08 16:35:49'),
(154, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Valiant 24 FED COM #706H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:29:22', '2020-07-08 16:35:49'),
(155, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Valiant 24 FED COM #728H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:29:22', '2020-07-08 16:35:49'),
(156, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Valiant 24 FED COM #723H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:30:52', '2020-07-08 16:35:49'),
(157, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Dauntless 7 FED CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:30:52', '2020-07-08 16:35:49'),
(158, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Hearns 34 State #405H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:30:52', '2020-07-08 16:35:49'),
(159, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Bandit 29 #505H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:30:52', '2020-07-08 16:35:49'),
(160, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Durango 2 State COM #705H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:30:52', '2020-07-08 16:35:49'),
(161, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Durango 2 State COM #707H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:30:52', '2020-07-08 16:35:49'),
(162, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Savage 2 #505H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:32:09', '2020-07-08 16:35:49'),
(163, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Getty 5 FED COM #502H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:32:09', '2020-07-08 16:35:49'),
(164, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Getty 5 FED COM #501H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:32:09', '2020-07-08 16:35:49'),
(165, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Getty 5 FED COM #503H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:32:09', '2020-07-08 16:35:49'),
(166, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Quijote 2 FED CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:32:09', '2020-07-08 16:35:49'),
(167, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Red Hills North Hawk Falcon CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:33:15', '2020-07-08 16:35:49'),
(168, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Red Hills North Green Drake CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:33:15', '2020-07-08 16:35:49'),
(169, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Resolute BTO Federal #1', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:33:15', '2020-07-08 16:35:49'),
(170, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Quijote 2 State COM #715H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:33:15', '2020-07-08 16:35:49'),
(171, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Beowolf 33 SC CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:34:24', '2020-07-08 16:35:49'),
(172, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Funky Honks 8 FED CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:34:24', '2020-07-08 16:35:49'),
(173, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Hunter 21 CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:34:24', '2020-07-08 16:35:49'),
(174, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Foghorn/Leghorn 32 CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:34:24', '2020-07-08 16:35:49'),
(175, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Convoy 28 SC', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:34:24', '2020-07-08 16:35:49'),
(176, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Hawk 35 CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:35:49', '2020-07-08 16:35:49'),
(177, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Hawk 35 #10H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:35:49', '2020-07-08 16:35:49'),
(178, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Antietam 9 FED COM CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:35:49', '2020-07-08 16:35:49'),
(179, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'GEM 36 CTB', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:35:49', '2020-07-08 16:35:49'),
(180, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Mamba 30 #703H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:35:49', '2020-07-08 16:35:49'),
(181, 'customer', 329, NULL, NULL, NULL, NULL, NULL, NULL, 'Savage 2 #507H', NULL, NULL, NULL, 'manny_murillo@eogresources.com', 'Manny Murillo', NULL, NULL, '2020-07-08 16:35:49', '2020-07-08 16:35:49'),
(192, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Olympus E/W SWD', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-09 12:31:20', '2020-07-09 12:31:20'),
(193, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'Midland National Bank 39 SWD #1', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-09 12:31:20', '2020-07-09 12:31:20'),
(194, 'customer', 348, NULL, NULL, NULL, NULL, NULL, NULL, 'McMorris 18 #2D', NULL, NULL, NULL, NULL, 'Robert Knight', NULL, NULL, '2020-07-09 12:31:20', '2020-07-09 12:31:20'),
(195, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Shaq 38-14 4211H/4309H/4411H', NULL, 'White Crew- Double Eagle Energy', NULL, NULL, NULL, NULL, NULL, '2020-07-09 12:41:15', '2020-07-09 14:15:45'),
(196, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Frank Powell 30-31 #3111AH I- 3151 CH- 3101BH', NULL, 'Purple Crew- XTO Energy', NULL, NULL, NULL, NULL, NULL, '2020-07-09 12:41:15', '2020-07-09 14:15:45'),
(197, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'WTH 22-15 B 22', NULL, 'Grey Crew- Endeavor Energy Resources', NULL, NULL, NULL, NULL, NULL, '2020-07-09 12:41:15', '2020-07-09 14:15:45'),
(198, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'McClintic O\'Daniel 60 #13H/14H/15H/18H', NULL, 'PPS3- Pioneer Resources', NULL, NULL, NULL, NULL, NULL, '2020-07-09 12:41:15', '2020-07-09 14:15:45'),
(200, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Brook B-17 A-17 B-8H C-9H', NULL, 'PPS2- Pioneer Resources', NULL, NULL, NULL, NULL, NULL, '2020-07-09 12:45:08', '2020-07-09 14:15:45'),
(201, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Adam 43-6 B JM 502LS/501LS', NULL, 'Navy Crew- Diamondback Energy', NULL, NULL, NULL, NULL, NULL, '2020-07-09 12:45:08', '2020-07-09 14:15:45'),
(202, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Guitar South 17-8-A 4202H/ 17-8-C 4206H', NULL, 'Brown Crew- Parsley Energy', NULL, NULL, NULL, NULL, NULL, '2020-07-09 12:45:08', '2020-07-09 14:15:45'),
(203, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Adam 43-6 C 102WA/202WB', NULL, 'Black Crew- Diamondback Energy', NULL, NULL, NULL, NULL, NULL, '2020-07-09 12:45:08', '2020-07-09 14:15:45'),
(204, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Charles Midkiff 4701/4711/4751/4781', NULL, 'Aqua Crew- XTO Energy', NULL, NULL, NULL, NULL, NULL, '2020-07-09 12:45:08', '2020-07-09 14:15:45'),
(205, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Propetro Midkiff Yard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 12:45:08', '2020-07-09 14:15:45'),
(208, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Sits 9-4 2WA', NULL, 'Navy Crew- Diamondback Energy', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(209, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'PLU 25 Brushy Draw 124H/104H/703H', NULL, 'Tan Crew- XTO Energy', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(210, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Cassidy Pad 4 #3075JH/3025SH/3065DH/3005BH', NULL, 'Red Crew', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(211, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Watkins-Wavle 1204 A-1H/B-2H/C-3H', NULL, 'PPS2', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(212, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Cassidy Unit #3028SH/30118AH/3007BH', NULL, 'Purple Crew', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(213, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Mabee Breedlove D 2408LS/2408MS/2408WA', NULL, 'Black Crew- Diamondback Energy', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(214, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Bigtooth Maple H #13HB/14HA', NULL, 'Green Crew- CrownQuest', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(215, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Blagrave O 31-43 #2815H/4215H', NULL, 'Bronze Crew- Double Eagle III', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(216, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Ringo 8-9 Marienfeld 13-24-B #2704H/2806H/4104H/4404H', NULL, 'Brown Crew- Parsley Energy', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(217, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Brook N-13 P-113H/Q-114H/R-115H', NULL, 'PPS1- Pioneer Resources', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(218, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Cosmo Daltex Unit 45-4 #28WB', NULL, 'Peach Crew- Admiral Permian', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(219, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Rio Wichita 15-10 ESL 1 #123/221/222/231/132/143/241/242', NULL, 'Platinum Crew- Endeavor Energy Resources', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(220, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Kennett 29-41 Pad 1 #271/241/251/231', NULL, 'Grey Crew- Endeavor Energy Resources', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(221, 'customer', 330, NULL, NULL, NULL, NULL, NULL, NULL, 'Comancheros 3181 NR', NULL, 'Aqua Crew- XTO Energy', NULL, NULL, NULL, NULL, NULL, '2020-07-09 13:03:26', '2020-07-09 14:15:45'),
(223, 'customer', 362, NULL, NULL, NULL, NULL, NULL, NULL, 'LSX2 MV1 #8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 16:06:23', '2020-07-09 16:06:23'),
(224, 'customer', 362, NULL, NULL, NULL, NULL, NULL, NULL, 'LSX2 MV1 #9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 16:06:23', '2020-07-09 16:06:23'),
(225, 'customer', 362, NULL, NULL, NULL, NULL, NULL, NULL, 'LSX2 MV1 #1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 16:06:23', '2020-07-09 16:06:23'),
(226, 'customer', 362, NULL, NULL, NULL, NULL, NULL, NULL, 'LSX2 MV1 #2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 16:06:23', '2020-07-09 16:06:23'),
(227, 'customer', 362, NULL, NULL, NULL, NULL, NULL, NULL, 'LSX2 MV1 #5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 16:06:23', '2020-07-09 16:06:23'),
(228, 'customer', 362, NULL, NULL, NULL, NULL, NULL, NULL, 'LSX2 MV1 #6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 16:06:23', '2020-07-09 16:06:23'),
(229, 'customer', 362, NULL, NULL, NULL, NULL, NULL, NULL, 'LSX2 MV1 #4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 16:06:23', '2020-07-09 16:06:23'),
(230, 'customer', 362, NULL, NULL, NULL, NULL, NULL, NULL, 'LSX2 MV1 #7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 16:06:23', '2020-07-09 16:06:23'),
(231, 'customer', 362, NULL, NULL, NULL, NULL, NULL, NULL, 'LSX2 MV1 #3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 16:06:23', '2020-07-09 16:06:23'),
(232, 'customer', 362, NULL, NULL, NULL, NULL, NULL, NULL, 'Baden Launcher', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 16:06:23', '2020-07-09 16:06:23'),
(233, 'customer', 360, NULL, NULL, NULL, NULL, NULL, NULL, 'Curacao 18 Unit #211H', NULL, '4238919084', NULL, NULL, 'Chris Baehl', NULL, NULL, '2020-07-09 16:11:22', '2020-07-09 16:11:22'),
(234, 'customer', 338, NULL, NULL, NULL, NULL, NULL, NULL, 'Frances 12-1 12-1B #2704H', NULL, NULL, NULL, NULL, 'Jeremy C.', NULL, NULL, '2020-07-09 16:44:26', '2020-07-09 17:59:45'),
(235, 'customer', 338, NULL, NULL, NULL, NULL, NULL, NULL, 'Claire 48-37-A #4101H', NULL, NULL, NULL, NULL, 'Tony Hernandez', NULL, NULL, '2020-07-09 16:44:26', '2020-07-09 17:59:45'),
(236, 'customer', 338, NULL, NULL, NULL, NULL, NULL, NULL, 'Reese 15-10-D #4307H', NULL, NULL, NULL, NULL, 'Robert Holley', NULL, NULL, '2020-07-09 16:44:26', '2020-07-09 17:59:45'),
(237, 'customer', 338, NULL, NULL, NULL, NULL, NULL, NULL, 'Strain Ranch 12-1 #4206H', NULL, NULL, NULL, NULL, 'Buck Sifuentes', NULL, NULL, '2020-07-09 17:59:45', '2020-07-09 17:59:45'),
(238, 'customer', 338, NULL, NULL, NULL, NULL, NULL, NULL, 'Strain Ranch 12-1 #4304H', NULL, NULL, NULL, NULL, 'Buck Sifuentes', NULL, NULL, '2020-07-09 17:59:45', '2020-07-09 17:59:45'),
(239, 'customer', 338, NULL, NULL, NULL, NULL, NULL, NULL, 'Strain Ranch 12-1 #4407H', NULL, NULL, NULL, NULL, 'Buck Sifuentes', NULL, NULL, '2020-07-09 17:59:45', '2020-07-09 17:59:45'),
(240, 'customer', 338, NULL, NULL, NULL, NULL, NULL, NULL, 'Cormac 8-17-A #4302H', NULL, NULL, NULL, NULL, 'Buck Sifuentes', NULL, NULL, '2020-07-09 17:59:45', '2020-07-09 17:59:45'),
(241, 'customer', 377, NULL, NULL, NULL, NULL, NULL, NULL, 'Mabee DDA F15 #3310AH', 'Latshaw 44', 'NMB020107DR', NULL, NULL, NULL, NULL, NULL, '2020-07-09 18:01:39', '2020-07-09 18:01:39'),
(242, 'customer', 377, NULL, NULL, NULL, NULL, NULL, NULL, 'Mabee DDA #509KS', 'Nabors X46', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 18:01:39', '2020-07-09 18:01:39'),
(243, 'customer', 377, NULL, NULL, NULL, NULL, NULL, NULL, 'Mabee DDA #3907FB', 'Nabors X46', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 18:01:39', '2020-07-09 18:01:39'),
(244, 'customer', 377, NULL, NULL, NULL, NULL, NULL, NULL, 'Mabee DDA #3907FS', 'Nabors X46', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 18:01:39', '2020-07-09 18:01:39'),
(245, 'customer', 377, NULL, NULL, NULL, NULL, NULL, NULL, 'Mabee DDA #3907FM', 'Nabors X46', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 18:01:40', '2020-07-09 18:01:40'),
(246, 'customer', 372, NULL, NULL, NULL, NULL, NULL, NULL, 'Mitchell B Fee 16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 18:02:36', '2020-08-29 06:18:25'),
(247, 'customer', 372, NULL, NULL, NULL, NULL, NULL, NULL, 'Cone 10 XOG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 18:02:36', '2020-08-29 06:18:25'),
(248, 'customer', 372, NULL, NULL, NULL, NULL, NULL, NULL, 'McKenney T.L. D-41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 18:02:36', '2020-08-29 06:18:25'),
(249, 'customer', 372, NULL, NULL, NULL, NULL, NULL, NULL, 'McClevan Chapman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 18:02:36', '2020-08-29 06:18:25'),
(250, 'customer', 343, NULL, NULL, NULL, NULL, NULL, NULL, 'Poker Lake Unit Big Sink 154H', '118', NULL, NULL, NULL, 'Corey Bragg', NULL, NULL, '2020-07-09 18:04:49', '2020-07-09 18:04:49'),
(251, 'customer', 343, NULL, NULL, NULL, NULL, NULL, NULL, 'Cimarex/Cactus 148 Cappleton West #4H', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 18:04:49', '2020-07-09 18:04:49'),
(252, 'customer', 343, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Precision 553', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 18:04:49', '2020-07-09 18:04:49'),
(253, 'customer', 343, NULL, NULL, NULL, NULL, NULL, NULL, 'Jason Brown UL G 1-24 Unit #4H', 'Precision 599 Elevation', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-09 18:04:49', '2020-07-09 18:04:49'),
(254, 'customer', 369, NULL, NULL, NULL, NULL, NULL, NULL, 'Charger H-140 WB', 'Lasso 101', '42078', NULL, NULL, 'Jim S.', NULL, NULL, '2020-07-09 18:07:05', '2020-07-09 18:07:05'),
(255, 'customer', 369, NULL, NULL, NULL, NULL, NULL, NULL, 'Charger H-160 UH', 'Latshaw 16', '42165', NULL, NULL, NULL, NULL, NULL, '2020-07-09 18:07:05', '2020-07-09 18:07:05'),
(256, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, 'Dickenson 18-7 ESL Pad 3 #111', NULL, 'ODR19039', '34555', NULL, NULL, NULL, NULL, '2020-07-09 18:09:15', '2020-07-13 21:35:16'),
(260, 'customer', 342, 'tst line1', NULL, NULL, NULL, NULL, NULL, 'tst lease', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-13 07:13:50', '2020-07-13 21:35:16'),
(261, 'customer', 342, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asdsadsa', NULL, NULL, NULL, NULL, '2020-07-13 10:56:45', '2020-07-13 21:35:16'),
(262, 'customer', 379, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '34.344', '344.234', '2020-07-20 07:52:13', '2020-08-29 06:47:59'),
(263, 'customer', 379, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'adas@sadas.com', NULL, '32.324', '34.34', '2020-07-20 07:52:50', '2020-08-29 06:47:59');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(10) UNSIGNED NOT NULL,
  `subcategory_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `cat_id`, `subcategory_name`, `type`, `created_at`, `updated_at`) VALUES
(1, 1, 'Vaccum Truck', 'inventory', '2020-08-13 00:54:34', '2020-08-13 07:25:27'),
(2, 1, 'rs truck', 'inventory', '2020-09-01 06:49:58', '2020-09-03 06:18:24');

-- --------------------------------------------------------

--
-- Table structure for table `subcontractor_agreement`
--

CREATE TABLE `subcontractor_agreement` (
  `id` int(11) NOT NULL,
  `day` varchar(191) DEFAULT NULL,
  `month` varchar(191) DEFAULT NULL,
  `year` varchar(191) DEFAULT NULL,
  `subcontractor_name` varchar(191) DEFAULT NULL,
  `type_of_entity` varchar(191) DEFAULT NULL,
  `mailing_addr` varchar(191) DEFAULT NULL,
  `physical_addr` varchar(191) DEFAULT NULL,
  `contact_person` varchar(191) DEFAULT NULL,
  `contact_person_title` varchar(191) DEFAULT NULL,
  `contact_person_email` varchar(191) DEFAULT NULL,
  `contact_person_phone` varchar(191) DEFAULT NULL,
  `contact_person_fax` varchar(191) DEFAULT NULL,
  `general_liability_limit` varchar(191) DEFAULT NULL,
  `automobile_liability_limit` varchar(191) DEFAULT NULL,
  `umbrella_limit` varchar(191) DEFAULT NULL,
  `employer_liability_limit` varchar(191) DEFAULT NULL,
  `riggers_liability_limit` varchar(191) DEFAULT NULL,
  `motor_trade_limit` varchar(191) DEFAULT NULL,
  `installation_floater_limit` varchar(191) DEFAULT NULL,
  `notice_contractor_name` varchar(191) DEFAULT NULL,
  `notice_contractor_address` varchar(191) DEFAULT NULL,
  `notice_contractor_phone` varchar(191) DEFAULT NULL,
  `notice_contractor_email` varchar(191) DEFAULT NULL,
  `notice_contractor_attn` varchar(191) DEFAULT NULL,
  `notice_subcontractor_name` varchar(191) DEFAULT NULL,
  `notice_subcontractor_address` varchar(191) DEFAULT NULL,
  `notice_subcontractor_phone` varchar(191) DEFAULT NULL,
  `notice_subcontractor_email` varchar(191) DEFAULT NULL,
  `notice_subcontractor_contact` varchar(191) DEFAULT NULL,
  `executed_day` varchar(191) DEFAULT NULL,
  `executed_month` varchar(191) DEFAULT NULL,
  `executed_year` varchar(191) DEFAULT NULL,
  `executed_contractor_name` varchar(191) DEFAULT NULL,
  `executed_contractor_by` varchar(191) DEFAULT NULL,
  `executed_contractor_title` varchar(191) DEFAULT NULL,
  `executed_contractor_date` varchar(191) DEFAULT NULL,
  `executed_subcontractor_name` varchar(191) DEFAULT NULL,
  `executed_subcontractor_by` varchar(191) DEFAULT NULL,
  `executed_subcontractor_title` varchar(191) DEFAULT NULL,
  `executed_subcontractor_date` varchar(191) DEFAULT NULL,
  `contractor_initial` varchar(191) DEFAULT NULL,
  `subcontractor_initial` varchar(191) DEFAULT NULL,
  `submitted_by` varchar(191) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `agreement_send_mail_to` varchar(191) DEFAULT NULL,
  `agreement_send_mail_date` varchar(191) DEFAULT NULL,
  `submitted_by_name` varchar(191) DEFAULT NULL,
  `submitted_by_email` varchar(191) DEFAULT NULL,
  `signed_by` varchar(191) DEFAULT NULL,
  `submitted_to_insurance` varchar(191) DEFAULT NULL,
  `submitted_date_insurance` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `access_code` varchar(191) DEFAULT NULL,
  `url_token` varchar(191) DEFAULT NULL,
  `evos_access_code` varchar(191) DEFAULT NULL,
  `evos_url_token` varchar(191) DEFAULT NULL,
  `reject` text DEFAULT NULL,
  `sent` text DEFAULT NULL,
  `seen` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcontractor_agreement`
--

INSERT INTO `subcontractor_agreement` (`id`, `day`, `month`, `year`, `subcontractor_name`, `type_of_entity`, `mailing_addr`, `physical_addr`, `contact_person`, `contact_person_title`, `contact_person_email`, `contact_person_phone`, `contact_person_fax`, `general_liability_limit`, `automobile_liability_limit`, `umbrella_limit`, `employer_liability_limit`, `riggers_liability_limit`, `motor_trade_limit`, `installation_floater_limit`, `notice_contractor_name`, `notice_contractor_address`, `notice_contractor_phone`, `notice_contractor_email`, `notice_contractor_attn`, `notice_subcontractor_name`, `notice_subcontractor_address`, `notice_subcontractor_phone`, `notice_subcontractor_email`, `notice_subcontractor_contact`, `executed_day`, `executed_month`, `executed_year`, `executed_contractor_name`, `executed_contractor_by`, `executed_contractor_title`, `executed_contractor_date`, `executed_subcontractor_name`, `executed_subcontractor_by`, `executed_subcontractor_title`, `executed_subcontractor_date`, `contractor_initial`, `subcontractor_initial`, `submitted_by`, `status`, `agreement_send_mail_to`, `agreement_send_mail_date`, `submitted_by_name`, `submitted_by_email`, `signed_by`, `submitted_to_insurance`, `submitted_date_insurance`, `created_at`, `updated_at`, `access_code`, `url_token`, `evos_access_code`, `evos_url_token`, `reject`, `sent`, `seen`) VALUES
(1, '3', 'february', '2020', 'sc', 'llc', '2131@34234.com', 'asdasd', 'dsad sadsad', 'dasdasd', 'asdsad@asdsad.com', '(231) 231-2312', '(123) 123-1231', '1,000,000.00', '1,000,000.00', '600,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(231) 231-2312', 'asdsad@asdsad.com', 'dsad sadsad22', '12', 'march', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', NULL, 'sadsad', 'asdsadsa', 'dasdasd', '08/10/2020', NULL, NULL, NULL, 3, 'looklike.himu@gmail.com', '08/09/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-09 01:00:38', '2020-08-09 02:36:14', '192527', '1-CJYT9MW8F3KZEPA7RNX0SLQBHVD4I2', NULL, NULL, '<p>eqeeqweq</p><p>eqweqwewqeqwq</p>', '[\"2020-08-09T07:00:38.724015Z\",\"2020-08-09T07:16:17.732346Z\",\"2020-08-09T08:33:05.226229Z\"]', '[\"2020-08-09T07:02:54.575977Z\",\"2020-08-09T07:03:31.390371Z\",\"2020-08-09T08:18:21.705334Z\",\"2020-08-09T08:36:14.834984Z\"]'),
(2, '12', 'february', '2020', 'sc', 'corporation', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', '213123', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'march', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/05/2020', 'dasa', 'dasd', 'daasda', '08/23/2020', NULL, NULL, NULL, 3, 'looklike.himu@gmail.com', '08/09/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-09 07:55:15', '2020-08-09 07:59:44', '427962', '2-TS7YGRPVN38B19E2HZ4WKOJ5U6IXFL', NULL, NULL, NULL, '[\"2020-08-09T13:55:15.401656Z\",\"2020-08-09T13:58:04.370368Z\"]', '[\"2020-08-09T13:56:00.618241Z\",\"2020-08-09T13:58:26.069282Z\",\"2020-08-09T13:59:44.392602Z\"]'),
(3, '2', 'february', '2020', 'dasd sad', 'limited_partnership', 'asda@asds.com', 'sad asd dasd', 'dsad sadsad', 'asdasd asdsad', 'dasda@asdsa.com', '(312) 321-3213', '(123) 123-1231', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'dasd sad', 'asda@asds.com', '(312) 321-3213', 'dasda@asdsa.com', 'dsad sadsad', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', NULL, 'assad', 'asdas', 'asdasd', '08/11/2020', NULL, NULL, NULL, 3, 'looklike.himu@gmail.com', '08/10/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-10 05:04:56', '2020-08-10 05:24:29', '247189', '3-2U9F7YZ4OGRXSVC8QPBHTJ16W5DM0N', NULL, NULL, NULL, '[\"2020-08-10T11:04:56.460410Z\",\"2020-08-10T11:21:56.299305Z\",\"2020-08-10T11:24:29.293207Z\"]', '[\"2020-08-10T11:05:55.236557Z\",\"2020-08-10T11:23:19.780610Z\"]'),
(4, '2', 'february', '2020', '31', 'sole_proprietorship', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', '213123', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', '31', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'february', NULL, 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/05/2020', 'dasa', 'dasd', 'daasda', '08/04/2020', NULL, NULL, NULL, 2, 'looklike.himu@gmail.com', '08/11/2020', 'test1', 'eg.himel@gmail.com', NULL, 'himel.biswas@asciisys.com', '08/11/2020', '2020-08-10 23:08:30', '2020-08-11 01:32:52', '644918', '4-ZCSOU6G42X1KRVIEJBPD5N7T890WFM', NULL, NULL, NULL, '[\"2020-08-11T05:08:30.741741Z\",\"2020-08-11T06:43:52.393166Z\",\"2020-08-11T06:53:54.067205Z\",\"2020-08-11T07:09:01.157182Z\",\"2020-08-11T07:10:17.147718Z\"]', '[\"2020-08-11T05:09:35.886411Z\",\"2020-08-11T05:13:19.952351Z\",\"2020-08-11T06:45:36.342343Z\",\"2020-08-11T06:45:49.777201Z\",\"2020-08-11T07:01:59.138035Z\",\"2020-08-11T07:03:16.528867Z\",\"2020-08-11T07:10:44.141583Z\",\"2020-08-11T07:12:49.902389Z\",\"2020-08-11T07:13:51.270115Z\"]'),
(5, '2', 'february', '2020', '31', 'llc', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', '213123', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', '31', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/11/2020', 'dasa', 'dasd', 'daasda', '08/12/2020', 'EVOS', 'asdsadsad', NULL, 2, 'looklike.himu@gmail.com', '08/11/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-11 01:50:33', '2020-08-11 02:00:13', '440854', '5-9X6AU3N517WJCLHYMVKR4Q0TFSDIP2', NULL, NULL, NULL, '[\"2020-08-11T07:50:33.893557Z\"]', '[\"2020-08-11T07:51:00.462681Z\"]'),
(6, '2', 'february', '2020', 'sc', 'limited_partnership', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', '213123', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '6,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', NULL, NULL, '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/11/2020', NULL, NULL, NULL, NULL, 'EVOS', NULL, NULL, 2, 'looklike.himu@gmail.com', '08/11/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-11 03:19:48', '2020-08-11 05:17:47', '477183', '6-3VJS5PFN0BDLC2714IAOX9ZKRY8EWQ', NULL, NULL, '<p>1.sadsad&nbsp;</p><p>2. sadsadas</p>', '[\"2020-08-11T09:19:48.833301Z\",\"2020-08-11T09:33:17.863105Z\"]', '[\"2020-08-11T09:20:20.501204Z\",\"2020-08-11T09:30:11.590025Z\",\"2020-08-11T09:33:54.631331Z\",\"2020-08-11T09:34:27.947356Z\",\"2020-08-11T09:36:04.535246Z\",\"2020-08-11T09:36:52.880008Z\",\"2020-08-11T09:38:58.006131Z\",\"2020-08-11T09:39:27.947769Z\",\"2020-08-11T09:40:11.638494Z\",\"2020-08-11T09:40:36.355077Z\",\"2020-08-11T09:40:57.222008Z\",\"2020-08-11T09:41:17.986188Z\",\"2020-08-11T09:44:35.857388Z\",\"2020-08-11T09:47:19.641148Z\",\"2020-08-11T09:48:15.553636Z\",\"2020-08-11T09:50:01.661942Z\",\"2020-08-11T09:52:13.515965Z\",\"2020-08-11T09:52:28.574243Z\",\"2020-08-11T09:52:50.049872Z\",\"2020-08-11T09:55:22.050553Z\",\"2020-08-11T09:56:16.501142Z\",\"2020-08-11T10:01:04.466676Z\",\"2020-08-11T10:17:35.161054Z\",\"2020-08-11T10:24:44.547988Z\",\"2020-08-11T10:40:30.126223Z\",\"2020-08-11T10:41:13.562844Z\",\"2020-08-11T11:03:54.185709Z\",\"2020-08-11T11:17:36.120458Z\"]'),
(7, '2', 'february', '2020', 'sc', 'corporation', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', '213123', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', NULL, NULL, '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/11/2020', NULL, NULL, NULL, NULL, 'EVOS', NULL, NULL, 2, 'looklike.himu@gmail.com', '08/11/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-11 05:19:57', '2020-08-11 05:21:04', '933858', '7-MZ8XR457EH3TLBNVO2GAFYQ1CK09JI', NULL, NULL, NULL, '[\"2020-08-11T11:19:57.622409Z\"]', '[\"2020-08-11T11:20:35.739056Z\"]'),
(8, NULL, NULL, NULL, 'sc', 'corporation', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', NULL, '213123', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', NULL, 'asda@asdda.com', '213', NULL, NULL, NULL, 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/11/2020', NULL, NULL, NULL, NULL, 'EVOS', NULL, NULL, 1, 'looklike.himu@gmail.com', '08/11/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-11 05:24:41', '2020-08-11 05:27:29', '406206', '8-IJUEH06VMRQB4TPFL89N3WA2X1GKDC', NULL, NULL, NULL, '[\"2020-08-11T11:24:41.221235Z\"]', '[\"2020-08-11T11:26:32.044337Z\",\"2020-08-11T11:27:29.045892Z\"]'),
(9, '2', 'may', '2020', 'sc', 'llc', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', '213123', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/11/2020', 'dasa', 'dasd', 'daasda', '08/12/2020', 'EVOS', 'asdsadsad', NULL, 3, 'looklike.himu@gmail.com', '08/11/2020', 'test1', 'eg.himel@gmail.com', 'hhhtttt', 'looklike.himu@gmail.com', '08/11/2020', '2020-08-11 07:17:30', '2020-08-11 07:35:43', '690625', '9-F1CS0GJWVNPIZ4L6T85X9AED2R73HU', NULL, NULL, NULL, '[\"2020-08-11T13:17:30.958053Z\",\"2020-08-11T13:21:59.968316Z\",\"2020-08-11T13:31:38.240794Z\",\"2020-08-11T13:35:23.092801Z\"]', '[\"2020-08-11T13:18:13.620739Z\",\"2020-08-11T13:22:38.057054Z\",\"2020-08-11T13:32:48.615686Z\",\"2020-08-11T13:35:43.646028Z\"]'),
(10, '2', 'may', '2020', 'sc', 'llc', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', '213123', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'march', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/11/2020', 'dasa', 'dasd', 'daasda', '08/11/2020', 'EVOS', 'asdsadsad', NULL, 3, 'looklike.himu@gmail.com', '08/11/2020', 'test1', 'eg.himel@gmail.com', 'hjhjhj', 'looklike.himu@gmail.com', '08/11/2020', '2020-08-11 08:23:31', '2020-08-11 08:48:15', '749915', '10-PR15IVN9XHTBGU6YE4807JMO3AFSWZ', NULL, NULL, NULL, '[\"2020-08-11T14:23:31.099356Z\",\"2020-08-11T14:28:26.476146Z\",\"2020-08-11T14:47:23.448429Z\"]', '[\"2020-08-11T14:23:57.114676Z\",\"2020-08-11T14:28:50.385041Z\",\"2020-08-11T14:32:36.993960Z\",\"2020-08-11T14:48:15.451572Z\"]'),
(11, '2', 'february', '2020', 'sc', 'llc', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', '213123', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/12/2020', 'dasa', 'dasd', 'daasda', '08/11/2020', 'EVOS', 'asdsadsad', NULL, 3, 'looklike.himu@gmail.com', '08/12/2020', 'test1', 'eg.himel@gmail.com', 'tttt', 'looklike.himu@gmail.com', '08/12/2020', '2020-08-11 23:36:50', '2020-08-11 23:41:40', '290827', '11-0KD2GHA6F73ZLIVNER9UJSY8CM41BQ', NULL, NULL, NULL, '[\"2020-08-12T05:36:50.305895Z\",\"2020-08-12T05:39:58.924050Z\",\"2020-08-12T05:41:21.376609Z\"]', '[\"2020-08-12T05:37:38.784906Z\",\"2020-08-12T05:40:22.983477Z\",\"2020-08-12T05:41:40.031377Z\"]'),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/13/2020', NULL, NULL, NULL, NULL, 'EVOS', NULL, NULL, 1, 'looklike.himu@gmail.com', '08/13/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-13 07:19:24', '2020-08-13 07:19:24', '714371', '12-ET8RMZK9FPAG17XY4ODQJ5I0S6WVC3', NULL, NULL, NULL, '[\"2020-08-13T13:19:24.557297Z\"]', NULL),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/16/2020', NULL, NULL, NULL, NULL, 'EVOS', NULL, NULL, 1, 'looklike.himu@gmail.com', '08/16/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-16 02:02:03', '2020-08-16 07:26:08', '826379', '13-U0TE3KAB5P28XMLV69YCO17QDNGISH', NULL, NULL, NULL, '[\"2020-08-16T08:02:03.650597Z\"]', '[\"2020-08-16T08:02:47.232713Z\",\"2020-08-16T08:37:13.251547Z\",\"2020-08-16T09:01:39.554579Z\",\"2020-08-16T09:03:06.668988Z\",\"2020-08-16T09:08:42.384297Z\",\"2020-08-16T09:09:14.019862Z\",\"2020-08-16T09:15:12.651027Z\",\"2020-08-16T09:16:25.125785Z\",\"2020-08-16T09:57:45.376023Z\",\"2020-08-16T10:00:15.724145Z\",\"2020-08-16T10:07:34.295844Z\",\"2020-08-16T10:08:34.707295Z\",\"2020-08-16T10:08:55.782585Z\",\"2020-08-16T10:10:18.811295Z\",\"2020-08-16T10:10:44.237858Z\",\"2020-08-16T10:11:10.431840Z\",\"2020-08-16T10:32:26.988033Z\",\"2020-08-16T13:14:07.350597Z\",\"2020-08-16T13:14:57.736146Z\",\"2020-08-16T13:23:40.847227Z\",\"2020-08-16T13:23:56.819890Z\",\"2020-08-16T13:26:08.569178Z\"]'),
(14, '2', 'february', '2020', 'sc', 'corporation', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', '213123', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/16/2020', 'dasa', 'dasd', 'daasda', '08/18/2020', 'EVOS', 'asdsadsad', NULL, 4, 'looklike.himu@gmail.com', '08/16/2020', 'test1', 'eg.himel@gmail.com', 'sdsad', NULL, NULL, '2020-08-16 07:32:35', '2020-08-17 08:20:44', '613789', '14-JFGV9BQNUEDZRHT6SX3WIL0CO5AMY7', NULL, NULL, '<p>1. eqwewqe</p><p>2. sadada</p>', '[\"2020-08-16T13:32:35.986582Z\",\"2020-08-17T12:03:21.115462Z\"]', '[\"2020-08-17T05:37:34.144701Z\",\"2020-08-17T10:42:55.456624Z\",\"2020-08-17T12:04:50.842877Z\",\"2020-08-17T12:06:38.209553Z\",\"2020-08-17T12:09:42.796473Z\",\"2020-08-17T12:14:56.420264Z\",\"2020-08-17T14:19:04.413258Z\",\"2020-08-17T14:19:22.949969Z\",\"2020-08-17T14:19:55.409971Z\",\"2020-08-17T14:20:44.964126Z\"]'),
(15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/16/2020', NULL, NULL, NULL, NULL, 'EVOS', NULL, NULL, 1, 'looklike.himu@gmail.com', '08/16/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-16 07:42:52', '2020-08-16 07:42:52', '131312', '15-W4GDNKI1PFHE0V9M7RQC5J326BL8YU', NULL, NULL, NULL, '[\"2020-08-16T13:42:52.831322Z\"]', NULL),
(16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/17/2020', NULL, NULL, NULL, NULL, 'EVOS', NULL, NULL, 1, 'looklike.himu@gmail.com', '08/17/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-16 23:37:02', '2020-08-16 23:37:02', '544915', '16-2HO7YW6XBZEJSP93VFK8MRLCN41T05', NULL, NULL, NULL, '[\"2020-08-17T05:37:02.157068Z\"]', NULL),
(17, '2', 'january', '2020', 'sc', 'corporation', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', '213123', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'january', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/18/2020', '2331', 'asda', 'sad', '08/04/2020', 'EVOS', 'sad', NULL, 3, 'looklike.himu@gmail.com', '08/18/2020', 'test1', 'eg.himel@gmail.com', NULL, 'looklike.himu@gmail.com', '08/18/2020', '2020-08-18 07:33:09', '2020-08-18 07:44:57', '112107', '17-01TQE2GCHR75PZODBAMSIFX983J4LK', NULL, NULL, NULL, '[\"2020-08-18T13:33:09.609839Z\",\"2020-08-18T13:40:21.538637Z\",\"2020-08-18T13:44:43.402469Z\"]', '[\"2020-08-18T13:34:42.895779Z\",\"2020-08-18T13:35:04.906184Z\",\"2020-08-18T13:43:36.095602Z\",\"2020-08-18T13:43:51.957447Z\",\"2020-08-18T13:44:57.979105Z\"]'),
(18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/18/2020', NULL, NULL, NULL, NULL, 'EVOS', NULL, NULL, 1, 'looklike.himu@gmail.com', '08/18/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-18 08:39:00', '2020-08-18 08:39:00', '444917', '18-KSR8GNAVJH67ZQFBY42I5XUT9DOEP0', NULL, NULL, NULL, '[\"2020-08-18T14:39:00.535954Z\"]', NULL),
(19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/19/2020', NULL, NULL, NULL, NULL, 'EVOS', NULL, NULL, 1, 'nnnllkklk ;lk;lk', '08/19/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-19 01:11:53', '2020-08-19 01:11:53', '781916', '19-V0IM5P89ZJNSRLH2TUA6QC1XOBY4FD', NULL, NULL, NULL, '[\"2020-08-19T07:11:53.717719Z\"]', NULL),
(20, '1', 'february', '2020', 'sc', 'llc', '2131@34234.com', '3123', '213', '3123', 'asda@asdda.com', '(123) 123-213', '213123', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(123) 123-213', 'asda@asdda.com', '213', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/19/2020', 'asda', 'dasd', 'dasd', '08/05/2020', NULL, 'asd', NULL, 4, 'looklike.himu@gmail.com', '08/19/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-19 07:59:38', '2020-08-19 08:24:36', '912604', '20-3ZD7HEAI9KSG1NMF52PLTBUOXJC408', NULL, NULL, '<p>1.sadsdd</p><p>2. sadsad</p>', '[\"2020-08-19T13:59:38.758893Z\",\"2020-08-19T14:02:03.968894Z\"]', '[\"2020-08-19T14:00:09.112143Z\",\"2020-08-19T14:24:36.432275Z\"]'),
(21, '2', 'february', '2020', 'sc', 'llc', '2131@34234.com', 'dgdg', '213', '432432432432432', 'jjjjsasa@adsa.com', '(423) 432-4324', '4234', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(423) 432-4324', 'jjjjsasa@adsa.com', '213', '12', 'january', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '08/19/2020', 'wwwe', 'qweq', 'qweqw', '07/27/2020', NULL, 'ewqe', NULL, 2, 'looklike.himu@gmail.com', '08/19/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-08-19 08:23:26', '2020-08-19 08:34:46', '596713', '21-WA2S4C8EP961QVDT5FZNL3JUO07YKG', NULL, NULL, NULL, '[\"2020-08-19T14:23:26.393741Z\"]', '[\"2020-08-19T14:25:30.420990Z\"]'),
(22, '2', 'february', '2020', 'sc', 'corporation', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', '(213) 123-1231', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '09/03/2020', 'dasa', 'dasd', 'daasda', '09/15/2020', NULL, 'asdsadsad', NULL, 3, 'looklike.himu@gmail.com', '09/03/2020', 'test1', 'eg.himel@gmail.com', NULL, 'himel.biswas@asciisys.com', '09/03/2020', '2020-09-03 07:46:17', '2020-09-03 09:31:07', '206042', '22-CVS06MQJR52FZEWDN8GAB7T91HKLPX', NULL, NULL, NULL, '[\"2020-09-03T13:46:17.162999Z\",\"2020-09-03T15:18:26.936441Z\",\"2020-09-03T15:21:03.160817Z\",\"2020-09-03T15:23:09.874489Z\",\"2020-09-03T15:24:18.859829Z\",\"2020-09-03T15:30:11.724536Z\"]', '[\"2020-09-03T13:46:52.135965Z\",\"2020-09-03T15:30:42.957482Z\",\"2020-09-03T15:31:07.683993Z\"]'),
(23, '2', 'february', '2020', 'sc', 'llc', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', '(213) 123-1231', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'january', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '09/03/2020', 'dasa', 'dasd', 'daasda', '09/08/2020', NULL, 'asdsadsad', NULL, 3, 'eg.himel@gmail.com', '09/03/2020', 'test1', 'eg.himel@gmail.com', NULL, 'himel.biswas@asciisys.com', '09/03/2020', '2020-09-03 11:24:12', '2020-09-03 11:26:48', '401801', '23-6FJ2ESRT39WU0XNK7AMQO5Z84YBLCD', NULL, NULL, NULL, '[\"2020-09-03T17:24:12.864246Z\",\"2020-09-03T17:26:48.294925Z\"]', '[\"2020-09-03T17:25:08.083653Z\"]'),
(24, '2', 'february', '2020', 'sc', 'corporation', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', '(213) 123-1231', '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'january', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '09/03/2020', 'dasa', 'dasd', 'daasda', '09/15/2020', NULL, 'asdsadsad', NULL, 3, 'looklike.himu@gmail.com', '09/03/2020', 'test1', 'eg.himel@gmail.com', NULL, 'himel.biswas@asciisys.com', '09/03/2020', '2020-09-03 11:29:22', '2020-09-03 11:35:19', '553645', '24-PV2IGL5140RKA8MCTNX67HD9BYOWFQ', NULL, NULL, NULL, '[\"2020-09-03T17:29:22.661836Z\",\"2020-09-03T17:35:19.854268Z\"]', '[\"2020-09-03T17:29:41.195266Z\",\"2020-09-03T17:33:00.799095Z\"]'),
(25, '2', 'february', '2020', 'sc', 'llc', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', NULL, '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '09/06/2020', '2331', 'dasd', 'daasda', '09/15/2020', NULL, 'asdsadsad', NULL, 2, 'looklike.himu@gmail.com', '09/06/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-09-05 23:25:58', '2020-09-05 23:29:53', '751619', '25-AY37F15ZETIWSRM26KVHQX0PNBOGUC', NULL, NULL, NULL, '[\"2020-09-06T05:25:58.223973Z\"]', '[\"2020-09-06T05:27:59.488081Z\"]'),
(26, '4', 'february', '2020', 'sc 232323', 'llc', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', NULL, '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc 232323', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '09/29/2020', 'dasa', 'dasd', 'daasda', '09/08/2020', NULL, 'asdsadsad', NULL, 2, 'looklike.himu@gmail.com', '09/29/2020', 'test1', 'eg.himel@gmail.com', NULL, 'elizabeth@mcanallywilkins.com', '09/29/2020', '2020-09-29 05:14:24', '2020-09-29 06:50:16', '802059', '26-P4YJCWVFLBR8Z2TU9NXMEI6G0D1OSH', NULL, NULL, NULL, '[\"2020-09-29T11:14:24.236361Z\",\"2020-09-29T12:35:29.215335Z\"]', '[\"2020-09-29T11:14:59.490690Z\",\"2020-09-29T11:16:39.291106Z\",\"2020-09-29T11:55:26.270104Z\",\"2020-09-29T11:56:19.376372Z\",\"2020-09-29T11:57:45.891317Z\",\"2020-09-29T12:35:59.849420Z\",\"2020-09-29T12:39:30.774276Z\",\"2020-09-29T12:39:47.955675Z\",\"2020-09-29T12:40:52.023972Z\",\"2020-09-29T12:41:08.536745Z\",\"2020-09-29T12:42:26.771413Z\",\"2020-09-29T12:45:08.436284Z\",\"2020-09-29T12:45:53.464882Z\"]'),
(28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '10/18/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'eg.himel@gmail.com', '10/18/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-10-18 05:29:13', '2020-10-18 05:29:13', '554982', '28-TKOZEQDJL3B8HG2PIY5RAC6F4W9MS7', NULL, NULL, NULL, '[\"2020-10-18T11:29:13.934564Z\"]', NULL),
(29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '10/18/2020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'eg.himel@gmail.com', '10/18/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-10-18 05:32:24', '2020-10-18 05:32:24', '429450', '29-BODQCEFYMXR7A4H2JVSN5601ZWG9KI', NULL, NULL, NULL, '[\"2020-10-18T11:32:24.016602Z\"]', NULL),
(30, '2', 'february', '2020', 'sc', 'corporation', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', NULL, '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'february', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '10/27/2020', 'dasa', 'dasd', 'daasda', '10/20/2020', NULL, 'asdsadsad', NULL, 2, 'looklike.himu@gmail.com', '10/27/2020', 'test1', 'eg.himel@gmail.com', NULL, 'elizabeth@mcanallywilkins.com', '10/27/2020', '2020-10-26 23:16:02', '2020-10-26 23:25:02', '411784', '30-N6THFR9C3UY15XZLGDEKAWOPSVQ8JM', NULL, NULL, NULL, '[\"2020-10-27T05:16:02.777495Z\",\"2020-10-27T05:23:13.036833Z\"]', '[\"2020-10-27T05:16:31.430708Z\",\"2020-10-27T05:24:17.596686Z\"]'),
(31, '3', 'february', '2020', 'sc', 'corporation', '2131@34234.com', 'asdasd', '213', 'wqeqwe33333', 'asda@asdda.com', '(312) 312-3213', NULL, '1,000,000.00', '1,000,000.00', '500,000.00', '1,000,000.00', '250,000.00', '250,000.00', '1,000,000.00', 'EV Oilfield Services', '903 West Industrial Ave. Midland, TX 79701', '(432) 253-9651', 'info@evoilfieldservices.com', 'Zeke Chavez', 'sc', '2131@34234.com', '(312) 312-3213', 'asda@asdda.com', '213', '12', 'january', '2020', 'EV Oilfield Services, LLC', 'Eric Sanchez', 'President', '10/27/2020', 'dasa', 'dasd', 'daasda', '10/06/2020', NULL, 'asdsadsad', NULL, 2, 'looklike.himu@gmail.com', '10/27/2020', 'test1', 'eg.himel@gmail.com', NULL, NULL, NULL, '2020-10-26 23:26:05', '2020-10-26 23:27:25', '517103', '31-WOCVSXADLU67MGZJEN8B0T52I1QP93', NULL, NULL, NULL, '[\"2020-10-27T05:26:05.441229Z\"]', '[\"2020-10-27T05:26:31.452873Z\"]');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `phone_no`, `status`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'siteadmin', 'baulsuphal@gmail.com', NULL, '$2y$10$LE.jxFruSA5HnQ3qg9ApFua4BNfDGsRz0FNLSC01OD/o96VY20caS', NULL, 'active', 'admin', 'Qp95fm0zVp0fmqunm60sUXFT8TtEsoVDOf9gQf48DRBR6mPdYQxev7wyw2QA', '2020-02-24 05:36:56', '2020-02-25 14:26:43'),
(2, 'test1', 'm.reasat38@gmail.com', NULL, '$2y$10$7H3zI9QIMWrvx08jfhADEuoP2pk2BGVWVynqK7q7es3Q.oMjFN16C', '0123456789', 'active', 'admin', '9ytfVhhBmnbW7Ob0xcedGzCe3rLLYkl8uvJcuUCcw6lzFEucwlcszNCf3SPD', '2020-02-24 10:49:43', '2020-03-01 15:20:22'),
(4, 'Michael', 'mcohen@evoilfieldservices.com', NULL, '$2y$10$HHNiKw1ON72PPlTYz/v7w.mCYMLsLeVm0.CXaodPzHZ7ybjSerqSK', '0123456789', 'active', 'admin', NULL, '2020-02-24 13:56:21', '2020-06-15 15:19:49'),
(5, 'Mayra Holguin', 'mholguin@evoilfieldservices.com', NULL, '$2y$10$zgq35./KFTNYcfKQb5f8g.LN3DT2TxxdQ4fHdSQ1jyWrc95quJGe6', NULL, 'active', 'admin', 'EL7rLVWV1o7aUdWRFKsemUrSVQezpysvgcolOACtmi5Vgkg5VCCmFixqm4xZ', '2020-06-15 18:04:14', '2020-06-15 18:04:14'),
(6, 'Ali Martinez', 'amartinez@evoilfieldservices.com', NULL, '$2y$10$Zp4x2NcETjEJd8wIspg4a.DmfAvMYt8zKgPE5PdN8mUa.wvMz5GJy', '(432) 253-9651 x105', 'active', 'admin', NULL, '2020-06-24 19:07:41', '2020-06-24 19:07:41');

-- --------------------------------------------------------

--
-- Table structure for table `vacuum_checklist`
--

CREATE TABLE `vacuum_checklist` (
  `id` int(11) NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  `pre_trip_id` int(11) DEFAULT NULL,
  `speci_plate` varchar(191) DEFAULT NULL,
  `pressure_vessel_code` varchar(191) DEFAULT NULL,
  `truck_ins_sticker` varchar(191) DEFAULT NULL,
  `truck_equip` varchar(191) DEFAULT NULL,
  `cargo_tank_clean` varchar(191) DEFAULT NULL,
  `press_valves_relief` varchar(191) DEFAULT NULL,
  `float_liq_level_ind` varchar(191) DEFAULT NULL,
  `vac_equi_50` varchar(191) DEFAULT NULL,
  `hoses_access` varchar(191) DEFAULT NULL,
  `conn_clean` varchar(191) DEFAULT NULL,
  `hoses_conn_fitt` varchar(191) DEFAULT NULL,
  `hoses_100mm` varchar(191) DEFAULT NULL,
  `bonding_hoss_ground` varchar(191) DEFAULT NULL,
  `strong_alligator` varchar(191) DEFAULT NULL,
  `wheel_chocks` varchar(191) DEFAULT NULL,
  `PTO_driven` varchar(191) DEFAULT NULL,
  `prop_hazard_load` varchar(191) DEFAULT NULL,
  `equipped_other_ins` varchar(191) DEFAULT NULL,
  `wearing_protection` varchar(191) DEFAULT NULL,
  `carrying_certification` varchar(191) DEFAULT NULL,
  `vacuum_truck_log` varchar(191) DEFAULT NULL,
  `truck_locate_35` varchar(191) DEFAULT NULL,
  `vacuum_pump_discharge` varchar(191) DEFAULT NULL,
  `temp_mat_not_75` varchar(191) DEFAULT NULL,
  `ope_truck_source` varchar(191) DEFAULT NULL,
  `truck_engine_shut_down` varchar(191) DEFAULT NULL,
  `truck_park_ground` varchar(191) DEFAULT NULL,
  `cond_path_truck_source` varchar(191) DEFAULT NULL,
  `grounded_metallic_structure` varchar(191) DEFAULT NULL,
  `vehicle_check_date` varchar(191) DEFAULT NULL,
  `printed_name` varchar(191) DEFAULT NULL,
  `speci_plate_input` varchar(191) DEFAULT NULL,
  `pressure_vessel_code_input` varchar(191) DEFAULT NULL,
  `truck_ins_sticker_input` varchar(191) DEFAULT NULL,
  `truck_equip_input` varchar(191) DEFAULT NULL,
  `cargo_tank_clean_input` varchar(191) DEFAULT NULL,
  `press_valves_relief_input` varchar(191) DEFAULT NULL,
  `float_liq_level_ind_input` varchar(191) DEFAULT NULL,
  `vac_equi_50_input` varchar(191) DEFAULT NULL,
  `hoses_access_input` varchar(191) DEFAULT NULL,
  `conn_clean_input` varchar(191) DEFAULT NULL,
  `hoses_conn_fitt_input` varchar(191) DEFAULT NULL,
  `hoses_100mm_input` varchar(191) DEFAULT NULL,
  `bonding_hoss_ground_input` varchar(191) DEFAULT NULL,
  `strong_alligator_input` varchar(191) DEFAULT NULL,
  `wheel_chocks_input` varchar(191) DEFAULT NULL,
  `PTO_driven_input` varchar(191) DEFAULT NULL,
  `prop_hazard_load_input` varchar(191) DEFAULT NULL,
  `equipped_other_ins_input` varchar(191) DEFAULT NULL,
  `wearing_protection_input` varchar(191) DEFAULT NULL,
  `carrying_certification_input` varchar(191) DEFAULT NULL,
  `vacuum_truck_log_input` varchar(191) DEFAULT NULL,
  `truck_locate_35_input` varchar(191) DEFAULT NULL,
  `vacuum_pump_discharge_input` varchar(191) DEFAULT NULL,
  `temp_mat_not_75_input` varchar(191) DEFAULT NULL,
  `ope_truck_source_input` varchar(191) DEFAULT NULL,
  `truck_engine_shut_down_input` varchar(191) DEFAULT NULL,
  `truck_park_ground_input` varchar(191) DEFAULT NULL,
  `cond_path_truck_source_input` varchar(191) DEFAULT NULL,
  `grounded_metallic_structure_input` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vacuum_checklist`
--

INSERT INTO `vacuum_checklist` (`id`, `job_id`, `pre_trip_id`, `speci_plate`, `pressure_vessel_code`, `truck_ins_sticker`, `truck_equip`, `cargo_tank_clean`, `press_valves_relief`, `float_liq_level_ind`, `vac_equi_50`, `hoses_access`, `conn_clean`, `hoses_conn_fitt`, `hoses_100mm`, `bonding_hoss_ground`, `strong_alligator`, `wheel_chocks`, `PTO_driven`, `prop_hazard_load`, `equipped_other_ins`, `wearing_protection`, `carrying_certification`, `vacuum_truck_log`, `truck_locate_35`, `vacuum_pump_discharge`, `temp_mat_not_75`, `ope_truck_source`, `truck_engine_shut_down`, `truck_park_ground`, `cond_path_truck_source`, `grounded_metallic_structure`, `vehicle_check_date`, `printed_name`, `speci_plate_input`, `pressure_vessel_code_input`, `truck_ins_sticker_input`, `truck_equip_input`, `cargo_tank_clean_input`, `press_valves_relief_input`, `float_liq_level_ind_input`, `vac_equi_50_input`, `hoses_access_input`, `conn_clean_input`, `hoses_conn_fitt_input`, `hoses_100mm_input`, `bonding_hoss_ground_input`, `strong_alligator_input`, `wheel_chocks_input`, `PTO_driven_input`, `prop_hazard_load_input`, `equipped_other_ins_input`, `wearing_protection_input`, `carrying_certification_input`, `vacuum_truck_log_input`, `truck_locate_35_input`, `vacuum_pump_discharge_input`, `temp_mat_not_75_input`, `ope_truck_source_input`, `truck_engine_shut_down_input`, `truck_park_ground_input`, `cond_path_truck_source_input`, `grounded_metallic_structure_input`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', 'n/a', '09/02/2020', 'rtert', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-23 06:45:12'),
(2, 10, 6, 'no', 'n/a', NULL, 'n/a', NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no.', 'n/a.', NULL, 'n/a.', NULL, 'no.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-23 09:54:40', '2020-09-23 09:54:40'),
(3, 1, 7, 'yes', 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-23 10:04:48');

-- --------------------------------------------------------

--
-- Table structure for table `variables`
--

CREATE TABLE `variables` (
  `id` int(11) NOT NULL,
  `type` varchar(191) DEFAULT NULL,
  `variable_name` varchar(191) DEFAULT NULL,
  `enable_disable` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `variables`
--

INSERT INTO `variables` (`id`, `type`, `variable_name`, `enable_disable`, `created_at`, `updated_at`) VALUES
(1, 'inventory', 'Fresh Water', 1, '2020-06-01 06:26:17', '2020-06-01 06:26:17'),
(2, 'inventory', 'Brine Water', 1, '2020-06-01 06:27:46', '2020-06-01 06:27:46'),
(3, 'inventory', 'Tank Wash', 1, '2020-06-01 06:28:53', '2020-06-01 06:28:53'),
(4, 'inventory', 'Disposal Fee - Liquid', 1, '2020-06-01 06:37:57', '2020-06-01 06:37:57'),
(5, 'inventory', 'Disposal Fee - Sludge', 1, '2020-06-01 06:39:07', '2020-06-01 06:39:07'),
(6, 'inventory', 'Disposal Fee - Heavy Sludge', 1, '2020-06-01 06:39:42', '2020-06-01 06:39:42'),
(7, 'inventory', 'Machine - Vehicle - Equipment Part ( AX325-5Az )', 1, '2020-06-01 06:44:54', '2020-06-01 07:07:24'),
(8, 'inventory', 'Machine - Vehicle - Equipment Part ( Labor )', 1, '2020-06-01 06:47:35', '2020-06-01 06:47:35'),
(9, 'inventory', 'Machine - Vehicle - Equipment Part ( BB32785 )', 1, '2020-06-01 06:49:10', '2020-06-01 07:09:06'),
(10, 'inventory', 'Fuel', 1, '2020-06-01 06:54:27', '2020-06-01 06:54:27'),
(11, 'inventory', 'Insurance', 1, '2020-06-01 06:55:26', '2020-06-01 06:55:26'),
(12, 'inventory', 'Schedule', 1, '2020-06-01 14:59:02', '2020-06-02 08:15:49'),
(13, 'inventory', 'Service Operator', 1, '2020-06-03 12:58:31', '2020-06-09 14:12:50');

-- --------------------------------------------------------

--
-- Table structure for table `variables_old`
--

CREATE TABLE `variables_old` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `var_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `var_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable_disable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variables_old`
--

INSERT INTO `variables_old` (`id`, `parent_id`, `type`, `var_name`, `var_type`, `enable_disable`, `created_at`, `updated_at`) VALUES
(1, 10, 'inventory', 'Disposal Fee', 'input', '1', '2020-05-06 13:05:00', '2020-05-13 13:36:58'),
(2, 10, 'inventory', 'Fresh Water', 'radio', '1', '2020-05-06 13:53:28', '2020-05-13 13:36:58'),
(4, 16, 'inventory', 'test', 'input', '0', '2020-05-12 15:39:44', '2020-05-12 15:39:44'),
(6, 10, 'inventory', 'Brine Water', 'radio', '1', '2020-05-12 20:13:38', '2020-05-13 13:36:58'),
(7, 10, 'inventory', 'Tank Wash Out', 'radio', '0', '2020-05-12 20:13:38', '2020-05-13 13:36:58'),
(8, 4, 'service', 'v1', 'checkbox', '1', '2020-05-14 11:36:07', '2020-05-14 11:36:07');

-- --------------------------------------------------------

--
-- Table structure for table `variable_parameters`
--

CREATE TABLE `variable_parameters` (
  `id` int(11) NOT NULL,
  `var_id` int(11) DEFAULT NULL,
  `var_param_name` varchar(191) DEFAULT NULL,
  `var_param_data_type` varchar(191) DEFAULT NULL,
  `var_param_value_type` varchar(191) DEFAULT NULL,
  `var_param_value` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `variable_parameters`
--

INSERT INTO `variable_parameters` (`id`, `var_id`, `var_param_name`, `var_param_data_type`, `var_param_value_type`, `var_param_value`, `created_at`, `updated_at`) VALUES
(1, 1, 'Cost - Per Barrel', 'decimal', '0', '1.50', '2020-06-01 06:26:17', '2020-06-01 06:26:17'),
(2, 1, 'Amount / Qty ( Barrel )', 'integer', '1', NULL, '2020-06-01 06:26:17', '2020-06-01 06:26:17'),
(3, 2, 'Cost - Per Barrel', 'decimal', '0', '2.10', '2020-06-01 06:27:46', '2020-06-01 06:27:46'),
(4, 2, 'Amount / Qty ( Barrel )', 'integer', '1', NULL, '2020-06-01 06:27:46', '2020-06-01 06:27:46'),
(5, 3, 'Cost - Per Tank', 'decimal', '0', '292.50', '2020-06-01 06:28:53', '2020-06-01 06:28:53'),
(6, 3, 'Amount / Qty ( Tank )', 'integer', '1', NULL, '2020-06-01 06:28:53', '2020-06-01 06:28:53'),
(7, 4, 'Cost - Per Barrel', 'decimal', '0', '1.15', '2020-06-01 06:37:57', '2020-06-01 06:37:57'),
(8, 4, 'Amount / Qty ( Barrel )', 'integer', '1', NULL, '2020-06-01 06:37:57', '2020-06-01 06:37:57'),
(9, 5, 'Cost - Per Barrel', 'decimal', '0', '12.35', '2020-06-01 06:39:07', '2020-06-01 06:39:07'),
(10, 5, 'Amount / Qty ( Barrel )', 'integer', '1', NULL, '2020-06-01 06:39:07', '2020-06-01 06:39:07'),
(11, 6, 'Cost - Per Barrel', 'decimal', '0', '16.90', '2020-06-01 06:39:42', '2020-06-01 06:39:42'),
(12, 6, 'Amount / Qty ( Barrel )', 'integer', '1', NULL, '2020-06-01 06:39:42', '2020-06-01 06:39:42'),
(13, 7, 'Part Number', 'string', '0', 'AX325-5Az', '2020-06-01 06:44:54', '2020-06-01 07:07:24'),
(14, 7, 'Cost - Per Headlight', 'decimal', '0', '85.93', '2020-06-01 06:44:54', '2020-06-01 07:07:24'),
(15, 7, 'Amount / Qty ( Headlight )', 'integer', '1', NULL, '2020-06-01 06:44:54', '2020-06-01 07:07:24'),
(16, 8, 'Cost - Per Hour', 'decimal', '0', '95.00', '2020-06-01 06:47:35', '2020-06-01 06:47:35'),
(17, 8, 'Amount / Qty ( Hour )', 'integer', '1', NULL, '2020-06-01 06:47:35', '2020-06-01 06:47:35'),
(18, 9, 'Part Number', 'string', '0', 'BB32785', '2020-06-01 06:49:10', '2020-06-01 07:09:06'),
(19, 9, 'Cost - Per Bumper', 'decimal', '0', '275.83', '2020-06-01 06:49:10', '2020-06-01 07:09:06'),
(20, 9, 'Amount / Qty ( Bumper )', 'integer', '1', NULL, '2020-06-01 06:49:10', '2020-06-01 07:09:06'),
(21, 10, 'Cost - Per Gallon', 'decimal', '0', '3.25', '2020-06-01 06:54:27', '2020-06-01 06:54:27'),
(22, 10, 'Amount / Qty ( Gallon )', 'integer', '1', NULL, '2020-06-01 06:54:27', '2020-06-01 06:54:27'),
(23, 11, 'Cost - Per Week', 'decimal', '0', '175.00', '2020-06-01 06:55:26', '2020-06-01 06:55:26'),
(24, 11, 'Amount / Qty ( Week )', 'integer', '1', NULL, '2020-06-01 06:55:26', '2020-06-01 06:55:26'),
(25, 12, 'Service Date', 'datetime', '1', NULL, '2020-06-01 14:59:02', '2020-06-02 08:15:49'),
(26, 13, 'Cost Per hour', 'decimal', '0', '45', '2020-06-03 12:58:31', '2020-06-09 14:12:50'),
(27, 13, 'Total Hours', 'integer', '1', NULL, '2020-06-09 14:12:50', '2020-06-09 14:12:50');

-- --------------------------------------------------------

--
-- Table structure for table `variable_section`
--

CREATE TABLE `variable_section` (
  `id` int(11) NOT NULL,
  `type` varchar(191) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `var_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `variable_section`
--

INSERT INTO `variable_section` (`id`, `type`, `parent_id`, `var_id`, `created_at`, `updated_at`) VALUES
(1, 'inventory', 10, 1, '2020-05-27 01:51:49', '2020-07-27 04:45:37'),
(10, 'service', 4, 3, '2020-05-27 13:19:14', '2020-05-27 13:19:14'),
(5, 'service', 16, 3, '2020-05-27 07:56:51', '2020-05-27 07:59:10'),
(7, 'service', 16, 3, '2020-05-27 07:59:10', '2020-05-27 07:59:10'),
(8, 'inventory', 25, 2, '2020-05-27 09:07:23', '2020-05-27 09:10:39'),
(11, 'inventory', 13, 2, '2020-06-01 10:49:33', '2020-06-01 10:49:33'),
(12, 'inventory', 10, 3, '2020-06-01 15:56:31', '2020-07-27 04:45:37'),
(13, 'inventory', 10, 4, '2020-06-03 12:52:11', '2020-07-27 04:45:37'),
(14, 'inventory', 10, 13, '2020-06-03 12:59:56', '2020-07-27 04:45:38'),
(15, 'inventory', 10, 12, '2020-06-09 14:15:30', '2020-07-27 04:45:38'),
(16, 'inventory', 146, 1, '2020-07-26 23:57:15', '2020-07-27 03:10:58'),
(17, 'inventory', 1, 1, '2020-08-13 01:25:55', '2020-09-02 05:03:47'),
(18, 'inventory', 1, 2, '2020-08-13 01:25:55', '2020-09-02 05:03:47'),
(19, 'inventory', 2, 2, '2020-09-01 06:51:11', '2020-09-02 05:18:23'),
(20, 'inventory', 2, 6, '2020-09-01 06:51:11', '2020-09-02 05:18:23'),
(22, 'inventory', 1, 4, '2020-09-02 05:03:47', '2020-09-02 05:03:47');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `companyName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `displayName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `givenName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middleName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `familyName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suffix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primaryPhone_freeFormNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primaryEmailAddr_addess` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alternatePhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_line1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_line2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_countrySubDivisionCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billAddr_postalCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taxIdentifier` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accountNum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `webAddr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reg_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quickbooks_id` int(11) DEFAULT NULL,
  `syncToken` int(11) DEFAULT NULL,
  `quickbooks` tinyint(4) NOT NULL DEFAULT 0,
  `manual` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `temp` tinyint(4) NOT NULL DEFAULT 0,
  `updated` tinyint(4) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_resources`
--

CREATE TABLE `vendor_resources` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `resource_id` int(11) DEFAULT NULL,
  `resource_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resource_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resource_model_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_url`
--
ALTER TABLE `app_url`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_application`
--
ALTER TABLE `credit_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_application_history`
--
ALTER TABLE `credit_application_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_configuration`
--
ALTER TABLE `email_configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_driver_request`
--
ALTER TABLE `insurance_driver_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_equipment_request`
--
ALTER TABLE `insurance_equipment_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_vehicle_request`
--
ALTER TABLE `insurance_vehicle_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventories`
--
ALTER TABLE `inventories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventories_cat_id_foreign` (`cat_id`),
  ADD KEY `inventories_subcat_id_foreign` (`subcat_id`);

--
-- Indexes for table `inventories_old`
--
ALTER TABLE `inventories_old`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventories_cat_id_foreign` (`cat_id`),
  ADD KEY `inventories_subcat_id_foreign` (`subcat_id`);

--
-- Indexes for table `inventory_unit_details`
--
ALTER TABLE `inventory_unit_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventories_subcat_id_foreign` (`inventory_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `open_invoice_settings`
--
ALTER TABLE `open_invoice_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner_agreement`
--
ALTER TABLE `owner_agreement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_inventories`
--
ALTER TABLE `product_inventories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quickbooks_settings`
--
ALTER TABLE `quickbooks_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quote_item`
--
ALTER TABLE `quote_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quote_item_details`
--
ALTER TABLE `quote_item_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quote_variable`
--
ALTER TABLE `quote_variable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quote_var_parameters`
--
ALTER TABLE `quote_var_parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rental_agreement`
--
ALTER TABLE `rental_agreement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resource_parameters`
--
ALTER TABLE `resource_parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_location_address`
--
ALTER TABLE `service_location_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcontractor_agreement`
--
ALTER TABLE `subcontractor_agreement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vacuum_checklist`
--
ALTER TABLE `vacuum_checklist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variables`
--
ALTER TABLE `variables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variables_old`
--
ALTER TABLE `variables_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variable_parameters`
--
ALTER TABLE `variable_parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variable_section`
--
ALTER TABLE `variable_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_resources`
--
ALTER TABLE `vendor_resources`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_url`
--
ALTER TABLE `app_url`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `credit_application`
--
ALTER TABLE `credit_application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `credit_application_history`
--
ALTER TABLE `credit_application_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `email_configuration`
--
ALTER TABLE `email_configuration`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=471;

--
-- AUTO_INCREMENT for table `insurance_driver_request`
--
ALTER TABLE `insurance_driver_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `insurance_equipment_request`
--
ALTER TABLE `insurance_equipment_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `insurance_vehicle_request`
--
ALTER TABLE `insurance_vehicle_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `inventories`
--
ALTER TABLE `inventories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `inventories_old`
--
ALTER TABLE `inventories_old`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT for table `inventory_unit_details`
--
ALTER TABLE `inventory_unit_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `open_invoice_settings`
--
ALTER TABLE `open_invoice_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owner_agreement`
--
ALTER TABLE `owner_agreement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `product_inventories`
--
ALTER TABLE `product_inventories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quickbooks_settings`
--
ALTER TABLE `quickbooks_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quotation`
--
ALTER TABLE `quotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quote_item`
--
ALTER TABLE `quote_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quote_item_details`
--
ALTER TABLE `quote_item_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quote_variable`
--
ALTER TABLE `quote_variable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `quote_var_parameters`
--
ALTER TABLE `quote_var_parameters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `rental_agreement`
--
ALTER TABLE `rental_agreement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `resource_parameters`
--
ALTER TABLE `resource_parameters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `service_location_address`
--
ALTER TABLE `service_location_address`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=264;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subcontractor_agreement`
--
ALTER TABLE `subcontractor_agreement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `vacuum_checklist`
--
ALTER TABLE `vacuum_checklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `variables`
--
ALTER TABLE `variables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `variables_old`
--
ALTER TABLE `variables_old`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `variable_parameters`
--
ALTER TABLE `variable_parameters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `variable_section`
--
ALTER TABLE `variable_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendor_resources`
--
ALTER TABLE `vendor_resources`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
