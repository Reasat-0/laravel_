<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class UserController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

	public function index(){
	    $users = User::all();
        return view('admin.user.index', compact('users'));
	}
    

    public function create(){
    	return view('admin.user.create');
    }

    
    public function store(Request $request){

    	$request->validate([
            'name' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required',
        ]);

    	$user = new User([
    		'name' => $request->name,
    		'email' => $request->email,
    		'password' => bcrypt($request->password),
    		'phone_no' => $request->phone_no,
    		'status' => $request->status == 'on' ? 'active' : 'inactive',
    		'role' => $request->role == 'on' ? 'admin' : 'agent',
		]);
 
        $user->save();

        session()->flash('success', 'Successfully added');

        return back();

    }

    
    public function show(){
    	//
    }

    
    public function edit($name){
        //$user = User::find($id);
        $user = User::where('name',$name)->first();
        return view('admin.user.edit', compact('user'));
    }

    
     public function update(Request $request, $id){
     	$user = User::find($id);

        $user->phone_no = $request->phone_no;
        $user->status = $request->status == 'on' ? 'active' : 'inactive';
        $user->role = $request->role == 'on' ? 'admin' : 'agent';

        if($request->password){
            $user->password = bcrypt($request->password);
        }

        $user->update();

        session()->flash('success', 'Successfully updated');
        return redirect()->route('users.index');
    }

    
     public function destroy(User $user){
     	$user->delete();
        session()->flash('success', 'Successfully deleted');
        return back();
    }


    public function myProfile(){

        $user = auth()->user();
        return view('admin.user.myProfile', compact('user'));
    }


    public function myProfileUpdate(Request $request){

        $user = auth()->user();
        
        $myProfile = User::where('id', $user->id)->first();

        $myProfile->phone_no = $request->phone_no;
        $myProfile->status = $request->status == 'on' ? 'active' : 'inactive';
        $myProfile->role = $request->role == 'on' ? 'admin' : 'agent';

        if($request->password){
            $myProfile->password = bcrypt($request->password);
        }

        $myProfile->update();

        session()->flash('success', 'You have successfully updated your profile');

        return back();
    }


    public function make_admin($id){
        $user = User::find($id);

        $user->role = 'admin';
        $user->save();

        session()->flash('success', 'Successfully changed the role');
        return redirect()->back();
    }


    public function make_agent($id){
        $user = User::find($id);

        $user->role = 'agent';
        $user->save();

        session()->flash('success', 'Successfully changed the role');
        return redirect()->back();
    }


}
