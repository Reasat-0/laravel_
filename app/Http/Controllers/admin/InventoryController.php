<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Subcategory;
use App\Models\ResourceParameter;
use App\Models\Inventory;
use App\Models\InventoryUnit;
use App\Models\Variable;
use App\Models\VariableParameter;
use App\Models\VariableSection;
use App\Models\Image;
use Validator;

class InventoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	public function index(){
        $cat = Category::where('type', 'inventory')->get();
        $subcat = Subcategory::with(['category'])->where('type', 'inventory')->get();
        $variables = Variable::where('type', 'inventory')->get();
        $inventory = Inventory::with(['category', 'subcategory'])->orderBy('id', 'asc')->groupBy('subcat_id')->get();

        if(request()->ajax()){
            if($_GET['type'] == 'cat'){
                $cat = Category::with(['subcat', 'subcat.resources'])->where('type', 'inventory')->orderByRaw(" IF(category_name RLIKE '^[a-z]', 1, 2), category_name ");;
                return datatables()->of($cat)
                        ->addColumn('action', function($data){
                            $button = '<form action="' .route("inventory.destroy",[$data->id, "type" => "cat"]). '" method="POST">' ;
                            $button .= '<a href="' .route('inventory.edit', [$data->id, 'type' => 'cat']). '" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a>';       
                            $button .= '<input name="_token" type="hidden" value="' .csrf_token(). '">';
                            $button .= '<input type="hidden" name="_method" value="DELETE">';
                            $button .= '<button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm(\'Are you sure you want to delete?\');"><i class="material-icons">close</i></button>';
                            $button .= '</form>';
                            
                            return $button;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
            }
            else if($_GET['type'] == 'subcat'){
                $subcat = Subcategory::with(['category', 'resources'])->where('type', 'inventory')->orderByRaw(" IF(subcategory_name RLIKE '^[a-z]', 1, 2), subcategory_name ");
                return datatables()->of($subcat)
                        ->addColumn('action', function($data){
                            $button = '<form action="' .route("inventory.destroy",[$data->id, "type" => "subcat"]). '" method="POST">' ; 
                             $button .= '<a href="' .route('inventory.edit', [$data->id, 'type' => 'subcat']). '" class="btn btn-link btn-success btn-just-icon edit"><i class="material-icons">edit</i></a>';      
                            $button .= '<input name="_token" type="hidden" value="' .csrf_token(). '">';
                            $button .= '<input type="hidden" name="_method" value="DELETE">';
                            $button .= '<button type="submit" class="btn btn-link btn-danger btn-just-icon remove" onclick="return confirm(\'Are you sure you want to delete?\');"><i class="material-icons">close</i></button>';
                            $button .= '</form>';
                            
                            return $button;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
            }
        }

        return view('admin.inventory.index', compact('cat', 'subcat', 'variables', 'inventory'));
	}


    public function create(){

        $cat = Category::where('type', 'inventory')->get();
        $subcat = Subcategory::where('type', 'inventory')->get();
        $variables = Variable::where('type', 'inventory')->where('enable_disable', 1)->get();

        return view('admin.inventory.create', compact('cat', 'subcat', 'variables'));
    }
    

    public function store(Request $request){

        if($request->tab == 'category')
            return $this->category($request, 'store');
        elseif($request->tab == 'subcategory'){
            return $this->subCategory($request, 'store');
        }
        elseif($request->tab == 'variable')
            return $this->variable($request, 'store');
        elseif($request->tab == 'inventory')
            return $this->inventory($request, 'store');
        

    }

    public function category($request, $type){
        $validator = Validator::make($request->all(), [
            'category_name' => 'required|unique:categories',
        ]);

        if($type == 'store'){
            if($validator->fails()){
                return back()->withErrors($validator)->withInput(['tab'=>'profile']);
            }
            else{
                Category::create($request->except(['tab']));
                session()->flash('success', 'Successfully added category');  
                return back()->withInput(['tab'=>'profile']);  
            }
                
        }
        elseif($type == 'update'){
            Category::whereId($request->p_key)->update($request->except(['tab', 'p_key', '_token', '_method']));
            session()->flash('success', 'Successfully updated category');    
            return back();
        }

        
    }

    public function subcategory($request, $type){
        $validator = Validator::make($request->all(), [
            'cat_id' => 'required',
            'subcategory_name' => 'required|unique:subcategories',
        ]);

        if($type == 'store'){
            if($validator->fails()){
                    return back()->withErrors($validator)->withInput(['tab'=>'messages']);
            }
            else{
                $subcat = Subcategory::create([
                    "cat_id" => $request->cat_id,
                    "subcategory_name" => $request->subcategory_name,
                    "type" => 'inventory',
                ]);

                $params = [];
                $unit_param_name = $request->unit_param_name;
                foreach($unit_param_name as $key=>$value){
                    $params[] = [
                        'subcat_id' => $subcat->id,
                        'unit_param_name' => $request->unit_param_name[$key],
                        'unit_param_data_type' => $request->unit_param_data_type[$key],
                        "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                        "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                    
                    ];
                }
                ResourceParameter::insert($params);

                session()->flash('success', 'Successfully added subcategory');
                return back()->withInput(['tab'=>'messages']);
            }
    
        }
        elseif($type == 'update'){
           Subcategory::whereId($request->p_key)->update([
                "cat_id" => $request->cat_id,
                "subcategory_name" => $request->subcategory_name,
                "type" => 'inventory',
           ]);

            $unit_param_key = $request->unit_param_key;
            $unit_param_rows = ResourceParameter::where('subcat_id', $request->p_key)->get();
            if($unit_param_key){
                foreach ($unit_param_rows as $rowdata) {
                       if(!in_array($rowdata->id, $unit_param_key)){
                          ResourceParameter::where('id', $rowdata->id)->delete();  
                       }
                }
            }
            else{
                foreach ($unit_param_rows as $rowdata) {
                    ResourceParameter::where('id', $rowdata->id)->delete();  
                }
            }

            $unit_param_name = $request->unit_param_name;
            if($unit_param_name){
                foreach($unit_param_name as $key=>$value){
                        if(isset($request->unit_param_key[$key])){
                            ResourceParameter::where('id', $request->unit_param_key[$key])->update([
                                'subcat_id' => $request->p_key,
                                'unit_param_name' => $request->unit_param_name[$key],
                                'unit_param_data_type' => $request->unit_param_data_type[$key],
                                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
                            ]);
                        }
                        else{
                            ResourceParameter::create([
                                'subcat_id' => $request->p_key,
                                'unit_param_name' => $request->unit_param_name[$key],
                                'unit_param_data_type' => $request->unit_param_data_type[$key],
                            ]);
                        }
                    }
            }

           session()->flash('success', 'Successfully updated subcategory');
           return back();   
        }
        
    }

    
    public function variable($request, $type){
        if($type == 'store'){
            $var = Variable::create([
                'type' => 'inventory',
                'variable_name' => $request->variable_name,
            ]);
            $var_param_name = $request->var_param_name;
            if($var_param_name){
                foreach ($var_param_name as $key => $value) {
                    VariableParameter::create([ 
                        'var_id' => $var->id,
                        'var_param_name' => $request->var_param_name[$key],
                        'var_param_data_type' => $request->var_param_data_type[$key],
                        'var_param_value_type' => $request->var_param_value_type[$key],
                        'var_param_value' => $request->var_param_value[$key],  
                    ]);
                }
                session()->flash('success', 'Successfully added variable');    
            }
             return back()->withInput(['tab'=>'variable']); 
        }
        elseif($type == 'update'){
            $var = Variable::where('id', $request->p_key)->update([
                'type' => 'inventory',
                'variable_name' => $request->variable_name,
            ]);

            $var_param_key = $request->var_param_key;
            $var_param_name = $request->var_param_name;
            $var_param_rows = VariableParameter::where('var_id', $request->p_key)->get();
            if($var_param_key){
                foreach ($var_param_rows as $rowdata) {
                       if(!in_array($rowdata->id, $var_param_key)){
                          VariableParameter::where('id', $rowdata->id)->delete();  
                       }
                }
            }
            else{
                foreach ($var_param_rows as $rowdata) {
                    VariableParameter::where('id', $rowdata->id)->delete();  
                }
            }
            
            if($var_param_name){
                foreach ($var_param_name as $key => $value) {
                    if(isset($request->var_param_key[$key])){
                        VariableParameter::where('id', $request->var_param_key[$key])->update([
                            'var_id' => $request->p_key,
                            'var_param_name' => $request->var_param_name[$key],
                            'var_param_data_type' => $request->var_param_data_type[$key],
                            'var_param_value_type' => $request->var_param_value_type[$key],
                            'var_param_value' => $request->var_param_value[$key],  
                        ]);
                    }
                    else{
                      VariableParameter::create([
                            'var_id' => $request->p_key,
                            'var_param_name' => $request->var_param_name[$key],
                            'var_param_data_type' => $request->var_param_data_type[$key],
                            'var_param_value_type' => $request->var_param_value_type[$key],
                            'var_param_value' => $request->var_param_value[$key],  
                        ]);  
                    }
                }
            }
                session()->flash('success', 'Successfully updated variable');   

                return back();
        }
    }


    public function inventory($request, $type){
        $validator = Validator::make($request->all(), [
            'cat_id' => 'required',
            'subcat_id' => 'required',
            'serial_name' => 'required',
            //'upc_vin' => 'required',
            'total_unit' => 'required',
            'available_unit' => 'required',
            'allocated_unit' => 'required',
            'resource_name' => 'required',
            'resource_code' => 'required',
            'model_num' => 'required',
        ]);

        if($type == 'store'){
            if($validator->fails()){
                return back()->withErrors($validator)->withInput(['tab'=>'settings']);
            }
            else{
            $row = Inventory::where('subcat_id', $request->subcat_id)->first();

        if(!$row){
            $inventory =  Inventory::create([
                'cat_id'=> $request->cat_id,
                'subcat_id'=> $request->subcat_id,
                'total_unit'=> $request->total_unit,
                'available_unit'=> $request->available_unit,
                'allocated_unit'=> $request->allocated_unit,
                'resource_name'=> $request->resource_name,
                'resource_code'=> $request->resource_code,
                'model_num'=> $request->model_num,
                'web_short_des'=> $request->web_short_des,
                'web_long_des'=> $request->web_long_des,
                'price_per_hour_service'=> $request->price_per_hour_service,
                'price_per_day_service'=> $request->price_per_day_service,
                'price_per_week_service'=> $request->price_per_week_service,
                'price_per_month_service'=> $request->price_per_month_service,
                'price_per_hour_rental'=> $request->price_per_hour_rental,
                'price_per_day_rental'=> $request->price_per_day_rental,
                'price_per_week_rental'=> $request->price_per_week_rental,
                'price_per_month_rental'=> $request->price_per_month_rental,
            ]);

            if($request->images){
            $images = $request->images;
            $all_image = [];
            $i = 0;
            foreach($images as $rowdata){
                $i++;
                //$name = preg_replace('/\s+/', '',$rowdata->getClientOriginalName());
                //$original_name = preg_replace('/\(([^()]*+|(?R))*\)\s*/', '', $name);
                $original_name = $rowdata->getClientOriginalName();
                $disk_image_name = time() . $i .$original_name;  
                $rowdata->move(storage_path('app/public/images'), $disk_image_name); 

                $all_image[] = [
                    'type' => 'inventory',
                    'parent_id' => $inventory->id,
                    'name' => $original_name,
                    'url' => url('/') . '/storage/images/' . $disk_image_name,
                ]; 
            }
            Image::insert($all_image);
            }

        }
        else{
            $inventory =  Inventory::where('id', $row->id)->update([
                'cat_id'=> $request->cat_id,
                'subcat_id'=> $request->subcat_id,
                'total_unit'=> $request->total_unit,
                'available_unit'=> $request->available_unit,
                'allocated_unit'=> $request->allocated_unit,
                'resource_name'=> $request->resource_name,
                'resource_code'=> $request->resource_code,
                'model_num'=> $request->model_num,
                'web_short_des'=> $request->web_short_des,
                'web_long_des'=> $request->web_long_des,
                'price_per_hour_service'=> $request->price_per_hour_service,
                'price_per_day_service'=> $request->price_per_day_service,
                'price_per_week_service'=> $request->price_per_week_service,
                'price_per_month_service'=> $request->price_per_month_service,
                'price_per_hour_rental'=> $request->price_per_hour_rental,
                'price_per_day_rental'=> $request->price_per_day_rental,
                'price_per_week_rental'=> $request->price_per_week_rental,
                'price_per_month_rental'=> $request->price_per_month_rental,
            ]);

            if($request->images){
            $images = $request->images;
            $all_image = [];
            $i = 0;
            foreach($images as $rowdata){
                $i++;
                //$name = preg_replace('/\s+/', '',$rowdata->getClientOriginalName());
                //$original_name = preg_replace('/\(([^()]*+|(?R))*\)\s*/', '', $name);
                $original_name = $rowdata->getClientOriginalName();
                $disk_image_name = time() . $i . $original_name;  
                $rowdata->move(storage_path('app/public/images'), $disk_image_name); 

                $all_image[] = [
                    'type' => 'inventory',
                    'parent_id' => $row->id,
                    'name' => $original_name,
                    'url' => url('/') . '/storage/images/' . $disk_image_name,
                ]; 
            }
            Image::insert($all_image);
            }
        }


        ////UNIT SECTION/////    
            $inventory_unit = InventoryUnit::create([
               'inventory_id' => $row ? $row->id : $inventory->id,
               'serial_name'=> $request->serial_name, 
               //'upc_vin'=> $request->upc_vin,
               //'additional_info'=>$request->additional_info,
               'properties' => $request->properties,
            ]);

            if($request->images2){
                $images = $request->images2;
                $all_image = [];
                $i = 0;
                foreach($images as $rowdata){
                    $i++;
                    //$name = preg_replace('/\s+/', '',$rowdata->getClientOriginalName());
                    //$original_name = preg_replace('/\(([^()]*+|(?R))*\)\s*/', '', $name);
                    $original_name = $rowdata->getClientOriginalName();
                    $disk_image_name = time() . $i . $original_name;  
                    $rowdata->move(storage_path('app/public/images'), $disk_image_name); 

                    $all_image[] = [
                        'type' => 'inventory_unit',
                        'parent_id' => $inventory_unit->id,
                        'name' => $original_name,
                        'url' => url('/') . '/storage/images/' . $disk_image_name,
                    ]; 
                }
                Image::insert($all_image);
            }
        ////UNIT SECTION/////

        ////VARIABLE SECTION/////
        $var_id = $request->var_id;
        if($var_id){
            foreach ($var_id as $key => $value) {
                VariableSection::create([
                    'type'=> 'inventory',
                    'parent_id'=> $row ? $row->id : $inventory->id,
                    'var_id'=> $request->var_id[$key],
                    //'var_type'=> $request->var_type[$key],
                    //'enable_disable'=> $request->enable_disable[$key], 
                ]);
            }
        }
        ////VARIABLE SECTION/////

        session()->flash('success', 'Successfully added inventory');

         return back()->withInput(['tab'=>'settings']);
    }
        
        }
        elseif($type == 'update'){

        $inventory = Inventory::where('id', $request->inventory_key)->first();
        
        if($request->unit_save == null){
           Inventory::whereId($inventory->id)->update([
                'cat_id'=> $request->cat_id,
                'subcat_id'=> $request->subcat_id,
                'total_unit'=> $request->total_unit,
                'available_unit'=> $request->available_unit,
                'allocated_unit'=> $request->allocated_unit,
                'resource_name'=> $request->resource_name,
                'resource_code'=> $request->resource_code,
                'model_num'=> $request->model_num,
                'web_short_des'=> $request->web_short_des,
                'web_long_des'=> $request->web_long_des,
                'price_per_hour_service'=> $request->price_per_hour_service,
                'price_per_day_service'=> $request->price_per_day_service,
                'price_per_week_service'=> $request->price_per_week_service,
                'price_per_month_service'=> $request->price_per_month_service,
                'price_per_hour_rental'=> $request->price_per_hour_rental,
                'price_per_day_rental'=> $request->price_per_day_rental,
                'price_per_week_rental'=> $request->price_per_week_rental,
                'price_per_month_rental'=> $request->price_per_month_rental,
           ]);

            $img_key = $request->preloaded;
            $img_rows = Image::where('type', 'inventory')->where('parent_id', $inventory->id)->get();
            if($img_key){
                foreach ($img_rows as $rowdata) {
                       if(!in_array($rowdata->id, $img_key)){
                          Image::where('id', $rowdata->id)->delete();  
                       }
                }
            }
            else{
                foreach ($img_rows as $rowdata) {
                        Image::where('id', $rowdata->id)->delete();  
                }
            }
            if($request->images){
                $images = $request->images;
                $all_image = [];
                $i = 0;
                foreach($images as $rowdata){
                    $i++;
                    //$name = preg_replace('/\s+/', '',$rowdata->getClientOriginalName());
                    //$original_name = preg_replace('/\(([^()]*+|(?R))*\)\s*/', '', $name);
                    $original_name = $rowdata->getClientOriginalName();
                    $disk_image_name = time() . $i . $original_name;  
                    $rowdata->move(storage_path('app/public/images'), $disk_image_name); 

                    $all_image[] = [
                        'type' => 'inventory',
                        'parent_id' => $inventory->id,
                        'name' => $original_name,
                        'url' => url('/') . '/storage/images/' . $disk_image_name,
                    ]; 
                }
                Image::insert($all_image);
            }

                            ////VARIABLE SECTION/////
                    $var_section_key = $request->var_section_key;
                    $var_id = $request->var_id;
                    $var_rows = VariableSection::where('type', 'inventory')->where('parent_id', $inventory->id)->get();
                    //dd($var_section_key);
                    if($var_section_key){
                        foreach ($var_rows as $rowdata) {
                               if(!in_array($rowdata->id, $var_section_key)){
                                  VariableSection::where('id', $rowdata->id)->delete();  
                               }
                        }
                    }
                    else{
                        foreach ($var_rows as $rowdata) {
                            VariableSection::where('id', $rowdata->id)->delete();
                        }   
                    }
                    if($var_id){
                        foreach ($var_id as $key => $value) {
                            if(isset($request->var_section_key[$key])){
                                //dd($request->enable_disable[2]);
                                VariableSection::where('id', $request->var_section_key[$key])->update([
                                    'type'=> 'inventory',
                                    'parent_id'=> $inventory->id,
                                    'var_id'=> $request->var_id[$key],
                                ]);
                            }
                            else{
                              VariableSection::create([
                                    'type'=> 'inventory',
                                    'parent_id'=> $inventory->id,
                                    'var_id'=> $request->var_id[$key], 
                                ]);  
                            }
                        }
                    } 
                /////VARIABLE SECTION///// 
            session()->flash('success', 'Successfully updated resources'); 
        }

        else{
            ////UNIT SECTION/////
            if($request->inventory_unit_key == 0){
            	$new_inventory_unit = InventoryUnit::create([
                   'inventory_id' => $inventory->id,
                   'serial_name'=> $request->serial_name, 
                   //'upc_vin'=> $request->upc_vin,
                   //'additional_info'=>$request->additional_info,
                   'properties'=> $request->properties,
                ]);
            }
            else{
                $data = json_encode($request->properties);
               InventoryUnit::whereId($request->inventory_unit_key)->update([
                    'serial_name'=> $request->serial_name,
                    //'upc_vin'=> $request->upc_vin,
                    //'additional_info'=>$request->additional_info
                    'properties'=> $data,
               ]);
            }

            $unit_img_key = $request->preloaded2;
            $img_rows = Image::where('type', 'inventory_unit')->where('parent_id', $request->inventory_unit_key)->get();
            if($unit_img_key){
                foreach ($img_rows as $rowdata) {
                       if(!in_array($rowdata->id, $unit_img_key)){
                          Image::where('id', $rowdata->id)->delete();  
                       }
                }
            }
            else{
                foreach ($img_rows as $rowdata) {
                          Image::where('id', $rowdata->id)->delete();  
                }
            }
            if($request->images2){
                $images = $request->images2;
                $all_image = [];
                $i = 0;
                foreach($images as $rowdata){
                    $i++;
                    //$name = preg_replace('/\s+/', '',$rowdata->getClientOriginalName());
                    //$original_name = preg_replace('/\(([^()]*+|(?R))*\)\s*/', '', $name);
                    $original_name = $rowdata->getClientOriginalName();
                    $disk_image_name = time() . $i . $original_name;  
                    $rowdata->move(storage_path('app/public/images'), $disk_image_name); 

                    $all_image[] = [
                        'type' => 'inventory_unit',
                        'parent_id' => $request->inventory_unit_key == 0 ? $new_inventory_unit->id : $request->inventory_unit_key,
                        'name' => $original_name,
                        'url' => url('/') . '/storage/images/' . $disk_image_name,
                    ]; 
                }
                Image::insert($all_image);
            }
            session()->flash('success', 'Successfully updated Unit'); 
        }
        ////UNIT SECTION///// 

          
        return back();

        /*$previousUrl = strtok(url()->previous(), '?');
        return redirect()->to(
		    $previousUrl . '?' . http_build_query(['type' => 'inventory', 'unit'=>$request->inventory_unit_key == 0 ? $new_inventory_unit->id : $request->inventory_unit_key,])
		);*/
        
        }

    }


    public function getSubcategory(Request $request){
         $value = $request->get('value');
         $type = $request->get('type');

         $data = Subcategory::where('cat_id', $value)->where('type', $type)->get();
         
         $output = '';

         if(!$data->isEmpty()){
            $output .= '<option class="form-select-placeholder"></option>';
            foreach($data as $row)
             {
              $output .= '<option value="'.$row->id.'">'.$row->subcategory_name.'</option>';
             }
        }
        else
            $output .= '<option value="" disabled>No Subcategory</option>';

         echo $output;
    }

    public function edit($id){
        if($_GET['type'] == 'cat'){
            $cat = Category::find($id);
            return view('admin.inventory.categoryEdit', compact('cat'));
        }

        elseif($_GET['type'] == 'subcat'){
            $subcat = Subcategory::find($id);
            $cat = Category::where('type', 'inventory')->get();

            $unit_params = ResourceParameter::where('subcat_id', $subcat->id)->get();
            return view('admin.inventory.subcategoryEdit', compact('subcat', 'cat', 'unit_params'));
        }

        elseif($_GET['type'] == 'variable'){
            $variable = Variable::find($id);
            $var_params = VariableParameter::where('var_id', $id)->orderBy('id', 'asc')->get();
            return view('admin.inventory.variableEdit', compact('variable', 'var_params'));
        }

        elseif($_GET['type'] == 'inventory'){
			$inventory = Inventory::find($id);
			$inventory_unit = InventoryUnit::find($_GET['unit']); 

			$total_unit = InventoryUnit::where('inventory_id', $inventory->id)->get();

            $unit_params = ResourceParameter::where('subcat_id', $inventory->subcat_id)->orderBy('id', 'asc')->get();

			if($inventory_unit){
				foreach($total_unit as $key => $value){
					if($value->id == $inventory_unit->id){
						$sr_no = $key;
					}
				}
			}

			$sr_no = $inventory_unit ? $sr_no + 1 : $total_unit->count()+1;

            $cat = Category::where('type', 'inventory')->get();
            $subcat = Subcategory::where('type', 'inventory')->get();
            $variables = Variable::where('type', 'inventory')->where('enable_disable', 1)->get();
            $variable_section = VariableSection::where('type', 'inventory')->where('parent_id', $inventory->id)->get();
            $prefix = Inventory::where('subcat_id', $inventory->subcat_id)->count();
            $images = Image::where('type', 'inventory')->where('parent_id', $inventory->id)->get();
            $unit_images = Image::where('type', 'inventory_unit')->where('parent_id', $inventory->id)->get();

            return view('admin.inventory.inventoryEdit', compact('inventory_unit','inventory', 'cat', 'subcat', 'variables', 'variable_section', 'prefix', 'images', 'unit_images', 'total_unit', 'sr_no', 'unit_params'));
        }

    }

     public function update(Request $request, $id){
        if($request->tab == 'category'){
            $this->category($request, 'update');
            return back();
        }

        elseif($request->tab == 'subcategory'){
            $this->subcategory($request, 'update');
        	return back();
        }

        elseif($request->tab == 'variable'){
            $this->variable($request, 'update');
        	return back();
        }

        elseif($request->tab == 'inventory'){
            return $this->inventory($request, 'update');
        }

    }


    public function destroy($id){
        if($_GET['type'] == 'cat'){
            $cat = Category::find($id);

            $inventory = Inventory::where('cat_id', $id)->get();
            foreach ($inventory as $rowdata) {
                InventoryUnit::where('inventory_id', $rowdata->id)->delete();
            }

            Inventory::where('cat_id', $id)->delete();
            
            Subcategory::where('cat_id', $id)->delete();
            
            $cat->delete();
            
            session()->flash('success', 'Successfully deleted category');
            return back()->withInput(['tab'=>'category_management']);
        }
        elseif($_GET['type'] == 'subcat'){
            $subcat = Subcategory::find($id);
            
            ResourceParameter::where('subcat_id', $subcat->id)->delete();
            
            $inventory = Inventory::where('subcat_id', $id)->get();
            foreach ($inventory as $rowdata) {
                InventoryUnit::where('inventory_id', $rowdata->id)->delete();
            }

            Inventory::where('subcat_id', $id)->delete();
            
            $subcat->delete();
            
            session()->flash('success', 'Successfully deleted subcategory');
            return back()->withInput(['tab'=>'sub_category_management']);
        }
        elseif($_GET['type'] == 'variable'){
            $variable = Variable::find($id);
            VariableParameter::where('var_id', $variable->id)->delete();
            VariableSection::where('var_id', $variable->id)->delete();
            Variable::where('id', $variable->id)->delete();
            session()->flash('success', 'Successfully deleted variable');
            return back()->withInput(['tab'=>'variable_management']);
        }
        elseif($_GET['type'] == 'inventory'){
            //$inventory_unit = InventoryUnit::find($id);
            //$inventory = Inventory::where('id', $inventory_unit->inventory_id)->first();
            
            $inventory_unit = InventoryUnit::find($_GET['unit']);
            $inventory = Inventory::find($id);
            
            $inventory_unit->delete();
            
            //$inventory->delete();
            
            session()->flash('success', 'Successfully deleted inventory');
            return back()->withInput(['tab'=>'resources_management']);
        }
        return back();
    }


    public function getInventoryInfo(Request $request){
        $row_data = $request->get('row_data');

        $inventory_row = Inventory::where('id', $request->get('value'))->first();

        if($row_data == 'inventory'){
            echo $inventory_row->id;
        }
    }

    public function getResourceInfoBySubcat(Request $request){
        $value = $request->get('value');

         $data = Inventory::where('subcat_id', $value)->first();
         if($data)
            $prefix = InventoryUnit::where('inventory_id', $data->id)->count();
         
         $output = '';

         if($data){
             $return_arr[] = array("resource_code" => $data->resource_code,
                    "resource_name" => $data->resource_name,
                    "total_unit" => $data->total_unit,
                    "available_unit" => $data->available_unit,
                    "allocated_unit" => $data->allocated_unit,
                    "price_per_hour_service" => $data->price_per_hour_service,
                    "price_per_day_service" => $data->price_per_day_service,
                    "price_per_week_service" => $data->price_per_week_service,
                    "price_per_month_service" => $data->price_per_month_service,
                    "price_per_hour_rental" => $data->price_per_hour_rental,
                    "price_per_day_rental" => $data->price_per_day_rental,
                    "price_per_week_rental" => $data->price_per_week_rental,
                    "price_per_month_rental" => $data->price_per_month_rental,
                    "model_num" => $data->model_num,
                    "web_short_des" => $data->web_short_des,
                    "web_long_des" => $data->web_long_des,
                    "common_image" => $data->common_image,
                    "inventory_id" => $data->id,
                    "prefix" => $prefix,
        );

        }
        else{
           $return_arr[] = array("resource_code" => null,
                    "resource_name" => null,  
                    "total_unit" => null,
                    "available_unit" => null,
                    "allocated_unit" => null,
                    "price_per_hour_service" => null,
                    "price_per_day_service" => null,
                    "price_per_week_service" => null,
                    "price_per_month_service" => null,
                    "price_per_hour_rental" => null,
                    "price_per_day_rental" => null,
                    "price_per_week_rental" => null,
                    "price_per_month_rental" => null,
                    "model_num" => null,
                    "web_short_des" => null,
                    "web_long_des" => null,
                    "inventory_id" => null,  
                    "prefix" => null,  
                    );
        }

        echo json_encode($return_arr);

    }


    public function getVariablesBySubcat(Request $request){
         $value = $request->get('value');

         $data = Inventory::where('subcat_id', $value)->first();

         $tbl= '';

         if($data){
            $variables = VariableSection::where('type', 'inventory')->where('parent_id', $data->id)->get();
            
            foreach ($variables as $rowdata) {
                 $variable_name = Variable::where('id', $rowdata->var_id)->first();
            $tbl .= '<div class="card single_card_for_single_service variable_single_card">
                    <div class="quotation_table_main_section variable_table_main_section">
                      <div class="quotation_table_section">
                        <div class="quotation_table_row variable_table_row">
                          <div class="quote_col">
                            <p>'.$variable_name->variable_name.'</p>
                          </div>
                          <div class="quote_col single_resource_main">';
                        $var_params = VariableParameter::where('var_id', $rowdata->var_id)->get();
                        foreach($var_params as $rowdata){
                        if($rowdata->var_param_value_type == 0){
                            $value_type = 'PreDefined';
                        }
                        else{
                            $value_type = 'UserDefined';
                        }
            $tbl .=             '<div class="all_single_resounce">
                                  <div class="single_resource_col price_select_wrapper">
                                    <p>'.$rowdata->var_param_name.'</p>
                                  </div>
                                  <div class="single_resource_col">
                                    <p> '.$rowdata->var_param_data_type.' </p>
                                  </div>
                                  <div class="single_resource_col">
                                    <p>' .$value_type. '</p>
                                  </div>
                                  <div class="single_resource_col colTotal">
                                    '.$rowdata->var_param_value.'
                                  </div>
                                </div>';
                            }                                                       
            $tbl .=                  '</div>
                            </div>
                          </div>
                        </div>
                      </div>';
            }
        }
        
        echo $tbl;

    }


    public function getImagesBySubcat(Request $request){
        $value = $request->get('value');

        $data = Inventory::where('subcat_id', $value)->first();

        $tbl = '';

        if($data){
            $images = Image::where('type','inventory')->where('parent_id', $data->id)->get();
            
            if(!$images->isEmpty()){
            $tbl = '<div class="input-images">
                        <div class="image-uploader has-files">
                            <div class="uploaded">';
                        foreach($images as $rowdata){
            $tbl .=               '<div class="uploaded-image" data-index="0">
                                        <img src="' .$rowdata->url. '">
                                    </div>';
                        }
            $tbl .= '</div></div></div>';
            }
        }   

        
        echo $tbl;
    
    }


    public function addVariableSection(Request $request){
        $var_id = $request->var_id;

        $variable = Variable::where('id', $var_id)->first();
        $var_params = VariableParameter::where('var_id', $var_id)->get();

        $tbl = '';

        $tbl .= '<div class="card single_card_for_single_service variable_single_card">
                    <div class="quotation_table_main_section variable_table_main_section">
                      <div class="quotation_table_section">
                        <div class="quotation_table_row variable_table_row">
                          <div class="quote_col">
                            <input type="hidden" name="var_id[]" value="'.$var_id.'">
                            <p>'.$variable->variable_name.'</p>
                          </div>
                          <div class="quote_col single_resource_main">';
                        foreach($var_params as $rowdata){
                        if($rowdata->var_param_value_type == 0){
                            $value_type = 'PreDefined';
                        }
                        else{
                            $value_type = 'UserDefined';
                        }
        $tbl .=             '<div class="all_single_resounce">
                              <div class="single_resource_col price_select_wrapper">
                                <p>'.$rowdata->var_param_name.'</p>
                              </div>
                              <div class="single_resource_col">
                                <p> '.$rowdata->var_param_data_type.' </p>
                              </div>
                              <div class="single_resource_col">
                                <p>' .$value_type. '</p>
                              </div>
                              <div class="single_resource_col colTotal">
                                '.$rowdata->var_param_value.'
                              </div>
                            </div>';
                        }                                                       
        $tbl .=                  '</div>
                        </div>
                      </div>
                    </div>
                    <div class="cross">
                      <a href="#" class="btn btn-danger btn-round var_remove_btn">
                        <i class="material-icons">close</i>
                      </a>
                    </div>
                  </div>';

        echo $tbl;
    }


    public function getUnitDetailsBySubcat(Request $request){
        $unit_params = ResourceParameter::where('subcat_id', $request->value)->orderBy('id', 'asc')->get();
        $div = '';
        $div .= '<div class="col-md-6 serial_name_wrapper input_wrapper">
                                  <div class="form-group bmd-form-group">
                                            <label for="inventory_name" class="bmd-label-floating input_label">Serial Name</label>
                                            <span class="serial_no_badge" style="display: none;">0</span>
                                            <span class="serial_no_badge_postfix" style="display: none;"></span>
                                            <span class="serial_no_badge_hyphen" style="display: none;"> - </span>
                                            <input type="text" class="form-control form_input_field serial_no_field" id="inventory_name" name="serial_name">
                                  </div>
                              </div>';
        if(!$unit_params->isEmpty()){
            $i=0;
            foreach ($unit_params as $rowdata) {
                    if($rowdata->unit_param_data_type == 'upc_number'){
                        $div .= '<div class="col-md-6 upc_holder">
                                <div class="row">

                                  <div class="col-md-7">
                                    <div class="upc_div input_wrapper">
                                      <div class="form-group bmd-form-group is-filled">
                                        <label for="" class="bmd-label-floating input_label">'.$rowdata->unit_param_name.'</label>
                                        <input type="hidden" name="properties['.$i.'][key]" value="'.$rowdata->unit_param_name.'">
                                        <input type="hidden" name="properties['.$i.'][type]" value="'.$rowdata->unit_param_data_type.'">
                                        <input type="hidden" name="properties['.$i.'][unit_param_id]" value="'.$rowdata->id.'">
                                        <input type="text" class="form-control form_input_field" id="upc" name="properties['.$i.'][value]" required="true" aria-required="true">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-5">
                                    <div class="upc_btn_div input_wrapper">
                                      <div class="form-group bmd-form-group">
                                        <button type="button" class="btn random_btn upc_btn btn-round btn-sm btn-primary in" style="width: 100%">Random UPC</button>
                                      </div>
                                    </div>
                                  </div>
                                  
                                </div>
                            </div>';
                    }
                    elseif($rowdata->unit_param_data_type == 'vin_number'){
                        $div .=    '<div class="col-md-6 upc_holder">
                                        <div class="row">

                                          <div class="col-md-7">
                                            <div class="vin_div input_wrapper">
                                              <div class="form-group bmd-form-group is-filled">
                                                <label for="upc_vin" class="bmd-label-floating input_label">'.$rowdata->unit_param_name.'</label>
                                                 <input type="hidden" name="properties['.$i.'][key]" value="'.$rowdata->unit_param_name.'">
                                                <input type="hidden" name="properties['.$i.'][type]" value="'.$rowdata->unit_param_data_type.'">
                                                <input type="hidden" name="properties['.$i.'][unit_param_id]" value="'.$rowdata->id.'">
                                                <input type="text" class="form-control form_input_field" id="vin" name="properties['.$i.'][value]" required="true" aria-required="true">
                                              </div>
                                            </div>
                                          </div>
                                           <div class="col-md-5">
                                            <div class="vin_btn_div input_wrapper">
                                              <div class="form-group bmd-form-group">
                                                <button type="button" class="btn random_btn vin_btn btn-round btn-sm btn-primary" style="width: 100%">Random VIN</button>
                                              </div>
                                            </div>
                                          </div>

                                        </div>
                                    </div>';

                    }
                    elseif($rowdata->unit_param_data_type == 'string' || $rowdata->unit_param_data_type == 'integer' || $rowdata->unit_param_data_type == 'decimal' || $rowdata->unit_param_data_type == 'datetime'){
                        if($rowdata->unit_param_data_type == 'string'){
                                $type = 'text';
                                $attr = '';
                                $classname = '';
                            }
                        elseif($rowdata->unit_param_data_type == 'integer'){
                                $type = 'number';
                                $attr = '';
                                $classname = '';
                        }
                        elseif($rowdata->unit_param_data_type == 'decimal'){
                                $type = 'number';
                                $attr = 'step=0.01';
                                $classname = '';
                        }
                        elseif($rowdata->unit_param_data_type == 'datetime'){
                                $type = 'text';
                                $attr = '';
                                $classname = 'datepicker';
                        }

                        $div .= '<div class="col-md-6 serial_name_wrapper input_wrapper">
                                  <div class="form-group bmd-form-group">
                                            <label for="" class="bmd-label-floating input_label">'.$rowdata->unit_param_name.'</label>
                                            <input type="hidden" name="properties['.$i.'][key]" value="'.$rowdata->unit_param_name.'">
                                            <input type="hidden" name="properties['.$i.'][type]" value="'.$rowdata->unit_param_data_type.'">
                                            <input type="hidden" name="properties['.$i.'][unit_param_id]" value="'.$rowdata->id.'">
                                            <input type="'.$type.'" class="form-control form_input_field '.$classname.'" '.$attr.' id="" name="properties['.$i.'][value]" required="true" aria-required="true">
                                  </div>
                              </div>';
                    } 
                    elseif($rowdata->unit_param_data_type == 'checkbox'){
                        $div .= '<div class="col-md-6 input_wrapper physical-liabilty-section">
                                  <div class="form-check" style="">
                                    <h5>'.$rowdata->unit_param_name.'</h5>
                                    <label class="form-check-label">
                                     <input type="hidden" name="properties['.$i.'][key]" value="'.$rowdata->unit_param_name.'">
                                     <input type="hidden" name="properties['.$i.'][type]" value="'.$rowdata->unit_param_data_type.'">
                                     <input type="hidden" name="properties['.$i.'][unit_param_id]" value="'.$rowdata->id.'">
                                      <input class="form-check-input form_fields vehicle_liability" name="properties['.$i.'][value]" type="radio" value="yes">Yes
                                      <span class="form-check-sign">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                    <label class="form-check-label">
                                      <input class="form-check-input vehicle_liability" name="properties['.$i.'][value]" type="radio" value="no">No
                                      <span class="form-check-sign">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>
                                </div>';
                    }  

                    $i++;

            }
        }

        echo $div;

    }

    public function unitProperties(Request $request){
        $unit_p_key = $request->unit_p_key;
        $subcat_id  = $request->subcat_id;
        $unit_type  = $request->unit_type;

        $inventory_unit = InventoryUnit::where('id', $unit_p_key)->first();
        $unit_params = ResourceParameter::where('subcat_id', $subcat_id)->orderBy('id', 'asc')->get();
        $total_unit = InventoryUnit::where('inventory_id', $request->inventory_id)->get();

        $inventory = Inventory::where('id', $request->inventory_id)->first();

        if($inventory_unit){
                foreach($total_unit as $key => $value){
                    if($value->id == $inventory_unit->id){
                        $sr_no = $key;
                    }
                }
        $serial_name = $inventory_unit->serial_name;
        }
        else{
            $sr_no = count($total_unit); 
            $serial_name = '';
        }

        $card = '';

        $card .= '<div class="card unit_info_card unit_visibility_card" style="margin: 0 auto; width: 99.5%; margin-top: 5px !important; ">
                              <div class="card-body">
                                <div class="row input_row unit_details_param_section">
                                  
                                  <div class="col-md-6 input_wrapper serial_name_wrapper">
                                    <div class="form-group bmd-form-group is-filled">
                                      <label for="inventory_name" class="bmd-label-floating input_label">Serial Name</label>

                                      <span class="serial_no_badge">' .($sr_no+1). '</span>
                                      <span class="serial_no_badge_postfix">'.$inventory->resource_code.'</span>
                                      <span class="serial_no_badge_hyphen" style="display: none;"> - </span>
                                      <input type="hidden" name="inventory_unit_key" value="'.$unit_p_key.'">

                                      <input type="text" class="form-control form_input_field serial_no_field" id="inventory_name" name="serial_name" value="'.$serial_name.'">
                                    </div>                                    
                                  </div>';

                            $i=0;
                            foreach($unit_params as $rowdata){
                                if($inventory_unit){ 
                                  if($inventory_unit->properties != null){
                                    $arr_key = array_search($rowdata->id, array_column($inventory_unit->properties, 'unit_param_id'));
                                    if($arr_key !== false){
                                      $value = $inventory_unit->properties[$arr_key]['value'];
                                    }
                                    else{
                                      $value = '';
                                    }
                                  }
                                  else{
                                    $value = '';
                                  }
                                }
                                else{
                                    $value = '';
                                }

                                    if($rowdata->unit_param_data_type == 'upc_number'){
    $card .=                        '<div class="col-md-6 upc_holder">
                                        <div class="row">

                                          <div class="col-md-7">
                                            <div class="upc_div input_wrapper">
                                              <div class="form-group bmd-form-group is-filled">
                                                <label for="" class="bmd-label-floating input_label">'.$rowdata->unit_param_name.'</label>
                                                <input type="hidden" class="properties_key" name="properties['.$i.'][key]" value="'.$rowdata->unit_param_name.'">
                                                <input type="hidden" class="properties_type" name="properties['.$i.'][type]" value="'.$rowdata->unit_param_data_type.'">
                                                <input type="hidden" class="properties_unit_param_id" name="properties['.$i.'][unit_param_id]" value="'.$rowdata->id.'">
                                                <input type="text" class="form-control form_input_field unique'.$rowdata->id.'" id="upc" name="properties['.$i.'][value]" value="'.$value.'">
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-5">
                                            <div class="upc_btn_div input_wrapper">
                                              <div class="form-group bmd-form-group">
                                                <button type="button" class="btn random_btn upc_btn btn-round btn-sm btn-primary in" style="width: 100%">Random UPC</button>
                                              </div>
                                            </div>
                                          </div>
                                          
                                        </div>
                                    </div>';
                                    }
                                    
                                    else if($rowdata->unit_param_data_type == 'vin_number'){
    $card .=                          '<div class="col-md-6 upc_holder">
                                        <div class="row">

                                          <div class="col-md-7">
                                            <div class="vin_div input_wrapper">
                                              <div class="form-group bmd-form-group is-filled">
                                                <label for="upc_vin" class="bmd-label-floating input_label">'.$rowdata->unit_param_name.'</label>
                                                 <input type="hidden" class="properties_key" name="properties['.$i.'][key]" value="'.$rowdata->unit_param_name.'">
                                                <input type="hidden" class="properties_type" name="properties['.$i.'][type]" value="'.$rowdata->unit_param_data_type.'">
                                                <input type="hidden" class="properties_unit_param_id" name="properties['.$i.'][unit_param_id]" value="'.$rowdata->id.'">
                                                <input type="text" class="form-control form_input_field unique{{$rowdata->id}}" id="vin" name="properties['.$i.'][value]" value="'.$value.'">
                                              </div>
                                            </div>
                                          </div>
                                           <div class="col-md-5">
                                            <div class="vin_btn_div input_wrapper">
                                              <div class="form-group bmd-form-group">
                                                <button type="button" class="btn random_btn vin_btn btn-round btn-sm btn-primary" style="width: 100%">Random VIN</button>
                                              </div>
                                            </div>
                                          </div>

                                        </div>
                                    </div>';
                                    }

                            else if($rowdata->unit_param_data_type == 'string' || $rowdata->unit_param_data_type == 'integer' || $rowdata->unit_param_data_type == 'decimal' || $rowdata->unit_param_data_type == 'datetime'){
                                if($rowdata->unit_param_data_type == 'string'){
                                    
                                      $type = 'text';
                                      $attr = '';
                                      $classname = '';
                                }
                                else if($rowdata->unit_param_data_type == 'integer'){
                                    
                                      $type = 'number';
                                      $attr = '';
                                      $classname = '';
                                    
                                }
                                else if($rowdata->unit_param_data_type == 'decimal'){
                                      
                                      $type = 'number';
                                      $attr = 'step=0.01';
                                      $classname = '';
                                    
                                }
                                else if($rowdata->unit_param_data_type == 'datetime'){
                                      $type = 'text';
                                      $attr = '';
                                      $classname = 'datepicker';
                                    
                                }

    $card .=                    '<div class="col-md-6 serial_name_wrapper input_wrapper">
                                  <div class="form-group bmd-form-group">
                                            <label for="" class="bmd-label-floating input_label">'.$rowdata->unit_param_name.'</label>
                                            <input type="hidden" class="properties_key" name="properties['.$i.'][key]" value="'.$rowdata->unit_param_name.'">
                                            <input type="hidden" class="properties_type" name="properties['.$i.'][type]" value="'.$rowdata->unit_param_data_type.'">
                                            <input type="hidden" class="properties_unit_param_id" name="properties['.$i.'][unit_param_id]" value="'.$rowdata->id.'">
                                            <input type="'.$type.'" class="form-control form_input_field unique'.$rowdata->id.' '.$classname.'" '.$attr.' id="" name="properties['.$i.'][value]" value="'.$value.'">
                                  </div>
                                </div>';
                            }
                            else if($rowdata->unit_param_data_type == 'checkbox'){
                                if($value == 'yes')
                                    $checked = 'checked';
                                else
                                    $checked = '';
                                if($value == 'no')
                                    $no_checked = 'checked';
                                else
                                    $no_checked = '';
    $card.=                    '<div class="col-md-6 input_wrapper physical-liabilty-section">
                                  <div class="form-check" style="">
                                    <h5>'.$rowdata->unit_param_name.'</h5>
                                    <label class="form-check-label">
                                     <input type="hidden" class="properties_key" name="properties['.$i.'][key]" value="'.$rowdata->unit_param_name.'">
                                     <input type="hidden" class="properties_type" name="properties['.$i.'][type]" value="'.$rowdata->unit_param_data_type.'">
                                     <input type="hidden" class="properties_unit_param_id" name="properties['.$i.'][unit_param_id]" value="'.$rowdata->id.'">
                                      <input class="form-check-input form_fields unit_param_chk unique'.$rowdata->id.'" name="properties['.$i.'][value]" type="radio" value="yes" '.$checked.'>Yes
                                      <span class="form-check-sign">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                    <label class="form-check-label">
                                      <input class="form-check-input unit_param_chk unique'.$rowdata->id.'" name="properties['.$i.'][value]" type="radio" value="no" '.$no_checked.'>No
                                      <span class="form-check-sign">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>
                                </div>';
                            }

                            $i++;
                            }
                                  
    $card .=                '</div>
                                
                                <div class="row"  style="margin: 10px 0">
                                </div>

                                <!-- Image Row -->
                                <div class="row">
                                  <div class="col-md-12">
                                      <h4 class="title">Image</h4>
                                      <div class="input-images2 edit_section_img"></div>
                                  </div>
                                </div>

                                <div class="form-group bmd-form-group save_btn_wrapper text-right">
                                  <button type="button" class="btn btn-sm btn-round save_btn btn-danger unit_properties_close">close</button>
                                  <button type="submit" name="unit_save" value="new_or_exist" class="btn btn-round btn-sm save_btn custom-btn-one">Save Unit</button>
                                </div>

                              </div>
                            </div>';

        echo $card;

    }


    public function unitPropertiesDlt(Request $request){
        $unit_p_key = $request->unit_p_key;
        $inventory_unit = InventoryUnit::where('id', $unit_p_key)->first();
        if($inventory_unit){
            $inventory_unit->delete();
            $unit_images = Image::where('type', $inventory_unit)->where('parent_id', $inventory_unit->id)->get();
            if(!$unit_images->isEmpty()){
                Image::where('type', $inventory_unit)->where('parent_id', $inventory_unit->id)->delete();
            }            
        }

    }


    public function variableEnableDisable(Request $request){
        $var_key = $request->var_key;
        $value = $request->value == 'true' ? 1 : 0;

        Variable::where('id', $var_key)->update(['enable_disable'=>$value]);

        echo $value;

    }

    public function deleteInventoryUnit(Request $request){
    	$inventory_to_be_deleted = InventoryUnit::where('id', $request->dlt_key)->first();
    	if($inventory_to_be_deleted){
    		$inventory_to_be_deleted->delete();
			Image::where('id', $request->dlt_key)->delete();    
		}


    }

    public function test(){
    	// $unit = InventoryUnit::all();
    	// foreach ($unit as $rowdata) {
    	// 	InventoryUnit::where('id', $rowdata->id)->update([
     //            'serial_name'=> ltrim($rowdata->serial_name),
     //        ]);
    	// }

        return view('admin.inventory.new_form_design');

    }

}
