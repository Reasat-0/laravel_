<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Inventory;
use App\Models\Product;
use App\Models\ProductInventory;
use App\Models\Variable;
use App\Models\VariableParameter;
use App\Models\VariableSection;
use App\Models\Image;

use Validator;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function index(){
        $cat = Category::where('type', 'product')->get();
        $subcat = Subcategory::with(['category'])->where('type', 'product')->get();
        $variables = Variable::where('type', 'service')->get();
        $products = Product::with(['category', 'subcategory'])->orderBy('id', 'asc')->get();
        return view('admin.product.index', compact('cat', 'subcat', 'variables', 'products'));
	}
    

    public function create(){
        $cat = Category::all();
        $subcat = Subcategory::all();
        $variables = Variable::where('type', 'service')->where('enable_disable', 1)->get();
    	return view('admin.product.create', compact('cat', 'subcat', 'variables'));
    }

    
    public function store(Request $request){
        if($request->tab == 'category')
            return $this->category($request, 'store');
        elseif($request->tab == 'subcategory')
            return $this->subCategory($request, 'store');
        elseif($request->tab == 'variable')
            return $this->variable($request, 'store');
        elseif($request->tab == 'product')
            return $this->product($request, 'store');
        return back();
    }

    public function category($request, $type){
        $validator = Validator::make($request->all(), [
            'category_name' => 'required|unique:categories',
        ]);

        if($type == 'store'){
            if($validator->fails()){
                return back()->withErrors($validator)->withInput(['tab'=>'profile']);
            }
            else{
                Category::create($request->except(['tab']));
                session()->flash('success', 'Successfully added category');    
                return back()->withInput(['tab'=>'profile']);
            }
        }
        elseif($type == 'update'){
            Category::whereId($request->p_key)->update($request->except(['tab', 'p_key', '_token', '_method']));
            session()->flash('success', 'Successfully updated category'); 
            return back();   
        }
    }

    public function subcategory($request, $type){
        $validator = Validator::make($request->all(), [
            'cat_id' => 'required',
            'subcategory_name' => 'required|unique:subcategories',
        ]);

        if($type == 'store'){
            if($validator->fails()){
                return back()->withErrors($validator)->withInput(['tab'=>'messages']);
            }
            else{
                Subcategory::create($request->except(['tab']));
                session()->flash('success', 'Successfully added subcategory');    
                return back()->withInput(['tab'=>'messages']);
            }
        }
        elseif($type == 'update'){
           Subcategory::whereId($request->p_key)->update($request->except(['tab', 'p_key', '_token', '_method']));
           session()->flash('success', 'Successfully updated subcategory'); 
           return back();  
        }
    }


    public function variable($request, $type){
        if($type == 'store'){
            $var = Variable::create([
                'type' => 'service',
                'variable_name' => $request->variable_name,
            ]);
            $var_param_name = $request->var_param_name;
            if($var_param_name){
                foreach ($var_param_name as $key => $value) {
                    VariableParameter::create([ 
                        'var_id' => $var->id,
                        'var_param_name' => $request->var_param_name[$key],
                        'var_param_data_type' => $request->var_param_data_type[$key],
                        'var_param_value_type' => $request->var_param_value_type[$key],
                        'var_param_value' => $request->var_param_value[$key],  
                    ]);
                }
            }
            session()->flash('success', 'Successfully added variable');  
            return back()->withInput(['tab'=>'variable']);
        }
        elseif($type == 'update'){
            $var = Variable::where('id', $request->p_key)->update([
                'type' => 'service',
                'variable_name' => $request->variable_name,
            ]);

            $var_param_key = $request->var_param_key;
            $var_param_name = $request->var_param_name;
            $var_param_rows = VariableParameter::where('var_id', $request->p_key)->get();
            if($var_param_key){
                foreach ($var_param_rows as $rowdata) {
                       if(!in_array($rowdata->id, $var_param_key)){
                          VariableParameter::where('id', $rowdata->id)->delete();  
                       }
                }
            }
            else{
                foreach ($var_param_rows as $rowdata) {
                    VariableParameter::where('id', $rowdata->id)->delete();  
                }
            }
            
            if($var_param_name){
                foreach ($var_param_name as $key => $value) {
                    if(isset($request->var_param_key[$key])){
                        VariableParameter::where('id', $request->var_param_key[$key])->update([
                            'var_id' => $request->p_key,
                            'var_param_name' => $request->var_param_name[$key],
                            'var_param_data_type' => $request->var_param_data_type[$key],
                            'var_param_value_type' => $request->var_param_value_type[$key],
                            'var_param_value' => $request->var_param_value[$key],  
                        ]);
                    }
                    else{
                      VariableParameter::create([
                            'var_id' => $request->p_key,
                            'var_param_name' => $request->var_param_name[$key],
                            'var_param_data_type' => $request->var_param_data_type[$key],
                            'var_param_value_type' => $request->var_param_value_type[$key],
                            'var_param_value' => $request->var_param_value[$key],  
                        ]);  
                    }
                }
            }
                session()->flash('success', 'Successfully updated variable');  
                return back(); 
        }
    }


    public function product($request, $type){
        $validator = Validator::make($request->all(), [
            'cat_id' => 'required',
            'subcat_id' => 'required',
            'product_name' => 'required',
            'sku' => 'required',
            'inventory_id' => 'required',
        ],
        [
        'inventory_id.required' => 'Resource name field is required',
        ]);


        if($type == 'store'){  
            if($validator->fails()){
                return back()->withErrors($validator)->withInput(['tab'=>'settings']);
            }
            else{           
             $product = Product::create([
                'cat_id' => $request->cat_id,
                'subcat_id' => $request->subcat_id,
                'product_name' => $request->product_name,               
                'sku' => $request->sku, 
                'web_short_des' => $request->web_short_des, 
                'web_long_des' => $request->web_long_des, 
             ]);

            if($request->images){
                $images = $request->images;
                $all_image = [];
                $i = 0;
                foreach($images as $rowdata){
                    $i++;
                    //$name = preg_replace('/\s+/', '',$rowdata->getClientOriginalName());
                    //$original_name = preg_replace('/\(([^()]*+|(?R))*\)\s*/', '', $name);
                    $original_name = $rowdata->getClientOriginalName();
                    $disk_image_name = time() . $i . $original_name;  
                    $rowdata->move(storage_path('app/public/images'), $disk_image_name); 

                    $all_image[] = [
                        'type' => 'service',
                        'parent_id' => $product->id,
                        'name' => $original_name,
                        'url' => url('/') . '/storage/images/' . $disk_image_name,
                    ]; 
                }
            Image::insert($all_image);
            }



        /////RESOURCE SECTION/////
            $resources = $request->inventory_id;
            $data = [];
            if($resources){
                 foreach ($resources as $rowdata) {
                     $data[] = [
                        'product_id' => $product->id, 
                        'inventory_id' => $rowdata,
                         
                    ];         
                 }
            }
            ProductInventory::insert($data);
        /////RESOURCE SECTION/////



        /////VARIABLE SECTION/////
            $var_id = $request->var_id;
            if($var_id){
                foreach ($var_id as $key => $value) {
                    VariableSection::create([
                        'type'=> 'service',
                        'parent_id'=> $product->id,
                        'var_id'=> $request->var_id[$key],
                        //'var_type'=> $request->var_type[$key],
                        //'enable_disable'=> $request->enable_disable[$key], 
                    ]);
                }
            }
        /////VARIABLE SECTION/////


            session()->flash('success', 'Successfully added service');  
            return back()->withInput(['tab'=>'settings']); 
            }  
        }

        elseif($type == 'update'){
           $product = Product::where('id', $request->p_key)->first();

           $resources = $request->inventory_id;

           Product::where('id', $request->p_key)->update([
                'cat_id' => $request->cat_id,
                'subcat_id' => $request->subcat_id,
                'product_name' => $request->product_name,               
                'sku' => $request->sku, 
                'web_short_des' => $request->web_short_des, 
                'web_long_des' => $request->web_long_des,  
             ]);


        $img_key = $request->preloaded;
        $img_rows = Image::where('type', 'service')->where('parent_id', $product->id)->get();
        if($img_key){
            foreach ($img_rows as $rowdata) {
                   if(!in_array($rowdata->id, $img_key)){
                      Image::where('id', $rowdata->id)->delete();  
                   }
            }
        }
        else{
            foreach ($img_rows as $rowdata) {
                    Image::where('id', $rowdata->id)->delete();  
            }   
        }


        if($request->images){
            $images = $request->images;
            $all_image = [];
            $i = 0;
            foreach($images as $rowdata){
                $i++;
                //$name = preg_replace('/\s+/', '',$rowdata->getClientOriginalName());
                //$original_name = preg_replace('/\(([^()]*+|(?R))*\)\s*/', '', $name);
                $original_name = $rowdata->getClientOriginalName();
                $disk_image_name = time() . $i . $original_name;  
                $rowdata->move(storage_path('app/public/images'), $disk_image_name); 

                $all_image[] = [
                    'type' => 'service',
                    'parent_id' => $product->id,
                    'name' => $original_name,
                    'url' => url('/') . '/storage/images/' . $disk_image_name,
                ]; 
            }
            Image::insert($all_image);
        }



        ////RESOURCES SECTION/////
           $product_inventory = ProductInventory::where('product_id', $product->id)->get();
           if($resources){
               foreach ($product_inventory as $rowdata) {
                   if(!in_array($rowdata->inventory_id, $resources)){
                      ProductInventory::where('id', $rowdata->id)->delete();  
                   }
               }
            }
            else{
               foreach ($product_inventory as $rowdata) {
                    ProductInventory::where('id', $rowdata->id)->delete();  
               } 
            }

           $product_inventory_arr = ProductInventory::where('product_id', $product->id)->pluck('inventory_id')->toArray();
           $data = [];
           foreach ($resources as $rowdata) {
                if(!in_array($rowdata, $product_inventory_arr)){
                    $data[] = [
                            'product_id' => $product->id, 
                            'inventory_id' => $rowdata,
                         
                        ];   
                }
            }
            ProductInventory::insert($data); 
        ////RESOURCES SECTION/////


    ////VARIABLE SECTION/////
        $var_section_key = $request->var_section_key;
        $var_id = $request->var_id;
        $var_rows = VariableSection::where('type', 'service')->where('parent_id', $product->id)->get();
        //dd($var_section_key);
        if($var_section_key){
            foreach ($var_rows as $rowdata) {
                   if(!in_array($rowdata->id, $var_section_key)){
                      VariableSection::where('id', $rowdata->id)->delete();  
                   }
            }
        }
        else{
            foreach ($var_rows as $rowdata) {
                VariableSection::where('id', $rowdata->id)->delete();
            }   
        }
        if($var_id){
            foreach ($var_id as $key => $value) {
                if(isset($request->var_section_key[$key])){
                    //dd($request->enable_disable[2]);
                    VariableSection::where('id', $request->var_section_key[$key])->update([
                        'type'=> 'service',
                        'parent_id'=> $product->id,
                        'var_id'=> $request->var_id[$key],
                    ]);
                }
                else{
                  VariableSection::create([
                        'type'=> 'service',
                        'parent_id'=> $product->id,
                        'var_id'=> $request->var_id[$key], 
                    ]);  
                }
            }
        } 
    /////VARIABLE SECTION/////

           session()->flash('success', 'Successfully updated service'); 
           return back();  
        }
    }

    
    public function show(){
    	//
    }

    
    public function edit($id){
        if($_GET['type'] == 'cat'){
            $cat = Category::find($id);
            return view('admin.product.categoryEdit', compact('cat'));
        }

        elseif($_GET['type'] == 'subcat'){
            $subcat = Subcategory::find($id);
            $cat = Category::where('type', 'product')->get();
            return view('admin.product.subcategoryEdit', compact('subcat', 'cat'));
        }

        elseif($_GET['type'] == 'variable'){
            $variable = Variable::find($id);
            $var_params = VariableParameter::where('var_id', $id)->orderBy('id', 'asc')->get();
            return view('admin.product.variableEdit', compact('variable', 'var_params'));
        }

        elseif($_GET['type'] == 'product'){
            $product = Product::find($id);
            $cat = Category::all();
            $subcat = Subcategory::all();
            $product_inventory = ProductInventory::where('product_id', $product->id)->get();
            $variables = Variable::where('type', 'service')->where('enable_disable', 1)->get();
            $variable_section = VariableSection::where('type', 'service')->where('parent_id', $product->id)->get();
            $images = Image::where('type', 'service')->where('parent_id', $product->id)->get();
            return view('admin.product.productEdit', compact('product', 'cat', 'subcat', 'product_inventory', 'variables', 'variable_section', 'images'));
        }
    }

    
     public function update(Request $request, $id){
        if($request->tab == 'category'){
            $this->category($request, 'update');
        }

        elseif($request->tab == 'subcategory'){
            $this->subcategory($request, 'update');
        }

        elseif($request->tab == 'variable'){
            $this->variable($request, 'update');
        }

        elseif($request->tab == 'product'){
            $this->product($request, 'update');
        }

        return back();
    }

    
     public function destroy($id){
        if($_GET['type'] == 'cat'){
            $cat = Category::find($id);
            $cat->delete();
            session()->flash('success', 'Successfully deleted category');
            return back()->withInput(['tab'=>'product_category_management']);
        }
        elseif($_GET['type'] == 'subcat'){
            $subcat = Subcategory::find($id);
            $subcat->delete();
            session()->flash('success', 'Successfully deleted subcategory');
            return back()->withInput(['tab'=>'product_sub_category_management']);
        }
        elseif($_GET['type'] == 'variable'){
            $variable = Variable::find($id);
            VariableParameter::where('var_id', $variable->id)->delete();
            VariableSection::where('var_id', $variable->id)->delete();
            Variable::where('id', $variable->id)->delete();
            session()->flash('success', 'Successfully deleted variable');
            return back()->withInput(['tab'=>'variable_management']);
        }
        elseif($_GET['type'] == 'product'){
            //dd('okk');
            $product = Product::find($id);
            $product_inventory = ProductInventory::where('product_id', $id)->delete();
            $product->delete();
            session()->flash('success', 'Successfully deleted product');
            return back()->withInput(['tab'=>'product_management']);
        }
        return back();
    }





}
