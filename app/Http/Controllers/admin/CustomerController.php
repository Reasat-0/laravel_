<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use QuickBooksOnline\API\DataService\DataService;

use App\Models\Customer;
use App\Models\CreditApplication;
use App\Models\CreditApplicationHistory;
use App\Models\Image;
use App\Mail\CreditAppEmail;
use QuickBooksOnline\API\Core\OAuth\OAuth2\OAuth2LoginHelper;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;

use Mail;
use File;
use Storage;
use DB;
use App\Models\ServiceLocationAddress;

use Validator;
use PDF;
use Response;
use Carbon\Carbon;


class CustomerController extends Controller
{
    public $dataService;

    public function __construct(){
        $this->middleware('auth', ['except' => ['creditAppToken', 'creditAppCustomer', 'creditAppUpdateForCustomer', 'creditAppMessage']]);

        $this->dataService = DataService::Configure(array(
            'auth_mode' => 'oauth2',
            'ClientID' => config('quickbooks.client_id') ? config('quickbooks.client_id') : null,
            'ClientSecret' =>  config('quickbooks.client_secret'),
            'RedirectURI' => config('quickbooks.oauth_redirect_uri'),
            'scope' => config('quickbooks.oauth_scope'),
            'baseUrl' => config('quickbooks.environment')
        ));
    }

	public function index(){

        $customers_quickbooks_temp = Customer::whereNull('manual')->where('quickbooks', 1)->where('temp', 1)->orderByRaw(" IF(displayName RLIKE '^[a-z]', 1, 2), displayName ")->get();
        $customers_quickbooks = Customer::whereNull('manual')->where('quickbooks', 1)->where('temp', 0)->orderByRaw(" IF(displayName RLIKE '^[a-z]', 1, 2), displayName ")->get();
        $customers_manual = Customer::whereIn('manual', [0,1])->orderByRaw('displayName * 1', 'ASC')->get();
        return view('admin.customer.index', compact('customers_quickbooks_temp','customers_quickbooks', 'customers_manual'));
	}
    

    public function create(){
        //dd(config('quickbooks.client_id'));

        session_start();


        $OAuth2LoginHelper = $this->dataService->getOAuth2LoginHelper();
        $authUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();

        // Store the url in PHP Session Object;
        $_SESSION['authUrl'] = $authUrl;

        //set the access token using the auth object
        if (isset($_SESSION['sessionAccessToken'])) {

            $accessToken = $_SESSION['sessionAccessToken'];
            $accessTokenJson = array('token_type' => 'bearer',
                'access_token' => $accessToken->getAccessToken(),
                'refresh_token' => $accessToken->getRefreshToken(),
                'x_refresh_token_expires_in' => $accessToken->getRefreshTokenExpiresAt(),
                'expires_in' => $accessToken->getAccessTokenExpiresAt()
            );
            $this->dataService->updateOAuth2Token($accessToken);
            $oauthLoginHelper = $this->dataService->getOAuth2LoginHelper();

            return view('admin.customer.create', compact('authUrl', 'accessTokenJson'));
        }
        else{
            return view('admin.customer.create', compact('authUrl'));
        }
        
    }

    

    public function qbCallback(){
        session_start();
        
        $OAuth2LoginHelper = $this->dataService->getOAuth2LoginHelper();
        $parseUrl = $this->parseAuthRedirectUrl($_SERVER['QUERY_STRING']);

        /*
         * Update the OAuth2Token
         */
        $accessToken = $OAuth2LoginHelper->exchangeAuthorizationCodeForToken($parseUrl['code'], $parseUrl['realmId']);
        $this->dataService->updateOAuth2Token($accessToken);

        /*
         * Setting the accessToken for session variable
         */
        $_SESSION['sessionAccessToken'] = $accessToken;
        $_SESSION['realmId'] = $parseUrl['realmId'];
    }


    function parseAuthRedirectUrl($url)
    {
        parse_str($url,$qsArray);
        return array(
            'code' => $qsArray['code'],
            'realmId' => $qsArray['realmId']
        );
    }


    public function qbCustomer(){
    session_start();

    /*
     * Retrieve the accessToken value from session variable
     */
    $accessToken = $_SESSION['sessionAccessToken'];
    $realmId = $_SESSION['realmId'];

    //dd($realmId);

    if( gmdate("Y/m/d H:i:s") > $accessToken->getAccessTokenExpiresAt()){
        //$oauth2LoginHelper = new OAuth2LoginHelper(config('quickbooks.client_id'),config('quickbooks.client_secret'));
        //$refreshedAccessTokenObj = $oauth2LoginHelper->refreshAccessTokenWithRefreshToken($accessToken->getRefreshToken());

        //$_SESSION['sessionAccessToken'] = $refreshedAccessTokenObj;
        //$this->dataService->updateOAuth2Token($refreshedAccessTokenObj);
        /*$this->dataService = DataService::Configure(array(
            'auth_mode' => 'oauth2',
            'ClientID' => config('quickbooks.client_id'),
            'ClientSecret' =>  config('quickbooks.client_secret'),
            'RedirectURI' => config('quickbooks.oauth_redirect_uri'),
            'baseUrl' => "development",
            'refreshTokenKey' => $accessToken->getRefreshToken(),
            'QBORealmID' => $realmId,
        ));*/

            /*
             * Update the OAuth2Token of the dataService object
             */
            /*$OAuth2LoginHelper = $this->dataService->getOAuth2LoginHelper();
            $refreshedAccessTokenObj = $OAuth2LoginHelper->refreshToken();
            $this->dataService->updateOAuth2Token($refreshedAccessTokenObj);

            $_SESSION['sessionAccessToken'] = $refreshedAccessTokenObj;*/

            //print_r($refreshedAccessTokenObj);
            //return $refreshedAccessTokenObj;

        echo "Your access token is expired, Please click on <b>Connect to Quickbooks</b> button above";
        die();

    }

    /*
     * Update the OAuth2Token of the dataService object
     */
    else{
        $this->dataService->updateOAuth2Token($accessToken);
    }
    
    $customer = Customer::where('quickbooks', 1)->orderBy('id', 'desc')->first();
    if($customer)
        $quickbooks_id_q = $customer->quickbooks_id;
    else
        $quickbooks_id_q = 0;

    $entities = $this->dataService->Query("Select * from Customer WHERE Id >'" .$quickbooks_id_q.  "' ORDERBY Id ASC MaxResults 1000");
    //$entities = $this->dataService->Query("Select * from Customer WHERE Id = '2'");

    //print_r($entities);

    $customer_type = $this->dataService->Query("Select * from CustomerType WHERE Name = 'EVOS'");
    $customer_type_value = $customer_type ? $customer_type[0]->Id : null;

    if($entities){
        $customer_all = [];
        foreach ($entities as $rowdata) {
            if($customer_type_value != null && $rowdata->CustomerTypeRef && $rowdata->CustomerTypeRef == $customer_type_value){
                 $customer_all[] = [
                    'givenName' => isset($rowdata->GivenName) ? $rowdata->GivenName : null,
                    'middleName' => isset($rowdata->MiddleName) ? $rowdata->MiddleName : null,
                    'familyName' => isset($rowdata->FamilyName) ? $rowdata->FamilyName : null,
                    'suffix' => isset($rowdata->Suffix) ? $rowdata->Suffix : null,
                    'displayName' => isset($rowdata->DisplayName) ? $rowdata->DisplayName : null,
                    'companyName' => isset($rowdata->CompanyName) ? $rowdata->CompanyName : null,
                    'title' => isset($rowdata->Title) ? $rowdata->Title : null,
                    //'businessNumber' => isset($rowdata->BusinessNumber) ? $rowdata->BusinessNumber : null,
                    //'organization' => isset($rowdata->Organization) ? $rowdata->Organization : null,

                    //'primaryPhone_deviceType' => isset($rowdata->PrimaryPhone->DeviceType) ? $rowdata->PrimaryPhone->DeviceType : null,
                    //'primaryPhone_countryCode' => isset($rowdata->PrimaryPhone->CountryCode) ? $rowdata->PrimaryPhone->CountryCode : null,
                    //'primaryPhone_areaCode' => isset($rowdata->PrimaryPhone->AreaCode) ? $rowdata->PrimaryPhone->AreaCode : null,
                    //'primaryPhone_exchangeCode' => isset($rowdata->PrimaryPhone->ExchangeCode) ? $rowdata->PrimaryPhone->ExchangeCode : null,
                    //'primaryPhone_extension' => isset($rowdata->PrimaryPhone->Extension) ? $rowdata->PrimaryPhone->Extension : null,
                    'primaryPhone_freeFormNumber' => isset($rowdata->PrimaryPhone->FreeFormNumber) ? $rowdata->PrimaryPhone->FreeFormNumber : null,
                    'alternatePhone' => isset($rowdata->AlternatePhone->FreeFormNumber) ? $rowdata->AlternatePhone->FreeFormNumber : null,
                    'mobile' => isset($rowdata->Mobile->FreeFormNumber) ? $rowdata->Mobile->FreeFormNumber : null,
                    //'fax' => isset($rowdata->Fax->FreeFormNumber) ? $rowdata->Fax->FreeFormNumber : null,

                    'primaryEmailAddr_addess' => isset($rowdata->PrimaryEmailAddr->Address) ? $rowdata->PrimaryEmailAddr->Address : null,
                    //'primaryEmailAddr_default' => isset($rowdata->PrimaryEmailAddr->Default) ? $rowdata->PrimaryEmailAddr->Default : null,


                    'billAddr_line1' => isset($rowdata->BillAddr->Line1) ? $rowdata->BillAddr->Line1 : null,
                    'billAddr_line2' => isset($rowdata->BillAddr->Line2) ? $rowdata->BillAddr->Line2 : null,
                    'billAddr_city' => isset($rowdata->BillAddr->City) ? $rowdata->BillAddr->City : null,
                    'billAddr_country' => isset($rowdata->BillAddr->Country) ? $rowdata->BillAddr->Country : null,
                    //'billAddr_countryCode' => isset($rowdata->BillAddr->CountryCode) ? $rowdata->BillAddr->CountryCode : null,
                    'billAddr_countrySubDivisionCode' => isset($rowdata->BillAddr->CountrySubDivisionCode) ? $rowdata->BillAddr->CountrySubDivisionCode : null,
                    'billAddr_postalCode' => isset($rowdata->BillAddr->PostalCode) ? $rowdata->BillAddr->PostalCode : null,
                    //'billAddr_postalCodeSuffix' => isset($rowdata->BillAddr->PostalCodeSuffix) ? $rowdata->BillAddr->PostalCodeSuffix : null,
                    //'billAddr_lat' => isset($rowdata->BillAddr->Lat) ? $rowdata->BillAddr->Lat : null,
                    //'billAddr_long' => isset($rowdata->BillAddr->Long) ? $rowdata->BillAddr->Long : null,

                    'shipAddr_line1' => isset($rowdata->ShipAddr->Line1) ? $rowdata->ShipAddr->Line1 : null,
                    'shipAddr_line2' => isset($rowdata->ShipAddr->Line2) ? $rowdata->ShipAddr->Line2 : null,
                    'shipAddr_city' => isset($rowdata->ShipAddr->City) ? $rowdata->ShipAddr->City : null,
                    'shipAddr_country' => isset($rowdata->ShipAddr->Country) ? $rowdata->ShipAddr->Country : null,
                    //'shipAddr_countryCode' => isset($rowdata->ShipAddr->CountryCode) ? $rowdata->ShipAddr->CountryCode : null,
                    'shipAddr_countrySubDivisionCode' => isset($rowdata->ShipAddr->CountrySubDivisionCode) ? $rowdata->ShipAddr->CountrySubDivisionCode : null,
                    'shipAddr_postalCode' => isset($rowdata->ShipAddr->PostalCode) ? $rowdata->ShipAddr->PostalCode : null,
                    //'shipAddr_postalCodeSuffix' => isset($rowdata->ShipAddr->PostalCodeSuffix) ? $rowdata->ShipAddr->PostalCodeSuffix : null,
                   // 'shipAddr_lat' => isset($rowdata->ShipAddr->Lat) ? $rowdata->ShipAddr->Lat : null,
                    //'shipAddr_long' => isset($rowdata->ShipAddr->Long) ? $rowdata->ShipAddr->Long : null,
                    
                    'otherAddr' => isset($rowdata->OtherAddr) ? $rowdata->OtherAddr : null,
                    //'contactName' => isset($rowdata->ContactName) ? $rowdata->ContactName : null,
                    //'altContactName' => isset($rowdata->AltContactName) ? $rowdata->AltContactName : null,
                    'webAddr' => isset($rowdata->WebAddr->URI) ? $rowdata->WebAddr->URI : null,
                    //'otherContactInfo' => isset($rowdata->OtherContactInfo) ? $rowdata->OtherContactInfo : null,
                    'notes' => isset($rowdata->Notes) ? $rowdata->Notes : null,


                    'quickbooks_id' => $rowdata->Id,
                    'syncToken' => $rowdata->SyncToken,
                    'quickbooks' => 1,
                    'temp' => 1,
                ];
            }

        }

        try{
        Customer::insert($customer_all);
        echo "Customer data has been imported from Quickbooks. Please  <a href=" .route('customers.index', ['temp' => 'true']) . ">Check Here</a> and review it for saving into database";
        }
        catch(Exception $e) {
            echo $e; 
        }

    }
    else{
        echo "No new Customer data is present in Quickbooks. To see all the customers, please  <a href=" .route('customers.index') . ">Check Here</a>";
    }
    
    
    
}

    
    public function synchronizeCustomer(){
        session_start();

    /*
     * Retrieve the accessToken value from session variable
     */
    $accessToken = $_SESSION['sessionAccessToken'];
    $realmId = $_SESSION['realmId'];


    if( gmdate("Y/m/d H:i:s") > $accessToken->getAccessTokenExpiresAt()){
        echo "Your access token is expired, Please click on <b>Connect to Quickbooks</b> button above";
        die();
    }

    else{
        $this->dataService->updateOAuth2Token($accessToken);
    }


    dispatch(new \App\Jobs\QbSynchronizeData($this->dataService, 'syncEvo'));
  
    return  "Your customer data has been synchronized with quickbook";

    }

    public function synchronizeQb(){
        session_start();

    /*
     * Retrieve the accessToken value from session variable
     */
    $accessToken = $_SESSION['sessionAccessToken'];
    $realmId = $_SESSION['realmId'];


    if( gmdate("Y/m/d H:i:s") > $accessToken->getAccessTokenExpiresAt()){
        echo "Your access token is expired, Please click on <b>Connect to Quickbooks</b> button above";
        die();
    }

    else{
        $this->dataService->updateOAuth2Token($accessToken);
    }

    dispatch(new \App\Jobs\QbSynchronizeData($this->dataService, 'syncQb'));
    return  "Quickbook has been synchronized with Evo customers";



   
    }


    public function singleCustomerQB(){
        session_start();

    /*
     * Retrieve the accessToken value from session variable
     */
    //$accessToken = $_SESSION['sessionAccessToken'];
    //$realmId = $_SESSION['realmId'];

    if(isset($_SESSION['sessionAccessToken'])){
        $accessToken = $_SESSION['sessionAccessToken'];
        if( gmdate("Y/m/d H:i:s") > $accessToken->getAccessTokenExpiresAt()){
            echo "expire";
            die();
        }

        else{
           //echo "asasdsadasd";
        }
    }
    else{
        echo "connect";
    }

    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'displayName' => 'required|unique:customers',
        ]);

if($validator->fails()){
    return back()->withErrors($validator)->withInput(['tab'=>'custom_customer']);
}
else{

DB::beginTransaction();
    try{
        //dd('okk');
        $customer = Customer::create($request->except(['singleCustomerQB', 'shipAddr_line1', 'shipAddr_line2', 'shipAddr_city', 'shipAddr_country', 'shipAddr_countrySubDivisionCode', 'shipAddr_postalCode', 'lease_name', 'rig_number', 'afe_number', 'site_contact_phone', 'rig_contact_email', 'company_man_contact_name', 'latitude', 'longitude', 'is_default', 'service_default']));
        $customer->manual = 0; 
        $customer->save();
//dd('okk');
        $shipAddr_line1 = $request->shipAddr_line1;
        //dd($shipAddr_line1);
        foreach ($shipAddr_line1 as $key => $value) {
            if($request->is_default[$key] == 1){
                Customer::where('id', $customer->id)->update([
                    'shipAddr_line1' => $request->shipAddr_line1[$key],
                    'shipAddr_line2' => $request->shipAddr_line2[$key],
                    'shipAddr_city' => $request->shipAddr_city[$key],
                    'shipAddr_country' => $request->shipAddr_country[$key],
                    'shipAddr_countrySubDivisionCode' => $request->shipAddr_countrySubDivisionCode[$key],
                    'shipAddr_postalCode' => $request->shipAddr_postalCode[$key],
                    'lease_name' => $request->lease_name[$key],
                    'rig_number' => $request->rig_number[$key],
                    'afe_number' => $request->afe_number[$key],
                    'site_contact_phone' => $request->site_contact_phone[$key],
                    'rig_contact_email' => $request->rig_contact_email[$key],
                    'company_man_contact_name' => $request->company_man_contact_name[$key],
                    'latitude' => $request->latitude[$key],
                    'longitude' => $request->longitude[$key],
                ]);
            }
            else{
                ServiceLocationAddress::create([
                    'type' => 'customer',
                    'parent_id' => $customer->id,
                    'shipAddr_line1' => $request->shipAddr_line1[$key],
                    'shipAddr_line2' => $request->shipAddr_line2[$key],
                    'shipAddr_city' => $request->shipAddr_city[$key],
                    'shipAddr_country' => $request->shipAddr_country[$key],
                    'shipAddr_countrySubDivisionCode' => $request->shipAddr_countrySubDivisionCode[$key],
                    'shipAddr_postalCode' => $request->shipAddr_postalCode[$key],
                    'lease_name' => $request->lease_name[$key],
                    'rig_number' => $request->rig_number[$key],
                    'afe_number' => $request->afe_number[$key],
                    'site_contact_phone' => $request->site_contact_phone[$key],
                    'rig_contact_email' => $request->rig_contact_email[$key],
                    'company_man_contact_name' => $request->company_man_contact_name[$key],
                    'latitude' => $request->latitude[$key],
                    'longitude' => $request->longitude[$key],
                ]);
            }
        }

        if($request->singleCustomerQB == '1'){
            session_start();

            $accessToken = $_SESSION['sessionAccessToken'];
            $realmId = $_SESSION['realmId'];

            $this->dataService->updateOAuth2Token($accessToken);

            $customer_type = $this->dataService->Query("Select * from CustomerType WHERE Name = 'EVOS'");
            $customer_type_value = $customer_type ? $customer_type[0]->Id : null;

            $theResourceObj = \QuickBooksOnline\API\Facades\Customer::create([
                        "BillAddr" => [
                            "Line1" => $customer->billAddr_line1,
                            "Line2" => $customer->billAddr_line2,
                            "City" => $customer->billAddr_city,
                            "Country" => $customer->billAddr_country,
                            "CountrySubDivisionCode" => $customer->billAddr_countrySubDivisionCode,
                            "PostalCode" => $customer->billAddr_postalCode
                        ],
                        "ShipAddr" => [
                            "Line1" => $customer->shipAddr_line1,
                            "Line2" => $customer->shipAddr_line2,
                            "City" => $customer->shipAddr_city,
                            "Country" => $customer->shipAddr_country,
                            "CountrySubDivisionCode" => $customer->shipAddr_countrySubDivisionCode,
                            "PostalCode" => $customer->shipAddr_postalCode
                        ],
                        "OtherAddr"=> $customer->otherAddr,
                        "Notes" => $customer->notes,
                        "Title" => $customer->title,
                        "GivenName" => $customer->givenName,
                        "MiddleName" => $customer->middleName,
                        "FamilyName" => $customer->familyName,
                        "Suffix" => $customer->suffix,
                        "CompanyName" => $customer->companyName,
                        "DisplayName" => $customer->displayName,
                        "PrimaryPhone" => [
                            "FreeFormNumber" => $customer->primaryPhone_freeFormNumber
                        ],
                        "PrimaryEmailAddr" => [
                            "Address" => $customer->primaryEmailAddr_addess
                        ],
                        "AlternatePhone" => [
                            "FreeFormNumber" => $customer->alternatePhone
                        ],
                        "Mobile" => [
                            "FreeFormNumber" => $customer->mobile
                        ],
                        /*"Fax" => [
                            "FreeFormNumber" => $customer->fax
                        ],*/
                        "WebAddr" => [
                            "URI" => $customer->webAddr
                        ],
                        "CustomerTypeRef" => $customer_type_value,
            ]);

                    $resultingObj = $this->dataService->Add($theResourceObj);
                    Customer::where('id', $customer->id)->update(['manual' => 1, 'quickbooks' => 1, 'quickbooks_id' => $resultingObj->Id, 'syncToken' => $resultingObj->SyncToken,]);

                    DB::commit();
                    session()->flash('success', 'Successfully added to Evos database & Quickbooks');
        }
        else{
            DB::commit();
            session()->flash('success', 'Successfully added');  
        }

    }
    catch (\Exception $ex) {
            DB::rollback();
            //return response()->json(['error' => $ex->getMessage()], 500);
            session()->flash('error', $ex);
    }


        return back()->withInput(['tab'=>'custom_customer']);

        }
    }

    public function edit($id){

        $customer = Customer::find($id);
        $customer_service_location_address = ServiceLocationAddress::where('type', 'customer')->where('parent_id', $customer->id)->get();

        $credit_app = CreditApplication::where('customer_id', $id)->first();

        if($credit_app)
            $credit_app_history = CreditApplicationHistory::where('credit_app_id', $credit_app->id)->get();
        else
            $credit_app_history = [];  

        //dd($credit_app_history);

        return view('admin.customer.edit', compact('customer', 'customer_service_location_address', 'credit_app', 'credit_app_history'));
    }

    public function show($id){

        $customer = Customer::find($id);
        return view('admin.customer.show', compact('customer'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'displayName' => 'required|unique:customers,displayName,'.$id,
        ]);
        

    DB::beginTransaction();
    try{
        Customer::whereId($id)->update($request->except(['_token', '_method', 'singleCustomerQB', 'shipAddr_line1', 'shipAddr_line2', 'shipAddr_city', 'shipAddr_country', 'shipAddr_countrySubDivisionCode', 'shipAddr_postalCode', 'lease_name', 'rig_number', 'afe_number', 'site_contact_phone', 'rig_contact_email', 'company_man_contact_name', 'latitude', 'longitude', 'is_default', 'service_default', 'addr_key']));
        Customer::whereId($id)->update(["updated"=>1]);



        $addr_rows = ServiceLocationAddress::where('type', 'customer')->where('parent_id', $id)->get();
        $addr_key = $request->addr_key;
        //dd($addr_key);
        if($addr_key){
            foreach ($addr_rows as $rowdata) {
                   if(!in_array($rowdata->id, $addr_key)){
                      ServiceLocationAddress::where('id', $rowdata->id)->delete();  
                   }
            }
        }
        else{
            foreach ($addr_rows as $rowdata) {
                ServiceLocationAddress::where('id', $rowdata->id)->delete();  
            }
        }

        $shipAddr_line1 = $request->shipAddr_line1;
        //dd($request->afe_number);
        foreach($shipAddr_line1 as $key=>$value){
            if($request->is_default[$key] == 1){
                Customer::where('id', $id)->update([
                    'shipAddr_line1' => isset($request->shipAddr_line1[$key]) ? $request->shipAddr_line1[$key] : null,
                        'shipAddr_line2' => isset($request->shipAddr_line2[$key]) ? $request->shipAddr_line2[$key] : null,
                        'shipAddr_city' => isset($request->shipAddr_city[$key]) ? $request->shipAddr_city[$key] : null,
                        'shipAddr_country' => isset($request->shipAddr_country[$key]) ? $request->shipAddr_country[$key] : null,
                        'shipAddr_countrySubDivisionCode' => isset($request->shipAddr_countrySubDivisionCode[$key]) ? $request->shipAddr_countrySubDivisionCode[$key] : null,
                        'shipAddr_postalCode' => isset($request->shipAddr_postalCode[$key]) ? $request->shipAddr_postalCode[$key] : null,
                        'lease_name' => isset($request->lease_name[$key]) ? $request->lease_name[$key] : null,
                        'rig_number' => isset($request->rig_number[$key]) ? $request->rig_number[$key] : null,
                        'afe_number' => isset($request->afe_number[$key]) ? $request->afe_number[$key] : null,
                        'site_contact_phone' => isset($request->site_contact_phone[$key]) ? $request->site_contact_phone[$key] : null,
                        'rig_contact_email' => isset($request->rig_contact_email[$key]) ? $request->rig_contact_email[$key] : null,
                        'company_man_contact_name' => isset($request->company_man_contact_name[$key]) ? $request->company_man_contact_name[$key] : null,
                        'latitude' => isset($request->latitude[$key]) ? $request->latitude[$key] : null,
                        'longitude' => isset($request->longitude[$key]) ? $request->longitude[$key] : null,
                ]);

                if(isset($request->addr_key[$key])){
                    $addr_key = $request->addr_key[$key];
                    if($addr_key != 0){
                        $service_location_addr = ServiceLocationAddress::where('id', $addr_key)->first();
                        if($service_location_addr){
                            $service_location_addr->delete();
                        }
                    }
                }
                
            }
            else{
                if(isset($request->addr_key[$key])){
                    if($request->addr_key[$key] == 0){
                        ServiceLocationAddress::create([
                            'type' => 'customer',
                            'parent_id' => $id,
                            'shipAddr_line1' => isset($request->shipAddr_line1[$key]) ? $request->shipAddr_line1[$key] : null,
                        'shipAddr_line2' => isset($request->shipAddr_line2[$key]) ? $request->shipAddr_line2[$key] : null,
                        'shipAddr_city' => isset($request->shipAddr_city[$key]) ? $request->shipAddr_city[$key] : null,
                        'shipAddr_country' => isset($request->shipAddr_country[$key]) ? $request->shipAddr_country[$key] : null,
                        'shipAddr_countrySubDivisionCode' => isset($request->shipAddr_countrySubDivisionCode[$key]) ? $request->shipAddr_countrySubDivisionCode[$key] : null,
                        'shipAddr_postalCode' => isset($request->shipAddr_postalCode[$key]) ? $request->shipAddr_postalCode[$key] : null,
                        'lease_name' => isset($request->lease_name[$key]) ? $request->lease_name[$key] : null,
                        'rig_number' => isset($request->rig_number[$key]) ? $request->rig_number[$key] : null,
                        'afe_number' => isset($request->afe_number[$key]) ? $request->afe_number[$key] : null,
                        'site_contact_phone' => isset($request->site_contact_phone[$key]) ? $request->site_contact_phone[$key] : null,
                        'rig_contact_email' => isset($request->rig_contact_email[$key]) ? $request->rig_contact_email[$key] : null,
                        'company_man_contact_name' => isset($request->company_man_contact_name[$key]) ? $request->company_man_contact_name[$key] : null,
                        'latitude' => isset($request->latitude[$key]) ? $request->latitude[$key] : null,
                        'longitude' => isset($request->longitude[$key]) ? $request->longitude[$key] : null,
                                ]);
                    }
                    else{
                        ServiceLocationAddress::where('id', $request->addr_key[$key])->update([
                            'type' => 'customer',
                            'parent_id' => $id,
                            'shipAddr_line1' => isset($request->shipAddr_line1[$key]) ? $request->shipAddr_line1[$key] : null,
                        'shipAddr_line2' => isset($request->shipAddr_line2[$key]) ? $request->shipAddr_line2[$key] : null,
                        'shipAddr_city' => isset($request->shipAddr_city[$key]) ? $request->shipAddr_city[$key] : null,
                        'shipAddr_country' => isset($request->shipAddr_country[$key]) ? $request->shipAddr_country[$key] : null,
                        'shipAddr_countrySubDivisionCode' => isset($request->shipAddr_countrySubDivisionCode[$key]) ? $request->shipAddr_countrySubDivisionCode[$key] : null,
                        'shipAddr_postalCode' => isset($request->shipAddr_postalCode[$key]) ? $request->shipAddr_postalCode[$key] : null,
                        'lease_name' => isset($request->lease_name[$key]) ? $request->lease_name[$key] : null,
                        'rig_number' => isset($request->rig_number[$key]) ? $request->rig_number[$key] : null,
                        'afe_number' => isset($request->afe_number[$key]) ? $request->afe_number[$key] : null,
                        'site_contact_phone' => isset($request->site_contact_phone[$key]) ? $request->site_contact_phone[$key] : null,
                        'rig_contact_email' => isset($request->rig_contact_email[$key]) ? $request->rig_contact_email[$key] : null,
                        'company_man_contact_name' => isset($request->company_man_contact_name[$key]) ? $request->company_man_contact_name[$key] : null,
                        'latitude' => isset($request->latitude[$key]) ? $request->latitude[$key] : null,
                        'longitude' => isset($request->longitude[$key]) ? $request->longitude[$key] : null,
                        ]);   
                    }
                }
                else{
                    ServiceLocationAddress::create([
                        'type' => 'customer',
                        'parent_id' => $id,
                        'shipAddr_line1' => isset($request->shipAddr_line1[$key]) ? $request->shipAddr_line1[$key] : null,
                        'shipAddr_line2' => isset($request->shipAddr_line2[$key]) ? $request->shipAddr_line2[$key] : null,
                        'shipAddr_city' => isset($request->shipAddr_city[$key]) ? $request->shipAddr_city[$key] : null,
                        'shipAddr_country' => isset($request->shipAddr_country[$key]) ? $request->shipAddr_country[$key] : null,
                        'shipAddr_countrySubDivisionCode' => isset($request->shipAddr_countrySubDivisionCode[$key]) ? $request->shipAddr_countrySubDivisionCode[$key] : null,
                        'shipAddr_postalCode' => isset($request->shipAddr_postalCode[$key]) ? $request->shipAddr_postalCode[$key] : null,
                        'lease_name' => isset($request->lease_name[$key]) ? $request->lease_name[$key] : null,
                        'rig_number' => isset($request->rig_number[$key]) ? $request->rig_number[$key] : null,
                        'afe_number' => isset($request->afe_number[$key]) ? $request->afe_number[$key] : null,
                        'site_contact_phone' => isset($request->site_contact_phone[$key]) ? $request->site_contact_phone[$key] : null,
                        'rig_contact_email' => isset($request->rig_contact_email[$key]) ? $request->rig_contact_email[$key] : null,
                        'company_man_contact_name' => isset($request->company_man_contact_name[$key]) ? $request->company_man_contact_name[$key] : null,
                        'latitude' => isset($request->latitude[$key]) ? $request->latitude[$key] : null,
                        'longitude' => isset($request->longitude[$key]) ? $request->longitude[$key] : null,
                        ]);
                }
            }
        }


//dd('okk');
        if($request->singleCustomerQB == '1'){
            session_start();
            if(isset($_SESSION['sessionAccessToken'])){
                $accessToken = $_SESSION['sessionAccessToken'];

                if( gmdate("Y/m/d H:i:s") > $accessToken->getAccessTokenExpiresAt()){
                    session()->flash('error', 'Your access token is expired, Please click on the connect to quickbooks button first if you want to synchronize with quickbooks');  
                }
                else{

                $this->dataService->updateOAuth2Token($accessToken);
                $customer_row= Customer::find($id);
                if($customer_row->quickbooks == 1){
                    $customer = $this->dataService->FindbyId('customer', $customer_row->quickbooks_id);
                    $theResourceObj = \QuickBooksOnline\API\Facades\Customer::update($customer  , [
                          "BillAddr" => [
                                    "Line1" => $request->billAddr_line1,
                                    "Line2" => $request->billAddr_line2,
                                    "City" => $request->billAddr_city,
                                    "Country" => $request->billAddr_country,
                                    "CountrySubDivisionCode" => $request->billAddr_countrySubDivisionCode,
                                    "PostalCode" => $request->billAddr_postalCode
                                ],
                                "ShipAddr" => [
                                    "Line1" => $customer_row->shipAddr_line1,
                                    "Line2" => $customer_row->shipAddr_line2,
                                    "City" => $customer_row->shipAddr_city,
                                    "Country" => $customer_row->shipAddr_country,
                                    "CountrySubDivisionCode" => $customer_row->shipAddr_countrySubDivisionCode,
                                    "PostalCode" => $customer_row->shipAddr_postalCode
                                ],
                                "OtherAddr"=> $request->otherAddr,
                                "Notes" => $request->notes,
                                "Title" => $request->title,
                                "GivenName" => $request->givenName,
                                "MiddleName" => $request->middleName,
                                "FamilyName" => $request->familyName,
                                "Suffix" => $request->suffix,
                                "CompanyName" => $request->companyName,
                                "DisplayName" => $request->displayName,
                                "PrimaryPhone" => [
                                    "FreeFormNumber" => $request->primaryPhone_freeFormNumber
                                ],
                                "PrimaryEmailAddr" => [
                                    "Address" => $request->primaryEmailAddr_addess
                                ],
                                "AlternatePhone" => [
                                    "FreeFormNumber" => $request->alternatePhone
                                ],
                                "Mobile" => [
                                    "FreeFormNumber" => $request->mobile
                                ],
                                /*"Fax" => [
                                    "FreeFormNumber" => $request->fax
                                ],*/
                                "WebAddr" => [
                                    "URI" => $request->webAddr
                                ]
                    ]);
                    $resultingObj = $this->dataService->Update($theResourceObj);
                    Customer::where('id', $customer_row->id)->increment('syncToken');
                    Customer::where('id', $customer_row->id)->update(['updated' => 0]);
                    
                    DB::commit();
                    session()->flash('success', 'Successfully Updated & synchronized with quickbooks');
                }
                else{
                    $customer_type = $this->dataService->Query("Select * from CustomerType WHERE Name = 'EVOS'");
                    $customer_type_value = $customer_type ? $customer_type[0]->Id : null;

                    $theResourceObj = \QuickBooksOnline\API\Facades\Customer::create([
                        "BillAddr" => [
                            "Line1" => $request->billAddr_line1,
                            "Line2" => $request->billAddr_line2,
                            "City" => $request->billAddr_city,
                            "Country" => $request->billAddr_country,
                            "CountrySubDivisionCode" => $request->billAddr_countrySubDivisionCode,
                            "PostalCode" => $request->billAddr_postalCode
                        ],
                        "ShipAddr" => [
                            "Line1" => $customer_row->shipAddr_line1,
                            "Line2" => $customer_row->shipAddr_line2,
                            "City" => $customer_row->shipAddr_city,
                            "Country" => $customer_row->shipAddr_country,
                            "CountrySubDivisionCode" => $customer_row->shipAddr_countrySubDivisionCode,
                            "PostalCode" => $customer_row->shipAddr_postalCode
                        ],
                        "OtherAddr"=> $request->otherAddr,
                        "Notes" => $request->notes,
                        "Title" => $request->title,
                        "GivenName" => $request->givenName,
                        "MiddleName" => $request->middleName,
                        "FamilyName" => $request->familyName,
                        "Suffix" => $request->suffix,
                        "CompanyName" => $request->companyName,
                        "DisplayName" => $request->displayName,
                        "PrimaryPhone" => [
                            "FreeFormNumber" => $request->primaryPhone_freeFormNumber
                        ],
                        "PrimaryEmailAddr" => [
                            "Address" => $request->primaryEmailAddr_addess
                        ],
                        "AlternatePhone" => [
                            "FreeFormNumber" => $request->alternatePhone
                        ],
                        "Mobile" => [
                            "FreeFormNumber" => $request->mobile
                        ],
                        /*"Fax" => [
                            "FreeFormNumber" => $request->fax
                        ],*/
                        "WebAddr" => [
                            "URI" => $request->webAddr
                        ],
                        "CustomerTypeRef"=> $customer_type_value,
                    ]);

                    $resultingObj = $this->dataService->Add($theResourceObj);
                    if($customer_row->manual == 0){
                        Customer::where('id', $customer_row->id)->update(['manual' => 1, 'quickbooks' => 1, 'quickbooks_id' => $resultingObj->Id, 'syncToken' => $resultingObj->SyncToken, 'updated' => 0,]);
                    }
                    else{
                        Customer::where('id', $customer_row->id)->update(['quickbooks' => 1, 'quickbooks_id' => $resultingObj->Id, 'syncToken' => $resultingObj->SyncToken, 'updated' => 0,]);
                    }

                    DB::commit();
                    session()->flash('success', 'Successfully added to Evos database & Quickbooks');
                }

                }
            }
            else{
              session()->flash('error', 'Please click on the connect to quickbooks button first if you want to synchronize with quickbooks');  
            }
        }
        else{
            DB::commit();
            session()->flash('success', 'Successfully Updated');
        }
    }
    catch (\Exception $ex) {
            DB::rollback();
            session()->flash('error', $ex);
    }


        return back();

    }

     public function destroy(Customer $customer){
            Customer::where('id', $customer->id)->update(['updated' => 2]);
            $customer->delete();
            session()->flash('success', 'Successfully deleted');
            return back();
        }

    public function importQbData(Request $request){
        $value = $request->get('value');
        echo $value;
    }
    public function insertQbCustomer(){

        $customer = Customer::where('temp', 1)->get();
        if(!$customer->isEmpty()){
            $customer = Customer::where('temp', 1)->update(['temp' => 0]);
            session()->flash("message", "<b>Customer data has been imported from Quickbooks into database. To see all the customers, please  <a href=" .route('customers.index') . ">Check Here</a></b>");
        }
        else
            session()->flash("message", "<b>There is no temporary customer data for review. To see all the customers, please  <a href=" .route('customers.index') . ">Check Here</a></b>");
    }

    public function discardQbCustomer(){
        $customer = Customer::where('temp', 1)->get();
        if(!$customer->isEmpty()){
            $customer = Customer::where('temp', 1)->delete();
            session()->flash("message", "<b>Customer data has been discarded. To see all the customers, please  <a href=" .route('customers.index') . ">Check Here</a></b>");
        }
        else
            session()->flash("message", "<b>There is no temporary customer data for discard. To see all the customers, please  <a href=" .route('customers.index') . ">Check Here</a></b>");
    }


    public function creditApplication(){
        return view('admin.customer.creditApplication');
    }

    public function creditApplicationEmail(){
        return view('admin.customer.creditApplicationEmail');
    }

    public function creditApplicationPdf(){
        $pdf = PDF::loadView('admin.customer.creditApplicationPdf')->setPaper('a4');  
        $output = $pdf->output();
        $response = Response::make($output,200);
        $response->header('Content-Type', 'application/pdf');
        return $response;
    }

    public function creditAppEdit($id){
        $customer_credit = Customer::find($id);
        
        if($customer_credit->credit_app_creation == 0){
            session()->flash('message', 'Credit Application is not generated yet for this customer');
            return back();
        }
        else{
            $credit_app = CreditApplication::where('customer_id', $id)->first();
            if($credit_app)
                $customer_sign = Image::where('type', 'customer_sign')->where('parent_id', $credit_app->id)->first();
            else
                $customer_sign =null;

            if(isset($_GET['cr_limit'])){
                $cr_limit_history = CreditApplicationHistory::where('credit_app_id', $credit_app->id)->where('id', $_GET['cr_limit'])->first();
                if($cr_limit_history){
                    $cr_limit = $cr_limit_history->credit_limit;
                    $ca_terms = $cr_limit_history->ca_terms;
                }
                else{
                    session()->flash('error', 'This Credit application is unavailable');
                    return back();
                }
            }
            else{
                if($credit_app){
                    $cr_limit = $credit_app->ca_credit_limit;
                    $ca_terms = $credit_app->ca_terms;
                }
                else{
                    $cr_limit = null;
                    $ca_terms = null;
                }
            }


            return view('admin.customer.creditApplication', compact('customer_credit', 'credit_app', 'customer_sign', 'cr_limit', 'ca_terms'));
        }
    }

    public function creditAppShow($id){
        $customer_credit = Customer::find($id);
        
        if($customer_credit->credit_app_creation == 0){
            session()->flash('message', 'Credit Application is not generated yet for this customer');
            return back();
        }
        else{
            $credit_app = CreditApplication::where('customer_id', $id)->first();
            if($credit_app)
                $customer_sign = Image::where('type', 'customer_sign')->where('parent_id', $credit_app->id)->first();
            else
                $customer_sign =null;

            return view('admin.customer.creditApplicationShow', compact('customer_credit', 'credit_app', 'customer_sign'));
        }
    }

    public function creditAppDelete($id){
        $customer = Customer::find($id);

        if(isset($_GET['cr_limit'])){
           $cr_limit_history = CreditApplicationHistory::where('credit_app_id', $credit_app->id)->where('id', $_GET['cr_limit'])->first();  
           if($cr_limit_history){
            $cr_limit_history->delete();
            session()->flash('success', 'Successfully Deleted');
            return back();
           }
           else{
            session()->flash('error', 'This Credit application is unavailable');
            return back();
           }
        }
        else{
            Customer::where('id', $customer->id)->update(['credit_app_creation' => 0]);
            $credit_app = CreditApplication::where('customer_id', $customer->id)->first();
            if($credit_app){
                CreditApplicationHistory::where('credit_app_id', $credit_app->id)->delete();  
                $credit_app->delete();
            }

            session()->flash('success', 'Deleted Credit Application');  
            return back();    
        }
        
    }

    public function creditAppUpdate(Request $request){
        if($request->status == 1){
            //dd('okkk');
                $request->validate([
                    'customer_send_mail_to' => 'required|email',
                ]);

                $credit_app = CreditApplication::create($request->except(['submit', 'files']));
                
                $credit_app->access_code =  mt_rand(100000,999999);
                $credit_app->url_token =  $credit_app->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);

                $credit_app->submitted_by_email = auth()->user()->email;
                $credit_app->submitted_by_name = auth()->user()->name;
                $credit_app->sent = json_encode([Carbon::now()]);
                $credit_app->save();

                $request_type = 'credit_app';
                $subject = 'Credit Application Request';
                $body = '<p>Thank you for doing / requesting business with EV Oilfield Services.  Please click the link below to fill out our Credit Application.  We Thank You your business and appreciate the opportunity to work with you!</p>';
                $status = 2;
                $reject = '';
                Mail::to($request->customer_send_mail_to)->send(new CreditAppEmail($credit_app, $request_type, $subject, $body, $status, $reject, ''));

                session()->flash('success', 'Successfully submitted.');  
                return back();
            }

            elseif($request->status == 3){

            if($request->submit == 'reject'){
                $credit_app = CreditApplication::where('id', $request->p_key)->first();
                CreditApplication::where('id', $request->p_key)
                                                                ->update($request->except([
                                                                    'submit',
                                                                    'p_key',
                                                                    '_token',
                                                                    'files'
                                                                ])
                                                            );
                $credit_app->access_code =  mt_rand(100000,999999);
                $credit_app->url_token =  $credit_app->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);
                $credit_app->status = 4;

                $arr = json_decode($credit_app->sent, true);
                $arr[] = Carbon::now();
                $credit_app->sent = json_encode($arr);
                
                $credit_app->update();


                $request_type = 'credit_app';
                $subject = 'Credit Application';
                $body = 'Hello, Your Credit Application (CR-'.$credit_app->customer_id.') has been rejected because -';
                $reject = $request->reject;
                $status = 4;
                Mail::to($credit_app->customer_send_mail_to)->send(new CreditAppEmail($credit_app, $request_type, $subject, $body, $status, $reject, ''));

                session()->flash('success', 'Successfully submitted.');
                return back();
            }
            else if($request->submit == 'first_submit'){
                $credit_app = CreditApplication::where('id', $request->p_key)->first();

                CreditApplication::where('id', $request->p_key)
                                                                ->update($request->except([
                                                                    'submit',
                                                                    'p_key',
                                                                    '_token',
                                                                    'files'
                                                                ])
                                                            );
                $credit_app->access_code =  mt_rand(100000,999999);
                $credit_app->url_token =  $credit_app->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);

                $arr = json_decode($credit_app->sent, true);
                $arr[] = Carbon::now();
                $credit_app->sent = json_encode($arr);

                $credit_app->update();

                $request_type = 'credit_app';
                $subject = 'Credit Application';
                $body = '<p>Thank you for doing / requesting business with EV Oilfield Services. Congratulation your credit application is approved and finalized. Please click the link below to view the acknowledgement. We Thank You your business and appreciate the opportunity to work with you!</p>';
                $status = 5;
                $reject = '';
                Mail::to($request->customer_send_mail_to)->send(new CreditAppEmail($credit_app, $request_type, $subject, $body, $status, $reject, ''));

                session()->flash('success', 'Successfully submitted.');
                return back();
                }
            else if($request->submit == 'cr_limit_history'){

                //dd($request->submitted_by_email);


                $credit_app = CreditApplication::where('id', $request->p_key)->first();

                /*CreditApplication::where('id', $request->p_key)
                                                                ->update($request->except([
                                                                    'submit',
                                                                    'p_key',
                                                                    '_token',
                                                                    'files',
                                                                    'ca_credit_limit',
                                                                    'customer_send_mail_date'
                                                                ])
                                                            );*/
                //$credit_app->access_code =  mt_rand(100000,999999);
                //$credit_app->url_token =  $credit_app->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);

                $arr = json_decode($credit_app->sent, true);
                $arr[] = Carbon::now();
                $credit_app->sent = json_encode($arr);

                $credit_app->update();

                $credit_app_history = CreditApplicationHistory::create([
                    'credit_app_id' => $credit_app->id,
                    'credit_limit' => $request->ca_credit_limit,
                    'ca_terms' => $request->ca_terms,
                    'date' => $request->customer_send_mail_date,

                ]);

                $request_type = 'credit_app';
                $subject = 'Credit Application';
                $body = 'Hello, A Credit Application\'s (CR-'.$credit_app->customer_id.') credit limit has been changed by -'. $request->submitted_by_email .'-'. $request->submitted_by_name;
                $status = 5;
                $reject = '';
                Mail::to($request->customer_send_mail_to)->send(new CreditAppEmail($credit_app, $request_type, $subject, $body, $status, $reject, $credit_app_history));

                session()->flash('success', 'Successfully submitted.');
                return back();
            }

            }
    }


    public function creditAppUpdateForCustomer(Request $request){
        if($request->status == 2 || $request->status == 4){
                $credit_app = CreditApplication::where('id', $request->p_key)->first();
                CreditApplication::where('id', $request->p_key)
                                                                ->update($request->except([
                                                                    'submit',
                                                                    'p_key',
                                                                    '_token',
                                                                    'signed'
                                                                ])
                                                            );
                $credit_app->access_code =  mt_rand(100000,999999);
                $credit_app->url_token =  $credit_app->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);
                $credit_app->status =  2;
                $credit_app->update();

            if($request->signed){
                $image_parts = explode(";base64,", $request->signed);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                
                $original_name = 'customer_' . time() . '_' .$credit_app->id;
                $file = storage_path() . '/app/public/files/' . $original_name . '.'.$image_type;
                file_put_contents($file, $image_base64);

                $customer_sign = Image::where('type', 'customer_sign')->where('parent_id', $request->p_key)->first();
                if($customer_sign){
                    Image::where('parent_id', $request->p_key)->update([
                        'name' => $original_name . '.' . $image_type,
                        'url' => url('/') . '/storage/files/' . $original_name . '.' . $image_type,
                    ]);
                }
                else{
                    Image::create([
                        'type' => 'customer_sign',
                        'parent_id' => $request->p_key,
                        'name' => $original_name . '.' . $image_type,
                        'url' => url('/') . '/storage/files/' . $original_name . '.' . $image_type,
                    ]);
                }
            }


                $request_type = 'credit_app';
                $subject = 'Credit Application';
                $body = $request->status == 2 ? 'Hello, A Credit Application (CR-'.$credit_app->customer_id.') has been submitted by -'. $credit_app->customer_send_mail_to : 'Hello, A Credit Application (CR-'.$credit_app->customer_id.') has been re-submitted by -'. $credit_app->customer_send_mail_to;
                $status = 3;
                $reject = '';
                Mail::to($credit_app->submitted_by_email)->send(new CreditAppEmail($credit_app, $request_type, $subject, $body, $status, $reject, ''));

                session()->flash('message', 'Successfully submitted.');
                return redirect()->route('creditAppMessage');

            }
    }

    public function creditAppToken($token){
        if($_GET['reqType'] == 'credit_app'){
            $url_token = $token;
            $req_type = 'credit_app';
            return view('admin.customer.accessCode', compact('url_token', 'req_type'));
        }
    }

    public function creditAppMessage(){
        return view('admin.customer.creditAppView');
    }

    public function creditAppCustomer(Request $request){
        $credit_app = CreditApplication::where('url_token', $request->url_token)->first();
        if($credit_app){
            if($request->access_code != $credit_app->access_code){
                session()->flash('error', 'Access Code is not matched');
                return back();
            }
            else{
                //dd($_GET['reqType']);
                $status = $request->status;
            
                if($status == 5){
                    $arr = json_decode($credit_app->seen, true);
                    $arr[] = Carbon::now();
                    $credit_app->seen = json_encode($arr);
                    $credit_app->save();

                    $customer_sign = Image::where('type', 'customer_sign')->where('parent_id', $credit_app->id)->first();

                    if($request->cr_limit != 0){
                        $cr_limit_history = CreditApplicationHistory::where('id', $request->cr_limit)->where('credit_app_id', $credit_app->id)->first();
                        if($cr_limit_history){
                            $cr_limit = $cr_limit_history->credit_limit;
                            $ca_terms = $cr_limit_history->ca_terms;
                        }
                        else{
                             session()->flash('error', 'This Credit application is unavailable');
                             return back();
                        }
                    }

                    else{
                        $cr_limit = $credit_app->ca_credit_limit;
                        $ca_terms = $credit_app->ca_terms;
                    }

                    $pdf = PDF::loadView('admin.customer.creditApplicationPdf', array('credit_app' => $credit_app, 'customer_sign' => $customer_sign, 'cr_limit' => $cr_limit, 'ca_terms' => $ca_terms))->setPaper('a4');  
                    $output = $pdf->output();
                    $response = Response::make($output,200);
                    $response->header('Content-Type', 'application/pdf');
                    //dd($cr_limit);
                    return $response;
                }
                    
                else{
                    if($status != 3){
                        if(!$credit_app->seen){
                            $credit_app->seen = json_encode([Carbon::now()]);
                            $credit_app->save();
                        }
                        else{
                            $arr = json_decode($credit_app->seen, true);
                            $arr[] = Carbon::now();
                            $credit_app->seen = json_encode($arr);
                            $credit_app->save();
                        }
                    }
                    
                    $url_token = $request->url_token;
                    $customer_sign = Image::where('type', 'customer_sign')->where('parent_id', $credit_app->id)->first();
                    return view('admin.customer.creditApplicationEmail', compact('status', 'credit_app', 'url_token', 'customer_sign'));
                }
            }
        }
        else{
            session()->flash('error', 'This Credit application is unavailable');
            return back();
        }
    }

    public function creditAppPdf($id){
        //dd($id);
        $credit_app = CreditApplication::where('customer_id', $id)->first();

        $customer_sign = Image::where('type', 'customer_sign')->where('parent_id', $credit_app->id)->first();
        if($credit_app->status==3){
            //dd($_GET['cr_limit']);
            if(isset($_GET['cr_limit'])){
                $cr_limit_history = CreditApplicationHistory::where('credit_app_id', $credit_app->id)->where('id', $_GET['cr_limit'])->first();
                if($cr_limit_history){
                    $cr_limit = $cr_limit_history->credit_limit;
                    $ca_terms = $cr_limit_history->ca_terms;
                }
                else{
                    session()->flash('error', 'This Credit application pdf is unavailable');
                    return back();
                }
            }
            else{
                $cr_limit = $credit_app->ca_credit_limit;
                $ca_terms = $credit_app->ca_terms;
            }

            $pdf = PDF::loadView('admin.customer.creditApplicationPdf', array('credit_app' => $credit_app, 'customer_sign' => $customer_sign, 'cr_limit' => $cr_limit, 'ca_terms' => $ca_terms))->setPaper('a4');  
            $output = $pdf->output();
            $response = Response::make($output,200);
            $response->header('Content-Type', 'application/pdf');
            return $response; 
        }
        else{
            session()->flash('error', 'This Credit application pdf is unavailable');
            return back();
        }

    }


}
