<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class JobChecklistController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(){
        return view('admin.jobChecklist.index');
	}
    

    public function create(){
    }

    
    public function store(Request $request){
    }

    
    public function show(){
    	//
    }

    
    public function edit($id){
    }

    
     public function update(Request $request, $id){
    }

    
     public function destroy($id){
    }


}
