<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Customer;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Product;
use App\Models\ProductInventory;
use App\Models\Inventory;
use App\Models\Variable;
use App\Models\VariableSection;
use App\Models\VariableParameter;
use App\Models\Quotation;
use App\Models\QuoteItem;
use App\Models\QuoteItemDetails;
use App\Models\QuoteVariable;
use App\Models\QuoteVariableParameter;
use App\Models\ServiceLocationAddress;
use DB;

class QuotationController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(){
        $quotations = Quotation::all();
        return view('admin.quotation.index', compact('quotations'));
	}
    

    public function create(){
        $customers = Customer::select('id', 'displayName')->get();
        $cat = Category::all();
        $subcat = Subcategory::all();
    	return view('admin.quotation.create', compact('customers','cat', 'subcat'));
    }

    
    public function store(Request $request){

        $quotation = new Quotation([
          'quote_ticket' => $request->quote_ticket,
          'date' => $request->date,
          'customer_id' => $request->customer_id,
          'total_gross_cost' => $request->total_gross_cost,
        ]);
        $quotation->save();
        

        $item_id = $request->item_id;
        foreach ($item_id as $key => $value) {
          //dd($request->resource_price[0][0]);

           if($request->address_selected[$key][0] == 'new_address'){
              $new_address = ServiceLocationAddress::create([
                  'type' => 'customer',
                  'parent_id' => $quotation->customer_id,
                  'shipAddr_line1' => isset($request->line1[$key][0]) ? $request->line1[$key][0] : null,
                  'shipAddr_line2' => isset($request->line2[$key][0]) ? $request->line2[$key][0] : null,
                  'shipAddr_city' => isset($request->city[$key][0]) ? $request->city[$key][0] : null,
                  'shipAddr_country' => isset($request->country[$key][0]) ? $request->country[$key][0] : null,
                  'shipAddr_countrySubDivisionCode' => isset($request->subdivisioncode[$key][0]) ? $request->subdivisioncode[$key][0] : null,
                  'shipAddr_postalCode' => isset($request->postalcode[$key][0]) ? $request->postalcode[$key][0] : null,
              ]);
              $service_location_address_id = $new_address->id;
           }
           else{
              $service_location_address_id = $request->address_selected[$key][0];
           }

           $quoteItem = QuoteItem::create([
                  'quote_id'=> $quotation->id,
                  'item_id'=> $request->item_id[$key][0],
                  'item_type'=> $request->item_type[$key][0], 
                  'service_location_address_id'=> $service_location_address_id, 
                  'item_total'=> isset($request->item_total[$key][0]) ? $request->item_total[$key][0] : null,
                  'var_total'=> isset($request->var_total[$key][0]) ? $request->var_total[$key][0] : null, 
                  'gross_total'=> isset($request->gross_total[$key][0]) ? $request->gross_total[$key][0] : null, 
           ]);

           //$service_variable = $request->service_var_name[$key];
           if(isset($request->service_var_name[$key])){
             foreach ($request->service_var_name[$key] as $service_key => $service_value) {
               $quoteItemVariable = QuoteVariable::create([
                      'quote_id'=> $quotation->id,
                      'quote_item_type'=> 'service',
                      'quote_item_id'=> $quoteItem->id,
                      'var_name'=> isset($request->service_var_name[$key][$service_key]) ? $request->service_var_name[$key][$service_key] : null,                       
                      'item_var_total'=> isset($request->service_var_total[$key][$service_key]) ? $request->service_var_total[$key][$service_key] : null,                       
                  ]);

                  if(isset($request->service_var_param_name[$key][$service_key])){
                     foreach ($request->service_var_param_name[$key][$service_key] as $service_param_key => $service_param_value) {
                       $quote_var_param = QuoteVariableParameter::create([
                              'quote_id'=> $quotation->id,
                              //'quote_item_type'=> 'service',
                              'quote_item_var_id'=> $quoteItemVariable->id,
                              'var_param_name'=> isset($request->service_var_param_name[$key][$service_key][$service_param_key]) ? $request->service_var_param_name[$key][$service_key][$service_param_key] : null,                       
                              'var_param_data_type'=> isset($request->service_var_param_data_type[$key][$service_key][$service_param_key]) ? $request->service_var_param_data_type[$key][$service_key][$service_param_key] : null,                       
                              'var_param_value_type'=> isset($request->service_var_param_value_type[$key][$service_key][$service_param_key]) ? $request->service_var_param_value_type[$key][$service_key][$service_param_key] : null,                       
                              'var_param_value'=> isset($request->service_var_param_value[$key][$service_key][$service_param_key]) ? $request->service_var_param_value[$key][$service_key][$service_param_key] : null,                       
                          ]);
                     }
                  }

             }
          }


           //$resource_id = $request->resource_id[$key];
           if(isset($request->resource_id[$key])){
             foreach($request->resource_id[$key] as $key1=>$value1){
                $quoteItemDetails = QuoteItemDetails::create([
                    'quote_id'=> $quotation->id,
                    'quote_item_id'=> $quoteItem->id,
                    'resource_id'=> $request->resource_id[$key][$key1],
                    'resource_price'=> $request->resource_price[$key][$key1], 
                    'resource_unit'=> $request->resource_unit[$key][$key1], 
                    'resource_duration'=> $request->resource_duration[$key][$key1], 
                    'item_detail_total'=> $request->item_detail_total[$key][$key1],
                ]);

                if(isset($request->resource_var_name[$key][$key1])){
                    foreach($request->resource_var_name[$key][$key1] as $key2=>$value2){
                      $quoteItemVariable = QuoteVariable::create([
                          'quote_id'=> $quotation->id,
                          'quote_item_type'=> 'inventory',
                          'quote_item_id'=> $quoteItemDetails->id,
                          'var_name'=> isset($request->resource_var_name[$key][$key1][$key2]) ? $request->resource_var_name[$key][$key1][$key2] : null,                           
                          'item_var_total'=> isset($request->item_var_total[$key][$key1][$key2]) ? $request->item_var_total[$key][$key1][$key2] : null,                           
                      ]);
                      if(isset($request->resource_var_param_name[$key][$key1][$key2])){
                          foreach($request->resource_var_param_name[$key][$key1][$key2] as $key3=>$value3){
                            $quoteVarParam = QuoteVariableParameter::create([
                                'quote_id'=> $quotation->id,
                                //'quote_item_type'=> 'inventory',
                                'quote_item_var_id'=> $quoteItemVariable->id,
                                'var_param_name'=> isset($request->resource_var_param_name[$key][$key1][$key2][$key3]) ? $request->resource_var_param_name[$key][$key1][$key2][$key3] : null,
                                'var_param_data_type'=> isset($request->resource_var_param_data_type[$key][$key1][$key2][$key3]) ? $request->resource_var_param_data_type[$key][$key1][$key2][$key3] : null,
                                'var_param_value_type'=> isset($request->resource_var_param_value_type[$key][$key1][$key2][$key3]) ? $request->resource_var_param_value_type[$key][$key1][$key2][$key3] : null,
                                'var_param_value'=> isset($request->resource_var_param_value[$key][$key1][$key2][$key3]) ? $request->resource_var_param_value[$key][$key1][$key2][$key3] : null,
                            ]);
                          }
                      }

                    }
                }
             }

           }

        }      

        session()->flash('success', 'Successfully added');
        return back();

    }

    
    public function show(){
    	//
    }

    
    public function edit($id){
        $quotation = Quotation::find($id);
        $customers = Customer::select('id', 'displayName')->get();
        $cat = Category::all();
        $subcat = Subcategory::all();

        $quote_item = QuoteItem::where('quote_id', $quotation->id)->get();
        //dd($quote_item->count());
        return view('admin.quotation.edit', compact('quotation', 'customers', 'cat', 'subcat', 'quote_item'));
    }

    
     public function update(Request $request, $id){
     	$quote = Quotation::find($id);

        $quotation = Quotation::where('id', $quote->id)->update([
          'quote_ticket' => $request->quote_ticket,
          'date' => $request->date,
          'customer_id' => $request->customer_id,
          'total_gross_cost' => $request->total_gross_cost,
        ]);

        $quote_item_key_for_dlt = $request->quote_item_key_for_dlt;
        $quote_tem_rows = QuoteItem::where('quote_id', $id)->get();
        //dd($quote_item_key_for_dlt);
        if($quote_item_key_for_dlt){
            foreach ($quote_tem_rows as $rowdata) {
                   if(!in_array($rowdata->id, $quote_item_key_for_dlt)){
                      QuoteItem::where('id', $rowdata->id)->delete();  
                   }
            }
        }
        else{
            foreach ($quote_tem_rows as $rowdata) {
                    QuoteItem::where('id', $rowdata->id)->delete();  
            }   
        }

      $quote_item_key = $request->quote_item_key;
      //dd($quote_item_key);
      if($quote_item_key){
        foreach ($quote_item_key as $key => $value) {
          //dd($request->resource_price[0][0]);

           if($request->address_selected[$key][0] == 'new_address'){
              $new_address = ServiceLocationAddress::create([
                  'type' => 'customer',
                  'parent_id' => $quote->customer_id,
                  'shipAddr_line1' => isset($request->line1[$key][0]) ? $request->line1[$key][0] : null,
                  'shipAddr_line2' => isset($request->line2[$key][0]) ? $request->line2[$key][0] : null,
                  'shipAddr_city' => isset($request->city[$key][0]) ? $request->city[$key][0] : null,
                  'shipAddr_country' => isset($request->country[$key][0]) ? $request->country[$key][0] : null,
                  'shipAddr_countrySubDivisionCode' => isset($request->subdivisioncode[$key][0]) ? $request->subdivisioncode[$key][0] : null,
                  'shipAddr_postalCode' => isset($request->postalcode[$key][0]) ? $request->postalcode[$key][0] : null,
              ]);
              $service_location_address_id = $new_address->id;
           }
           else{
              $service_location_address_id = $request->address_selected[$key][0];
           }

           $quoteItem = QuoteItem::where('id', $request->quote_item_key[$key][0])->update([
                  //'quote_id'=> $quotation->id,
                  //'item_id'=> $request->item_id[$key][0],
                  //'item_type'=> $request->item_type[$key][0], 
                  'service_location_address_id'=> $service_location_address_id, 
                  'item_total'=> isset($request->item_total[$key][0]) ? $request->item_total[$key][0] : null,
                  'var_total'=> isset($request->var_total[$key][0]) ? $request->var_total[$key][0] : null, 
                  'gross_total'=> isset($request->gross_total[$key][0]) ? $request->gross_total[$key][0] : null,
           ]);

           //$service_variable = $request->service_var_name[$key];
           if(isset($request->service_var_name[$key])){
             foreach ($request->service_var_name[$key] as $service_key => $service_value) {
               $quoteItemVariable = QuoteVariable::where('id', $request->service_var_key[$key][$service_key])->update([
                      //'quote_id'=> $quotation->id,
                      'quote_item_type'=> 'service',
                      //'quote_item_id'=> $quoteItem->id,
                      'var_name'=> isset($request->service_var_name[$key][$service_key]) ? $request->service_var_name[$key][$service_key] : null, 
                      'item_var_total'=> isset($request->service_var_total[$key][$service_key]) ? $request->service_var_total[$key][$service_key] : null,                              
                  ]);

                if(isset($request->service_var_param_name[$key][$service_key])){
                     foreach ($request->service_var_param_name[$key][$service_key] as $service_param_key => $service_param_value) {
                       $quote_var_param = QuoteVariableParameter::where('id', $request->service_var_param_key[$key][$service_key][$service_param_key])->update([
                              //'quote_id'=> $quotation->id,
                              //'quote_item_type'=> 'service',
                              //'quote_item_var_id'=> $quoteItemVariable->id,
                        //dd('okk'),
                        //dd($request->service_var_param_key[$key][$service_key][$service_param_key]),

                              'var_param_name'=> isset($request->service_var_param_name[$key][$service_key][$service_param_key]) ? $request->service_var_param_name[$key][$service_key][$service_param_key] : null,                       
                              'var_param_data_type'=> isset($request->service_var_param_data_type[$key][$service_key][$service_param_key]) ? $request->service_var_param_data_type[$key][$service_key][$service_param_key] : null,                       
                              'var_param_value_type'=> isset($request->service_var_param_value_type[$key][$service_key][$service_param_key]) ? $request->service_var_param_value_type[$key][$service_key][$service_param_key] : null,                       
                              'var_param_value'=> isset($request->service_var_param_value[$key][$service_key][$service_param_key]) ? $request->service_var_param_value[$key][$service_key][$service_param_key] : null,                       
                          ]);
                     }
                  }


             }
          }


           //$resource_id = $request->resource_id[$key];
           if(isset($request->resource_id[$key])){
             foreach($request->resource_id[$key] as $key1=>$value1){
                $quoteItemDetails = QuoteItemDetails::where('id', $request->quote_item_detail_key[$key][$key1])->update([
                   // 'quote_id'=> $quotation->id,
                    //'quote_item_id'=> $quoteItem->id,
                    'resource_id'=> $request->resource_id[$key][$key1],
                    'resource_price'=> $request->resource_price[$key][$key1], 
                    'resource_unit'=> $request->resource_unit[$key][$key1], 
                    'resource_duration'=> $request->resource_duration[$key][$key1], 
                    'item_detail_total'=> $request->item_detail_total[$key][$key1],
                ]);

                if(isset($request->resource_var_name[$key][$key1])){
                    foreach($request->resource_var_name[$key][$key1] as $key2=>$value2){
                      $quoteItemVariable = QuoteVariable::where('id', $request->resource_var_key[$key][$key1][$key2])->update([
                         // 'quote_id'=> $quotation->id,
                          'quote_item_type'=> 'inventory',
                          //'quote_item_id'=> $quoteItemDetails->id,
                          'var_name'=> isset($request->resource_var_name[$key][$key1][$key2]) ? $request->resource_var_name[$key][$key1][$key2] : null,
                          'item_var_total'=> isset($request->item_var_total[$key][$key1][$key2]) ? $request->item_var_total[$key][$key1][$key2] : null,        
                      ]);
                      if(isset($request->resource_var_param_name[$key][$key1][$key2])){
                          foreach($request->resource_var_param_name[$key][$key1][$key2] as $key3=>$value3){
                            $quoteVarParam = QuoteVariableParameter::where('id', $request->resource_var_param_key[$key][$key1][$key2][$key3])->update([
                                //'quote_id'=> $quotation->id,
                                //'quote_item_type'=> 'inventory',
                                //'quote_item_var_id'=> $quoteItemVariable->id,
                                'var_param_name'=> isset($request->resource_var_param_name[$key][$key1][$key2][$key3]) ? $request->resource_var_param_name[$key][$key1][$key2][$key3] : null,
                                'var_param_data_type'=> isset($request->resource_var_param_data_type[$key][$key1][$key2][$key3]) ? $request->resource_var_param_data_type[$key][$key1][$key2][$key3] : null,
                                'var_param_value_type'=> isset($request->resource_var_param_value_type[$key][$key1][$key2][$key3]) ? $request->resource_var_param_value_type[$key][$key1][$key2][$key3] : null,
                                'var_param_value'=> isset($request->resource_var_param_value[$key][$key1][$key2][$key3]) ? $request->resource_var_param_value[$key][$key1][$key2][$key3] : null,
                            ]);
                          }
                      }


                    }
                }
             }

           }

        }
      }


      $item_id = $request->item_id;
      if($item_id){
        foreach ($item_id as $key => $value) {
          //dd($request->resource_price[0][0]);
          //dd($request->line1[$key][0]);
           if($request->address_selected[$key][0] == 'new_address'){
              $new_address = ServiceLocationAddress::create([
                  'type' => 'customer',
                  'parent_id' => $quote->customer_id,
                  'shipAddr_line1' => isset($request->line1[$key][0]) ? $request->line1[$key][0] : null,
                  'shipAddr_line2' => isset($request->line2[$key][0]) ? $request->line2[$key][0] : null,
                  'shipAddr_city' => isset($request->city[$key][0]) ? $request->city[$key][0] : null,
                  'shipAddr_country' => isset($request->country[$key][0]) ? $request->country[$key][0] : null,
                  'shipAddr_countrySubDivisionCode' => isset($request->subdivisioncode[$key][0]) ? $request->subdivisioncode[$key][0] : null,
                  'shipAddr_postalCode' => isset($request->postalcode[$key][0]) ? $request->postalcode[$key][0] : null,
              ]);
              $service_location_address_id = $new_address->id;
           }
           else{
              $service_location_address_id = $request->address_selected[$key][0];
           }

           $quoteItem = QuoteItem::create([
                  'quote_id'=> $quote->id,
                  'item_id'=> $request->item_id[$key][0],
                  'item_type'=> $request->item_type[$key][0], 
                  'service_location_address_id'=> $service_location_address_id, 
                  'item_total'=> isset($request->item_total[$key][0]) ? $request->item_total[$key][0] : null, 
                  'var_total'=> isset($request->var_total[$key][0]) ? $request->var_total[$key][0] : null, 
                  'gross_total'=> isset($request->gross_total[$key][0]) ? $request->gross_total[$key][0] : null, 
           ]);

           //$service_variable = $request->service_var_name[$key];
           if(isset($request->service_var_name[$key])){
             foreach ($request->service_var_name[$key] as $service_key => $service_value) {
               $quoteItemVariable = QuoteVariable::create([
                      'quote_id'=> $quote->id,
                      'quote_item_type'=> 'service',
                      'quote_item_id'=> $quoteItem->id,
                      'var_name'=> isset($request->service_var_name[$key][$service_key]) ? $request->service_var_name[$key][$service_key] : null,     
                      'item_var_total'=> isset($request->service_var_total[$key][$service_key]) ? $request->service_var_total[$key][$service_key] : null,     

                  ]);

                  if(isset($request->service_var_param_name[$key][$service_key])){
                     foreach ($request->service_var_param_name[$key][$service_key] as $service_param_key => $service_param_value) {
                       $quote_var_param = QuoteVariableParameter::create([
                              'quote_id'=> $quote->id,
                              //'quote_item_type'=> 'service',
                              'quote_item_var_id'=> $quoteItemVariable->id,
                              'var_param_name'=> isset($request->service_var_param_name[$key][$service_key][$service_param_key]) ? $request->service_var_param_name[$key][$service_key][$service_param_key] : null,                       
                              'var_param_data_type'=> isset($request->service_var_param_data_type[$key][$service_key][$service_param_key]) ? $request->service_var_param_data_type[$key][$service_key][$service_param_key] : null,                       
                              'var_param_value_type'=> isset($request->service_var_param_value_type[$key][$service_key][$service_param_key]) ? $request->service_var_param_value_type[$key][$service_key][$service_param_key] : null,                       
                              'var_param_value'=> isset($request->service_var_param_value[$key][$service_key][$service_param_key]) ? $request->service_var_param_value[$key][$service_key][$service_param_key] : null,                       
                          ]);
                     }
                  }

             }
          }


           //$resource_id = $request->resource_id[$key];
           if(isset($request->resource_id[$key])){
             foreach($request->resource_id[$key] as $key1=>$value1){
                $quoteItemDetails = QuoteItemDetails::create([
                    'quote_id'=> $quote->id,
                    'quote_item_id'=> $quoteItem->id,
                    'resource_id'=> $request->resource_id[$key][$key1],
                    'resource_price'=> $request->resource_price[$key][$key1], 
                    'resource_unit'=> $request->resource_unit[$key][$key1], 
                    'resource_duration'=> $request->resource_duration[$key][$key1], 
                    'item_detail_total'=> $request->item_detail_total[$key][$key1],
                ]);

                if(isset($request->resource_var_name[$key][$key1])){
                    foreach($request->resource_var_name[$key][$key1] as $key2=>$value2){
                      $quoteItemVariable = QuoteVariable::create([
                          'quote_id'=> $quote->id,
                          'quote_item_type'=> 'inventory',
                          'quote_item_id'=> $quoteItemDetails->id,
                          'var_name'=> isset($request->resource_var_name[$key][$key1][$key2]) ? $request->resource_var_name[$key][$key1][$key2] : null,
                          'item_var_total'=> isset($request->item_var_total[$key][$key1][$key2]) ? $request->item_var_total[$key][$key1][$key2] : null,                                
                      ]);
                      if(isset($request->resource_var_param_name[$key][$key1][$key2])){
                          foreach($request->resource_var_param_name[$key][$key1][$key2] as $key3=>$value3){
                            $quoteVarParam = QuoteVariableParameter::create([
                                'quote_id'=> $quote->id,
                                //'quote_item_type'=> 'inventory',
                                'quote_item_var_id'=> $quoteItemVariable->id,
                                'var_param_name'=> isset($request->resource_var_param_name[$key][$key1][$key2][$key3]) ? $request->resource_var_param_name[$key][$key1][$key2][$key3] : null,
                                'var_param_data_type'=> isset($request->resource_var_param_data_type[$key][$key1][$key2][$key3]) ? $request->resource_var_param_data_type[$key][$key1][$key2][$key3] : null,
                                'var_param_value_type'=> isset($request->resource_var_param_value_type[$key][$key1][$key2][$key3]) ? $request->resource_var_param_value_type[$key][$key1][$key2][$key3] : null,
                                'var_param_value'=> isset($request->resource_var_param_value[$key][$key1][$key2][$key3]) ? $request->resource_var_param_value[$key][$key1][$key2][$key3] : null,
                            ]);
                          }
                      }

                    }
                }
             }

           }

        } 
      }

      session()->flash('success', 'Successfully Updated');
      return back();
    }

    
    public function destroy($id){
        return back();
    }

    public function getService(Request $request){
        $subcat_id = $request->value;
        $service = Product::where('subcat_id', $subcat_id)->get();

        $output = '';

         if(!$service->isEmpty()){
            $output .= '<option class="form-select-placeholder"></option>';
            foreach($service as $row)
             {
              $output .= '<option value="'.$row->id.'">'.$row->product_name.'</option>';
             }
        }
        else
            $output .= '<option value="" disabled>No Services</option>';

         echo $output;

    }

    public function getServiceResourceInfo(Request $request){
      $service_id = $request->value;
      $counter = $request->counter;
      $item_no = $request->item_no;

      $service_name = Product::where('id', $service_id)->first();
      $service_resources = ProductInventory::where('product_id', $service_id)->get();

      $cat = Category::where('id', $service_name->cat_id)->first();
      $subcat = Subcategory::where('id', $service_name->subcat_id)->first();

      $tbl = '';

      $tbl.= '<div class="card single_service_rental_card whole_unit_card">

                          <div class="card-body">

                            <div class="single_service_rental_badge service_badge">
                              <p>Item No: <span class="item_no">'.($item_no+1).' </span> - <span>Service</span></p>
                            </div>

                            <div class="card main_service_card_section">
                              <div class="service_name_main_variable_section">
                                <div class="service_name_main_variable_table">
                                  <div class="service_name_main_variable_row service_name_main_variable_header">
                                    <div class="service_name_main_variable_col">
                                      <p>Service Name</p>
                                    </div>
                                    <div class="service_name_main_variable_col">
                                      <p>Service Variable Section</p>
                                    </div>
                                  </div>
                                  <div class="service_name_main_variable_row">
                                    <div class="service_name_main_variable_col">
                                    <input type="hidden" name="item_type['.$counter.'][]" value="service">
                                    <input type="hidden" class="service_id_input" name="item_id['.$counter.'][]" value="' .$service_id. '">
                                      <p>'.$service_name->product_name.'</p>
                                    </div>
                                    <div class="service_name_main_variable_col">
                                      <div class="card single_service_under_variables_section">


                                        <div class="card-body variable_card_table">
                                          <div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <p>Name</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <p>Parameter</p>
                                            </div>
                                            
                                            <div class="variable_card_section_col">
                                              <p>Total</p>
                                            </div>
                                          </div>';

                                        $variables = VariableSection::where('type', 'service')->where('parent_id', $service_id)->get();
                                        if(!$variables->isEmpty()){
                                        $service_var_param_counter = 0;
                                        foreach($variables as $variable){
                                        $var = Variable::where('id', $variable->var_id)->first(); 
    $tbl.=                               '<div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <input type="hidden" name="service_var_name['.$counter.'][]" value="'.$var->variable_name.'">
                                              <p>'.$var->variable_name.'</p>
                                            </div>
                                            <div class="variable_card_section_col">';

                                            $var_params = VariableParameter::where('var_id', $variable->var_id)->get();
                                            foreach($var_params as $var_param){
                                              
    $tbl.=                                   '<div class="Parameter_with_value">
                                                <p>'.$var_param->var_param_name.'</p>
                                                <input type="hidden" name="service_var_param_name['.$counter.']['.$service_var_param_counter.'][]" value="'.$var_param->var_param_name.'">';
                                          
                                                      if($var_param->var_param_data_type == 'decimal')
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="service_var_param_value_type['.$counter.']['.$service_var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="number" class="form-control form_input_field multiply" step="0.01" name="service_var_param_value['.$counter.']['.$service_var_param_counter.'][]" value="'.$var_param->var_param_value.'">
                                                </p>';
                                                      elseif($var_param->var_param_data_type == 'integer')
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="service_var_param_value_type['.$counter.']['.$service_var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="number" class="form-control form_input_field multiply" name="service_var_param_value['.$counter.']['.$service_var_param_counter.'][]" value="'.$var_param->var_param_value.'">
                                                </p>';                                               
                                                      elseif($var_param->var_param_data_type == 'datetime')
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="service_var_param_value_type['.$counter.']['.$service_var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="text" class="form-control form_input_field datepicker" name="service_var_param_value['.$counter.']['.$service_var_param_counter.'][]" value="'.$var_param->var_param_value.'">
                                                </p>';
                                                      elseif($var_param->var_param_data_type == 'percentage')
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="service_var_param_value_type['.$counter.']['.$service_var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="text" class="form-control form_input_field percentage" name="service_var_param_value['.$counter.']['.$service_var_param_counter.'][]" value="'.$var_param->var_param_value.'" class="">
                                                </p>';                                                  
                                                      else
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="service_var_param_value_type['.$counter.']['.$service_var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="text" class="form-control form_input_field" name="service_var_param_value['.$counter.']['.$service_var_param_counter.'][]" value="'.$var_param->var_param_value.'">
                                                </p>';
                                           

      $tbl.=                                 '</div>';

                                            }


      $tbl.=                               '</div>
                                            
                                            <div class="variable_card_section_col">
                                              <p class="unit_var_total">$0</p>
                                            </div>
                                          </div>';
                                        $service_var_param_counter++;
                                        }
                                      }
                                      else{
      $tbl.=                              '<div class="variable_card_section_row">
                                              
                                                <p>No vairiables</p>
                                             
                                          </div>';
                                      }

     $tbl.=                             '</div>


                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>';

                            $resource_var_counter=0;
                            foreach ($service_resources as $rowdata) {
                            $resource_info = Inventory::where('id', $rowdata->inventory_id)->first();
    $tbl.=                   '<div class="service_item_card_main_section_table_wrapper">
                              <div class="service_item_card_main_section">
                                <div class="service_item_card_table_wrapper">
                                  <div class="service_item_card_table">
                                    <div class="service_item_card_row service_item_card_header">
                                      <div class="service_item_card_col">
                                        <p>Item Name</p>
                                      </div>
                                      <div class="service_item_card_col">
                                        <p>Price</p>
                                      </div>
                                      <div class="service_item_card_col">
                                        <p>Total Unit</p>
                                      </div>
                                      <div class="service_item_card_col">
                                        <p>Total Duration</p>
                                      </div>
                                      <div class="service_item_card_col">
                                        <p>Total Unit Cost</p>
                                      </div>
                                    </div>
                                    <div class="service_item_card_row resource_cal_row">
                                      <div class="service_item_card_col">
                                      <input type="hidden" name="resource_id[' .$counter. '][]" value="' .$resource_info->id. '">
                                        <p>'.$resource_info->resource_name.'</p>
                                      </div>
                                      <div class="service_item_card_col">
                                         
                                            
                                          <div class="custom_select2_col">  
                                            <div class="form-group custom-form-select">
                                            <select class="custom-select cat_service colInput price_select select2Price" name="resource_price[' .$counter. '][]" required>
                                            <option class="form-select-placeholder"></option>
                                            <option value="' .$resource_info->price_per_hour_service. '">$ ' .$resource_info->price_per_hour_service. ' /hour</option>
                                            <option value="' .$resource_info->price_per_day_service. '">$ ' .$resource_info->price_per_day_service. ' /day</option>
                                            <option value="' .$resource_info->price_per_week_service. '">$ ' .$resource_info->price_per_week_service. ' /week</option>
                                            <option value="' .$resource_info->price_per_month_service. '">$ ' .$resource_info->price_per_month_service. ' /month</option>
                                            </select>
                                            <div class="form-element-bar">
                                            </div>
                                          </div>
                                          </div>

                                      </div>
                                      <div class="service_item_card_col">
                                        <div class="form-group custom-form-select bmd-form-group">      
                                          <input type="number" class="form-control form_input_field colInput" name="resource_unit['.$counter.'][]" value="">
                                        </div>
                                      </div>
                                      <div class="service_item_card_col">
                                        <div class="form-group custom-form-select bmd-form-group">      
                                          <input type="number" class="form-control form_input_field colInput" name="resource_duration['.$counter.'][]" value="">
                                        </div>
                                      </div>
                                      <div class="service_item_card_col">
                                        <p class="colTotal">0</p>
                                        <input type="hidden" name="item_detail_total['.$counter.'][]" class="colTotal_hidden_input">
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="card single_service_under_variables_section item_variables">
                                  <div class="card-body resource_variable_card_body">
                                    <div class="section-title text-center">
                                      <h4>Resource Variable section</h4>
                                    </div>




                                    <div class="card single_service_under_variables_section variable_card_wrapper">
                                        <div class="card-body variable_card_table">
                                          <div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <p>Name</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <p>Parameter</p>
                                            </div>
                                            <div class="variable_card_section_col">
                                              <p>Total</p>
                                            </div>
                                          </div>';

                                        $variables = VariableSection::where('type', 'inventory')->where('parent_id', $resource_info->id)->get();
                                        if(!$variables->isEmpty()){
                                          $var_param_counter=0;
                                          foreach($variables as $variable){  
                                          $var = Variable::where('id', $variable->var_id)->first();
                                          
    $tbl.=                               '<div class="variable_card_section_row">
                                            <div class="variable_card_section_col">
                                              <input type="hidden" name="resource_var_name['.$counter.']['.$resource_var_counter.'][]" value="'.$var->variable_name.'">
                                              <p>'.$var->variable_name.'</p>
                                            </div>
                                            <div class="variable_card_section_col">';

                                            $var_params = VariableParameter::where('var_id', $variable->var_id)->get();
                                                foreach($var_params as $var_param){

    $tbl.=                                    '<div class="Parameter_with_value">
                                                <p>'.$var_param->var_param_name.'
                                                <input type="hidden" name="resource_var_param_name['.$counter.']['.$resource_var_counter.']['.$var_param_counter.'][]" value="'.$var_param->var_param_name.'"></p>';

                                              
                                                  if($var_param->var_param_data_type == 'decimal')
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="resource_var_param_value_type['.$counter.']['.$resource_var_counter.']['.$var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="number" class="form-control form_input_field multiply" step="0.01" name="resource_var_param_value['.$counter.']['.$resource_var_counter.']['.$var_param_counter.'][]" value="'.$var_param->var_param_value.'">
                                                </p>';
                                                      elseif($var_param->var_param_data_type == 'integer')
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="resource_var_param_value_type['.$counter.']['.$resource_var_counter.']['.$var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="number" class="form-control form_input_field multiply" name="resource_var_param_value['.$counter.']['.$resource_var_counter.']['.$var_param_counter.'][]" value="'.$var_param->var_param_value.'">
                                                </p>';                                               
                                                      elseif($var_param->var_param_data_type == 'datetime')
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="resource_var_param_value_type['.$counter.']['.$resource_var_counter.']['.$var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="text" class="form-control form_input_field datepicker" name="resource_var_param_value['.$counter.']['.$resource_var_counter.']['.$var_param_counter.'][]" value="'.$var_param->var_param_value.'">
                                                </p>';
                                                      elseif($var_param->var_param_data_type == 'percentage')
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="resource_var_param_value_type['.$counter.']['.$resource_var_counter.']['.$var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="text" class="form-control form_input_field percentage" name="resource_var_param_value['.$counter.']['.$resource_var_counter.']['.$var_param_counter.'][]" value="'.$var_param->var_param_value.'">
                                                </p>';
                                                      else
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="resource_var_param_value_type['.$counter.']['.$resource_var_counter.']['.$var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="text" class="form-control form_input_field" name="resource_var_param_value['.$counter.']['.$resource_var_counter.']['.$var_param_counter.'][]" value="'.$var_param->var_param_value.'">
                                                </p>';
                                              
      $tbl.=                                 '</div>';
                                            }

                                              
      $tbl.=                              '</div>
                                            
                                            <div class="variable_card_section_col">
                                              <p class="unit_var_total">$0</p>
                                            </div>
                                          </div>';
                                          
                                          $var_param_counter++;
                                          }
                                        }
                                        else{
    $tbl.=                                '<div>
                                            <p>No variables</p>
                                          </div>';
                                        }

    $tbl.=                              '</div>
                                      </div>




                                  </div>
                                </div>
                              </div>
                              </div>';
                            $resource_var_counter++;
                            }

    $tbl.=                 '</div>';

      $tbl .=     '<div class="card service_loaction_net_total_section">
                    <div class="card-body service_loaction_net_tota has_no_address_inside">';

//  Services Service Location section

      $tbl .=       '<div class="service_location_selection">';

      $tbl .=           '<div class="service_location">
                        <input type="hidden" class="address_selected" name="address_selected['.$counter.'][]" value="">
                        <input type="hidden" class="new_address_hidden_field line1" name="line1['.$counter.'][]" value="">
                        <input type="hidden" class="new_address_hidden_field line2" name="line2['.$counter.'][]" value="">
                        <input type="hidden" class="new_address_hidden_field city" name="city['.$counter.'][]" value="">
                        <input type="hidden" class="new_address_hidden_field country" name="country['.$counter.'][]" value="">
                        <input type="hidden" class="new_address_hidden_field subdivisioncode" name="subdivisioncode['.$counter.'][]" value="">
                        <input type="hidden" class="new_address_hidden_field postalcode" name="postalcode['.$counter.'][]" value="">

                        <input type="hidden" class="new_address_hidden_field lease_name" name="lease_name['.$counter.'][]" value="">
                        <input type="hidden" class="new_address_hidden_field rig_number" name="rig_number['.$counter.'][]" value="">
                        <input type="hidden" class="new_address_hidden_field afe_number" name="afe_number['.$counter.'][]" value="">
                        <input type="hidden" class="new_address_hidden_field site_contact_no" name="site_contact_no['.$counter.'][]" value="">
                        <input type="hidden" class="new_address_hidden_field rig_contact_email" name="rig_contact_email['.$counter.'][]" value="">
                        <input type="hidden" class="new_address_hidden_field company_man_contact_name" name="company_man_contact_name['.$counter.'][]" value="">                        

                        <button class="btn btn-info btn-lg btn-round quote_address_btn" type="button" counter="'.$counter.'">Service Location</button>
                        <div id="myModal'.$counter.'" class="modal addressModal" style="display: none;"></div>
                        </div>';
      $tbl .=           '<div class="card selected_address_card" style="display:none;">
                          <div class="card-body">
                            <div id="address_viewer" class="">
                              <h6> Selected Address </h6>
                              <hr class="selected-head-hr selected_head_card_hr">
                              <div class="row ex_add_p_row ex_add_p_row_mod">
                                <div class="col-md-12">
                                  <div class="row inline_rows">
                                    <div class="col-md-3 col-sm-3 col-3"><label> Line-1: </label></div>
                                    <div class="col-md-9 col-sm-9 col-9"><p class=""></p></div>
                                  </div>
                                  <div class="row inline_rows input_wrapper">
                                    <div class="col-md-3 col-sm-3 col-3"><label> Line-2: </label></div>
                                    <div class="col-md-9 col-sm-9 col-9"><p class=""></p></div>
                                  </div>
                                  <div class="row block_rows">
                                    <div class="col-md-4 col-sm-4 col-6 input_wrapper">
                                      <label> Sub Division Code: </label>
                                      <p> </p>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-6 input_wrapper">
                                      <label> Postal Code: </label>
                                      <p class=""> </p>
                                    </div>
                                    <div class="col-md-4 col-sm-4 input_wrapper col-6-skipped">
                                      <label> City: </label>
                                      <p class=""> </p>
                                    </div>                                
                                  </div>
                                  <div class="row block_rows">
                                    <div class="col-md-4 col-sm-4 input_wrapper col-6-skipped">
                                      <label> Country: </label>
                                      <p class=""> </p>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-6 input_wrapper">
                                      <label> Lease Name: </label>
                                      <p></p>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-6 input_wrapper">
                                      <label> Rig Number: </label>
                                      <p></p>
                                    </div>                                                               
                                  </div>
                                  <div class="row block_rows">
                                    <div class="col-md-6 col-sm-6 col-6 input_wrapper">
                                      <label> AFE Number: </label>
                                      <p></p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6 input_wrapper">
                                      <label> Site Contact No.: </label>
                                      <p class=""></p>
                                    </div>
                                  </div>
                                  <div class="row block_rows">
                                    <div class="col-md-6 col-sm-6 col-6 input_wrapper">
                                      <label> Rig/Contact Email: </label>
                                      <p></p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6 input_wrapper">
                                      <label> Company Man Contact Name: </label>
                                      <p></p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                         </div>';

      $tbl .=       '</div>';              

      $tbl .=         '<div class="net_total">';
     
      $tbl .=          '<div class="net_total_single">
                          <p class="tr_outside_total_unit_cost total_unit_cost">Total Unit Cost<span>$0</span></p>
                          <input type="hidden" class="tr_outside_total_unit_cost_input_hidden total_unit_cost_input_hidden" name="item_total['.$counter.'][]">

                        </div>';
     
      $tbl .=          '<div class="net_total_single">
                          <p class="tr_outside_var_cost_1 total_var_cost">Total Variable Cost <span>$0</span></p>
                          <input type="hidden" name="var_total['.$counter.'][]" class="final_total_variable_hidden total_var_cost_input_hidden" value="">
                        </div>';
    
      $tbl .=           '<hr style="width:40%;margin-right:0;">';
      
      $tbl .=           '<div class="net_total_single tr_outside_gross_cost">
                          <p class="gross_total">Gross Total<span>$0</span></p>
                          <input type="hidden" name="gross_total['.$counter.'][]" class="gross_total_hidden_input">
                        </div>';
      
      $tbl .=        '</div>';
                    
      $tbl .=        '</div>
                  </div>';

      $tbl.=             '<div class="cross">
                            <a href="#" class="btn btn-danger btn-round">
                              <i class="material-icons">close</i>
                            </a>
                          </div>

                          </div>
                        </div>';

      echo $tbl;

    }



    public function getRentalResourceInfo(Request $request){
       $inventory_id = $request->value;
        $counter = $request->counter;
        $item_no = $request->item_no;

        $resource_info = Inventory::where('id', $inventory_id)->first();

        $tbl = '';

        $tbl.= '<div class="card single_service_rental_card whole_unit_card">

                          <div class="card-body">

                            <div class="single_service_rental_badge rental_badge">
                            <input type="hidden" name="item_type['.$counter.'][]" value="rental">
                            <input type="hidden" name="item_id['.$counter.'][]" value="' .$inventory_id. '">
                              <p>Item No: <span class="item_no">'.($item_no+1).' </span> - <span>Rental</span></p>
                            </div>

                            <div class="card rental_card_body">
                              <div class="rental_card_body_table">
                                <div class="rental_card_body_row rental_card_body_header">
                                  <div class="rental_column">
                                    <p>Item Name</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>Price</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>Total Unit</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>Total Duration</p>
                                  </div>
                                  <div class="rental_column">
                                    <p>Total Unit Cost</p>
                                  </div>
                                </div>
                                <div class="rental_card_body_row resource_cal_row">
                                  <div class="rental_column">
                                    <input type="hidden" class="rental_id_input" name="resource_id['.$counter.'][]" value="' .$inventory_id. '">
                                    <p>' .$resource_info->resource_name. '</p>
                                  </div>
                                  <div class="rental_column">
                                     
                                      <div class="custom_select2_col">  
                                            <div class="form-group custom-form-select">
                                            <select class="custom-select cat_service colInput price_select select2Price" name="resource_price['.$counter.'][]">
                                            <option class="form-select-placeholder"></option>
                                            <option value="' .$resource_info->price_per_hour_rental. '">$ ' .$resource_info->price_per_hour_rental. ' /hour</option>
                                            <option value="' .$resource_info->price_per_day_rental. '">$ ' .$resource_info->price_per_day_rental. ' /day</option>
                                            <option value="' .$resource_info->price_per_week_rental. '">$ ' .$resource_info->price_per_week_rental. ' /week</option>
                                            <option value="' .$resource_info->price_per_month_rental. '">$ ' .$resource_info->price_per_month_rental. ' /month</option>
                                            </select>
                                            <div class="form-element-bar">
                                            </div>
                                          </div>
                                      </div>

                                  </div>
                                  <div class="rental_column">
                                    <div class="form-group custom-form-select bmd-form-group">      
                                      <input type="number" class="form-control form_input_field colInput" name="resource_unit['.$counter.'][]" value="">
                                    </div>
                                  </div>
                                  <div class="rental_column">
                                    <div class="form-group custom-form-select bmd-form-group">      
                                      <input type="number" class="form-control form_input_field colInput" name="resource_duration['.$counter.'][]" value="">
                                    </div>
                                  </div>
                                  <div class="rental_column">
                                    <p class="colTotal">0</p>
                                    <input type="hidden" name="item_detail_total['.$counter.'][]" class="colTotal_hidden_input">
                                  </div>
                                </div>
                              </div>
                            </div>



                            <div class="card single_service_under_variables_section item_variables rental_variable_card_section">
                              <div class="card-body">
                                <div class="section-title text-center">
                                  <h4>Resource Variable section</h4>
                                </div>
                                
                                <div class="card single_service_under_variables_section variable_card_wrapper">
                                  <div class="card-body variable_card_table">
                                    <div class="variable_card_section_row">
                                      <div class="variable_card_section_col">
                                        <p>Name</p>
                                      </div>
                                      <div class="variable_card_section_col">
                                        <p>Parameter</p>
                                      </div>
                                      <div class="variable_card_section_col">
                                        <p>Total</p>
                                      </div>
                                    </div>';

                                $variables = VariableSection::where('type', 'inventory')->where('parent_id', $resource_info->id)->get();
                                if(!$variables->isEmpty()){
                                $var_param_counter = 0;
                                    foreach($variables as $variable){
                                    $var = Variable::where('id', $variable->var_id)->first();
    $tbl.=                          '<div class="variable_card_section_row">
                                      <div class="variable_card_section_col">
                                        <input type="hidden" name="resource_var_name['.$counter.'][0][]" value="'.$var->variable_name.'">
                                        <p>'.$var->variable_name.'</p>
                                      </div>
                                      <div class="variable_card_section_col">';
                                      $var_params = VariableParameter::where('var_id', $var->id)->get();
                                      foreach($var_params as $var_param){
    $tbl.=                              '<div class="Parameter_with_value">
                                            <p>'.$var_param->var_param_name.'
                                            <input type="hidden" name="resource_var_param_name['.$counter.'][0]['.$var_param_counter.'][]" value="'.$var_param->var_param_name.'"></p>';

                                               if($var_param->var_param_data_type == 'decimal')
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="resource_var_param_value_type['.$counter.'][0]['.$var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="number" class="form-control form_input_field multiply" step="0.01" name="resource_var_param_value['.$counter.'][0]['.$var_param_counter.'][]" value="'.$var_param->var_param_value.'">
                                                </p>';
                                                      elseif($var_param->var_param_data_type == 'integer')
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="resource_var_param_value_type['.$counter.'][0]['.$var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="number" class="form-control form_input_field multiply" name="resource_var_param_value['.$counter.'][0]['.$var_param_counter.'][]" value="'.$var_param->var_param_value.'">
                                                </p>';                                               
                                                      elseif($var_param->var_param_data_type == 'datetime')
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="resource_var_param_value_type['.$counter.'][0]['.$var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="text" class="form-control form_input_field datepicker" name="resource_var_param_value['.$counter.'][0]['.$var_param_counter.'][]" value="'.$var_param->var_param_value.'">
                                                </p>';
                                                     elseif($var_param->var_param_data_type == 'percentage')
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="resource_var_param_value_type['.$counter.'][0]['.$var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="text" class="form-control form_input_field percentage" name="resource_var_param_value['.$counter.'][0]['.$var_param_counter.'][]" value="'.$var_param->var_param_value.'">
                                                </p>';
                                                      else
      $tbl .=                                   '<p class="form-group bmd-form-group">
                                                    <input type="hidden" name="resource_var_param_value_type['.$counter.'][0]['.$var_param_counter.'][]" value="'.$var_param->var_param_value_type.'">
                                                      <input type="text" class="form-control form_input_field" value="'.$var_param->var_param_value.'">
                                                </p>';


    $tbl.=                              '</div>';
                                      }
                                        
    $tbl.=                             '</div>
                                      
                                      <div class="variable_card_section_col">
                                        <p class="unit_var_total">$0</p>
                                      </div>
                                    </div>';

                                    $var_param_counter++;
                                    }
                                }
                                else{
    $tbl.=                         '<div class="variable_card_section_row">
                                      <p>No variables</p>
                                    </div>';     
                                }


    $tbl.=                        '</div>
                                </div>

                              </div>
                            </div>';



//  Rental Service Loation section

      $tbl .=   '<div class="card service_loaction_net_total_section">
                  <div class="card-body service_loaction_net_tota has_no_address_inside">';

      $tbl .=     '<div class="service_location_selection">';
      $tbl .=           '<div class="service_location">
                      <input type="hidden" class="address_selected" name="address_selected['.$counter.'][]" value="">
                      <input type="hidden" class="new_address_hidden_field line1" name="line1['.$counter.'][]" value="">
                      <input type="hidden" class="new_address_hidden_field line2" name="line2['.$counter.'][]" value="">
                      <input type="hidden" class="new_address_hidden_field city" name="city['.$counter.'][]" value="">
                      <input type="hidden" class="new_address_hidden_field country" name="country['.$counter.'][]" value="">
                      <input type="hidden" class="new_address_hidden_field subdivisioncode" name="subdivisioncode['.$counter.'][]" value="">
                      <input type="hidden" class="new_address_hidden_field postalcode" name="postalcode
                      ['.$counter.'][]" value="">

                      <input type="hidden" class="new_address_hidden_field lease_name" name="lease_name['.$counter.'][]" value="">
                      <input type="hidden" class="new_address_hidden_field rig_number" name="rig_number['.$counter.'][]" value="">
                      <input type="hidden" class="new_address_hidden_field afe_number" name="afe_number['.$counter.'][]" value="">
                      <input type="hidden" class="new_address_hidden_field site_contact_no" name="site_contact_no['.$counter.'][]" value="">
                      <input type="hidden" class="new_address_hidden_field rig_contact_email" name="rig_contact_email['.$counter.'][]" value="">
                      <input type="hidden" class="new_address_hidden_field company_man_contact_name" name="company_man_contact_name['.$counter.'][]" value="">



                      <button class="btn btn-info btn-lg btn-round quote_address_btn" type="button" counter="'.$counter.'">Service Location</button>
                      <div id="myModal'.$counter.'" class="modal addressModal" style="display: none;"></div>
                        </div>';
      $tbl .=           '<div class="card selected_address_card" style="display:none;">
                          <div class="card-body">
                            <div id="address_viewer" class="">
                              <h6> Selected Address </h6>
                              <hr class="selected-head-hr selected_head_card_hr">
                              <div class="row ex_add_p_row ex_add_p_row_mod">
                                <div class="col-md-12">
                                  <div class="row inline_rows">
                                    <div class="col-md-3 col-sm-3 col-3"><label> Line-1: </label></div>
                                    <div class="col-md-9 col-sm-9 col-9"><p class=""></p></div>
                                  </div>
                                  <div class="row inline_rows input_wrapper">
                                    <div class="col-md-3 col-sm-3 col-3"><label> Line-2: </label></div>
                                    <div class="col-md-9 col-sm-9 col-9"><p class=""></p></div>
                                  </div>
                                  <div class="row block_rows">
                                    <div class="col-md-4 col-sm-4 col-6 input_wrapper">
                                      <label> Sub Division Code: </label>
                                      <p> </p>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-6 input_wrapper">
                                      <label> Postal Code: </label>
                                      <p class=""> </p>
                                    </div>
                                    <div class="col-md-4 col-sm-4 input_wrapper col-6-skipped">
                                      <label> City: </label>
                                      <p class=""> </p>
                                    </div>                                
                                  </div>
                                  <div class="row block_rows">
                                    <div class="col-md-4 col-sm-4 input_wrapper col-6-skipped">
                                      <label> Country: </label>
                                      <p class=""> </p>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-6 input_wrapper">
                                      <label> Lease Name: </label>
                                      <p></p>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-6 input_wrapper">
                                      <label> Rig Number: </label>
                                      <p></p>
                                    </div>                                                               
                                  </div>
                                  <div class="row block_rows">
                                    <div class="col-md-6 col-sm-6 col-6 input_wrapper">
                                      <label> AFE Number: </label>
                                      <p></p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6 input_wrapper">
                                      <label> Site Contact No.: </label>
                                      <p class=""></p>
                                    </div>
                                  </div>
                                  <div class="row block_rows">
                                    <div class="col-md-6 col-sm-6 col-6 input_wrapper">
                                      <label> Rig/Contact Email: </label>
                                      <p></p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-6 input_wrapper">
                                      <label> Company Man Contact Name: </label>
                                      <p></p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>';

      $tbl .=     '</div>';                



      $tbl .=        '<div class="net_total">';
      $tbl .=          '<div class="net_total_single">
                        <p class="tr_outside_total_unit_cost total_unit_cost">Total Unit Cost<span>$0</span></p>
                        <input type="hidden" class="tr_outside_total_unit_cost_input_hidden total_unit_cost_input_hidden" name="item_total['.$counter.'][]">
                      </div>';
      $tbl .=         '<div class="net_total_single">
                        <p class="tr_outside_var_cost_1 total_var_cost">Total Variable Cost <span>$0</span></p>
                        <input type="hidden" name="var_total['.$counter.'][]" class="final_total_variable_hidden total_var_cost_input_hidden">
                      </div>';

      $tbl .=         '<hr style="width:40%;margin-right:0;">';
      $tbl .=         '<div class="net_total_single tr_outside_gross_cost">
                        <p class="gross_total">Gross Total <span>$0</span></p>
                        <input type="hidden" name="gross_total['.$counter.'][]" class="gross_total_hidden_input">
                      </div>';

      $tbl .=      '</div>';
      $tbl .=     '</div>
                </div>';

  $tbl.=                  '<div class="cross">
                            <a href="#" class="btn btn-danger btn-round">
                              <i class="material-icons">close</i>
                            </a>
                          </div>

                          </div>
                        </div>';

      echo $tbl;
    }

    

    public function getAddress(Request $request){
      $value = $request->value;
      $counter = $request->counter;
      $service_location_id = $request->service_location_id;
      $line1 = $request->line1;
      $line2 = $request->line2;
      $city = $request->city;
      $country = $request->country;
      $subdivisioncode = $request->subdivisioncode;
      $postalcode = $request->postalcode;

      $lease_name = $request->lease_name;
      $rig_number = $request->rig_number;
      $afe_number = $request->afe_number;
      $site_contact_no = $request->site_contact_no;
      $rig_contact_email = $request->rig_contact_email;
      $company_man_contact_name = $request->company_man_contact_name;

      $customer_default = Customer::where('id', $value)->first();
      $customer_other = ServiceLocationAddress::where('type', 'customer')->where('parent_id', $value)->get();

      $radio_selected_modal = '';
      if($service_location_id == "default")
        $radio_selected_modal = 'checked';

      $modal = '';
      $modal .= '<div class="card modal-content address_card">
                              <div class="card-header card-header-rose card-header-text" style="width:22%">
                                <div class="card-icon">
                                    <i class="material-icons">home</i>
                                </div>
                                  <h4 class="card-title">Address</h4>
                              </div>
                              <div class="card-body address_card_body">
                                <div class="row address_card_body_row">
                                  <div class="col-md-6 ex_address_col">
                                    <div class="card inner_card">
                                      <div class="card-body">
                                        <div class="row">
                                          <div class="col-md-2">
                                            <div class="card-icon icon-rose text-left">
                                              <i class="material-icons">home</i>
                                            </div>
                                          </div>
                                          <div class="col-md-7">
                                            <h6> Address </h6>
                                          </div>
                                        </div>
                                        <hr class="head-hr">
                                        <div class="row ex_add_p_row">
                                          <div class="col-md-12">
                                            <div class="row inline_rows">
                                              <div class="col-md-3"><label> Line-1: </label></div>
                                              <div class="col-md-9"><p class="take">'.$customer_default->shipAddr_line1. '</p></div>
                                            </div>
                                            <div class="row inline_rows input_wrapper">
                                              <div class="col-md-3"><label> Line-2: </label></div>
                                              <div class="col-md-9"><p class="take">'.$customer_default->shipAddr_line2. '</p></div>
                                            </div>
                                            <div class="row block_rows">
                                              <div class="col-md-6 input_wrapper">
                                                <label> Sub Division Code: </label>
                                                <p class="take"> '.$customer_default->shipAddr_countrySubDivisionCode.' </p>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <label> Postal Code: </label>
                                                <p class="take"> '.$customer_default->shipAddr_countryCode.' </p>
                                              </div>
                                            </div>
                                            <div class="row block_rows">
                                              <div class="col-md-6 input_wrapper">
                                                <label> City: </label>
                                                <p class="take"> '.$customer_default->shipAddr_city.' </p>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <label> Country: </label>
                                                <p class="take"> '.$customer_default->shipAddr_country.' </p>
                                              </div>
                                            </div>
                                            <div class="row block_rows">
                                              <div class="col-md-6 input_wrapper">
                                                <label> Lease Name: </label>
                                                <p class="take">' .$customer_default->lease_name. '</p>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <label> Rig Number: </label>
                                                <p class="take">' .$customer_default->rig_number. '</p>
                                              </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-6 input_wrapper">
                                                <label> AFE Number: </label>
                                                <p class="take">' .$customer_default->afe_number. '</p>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <label> Site Contact No.: </label>
                                                <p class="take">' .$customer_default->site_contact_no. '</p>
                                              </div>
                                            </div>
                                            <div class="row block_rows">
                                              <div class="col-md-6 input_wrapper">
                                                <label> Rig/Contact Email: </label>
                                                <p class="take">' .$customer_default->rig_contact_email. '</p>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <label> Company Man Contact Name: </label>
                                                <p class="take">' .$customer_default->company_man_contact_name. '</p>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <hr class="foot-hr">
                                        <div class="row">
                                          <div class="col-md-12">';
      $modal .=                             '<div class="form-check">
                                              <label class="form-check-label">
                                                <input type="hidden" name="address_selection['.$counter.'][]" value="0">
                                                <input class="form-check-input address_radio_input" type="radio" name="address_selection['.$counter.'][]" value="default" '.$radio_selected_modal.'>
                                                Select
                                                <span class="circle">
                                                  <span class="check"></span>
                                                </span>
                                              </label>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>';
                              //$radio_other_modal = '';
                              foreach($customer_other as $rowdata){
                              
                              if($rowdata->id == $service_location_id)
                                $radio_other_modal = 'checked';
                              else
                                $radio_other_modal = '';
      $modal .=                   '<div class="col-md-6 ex_address_col">
                                    <div class="card inner_card">
                                      <div class="card-body">
                                        <div class="row">
                                          <div class="col-md-2">
                                            <div class="card-icon icon-rose text-left">
                                              <i class="material-icons">home</i>
                                            </div>
                                          </div>
                                          <div class="col-md-7">
                                            <h6> Address </h6>
                                          </div>
                                        </div>
                                        <hr class="head-hr">
                                        <div class="row ex_add_p_row">
                                          <div class="col-md-12">
                                            <div class="row inline_rows">
                                              <div class="col-md-3"><label> Line-1: </label></div>
                                              <div class="col-md-9"><p class="take">'.$rowdata->shipAddr_line1. '</p></div>
                                            </div>
                                            <div class="row inline_rows input_wrapper">
                                              <div class="col-md-3"><label> Line-2: </label></div>
                                              <div class="col-md-9"><p class="take">'.$rowdata->shipAddr_line2. '</p></div>
                                            </div>
                                            <div class="row block_rows">
                                              <div class="col-md-6 input_wrapper">
                                                <label> Sub Division Code: </label>
                                                <p class="take"> '.$rowdata->shipAddr_countrySubDivisionCode.' </p>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <label> Postal Code: </label>
                                                <p class="take"> '.$rowdata->shipAddr_countryCode.' </p>
                                              </div>
                                            </div>
                                            <div class="row block_rows">
                                              <div class="col-md-6 input_wrapper">
                                                <label> City: </label>
                                                <p class="take"> '.$rowdata->shipAddr_city.' </p>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <label> Country: </label>
                                                <p class="take"> '.$rowdata->shipAddr_country.' </p>
                                              </div>
                                            </div>
                                            <div class="row block_rows">
                                              <div class="col-md-6 input_wrapper">
                                                <label> Lease Name: </label>
                                                <p class="take">' .$rowdata->lease_name. '</p>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <label> Rig Number: </label>
                                                <p class="take">' .$rowdata->rig_number. '</p>
                                              </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-6 input_wrapper">
                                                <label> AFE Number: </label>
                                                <p>'.$rowdata->afe_number. '</p>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <label> Site Contact No.: </label>
                                                <p class="take">' .$rowdata->site_contact_no.'</p>
                                              </div>
                                            </div>
                                            <div class="row block_rows">
                                              <div class="col-md-6 input_wrapper">
                                                <label> Rig/Contact Email: </label>
                                                <p class="take">' .$rowdata->rig_contact_email. '</p>
                                              </div>
                                              <div class="col-md-6 input_wrapper">
                                                <label> Company Man Contact Name: </label>
                                                <p class="take">' .$rowdata->company_man_contact_name. '</p>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <hr class="foot-hr">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-check">
                                              <label class="form-check-label">
                                                <input type="hidden" name="address_selection['.$counter.'][]" value="0">
                                                <input class="form-check-input address_radio_input" type="radio" name="address_selection['.$counter.'][]" value="'.$rowdata->id.'" '.$radio_other_modal.'>
                                                Select
                                                <span class="circle">
                                                  <span class="check"></span>
                                                </span>
                                              </label>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>';
                              }
      if($service_location_id == 'new_address'){
        $radio_new_modal = 'checked';
        $new_addr = '<div class="col-md-6 new_address_col">';
      }
      else{
        $radio_new_modal = '';
        $new_addr = '<div class="col-md-6 new_address_col" style="display:none">'; 
      }

      if($line1 != 'undefined'){
        $line1 = $line1;
      }
      else{
        $line1 = '';
      }
      if($line2 != 'undefined'){
        $line2 = $line2;
      }
      else{
        $line2 = '';
      }
      if($city != 'undefined'){
        $city = $city;
      }
      else{
        $city = '';
      }
      if($country != 'undefined'){
        $country = $country;
      }
      else{
        $country = '';
      }
      if($subdivisioncode != 'undefined'){
        $subdivisioncode = $subdivisioncode;
      }
      else{
        $subdivisioncode = '';
      }

      if($postalcode != 'undefined'){
        $postalcode = $postalcode;
      }
      else{
        $postalcode = '';
      }

      $modal  .= $new_addr;
      $modal  .=   '<div class="card inner_card">';
      $modal  .=     '<div class="card-body">';
      $modal  .=       '<div class="row">';
      $modal  .=         '<div class="col-md-2">';
      $modal  .=           '<div class="card-icon icon-rose text-left">';
      $modal  .=             '<i class="material-icons">home</i>';
      $modal  .=           '</div>';
      $modal  .=         '</div>';
      $modal  .=         '<div class="col-md-7">';
      $modal  .=           '<h6> Address New </h6>';
      $modal  .=         '</div>';
      $modal  .=       '</div>';
      $modal  .=       '<hr class="head-hr" style="margin-bottom:16px;">';

      $modal  .=       '<div class="form-group bmd-form-group input_wrapper">';
      $modal  .=         '<label for="shipAddr_line1" class="bmd-label-floating input_label">Line 1</label>';
      $modal  .=         '<input type="text" class="form-control form_input_field take" id="shipAddr_line1" name="shipAddr_line1['.$counter.'][]" value="'.$line1.'">';
      $modal  .=       '</div>';

      $modal  .=       '<div class="form-group bmd-form-group input_wrapper">';
      $modal  .=         '<label for="shipAddr_line2" class="bmd-label-floating input_label">Line 2</label>';
      $modal  .=         '<input type="text" class="form-control form_input_field take" id="shipAddr_line2" name="shipAddr_line2['.$counter.'][]" value="'.$line2.'">';
      $modal  .=       '</div>';

      $modal  .=     '<div class = "row">';

      $modal  .=       '<div class="col-md-6 input_wrapper">
                        <div class="form-group bmd-form-group ">';
      $modal  .=         '<label for="city" class="bmd-label-floating input_label">Subdivision Code</label>';
      $modal  .=         '<input type="text" class="form-control form_input_field take" id="city" name="shipAddr_city['.$counter.'][]" value="'.$subdivisioncode.'">';
      $modal  .=       '</div></div>';


      $modal  .=       '<div class="col-md-6 input_wrapper">
                        <div class="form-group bmd-form-group ">';
      $modal  .=         '<label for="city" class="bmd-label-floating input_label">Postal Code</label>';
      $modal  .=         '<input type="text" class="form-control form_input_field take" id="city" name="shipAddr_city['.$counter.'][]" value="'.$postalcode.'">';
      $modal  .=       '</div></div>';

      $modal  .=     '</div>';

      $modal  .=     '<div class = "row">';


      $modal  .=       '<div class="col-md-6 input_wrapper">
                        <div class="form-group bmd-form-group ">';
      $modal  .=         '<label for="city" class="bmd-label-floating input_label">City</label>';
      $modal  .=         '<input type="text" class="form-control form_input_field take" id="city" name="shipAddr_city['.$counter.'][]" value="'.$city.'">';
      $modal  .=       '</div></div>';


      $modal  .=       '<div class="col-md-6 input_wrapper">
                        <div class="form-group bmd-form-group">';
      $modal  .=         '<label for="city" class="bmd-label-floating input_label">Country</label>';
      $modal  .=         '<input type="text" class="form-control form_input_field take" id="city" name="shipAddr_city['.$counter.'][]" value="'.$country.'">';
      $modal  .=       '</div></div>';

      $modal  .=     '</div>';




      // New Added Rows

      $modal  .=     '<div class = "row">';

      $modal  .=       '<div class="col-md-6 input_wrapper">
                        <div class="form-group bmd-form-group">';
      $modal  .=         '<label for="lease_name" class="bmd-label-floating input_label">Lease Name</label>';
      $modal  .=         '<input type="text" class="form-control form_input_field take" id="city" name="lease_name[][]" value="'.$lease_name.'">';
      $modal  .=       '</div></div>';


      $modal  .=       '<div class="col-md-6 input_wrapper">
                        <div class="form-group bmd-form-group ">';
      $modal  .=         '<label for="rig_number" class="bmd-label-floating input_label">Rig Number</label>';
      $modal  .=         '<input type="text" class="form-control form_input_field take" id="city" name="rig_number[][]" value="'.$rig_number.'">';
      $modal  .=       '</div></div>';

      $modal  .=     '</div>';

      $modal  .=     '<div class = "row">';

      $modal  .=       '<div class="col-md-6 input_wrapper">
                        <div class="form-group bmd-form-group ">';
      $modal  .=         '<label for="afe_number" class="bmd-label-floating input_label">AFE Number</label>';
      $modal  .=         '<input type="text" class="form-control form_input_field take" id="city" name="afe_number[][]" value="'.$afe_number.'">';
      $modal  .=       '</div></div>';


      $modal  .=       '<div class="col-md-6 input_wrapper">
                        <div class="form-group bmd-form-group ">';
      $modal  .=         '<label for="site_contact_no" class="bmd-label-floating input_label">Site Contact No.</label>';
      $modal  .=         '<input type="text" class="form-control form_input_field take" id="city" name="site_contact_no[][]" value="'.$site_contact_no.'">';
      $modal  .=       '</div></div>';

      $modal  .=     '</div>';


      $modal  .=     '<div class = "row">';

      $modal  .=       '<div class="col-md-6 input_wrapper">
                        <div class="form-group bmd-form-group ">';
      $modal  .=         '<label for="rig_contact_email" class="bmd-label-floating input_label">Rig/Contact Email</label>';
      $modal  .=         '<input type="email" class="form-control form_input_field take" id="city" name="rig_contact_email[][]" value="'.$rig_contact_email.'">';
      $modal  .=       '</div></div>';


      $modal  .=       '<div class="col-md-6 input_wrapper">
                        <div class="form-group bmd-form-group input_wrapper">';
      $modal  .=         '<label for="company_contact_name" class="bmd-label-floating input_label">Company Man/Contact Name</label>';
      $modal  .=         '<input type="text" class="form-control form_input_field take" id="city" name="company_contact_name[][]" value="'.$company_man_contact_name.'">';
      $modal  .=       '</div></div>';

      $modal  .=     '</div>';







      // $modal  .=       '<div class="form-group bmd-form-group input_wrapper">';
      // $modal  .=         '<label for="subdivisioncode" class="bmd-label-floating input_label">Subdivision Code</label>';
      // $modal  .=         '<input type="text" class="form-control form_input_field" id="subdivisioncode" name="shipAddr_countrySubDivisionCode['.$counter.'][]" value="'.$subdivisioncode.'">';


      // $modal  .=       '</div>';

      // $modal  .=       '<div class="form-group bmd-form-group input_wrapper">';
      // $modal  .=         '<label for="postalcode" class="bmd-label-floating input_label">Postal Code</label>';
      // $modal  .=         '<input type="text" class="form-control form_input_field" id="postalcode" name="shipAddr_postalCode['.$counter.'][]" value="'.$postalcode.'">';
      // $modal  .=       '</div>';

      $modal  .=       '<hr class="foot-hr">';
      $modal  .=       '<div class="row">';
      $modal  .=         '<div class="col-md-12">';
      $modal  .=           '<div class="form-check">';
      $modal  .=             '<label class="form-check-label">';
      $modal  .=             '<input type="hidden" name="address_selection['.$counter.'][]" value="0">';
      $modal  .=             '<input class="form-check-input address_radio_input" type="radio" name="address_selection['.$counter.'][]" value="new_address" '.$radio_new_modal.'> Select';
      $modal  .=               '<span class="circle">';
      $modal  .=                 '<span class="check"></span>';
      $modal  .=               '</span>';
      $modal  .=             '</label>';
      $modal  .=           '</div>';
      $modal  .=         '</div>';
      $modal  .=       '</div>';

      $modal  .=     '</div>';
      $modal  .=   '</div>';
      $modal  .= '</div>';

      if($service_location_id != 'new_address'){
      $modal .=                  '<div class="col-md-6">
                                    <div class="card inner_card card-pricing new_address_card">
                                      <div class="card-body ">
                                        <div class="card-icon">
                                          <i class="material-icons">add</i>
                                        </div>
                                        <h4 class="card-title text-uppercase">Add Address</h4>
                                      </div>
                                    </div>
                                  </div>';
      }

      $modal .=                   '</div>
                              </div>
                              
                            </div>
                             <span class="close">×</span>';


        echo $modal;
    }



    public function getCustomerDetails(Request $request){
      $value = $request->value;
      $customer= Customer::where('id', $value)->first();

      echo $customer;
    }

    public function updateCustomerQuoteInfo(Request $request){
      $customer_id= $request->customer_id;
      $account_no= $request->account_no;
      $billAddr_line1= $request->billAddr_line1;
      $billAddr_city= $request->billAddr_city;
      $billAddr_country= $request->billAddr_country;
      $billAddr_countrySubDivisionCode= $request->billAddr_countrySubDivisionCode;
      $billAddr_postalCode= $request->billAddr_postalCode;
      $billing_phone= $request->billing_phone;
      $billing_email= $request->billing_email;
      $billing_contact_name= $request->billing_contact_name;

      Customer::where('id', $customer_id)->update([
          'account_no' => $account_no,
          'billAddr_line1' => $billAddr_line1,
          'billAddr_city' => $billAddr_city,
          'billAddr_country' => $billAddr_country,
          'billAddr_countrySubDivisionCode' => $billAddr_countrySubDivisionCode,
          'billAddr_postalCode' => $billAddr_postalCode,
          'billing_phone' => $billing_phone,
          'billing_email' => $billing_email,
          'billing_contact_name' => $billing_email,
          'updated' => 1,
      ]);

      $return_arr[] = [
          'account_no' => $account_no,
          'billAddr_line1' => $billAddr_line1,
          'billAddr_city' => $billAddr_city,
          'billAddr_country' => $billAddr_country,
          'billAddr_countrySubDivisionCode' => $billAddr_countrySubDivisionCode,
          'billAddr_postalCode' => $billAddr_postalCode,
          'billing_phone' => $billing_phone,
          'billing_email' => $billing_email,
          'billing_contact_name' => $billing_email,
      ];

      echo json_encode($return_arr);



    }

}
