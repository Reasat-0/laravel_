<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Vendor;
use App\Models\Variable;
use App\Models\Inventory;
use App\Models\VendorResource;
use App\Models\Image;

use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Core\OAuth\OAuth2\OAuth2LoginHelper;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;

use DB;
use Validator;

class VendorController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');

        $this->dataService = DataService::Configure(array(
            'auth_mode' => 'oauth2',
            'ClientID' => config('quickbooks.client_id') ? config('quickbooks.client_id') : null,
            'ClientSecret' =>  config('quickbooks.client_secret'),
            'RedirectURI' => config('quickbooks.oauth_redirect_uri'),
            'scope' => config('quickbooks.oauth_scope'),
            'baseUrl' => config('quickbooks.environment')
        ));

    }

	public function index(){
        $vendors_quickbooks_temp = Vendor::whereNull('manual')->where('quickbooks', 1)->where('temp', 1)->orderByRaw(" IF(displayName RLIKE '^[a-z]', 1, 2), displayName ")->get();
        $vendors_quickbooks = Vendor::whereNull('manual')->where('quickbooks', 1)->where('temp', 0)->orderByRaw(" IF(displayName RLIKE '^[a-z]', 1, 2), displayName ")->get();
        $vendors_manual = Vendor::whereIn('manual', [0,1])->orderByRaw('displayName * 1', 'ASC')->get();
        return view('admin.vendor.index', compact('vendors_quickbooks_temp', 'vendors_quickbooks', 'vendors_manual'));
	}
    

    public function create(){
        $resources = Inventory::all();

        session_start();

        $OAuth2LoginHelper = $this->dataService->getOAuth2LoginHelper();
        $authUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();

        // Store the url in PHP Session Object;
        $_SESSION['authUrl'] = $authUrl;

        //set the access token using the auth object
        if (isset($_SESSION['sessionAccessToken'])) {

            $accessToken = $_SESSION['sessionAccessToken'];
            $accessTokenJson = array('token_type' => 'bearer',
                'access_token' => $accessToken->getAccessToken(),
                'refresh_token' => $accessToken->getRefreshToken(),
                'x_refresh_token_expires_in' => $accessToken->getRefreshTokenExpiresAt(),
                'expires_in' => $accessToken->getAccessTokenExpiresAt()
            );
            $this->dataService->updateOAuth2Token($accessToken);
            $oauthLoginHelper = $this->dataService->getOAuth2LoginHelper();

            return view('admin.vendor.create', compact('authUrl', 'accessTokenJson', 'resources'));
        }
        else{
            return view('admin.vendor.create', compact('authUrl', 'resources'));
        }
    }



    public function getQbVendor(){
        session_start();

        $accessToken = $_SESSION['sessionAccessToken'];
        $realmId = $_SESSION['realmId'];

        if( gmdate("Y/m/d H:i:s") > $accessToken->getAccessTokenExpiresAt()){
            echo "Your access token is expired, Please click on <b>Connect to Quickbooks</b> button above";
            die();
        }
        else{
            $this->dataService->updateOAuth2Token($accessToken);
        }
        

        $vendor = Vendor::where('quickbooks', 1)->orderBy('id', 'desc')->first();
        if($vendor)
            $quickbooks_id_q = $vendor->quickbooks_id;
        else
            $quickbooks_id_q = 0;


        $entities = $this->dataService->Query("Select * from Vendor WHERE Id >'" .$quickbooks_id_q.  "' ORDERBY Id ASC MaxResults 1000");
        if($entities){
            $vendor_all = [];
            foreach ($entities as $rowdata) {

                 $vendor_all[] = [
                    'companyName' => isset($rowdata->CompanyName) ? $rowdata->CompanyName : null,
                    'displayName' => isset($rowdata->DisplayName) ? $rowdata->DisplayName : null,
                    'title' => isset($rowdata->Title) ? $rowdata->Title : null,
                    'givenName' => isset($rowdata->GivenName) ? $rowdata->GivenName : null,
                    'middleName' => isset($rowdata->MiddleName) ? $rowdata->MiddleName : null,
                    'familyName' => isset($rowdata->FamilyName) ? $rowdata->FamilyName : null,
                    'suffix' => isset($rowdata->Suffix) ? $rowdata->Suffix : null,

                    'primaryPhone_freeFormNumber' => isset($rowdata->PrimaryPhone->FreeFormNumber) ? $rowdata->PrimaryPhone->FreeFormNumber : null,
                    'primaryEmailAddr_addess' => isset($rowdata->PrimaryEmailAddr->Address) ? $rowdata->PrimaryEmailAddr->Address : null,
                    'mobile' => isset($rowdata->Mobile->FreeFormNumber) ? $rowdata->Mobile->FreeFormNumber : null,
                    //'fax' => isset($rowdata->Fax->FreeFormNumber) ? $rowdata->Fax->FreeFormNumber : null,
                    'alternatePhone' => isset($rowdata->AlternatePhone->FreeFormNumber) ? $rowdata->AlternatePhone->FreeFormNumber : null,

                    'billAddr_line1' => isset($rowdata->BillAddr->Line1) ? $rowdata->BillAddr->Line1 : null,
                    'billAddr_line2' => isset($rowdata->BillAddr->Line2) ? $rowdata->BillAddr->Line2 : null,
                    'billAddr_city' => isset($rowdata->BillAddr->City) ? $rowdata->BillAddr->City : null,
                    'billAddr_country' => isset($rowdata->BillAddr->Country) ? $rowdata->BillAddr->Country : null,
                    'billAddr_countrySubDivisionCode' => isset($rowdata->BillAddr->CountrySubDivisionCode) ? $rowdata->BillAddr->CountrySubDivisionCode : null,
                    'billAddr_postalCode' => isset($rowdata->BillAddr->PostalCode) ? $rowdata->BillAddr->PostalCode : null,
                    
                    'TaxIdentifier' => isset($rowdata->TaxIdentifier) ? $rowdata->TaxIdentifier : null,
                    'accountNum' => isset($rowdata->AcctNum) ? $rowdata->AcctNum : null,
                    'webAddr' => isset($rowdata->WebAddr->URI) ? $rowdata->WebAddr->URI : null,
                    'notes' => isset($rowdata->Notes) ? $rowdata->Notes : null,
                    
                    'quickbooks_id' => $rowdata->Id,
                    'syncToken' => $rowdata->SyncToken,
                    'quickbooks' => 1,
                    'temp' => 1,
                ];
            }

            try{
                Vendor::insert($vendor_all);
                echo "Vendor data has been imported from Quickbooks. Please  <a href=" .route('vendors.index', ['temp' => 'true']) . ">Check Here</a> and review it for saving into database";
            }
            catch(Exception $e) {
                echo $e; 
            }

        }
        else{
            echo "No new Vendor data is present in Quickbooks. To see all the vendors, please  <a href=" .route('vendors.index') . ">Check Here</a>";
        }
    
    }


    public function synchronizeEvosVendor(){
        session_start();

        $accessToken = $_SESSION['sessionAccessToken'];
        if( gmdate("Y/m/d H:i:s") > $accessToken->getAccessTokenExpiresAt()){
            echo "Your access token is expired, Please click on <b>Connect to Quickbooks</b> button above";
            die();
        }
        else{
            $this->dataService->updateOAuth2Token($accessToken);
        }
        dispatch(new \App\Jobs\SynchronizeVendor($this->dataService, 'syncEvo'));

        return  "Your Vendor data has been synchronized with quickbook";
    }

    public function synchronizeQbVendor(){
        session_start();

        $accessToken = $_SESSION['sessionAccessToken'];
        if( gmdate("Y/m/d H:i:s") > $accessToken->getAccessTokenExpiresAt()){
            echo "Your access token is expired, Please click on <b>Connect to Quickbooks</b> button above";
            die();
        }
        else{
            $this->dataService->updateOAuth2Token($accessToken);
        }
        dispatch(new \App\Jobs\SynchronizeVendor($this->dataService, 'syncQb'));
      
        return  "Quickbook has been synchronized with Evo vendors";
    }

    
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'displayName' => 'required|unique:vendors',
        ]);

    if($validator->fails()){
            return back()->withErrors($validator)->withInput(['tab'=>'custom_vendor']);
    }
    else{
        DB::beginTransaction();
        try{
            $vendor = Vendor::create([
                'companyName' => $request->companyName,
                'displayName' => $request->displayName,
                'title' => $request->title,
                'givenName' => $request->givenName,
                'middleName' => $request->middleName,
                'familyName' => $request->familyName,
                'suffix' => $request->suffix,

                'primaryPhone_freeFormNumber' => $request->primaryPhone_freeFormNumber,
                'primaryEmailAddr_addess' => $request->primaryEmailAddr_addess,
                'mobile' => $request->mobile,
                'alternatePhone' => $request->alternatePhone,

                'billAddr_line1' => $request->billAddr_line1,
                'billAddr_line2' => $request->billAddr_line2,
                'billAddr_city' => $request->billAddr_city,
                'billAddr_country' => $request->billAddr_country,
                'billAddr_countrySubDivisionCode' => $request->billAddr_countrySubDivisionCode,
                'billAddr_postalCode' => $request->billAddr_postalCode,
                
                'TaxIdentifier' => $request->TaxIdentifier,
                'accountNum' => $request->accountNum,
                'webAddr' => $request->webAddr,
                'notes' => $request->notes,
                
                'reg_number' => $request->reg_number,
                'license_number' => $request->license_number,

                'manual' => 0,
            ]);

            if($request->singleCustomerQB == '1'){
                session_start();

                $accessToken = $_SESSION['sessionAccessToken'];
                $this->dataService->updateOAuth2Token($accessToken);

                $theResourceObj = \QuickBooksOnline\API\Facades\Vendor::create([
                            "BillAddr" => [
                                "Line1" => $vendor->billAddr_line1,
                                "Line2" => $vendor->billAddr_line2,
                                "City" => $vendor->billAddr_city,
                                "Country" => $vendor->billAddr_country,
                                "CountrySubDivisionCode" => $vendor->billAddr_countrySubDivisionCode,
                                "PostalCode" => $vendor->billAddr_postalCode
                            ],
                            "TaxIdentifier"=> $vendor->taxIdentifier,
                            "AcctNum"=> $vendor->accountNum,
                            "OtherAddr"=> $vendor->otherAddr,
                            "Notes" => $vendor->notes,
                            "Title" => $vendor->title,
                            "GivenName" => $vendor->givenName,
                            "MiddleName" => $vendor->middleName,
                            "FamilyName" => $vendor->familyName,
                            "Suffix" => $vendor->suffix,
                            "CompanyName" => $vendor->companyName,
                            "DisplayName" => $vendor->displayName,
                            "PrimaryPhone" => [
                                "FreeFormNumber" => $vendor->primaryPhone_freeFormNumber
                            ],
                            "PrimaryEmailAddr" => [
                                "Address" => $vendor->primaryEmailAddr_addess
                            ],
                            "AlternatePhone" => [
                                "FreeFormNumber" => $vendor->alternatePhone
                            ],
                            "Mobile" => [
                                "FreeFormNumber" => $vendor->mobile
                            ],
                            /*"Fax" => [
                                "FreeFormNumber" => $vendor->fax
                            ],*/
                            "WebAddr" => [
                                "URI" => $vendor->webAddr
                            ]
                ]);
//dd('okk');
                $resultingObj = $this->dataService->Add($theResourceObj);
                Vendor::where('id', $vendor->id)->update(['manual' => 1, 'quickbooks' => 1, 'quickbooks_id' => $resultingObj->Id, 'syncToken' => $resultingObj->SyncToken,]);

                DB::commit();
            }

            else{
                DB::commit(); 
            }   


            if($request->images){
                $images = $request->images;
                $all_image = [];
                $i = 0;
                foreach($images as $rowdata){
                    $i++;
                    //$name = preg_replace('/\s+/', '',$rowdata->getClientOriginalName());
                    //$original_name = preg_replace('/\(([^()]*+|(?R))*\)\s*/', '', $name);
                    //dd($original_name);
                    $original_name = $rowdata->getClientOriginalName();
                    $disk_image_name = time() . $i . $original_name;  
                    $rowdata->move(storage_path('app/public/images'), $disk_image_name); 

                    $all_image[] = [
                        'type' => 'vendor',
                        'parent_id' => $vendor->id,
                        'name' => $original_name,
                        'url' => url('/') . '/storage/images/' . $disk_image_name,
                    ]; 
                }
                Image::insert($all_image);
            }

            $resource_name = $request->resource_name;
            //dd($r_name);
            if($resource_name){
            foreach ($resource_name as $key => $value) {
                //dd($key);
                    $vendor_resource = VendorResource::create([
                        'vendor_id'=> $vendor->id,
                        'resource_id'=> $request->resource_id[$key],
                        'resource_name'=> $request->resource_name[$key],
                        'resource_code'=> $request->resource_code[$key],
                        'resource_model_no'=> $request->resource_model_no[$key],
                    ]);
                    //dd($key);
                    $row_index = $request->row_index[$key];
                    $var_name = $request->input('var_name'.$row_index);
                    //dd($request->var_name);
                    if($var_name){
                        $var_type = $request->input('var_type'.$row_index);
                        $var_status = $request->input('var_status'.$row_index);
                        foreach ($var_name as $key2 => $value2) {
                            DB::table('variables_old')->create([
                                'type'=> 'vendor',
                                'parent_id'=> $vendor_resource->id,
                                'var_name'=> $var_name[$key2],
                                'var_type'=> $var_type[$key2],
                                'enable_disable'=> $var_status[$key2],
                            ]);
                        }
                    }

                }
            }

            if($request->singleCustomerQB == '1'){
                session()->flash('success', 'Successfully added to Evos database & Quickbooks');
            }else{
                session()->flash('success', 'Successfully added');
            }

        }
        catch (\Exception $ex) {
            DB::rollback();
            session()->flash('error', $ex);
        }   

        return back()->withInput(['tab'=>'custom_vendor']);
    }

    }

    
    public function show(){
    	//
    }

    
    public function edit($id){
        $vendor = Vendor::find($id);
        $images = Image::where('type', 'vendor')->where('parent_id', $vendor->id)->get();
        $resources = Inventory::all();
        $vendor_resources = VendorResource::where('vendor_id', $id)->get();
        $variables = DB::table('variables_old')->where('type', 'vendor')->where('parent_id', $vendor->id)->get();
        return view('admin.vendor.edit', compact('vendor', 'images', 'resources', 'vendor_resources', 'variables'));
    }

    
     public function update(Request $request, $id){
        $request->validate([
            'displayName' => 'required|unique:vendors,displayName,'.$id,
        ]);

        DB::beginTransaction();
        try{
            Vendor::whereId($id)->update([
                'companyName' => $request->companyName,
                'displayName' => $request->displayName,
                'title' => $request->title,
                'givenName' => $request->givenName,
                'middleName' => $request->middleName,
                'familyName' => $request->familyName,
                'suffix' => $request->suffix,

                'primaryPhone_freeFormNumber' => $request->primaryPhone_freeFormNumber,
                'primaryEmailAddr_addess' => $request->primaryEmailAddr_addess,
                'mobile' => $request->mobile,
                'mobile' => $request->mobile,
                'alternatePhone' => $request->alternatePhone,

                'billAddr_line1' => $request->billAddr_line1,
                'billAddr_line2' => $request->billAddr_line2,
                'billAddr_city' => $request->billAddr_city,
                'billAddr_country' => $request->billAddr_country,
                'billAddr_countrySubDivisionCode' => $request->billAddr_countrySubDivisionCode,
                'billAddr_postalCode' => $request->billAddr_postalCode,
                
                'TaxIdentifier' => $request->TaxIdentifier,
                'accountNum' => $request->accountNum,
                'webAddr' => $request->webAddr,
                'notes' => $request->notes,
                
                'reg_number' => $request->reg_number,
                'license_number' => $request->license_number, 

                "updated" => 1
            ]);

            if($request->singleCustomerQB == '1'){
                session_start();
                if(isset($_SESSION['sessionAccessToken'])){
                    $accessToken = $_SESSION['sessionAccessToken'];

                    if( gmdate("Y/m/d H:i:s") > $accessToken->getAccessTokenExpiresAt()){
                        session()->flash('error', 'Your access token is expired, Please click on the connect to quickbooks button first if you want to synchronize with quickbooks');  
                    }
                    else{
                        $this->dataService->updateOAuth2Token($accessToken);
                        $vendor_row= Vendor::find($id);
                        if($vendor_row->quickbooks == 1){
                            $vendor = $this->dataService->FindbyId('vendor', $vendor_row->quickbooks_id);
                            $theResourceObj = \QuickBooksOnline\API\Facades\Vendor::update($vendor  , [
                                  "BillAddr" => [
                                            "Line1" => $request->billAddr_line1,
                                            "Line2" => $request->billAddr_line2,
                                            "City" => $request->billAddr_city,
                                            "Country" => $request->billAddr_country,
                                            "CountrySubDivisionCode" => $request->billAddr_countrySubDivisionCode,
                                            "PostalCode" => $request->billAddr_postalCode
                                        ],
                                        "OtherAddr"=> $request->otherAddr,
                                        "Notes" => $request->notes,
                                        "Title" => $request->title,
                                        "GivenName" => $request->givenName,
                                        "MiddleName" => $request->middleName,
                                        "FamilyName" => $request->familyName,
                                        "Suffix" => $request->suffix,
                                        "CompanyName" => $request->companyName,
                                        "DisplayName" => $request->displayName,
                                        "PrimaryPhone" => [
                                            "FreeFormNumber" => $request->primaryPhone_freeFormNumber
                                        ],
                                        "PrimaryEmailAddr" => [
                                            "Address" => $request->primaryEmailAddr_addess
                                        ],
                                        "AlternatePhone" => [
                                            "FreeFormNumber" => $request->alternatePhone
                                        ],
                                        "Mobile" => [
                                            "FreeFormNumber" => $request->mobile
                                        ],
                                        /*"Fax" => [
                                            "FreeFormNumber" => $request->fax
                                        ],*/
                                        "WebAddr" => [
                                            "URI" => $request->webAddr
                                        ]
                            ]);
                            $resultingObj = $this->dataService->Update($theResourceObj);
                            Vendor::where('id', $vendor_row->id)->increment('syncToken');
                            Vendor::where('id', $vendor_row->id)->update(['updated' => 0]);
                            
                            DB::commit();
                            session()->flash('success', 'Successfully Updated & synchronized with quickbooks');
                        }
                        else{
                            $theResourceObj = \QuickBooksOnline\API\Facades\Vendor::create([
                                "BillAddr" => [
                                    "Line1" => $request->billAddr_line1,
                                    "Line2" => $request->billAddr_line2,
                                    "City" => $request->billAddr_city,
                                    "Country" => $request->billAddr_country,
                                    "CountrySubDivisionCode" => $request->billAddr_countrySubDivisionCode,
                                    "PostalCode" => $request->billAddr_postalCode
                                ],
                                "OtherAddr"=> $request->otherAddr,
                                "Notes" => $request->notes,
                                "Title" => $request->title,
                                "GivenName" => $request->givenName,
                                "MiddleName" => $request->middleName,
                                "FamilyName" => $request->familyName,
                                "Suffix" => $request->suffix,
                                "CompanyName" => $request->companyName,
                                "DisplayName" => $request->displayName,
                                "PrimaryPhone" => [
                                    "FreeFormNumber" => $request->primaryPhone_freeFormNumber
                                ],
                                "PrimaryEmailAddr" => [
                                    "Address" => $request->primaryEmailAddr_addess
                                ],
                                "AlternatePhone" => [
                                    "FreeFormNumber" => $request->alternatePhone
                                ],
                                "Mobile" => [
                                    "FreeFormNumber" => $request->mobile
                                ],
                                /*"Fax" => [
                                    "FreeFormNumber" => $request->fax
                                ],*/
                                "WebAddr" => [
                                    "URI" => $request->webAddr
                                ]
                            ]);

                            $resultingObj = $this->dataService->Add($theResourceObj);
                            if($vendor_row->manual == 0){
                                Vendor::where('id', $vendor_row->id)->update(['manual' => 1, 'quickbooks' => 1, 'quickbooks_id' => $resultingObj->Id, 'syncToken' => $resultingObj->SyncToken, 'updated' => 0,]);
                            }
                            else{
                                Vendor::where('id', $vendor_row->id)->update(['quickbooks' => 1, 'quickbooks_id' => $resultingObj->Id, 'syncToken' => $resultingObj->SyncToken, 'updated' => 0,]);
                            }
                            DB::commit();
                            session()->flash('success', 'Successfully Updated & synchronized with quickbooks');
                        }
                    }
                }
                else{
                  session()->flash('error', 'Please click on the connect to quickbooks button first if you want to synchronize with quickbooks');  
                }
            }
            else{
                DB::commit();
                session()->flash('success', 'Successfully Updated');
            }

    
            $vendor = Vendor::find($id);
            $img_key = $request->preloaded;
            $img_rows = Image::where('type', 'vendor')->where('parent_id', $vendor->id)->get();
            if($img_key){
                foreach ($img_rows as $rowdata) {
                       if(!in_array($rowdata->id, $img_key)){
                          Image::where('id', $rowdata->id)->delete();  
                       }
                }
            }
            else{
                foreach ($img_rows as $rowdata) {
                    Image::where('id', $rowdata->id)->delete();  
                }
            }

            if($request->images){
                $images = $request->images;
                $all_image = [];
                $i = 0;
                foreach($images as $rowdata){
                    $i++;
                    //$name = preg_replace('/\s+/', '',$rowdata->getClientOriginalName());
                    //$original_name = preg_replace('/\(([^()]*+|(?R))*\)\s*/', '', $name);
                    $original_name = $rowdata->getClientOriginalName();
                    $disk_image_name = time() . $i. $original_name;  
                    $rowdata->move(storage_path('app/public/images'), $disk_image_name); 

                    $all_image[] = [
                        'type' => 'vendor',
                        'parent_id' => $vendor->id,
                        'name' => $original_name,
                        'url' => url('/') . '/storage/images/' . $disk_image_name,
                    ]; 
                }
                Image::insert($all_image);
            }

            $resource_name = $request->resource_name;
            $resource_key = $request->r_key;
            $resource_rows = VendorResource::where('vendor_id', $vendor->id)->get();
            if($resource_key){
                foreach ($resource_rows as $rowdata) {
                       if(!in_array($rowdata->id, $resource_key)){
                          DB::table('variables_old')->where('type', 'vendor')->where('parent_id', $rowdata->parent_id)->delete();
                          VendorResource::where('id', $rowdata->id)->delete();  
                       }
                }
            }
            else{
                foreach ($resource_rows as $rowdata) {
                    DB::table('variables_old')->where('type', 'vendor')->where('parent_id', $rowdata->parent_id)->delete();
                    VendorResource::where('id', $rowdata->id)->delete();                    
                }   
            }

            if($resource_name){
            foreach ($resource_name as $key => $value) {
                //dd($key);
                if(isset($request->r_key[$key])){
                    $vendor_resource = VendorResource::where('id', $request->r_key[$key])->update([
                        'vendor_id'=> $vendor->id,
                        'resource_id'=> $request->resource_id[$key],
                        'resource_name'=> $request->resource_name[$key],
                        'resource_code'=> $request->resource_code[$key],
                        'resource_model_no'=> $request->resource_model_no[$key],
                    ]);
                    //dd($key);
                    $row_index = $request->row_index[$key];
                    $var_name = $request->input('var_name0'.$row_index);
                    
                    $var_key = $request->input('var_key'.$row_index);
                    //dd($request->var_name);
                    if($var_name){
                        $var_type = $request->input('var_type0'.$row_index);
                        $var_status = $request->input('var_status0'.$row_index);
                        foreach ($var_name as $key2 => $value2) {
                                DB::table('variables_old')->where('id', $var_key[$key2])->update([
                                    'type'=> 'vendor',
                                    'parent_id'=> $request->r_key[$key],
                                    'var_name'=> $var_name[$key2],
                                    'var_type'=> $var_type[$key2],
                                    'enable_disable'=> $var_status[$key2],
                                ]);
                        }
                    }
                }
                else{
                    $vendor_resource = VendorResource::create([
                        'vendor_id'=> $vendor->id,
                        'resource_id'=> $request->resource_id[$key],
                        'resource_name'=> $request->resource_name[$key],
                        'resource_code'=> $request->resource_code[$key],
                        'resource_model_no'=> $request->resource_model_no[$key],
                    ]);
                    //dd($key);
                    $row_index = $request->row_index[$key];
                    $var_name = $request->input('var_name'.$row_index);
                    //dd($request->var_name);
                    if($var_name){
                        $var_type = $request->input('var_type'.$row_index);
                        $var_status = $request->input('var_status'.$row_index);
                        foreach ($var_name as $key2 => $value2) {

                            DB::table('variables_old')->create([
                                'type'=> 'vendor',
                                'parent_id'=> $vendor_resource->id,
                                'var_name'=> $var_name[$key2],
                                'var_type'=> $var_type[$key2],
                                'enable_disable'=> $var_status[$key2],
                            ]);
                        }
                    }   
                }


                }
            }

        }
        catch (\Exception $ex) {
            DB::rollback();
            session()->flash('error', $ex);
        }

        //session()->flash('success', 'Successfully updated');
        return back();
    }

    
     public function destroy(Vendor $vendor){
        Vendor::where('id', $vendor->id)->update(['updated' => 2]);
        $vendor->delete();
        session()->flash('success', 'Successfully deleted');
        return back();
    }

    public function getImage(Request $request){
        $get_images = Image::where('type', $request->type)
              ->where('parent_id', $request->value)
              //->orderBy('id', 'asc')
              ->get();

        $images = null;
        foreach ($get_images as $image) {
          $images[] = [
              'file' => $image->url,
              'id' =>  $image->id,
              'name' =>  $image->name,
                ];
            }

        echo json_encode($images);

    }


    public function insertQbVendor(){

        $vendor = Vendor::where('temp', 1)->get();
        if(!$vendor->isEmpty()){
            $vendor = Vendor::where('temp', 1)->update(['temp' => 0]);
            session()->flash("message", "<b>Vendor data has been imported from Quickbooks into database. To see all the vendors, please  <a href=" .route('vendors.index') . ">Check Here</a></b>");
        }
        else
            session()->flash("message", "<b>There is no temporary vendor data for review. To see all the vendors, please  <a href=" .route('vendors.index') . ">Check Here</a></b>");
    }

    public function discardQbVendor(){
        $vendor = Vendor::where('temp', 1)->get();
        if(!$vendor->isEmpty()){
            $vendor = Vendor::where('temp', 1)->delete();
            session()->flash("message", "<b>Vendor data has been discarded. To see all the vendors, please  <a href=" .route('vendors.index') . ">Check Here</a></b>");
        }
        else
            session()->flash("message", "<b>There is no temporary vendor data for discard. To see all the vendors, please  <a href=" .route('vendors.index') . ">Check Here</a></b>");
    }


}
