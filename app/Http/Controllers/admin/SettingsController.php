<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\QuickbooksSettings;
use App\Models\OpenInvoiceSettings;
use App\Models\EmailConfiguration;
use App\Models\AppUrl;


class SettingsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(){
        $quickbooks = QuickbooksSettings::first();
        $openinvoice = OpenInvoiceSettings::first();
        $email_config = EmailConfiguration::first();
        $app_url = AppUrl::first();

        return view('admin.settings.index', compact('quickbooks', 'openinvoice', 'email_config', 'app_url'));
	}
    

    public function store(Request $request){

        if($request->type == 'quickbooks')
            $this->quickbooks($request);
        elseif($request->type == 'openinvoice')
            $this->openinvoice($request);
        elseif($request->type == 'smtp')
            $this->smtp($request);
        elseif($request->type == 'appUrl')
            $this->appUrl($request);
        return back();

    }

    public function quickbooks($request){
        $quickbooks = QuickbooksSettings::first();
        if($quickbooks){
           $quickbooks->client_id = $request->client_id;
           $quickbooks->client_secret = $request->client_secret;
           $quickbooks->oauth_redirect_uri = $request->oauth_redirect_uri;
           $quickbooks->environment = $request->environment == 'on' ? 'Production' : 'Development';
           
           $quickbooks->update(); 
        }
        else{
           $quickbooks = new QuickbooksSettings([
                'client_id' => $request->client_id,
                'client_secret' => $request->client_secret,
                'oauth_redirect_uri' => $request->oauth_redirect_uri,
                'environment' => $request->environment == 'on' ? 'Production' : 'Development'
            ]);
            
            $quickbooks->save(); 
        }
        session()->flash('success', 'Successfully updated quickbooks settings');
    }

    public function openinvoice($request){
        $openinvoice = OpenInvoiceSettings::first();
        if($openinvoice){
           $openinvoice->public_key = $request->public_key;
           $openinvoice->secret_key = $request->secret_key;
           
           $openinvoice->update(); 
        }
        else{
           $openinvoice = new OpenInvoiceSettings([
                'public_key' => $request->public_key,
                'secret_key' => $request->secret_key,
            ]);
            
            $openinvoice->save(); 
        }
        session()->flash('success', 'Successfully updated openinvoice settings');
    }

    public function smtp($request){

        $email_config = EmailConfiguration::first();

        if($email_config){
            $email_config->from_name = $request->from_name;
            $email_config->from_email = $request->from_email;
            $email_config->mailer = $request->mailer;
            $email_config->smtp_host = $request->smtp_host;
            $email_config->auto_tls = $request->auto_tls == 'on' ? 'ssl' : null;
            $email_config->port_no = $request->port_no;
            $email_config->authentication = $request->authentication == 'on' ? 'on' : null;
            $email_config->smtp_user = $request->smtp_user;
            $email_config->smtp_password = $request->smtp_password;

            $email_config->update();
        }
        else{
             $email_config = new EmailConfiguration([
                'from_name' => $request->from_name,
                'from_email' => $request->from_email,
                'mailer' => $request->mailer,
                'smtp_host' => $request->smtp_host,
                'auto_tls' => $request->auto_tls=='on' ? 'ssl' : null,
                'port_no' => $request->port_no,
                'authentication' => $request->authentication=='on' ? 'on' : null,
                'smtp_user' => $request->smtp_user,
                'smtp_password' => $request->smtp_password,
            ]);

             $email_config->save();
        }

        session()->flash('success', 'Successfully updated email configuration');
    }



    public function appUrl($request){
        $appUrl = AppUrl::first();
        if($appUrl){
           $appUrl->url = $request->url;
           $appUrl->update(); 
        }
        else{
           $appUrl = new AppUrl([
                'url' => $request->url,
            ]);
            
            $appUrl->save(); 
        }
        session()->flash('success', 'Successfully updated App Url');
    }


}
