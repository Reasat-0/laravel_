<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\InsuranceEquipment;
use App\Models\InsuranceVehicle;
use App\Models\InsuranceDriver;
use App\Models\SubcontractorAgreement;
use App\Models\RentalAgreement;
use App\Models\OwnerAgreement;
use App\Mail\InsuranceEmail;
use App\Mail\AgreementEmail;
use App\Models\Image;

use App\Exports\InsuranceExport;
use App\Exports\AgreementExport;

use Mail;
use PDF;
use Response;
use Auth;
use Excel;
use Carbon\Carbon;

class InsuranceController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['insuranceToken', 'getPdf', 'agreement', 'getInsuranceImage', 'agreementMessage', 'store']]);
    }

	public function index(){
		$email = Auth::user()->email;
		$name = Auth::user()->name;

		$equipment_reqs = collect(InsuranceEquipment::all());
		$vehicle_reqs = collect(InsuranceVehicle::all());
		$driver_reqs = collect(InsuranceDriver::all());

		$all = $equipment_reqs->merge($vehicle_reqs)->merge($driver_reqs)->sortBy('created_at');
		$all_reqs = $all->values()->all();

		$subcontractor_agreement = collect(SubcontractorAgreement::all()); 
		$rental_agreement = collect(RentalAgreement::all()); 
		$owner_agreement = collect(OwnerAgreement::all()); 
		$agreement_merged = $subcontractor_agreement->merge($rental_agreement)->merge($owner_agreement)->sortBy('created_at');
		$all_agreement = $agreement_merged->values()->all();

        return view('admin.insurance.index', compact('email', 'name', 'equipment_reqs', 'vehicle_reqs', 'driver_reqs', 'all_reqs', 'all_agreement'));
	}

	public function store(Request $request){
        if($request->tab == 'equipment')
            return $this->equipment($request, 'store');
        elseif($request->tab == 'vehicle')
            return $this->vehicle($request, 'store');
        elseif($request->tab == 'driver')
            return $this->driver($request, 'store');
        elseif($request->tab == 'subcontractor_agreement')
            return $this->subcontractor_agreement($request, 'store');
        elseif($request->tab == 'rental_agreement')
            return $this->rental_agreement($request, 'store');
        elseif($request->tab == 'owner_agreement')
            return $this->owner_agreement($request, 'store');
	}

	
	public function equipment($request, $type){
		if($type == 'store'){
			$insurance = InsuranceEquipment::create($request->except('tab'));

			$insurance->token =  mt_rand(100000,999999);
			$insurance->url_token =  $insurance->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);
			$insurance->save();

			$request_type = 'equipment';
			$subject = 'Equipment Change Request';
			Mail::to($request->send_mail_to)->send(new InsuranceEmail($insurance, $request_type, $subject));

			session()->flash('success', 'Successfully added.');  
            return back()->withInput(['tab'=>'equipment_tab']);  
		}
	}

	public function vehicle($request, $type){
		if($type == 'store'){
			$insurance = InsuranceVehicle::create($request->except('tab'));
			
			$insurance->token =  mt_rand(100000,999999);
			$insurance->url_token =  $insurance->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);
			$insurance->save();

			$request_type = 'vehicle';
			$subject = 'vehicle Change Request';
			Mail::to($request->send_mail_to)->send(new InsuranceEmail($insurance, $request_type, $subject));			
			session()->flash('success', 'Successfully added.');  
            return back()->withInput(['tab'=>'vehicle_tab']);  
		}
	}

	public function driver($request, $type){
		if($type == 'store'){
			//dd($request->preloaded);

			$insurance = InsuranceDriver::create($request->except(['tab', 'file', 'preloaded']));
			
			$insurance->token =  mt_rand(100000,999999);
			$insurance->url_token =  $insurance->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);
			$insurance->save();


			if($request->preloaded){
				foreach ($request->preloaded as $rowdata) {
					$old_driver_file = Image::where('id', $rowdata)->first();

					$all_file[] = [
	                    'type' => 'driver-insurance',
	                    'parent_id' => $insurance->id,
	                    'name' => $old_driver_file->name,
	                    'url' => $old_driver_file->url,
                	]; 
				}
				Image::insert($all_file);
			}	

			if($request->file){
            $file = $request->file;
            $all_file = [];
            $i = 0;
            foreach($file as $rowdata){
                $i++;
                //$name = preg_replace('/\s+/', '',$rowdata->getClientOriginalName());
                //$original_name = preg_replace('/\(([^()]*+|(?R))*\)\s*/', '', $name);
                $original_name = $rowdata->getClientOriginalName();
                $disk_image_name = time() . $i .$original_name;  
                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

                $all_file[] = [
                    'type' => 'driver-insurance',
                    'parent_id' => $insurance->id,
                    'name' => $original_name,
                    'url' => url('/') . '/storage/files/' . $disk_image_name,
                ]; 
            }
            Image::insert($all_file);
            }


			$request_type = 'driver';
			$subject = 'Driver Change Request';
			Mail::to($request->send_mail_to)->send(new InsuranceEmail($insurance, $request_type, $subject));			
			session()->flash('success', 'Successfully added.');  
            return back()->withInput(['tab'=>'driver_tab']);  
		}
	}

	public function subcontractor_agreement($request, $type){
		if($type == 'store'){
			if($request->status == 1){
				$request->validate([
		            'agreement_send_mail_to' => 'required|email',
		        ]);

				$subcontractor_agreement = SubcontractorAgreement::create($request->except(['tab', 'subcontractor_agreement_w9', 'subcontractor_insurance_certificate']));
				
				$subcontractor_agreement->access_code =  mt_rand(100000,999999);
				$subcontractor_agreement->url_token =  $subcontractor_agreement->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);

				$subcontractor_agreement->sent = json_encode([Carbon::now()]);
				$subcontractor_agreement->save();


				if($request->subcontractor_agreement_w9){
	            $subcontractor_agreement_w9 = $request->subcontractor_agreement_w9;
	            $all_file = [];
	            $i = 0;
	            foreach($subcontractor_agreement_w9 as $rowdata){
	                $i++;
	                $original_name = $rowdata->getClientOriginalName();
	                $disk_image_name = time() . $i .$original_name;  
	                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

	                $all_file[] = [
	                    'type' => 'subcontractor_agreement_w9',
	                    'parent_id' => $subcontractor_agreement->id,
	                    'name' => $original_name,
	                    'url' => url('/') . '/storage/files/' . $disk_image_name,
	                ]; 
	            }
	            Image::insert($all_file);
	            }

	            if($request->subcontractor_insurance_certificate){
	            $subcontractor_insurance_certificate = $request->subcontractor_insurance_certificate;
	            $all_file = [];
	            $i = 0;
	            foreach($subcontractor_insurance_certificate as $rowdata){
	                $i++;
	                $original_name = $rowdata->getClientOriginalName();
	                $disk_image_name = time() . $i .$original_name;  
	                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

	                $all_file[] = [
	                    'type' => 'subcontractor_insurance_certificate',
	                    'parent_id' => $subcontractor_agreement->id,
	                    'name' => $original_name,
	                    'url' => url('/') . '/storage/files/' . $disk_image_name,
	                ]; 
	            }
	            Image::insert($all_file);
	            }

	            $request_type = 'subcontractor_agreement';
				$subject = 'Subcontractor Agreement Request';
				$body = 'Thank you for the opportunity to work with EV Oilfield Services, LLC as a Sub Contractor.  Please find below the link to our Sub Contractor Agreement.  Please fill it out completely and return to us as soon as possible so we can set up in our system as an authorized EV Sub Contractor.  Thank you again, and we look forward to working with you!';
				$status = 2;
				$reject = '';
				Mail::to($request->agreement_send_mail_to)->send(new AgreementEmail($subcontractor_agreement, $request_type, $subject, $body, $status, $reject));

				session()->flash('success', 'Successfully submitted.');  
	            return back()->withInput(['tab'=>'subcontractor_agreement']);
        	}
        	elseif($request->status == 2 || $request->status == 4){
				$subcontractor_agreement = SubcontractorAgreement::where('id', $request->p_key)->first();
				SubcontractorAgreement::where('id', $request->p_key)
																->update($request->except([
																	'tab', 
																	'url_token',
																	'subcontractor_agreement_w9', 
																	'subcontractor_insurance_certificate',
																	'preloaded',
																	'preloaded2',
																	'submit',
																	'p_key',
																	'_token',
																	'signed'
																])
															);
				$subcontractor_agreement->access_code =  mt_rand(100000,999999);
				$subcontractor_agreement->url_token =  $subcontractor_agreement->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);
				$subcontractor_agreement->status =  2;
				$subcontractor_agreement->update();
				
				$subcontractor_agreement_w9_key = $request->preloaded;
				//dd($subcontractor_agreement_w9_key);
		        $img_rows = Image::where('type', 'subcontractor_agreement_w9')->where('parent_id', $request->p_key)->get();
		        if($subcontractor_agreement_w9_key){
		            foreach ($img_rows as $rowdata) {
		                   if(!in_array($rowdata->id, $subcontractor_agreement_w9_key)){
		                      Image::where('id', $rowdata->id)->delete();  
		                   }
		            }
		        }
		        else{
		            foreach ($img_rows as $rowdata) {
		                      Image::where('id', $rowdata->id)->delete();  
		            }
		        }
				if($request->subcontractor_agreement_w9){
		            $subcontractor_agreement_w9 = $request->subcontractor_agreement_w9;
		            $all_file = [];
		            $i = 0;
		            foreach($subcontractor_agreement_w9 as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'subcontractor_agreement_w9',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }


	            $subcontractor_insurance_certificate_key = $request->preloaded2;
		        $img_rows = Image::where('type', 'subcontractor_insurance_certificate')->where('parent_id', $request->p_key)->get();
		        if($subcontractor_insurance_certificate_key){
		            foreach ($img_rows as $rowdata) {
		                   if(!in_array($rowdata->id, $subcontractor_insurance_certificate_key)){
		                      Image::where('id', $rowdata->id)->delete();  
		                   }
		            }
		        }
		        else{
		            foreach ($img_rows as $rowdata) {
		                      Image::where('id', $rowdata->id)->delete();  
		            }
		        }
	            if($request->subcontractor_insurance_certificate){
		            $subcontractor_insurance_certificate = $request->subcontractor_insurance_certificate;
		            $all_file = [];
		            $i = 0;
		            foreach($subcontractor_insurance_certificate as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'subcontractor_insurance_certificate',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }

	        if($request->signed){
	            $image_parts = explode(";base64,", $request->signed);
	        	$image_type_aux = explode("image/", $image_parts[0]);
	      		$image_type = $image_type_aux[1];
	      		$image_base64 = base64_decode($image_parts[1]);
	      		
	      		$original_name = 'sc_sign_' . time() . '_' .$subcontractor_agreement->id;
		        $file = storage_path() . '/app/public/files/' . $original_name . '.'.$image_type;
	    	    file_put_contents($file, $image_base64);

		        $sc_sign = Image::where('type', 'sc_sign')->where('parent_id', $request->p_key)->first();
		        if($sc_sign){
		        	Image::where('parent_id', $request->p_key)->update([
		        		'name' => $original_name . '.' . $image_type,
		        		'url' => url('/') . '/storage/files/' . $original_name . '.' . $image_type,
		        	]);
		        }
		        else{
		        	Image::create([
		        		'type' => 'sc_sign',
		                'parent_id' => $request->p_key,
		                'name' => $original_name . '.' . $image_type,
		        		'url' => url('/') . '/storage/files/' . $original_name . '.' . $image_type,
		        	]);
		        }
		    }


	            $request_type = 'subcontractor_agreement';
				$subject = 'Subcontractor Agreement Request';
				$body = $request->status == 2 ? 'Hello, A Subcontractor Agreement (SC-'.$subcontractor_agreement->id.') has been submitted by -'. $request->agreement_send_mail_to : 'Hello, A Subcontractor Agreement (SC-'.$subcontractor_agreement->id.') has been re-submitted by -'. $request->agreement_send_mail_to;
				$status = 3;
				$reject = '';
				Mail::to($request->submitted_by_email)->send(new AgreementEmail($subcontractor_agreement, $request_type, $subject, $body, $status, $reject));

				session()->flash('message', 'Successfully submitted.');
	            return redirect()->route('agreementMessage');

			}

			elseif($request->status == 3){

			if($request->submit == 'reject'){
				$subcontractor_agreement = SubcontractorAgreement::where('id', $request->p_key)->first();
				SubcontractorAgreement::where('id', $request->p_key)
																->update($request->except([
																	'tab', 
																	'url_token',
																	'submit',
																	'p_key',
																	'_token',
																	'submitted_to_insurance',
																	'submitted_date_insurance',
																])
															);
				$subcontractor_agreement->access_code =  mt_rand(100000,999999);
				$subcontractor_agreement->url_token =  $subcontractor_agreement->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);
				$subcontractor_agreement->status = 4;

				$arr = json_decode($subcontractor_agreement->sent, true);
				$arr[] = Carbon::now();
				$subcontractor_agreement->sent = json_encode($arr);
				
				$subcontractor_agreement->update();

				if($request->subcontractor_agreement_w9){
		            $subcontractor_agreement_w9 = $request->subcontractor_agreement_w9;
		            $all_file = [];
		            $i = 0;
		            foreach($subcontractor_agreement_w9 as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'subcontractor_agreement_w9',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }

	            if($request->subcontractor_insurance_certificate){
		            $subcontractor_insurance_certificate = $request->subcontractor_insurance_certificate;
		            $all_file = [];
		            $i = 0;
		            foreach($subcontractor_insurance_certificate as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'subcontractor_insurance_certificate',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }

	            $request_type = 'subcontractor_agreement';
				$subject = 'Subcontractor Agreement Request';
				$body = 'Hello, Your Subcontractor Agreement request (SC-'.$subcontractor_agreement->id.') has been rejected because -';
				$reject = $request->reject;
				$status = 4;
				Mail::to($subcontractor_agreement->agreement_send_mail_to)->send(new AgreementEmail($subcontractor_agreement, $request_type, $subject, $body, $status, $reject));

				session()->flash('success', 'Successfully submitted.');
	            return back();
			}
			else{
				$subcontractor_agreement = SubcontractorAgreement::where('id', $request->p_key)->first();

				SubcontractorAgreement::where('id', $request->p_key)
																->update($request->except([
																	'tab', 
																	'url_token',
																	'submit',
																	'p_key',
																	'_token'
																])
															);
				$subcontractor_agreement->access_code =  mt_rand(100000,999999);
				$subcontractor_agreement->url_token =  $subcontractor_agreement->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);

				$arr = json_decode($subcontractor_agreement->sent, true);
				$arr[] = Carbon::now();
				$subcontractor_agreement->sent = json_encode($arr);

				$subcontractor_agreement->update();

				if($request->subcontractor_agreement_w9){
		            $subcontractor_agreement_w9 = $request->subcontractor_agreement_w9;
		            $all_file = [];
		            $i = 0;
		            foreach($subcontractor_agreement_w9 as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'subcontractor_agreement_w9',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }

	            if($request->subcontractor_insurance_certificate){
		            $subcontractor_insurance_certificate = $request->subcontractor_insurance_certificate;
		            $all_file = [];
		            $i = 0;
		            foreach($subcontractor_insurance_certificate as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'subcontractor_insurance_certificate',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }

	            $request_type = 'subcontractor_agreement';
				$subject = 'Subcontractor Agreement Request';
				$body = 'Hello, A Subcontractor Agreement request (SC-'.$subcontractor_agreement->id.') has been finalised';
				$status = 5;
				$reject = '';
				Mail::to([$request->submitted_to_insurance, $request->agreement_send_mail_to])->send(new AgreementEmail($subcontractor_agreement, $request_type, $subject, $body, $status, $reject));
				//Mail::to($request->agreement_send_mail_to)->send(new AgreementEmail($subcontractor_agreement, $request_type, $subject, $body, $status, $reject));

				session()->flash('success', 'Successfully submitted.');
	            return back();
	        	}

			}

		}
	}

	public function rental_agreement($request, $type){
		if($type == 'store'){
			if($request->status == 1){
				$request->validate([
		            'ren_ag_send_mail_to' => 'required|email',
		        ]);

				$rental_agreement = RentalAgreement::create($request->except(['tab', 'rental_agreement_w9', 'rental_insurance_certificate']));
				
				$rental_agreement->access_code =  mt_rand(100000,999999);
				$rental_agreement->url_token =  $rental_agreement->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);

				$rental_agreement->sent = json_encode([Carbon::now()]);
				$rental_agreement->save();


				if($request->rental_agreement_w9){
	            $rental_agreement_w9 = $request->rental_agreement_w9;
	            $all_file = [];
	            $i = 0;
	            foreach($rental_agreement_w9 as $rowdata){
	                $i++;
	                $original_name = $rowdata->getClientOriginalName();
	                $disk_image_name = time() . $i .$original_name;  
	                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

	                $all_file[] = [
	                    'type' => 'rental_agreement_w9',
	                    'parent_id' => $rental_agreement->id,
	                    'name' => $original_name,
	                    'url' => url('/') . '/storage/files/' . $disk_image_name,
	                ]; 
	            }
	            Image::insert($all_file);
	            }

	            if($request->rental_insurance_certificate){
	            $rental_insurance_certificate = $request->rental_insurance_certificate;
	            $all_file = [];
	            $i = 0;
	            foreach($rental_insurance_certificate as $rowdata){
	                $i++;
	                $original_name = $rowdata->getClientOriginalName();
	                $disk_image_name = time() . $i .$original_name;  
	                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

	                $all_file[] = [
	                    'type' => 'rental_insurance_certificate',
	                    'parent_id' => $rental_agreement->id,
	                    'name' => $original_name,
	                    'url' => url('/') . '/storage/files/' . $disk_image_name,
	                ]; 
	            }
	            Image::insert($all_file);
	            }

	            $request_type = 'rental_agreement';
				$subject = 'Rental Agreement Request';
				$body = '<p>Thank you for renting from EV Rentals a division of EV Oilfield Services, LLC.  Please click the link below to complete your rental agreement and return the completed document to us as quickly as possible so we can activate your account and rental.</p><p>Thank you again for your business!</p>';
				$status = 2;
				$reject = '';
				Mail::to($request->ren_ag_send_mail_to)->send(new AgreementEmail($rental_agreement, $request_type, $subject, $body, $status, $reject));

				session()->flash('success', 'Successfully submitted.');  
	            return back()->withInput(['tab'=>'rental_agreement']);
        	}
        	elseif($request->status == 2 || $request->status == 4){
        		//dd($request->signed);

				$rental_agreement = RentalAgreement::where('id', $request->p_key)->first();
				RentalAgreement::where('id', $request->p_key)
																->update($request->except([
																	'tab', 
																	'url_token',
																	'rental_agreement_w9', 
																	'rental_insurance_certificate',
																	'preloaded',
																	'preloaded2',
																	'submit',
																	'p_key',
																	'_token',
																	'signed'
																])
															);
				$rental_agreement->access_code =  mt_rand(100000,999999);
				$rental_agreement->url_token =  $rental_agreement->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);
				$rental_agreement->status =  2;
				$rental_agreement->update();
				
				$rental_agreement_w9_key = $request->preloaded;
				//dd($subcontractor_agreement_w9_key);
		        $img_rows = Image::where('type', 'rental_agreement_w9')->where('parent_id', $request->p_key)->get();
		        if($rental_agreement_w9_key){
		            foreach ($img_rows as $rowdata) {
		                   if(!in_array($rowdata->id, $rental_agreement_w9_key)){
		                      Image::where('id', $rowdata->id)->delete();  
		                   }
		            }
		        }
		        else{
		            foreach ($img_rows as $rowdata) {
		                      Image::where('id', $rowdata->id)->delete();  
		            }
		        }
				if($request->rental_agreement_w9){
		            $rental_agreement_w9 = $request->rental_agreement_w9;
		            $all_file = [];
		            $i = 0;
		            foreach($rental_agreement_w9 as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'rental_agreement_w9',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }


	            $rental_insurance_certificate_key = $request->preloaded2;
		        $img_rows = Image::where('type', 'rental_insurance_certificate')->where('parent_id', $request->p_key)->get();
		        if($rental_insurance_certificate_key){
		            foreach ($img_rows as $rowdata) {
		                   if(!in_array($rowdata->id, $rental_insurance_certificate_key)){
		                      Image::where('id', $rowdata->id)->delete();  
		                   }
		            }
		        }
		        else{
		            foreach ($img_rows as $rowdata) {
		                      Image::where('id', $rowdata->id)->delete();  
		            }
		        }
	            if($request->rental_insurance_certificate){
		            $rental_insurance_certificate = $request->rental_insurance_certificate;
		            $all_file = [];
		            $i = 0;
		            foreach($rental_insurance_certificate as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'rental_insurance_certificate',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }

	        if($request->signed){
	            $image_parts = explode(";base64,", $request->signed);
	        	$image_type_aux = explode("image/", $image_parts[0]);
	      		$image_type = $image_type_aux[1];
	      		$image_base64 = base64_decode($image_parts[1]);
	      		
	      		$original_name = 'rental_sign_' . time() . '_' .$rental_agreement->id; 
		        $file = storage_path() . '/app/public/files/' . $original_name . '.'.$image_type;
	    	    file_put_contents($file, $image_base64);

	            $rental_sign = Image::where('type', 'rental_sign')->where('parent_id', $request->p_key)->first();
		        if($rental_sign){
		        	Image::where('parent_id', $request->p_key)->update([
		        		'name' => $original_name . '.' . $image_type,
		        		'url' => url('/') . '/storage/files/' . $original_name . '.' . $image_type,
		        	]);
		        }
		        else{
		        	Image::create([
		        		'type' => 'rental_sign',
		                'parent_id' => $request->p_key,
		                'name' => $original_name . '.' . $image_type,
		        		'url' => url('/') . '/storage/files/' . $original_name . '.' . $image_type,
		        	]);
		        }
		    }



	            $request_type = 'rental_agreement';
				$subject = 'Rental Agreement Request';
				$body = $request->status == 2 ? 'Hello, A Rental Agreement (RN-'.$rental_agreement->id.') has been submitted by -'. $request->ren_ag_send_mail_to : 'Hello, A Rental Agreement (RN-'.$rental_agreement->id.') has been re-submitted by -'. $request->ren_ag_send_mail_to;
				$status = 3;
				$reject = '';

				Mail::to($request->submitted_by_email)->send(new AgreementEmail($rental_agreement, $request_type, $subject, $body, $status, $reject));

				session()->flash('message', 'Successfully submitted.');
	            return redirect()->route('agreementMessage');

			}

			elseif($request->status == 3){

			if($request->submit == 'reject'){
				$rental_agreement = RentalAgreement::where('id', $request->p_key)->first();
				RentalAgreement::where('id', $request->p_key)
																->update($request->except([
																	'tab', 
																	'url_token',
																	'submit',
																	'p_key',
																	'_token',
																	'submitted_to_insurance',
																	'submitted_date_insurance'
																])
															);
				$rental_agreement->access_code =  mt_rand(100000,999999);
				$rental_agreement->url_token =  $rental_agreement->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);
				$rental_agreement->status = 4;

				$arr = json_decode($rental_agreement->sent, true);
				$arr[] = Carbon::now();
				$rental_agreement->sent = json_encode($arr);

				$rental_agreement->update();

				if($request->rental_agreement_w9){
		            $rental_agreement_w9 = $request->rental_agreement_w9;
		            $all_file = [];
		            $i = 0;
		            foreach($rental_agreement_w9 as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'rental_agreement_w9',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }

	            if($request->rental_insurance_certificate){
		            $rental_insurance_certificate = $request->rental_insurance_certificate;
		            $all_file = [];
		            $i = 0;
		            foreach($rental_insurance_certificate as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'rental_insurance_certificate',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }

	            $request_type = 'rental_agreement';
				$subject = 'Rental Agreement Request';
				$body = 'Hello, Your Rental Agreement request (RN-'.$rental_agreement->id.') has been rejected because -';
				$reject = $request->reject;
				$status = 4;
				Mail::to($rental_agreement->ren_ag_send_mail_to)->send(new AgreementEmail($rental_agreement, $request_type, $subject, $body, $status, $reject));

				session()->flash('success', 'Successfully submitted.');
	            return back();
			}
			else{
				$rental_agreement = RentalAgreement::where('id', $request->p_key)->first();

				RentalAgreement::where('id', $request->p_key)
																->update($request->except([
																	'tab', 
																	'url_token',
																	'submit',
																	'p_key',
																	'_token'
																])
															);
				$rental_agreement->access_code =  mt_rand(100000,999999);
				$rental_agreement->url_token =  $rental_agreement->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);

				$arr = json_decode($rental_agreement->sent, true);
				$arr[] = Carbon::now();
				$rental_agreement->sent = json_encode($arr);

				$rental_agreement->update();

				if($request->rental_agreement_w9){
		            $rental_agreement_w9 = $request->rental_agreement_w9;
		            $all_file = [];
		            $i = 0;
		            foreach($rental_agreement_w9 as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'rental_agreement_w9',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }

	            if($request->rental_insurance_certificate){
		            $rental_insurance_certificate = $request->rental_insurance_certificate;
		            $all_file = [];
		            $i = 0;
		            foreach($rental_insurance_certificate as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'rental_insurance_certificate',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }

	            $request_type = 'rental_agreement';
				$subject = 'Rental Agreement Request';
				$body = 'Hello, A Rental Agreement request (RN-'.$rental_agreement->id.') has been finalised';
				$status = 5;
				$reject = '';
				Mail::to([$request->submitted_to_insurance, $request->ren_ag_send_mail_to])->send(new AgreementEmail($rental_agreement, $request_type, $subject, $body, $status, $reject));

				session()->flash('success', 'Successfully submitted.');
	            return back();
	        	}

			}

		}
	}


	public function owner_agreement($request, $type){
		if($type == 'store'){
			if($request->status == 1){
				$request->validate([
		            'agreement_send_mail_to' => 'required|email',
		        ]);
		        
				$owner_agreement = OwnerAgreement::create($request->except(['tab', 'owner_w9', 'owner_operator_comprehensive', 'owner_vehicle_reg', 'owner_annual_insp_cer', 'owner_vehicle_use_form']));
				
				$owner_agreement->access_code =  mt_rand(100000,999999);
				$owner_agreement->url_token =  $owner_agreement->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);

				$owner_agreement->sent = json_encode([Carbon::now()]);
				$owner_agreement->save();


	            $request_type = 'owner_agreement';
				$subject = 'Owner Operator Agreement Request';
				$body = 'Thank you for the opportunity to work with EV Oilfield Services, LLC as a Owner Operator.  Please find below the link to our Owner Agreement.  Please fill it out completely and return to us as soon as possible so we can set up in our system as an authorized EV Owner Operator.  Thank you again, and we look forward to working with you!';
				$status = 2;
				$reject = '';
				Mail::to($request->agreement_send_mail_to)->send(new AgreementEmail($owner_agreement, $request_type, $subject, $body, $status, $reject));

				session()->flash('success', 'Successfully submitted.');  
	            return back()->withInput(['tab'=>'owner_agreement']);
        	}
        	elseif($request->status == 2 || $request->status == 4){
				$owner_agreement = OwnerAgreement::where('id', $request->p_key)->first();
				OwnerAgreement::where('id', $request->p_key)
																->update($request->except([
																	'tab', 
																	'url_token',
																	'owner_w9', 
																	'owner_operator_comprehensive', 
																	'owner_vehicle_reg', 
																	'owner_annual_insp_cer', 
																	'owner_vehicle_use_form',
																	'preloaded_owner_w9',
																	'preloaded_owner_operator_comprehensive',
																	'preloaded_owner_vehicle_reg', 
																	'preloaded_owner_annual_insp_cer', 
																	'preloaded_owner_vehicle_use_form',
																	'submit',
																	'p_key',
																	'_token',
																	'signed'
																])
															);
				$owner_agreement->access_code =  mt_rand(100000,999999);
				$owner_agreement->url_token =  $owner_agreement->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);
				$owner_agreement->status =  2;
				$owner_agreement->update();
				
				$owner_w9_key = $request->preloaded_owner_w9;
		        $img_rows = Image::where('type', 'owner_w9')->where('parent_id', $request->p_key)->get();
		        if($owner_w9_key){
		            foreach ($img_rows as $rowdata) {
		                   if(!in_array($rowdata->id, $owner_w9_key)){
		                      Image::where('id', $rowdata->id)->delete();  
		                   }
		            }
		        }
		        else{
		            foreach ($img_rows as $rowdata) {
		                      Image::where('id', $rowdata->id)->delete();  
		            }
		        }
				if($request->owner_w9){
		            $owner_w9 = $request->owner_w9;
		            $all_file = [];
		            $i = 0;
		            foreach($owner_w9 as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'owner_w9',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }


	            $owner_operator_comprehensive_key = $request->preloaded_owner_operator_comprehensive;
		        $img_rows = Image::where('type', 'owner_operator_comprehensive')->where('parent_id', $request->p_key)->get();
		        if($owner_operator_comprehensive_key){
		            foreach ($img_rows as $rowdata) {
		                   if(!in_array($rowdata->id, $owner_operator_comprehensive_key)){
		                      Image::where('id', $rowdata->id)->delete();  
		                   }
		            }
		        }
		        else{
		            foreach ($img_rows as $rowdata) {
		                      Image::where('id', $rowdata->id)->delete();  
		            }
		        }
	            if($request->owner_operator_comprehensive){
		            $owner_operator_comprehensive = $request->owner_operator_comprehensive;
		            $all_file = [];
		            $i = 0;
		            foreach($owner_operator_comprehensive as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'owner_operator_comprehensive',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }


	            $owner_annual_insp_cer_key = $request->preloaded_owner_annual_insp_cer;
		        $img_rows = Image::where('type', 'owner_annual_insp_cer')->where('parent_id', $request->p_key)->get();
		        if($owner_annual_insp_cer_key){
		            foreach ($img_rows as $rowdata) {
		                   if(!in_array($rowdata->id, $owner_annual_insp_cer_key)){
		                      Image::where('id', $rowdata->id)->delete();  
		                   }
		            }
		        }
		        else{
		            foreach ($img_rows as $rowdata) {
		                      Image::where('id', $rowdata->id)->delete();  
		            }
		        }
	            if($request->owner_annual_insp_cer){
		            $owner_annual_insp_cer = $request->owner_annual_insp_cer;
		            $all_file = [];
		            $i = 0;
		            foreach($owner_annual_insp_cer as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'owner_annual_insp_cer',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }


	            $owner_vehicle_reg_key = $request->preloaded_owner_vehicle_reg;
		        $img_rows = Image::where('type', 'owner_vehicle_reg')->where('parent_id', $request->p_key)->get();
		        if($owner_vehicle_reg_key){
		            foreach ($img_rows as $rowdata) {
		                   if(!in_array($rowdata->id, $owner_vehicle_reg_key)){
		                      Image::where('id', $rowdata->id)->delete();  
		                   }
		            }
		        }
		        else{
		            foreach ($img_rows as $rowdata) {
		                      Image::where('id', $rowdata->id)->delete();  
		            }
		        }
	            if($request->owner_vehicle_reg){
		            $owner_vehicle_reg = $request->owner_vehicle_reg;
		            $all_file = [];
		            $i = 0;
		            foreach($owner_vehicle_reg as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'owner_vehicle_reg',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }


	            $owner_vehicle_use_form_key = $request->preloaded_owner_vehicle_use_form;
		        $img_rows = Image::where('type', 'owner_vehicle_use_form')->where('parent_id', $request->p_key)->get();
		        if($owner_vehicle_use_form_key){
		            foreach ($img_rows as $rowdata) {
		                   if(!in_array($rowdata->id, $owner_vehicle_use_form_key)){
		                      Image::where('id', $rowdata->id)->delete();  
		                   }
		            }
		        }
		        else{
		            foreach ($img_rows as $rowdata) {
		                      Image::where('id', $rowdata->id)->delete();  
		            }
		        }
	            if($request->owner_vehicle_use_form){
		            $owner_vehicle_use_form = $request->owner_vehicle_use_form;
		            $all_file = [];
		            $i = 0;
		            foreach($owner_vehicle_use_form as $rowdata){
		                $i++;
		                $original_name = $rowdata->getClientOriginalName();
		                $disk_image_name = time() . $i .$original_name;  
		                $rowdata->move(storage_path('app/public/files'), $disk_image_name); 

		                $all_file[] = [
		                    'type' => 'owner_vehicle_use_form',
		                    'parent_id' => $request->p_key,
		                    'name' => $original_name,
		                    'url' => url('/') . '/storage/files/' . $disk_image_name,
		                ]; 
		            }
		            Image::insert($all_file);
	            }

	        if($request->signed){
	            $image_parts = explode(";base64,", $request->signed);
	        	$image_type_aux = explode("image/", $image_parts[0]);
	      		$image_type = $image_type_aux[1];
	      		$image_base64 = base64_decode($image_parts[1]);
	      		
	      		$original_name = 'owner_sign_' . time() . '_' .$owner_agreement->id;
		        $file = storage_path() . '/app/public/files/' . $original_name . '.'.$image_type;
	    	    file_put_contents($file, $image_base64);

		        $owner_sign = Image::where('type', 'owner_sign')->where('parent_id', $request->p_key)->first();
		        if($owner_sign){
		        	Image::where('parent_id', $request->p_key)->update([
		        		'name' => $original_name . '.' . $image_type,
		        		'url' => url('/') . '/storage/files/' . $original_name . '.' . $image_type,
		        	]);
		        }
		        else{
		        	Image::create([
		        		'type' => 'owner_sign',
		                'parent_id' => $request->p_key,
		                'name' => $original_name . '.' . $image_type,
		        		'url' => url('/') . '/storage/files/' . $original_name . '.' . $image_type,
		        	]);
		        }
		    }


	            $request_type = 'owner_agreement';
				$subject = 'Owner Operator Agreement Request';
				$body = $request->status == 2 ? 'Hello, A Owner Agreement (OA-'.$owner_agreement->id.') has been submitted by -'. $request->agreement_send_mail_to : 'Hello, A Owner Agreement (OA-'.$owner_agreement->id.') has been re-submitted by -'. $request->agreement_send_mail_to;
				$status = 3;
				$reject = '';
				Mail::to($request->submitted_by_email)->send(new AgreementEmail($owner_agreement, $request_type, $subject, $body, $status, $reject));

				session()->flash('message', 'Successfully submitted.');
	            return redirect()->route('agreementMessage');

			}

			elseif($request->status == 3){

			if($request->submit == 'reject'){
				$owner_agreement = OwnerAgreement::where('id', $request->p_key)->first();
				OwnerAgreement::where('id', $request->p_key)
																->update($request->except([
																	'tab', 
																	'url_token',
																	'submit',
																	'p_key',
																	'_token',
																	'submitted_to_insurance',
																	'submitted_date_insurance',
																])
															);
				$owner_agreement->access_code =  mt_rand(100000,999999);
				$owner_agreement->url_token =  $owner_agreement->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);
				$owner_agreement->status = 4;

				$arr = json_decode($owner_agreement->sent, true);
				$arr[] = Carbon::now();
				$owner_agreement->sent = json_encode($arr);
				
				$owner_agreement->update();

	            $request_type = 'owner_agreement';
				$subject = 'Owner Operator Agreement Request';
				$body = 'Hello, Your Owner Agreement request (OA-'.$owner_agreement->id.') has been rejected because -';
				$reject = $request->reject;
				$status = 4;
				Mail::to($owner_agreement->agreement_send_mail_to)->send(new AgreementEmail($owner_agreement, $request_type, $subject, $body, $status, $reject));

				session()->flash('success', 'Successfully submitted.');
	            return back();
			}
			else{
				$owner_agreement = OwnerAgreement::where('id', $request->p_key)->first();

				OwnerAgreement::where('id', $request->p_key)
																->update($request->except([
																	'tab', 
																	'url_token',
																	'submit',
																	'p_key',
																	'_token'
																])
															);
				$owner_agreement->access_code =  mt_rand(100000,999999);
				$owner_agreement->url_token =  $owner_agreement->id .'-'. substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);

				$arr = json_decode($owner_agreement->sent, true);
				$arr[] = Carbon::now();
				$owner_agreement->sent = json_encode($arr);

				$owner_agreement->update();


	            $request_type = 'owner_agreement';
				$subject = 'Owner Operator Agreement Request';
				$body = 'Hello, A Owner Agreement request (OA-'.$owner_agreement->id.') has been finalised';
				$status = 5;
				$reject = '';
				Mail::to([$request->submitted_to_insurance, $request->agreement_send_mail_to])->send(new AgreementEmail($owner_agreement, $request_type, $subject, $body, $status, $reject));

				session()->flash('success', 'Successfully submitted.');
	            return back();
	        	}

			}

		}
	}


	public function show($id){
		if($_GET['type'] == 'equipment'){
            $data = InsuranceEquipment::find($id);
            $type = 'equipment';            
            return view('admin.insurance.show', compact('data', 'type'));
        }
        elseif($_GET['type'] == 'vehicle'){
            $data = InsuranceVehicle::find($id);
            $type = 'vehicle';
            return view('admin.insurance.show', compact('data', 'type'));
        }
        elseif($_GET['type'] == 'driver'){
            $data = InsuranceDriver::find($id);
            $type = 'driver';

            $files = Image::where('type', 'driver-insurance')->where('parent_id', $data->id)->get();
        	return view('admin.insurance.show', compact('data', 'type', 'files'));
        } 
        elseif($_GET['type'] == 'subcontractor_agreement'){
            $data = SubcontractorAgreement::find($id);
            $type = 'subcontractor_agreement';

            $agreement_files = Image::where('type', 'subcontractor_agreement_w9')->where('parent_id', $data->id)->get();
			$insurance_files = Image::where('type', 'subcontractor_insurance_certificate')->where('parent_id', $data->id)->get();
			$sc_sign = Image::where('type', 'sc_sign')->where('parent_id', $data->id)->first();
        	return view('admin.insurance.show', compact('data', 'type', 'agreement_files', 'insurance_files', 'sc_sign'));
        } 
        elseif($_GET['type'] == 'rental_agreement'){
            $data = RentalAgreement::find($id);
            $type = 'rental_agreement';

            $agreement_files = Image::where('type', 'rental_agreement_w9')->where('parent_id', $data->id)->get();
			$insurance_files = Image::where('type', 'rental_insurance_certificate')->where('parent_id', $data->id)->get();
			$rn_sign = Image::where('type', 'rental_sign')->where('parent_id', $data->id)->first();
        	return view('admin.insurance.show', compact('data', 'type', 'agreement_files', 'insurance_files', 'rn_sign'));
        }
        elseif($_GET['type'] == 'owner_agreement'){
            $data = OwnerAgreement::find($id);
            $type = 'owner_agreement';

            $owner_w9 = Image::where('type', 'owner_w9')->where('parent_id', $data->id)->get();
			$owner_operator_comprehensive = Image::where('type', 'owner_operator_comprehensive')->where('parent_id', $data->id)->get();
			$owner_vehicle_reg = Image::where('type', 'owner_vehicle_reg')->where('parent_id', $data->id)->get();
			$owner_annual_insp_cer = Image::where('type', 'owner_annual_insp_cer')->where('parent_id', $data->id)->get();
			$owner_vehicle_use_form = Image::where('type', 'owner_vehicle_use_form')->where('parent_id', $data->id)->get();
			$owner_sign = Image::where('type', 'owner_sign')->where('parent_id', $data->id)->first();
        	return view('admin.insurance.show', compact('data', 'type', 'owner_w9', 'owner_operator_comprehensive', 'owner_vehicle_reg', 'owner_annual_insp_cer', 'owner_vehicle_use_form', 'owner_sign'));
        }        
        //return view('admin.insurance.show', compact('data', 'type', 'files'));
	}

	public function edit($id){
		if($_GET['type'] == 'equipment'){
            $data = InsuranceEquipment::find($id);
            $type = 'equipment';            
        }
        elseif($_GET['type'] == 'vehicle'){
            $data = InsuranceVehicle::find($id);
            $type = 'vehicle';
        }
        elseif($_GET['type'] == 'driver'){
            $data = InsuranceDriver::find($id);
            $type = 'driver';
        } 

        $email = Auth::user()->email;
		$name = Auth::user()->name;

        return view('admin.insurance.edit', compact('data', 'type', 'email', 'name'));
	}

	public function destroy($id){
		if($_GET['type'] == 'equipment'){
            $data = InsuranceEquipment::find($id);
            $data->delete();            
        }
        elseif($_GET['type'] == 'vehicle'){
            $data = InsuranceVehicle::find($id);
            $data->delete();
            session()->flash('success', 'Successfully deleted');
            return back()->withInput(['tab'=>'manage_tab']);
        }
        elseif($_GET['type'] == 'driver'){
            $data = InsuranceDriver::find($id);
            $data->delete();
            session()->flash('success', 'Successfully deleted');
            return back()->withInput(['tab'=>'manage_tab']);
        }
        session()->flash('success', 'Successfully deleted');
        return back()->withInput(['tab'=>'manage_tab']);
	}

	public function insuranceToken($token){
		if($_GET['reqType'] == 'equipment'){
			$url_token = $token;
			$req_type = 'equipment';
			return view('admin.insurance.accessCode', compact('url_token', 'req_type'));
		}
		elseif($_GET['reqType'] == 'vehicle'){
			$url_token = $token;
			$req_type = 'vehicle';
			return view('admin.insurance.accessCode', compact('url_token', 'req_type'));
		}
		elseif($_GET['reqType'] == 'driver'){
			$url_token = $token;
			$req_type = 'driver';
			return view('admin.insurance.accessCode', compact('url_token', 'req_type'));
		}
		elseif($_GET['reqType'] == 'subcontractor_agreement'){
			$url_token = $token;
			$req_type = 'subcontractor_agreement';
			return view('admin.insurance.agreement_accessCode', compact('url_token', 'req_type'));
		}
		elseif($_GET['reqType'] == 'rental_agreement'){
			$url_token = $token;
			$req_type = 'rental_agreement';
			return view('admin.insurance.agreement_accessCode', compact('url_token', 'req_type'));
		}
		elseif($_GET['reqType'] == 'owner_agreement'){
			$url_token = $token;
			$req_type = 'owner_agreement';
			return view('admin.insurance.agreement_accessCode', compact('url_token', 'req_type'));
		}
		//return view('admin.insurance.accessCode', compact('url_token', 'req_type'));
	}

	public function getPdf(Request $request){
		if($request->req_type == 'equipment'){
		  $url_token = InsuranceEquipment::where('url_token', $request->url_token)->first();
		  if($url_token){
			  if($request->access_code != $url_token->token){
			  	session()->flash('error', 'Access Code is not matched');
			  	return back();
			  }
			  else{
			  	$data =InsuranceEquipment::where('url_token', $request->url_token)->first();

			  	$data->status = 1;
			  	$data->update();

		 		$pdf = PDF::loadView('admin.insurance.equipment_pdf', array('data' => $data))->setPaper('a4');  

				$output = $pdf->output();
				$response = Response::make($output,200);
				$response->header('Content-Type', 'application/pdf');
				return $response;
			  }
			}
			else{
				session()->flash('error', 'This insurance request is unavailable');
			  	return back();
			}
		}
		elseif($request->req_type == 'vehicle'){
		  $url_token = InsuranceVehicle::where('url_token', $request->url_token)->first();
		  if($url_token){
			  if($request->access_code != $url_token->token){
			  	session()->flash('error', 'Access Code is not matched');
			  	return back();
			  }
			  else{
			  	$data =InsuranceVehicle::where('url_token', $request->url_token)->first();

			  	$data->status = 1;
			  	$data->update();

		 		$pdf = PDF::loadView('admin.insurance.vehicle_pdf', array('data' => $data))->setPaper('a4');  

				$output = $pdf->output();
				$response = Response::make($output,200);
				$response->header('Content-Type', 'application/pdf');
				return $response;
			  }	
		  }
		  else{
				session()->flash('error', 'This insurance request is unavailable');
			  	return back();
			}
		}
		elseif($request->req_type == 'driver'){
			$url_token = InsuranceDriver::where('url_token', $request->url_token)->first();
			if($url_token){
				if($request->access_code != $url_token->token){
			  		session()->flash('error', 'Access Code is not matched');
			  		return back();
			    }
			    else{
			  	$data =InsuranceDriver::where('url_token', $request->url_token)->first();

			  	$data->status = 1;
			  	$data->update();

			  	$files = Image::where('type', 'driver-insurance')->where('parent_id', $data->id)->get();

		 		$pdf = PDF::loadView('admin.insurance.driver_pdf', array('data' => $data, 'files' => $files))->setPaper('a4');  

				$output = $pdf->output();
				$response = Response::make($output,200);
				$response->header('Content-Type', 'application/pdf');
				return $response;
			   }	
			}
			else{
				session()->flash('error', 'This insurance request is unavailable');
			  	return back();
			}
		}
	}

	public function agreement(Request $request){
		if($request->req_type == 'subcontractor_agreement'){
			//if($request->status == 1){
				$data = SubcontractorAgreement::where('url_token', $request->url_token)->first();
				if($data){
					if($request->access_code != $data->access_code){
				  		session()->flash('error', 'Access Code is not matched');
				  		return back();
				    }
				    else{
				    	//dd($_GET['reqType']);
				    	$status = $request->status;
				    
					    if($status == 5){
					    	$arr = json_decode($data->seen, true);
							$arr[] = Carbon::now();
							$data->seen = json_encode($arr);
							$data->save();

					    	$agreement_files = Image::where('type', 'subcontractor_agreement_w9')->where('parent_id', $data->id)->get();
					    	$insurance_files = Image::where('type', 'subcontractor_insurance_certificate')->where('parent_id', $data->id)->get();
					    	$sc_sign = Image::where('type', 'sc_sign')->where('parent_id', $data->id)->first();

					    	$pdf = PDF::loadView('admin.insurance.subcontractor_pdf', array('data' => $data, 'agreement_files' => $agreement_files, 'insurance_files' => $insurance_files, 'sc_sign' => $sc_sign))->setPaper('a4');  
							$output = $pdf->output();
							$response = Response::make($output,200);
							$response->header('Content-Type', 'application/pdf');
							return $response;
					    }
					    	
					    else{
						    if($status != 3){
						    	if(!$data->seen){
						    		$data->seen = json_encode([Carbon::now()]);
						    		$data->save();
						    	}
						    	else{
						    		$arr = json_decode($data->seen, true);
									$arr[] = Carbon::now();
									$data->seen = json_encode($arr);
									$data->save();
						    	}
						    }
					    	
					    	$url_token = $request->url_token;
					    	$sc_sign = Image::where('type', 'sc_sign')->where('parent_id', $data->id)->first();
					  		return view('admin.insurance.subcontractor_agreement', compact('status', 'data', 'url_token', 'sc_sign'));
					    }
				  	}
				}
				else{
					session()->flash('error', 'This subcontractor agreement request is unavailable');
			  		return back();
				}
			//}
		}

		if($request->req_type == 'rental_agreement'){
			//if($request->status == 1){
				$data = RentalAgreement::where('url_token', $request->url_token)->first();
				if($data){
					if($request->access_code != $data->access_code){
				  		session()->flash('error', 'Access Code is not matched');
				  		return back();
				    }
				    else{
				    	//dd($_GET['reqType']);
				    	$status = $request->status;
				    
					    if($status == 5){
					    	$arr = json_decode($data->seen, true);
							$arr[] = Carbon::now();
							$data->seen = json_encode($arr);
							$data->save();

					    	$agreement_files = Image::where('type', 'rental_agreement_w9')->where('parent_id', $data->id)->get();
					    	$insurance_files = Image::where('type', 'rental_insurance_certificate')->where('parent_id', $data->id)->get();
					    	$rn_sign = Image::where('type', 'rental_sign')->where('parent_id', $data->id)->first();

					    	$pdf = PDF::loadView('admin.insurance.rental_pdf', array('data' => $data, 'agreement_files' => $agreement_files, 'insurance_files' => $insurance_files, 'rn_sign' => $rn_sign))->setPaper('a4');  
							$output = $pdf->output();
							$response = Response::make($output,200);
							$response->header('Content-Type', 'application/pdf');
							return $response;
					    }
					    	
					    else{
					    	if($status != 3){
						    	if(!$data->seen){
						    		$data->seen = json_encode([Carbon::now()]);
						    		$data->save();
						    	}
						    	else{
						    		$arr = json_decode($data->seen, true);
									$arr[] = Carbon::now();
									$data->seen = json_encode($arr);
									$data->save();
						    	}
						    }

					    	$url_token = $request->url_token;
					    	$rn_sign = Image::where('type', 'rental_sign')->where('parent_id', $data->id)->first();
					  		return view('admin.insurance.rental_agreement', compact('status', 'data', 'url_token', 'rn_sign'));
					    }
				  	}
				}
				else{
					session()->flash('error', 'This rental agreement request is unavailable');
			  		return back();
				}
			//}
		}

		if($request->req_type == 'owner_agreement'){
			//if($request->status == 1){
				$data = OwnerAgreement::where('url_token', $request->url_token)->first();
				if($data){
					if($request->access_code != $data->access_code){
				  		session()->flash('error', 'Access Code is not matched');
				  		return back();
				    }
				    else{
				    	//dd($_GET['reqType']);
				    	$status = $request->status;
				    
					    if($status == 5){
					    	$arr = json_decode($data->seen, true);
							$arr[] = Carbon::now();
							$data->seen = json_encode($arr);
							$data->save();

					    	$owner_w9 = Image::where('type', 'owner_w9')->where('parent_id', $data->id)->get();
							$owner_operator_comprehensive = Image::where('type', 'owner_operator_comprehensive')->where('parent_id', $data->id)->get();
							$owner_vehicle_reg = Image::where('type', 'owner_vehicle_reg')->where('parent_id', $data->id)->get();
							$owner_annual_insp_cer = Image::where('type', 'owner_annual_insp_cer')->where('parent_id', $data->id)->get();
							$owner_vehicle_use_form = Image::where('type', 'owner_vehicle_use_form')->where('parent_id', $data->id)->get();
							$owner_sign = Image::where('type', 'owner_sign')->where('parent_id', $data->id)->first();

					    	$pdf = PDF::loadView('admin.insurance.owner_pdf', array('data' => $data, 'owner_w9' => $owner_w9, 'owner_operator_comprehensive' => $owner_operator_comprehensive, 'owner_vehicle_reg' => $owner_vehicle_reg, 'owner_annual_insp_cer' => $owner_annual_insp_cer, 'owner_vehicle_use_form' => $owner_vehicle_use_form, 'owner_sign' => $owner_sign))->setPaper('a4');  
							$output = $pdf->output();
							$response = Response::make($output,200);
							$response->header('Content-Type', 'application/pdf');
							return $response;
					    }
					    	
					    else{
						    if($status != 3){
						    	if(!$data->seen){
						    		$data->seen = json_encode([Carbon::now()]);
						    		$data->save();
						    	}
						    	else{
						    		$arr = json_decode($data->seen, true);
									$arr[] = Carbon::now();
									$data->seen = json_encode($arr);
									$data->save();
						    	}
						    }
					    	
					    	$url_token = $request->url_token;
					    	$owner_sign = Image::where('type', 'owner_sign')->where('parent_id', $data->id)->first();
					  		return view('admin.insurance.owner_agreement', compact('status', 'data', 'url_token', 'owner_sign'));
					    }
				  	}
				}
				else{
					session()->flash('error', 'This subcontractor agreement request is unavailable');
			  		return back();
				}
			//}
		}

	}

	public function autocomplete(Request $request)
    {
	     if($request->get('query'))
	     {
	      $value = $request->get('query');
	      $type = $request->type;
	      
	      if($type == 'equipment'){
	      	 $data = InsuranceEquipment::select('vin')->where('vin', 'LIKE', '%'.$value.'%')->paginate(15);
	      	 $output = '<ul class="results">';
		      foreach($data as $row)
		      {
		       $output .= '
		       <li class="suggestion"><a href="#">'.$row->vin.'</a></li>
		       ';
		      }
		      $output .= '</ul>';
		      echo $output;
	      }
	      if($type == 'vehicle'){
	      	 $data = InsuranceVehicle::select('vin')->where('vin', 'LIKE', '%'.$value.'%')->paginate(15);
	      	 $output = '<ul class="results">';
		      foreach($data as $row)
		      {
		       $output .= '
		       <li class="suggestion"><a href="#">'.$row->vin.'</a></li>
		       ';
		      }
		      $output .= '</ul>';
		      echo $output;
	      }
	      if($type == 'driver'){
	      	 $data = InsuranceDriver::select('license')->where('license', 'LIKE', '%'.$value.'%')->paginate(15);
	      	 $output = '<ul class="results">';
		      foreach($data as $row)
		      {
		       $output .= '
		       <li class="suggestion"><a href="#">'.$row->license.'</a></li>
		       ';
		      }
		      $output .= '</ul>';
		      echo $output;
	      }
	     }

    }


	public function findInsurance(Request $request){
		$type = $request->type;
		$value = $request->value;

		if($type == 'equipment'){
			$result = InsuranceEquipment::where('vin', $value)->orderBy('id', 'desc')->first();
			echo $result;
		}
		elseif($type == 'vehicle'){
			$result = InsuranceVehicle::where('vin', $value)->orderBy('id', 'desc')->first();	
			echo $result;
		}
		elseif($type == 'driver'){
			$result = InsuranceDriver::where('license', $value)->orderBy('id', 'desc')->first();
			if($result)	
				$files = Image::where('parent_id', $result->id)->where('type', 'driver-insurance')->get();
			else
				$files = '';

			$return_arr[] = array('result'=>$result, 'files'=>$files);

			echo json_encode($return_arr);
		}

		
	}



	public function generateInsuranceReport(Request $request){
		/*$type = $request->insurace_type_report;
		$from = $request->insurance_report_from;
		$to = $request->insurance_report_to;*/

		$type = $request->type;
		$from = $request->from;
		$to = $request->to;

		//return (new InsuranceExport($type, $from, $to))->download('insurance.xlsx', \Maatwebsite\Excel\Excel::XLSX);

		$myFile = Excel::raw(new InsuranceExport($type, $from, $to), \Maatwebsite\Excel\Excel::XLSX);
        
        $response =  array(
           'name' => "insurance", //no extention needed
           'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($myFile) //mime type of used format
        );
        
        return response()->json($response);

	}


	public function generateAgreementReport(Request $request){
		/*$type = $request->insurace_type_report;
		$from = $request->insurance_report_from;
		$to = $request->insurance_report_to;*/

		$type = $request->type;
		$from = $request->from;
		$to = $request->to;

		//return (new InsuranceExport($type, $from, $to))->download('insurance.xlsx', \Maatwebsite\Excel\Excel::XLSX);

		$myFile = Excel::raw(new AgreementExport($type, $from, $to), \Maatwebsite\Excel\Excel::XLSX);
        
        $response =  array(
           'name' => "agreement", //no extention needed
           'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($myFile) //mime type of used format
        );
        
        return response()->json($response);

	}

	public function subcontractor_pdf(){
		$pdf = PDF::loadView('admin.insurance.subcontractor_pdf')->setPaper('a4');  
		$output = $pdf->output();
		$response = Response::make($output,200);
		$response->header('Content-Type', 'application/pdf');
		return $response;
	
	}

	public function rental_pdf(){
		$pdf = PDF::loadView('admin.insurance.rental_pdf')->setPaper('a4');  
		$output = $pdf->output();
		$response = Response::make($output,200);
		$response->header('Content-Type', 'application/pdf');
		return $response;
	
	}

	public function owner_pdf(){
		$pdf = PDF::loadView('admin.insurance.owner_pdf')->setPaper('a4');  
		$output = $pdf->output();
		$response = Response::make($output,200);
		$response->header('Content-Type', 'application/pdf');
		return $response;
	
	}

	public function agreementMessage(){
		return view('admin.insurance.agreementView');
	}

	public function getInsuranceImage(Request $request){
		$get_images = Image::where('type', $request->type)
              ->where('parent_id', $request->value)
              ->orderBy('id', 'asc')
              ->get();

        $images = null;
        foreach ($get_images as $image) {
          $images[] = [
              'file' => $image->url,
              'id' =>  $image->id,
              'name' =>  $image->name,
                ];
            }

        echo json_encode($images);
	}

	public function agreementPdf($id){
        //dd($id);

        if($_GET['type'] == 'subcontractor'){
        	$data = SubcontractorAgreement::where('id', $id)->first();
        	
        	$agreement_files = Image::where('type', 'subcontractor_agreement_w9')->where('parent_id', $data->id)->get();
	    	$insurance_files = Image::where('type', 'subcontractor_insurance_certificate')->where('parent_id', $data->id)->get();
	    	$sc_sign = Image::where('type', 'sc_sign')->where('parent_id', $data->id)->first();

	    	$pdf = PDF::loadView('admin.insurance.subcontractor_pdf', array('data' => $data, 'agreement_files' => $agreement_files, 'insurance_files' => $insurance_files, 'sc_sign' => $sc_sign))->setPaper('a4');  
			$output = $pdf->output();
			$response = Response::make($output,200);
			$response->header('Content-Type', 'application/pdf');
			return $response;
        }
        else if($_GET['type'] == 'rental'){
        	$data = RentalAgreement::where('id', $id)->first();
        	
        	$agreement_files = Image::where('type', 'rental_agreement_w9')->where('parent_id', $data->id)->get();
	    	$insurance_files = Image::where('type', 'rental_insurance_certificate')->where('parent_id', $data->id)->get();
	    	$rn_sign = Image::where('type', 'rental_sign')->where('parent_id', $data->id)->first();

	    	$pdf = PDF::loadView('admin.insurance.rental_pdf', array('data' => $data, 'agreement_files' => $agreement_files, 'insurance_files' => $insurance_files, 'rn_sign' => $rn_sign))->setPaper('a4');  
			$output = $pdf->output();
			$response = Response::make($output,200);
			$response->header('Content-Type', 'application/pdf');
			return $response;
        }
        else if($_GET['type'] == 'owner_operator'){
        	$data = OwnerAgreement::where('id', $id)->first();
        	
        	$owner_w9 = Image::where('type', 'owner_w9')->where('parent_id', $data->id)->get();
			$owner_operator_comprehensive = Image::where('type', 'owner_operator_comprehensive')->where('parent_id', $data->id)->get();
			$owner_vehicle_reg = Image::where('type', 'owner_vehicle_reg')->where('parent_id', $data->id)->get();
			$owner_annual_insp_cer = Image::where('type', 'owner_annual_insp_cer')->where('parent_id', $data->id)->get();
			$owner_vehicle_use_form = Image::where('type', 'owner_vehicle_use_form')->where('parent_id', $data->id)->get();
			$owner_sign = Image::where('type', 'owner_sign')->where('parent_id', $data->id)->first();

	    	$pdf = PDF::loadView('admin.insurance.owner_pdf', array('data' => $data, 'owner_w9' => $owner_w9, 'owner_operator_comprehensive' => $owner_operator_comprehensive, 'owner_vehicle_reg' => $owner_vehicle_reg, 'owner_annual_insp_cer' => $owner_annual_insp_cer, 'owner_vehicle_use_form' => $owner_vehicle_use_form, 'owner_sign' => $owner_sign))->setPaper('a4');  
			$output = $pdf->output();
			$response = Response::make($output,200);
			$response->header('Content-Type', 'application/pdf');
			return $response;
        }
        

    }


}
