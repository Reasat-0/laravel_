<?php

namespace App\Exports;

use App\Models\InsuraceEquipment;
use App\Models\InsuraceVehicle;
use App\Models\InsuraceDriver;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class InsuranceExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithEvents
{
    use Exportable;

    public function __construct($type, $from, $to)
    {
        $this->type = $type;
        $this->from = $from;
        $this->to = $to;
    }

    public function headings(): array
    {
        return [
           'Request Type',
           'Name',
           'Action',
           'Liability',
           'Collision',
           'Sent',
           'Insured',
        ];
    }

    public function collection()
    {	
    	$from_date = \DateTime::createFromFormat('d/m/Y', $this->from)->format('Y-m-d');
    	$to_date = \DateTime::createFromFormat('d/m/Y', $this->to)->format('Y-m-d');

    	if($this->type == 'all'){
    		$equipment_reqs = collect(\App\Models\InsuranceEquipment::where('status', 1)->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date)->cursor());
			$vehicle_reqs = collect(\App\Models\InsuranceVehicle::where('status', 1)->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date)->cursor());
			$driver_reqs = collect(\App\Models\InsuranceDriver::where('status', 1)->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date)->cursor());

			$all = $equipment_reqs->merge($vehicle_reqs)->merge($driver_reqs)->sortBy('created_at');

			$all_reqs = $all->values();
			return $all_reqs;
    	}

    	else if($this->type == 'equipment'){
    		$equipment = collect(\App\Models\InsuranceEquipment::where('status', 1)->orderBy('created_at', 'asc')->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date)->cursor());
    		return $equipment;
    	}

    	else if($this->type == 'vehicle'){
    		$vehicle = collect(\App\Models\InsuranceVehicle::where('status', 1)->orderBy('created_at', 'asc')->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date)->cursor());
    		return $vehicle;
    	}

    	else if($this->type == 'driver'){
    		$driver = collect(\App\Models\InsuranceDriver::where('status', 1)->orderBy('created_at', 'asc')->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date)->cursor());
    		return $driver;
    	}

    }

    public function map($insurance): array
   {
   		if($insurance->getTable() == 'insurance_equipment_request'){
   			$request_type = 'Equipment';
   			$name = $insurance->vin;
            $liability = 'N/A';
			$collision = 'N/A';
		}
   		else if($insurance->getTable() == 'insurance_vehicle_request'){
   			$request_type = 'Vehicle';
   			$name = $insurance->vin;
            $liability = $insurance->liability;
			$collision = $insurance->collision;
		}
   		else if($insurance->getTable() == 'insurance_driver_request'){
   			$request_type = 'Driver';
   			$name = $insurance->driver_first_name .' '. $insurance->driver_last_name;
            $liability = 'N/A';
			$collision = 'N/A';
		}

        return [
            $request_type,
            $name,
            $insurance->request == 'delete' ? strtoupper($insurance->request).'D' : strtoupper($insurance->request).'ED',
            $liability,
            $collision,
            \Carbon\Carbon::parse($insurance->created_at)->timezone('America/New_York')->format('m/d/y g:i A'),
            \Carbon\Carbon::parse($insurance->updated_at)->timezone('America/New_York')->format('m/d/y g:i A'),
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}
