<?php

namespace App\Exports;

use App\Models\InsuraceEquipment;
use App\Models\InsuraceVehicle;
use App\Models\InsuraceDriver;
use App\Models\SubcontractorAgreement;
use App\Models\RentalAgreement;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class AgreementExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithEvents
{
    use Exportable;

    public function __construct($type, $from, $to)
    {
        $this->type = $type;
        $this->from = $from;
        $this->to = $to;
    }

    public function headings(): array
    {
        return [
           'Request Type',
           'Name',
           'Action',
           'Uploaded Documents',
           'Sent',
           'Insured',
        ];
    }

    public function collection()
    {	
    	$from_date = \DateTime::createFromFormat('d/m/Y', $this->from)->format('Y-m-d');
    	$to_date = \DateTime::createFromFormat('d/m/Y', $this->to)->format('Y-m-d');

    	if($this->type == 'all'){
    		$subcontractor_agreement = collect(\App\Models\SubcontractorAgreement::whereDate('created_at', '>=', $from_date)->whereDate('created_at', '<=', $to_date)->cursor());
			  $rental_agreement = collect(\App\Models\RentalAgreement::whereDate('created_at', '>=', $from_date)->whereDate('created_at', '<=', $to_date)->cursor());

        $owner_agreement = collect(\App\Models\OwnerAgreement::whereDate('created_at', '>=', $from_date)->whereDate('created_at', '<=', $to_date)->cursor());

			$all = $subcontractor_agreement->merge($rental_agreement)->merge($owner_agreement)->sortBy('created_at');

			$all_reqs = $all->values();
			return $all_reqs;
    	}

    	else if($this->type == 'subcontractor_agreement'){
    		$subcontractor_agreement = collect(\App\Models\SubcontractorAgreement::orderBy('created_at', 'asc')->whereDate('created_at', '>=', $from_date)->whereDate('created_at', '<=', $to_date)->cursor());
    		return $subcontractor_agreement;
    	}

    	else if($this->type == 'rental_agreement'){
    		$rental_agreement = collect(\App\Models\RentalAgreement::orderBy('created_at', 'asc')->whereDate('created_at', '>=', $from_date)->whereDate('created_at', '<=', $to_date)->cursor());
    		return $rental_agreement;
    	}

      else if($this->type == 'owner_agreement'){
        $owner_agreement = collect(\App\Models\OwnerAgreement::orderBy('created_at', 'asc')->whereDate('created_at', '>=', $from_date)->whereDate('created_at', '<=', $to_date)->cursor());
        return $owner_agreement;
      }

    }

    public function map($agreement): array
   {
   		if($agreement->getTable() == 'subcontractor_agreement'){
   			$request_type = 'Subcontractor Agreement';
   			$name = $agreement->subcontractor_name;
        //$w3 = \App\Models\Image::where('type', 'subcontractor_agreement_w9')->where('parent_id', $agreement->id)->first();
			  //$insurance_certificate = \App\Models\Image::where('type', 'subcontractor_insurance_certificate')->where('parent_id', $agreement->id)->first();

        $w9 = isset($agreement->getW9) ? (count($agreement->getW9) ? 'w9, ' : '') : '';
        $insurance_certificate = isset($agreement->getInsuranceCertificate) ? (count($agreement->getInsuranceCertificate) ? 'Ins. Cert.,  ' : '') : '';

        $uploaded_doc = $w9 . $insurance_certificate;
		  }
   		else if($agreement->getTable() == 'rental_agreement'){
   			$request_type = 'Rental Agreement';
   			$name = $agreement->ren_ag_executed_lessee_name;
        
        $w9 = isset($agreement->getW9) ? (count($agreement->getW9) ? 'w9, ' : '') : '';
        $insurance_certificate = isset($agreement->getInsuranceCertificate) ? (count($agreement->getInsuranceCertificate) ? 'Ins. Cert.,  ' : '') : '';

        $uploaded_doc = $w9 . $insurance_certificate;
		  }

      else if($agreement->getTable() == 'owner_agreement'){
        $request_type = 'Owner Agreement';
        $name = $agreement->owner_name;

        $w9 = isset($agreement->getW9) ? (count($agreement->getW9) ? 'w9, ' : ''): '';
        $owner_operator_comprehensive = isset($agreement->owner_operator_comprehensive) ? (count($agreement->owner_operator_comprehensive) ? 'Comp./Bobtail Ins., ' : ''): '';
        $owner_vehicle_reg = isset($agreement->owner_vehicle_reg) ? (count($agreement->owner_vehicle_reg) ? 'Veh. Reg., ' : ''): '';
        $owner_annual_insp_cer = isset($agreement->owner_annual_insp_cer) ? (count($agreement->owner_annual_insp_cer) ? 'Ann. Insp. Cert., ' : ''): '';
        $owner_vehicle_use_form = isset($agreement->owner_vehicle_use_form) ? (count($agreement->owner_vehicle_use_form) ? 'Heavy Veh. Use, ' : ''): '';

        $uploaded_doc = $w9 . $owner_operator_comprehensive . $owner_vehicle_reg . $owner_annual_insp_cer . $owner_vehicle_use_form;
      }

      if($agreement->status == 1)
        $action = 'Sent to 3rd party';
      elseif($agreement->status == 2)
        $action = 'Pending for Review';
      elseif($agreement->status == 3)
        $action = 'Submitted';
      elseif($agreement->status == 4)
        $action = 'Rejected';

      $sent_json = json_decode($agreement->sent);
      $sent = $sent_json ? end($sent_json) : null;
      $seen_json = json_decode($agreement->seen);
      $seen = $seen_json ? end($seen_json) : null;


        return [
            $request_type,
            $name,
            $action,
            $uploaded_doc,
            $agreement->sent ? \Carbon\Carbon::parse($sent)->timezone('America/New_York')->format('m/d/y g:i A') : 'N/A',
            $agreement->seen ? \Carbon\Carbon::parse($seen)->timezone('America/New_York')->format('m/d/y g:i A') : 'N/A',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}
