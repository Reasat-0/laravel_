<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\DB;
use Config;

class QuickbooksServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
         //if (\Schema::hasTable('mail_settings')) {
            $quickbooks = DB::table('quickbooks_settings')->first();
            if($quickbooks)
            {
                 $config = array(
                    'authorizationRequestUrl'    => 'https://appcenter.intuit.com/connect/oauth2',
                    'tokenEndPointUrl'    => 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer',
                    'client_id'    => $quickbooks->client_id,
                    'client_secret'    => $quickbooks->client_secret,
                    'oauth_scope'       => 'com.intuit.quickbooks.accounting openid profile email phone address',
                    'oauth_redirect_uri'       => $quickbooks->oauth_redirect_uri,
                    'environment'       => $quickbooks->environment,
                );
                Config::set('quickbooks', $config);
            }
        //}

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
