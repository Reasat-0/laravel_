<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\DB;
use Config;

class EmailConfigServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
         //if (\Schema::hasTable('mail_settings')) {
            $mail = DB::table('email_configuration')->first();
            if ($mail) //checking if table is not empty
            {
                $config = array(
                    'driver'     => 'smtp',
                    'host'       => $mail->smtp_host,
                    'port'       => $mail->port_no,
                    'from'       => array('address' => $mail->from_email, 'name' => $mail->from_name),
                    'encryption' => $mail->auto_tls,
                    'username'   => $mail->smtp_user,
                    'password'   => $mail->smtp_password,
                    //'sendmail'   => '/usr/sbin/sendmail -bs',
                    //'pretend'    => false,
                );
                Config::set('mail', $config);
            }
        //}

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
