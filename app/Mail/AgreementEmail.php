<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\AppUrl;

class AgreementEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $insurance;
    private $request_type;
    private $req_subject;
    private $body;
    private $reject;

    public function __construct($insurance, $request_type, $req_subject, $body, $status, $reject)
    {
        $this->insurance = $insurance;
        $this->request_type = $request_type;
        $this->req_subject = $req_subject;
        $this->body = $body;
        $this->status = $status;
        $this->reject = $reject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $app_url = AppUrl::first();

        $action = array(
        'submitted_by' => $this->insurance->submitted_by_email .'-'. $this->insurance->submitted_by_name,
        'token' => $this->insurance->access_code,
        'link' => $this->status == 3 ? $app_url->url . '/insurance/'.$this->insurance->id.'?type='.$this->request_type : $app_url->url . '/insurance/accessCode/' . $this->insurance->url_token . '?reqType=' .$this->request_type .'&status=' . $this->status ,
        'body' => $this->body,
        'reject' => $this->reject,
        'status' => $this->status,
      );
        return $this->view('admin.insurance.agreement_email')->with('data', $action)->subject($this->req_subject);
    }
}
