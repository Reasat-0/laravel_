<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\AppUrl;

class CreditAppEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $creditApp;
    private $request_type;
    private $req_subject;
    private $body;
    private $reject;
    private $credit_app_history;

    public function __construct($creditApp, $request_type, $req_subject, $body, $status, $reject, $credit_app_history)
    {
        $this->creditApp = $creditApp;
        $this->request_type = $request_type;
        $this->req_subject = $req_subject;
        $this->body = $body;
        $this->status = $status;
        $this->reject = $reject;
        $this->credit_app_history = $credit_app_history;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $app_url = AppUrl::first();

        if($this->credit_app_history == ''){
            if($this->status == 3)
                $link = $app_url->url . '/creditApp/'.$this->creditApp->customer_id.'/edit'; 
            else
                $link = $app_url->url . '/creditApp/accessCode/' . $this->creditApp->url_token . '?reqType=' .$this->request_type .'&status=' . $this->status;
        }
        else{
            $link = $app_url->url . '/creditApp/accessCode/' . $this->creditApp->url_token . '?reqType=' .$this->request_type .'&status=' . $this->status .'&cr_limit=' . $this->credit_app_history->id;
        }

        $action = array(
        'submitted_by' => $this->creditApp->submitted_by_email .'-'. $this->creditApp->submitted_by_name,
        'token' => $this->creditApp->access_code,
        'link' => $link,
        'body' => $this->body,
        'reject' => $this->reject,
        'status' => $this->status,
      );
        return $this->view('admin.customer.creditApp_email')->with('data', $action)->subject($this->req_subject);
    }
}
