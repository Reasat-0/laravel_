<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\AppUrl;

class InsuranceEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $insurance;
    private $request_type;
    private $req_subject;

    public function __construct($insurance, $request_type, $req_subject)
    {
        $this->insurance = $insurance;
        $this->request_type = $request_type;
        $this->req_subject = $req_subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $app_url = AppUrl::first();

        $action = array(
        'submitted_by' => $this->insurance->submitted_by,
        'token' => $this->insurance->token,
        'link' => $app_url->url . '/insurance/accessCode/' . $this->insurance->url_token . '?reqType=' .$this->request_type
      );
        return $this->view('admin.insurance.email')->with('data', $action)->subject($this->req_subject);
    }
}
