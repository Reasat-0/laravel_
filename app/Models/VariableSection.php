<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class VariableSection extends Model
{
    protected $table= 'variable_section';

    public $guarded = [];
    
}