<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table= 'inventories';

    public $guarded = [];

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'cat_id');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Models\Subcategory', 'subcat_id');
    }

    public function inventory_info($id)
    {
        return InventoryUnit::where('inventory_id', $id)->orderby('id', 'asc')->get();
    }
    
}