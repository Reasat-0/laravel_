<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProductInventory extends Model
{
    protected $table= 'product_inventories';

    public $guarded = [];

    public function inventory()
    {
        return $this->belongsTo('App\Models\Inventory', 'inventory_id');
    }

    
}