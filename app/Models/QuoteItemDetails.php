<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class QuoteItemDetails extends Model
{
    protected $table= 'quote_item_details';

    public $guarded = [];

    public function quoteItem()
    {
        return $this->belongsTo('App\Models\Inventory', 'resource_id');
    }

    public function resource_vars($id){
    	return QuoteVariable::where('quote_item_type', 'inventory')->where('quote_item_id', $id)->get();
    }
    
}