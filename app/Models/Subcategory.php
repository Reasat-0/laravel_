<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table= 'subcategories';

    public $guarded = [];

    public function category()
    {
        return $this->belongsTo('App\models\category', 'cat_id');
    }

    public function resources(){
    	return $this->hasMany('App\models\Inventory', 'subcat_id');
    }
    
}