<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class VariableParameter extends Model
{
    protected $table= 'variable_parameters';

    public $guarded = [];
    
}