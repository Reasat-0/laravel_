<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CreditApplicationHistory extends Model
{
    protected $table= 'credit_application_history';

    public $guarded = [];
    
}