<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{	
	use SoftDeletes;

    protected $table= 'customers';

    public $guarded = [];

    public function credit_app(){
    	return $this->hasOne('App\Models\CreditApplication', 'customer_id');
    }
    
}