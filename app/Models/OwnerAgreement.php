<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class OwnerAgreement extends Model
{	

    protected $table= 'owner_agreement';

    public $guarded = [];

    public function getw9(){
    	return $this->hasMany('App\Models\Image', 'parent_id')->where('type', 'owner_w9');
    }

    public function owner_operator_comprehensive(){
    	return $this->hasMany('App\Models\Image', 'parent_id')->where('type', 'owner_operator_comprehensive');
    }

    public function owner_vehicle_reg(){
    	return $this->hasMany('App\Models\Image', 'parent_id')->where('type', 'owner_vehicle_reg');
    }

    public function owner_annual_insp_cer(){
    	return $this->hasMany('App\Models\Image', 'parent_id')->where('type', 'owner_annual_insp_cer');
    }

    public function owner_vehicle_use_form(){
    	return $this->hasMany('App\Models\Image', 'parent_id')->where('type', 'owner_vehicle_use_form');
    }
    
}