<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class AppUrl extends Model
{
    protected $table= 'app_url';

    public $guarded = [];
    
}