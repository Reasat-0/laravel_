<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class QuoteItem extends Model
{
    protected $table= 'quote_item';

    public $guarded = [];

    public function service()
    {
        return $this->belongsTo('App\Models\Product', 'item_id');
    }

    public function rental()
    {
        return $this->belongsTo('App\Models\Inventory', 'item_id');
    }

    public function quote_item_details($id){
    	return QuoteItemDetails::where('quote_item_id', $id)->get();
    }

    public function service_vars($id){
        return QuoteVariable::where('quote_item_type', 'service')->where('quote_item_id', $id)->get();
    }
    
}