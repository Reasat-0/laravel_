<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubcontractorAgreement extends Model
{	

    protected $table= 'subcontractor_agreement';

    public $guarded = [];

    public function getW9(){
    	return $this->hasMany('App\Models\Image', 'parent_id')->where('type', 'subcontractor_agreement_w9');
    }

    public function getInsuranceCertificate(){
    	return $this->hasMany('App\Models\Image', 'parent_id')->where('type', 'subcontractor_insurance_certificate');
    }
    
}