<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Variable;


class VendorResource extends Model
{	

    protected $table= 'vendor_resources';
	public $guarded = [];
   
   public function getVariables($id){
   		return Variable::where('type', 'vendor')->where('parent_id', $id)->get();
   }
}