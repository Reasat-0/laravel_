<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RentalAgreement extends Model
{	

    protected $table= 'rental_agreement';

    public $guarded = [];

    public function getW9(){
    	return $this->hasMany('App\Models\Image', 'parent_id')->where('type', 'rental_agreement_w9');
    }

    public function getInsuranceCertificate(){
    	return $this->hasMany('App\Models\Image', 'parent_id')->where('type', 'rental_insurance_certificate');
    }
    
}