<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CreditApplication extends Model
{
    protected $table= 'credit_application';

    public $guarded = [];
    
}