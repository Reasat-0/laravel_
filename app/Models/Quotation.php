<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $table= 'quotation';

    public $guarded = [];
    
    public function customers()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

}