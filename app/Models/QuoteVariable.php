<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class QuoteVariable extends Model
{
    protected $table= 'quote_variable';

    public $guarded = [];
    
}