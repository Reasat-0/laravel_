<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table= 'categories';

    public $guarded = [];

    public function subcat(){
    	return $this->hasMany('App\models\Subcategory', 'cat_id');
    }
    
}