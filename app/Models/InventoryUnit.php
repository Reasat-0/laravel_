<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class InventoryUnit extends Model
{
    protected $table= 'inventory_unit_details';

    public $guarded = [];

    protected $casts = [
        'properties' => 'array'
    ];

    public function inventory()
    {
        return $this->belongsTo('App\Models\Inventory', 'inventory_id');
    }

    
}