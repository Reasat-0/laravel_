<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Customer;

class QbSynchronizeData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $dataService;
    public $sync;

    public function __construct($dataService, $sync)
    {
        $this->dataService = $dataService;
        $this->sync = $sync;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {      

    if($this->sync == 'syncEvo'){

    $customer = Customer::where('quickbooks', 1)->orderBy('id', 'asc')->select('id', 'quickbooks_id', 'syncToken', 'quickbooks')->withTrashed()->get();

        Customer::chunk(30, function ($customer) {
            foreach($customer as $rowdata){
                $qb_id = $rowdata->quickbooks_id;

            $error = $this->dataService->getLastError();
            
            if(!$error){
                $entities = $this->dataService->Query("Select * from Customer WHERE Id ='" .$qb_id.  "'");
                if($entities){
                    if($rowdata->syncToken != $entities[0]->SyncToken){
                        Customer::where('id', $rowdata->id)
                                ->withTrashed()
                                ->update([
                        'middleName' => isset($entities[0]->MiddleName) ? $entities[0]->MiddleName : null,
                        'familyName' => isset($entities[0]->FamilyName) ? $entities[0]->FamilyName : null,
                        'givenName' => isset($entities[0]->GivenName) ? $entities[0]->GivenName : null,
                        'suffix' => isset($entities[0]->Suffix) ? $entities[0]->Suffix : null,
                        'displayName' => isset($entities[0]->DisplayName) ? $entities[0]->DisplayName : null,
                        'companyName' => isset($entities[0]->CompanyName) ? $entities[0]->CompanyName : null,
                        'title' => isset($entities[0]->Title) ? $entities[0]->Title : null,

                        'primaryPhone_freeFormNumber' => isset($entities[0]->PrimaryPhone->FreeFormNumber) ? $entities[0]->PrimaryPhone->FreeFormNumber : null,
                        'alternatePhone' => isset($entities[0]->AlternatePhone->FreeFormNumber) ? $entities[0]->AlternatePhone->FreeFormNumber : null,
                        'mobile' => isset($entities[0]->Mobile->FreeFormNumber) ? $entities[0]->Mobile->FreeFormNumber : null,
                        //'fax' => isset($entities[0]->Fax->FreeFormNumber) ? $entities[0]->Fax->FreeFormNumber : null,

                        'primaryEmailAddr_addess' => isset($entities[0]->PrimaryEmailAddr->Address) ? $entities[0]->PrimaryEmailAddr->Address : null,


                        'billAddr_line1' => isset($entities[0]->BillAddr->Line1) ? $entities[0]->BillAddr->Line1 : null,
                        'billAddr_line2' => isset($entities[0]->BillAddr->Line2) ? $entities[0]->BillAddr->Line2 : null,
                        'billAddr_city' => isset($entities[0]->BillAddr->City) ? $entities[0]->BillAddr->City : null,
                        'billAddr_country' => isset($entities[0]->BillAddr->Country) ? $entities[0]->BillAddr->Country : null,
                        'billAddr_countrySubDivisionCode' => isset($entities[0]->BillAddr->CountrySubDivisionCode) ? $entities[0]->BillAddr->CountrySubDivisionCode : null,
                        'billAddr_postalCode' => isset($entities[0]->BillAddr->PostalCode) ? $entities[0]->BillAddr->PostalCode : null,


                        'shipAddr_line1' => isset($entities[0]->ShipAddr->Line1) ? $entities[0]->ShipAddr->Line1 : null,
                        'shipAddr_line2' => isset($entities[0]->ShipAddr->Line2) ? $entities[0]->ShipAddr->Line2 : null,
                        'shipAddr_city' => isset($entities[0]->ShipAddr->City) ? $entities[0]->ShipAddr->City : null,
                        'shipAddr_country' => isset($entities[0]->ShipAddr->Country) ? $entities[0]->ShipAddr->Country : null,
                        'shipAddr_countrySubDivisionCode' => isset($entities[0]->ShipAddr->CountrySubDivisionCode) ? $entities[0]->ShipAddr->CountrySubDivisionCode : null,
                        'shipAddr_postalCode' => isset($entities[0]->ShipAddr->PostalCode) ? $entities[0]->ShipAddr->PostalCode : null,

                        
                        'otherAddr' => isset($entities[0]->OtherAddr) ? $entities[0]->OtherAddr : null,
                        'webAddr' => isset($entities[0]->WebAddr->URI) ? $entities[0]->WebAddr->URI : null,
                        'notes' => isset($entities[0]->Notes) ? $entities[0]->Notes : null,


                        'quickbooks_id' => $entities[0]->Id,
                        'syncToken' => $entities[0]->SyncToken,
                        'quickbooks' => 1,
                        'temp' => 0,
                        'deleted_at' => $rowdata->deleted_at ? null : $rowdata->deleted_at,
                                ]);
                    }
                }
                else{
                    Customer::where('id',$rowdata->id)->delete();
                }
            }            

            }
        });

    }

    else{

    $customer = Customer::where('manual', 0)->get();
    if(!$customer->isEmpty()){
    //Customer::chunk(10, function ($customer) {

    //$error = $this->dataService->getLastError();
    //if(!$error){
    //$batch = $this->dataService->CreateNewBatch();
    //$i = 1;
    $customer_type = $this->dataService->Query("Select * from CustomerType WHERE Name = 'EVOS'");
    $customer_type_value = $customer_type ? $customer_type[0]->Id : null;

        foreach ($customer as $rowdata) {
        $theResourceObj = \QuickBooksOnline\API\Facades\Customer::create([
                        "BillAddr" => [
                            "Line1" => $rowdata->billAddr_line1,
                            "Line2" => $rowdata->billAddr_line2,
                            "City" => $rowdata->billAddr_city,
                            "Country" => $rowdata->billAddr_country,
                            "CountrySubDivisionCode" => $rowdata->billAddr_countrySubDivisionCode,
                            "PostalCode" => $rowdata->billAddr_postalCode
                        ],
                        "ShipAddr" => [
                            "Line1" => $rowdata->shipAddr_line1,
                            "Line2" => $rowdata->shipAddr_line2,
                            "City" => $rowdata->shipAddr_city,
                            "Country" => $rowdata->shipAddr_country,
                            "CountrySubDivisionCode" => $rowdata->shipAddr_countrySubDivisionCode,
                            "PostalCode" => $rowdata->shipAddr_postalCode
                        ],
                        "OtherAddr"=> $rowdata->otherAddr,
                        "Notes" => $rowdata->notes,
                        "Title" => $rowdata->title,
                        "GivenName" => $rowdata->givenName,
                        "MiddleName" => $rowdata->middleName,
                        "FamilyName" => $rowdata->familyName,
                        "Suffix" => $rowdata->suffix,
                        "CompanyName" => $rowdata->companyName,
                        "DisplayName" => $rowdata->displayName,
                        "PrimaryPhone" => [
                            "FreeFormNumber" => $rowdata->primaryPhone_freeFormNumber
                        ],
                        "PrimaryEmailAddr" => [
                            "Address" => $rowdata->primaryEmailAddr_addess
                        ],
                        "AlternatePhone" => [
                            "FreeFormNumber" => $rowdata->alternatePhone
                        ],
                        "Mobile" => [
                            "FreeFormNumber" => $rowdata->mobile
                        ],
                        /*"Fax" => [
                            "FreeFormNumber" => $rowdata->fax
                        ],*/
                        "WebAddr" => [
                            "URI" => $rowdata->webAddr
                        ],
                        "CustomerTypeRef" => $customer_type_value,
            ]);
        //$batch->AddEntity($theResourceObj, "create-". $i, "create");
        //$batch->Execute();
        //$i++;
            $error = $this->dataService->getLastError();
            if(!$error){
                $resultingObj = $this->dataService->Add($theResourceObj);
                Customer::where('id', $rowdata->id)->update(['manual' => 1, 'quickbooks' => 1, 'updated' => 0, 'quickbooks_id' => $resultingObj->Id, 'syncToken' => $resultingObj->SyncToken,]);
            }
        }
   // }

    //});
        //Customer::where('quickbooks', 0)->update(['quickbooks' => 1]);
    }

    $customer_updated = Customer::where('updated', 1)->get();
    if(!$customer_updated->isEmpty()){
    //Customer::chunk(10, function ($customer_updated) {
        //$batch = $this->dataService->CreateNewBatch();
        //$i = 1;
        foreach ($customer_updated as $rowdata) {
            $error = $this->dataService->getLastError();
            if(!$error){
            $customer = $this->dataService->FindbyId('customer', $rowdata->quickbooks_id);
            $theResourceObj = \QuickBooksOnline\API\Facades\Customer::update($customer  , [
                  "BillAddr" => [
                            "Line1" => $rowdata->billAddr_line1,
                            "Line2" => $rowdata->billAddr_line2,
                            "City" => $rowdata->billAddr_city,
                            "Country" => $rowdata->billAddr_country,
                            "CountrySubDivisionCode" => $rowdata->billAddr_countrySubDivisionCode,
                            "PostalCode" => $rowdata->billAddr_postalCode
                        ],
                        "ShipAddr" => [
                            "Line1" => $rowdata->shipAddr_line1,
                            "Line2" => $rowdata->shipAddr_line2,
                            "City" => $rowdata->shipAddr_city,
                            "Country" => $rowdata->shipAddr_country,
                            "CountrySubDivisionCode" => $rowdata->shipAddr_countrySubDivisionCode,
                            "PostalCode" => $rowdata->shipAddr_postalCode
                        ],
                        "OtherAddr"=> $rowdata->otherAddr,
                        "Notes" => $rowdata->notes,
                        "Title" => $rowdata->title,
                        "GivenName" => $rowdata->givenName,
                        "MiddleName" => $rowdata->middleName,
                        "FamilyName" => $rowdata->familyName,
                        "Suffix" => $rowdata->suffix,
                        "CompanyName" => $rowdata->companyName,
                        "DisplayName" => $rowdata->displayName,
                        "PrimaryPhone" => [
                            "FreeFormNumber" => $rowdata->primaryPhone_freeFormNumber
                        ],
                        "PrimaryEmailAddr" => [
                            "Address" => $rowdata->primaryEmailAddr_addess
                        ],
                        "AlternatePhone" => [
                            "FreeFormNumber" => $rowdata->alternatePhone
                        ],
                        "Mobile" => [
                            "FreeFormNumber" => $rowdata->mobile
                        ],
                        /*"Fax" => [
                            "FreeFormNumber" => $rowdata->fax
                        ],*/
                        "WebAddr" => [
                            "URI" => $rowdata->webAddr
                        ]
            ]);
            //$batch->AddEntity($theResourceObj, "update-". $i, "update");
            //$batch->Execute();
            //$i++;
            $resultingObj = $this->dataService->Update($theResourceObj);
            Customer::where('id', $rowdata->id)->increment('syncToken');
            Customer::where('id', $rowdata->id)->update(['updated' => 0]);
            }
        }
    //});
        //Customer::where('updated', 1)->increment('syncToken');
        //Customer::where('updated', 1)->update(['updated' => 0]);
        
    }

    $customer_deleted = Customer::where('updated', 2)->withTrashed()->get();
    if(!$customer_deleted->isEmpty()){
        foreach ($customer_deleted as $rowdata) {
             $error = $this->dataService->getLastError();
                if(!$error){
                    $customer = $this->dataService->FindbyId('customer', $rowdata->quickbooks_id);
                    $theResourceObj = \QuickBooksOnline\API\Facades\Customer::update($customer  , [
                        "Active" => false
                    ]);
                    $updatedResult = $this->dataService->Update($theResourceObj);
                }
        }
        Customer::where('updated', 2)->withTrashed()->update(['updated' => 0]);
    }
    


    }

}


}
