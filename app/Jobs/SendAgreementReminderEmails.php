<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\SubcontractorAgreement;
use App\Models\RentalAgreement;
use App\Models\OwnerAgreement;

use App\Mail\AgreementEmail;

use Mail;
use Carbon\Carbon;

class SendAgreementReminderEmails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {      
        //\Log::info("Cron is working fine!");

        //get all Agreement reminder which is not seen yet

        $subcontractor = SubcontractorAgreement::whereIn('status', [1, 4])->where('updated_at', '<',Carbon::parse('-24 hours'))->get();
        $rental = RentalAgreement::whereIn('status', [1, 4])->where('updated_at', '<',Carbon::parse('-24 hours'))->get();
        $owner = OwnerAgreement::whereIn('status', [1, 4])->where('updated_at', '<',Carbon::parse('-24 hours'))->get();

        foreach ($subcontractor as $rowdata) {
            if($rowdata->status == 1){
                $body = 'Thank you for the opportunity to work with EV Oilfield Services, LLC as a Sub Contractor.  Please find below the link to our Sub Contractor Agreement.  Please fill it out completely and return to us as soon as possible so we can set up in our system as an authorized EV Sub Contractor.  Thank you again, and we look forward to working with you!';
                $status = 2;
                $reject = '';
                Mail::to($rowdata->agreement_send_mail_to)->send(new AgreementEmail($rowdata, 'subcontractor_agreement', 'Reminder - Subcontractor Agreement Request', $body, $status, $reject));
            }
            else if($rowdata->status == 4){
                $body = 'Hello, Your Subcontractor Agreement request (SC-'.$rowdata->id.') has been rejected because -';
                $reject = $rowdata->reject;
                $status = 4;
                Mail::to($rowdata->agreement_send_mail_to)->send(new AgreementEmail($rowdata, 'subcontractor_agreement', 'Reminder - Subcontractor Agreement Request', $body, $status, $reject));
            }
            
        }

        foreach ($rental as $rowdata) {
            if($rowdata->status == 1){
                $body = '<p>Thank you for renting from EV Rentals a division of EV Oilfield Services, LLC.  Please click the link below to complete your rental agreement and return the completed document to us as quickly as possible so we can activate your account and rental.</p><p>Thank you again for your business!</p>';
                $status = 2;
                $reject = '';
                Mail::to($rowdata->ren_ag_send_mail_to)->send(new AgreementEmail($rowdata, 'rental_agreement', 'Reminder - Rental Agreement Request', $body, $status, $reject));
            }
            else if($rowdata->status == 4){
                $body = 'Hello, Your Rental Agreement request (RN-'.$rowdata->id.') has been rejected because -';
                $reject = $rowdata->reject;
                $status = 4;
                Mail::to($rowdata->ren_ag_send_mail_to)->send(new AgreementEmail($rowdata, 'rental_agreement', 'Reminder - Rental Agreement Request', $body, $status, $reject));
            }
        }

        foreach ($owner as $rowdata) {
            if($rowdata->status == 1){
                $body = 'Thank you for the opportunity to work with EV Oilfield Services, LLC as a Owner Operator.  Please find below the link to our Owner Agreement.  Please fill it out completely and return to us as soon as possible so we can set up in our system as an authorized EV Owner Operator.  Thank you again, and we look forward to working with you!';
                $status = 2;
                $reject = '';
                Mail::to($rowdata->agreement_send_mail_to)->send(new AgreementEmail($rowdata, 'owner_agreement', 'Reminder - Owner Operator Agreement Request', $body, $status, $reject));
            }
            else if($rowdata->status == 4){
                $body = 'Hello, Your Owner Agreement request (OA-'.$rowdata->id.') has been rejected because -';
                $reject = $rowdata->reject;
                $status = 4;
                Mail::to($rowdata->agreement_send_mail_to)->send(new AgreementEmail($rowdata, 'owner_agreement', 'Reminder - Owner Operator Agreement Request', $body, $status, $reject));
            }
        }

    }


}
