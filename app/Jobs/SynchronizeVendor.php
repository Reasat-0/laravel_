<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Vendor;

class SynchronizeVendor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $dataService;
    public $sync;

    public function __construct($dataService, $sync)
    {
        $this->dataService = $dataService;
        $this->sync = $sync;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {      

    if($this->sync == 'syncEvo'){
        $vendor = Vendor::where('quickbooks', 1)->orderBy('id', 'asc')->select('id', 'quickbooks_id', 'syncToken', 'quickbooks')->withTrashed()->get();

        Vendor::chunk(300, function ($vendor) {
            foreach($vendor as $rowdata){
                $qb_id = $rowdata->quickbooks_id;

                $error = $this->dataService->getLastError();
                
                if(!$error){
                    $entities = $this->dataService->Query("Select * from Vendor WHERE Id ='" .$qb_id.  "'");
                    if($entities){
                        if($rowdata->syncToken != $entities[0]->SyncToken){
                            Vendor::where('id', $rowdata->id)
                                    ->withTrashed()
                                    ->update([
                            'middleName' => isset($entities[0]->MiddleName) ? $entities[0]->MiddleName : null,
                            'familyName' => isset($entities[0]->FamilyName) ? $entities[0]->FamilyName : null,
                            'givenName' => isset($entities[0]->GivenName) ? $entities[0]->GivenName : null,
                            'suffix' => isset($entities[0]->Suffix) ? $entities[0]->Suffix : null,
                            'displayName' => isset($entities[0]->DisplayName) ? $entities[0]->DisplayName : null,
                            'companyName' => isset($entities[0]->CompanyName) ? $entities[0]->CompanyName : null,
                            'title' => isset($entities[0]->Title) ? $entities[0]->Title : null,

                            'primaryPhone_freeFormNumber' => isset($entities[0]->PrimaryPhone->FreeFormNumber) ? $entities[0]->PrimaryPhone->FreeFormNumber : null,
                            'alternatePhone' => isset($entities[0]->AlternatePhone->FreeFormNumber) ? $entities[0]->AlternatePhone->FreeFormNumber : null,
                            'mobile' => isset($entities[0]->Mobile->FreeFormNumber) ? $entities[0]->Mobile->FreeFormNumber : null,
                            //'fax' => isset($entities[0]->Fax->FreeFormNumber) ? $entities[0]->Fax->FreeFormNumber : null,

                            'primaryEmailAddr_addess' => isset($entities[0]->PrimaryEmailAddr->Address) ? $entities[0]->PrimaryEmailAddr->Address : null,


                            'billAddr_line1' => isset($entities[0]->BillAddr->Line1) ? $entities[0]->BillAddr->Line1 : null,
                            'billAddr_line2' => isset($entities[0]->BillAddr->Line2) ? $entities[0]->BillAddr->Line2 : null,
                            'billAddr_city' => isset($entities[0]->BillAddr->City) ? $entities[0]->BillAddr->City : null,
                            'billAddr_country' => isset($entities[0]->BillAddr->Country) ? $entities[0]->BillAddr->Country : null,
                            'billAddr_countrySubDivisionCode' => isset($entities[0]->BillAddr->CountrySubDivisionCode) ? $entities[0]->BillAddr->CountrySubDivisionCode : null,
                            'billAddr_postalCode' => isset($entities[0]->BillAddr->PostalCode) ? $entities[0]->BillAddr->PostalCode : null,


                            'taxIdentifier' => isset($entities[0]->TaxIdentifier) ? $entities[0]->TaxIdentifier : null,
                            'accountNum' => isset($entities[0]->AcctNum) ? $entities[0]->AcctNum : null,
                            'webAddr' => isset($entities[0]->WebAddr->URI) ? $entities[0]->WebAddr->URI : null,
                            'notes' => isset($entities[0]->Notes) ? $entities[0]->Notes : null,


                            'quickbooks_id' => $entities[0]->Id,
                            'syncToken' => $entities[0]->SyncToken,
                            'quickbooks' => 1,
                            'temp' => 0,
                            'deleted_at' => $rowdata->deleted_at ? null : $rowdata->deleted_at,
                                    ]);
                        }
                    }
                    else{
                        Vendor::where('id',$rowdata->id)->delete();
                    }
                }            

            }
        });

    }

    else{
        $vendor = Vendor::where('manual', '0')->get();
        if(!$vendor->isEmpty()){
            foreach ($vendor as $rowdata) {
            $theResourceObj = \QuickBooksOnline\API\Facades\Vendor::create([
                            "BillAddr" => [
                                "Line1" => $rowdata->billAddr_line1,
                                "Line2" => $rowdata->billAddr_line2,
                                "City" => $rowdata->billAddr_city,
                                "Country" => $rowdata->billAddr_country,
                                "CountrySubDivisionCode" => $rowdata->billAddr_countrySubDivisionCode,
                                "PostalCode" => $rowdata->billAddr_postalCode
                            ],
                            "TaxIdentifier"=> $rowdata->taxIdentifier,
                            "AcctNum"=> $rowdata->accountNum,
                            "Notes" => $rowdata->notes,
                            "Title" => $rowdata->title,
                            "GivenName" => $rowdata->givenName,
                            "MiddleName" => $rowdata->middleName,
                            "FamilyName" => $rowdata->familyName,
                            "Suffix" => $rowdata->suffix,
                            "CompanyName" => $rowdata->companyName,
                            "DisplayName" => $rowdata->displayName,
                            "PrimaryPhone" => [
                                "FreeFormNumber" => $rowdata->primaryPhone_freeFormNumber
                            ],
                            "PrimaryEmailAddr" => [
                                "Address" => $rowdata->primaryEmailAddr_addess
                            ],
                            "AlternatePhone" => [
                                "FreeFormNumber" => $rowdata->alternatePhone
                            ],
                            "Mobile" => [
                                "FreeFormNumber" => $rowdata->mobile
                            ],
                            /*"Fax" => [
                                "FreeFormNumber" => $rowdata->fax
                            ],*/
                            "WebAddr" => [
                                "URI" => $rowdata->webAddr
                            ]
                ]);
                $error = $this->dataService->getLastError();
                if(!$error){
                    $resultingObj = $this->dataService->Add($theResourceObj);
                    Vendor::where('id', $rowdata->id)->update(['manual' => 1, 'quickbooks' => 1, 'updated' => 0, 'quickbooks_id' => $resultingObj->Id, 'syncToken' => $resultingObj->SyncToken]);
                }
            }
        }


        $vendor_updated = Vendor::where('updated', 1)->get();
        if(!$vendor_updated->isEmpty()){
            foreach ($vendor_updated as $rowdata) {
                $error = $this->dataService->getLastError();
                if(!$error){
                $vendor = $this->dataService->FindbyId('vendor', $rowdata->quickbooks_id);
                $theResourceObj = \QuickBooksOnline\API\Facades\Vendor::update($vendor  , [
                      "BillAddr" => [
                                "Line1" => $rowdata->billAddr_line1,
                                "Line2" => $rowdata->billAddr_line2,
                                "City" => $rowdata->billAddr_city,
                                "Country" => $rowdata->billAddr_country,
                                "CountrySubDivisionCode" => $rowdata->billAddr_countrySubDivisionCode,
                                "PostalCode" => $rowdata->billAddr_postalCode
                            ],
                            "TaxIdentifier"=> $rowdata->taxIdentifier,
                            "AcctNum"=> $rowdata->accountNum,
                            "Notes" => $rowdata->notes,
                            "Title" => $rowdata->title,
                            "GivenName" => $rowdata->givenName,
                            "MiddleName" => $rowdata->middleName,
                            "FamilyName" => $rowdata->familyName,
                            "Suffix" => $rowdata->suffix,
                            "CompanyName" => $rowdata->companyName,
                            "DisplayName" => $rowdata->displayName,
                            "PrimaryPhone" => [
                                "FreeFormNumber" => $rowdata->primaryPhone_freeFormNumber
                            ],
                            "PrimaryEmailAddr" => [
                                "Address" => $rowdata->primaryEmailAddr_addess
                            ],
                            "AlternatePhone" => [
                                "FreeFormNumber" => $rowdata->alternatePhone
                            ],
                            "Mobile" => [
                                "FreeFormNumber" => $rowdata->mobile
                            ],
                            /*"Fax" => [
                                "FreeFormNumber" => $rowdata->fax
                            ],*/
                            "WebAddr" => [
                                "URI" => $rowdata->webAddr
                            ]
                ]);
                $resultingObj = $this->dataService->Update($theResourceObj);
                Vendor::where('id', $rowdata->id)->increment('syncToken');
                Vendor::where('id', $rowdata->id)->update(['updated' => 0]);
                }
            }        
        }

        
        $vendor_deleted = Vendor::where('updated', 2)->withTrashed()->get();
        if(!$vendor_deleted->isEmpty()){
            foreach ($vendor_deleted as $rowdata) {
                 $error = $this->dataService->getLastError();
                    if(!$error){
                        $vendor = $this->dataService->FindbyId('vendor', $rowdata->quickbooks_id);
                        $theResourceObj = \QuickBooksOnline\API\Facades\Vendor::update($vendor  , [
                            "Active" => false
                        ]);
                        $updatedResult = $this->dataService->Update($theResourceObj);
                    }
            }
            Vendor::where('updated', 2)->withTrashed()->update(['updated' => 0]);
        }
    
    }


    }


}
