<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\InsuranceEquipment;
use App\Models\InsuranceVehicle;
use App\Models\InsuranceDriver;
use App\Mail\InsuranceEmail;

use Mail;
use Carbon\Carbon;

class SendInsuranceReminderEmailsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $dataService;
    public $sync;

    public function __construct()
    {
        //$this->dataService = $dataService;
        //$this->sync = $sync;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {      
        //\Log::info("Cron is working fine!");

        //get all insurance reminder which is not seen yet

        $equipment = InsuranceEquipment::where('status', 0)->where('created_at', '<',Carbon::parse('-24 hours'))->get();
        $vehicle = InsuranceVehicle::where('status', 0)->where('created_at', '<',Carbon::parse('-24 hours'))->get();
        $driver = InsuranceDriver::where('status', 0)->where('created_at', '<',Carbon::parse('-24 hours'))->get();

        foreach ($equipment as $rowdata) {
            Mail::to($rowdata->send_mail_to)->send(new InsuranceEmail($rowdata, 'equipment', 'Reminder - Equipment Change Request'));
        }

        foreach ($vehicle as $rowdata) {
            Mail::to($rowdata->send_mail_to)->send(new InsuranceEmail($rowdata, 'vehicle', 'Reminder - Vehicle Change Request'));
        }

        foreach ($driver as $rowdata) {
            Mail::to($rowdata->send_mail_to)->send(new InsuranceEmail($rowdata, 'driver', 'Reminder - Driver Change Request'));
        }

    }


}
