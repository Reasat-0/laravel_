

$(document).ready(function() {

    // Text AREA is field js
    if($('.textarea_label_wrapper_div').val()){
      /*$('.textarea_label_wrapper_div').addClass('is-filled');*/
    }
    // Javascript method's body can be found in assets/js/demos.js
   
    //search table

    $('.all_table').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, 100,-1],
          [10, 25, 50, 100, "All"]
        ],
        "ordering": false,
        "responsive": false,
        "bAutoWidth": false,
        "language": {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('.all_table').DataTable();

      //$('#product_viewer_row').hide();


      $(document).on('click', '.add_inventory', function (e) {
          //
          //var selected = $('#resource_code :selected');
          var resource_name = $('#resource_name').val();
          var unit_price = $('#unit_price').val();
          var inventory_id = $('#inventory_id').val();
          var found = false;
          $("#product_viewer_row .inventory_id_check").each(function(){
          var texttocheck = $(this).val();
          //alert(texttocheck);
          if(texttocheck == inventory_id){
          found = true
          }
          });
      // Product edit btn clicked
      $(document).on('click','.product_edit_btn',function(){
        $('#myEditModal').show();
        var resource_name_td =  $(this).parent().siblings('td')[0];
        var unit_price_td = $(this).parent().siblings('td')[1];

        $('.edit_product_card_body input[name="edit_pro_name"]').attr('value', resource_name_td.innerText);
        $('.edit_product_card_body input[name="edit_unit_price"]').attr('value', unit_price_td.innerText);

        $('.edit_product_card_body input[name="edit_pro_name"]').parent().addClass('is-filled');
        $('.edit_product_card_body input[name="edit_unit_price"]').parent().addClass('is-filled');

        // Product update btn clicked
          $(document).on('click','.update_product_btn',function(){
            var updated_name = $('.edit_product_card_body input[name="edit_pro_name"]').val();
            var updated_price = $('.edit_product_card_body input[name="edit_unit_price"]').val();

            resource_name_td.innerText = updated_name;
            unit_price_td.innerText = updated_price;

            $('#myEditModal').fadeOut();

          });
        })
        $('.close').click(function(){
        $('#myEditModal').fadeOut();

        })

      





          if(resource_name == ''){
            alert('Please select a resource name before adding');
          }
          else if(found == true){
            alert('You cannot select same resource name more than once');
          }
          else{
            //selected.each(function (a) {
            $('#product_viewer_row').show();
            $('.table_body').append('<tr><input type="hidden" name="inventory_id[]" id="inventory_id" class="inventory_id_check" value="' +inventory_id+ '"><td>'+resource_name+'</td> <td>$ '+unit_price+'</td> <td class="text-right"> <a href="#" id="product_delete_btn_" name="" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">close</i></a></td> </tr>');
            // $('#table1').append('<tr><td>'+$(this).text()+'</td></tr>');
            //if you need ah text like days do with that
            $('#category-custom-select').prop('selectedIndex',0);
            $('#category-custom-select').removeClass('hasvalue');
            $('#edit-category-custom-select').prop('selectedIndex',0);
            $('#edit-category-custom-select').removeClass('hasvalue');
            $('#sub_category_product_select').prop('selectedIndex',0);
            $('#sub_category_product_select').removeClass('hasvalue');
            $(".custom_select2_col select").select2();            

            $('#resource_name').val('');
            $('#resource_name').parent().removeClass('is-filled');
            $('#resource_code').val('');
            $('#resource_code').parent().removeClass('is-filled');

          } 
          
      });

      $(document).on("click", "#product_delete_btn_", function(){
          $(this).parent('td').parent().remove();
          var tableRowCount = $('.table_body tr').length;
          if(tableRowCount == 0){
            $('#product_viewer_row').hide();
          } 
       });

      if($('.badge_holder span').is(":hidden")){

          $('.prefix_serial_row .col-md-10').removeClass('col-md-10').addClass('col-md-12');
      }
      
      

      /* ===== Serial Name ====  */ 
      $('#sub-category-custom-select').change(function(){
        $('.prefix_serial_row .col-md-12').removeClass('col-md-12').addClass('col-md-10');
      })


      

      });

  /* ---- window. function ends ------ */





  // helper to get dimensions of an image
  const imageDimensions = file => new Promise((resolve, reject) => {
    const img = new Image()
  
    // the following handler will fire after the successful parsing of the image
    img.onload = () => {
        const { naturalWidth: width, naturalHeight: height } = img
        resolve({ width, height })
    }
  
    // and this handler will fire if there was an error with the image (like if it's not really an image or a corrupted one)
    img.onerror = () => {
        reject('There was some problem with the image.')
    }
    
    img.src = URL.createObjectURL(file)
  })
  
  // here's how to use the helper
  const getInfo = async ({ target: { files } }) => {
    const [file] = files
  
    try {
        const dimensions  = await imageDimensions(file);
        const img_width = dimensions.width;
        const img_height = dimensions.height;
        if ((img_width<500 || img_width>1200) || (img_height<400 || img_height>700)){
          
            swal(
              'Warning!',
              'Recommended Image Size Is <br> <b style="color: #ce6807"> ( 800px * 500px) - (1200px * 700px) <b>',
              'warning'
            )
          
        }
    } catch(error) {
        /* console.error(error) */
    }
  }


/* -------- SIDEBAR JS ----- 
 */


jQuery(function ($) {
  var path = window.location.href;
  $('.sidebar-wrapper .nav .nav-item .nav-link').each(function () {
      if(this.href === path) {
        var collapse_class =  $(this).parents('.collapse').attr('class');
        if(collapse_class){
          $(this).parents('.collapse').addClass('show');
        }
        $(this).parent('.nav-item').addClass('active');
      }
  });
});



/* --- Variable row Adding --- */

var num_of_input_text = $('.variable_card_body input[type = text]').length;
num_of_input_text = num_of_input_text +1;

var row =     '<div class="row input_row var_input_row">';
    row +=  '<div class="col-md-4 input_wrapper">';
    row +=    '<div class="form-group bmd-form-group">';
    row +=      '<label for="var_name" class="bmd-label-floating input_label">Name Of the Field</label>';
    row +=       '<input type="text" class="form-control form_input_field" name="var_name[]'+ num_of_input_text+'" aria-required="true">';
    row +=    '</div>';
    row +=  '</div>';

    row +=  '<div class="col-md-4 dynamic_var_select input_wrapper">';
    row +=    '<div class="form-group custom-form-select">';
    row +=      '<select class="custom-select fa" name="var_type[]" aria-required="true">';
    row +=        '<option class="form-select-placeholder"></option>';
    row +=        '<option class="" value="input">&#xf044; Input Field </option>';
    row +=        '<option class="" value="checkbox">&#xf14a; Checkbox </option>';
    row +=        '<option class="" value="datetime">&#xf073; Date & Time </option>';
    row +=        '<option class="" value="radio">&#xf192; Radio </option>';
    row +=      '</select>';
    row +=       '<div class="form-element-bar"></div>';
    row +=       '<label class="form-element-label input_label top_select_input_label" for="var_type">Select Field Type</label>';
    row +=    '</div>';
    row +=  '</div>';

    row += '<div class="col-md-3 d-flex align-items-center justify-content-center variable_toggle">';
    row +=    '<div class="togglebutton">';
    row +=      '<label>';
    row +=      '<input type="checkbox" name="enable_disable[]" class="enable_disable">Disable <span class="toggle"></span> Enable';
    row +=      '</label>';
    row +=    '</div>';
    row += '</div>';

    row += '<div class="col-md-1 variable_cross_btn">';
    row +=    '<a href="#" class="btn btn-link btn-danger btn-just-icon remove var_delete_btn"><i class="material-icons">close</i><div class="ripple-container"></div></a>';
    row += '</div>';

    row += '</div>';


$('.add_variable_btn').click(function(){
  $('.add_variable_btn_wrapper').before(row);
  $('.dynamic_var_select select.custom-select').change(function(){
    $('.dynamic_var_select select.custom-select').addClass('hasvalue');
  });

});

$(document).on("click", ".var_delete_btn", function(){
    $(this).parent('div.col-md-1').parent().remove();
  });


/*for(i= 0 ; i< $('.variable_card_body input.form-control.form_input_field').length){

}*/



/* ----- Vendor Resourc Section ------ */

/* ---- Create vendor Resource Btn click ---*/

// Has value bearing inputs length checker 
function hasValueFieldsCount(elem){
  return $(elem).filter(function(){return $(this).val();}).length;
}
// Has value checker
function hasValue(elem) {
      return $(elem).filter(function() { return $(this).val(); }).length > 0;
}

var clicked = 0;
$('.add_vendor_resource_btn').click(function(){
  var res_name =  $('#vendor_resource_name_select').val(); 
  var res_code =  $('#vendor_resource_code_existing').val();
  var res_mod_no =  $('#vendor_model_no_exist').val();

  var r_id = $('#vendor_resource_name_select option:selected').attr('r_id');

  var res_name_new =  $('#resource_name_new').val();
  var res_code_new =  $('#resource_code_new').val();
  var res_mod_no_new =  $('#model_no_new').val();


  /* Js work for variable section */
  var variable_td = '';

  // Checking and storing the number of field having values in variable section
  var input_hasValue  = hasValue('.variable_card_body input.form-control.form_input_field');
  var select_hasValue = hasValue('.variable_card_body select.custom-select.fa');


  var variables_arr = [];
  var checkbox_values_arr = [];
  var status = '';

  // Checking how much input field has value and pushing the values inside the variables_arr
  for(i= 0 ; i<hasValueFieldsCount('.variable_card_body input.form-control.form_input_field'); i++){
    variables_arr.push({var_name : $('.variable_card_body input.form-control.form_input_field')[i].value});    
  }
  // Checking how much select has value and pushing the values inside the variables_arr
  for(i= 0 ; i<hasValueFieldsCount('.variable_card_body select.custom-select.fa'); i++){
    variables_arr[i].var_type = $('.variable_card_body select.custom-select.fa')[i].value;    
  }
  // Setting the variable status based on toggle
  for(var x in variables_arr){
    if($('.enable_disable')[x].checked){
      status = "Enabled";
    }
    else{
      status = "Disabled";
    }
    variables_arr[x].var_status = status; 
  }

  //  Show variable btn stored in a variable
  if(input_hasValue && select_hasValue){
    variable_td =  '<button class="btn btn-sm btn-info variable_td_btn" type="button">Show</button>';
  }
  else{
    variable_td = 'N/A';
  }
  // Removing the object containing less than 3 key in variables array
  for(var o in variables_arr){
    if( Object.keys(variables_arr[o]).length < 3){
      variables_arr.splice(o,1);
    }
  }
  /* Js work for variable section end */

    /* Check if it is existing resoure or not and sending the value to td */
  var resource_status = "new";
  if($('.resource_toggle_section input')[0].checked === true){
    resource_status = "existing";
  }

  // Checking whether the fields are empty in existiong or new 
  if($('#existing_resource').is(":visible")){
    if( res_name === "" || res_code === "" || res_mod_no === "" ){
      alert("Please Check and fill up all the fields");
    }
    else{
      table_showing_and_deletion(resource_status); 
    }
  }
  else{
    if(res_name_new === "" || res_code_new === "" || res_mod_no_new === ""  ){
      alert("Please Check all the fields");
    }
    else{
      table_showing_and_deletion(resource_status);
    }
  }
  ////






  // Hidden input generator for the tr and all the variables selected
  function input_type_hidden_row_generator(){
    for(var x in variables_arr){
        for(var y in variables_arr[x]){
          if(variables_arr[x][y] === 'Disabled'){
            $('.vendor_res_table_body tr:last-child').find('td:first').before('<input type="hidden" class="variable_input_hidden" name="'+y+''+clicked+'[]" value="0">');
          }
          else if(variables_arr[x][y] === 'Enabled'){
            $('.vendor_res_table_body tr:last-child').find('td:first').before('<input type="hidden" class="variable_input_hidden" name="'+y+''+clicked+'[]" value="1">');
          }
          else{
            $('.vendor_res_table_body tr:last-child').find('td:first').before('<input type="hidden" class="variable_input_hidden" name="'+y+''+clicked+'[]" value="'+variables_arr[x][y]+'">');
          }
        }
        
      }
      $('.vendor_res_table_body tr:last-child').find('td:first').before('<input type="hidden" name="row_index[]" value="'+clicked+'">');
      clicked++;
  }

  function table_showing_and_deletion(){
    $('#resource_table_row').fadeIn('100000','swing',function(){
      $(this).show();
    })
    if($('.resource_id').prop('checked') == true){
      $('.table_body.vendor_res_table_body').append('<tr><input type="hidden" name="resource_id[]" value="'+r_id+'"><input type="hidden" name="resource_name[]" value="'+res_name+'"><input type="hidden" name="resource_code[]" value="'+res_code+'"><input type="hidden" name="resource_model_no[]" value="'+res_mod_no+'"><td> '+ res_name+' </td> <td> '+ res_code +' </td> <td> '+ res_mod_no +' </td> <td> '+variable_td+' </td> <td style="display:none">'+JSON.stringify(variables_arr)+'</td> <td style="display:none;"> '+resource_status +' </td> <td> <a href="#" id="vendor_resource_delete_btn" name="" class="btn btn-link btn-success btn-just-icon resource_edit"><i class="material-icons">edit</i></a> </td> <td class="text-right"> <a href="#" id="vendor_resource_delete_btn_" name="" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">close</i></a> </td> </tr>');
      input_type_hidden_row_generator();
    }
    else{
      $('.table_body.vendor_res_table_body').append('<tr><input type="hidden" name="resource_id[]" value="'+0+'"><input type="hidden" name="resource_name[]" value="'+res_name_new+'"><input type="hidden" name="resource_code[]" value="'+res_code_new+'"><input type="hidden" name="resource_model_no[]" value="'+res_mod_no_new+'"><td> '+ res_name_new +' </td> <td> '+ res_code_new +' </td> <td> '+ res_mod_no_new +' </td> <td> '+variable_td+' </td> <td style="display:none">'+JSON.stringify(variables_arr)+'</td> <td style="display:none;"> '+resource_status +' </td> <td> <a href="#" id="vendor_resource_delete_btn" name="" class="btn btn-link btn-success btn-just-icon resource_edit"><i class="material-icons">edit</i></a> </td>  <td class="text-right"> <a href="#" id="vendor_resource_delete_btn_" name="" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">close</i></a></td> </tr>');
      input_type_hidden_row_generator();
    }

    $('#vendor_resource_name_select').prop('selectedIndex',0);
    $('#vendor_resource_name_select').removeClass('hasvalue');
    $(".custom_select2_col select").select2();



    $('#vendor_resource_code_existing').val('');
    $('#vendor_resource_code_existing').parent().removeClass('is-filled');
    $('#vendor_resource_code_existing').removeAttr("readonly");

    $('#vendor_model_no_exist').val('');
    $('#vendor_model_no_exist').parent().removeClass('is-filled');




    $('#resource_name_new').val('');
    $('#resource_name_new').parent().removeClass('is-filled');

    $('#resource_code_new').val('');
    $('#resource_code_new').parent().removeClass('is-filled');

    $('#model_no_new').val('');
    $('#model_no_new').parent().removeClass('is-filled');

    $('.resouce_id').checked;

    // Doing the variable selection checkbox unchecked

    $('.variable_checkbox')[0].checked = false;
    if(this.checked){
        $('.variable_card').fadeIn('5000','swing');
      }else{
        $('.variable_card').fadeOut('10000','swing', function(){
          $('.variable_card_body .var_input_row').remove();
          $('.top_variable_row input[type=text]').val('');
          $('.top_variable_row .input_wrapper .form-group').removeClass('is-filled');
          $('.top_variable_row .custom-select').removeClass('hasvalue').find('option:first').prop('selected', true);
          $('.top_variable_row .enable_disable')[0].checked = false;
        });

      }



   $(document).on("click", "#vendor_resource_delete_btn_", function(){
            $(this).parent('td').parent().remove();
            var tableRowCount = $('.table_body tr').length;
            if(tableRowCount == 0){
              $('#resource_table_row').hide();
            } 
    });
  }









});

// edit btn clicked
var clicked_to_edit;
var edit_modal_res_name;
var edit_modal_res_code;
var edit_modal_model_no;
var upading_variable_arr;
var resource_variables_td;
var resource_variable_to_edit;
$(document).on('click','.resource_edit',function(){

    clicked_to_edit = $(this);
    var resource_status_td_val =$(clicked_to_edit).parent().siblings('td')[5].innerText;

    var resource_name_td = $(clicked_to_edit).parent().siblings('td')[0];
    var resource_code_td = $(clicked_to_edit).parent().siblings('td')[1];
    var resource_model_td = $(clicked_to_edit).parent().siblings('td')[2];
    var resource_name_hidden_input =$(clicked_to_edit).parent().siblings('input[name="resource_name[]"]');
    var resource_code_hidden_input =$(clicked_to_edit).parent().siblings('input[name="resource_code[]"]');
    var resource_model_no_hidden_input =$(clicked_to_edit).parent().siblings('input[name="resource_model_no[]"]');
    resource_variables_td = $(clicked_to_edit).parent().siblings('td')[4];

    // Setting the table texts to the edit modal inputs.
    $('.edit_resource_card_body .form-group.bmd-form-group').addClass('is-filled');
    
    edit_modal_res_code = $('.edit_resource_card_body input[name="edit_res_code"]');
    edit_modal_model_no = $('.edit_resource_card_body input[name="edit_model_no"]');

    var resource_name_to_edit = resource_name_td.innerText;
    var resource_code_to_edit = resource_code_td.innerText;
    var resource_model_no_to_edit = resource_model_td.innerText;

    
    edit_modal_res_code[0].value = resource_code_to_edit;
    edit_modal_model_no[0].value = resource_model_no_to_edit;

    if(resource_status_td_val === " existing "){
      // console.log(resource_status_td_val);
      $('.edit_resource_card_body .existing_resource_col').css('display','block');
      $('.edit_resource_card_body .new_resource_col').css('display','none');
      for(var x in $('.existing_resource_col select option')){
        if($('.existing_resource_col select option')[x].value === resource_name_td.innerText.trim()){
          $('.existing_resource_col select option')[x].selected = true;
        }
        else{
        }
      }
      edit_modal_res_name = $('.edit_resource_card_body select#vendor_resource_name_select');
    }
    else{
      // console.log(resource_status_td_val);
      $('.edit_resource_card_body .new_resource_col').css('display','block');
      $('.edit_resource_card_body .existing_resource_col').css('display','none');
      edit_modal_res_name = $('.edit_resource_card_body input[name="edit_res_name"]');
    }

    edit_modal_res_name[0].value = resource_name_to_edit;
    



    

    // variable in td
    resource_variable_to_edit = JSON.parse(resource_variables_td.innerText);

    if(resource_variable_to_edit.length){

    $('.edit_resource_variable_card').show();
      for(i = 0 ; i < resource_variable_to_edit.length; i++){
        // Variable field creation to edit
        
        let input_selected = resource_variable_to_edit[i].var_type === 'input' ? 'selected' : '';
        let checkbox_selected = resource_variable_to_edit[i].var_type === 'checkbox' ? 'selected' : '';
        let datetime_selected = resource_variable_to_edit[i].var_type === 'datetime' ? 'selected' : '';
        let radio_selected = resource_variable_to_edit[i].var_type === 'radio' ? 'selected' : '';

        var variable_contents = '<div class="row input_row">';
            variable_contents += '<div class="col-md-4 input_wrapper">';
            variable_contents +=   '<div class="form-group bmd-form-group">';
            variable_contents +=      '<label for="field_name" class="bmd-label-floating input_label">Name Of the Field</label>';
            variable_contents +=      '<input type="text" class="form-control form_input_field" name="var_name[]" value='+resource_variable_to_edit[i].var_name+' aria-required="true">';
            variable_contents +=   '</div>';
            variable_contents += '</div>';
            variable_contents += '<div class="col-md-5 input_wrapper">';
            variable_contents +=  '<div class="form-group custom-form-select">';
            variable_contents +=    '<select class="custom-select fa hasvalue" name="var_type[]" value='+resource_variable_to_edit[i].var_type+' aria-required="true" aria-hidden="true" >';
            variable_contents +=       '<option class="form-select-placeholder"></option>';
            variable_contents +=       '<option class="" value="input"' +input_selected+ '> Input Field</option>';
            variable_contents +=       '<option class="" value="checkbox"' +checkbox_selected+ '> Checkbox</option>';
            variable_contents +=       '<option class="" value="datetime"' +datetime_selected+ '> Date &amp; time</option>';
            variable_contents +=       '<option class="" value="radio"' +radio_selected+ '> Radio</option>';
            variable_contents +=     '</select>';
            variable_contents +=      '<div class="form-element-bar">';
            variable_contents +=      '</div>';
            variable_contents +=      '<label class="form-element-label input_label top_select_input_label" for="var_type">Select Field Type</label>';
            variable_contents +=    '</div>';
            variable_contents +=  '</div>';
            variable_contents +=  '<div class="col-md-3 d-flex align-items-center justify-content-center variable_toggle">';
            variable_contents +=    '<div class="togglebutton">';
            variable_contents +=      '<label>';
            variable_contents +=      '<input type="checkbox" name="enable_disable[]" value="'+resource_variable_to_edit[i].var_status+'" class="enable_disable">';
            variable_contents +=      'Disable';
            variable_contents +=      '<span class="toggle"></span>';
            variable_contents +=      'Enable';
            variable_contents +=      '</label>';
            variable_contents +=    '</div>';
            variable_contents +=  '</div>';
            /*variable_contents +=  '<div class="col-md-1 variable_cross_btn">';
            variable_contents +=    '<a href="#" class="btn btn-link btn-danger btn-just-icon remove disabled"><i class="material-icons">close</i><div class="ripple-container"></div></a>';
            variable_contents +=  '</div>';*/
            variable_contents += '</div>';
            

            $('.edit_resource_variable_card_body').append(variable_contents);

          /*for(x=0 ; x<$('.edit_resource_variable_card_body select[name="var_type[]"] option').length; x++){
            console.log(resource_variable_to_edit[i].var_type);
            if($('.edit_resource_variable_card_body select[name="var_type[]"] option')[x].value === resource_variable_to_edit[i].var_type ){
              $('.edit_resource_variable_card_body select[name="var_type[]"] option')[x].selected = true;
            }
          }*/
          // toggle viewing checked or unchecked based on var_status
          if(resource_variable_to_edit[i].var_status === "Enabled"){
            $('.edit_resource_variable_card input[name="enable_disable[]"]')[i].checked= true;
          }

      }

    }


    // Updating the existing variable 
    upading_variable_arr = function(){

      for(i=0 ; i<$('.edit_resource_variable_card input[name="var_name[]"]').length ; i++){
        resource_variable_to_edit[i].var_name =  $('.edit_resource_variable_card input[name="var_name[]"]')[i].value;
      }
      for(i=0;i<$('.edit_resource_variable_card select[name="var_type[]"]').length; i++){
        if($('.edit_resource_variable_card select[name="var_type[]"]')[i].value === ""){
          resource_variable_to_edit[i].var_type = resource_variable_to_edit[i].var_type;
        }
        else{
          resource_variable_to_edit[i].var_type = $('.edit_resource_variable_card select[name="var_type[]"]')[i].value;
        }
      }
      if(resource_variable_to_edit.length){
        for(var x in resource_variable_to_edit){
          if($('.edit_resource_variable_card_body input[name="enable_disable[]"]')[x].checked){
            status = "Enabled";
          }
          else{
            status = "Disabled";
          }
         resource_variable_to_edit[x].var_status = status; 
        }
      }

    }




    $('#myEditModal').show();
    $('.close').click(function(){
      $('#myEditModal').hide();
      $('.edit_resource_variable_card_body').empty();

    })

});

    // Update clicked and setting the values in td and closing the modal
$(document).on('click','.update_resource_btn',function(){
  clicked_to_edit.parent().siblings('td')[0].innerText = edit_modal_res_name.val();
  clicked_to_edit.parent().siblings('td')[1].innerText = edit_modal_res_code.val();
  clicked_to_edit.parent().siblings('td')[2].innerText = edit_modal_model_no.val();
  clicked_to_edit.parent().siblings('input[name="resource_name[]"]')[0].value = edit_modal_res_name.val();
  clicked_to_edit.parent().siblings('input[name="resource_code[]"]')[0].value = edit_modal_res_code.val();
  clicked_to_edit.parent().siblings('input[name="resource_model_no[]"]')[0].value = edit_modal_model_no.val();

  

  upading_variable_arr();
  //variable input hidden value changing after update
  if(resource_variable_to_edit.length){
    var num_in_var_name_obj = clicked_to_edit.parent().siblings('.variable_input_hidden').attr('name').match(/\d+/)[0];
  }
  for(var x in resource_variable_to_edit){
    clicked_to_edit.parent().siblings('input[name="var_name'+num_in_var_name_obj+'[]"]')[x].value = resource_variable_to_edit[x].var_name;
    clicked_to_edit.parent().siblings('input[name="var_type'+num_in_var_name_obj+'[]"]')[x].value = resource_variable_to_edit[x].var_type;
    if(resource_variable_to_edit[x].var_status === "Disabled"){
      var status_to_bin = 0;
    }
    else{
      var status_to_bin =1;
    }
    clicked_to_edit.parent().siblings('input[name="var_status'+num_in_var_name_obj+'[]"]')[x].value = status_to_bin;
  }
  resource_variables_td.innerText = JSON.stringify(resource_variable_to_edit);

  // Mod variable sending to td
  $('#myEditModal').hide();
  $('.edit_resource_variable_card_body').empty();
})





  // variable show btn clicked and modal 
  var show_clicked = 0;
  var variables_selected = [];
  $(document).on('click','.variable_td_btn',function(){
    show_clicked++;
    /*show_the_variable_modal(input_field_values_arr,checkbox_values_arr);*/

    variables_selected = JSON.parse($(this).parent().next('td').html());
    /*    
    console.log(variables_selected);
    console.log(variables_selected.length);*/
    // console.log("Cliked")

      for (var x=0 ; x < variables_selected.length; x++){
        var variables_tr = '<tr>';
            variables_tr +=   '<td>'+variables_selected[x].var_name+'</td>';
            variables_tr +=   '<td>';
            variables_tr +=   variables_selected[x].var_type;
            variables_tr +=   '</td>';
            variables_tr +=   '<td class="">';
            variables_tr +=     variables_selected[x].var_status;
            variables_tr +=   '</td>';
            variables_tr += '</tr>';
        

        $('.variable_table tbody').append(variables_tr);
      }

    // Get the modal
    var modal = document.getElementById("myModal");
    modal.style.display = "block";

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() { 
      $('.variable_table tbody').empty();
      $('.variable_table_card').addClass("out");
      setTimeout(function(){
        modal.style.display = "none";
        $('.variable_table_card').removeClass("out");
      },400);
    }

    modal.onclick = function(){
      
      $('.variable_table tbody').empty();
      $('.variable_table_card').addClass("out");
      setTimeout(function(){
        modal.style.display = "none";
        $('.variable_table_card').removeClass("out");
      },400);
    }
  })

/* ---- end Create vendor Resource Btn click ---*/

/* ---- EDIT vendor Resource Btn click ---*/

$('.edit_add_vendor_resource_btn').click(function(){

  var res_name =  $('#edit_vendor_resource_category_select').val();
  var res_code =  $('#edit_vendor_resource_code_existing').val();
  var res_mod_no =  $('#edit_vendor_model_no_exist').val();

  var r_id = $('#edit_vendor_resource_category_select option:selected').attr('r_id');

  var res_name_new =  $('#edit_resource_name_new').val();
  var res_code_new =  $('#edit_resource_code_new').val();
  var res_mod_no_new =  $('#edit_model_no_new').val();

  if($('#existing_resource').is(":visible")){
      if( res_name === "" || res_code === "" || res_mod_no === "" ){
        alert("Please Check and fill up all the fields");
      }
      else{
        edit_vendor_table_showing_deletion();
      }

    }
  else{
      if(res_name_new === "" || res_code_new === "" || res_mod_no_new === ""  ){
        alert("Please Check all the fields");
      }
      else{
        edit_vendor_table_showing_deletion();
      }
    }

  

/* Edit vendor */

  function edit_vendor_table_showing_deletion(){
    $('#resource_table_row').show();
    if($('.resource_id').prop('checked') == true){
        $('.table_body.vendor_res_table_body').append('<tr><input type="hidden" name="resource_id[]" value="'+r_id+'"><input type="hidden" name="resource_name[]" value="'+res_name+'"><input type="hidden" name="resource_code[]" value="'+res_code+'"><input type="hidden" name="resource_model_no[]" value="'+res_mod_no+'"><td> '+ res_name+' </td> <td> '+ res_code +' </td> <td> '+ res_mod_no +' </td> <td class="text-right"> <a href="#" id="vendor_resource_delete_btn_" name="" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">close</i></a></td> </tr>');
    }
    else{
        $('.table_body.vendor_res_table_body').append('<tr><input type="hidden" name="resource_id[]" value="'+0+'"><input type="hidden" name="resource_name[]" value="'+res_name_new+'"><input type="hidden" name="resource_code[]" value="'+res_code_new+'"><input type="hidden" name="resource_model_no[]" value="'+res_mod_no_new+'"><td> '+ res_name_new +' </td> <td> '+ res_code_new +' </td> <td> '+ res_mod_no_new +' </td> <td class="text-right"> <a href="#" id="vendor_resource_delete_btn_" name="" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">close</i></a></td> </tr>');
    }
    $('#edit_vendor_resource_category_select').prop('selectedIndex',0);
    $('#edit_vendor_resource_category_select').removeClass('hasvalue');
    $(".custom_select2_col select").select2();
  
  
  
    $('#edit_vendor_resource_code_existing').val('');
    $('#edit_vendor_resource_code_existing').parent().removeClass('is-filled');
    $('#edit_vendor_resource_code_existing').removeAttr("readonly");
  
    $('#edit_vendor_model_no_exist').val('');
    $('#edit_vendor_model_no_exist').parent().removeClass('is-filled');
  
  
  
  
    $('#edit_resource_name_new').val('');
    $('#edit_resource_name_new').parent().removeClass('is-filled');
  
    $('#edit_resource_code_new').val('');
    $('#edit_resource_code_new').parent().removeClass('is-filled');
  
    $('#edit_model_no_new').val('');
    $('#edit_model_no_new').parent().removeClass('is-filled');
  

        $(document).on("click", "#vendor_resource_delete_btn_", function(){
                $(this).parent('td').parent().remove();
                var tableRowCount = $('.table_body tr').length;
                if(tableRowCount == 0){
                  $('#resource_table_row').hide();
                } 
        });

  }
  



});

  // edit vendor resource table row deletion
  $(document).on("click", "#vendor_resource_delete_btn_", function(){
    $(this).parent('td').parent().remove();
    var tableRowCount = $('.table_body tr').length;
    if(tableRowCount == 0){
      $('#resource_table_row').hide();
    } 
  });

  $(document).on('click','.stored_variable_td_btn',function(){
    var variables_stored = JSON.parse($(this).parent().siblings('td')[3].innerText);
    var store_variable_row = '';
    for(var x in variables_stored){
      store_variable_row += '<tr>';
      store_variable_row +=   '<td>'+variables_stored[x].var_name+' </td>';
      store_variable_row +=   '<td>'+variables_stored[x].var_type+' </td>';
      store_variable_row +=   '<td>'+variables_stored[x].var_status+' </td>';
      store_variable_row += '</tr>'
    }
    $('.variable_table_wrapper .variable_table2 tbody').append(store_variable_row);
    let value = $(this).attr("value");

    // Get the modal
    var modal = document.getElementById("stored_var_myModal"+value);
    modal.style.display = "block";

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
      $('.variable_table2 tbody').empty();
      $('.variable_table_card').addClass("out");
      setTimeout(function(){
        modal.style.display = "none";
        $('.variable_table_card').removeClass("out");
      },400);
    }

    modal.onclick = function() {
      
      $('.variable_table2 tbody').empty();
      $('.variable_table_card').addClass("out");
      setTimeout(function(){
        modal.style.display = "none";
        $('.variable_table_card').removeClass("out");
      },400);
    }
})

/* ---- end edit vendor Resource Btn click ---*/





      /* ============ Image Modal ========== */

      /* ---- Add page --------------------------- */

      /* ---- Multi Image Slider Modal ------- */
      var image_slider_li     = $('.image_wrapper .multiple_image_main_section #_displayImages .slider ');
      var gen_modal           =       $('#genModal');
      var for_gen_close       =       $('.gen_close,#genModal');
      var modal_img           =       $('#gen_modal_img');


      var modal_viewer = function(){
          $('.uploaded-image img').click(function(){
            $('#genModal').show();
            $('#gen_modal_img').attr('src',this.src);
            modal_close();
        })
      }
      

      var drag_drop_placeholder = '';
          drag_drop_placeholder += '<div class="drag_drop_placeholder_wrapper uploaded-image" style="display:none; text-align:center">';
          drag_drop_placeholder +=    '<i class="material-icons cloud_upload_icon">cloud_upload</i>';
          drag_drop_placeholder +=    '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>';
          drag_drop_placeholder +=    '<p class="or_para">or</p><div class="fileuploader-input-button"><span>Browse Files</span></div>';
          drag_drop_placeholder += '</div>';

      var drag_drop_placeholder2 = '';
          drag_drop_placeholder2 += '<div class="drag_drop_placeholder_wrapper2 uploaded-image" style="display:none; text-align:center">';
          drag_drop_placeholder2 +=    '<i class="material-icons cloud_upload_icon">cloud_upload</i>';
          drag_drop_placeholder2 +=    '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>';
          drag_drop_placeholder2 +=    '<p class="or_para">or</p><div class="fileuploader-input-button"><span>Browse Files</span></div>';
          drag_drop_placeholder2 += '</div>';

      $('.input-images1 .image-uploader .uploaded').prepend(drag_drop_placeholder);
      $('.input-images .image-uploader .uploaded').prepend(drag_drop_placeholder);
      $('.input-images2 .image-uploader .uploaded').prepend(drag_drop_placeholder2);

      $("input[type='file'][name='images[]']").change(function(){
          if($('.input-images1 .uploaded .uploaded-image').length){
            $('.drag_drop_placeholder_wrapper.uploaded-image').show();
          }
          else if($('.input-images .uploaded .uploaded-image').length){
            $('.drag_drop_placeholder_wrapper.uploaded-image').show();
          }
      })
      $("input[type='file'][name='images2[]']").change(function(){
          if($('.input-images2 .uploaded .uploaded-image').length){
            $('.drag_drop_placeholder_wrapper2.uploaded-image').show();
          }
      })


      var modal_viewer_dynamic = function(){
           $(document).on("click",".uploaded-image img",function(){
            // console.log("clicked")
            $('#genModal').show();
            $('#gen_modal_img').attr('src',this.src);
            modal_close();
        })
      }
      modal_viewer();
      modal_viewer_dynamic()

      $('.imagesInput').change(function(){
        modal_viewer();

        $('.input-images .delete-image').click(function(){
          if($('.input-images .uploaded .uploaded-image').length <2 ){
            $('.drag_drop_placeholder_wrapper').hide();
            $('.input-images .image-uploader').removeClass('has-files');

          }
        })

        $('.input-images1 .delete-image').click(function(){
          if($('.input-images1 .uploaded .uploaded-image').length <2 ){
            $('.drag_drop_placeholder_wrapper').hide();
            $('.input-images1 .image-uploader').removeClass('has-files');
          }
        })

        $('.input-images2 .delete-image').click(function(){
          if($('.input-images2 .uploaded .uploaded-image').length <2){
            $('.drag_drop_placeholder_wrapper2').hide();
            $('.input-images2 .image-uploader').removeClass('has-files');
          }
        })
      });

      if($('.edit_section_img.input-images .uploaded .uploaded-image').length >1 ){
          $('.drag_drop_placeholder_wrapper').show();
      }
      if($('.edit_section_img.input-images2 .uploaded .uploaded-image').length >1 ){
          $('.drag_drop_placeholder_wrapper2').show();
      }


      $('.edit_section_img.input-images .delete-image').click(function(){
          if($('.input-images .uploaded .uploaded-image').length <2 ){
            $('.drag_drop_placeholder_wrapper').hide();
            $('.edit_section_img.input-images .image-uploader').removeClass('has-files');
          }
        })
      $('.edit_section_img.input-images2 .delete-image').click(function(){
          if($('.input-images2 .uploaded .uploaded-image').length <2 ){
            $('.edit_section_img.input-images2 .drag_drop_placeholder_wrapper2').hide();
            $('.edit_section_img.input-images2 .image-uploader').removeClass('has-files');
          }
        })

      var modal_close = function(){
            for_gen_close.click(function(){
            modal_img.addClass("out");
            setTimeout(function(){
              gen_modal.hide();
              modal_img.removeClass("out");
            },400);
          
          });
          }
      
        /*$('.uploaded-image img').click(function(){
            conole.log("clicked")
            $('#genModal').show();
            $('#gen_modal_img').attr('src',this.src);
            modal_close();
        });

        
        }*/


/* Image 2 */





/*var for_gen_close2       =       $('.gen_close2,#genModal2');
var modal_img2           =       $('#gen_modal_img2');

function modal_close2(){
  for_gen_close2.click(function(){
  modal_img2.addClass("out");
  setTimeout(function(){
    $('#genModal2').hide();
    modal_img2.removeClass("out");

  },400);

});
}

$(document).ready(function(){
  $('.uploaded-image img').click(function(){
              console.log("clicked");
              $('#genModal2').show();
              $('#gen_modal_img2').attr('src',this.src);
              modal_close2();
            });
})*/





/* ---- SERVICE MULTI ADDRESS ADDER ----- */


var new_addr_card = '';

    new_addr_card+=                                  '<div class="card mt-3 new_added">';
    new_addr_card+=                                    '<div class="card-header card-header-rose card-header-icon service_address_header">';
    new_addr_card+=                                      '<div class="timeline-heading">';
    new_addr_card+=                                        '<h4 class="badge badge-pill badge-info service_remove" style="">Default</h4>';
    new_addr_card+=                                        '<div class="form-check">';
    new_addr_card+=                                          '<label class="form-check-label">';
    new_addr_card+=                                            '<input type="hidden" class="is_default" name="is_default[]" value="0"><input class="form-check-input radio-input" type="radio" name="service_default" value="1"> Set as default';
    new_addr_card+=                                            '<span class="circle">';
    new_addr_card+=                                              '<span class="check"></span>';
    new_addr_card+=                                            '</span>';
    new_addr_card+=                                          '</label>';
    new_addr_card+=                                        '</div>';                                        
    new_addr_card+=                                      '</div>';                                      
    new_addr_card+=                                      '<button class="btn btn-danger btn-sm btn-round address_remove_btn">';
    new_addr_card+=                                        '<i class="material-icons">close</i>';
    new_addr_card+=                                        '<div class="ripple-container"></div>';
    new_addr_card+=                                      '</button>';
    new_addr_card+=                                    '</div>';
    new_addr_card+=                                    '<div class="card-body">';
    new_addr_card+=                                      '<div class="row">';
    new_addr_card+=                                        '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="shipAddr_line1" class="bmd-label-floating input_label">Line 1</label>';
    new_addr_card+=                                            '<input type="text" class="form-control form_input_field" id="shipAddr_line1" name="shipAddr_line1[]">';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                        '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="shipAddr_line2" class="bmd-label-floating input_label">Line 2</label>';
    new_addr_card+=                                            '<input type="text" class="form-control form_input_field" id="shipAddr_line2" name="shipAddr_line2[]">';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                      '</div>';
                                          
    new_addr_card+=                                      '<div class="row input_row">';
    new_addr_card+=                                       '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="shipAddr_city" class="bmd-label-floating input_label">City</label>';
    new_addr_card+=                                            '<input type="text" class="form-control form_input_field" id="shipAddr_city" name="shipAddr_city[]">';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                        '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="shipAddr_country" class="bmd-label-floating input_label">Country</label>';
    new_addr_card+=                                            '<input type="text" class="form-control form_input_field" id="shipAddr_country" name="shipAddr_country[]">';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                      '</div>';
                                          
    new_addr_card+=                                      '<div class="row input_row">';
    new_addr_card+=                                        '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="shipAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Country SubDivision Code</label>';
    new_addr_card+=                                            '<input type="text" class="form-control form_input_field" id="shipAddr_countrySubDivisionCode" name="shipAddr_countrySubDivisionCode[]">';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                        '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="shipAddr_postalCode" class="bmd-label-floating input_label">Postal Code</label>';
    new_addr_card+=                                            '<input type="text" class="form-control form_input_field" id="shipAddr_postalCode" name="shipAddr_postalCode[]">';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                      '</div>';

    new_addr_card+=                                      '<div class="row input_row">';
    new_addr_card+=                                        '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="shipAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Lease Name</label>';
    new_addr_card+=                                            '<input type="text" class="form-control form_input_field" id="lease_name" name="lease_name[]">';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                        '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="shipAddr_postalCode" class="bmd-label-floating input_label">Rig Number</label>';
    new_addr_card+=                                            '<input type="text" class="form-control form_input_field" id="rig_number" name="rig_number[]">';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                      '</div>';

    new_addr_card+=                                      '<div class="row input_row">';
    new_addr_card+=                                        '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="shipAddr_postalCode" class="bmd-label-floating input_label">AFE Number</label>';
    new_addr_card+=                                            '<input type="text" class="form-control form_input_field" id="afe_number" name="afe_number[]">';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                        '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="shipAddr_countrySubDivisionCode" class="bmd-label-floating input_label">Site Contact Phone</label>';
    new_addr_card+=                                            '<input type="text" class="form-control form_input_field" id="site_contact_phone" name="site_contact_phone[]">';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                      '</div>';



    new_addr_card+=                                      '<div class="row input_row">';

    new_addr_card+=                                        '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="shipAddr_postalCode" class="bmd-label-floating input_label">Rig Contact Email</label>';
    new_addr_card+=                                            '<input type="email" class="form-control form_input_field" id="rig_contact_email" name="rig_contact_email[]">';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                        '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="shipAddr_postalCode" class="bmd-label-floating input_label">Company Man Contact Name</label>';
    new_addr_card+=                                            '<input type="text" class="form-control form_input_field" id="company_man_contact_name" name="company_man_contact_name[]">';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                      '</div>';

    new_addr_card+=                                      '<fieldset class="custom_fieldset_2">';
    new_addr_card+=                                      '<legend>GPS location</legend>';
    new_addr_card+=                                      '<div class="row input_row dd_sec">';
    new_addr_card+=                                        '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="dd_latitude" class="bmd-label-floating input_label">Latitude</label>';
    new_addr_card+=                                            '<input type="number" class="form-control form_input_field dd_latitude" name="latitude[]">';
    new_addr_card+=                                            '<span class="error_msg"></span>';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                        '<div class="col-md-6 input_wrapper">';
    new_addr_card+=                                          '<div class="form-group bmd-form-group">';
    new_addr_card+=                                            '<label for="dd_longitude" class="bmd-label-floating input_label">Longitude</label>';
    new_addr_card+=                                            '<input type="number" class="form-control form_input_field dd_longitude" name="longitude[]">';
     new_addr_card+=                                            '<span class="error_msg"></span>';
    new_addr_card+=                                          '</div>';
    new_addr_card+=                                        '</div>';
    new_addr_card+=                                      '</div>';
    new_addr_card+=                                      '</fieldset>';



    new_addr_card+=                                    '</div>';
    new_addr_card+=                                  '</div>';


$('.add_address_btn').click(function(){
  $('.add_address_btn_wrapper').before(new_addr_card);
  /*$(this).animate({top: '30px'});*/

})

$(document).on("click",".address_remove_btn",function(){
  /*$(this).parent('div.card-header.card-header-rose.card-header-icon.service_address_header').parent('.card.single_address_card').remove();*/
  $(this).parent().parent().remove();
  if($('.service_address_card_body .card').length === 1){
    $('.top_single_address_card input[name="service_default"]').parents('.form-check').siblings('.badge').removeClass('service_remove');
    $('.top_single_address_card input[name="service_default"]').parents('.form-check').siblings('.badge').addClass('service_add');
    $('.top_single_address_card input[name="service_default"]').prop("checked",true);
  }

  // Setting up the 1st one as default
  var this_obj_input = $(this).siblings('.timeline-heading').children('div.form-check').children().children('input[name="service_default"]')[0];
  if(this_obj_input.checked == true){
    $('.top_single_address_card input[name="service_default"]').parents('.form-check').siblings('.badge').removeClass('service_remove');
    $('.top_single_address_card input[name="service_default"]').parents('.form-check').siblings('.badge').addClass('service_add');
    $('.top_single_address_card input[name="service_default"]').prop("checked",true);
    $('.top_single_address_card .is_default').prop("value",1);
  }
})


// $(document).on("click", ".var_delete_btn", function(){
//     $(this).parent('div.col-md-1').parent().remove();
//   });


/* Radio btn */

/*$(document).ready(function(){
 
  $('.service_address_header .badge').css('visibility','hidden');

  if($('input[name="service_default"]:checked')){
    $('input[name="service_default"]:checked').parents('.form-check').siblings('.badge').css('visibility','visible');
  }
  $(document).on('change', 'input[type="radio"][name="service_default"]', function() {
    if(this.checked){
      $('.service_address_header .badge').css('visibility','hidden');
      $(this).parents('.form-check').siblings('.badge').css('visibility','visible');
      
    }
  });

})*/

$(document).ready(function(){
 
/*  $('.service_address_header .badge').addClass('service_remove');*/

  if($('input[name="service_default"]:checked')){
    $('input[name="service_default"]:checked').parents('.form-check').siblings('.badge').removeClass('service_remove');
    $('input[name="service_default"]:checked').parents('.form-check').siblings('.badge').addClass('service_add');
  }
  $(document).on('change', 'input[type="radio"][name="service_default"]', function() {
    if(this.checked){
      $('.service_address_header .badge').removeClass('service_add').addClass('service_remove');
      $(this).parents('.form-check').siblings('.badge').addClass('service_add');
      
    }
  });

})

//  ========== Quotation Section ========== //

//  Customer in quotation
/*var change_option_count = 0;
$('#pro-sub-category-custom-select').change(function(e){
  change_option_count++;
  if(this.value.length>1 && change_option_count == 1){
     $('.customers_details').fadeIn();
  }else if(this.value.length>1 && change_option_count>1){
    $('.customers_details').fadeOut().fadeIn();
  }
})*/
/*$(document).on('click','.add_quote_tbl',function(){
  console.log("Clecked")
  $('#quote_tbl2').fadeIn();
})*/
$(document).on('click','.quote_row_delete_btn',function(){
  $(this).parent().parent().remove();
})

$('.card_to_scroll').on('scroll', function(){            
  $(".card_to_scroll2").not(this).scrollLeft($(this).scrollLeft());
});

// quote_address_btn click event
/*$(document).on('click','.quote_address_btn',function(){
  $('#myModal').show();
  $('.close').click(function(){
      $('.address_card').addClass("out");
      setTimeout(function(){
        $('#myModal').hide();
        $('.address_card').removeClass("out");
      },400);

    })
  $('.address_ok').click(function(){
      $('.address_card').addClass("out");
      setTimeout(function(){
        $('#myModal').hide();
        $('.address_card').removeClass("out");
      },400);

    })
})*/

/*$('.add_quote_tbl').click(function(){
  $('.product_quotation_table_card').fadeIn();
});*/

$(document).on('click','.new_address_card',function(){
  $(this).parent().fadeOut(function(){
  
  // $('.address_card_body .address_card_body_row').append(address_card).hide().fadeIn(1000);
  $('.new_address_col').fadeIn(1000);
  });
})

$(document).on('click','.cross a',function(){
//$('.cross a').click(function(){
  $(this).parents('.single_service_rental_card').fadeOut().remove();

  //if($('.single_card_for_single_service').length > 1){
    $('.item_no').text(function (i) {
      // returning the sum of i + 1 to compensate for
      // JavaScript's zero-indexing:
      return i + 1;
    });
  //}

  count_quote_row();

})

var count_quote_row = function(){
  if($('.single_service_rental_card').length < 1){
    $('.final_result').fadeOut(1000);
  }
}

// Address info sending from address modal
var clicked_address_btn = '';
var clicked_radio_btn = '';
$(document).on('click','.quote_address_btn',function(){
  clicked_address_btn = $(this);
  
  // if($('.new_address_hidden_field')){
  //     clicked_address_btn.siblings('.new_address_hidden_field').remove();
  //   }
  var new_address_fields;
  var new_address_hidden_fields = clicked_address_btn.siblings('.new_address_hidden_field');
  $(document).on('change', '.address_radio_input', function (e) {
    clicked_radio_btn = $(this);
    $('.is_selected').val('');
    var radio_value = $(this).val();
        new_address_fields = clicked_radio_btn.parents('.new_address_col').find('.form-group');
        clicked_address_btn.siblings('.address_selected').val(radio_value);
    // new_address_input_creator(clicked_address_btn); 
  });

  // new_address_input_creator(clicked_address_btn);

  $(document).on('click','.close',function(){
    if($('.new_address_hidden_field')){

      if(new_address_fields){
          for(var i= 0;  i < clicked_address_btn.siblings('.new_address_hidden_field').length ; i++ ){
          clicked_address_btn.siblings('.new_address_hidden_field')[i].value = $(clicked_address_btn.siblings('.addressModal').find('.new_address_col .form-control.form_input_field')[i]).val();
        }
      }
    }


    /* ---- Showing the selected address values beside address btn ---- */
    var selected_address_card =     clicked_address_btn.parent().siblings('.selected_address_card');
    var selected_address_p = '';
    if(clicked_radio_btn){
      selected_address_p = clicked_radio_btn.parent().parent().parent().parent().siblings('.ex_add_p_row').find('p');
      selected_address_p_to_take = clicked_radio_btn.parent().parent().parent().parent().siblings('.ex_add_p_row').find('p.take');
    }

    
    if(clicked_radio_btn){

      clicked_address_btn.siblings('.addressModal').find('.address_radio_input').each(function(){
        if($(this).prop('checked')){
          $(this).parents('.addressModal').addClass("address_true")
        }
      })
      var new_address_input = clicked_address_btn.siblings('.addressModal').find('.new_address_col input.form-control.form_input_field');
      var new_address_input_to_take = clicked_address_btn.siblings('.addressModal').find('.new_address_col input.take');

      console.log(new_address_input)
      console.log(new_address_input_to_take)
      if(clicked_address_btn.siblings('.addressModal').hasClass("address_true")){
        clicked_address_btn.parent().siblings('.selected_address_card').show();
        clicked_address_btn.parents('.service_loaction_net_tota').addClass('has_address_inside').removeClass('has_no_address_inside');

        if(clicked_address_btn.siblings('.address_selected')[0].value == "new_address"){
          for(i=0; i<new_address_input.length; i++){
            $(selected_address_card.find('p')[i]).text($(new_address_input_to_take[i]).val());
          }
        }
        else{
          console.log(selected_address_p.length)
          for(i=0 ; i<selected_address_p.length ; i++){
            $(selected_address_card.find('p')[i]).text($(selected_address_p_to_take[i]).text());
          }
        }

      }

      

      
    }
    else{
      
    }

  })





});

var changing_the_new_address_input_value = '';



function ScrollEventListener(el){
    el.on('scroll', function(){                
    $(".card_to_scroll2").not(this).scrollLeft($(this).scrollLeft());
  });
}

/*$('.card_to_scroll').on('scroll', function(){                
  $(".card_to_scroll2").not(this).scrollLeft($(this).scrollLeft());
});*/


// Calculation quote page
$(document).on('input', '.colInput', function (){


    $(this).parents('.resource_cal_row').each(function(){

    var resource_row_total = 1;
    $(this).find('.colInput').each(function(){
      var input = $(this).val();
      if(input.length !==0){
        resource_row_total *= parseFloat(input);

      }
    });
    console.log(resource_row_total)
    $(this).find('.colTotal').html('$ ' + resource_row_total);
    $(this).find('.colTotal_hidden_input').val(resource_row_total);
    
    });
    var colTotals = 0;
    $(this).parents('.whole_unit_card').find('.colTotal').each(function(){
      colTotals += parseFloat($(this).html().replace(/[^0-9.]/g, ""))
    })
    console.log(colTotals)
    $(this).parents('.whole_unit_card').find('.total_unit_cost span').html("$" + colTotals.toFixed(2));
    $(this).parents('.whole_unit_card').find('.total_unit_cost_input_hidden').val(colTotals);

    gross_calculation($(this))

});

//  Variable Calculation 
$(document).on('input', '.multiply', function (){

  console.log("Multiply clicked")
  var this_hidden = $(this).siblings();
  var siblings_multiply = $(this).parents('.Parameter_with_value').siblings().find('.multiply');
  var this_value = $(this).val();
  // Check if there is any percentage in siblings
  var var_total = 1;
  $(this).parents('.Parameter_with_value').siblings().each(function(){

      if($(this).find('input[type="hidden"]').hasClass('percentage')){
        $(this).parents('.variable_card_section_col').addClass('hasPercentage')
        
      }
      if($(this).parents('.variable_card_section_col').hasClass('hasPercentage')){
        var val_1 = parseFloat(siblings_multiply.val());
        var val_2 = parseInt(this_value);
        var val_3 = parseInt($(this).find('.percentage').val());

        var_total = (val_1 * val_2 * val_3)/100;
      }
      else{
        var_total = parseFloat(this_value)
        siblings_multiply.each(function(){
          var_total *= parseFloat($(this).val());
        })
      }
  })

  if(isNaN(var_total)){
    var_total = 0;
    console.log("Check it")
  }

  $(this).parents('.variable_card_section_row').find('.unit_var_total').html("$ " + var_total.toFixed(2));
  $(this).parents('.variable_card_section_row').find('.unit_var_total_input_hidden').val(var_total);



  var unit_var_total_tochange = $($(this).parents('.whole_unit_card').find('.total_var_cost')[0].firstElementChild);
  var final_variable_total = 0;
  $(this).parents('.whole_unit_card').find('.unit_var_total').each(function(){
    final_variable_total += parseFloat($(this).html().replace(/[^0-9.]/g, ""));

  })
  $(unit_var_total_tochange).html('$' +final_variable_total.toFixed(2));
  // Input hidden value setting
  $(this).parents('.whole_unit_card').find('.total_var_cost_input_hidden').val(final_variable_total);

  gross_calculation($(this));

})

// Total Gross Calculation 
var gross_calculation = function(this_element){

  var gross_to_change = this_element.parents('.whole_unit_card').find('.gross_total span');
  var total_unit_cost = parseFloat(this_element.parents('.whole_unit_card').find('.total_unit_cost_input_hidden').val().replace(/[^0-9.]/g, ""));
  var total_var_cost = parseFloat(this_element.parents('.whole_unit_card').find('.total_var_cost_input_hidden').val().replace(/[^0-9.]/g, ""));

      if(isNaN(total_var_cost)){
        total_var_cost = 0;
      }
      if(isNaN(total_unit_cost)){
        total_unit_cost = 0;
      }

    var total_gross = total_unit_cost + total_var_cost;
      // if(isNaN(total_gross)){
      //   total_gross = 0;
      // }

    console.log(total_gross)
    gross_to_change.html("$" + total_gross.toFixed(2));
    this_element.parents('.whole_unit_card').find(".gross_total_hidden_input").val(total_gross);

    // Final Gross Calculation
  var fin_gross = 0;
  $('.gross_total').each(function(){
    fin_gross += parseFloat($(this).siblings('.gross_total_hidden_input').val().replace(/[^0-9.]/g,""))
  })

      // if(isNaN(fin_gross)){
      //   fin_gross = 0;
      // }
  console.log(fin_gross)
  $('.final_gross_count span').html("$" + fin_gross.toFixed(2));
  $('.final_gross_count_hidden_input').val(fin_gross);


  $('.final_net_count span').html("$" + fin_gross.toFixed(2));
  $('.final_net_count_hidden_input').val(fin_gross);





}

// --- Quotation ends ---- ///


//Changed variable row work 

let var_row = '<div class="row var_param_row">';
    
    var_row +=  '<div class="col-md-3 input_wrapper first input_wrapper">';
    var_row +=   '<div class="form-group bmd-form-group">';
    var_row +=   '<label for="parameter_name" class="bmd-label-floating input_label">Name of parameter *</label>';
    var_row +=    '<input type="text" class="form-control form_input_field" id="parameter_name" name="var_param_name[]" required="true" aria-required="true">';
    var_row +=    '</div>';
    var_row +=    '</div>';

    var_row +=    '<div class="col-md-2 dynamic_var_select second input_wrapper custom_select2_col">';
    var_row +=    '<div class="form-group custom-form-select">';
    var_row +=     '<select class="custom-select var_data_type select2" id="variable_data_type" name="var_param_data_type[]" required="true" aria-required="true">';
    var_row +=        '<option class="form-select-placeholder"></option>';
    var_row +=         '<option value="decimal">Decimal</option>';
    var_row +=          '<option value="integer">Integer</option>';
    var_row +=           '<option value="datetime">DateTime</option>';
    var_row +=           '<option value="string">String</option>';
    var_row +=           '<option value="percentage">Percentage</option>';
    var_row +=      '</select>';
    var_row +=     '<div class="form-element-bar">';
    var_row +=      '</div>';
    var_row +=     '<label class="form-element-label" for="variable_data_type input_label">Data Type</label>';
    var_row +=     '</div>';
    var_row +=     '</div>';

    var_row +=      '<div class="col-md-3 d-flex align-items-center var_param_value_wrapper input_wrapper">';
    var_row +=        '<div class="togglebutton admin-role-toggle priceclass_toggle_section">';
    var_row +=           '<label>';
    var_row +=             '<input type="hidden" class="var_param_value_type" name="var_param_value_type[]" value="0"><input class="var_param_toggle price_class_toggle" type="checkbox" name="" checked="">';
    var_row +=                'Predefined <span class="toggle"></span> UserDefined';
    var_row +=            '</label>';
    var_row +=         '</div>';
    var_row +=       '</div>';

    var_row +=       '<div class="col-md-3 input_wrapper four input_wrapper">';
    var_row +=        '<div class="form-group bmd-form-group">';
    var_row +=          '<label for="parameter_name" class="bmd-label-floating input_label">Value </label>';
    var_row +=            '<input type="text" class="form-control form_input_field" id="var_param_value" name="var_param_value[]" required="true" aria-required="true">';
    var_row +=         '</div>';
    var_row +=       '</div>';

    var_row +=       '<div class="col-md-1 var_cross_btn">';
    var_row +=         '<a href="#" class="btn btn-link btn-danger btn-just-icon remove var_param_dlt_btn"><i class="material-icons">close</i><div class="ripple-container"></div></a>';
    var_row +=       '</div>';

    var_row +=        '</div>';

$('.add_var_btn').click(function(){
  $('.new_var_wrapper').before(var_row);
    //  Select 2 dw activation
  $('.custom_select2_col select').select2();


  
  $('.dynamic_var_select select.custom-select').change(function(){
    $('.dynamic_var_select select.custom-select').addClass('hasvalue');
  });

  $('.var_param_toggle').change(function(){
    if(this.checked){
      $(this).parent().find('.var_param_value_type').val(0);
      $(this).parents('.var_param_row').find('#var_param_value').attr('readonly', false).val('');
    }
    else{
      $(this).parent().find('.var_param_value_type').val(1);
      $(this).parents('.var_param_row').find('#var_param_value').attr('readonly', true).val('');
    }
  });

  $('.var_data_type').change(function() {
    //alert($(this).val());
    if($(this).val() == 'decimal'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'number').prop('step', '0.01');
    }
    else if($(this).val() == 'integer'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'number').prop('step', false);
    }
    else if($(this).val() == 'datetime'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'text').prop('step', false);
    }
    else if($(this).val() == 'string'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'text').prop('step', false);
    }
    else if($(this).val() == 'percentage'){
      $(this).parents('.var_param_row').find('#var_param_value').prop('type', 'text').prop('step', false);
    }
  });

});
$(document).on("click", ".var_param_dlt_btn", function(){
      $(this).parent('div.col-md-1').parent().remove();
});


// Dropdown selection and adding variable
$('#variable_selection_dw').change(function(){
  $(this).addClass('hasvalue')
})
/*$(document).on('click','.variable_adder_btn', function(){
  if($('#variable_selection_dw').val()){
      $('.variable_selection_table_card').fadeIn();
  }

  $('#variable_selection_dw').removeClass('hasvalue');
  $('#variable_selection_dw option:selected').removeAttr('selected');
});*/
// $(document).on('click','.variable_adder_btn', function(){
//   $('#variable_selection_dw').removeClass('hasvalue');
        
// });
$(document).on("click", ".var_remove_btn", function(){
  $(this).parents('.variable_single_card').remove();
  // console.log($('.variable_single_card').length);
  if($('.variable_single_card').length == 0 ){
    $('.variable_selection_table_card').fadeOut();
  }
});

//  Variable Table Scroller 

$('.variable_table_main_section').on('scroll', function(){            
  $(".variable_table_header").not(this).scrollLeft($(this).scrollLeft());
});


/* ===================== INSURANCE JS =======================  */



$(document).on('click','.preview_btn',function(){

//  Function for showing the preview field values


  var previewing_the_field_values = function(clicked_preview_btn){
    $('#insurance_header_section').fadeOut();
    $('#insurance_tab_header').fadeOut();

    // $(this).parents('.tab-pane').find('.preview_row').show();
    clicked_preview_btn.parents('.tab-pane').find('.form_card').fadeOut().next().fadeIn(3000);
    clicked_preview_btn.hide();

    var checkboxValues =clicked_preview_btn.parents('.tab-pane').find('.form_card .insurance_check:checked').map(function() {
      return $(this).val();
    }).get();


    clicked_preview_btn.parents('.tab-pane').find('.preview_row input[value='+checkboxValues[0]+']').prop('checked',true);
    clicked_preview_btn.parents('.tab-pane').find('.preview_row .insurance_check').attr('disabled',true)
    var formValues = [];
    var formLabels = [];
    var formObj = {};
    var name_arr = [];

    clicked_preview_btn.parents('.insurance_form').find('.form_card .form_fields').each(function(){
      if($(this).hasClass('vehicle_liability')){
        formLabels.push("Liability");
        if($('input[value=yes].vehicle_liability').prop("checked") == true){
          formValues.push("true");
        }
        else{
          formValues.push("false");
        }
        
      }
      else if($(this).hasClass('vehicle_physical_dam')){
        formLabels.push("Physical Damage");
        if($('input[value=yes].vehicle_physical_dam').prop("checked") == true){
          formValues.push("true");
        }
        else{
          formValues.push("false");
        }
      }
      else if($(this).parents('.row.name_row').hasClass('name_row')){
        function capitalizeFirstLetter(string) {
          return string.charAt(0).toUpperCase() + string.slice(1);
        }
        name_arr.push($(this).val())
        if(name_arr.length == 2){
          var full_name = capitalizeFirstLetter(name_arr[0]) + ' ' + name_arr[1]
          formValues.push(full_name)
        }
        if($(this).hasClass('name')){
          formLabels.push("Full Name");
        }
        console.log(formValues)
      }
      else if($(this).parents('.row.year_row').hasClass('year_row')){
        formLabels.push($(this).siblings().html());
        formValues.push($(this).val() + ' YRS');
      }
      else{
        formLabels.push($(this).siblings().html());
        formValues.push($(this).val());
      }

      
      for(var i in formLabels){
        formObj[formLabels[i]] = formValues[i];
      }
    })

    console.log(formLabels.length);
    console.log(formValues.length);
    for(var keys in formLabels){



          var show_row =  '<div class="row preview_input_holder_rows">';
              show_row +=   '<label class="col-sm-4 col-form-label input_label">'+formLabels[keys]+' <span> : </span> </label>';
              show_row +=   '<div class="col-sm-8" style="margin-top: 2px; ">';
            // Checking the checkboxes whether they r true or false
          if(formValues[keys] == "true" || formValues[keys] == "false"){
            
            // If true or yes was clickced
            if(formValues[keys] == "true"){
            show_row +=    '<div class="form-check form-check-inline vehicle_liability_dam_wrapper">';
            show_row +=      '<label class="form-check-label">';
            show_row +=       '<input class="form-check-input" type="checkbox" checked value="" disabled="disabled">Yes';
            show_row +=        '<span class="form-check-sign">';
            show_row +=          '<span class="check"></span>';
            show_row +=        '</span>';
            show_row +=      '</label>';
            show_row +=    '</div>';
            show_row +=    '<div class="form-check form-check-inline vehicle_liability_dam_wrapper">';
            show_row +=      '<label class="form-check-label">';
            show_row +=       '<input class="form-check-input" type="checkbox" value="" disabled="disabled"> No';
            show_row +=        '<span class="form-check-sign">';
            show_row +=          '<span class="check"></span>';
            show_row +=        '</span>';
            show_row +=      '</label>';
            show_row +=    '</div>';
            }
            // If false or No was clickced
            else{
            show_row +=    '<div class="form-check form-check-inline vehicle_liability_dam_wrapper">';
            show_row +=      '<label class="form-check-label">';
            show_row +=       '<input class="form-check-input" type="checkbox" value="" disabled="disabled">Yes';
            show_row +=        '<span class="form-check-sign">';
            show_row +=          '<span class="check"></span>';
            show_row +=        '</span>';
            show_row +=      '</label>';
            show_row +=    '</div>';
            show_row +=    '<div class="form-check form-check-inline vehicle_liability_dam_wrapper">';
            show_row +=      '<label class="form-check-label">';
            show_row +=       '<input class="form-check-input" type="checkbox" checked value="" disabled="disabled">No';
            show_row +=        '<span class="form-check-sign">';
            show_row +=          '<span class="check"></span>';
            show_row +=        '</span>';
            show_row +=      '</label>';
            show_row +=    '</div>';
            }
          }
          else{
            show_row +=     '<p class="input_label"> '+formValues[keys]+' </p>';
          }
            show_row +=   '</div>';
            show_row += '</div>';
          clicked_preview_btn.parents('.insurance_form').find('.preview_row .preview_card .card-body').append(show_row);

    }
    // var idx = formLabels.indexOf['First Name *'];
    // formLabels.splice(idx,1)
    $('.vehicle_liability_dam_wrapper').parent().addClass('vehicle_liability_dam_col');
    $('.vehicle_liability_dam_wrapper').parents('.preview_input_holder_rows').addClass('vehicle_liability_dam_row');


    //  Adding address classes ( ;) )
    if(clicked_preview_btn.parents('.insurance_form').find('.preview_card').hasClass('vehicle_preview_card')){
      for(i = 15 ; i<= 19 ; i++){
        $(clicked_preview_btn.parents('.insurance_form').find('.preview_input_holder_rows')[i]).addClass('vehicle_garr_address_row')
      }
      for(i = 23 ; i<= 27 ; i++){
        $(clicked_preview_btn.parents('.insurance_form').find('.preview_input_holder_rows')[i]).addClass('vehicle_less_address_row')
      }
    }
    else if(clicked_preview_btn.parents('.insurance_form').find('.preview_card').hasClass('equipment_preview_card')){
      for(i = 9 ; i<= 13 ; i++){
        $(clicked_preview_btn.parents('.insurance_form').find('.preview_input_holder_rows')[i]).addClass('equip_garr_address_row')
      }
      for(i = 16 ; i<= 20 ; i++){
        $(clicked_preview_btn.parents('.insurance_form').find('.preview_input_holder_rows')[i]).addClass('equip_less_address_row')
      }
    }

    var wrapping_the_addresses_into_fieldset = function(){
      // Wrapping the classes in to fieldset
      $(".vehicle_garr_address_row").wrapAll("<fieldset class='garr_address_fieldset address_fieldset custom_fieldset'></fieldset>");
      $(".equip_garr_address_row").wrapAll("<fieldset class='garr_address_fieldset address_fieldset custom_fieldset'></fieldset>");
      $('.garr_address_fieldset').prepend('<legend> Garraging Address </legend>');


      $(".vehicle_less_address_row").wrapAll("<fieldset class='less_address_fieldset address_fieldset custom_fieldset'></fieldset>");
      $(".equip_less_address_row").wrapAll("<fieldset class='less_address_fieldset address_fieldset custom_fieldset'></fieldset>");
      $('.less_address_fieldset').prepend('<legend> Finance Company / Lessor Address </legend>');

    }

    wrapping_the_addresses_into_fieldset();



  }
  // previewing_the_fields_values() ends

  if($(this).parents('.insurance_form').valid()){
    if (($(this).parents('.insurance_form').find("input[name*='request']:checked").length)<=0) {
      alert("Select Add , Delete or Amend");
    }
    else{
      if($(this).parents('.insurance_form').find('.vehicle_liability').length || $(this).parents('.insurance_form').find('.vehicle_physical_dam').length ){
        if (($(this).parents('.insurance_form').find("input[name*='liability']:checked").length)<=0 || ($(this).parents('.insurance_form').find("input[name*='damage']:checked").length)<=0 ) {
          alert('Select "Yes" or "NO"');
        }
        else{
          previewing_the_field_values($(this));
        }
      }
      else{
        previewing_the_field_values($(this));
      }
    }
  }
  else{
    alert("Please Check and fill up all the required field")  
  }

  })

$(document).on('click','.go_back',function(){

  $(this).parents('.tab-pane').find('.preview_row').fadeOut().prev().fadeIn(3000);

  $('#insurance_header_section').fadeIn();
  $('#insurance_tab_header').fadeIn();
  // $(this).parents('.tab-pane').find('.preview_row').fadeOut();
  // $(this).parents('.tab-pane').find('.form_card').fadeIn();
  $(this).parents('.tab-pane').find('.preview_btn').show();
  $(this).parents('.preview_row').find('.preview_card .card-body .preview_input_holder_rows').remove();
  $(this).parents('.preview_row').find('.address_fieldset').each(function(){
    $(this).remove();
  })


})




//checkbox design for insurance
$(".insurance_check").change(function(){
  $(".insurance_check").prop('checked',false);
  $(this).prop('checked',true);


});

//checkbox design for insurance
$(".vehicle_liability").change(function(){
  $(".vehicle_liability").prop('checked',false);
  $(this).prop('checked',true);


});
//checkbox design for insurance
$(".vehicle_physical_dam").change(function(){
  $(".vehicle_physical_dam").prop('checked',false);
  $(this).prop('checked',true);


});


$('.datepicker').on('click',function(){
  if($(this).value !== 0){
   $(this).parents('.form-group').addClass('is-filled');
  }
})


var trigger = '';
var phone_email_validator = function(input_field){

  if(input_field.val().length ==1 ){
    var firstCharCode = input_field[0].value.charCodeAt(0);
      if(firstCharCode <= 47 || firstCharCode >= 58){
        trigger = 'email';
      }
      else{
        trigger = 'phone';
    }
  }
      if(trigger == 'phone'){
        
        input_field.val(input_field.val().replace(/^(\d{3})(\d{3})(\d)+$/, "($1) $2-$3"));
        input_field.attr('maxlength','14');
        input_field.prop('type','text')
      }
      else{
        input_field.attr('maxlength','524288');
        input_field.prop('type','email');
      }  
}

$("input#company_phone_lessor_equip").on('input',function() {
  phone_email_validator($(this))
});

$('input#vehicel_fin_comp_info').on('input',function(){
  phone_email_validator($(this))
})


//INVENTOYR unit redesign

var select_menus_reforming_serial_no = function(root){
  var old_dw_html_box = [];
  var new_dw_html_box = [];
  var select_dw_holder_card = $(root).parents('.unit_info_card').siblings('.search-add-unit-card');

  select_dw_holder_card.find('#select_serial_name option.select_serial_name_option').each(function(){
    old_dw_html_box.push($(this).html());
  })

  for(var i=0; i<old_dw_html_box.length; i++){
    var idx = old_dw_html_box[i].indexOf('-');
    new_dw_html_box.push(old_dw_html_box[i].replaceBetween(0,idx,i+1));
    
  }
  select_dw_holder_card.find('#select_serial_name option.select_serial_name_option').each(function(i){
    $(this).html(new_dw_html_box[i])

  })
  $('#select_serial_name').select2();  
}

var deleteUnit = function(className) {
  if (confirm("Are you sure you want to delete this?")) {
     
     var dlt_key = $('input[name="inventory_unit_key"]').val();

     if(dlt_key == 0){
        $(className).parent().fadeOut('1000');
     }
     else{
       $.ajax({
              async: false,
              url:"/deleteInventoryUnit",
              method:"POST",
              data:{dlt_key:dlt_key, _token:_token2},
              success:function(result)
              {
                $("#select_serial_name option[value='"+dlt_key+"']").remove();
                $("#select_vin_upc option[value='"+dlt_key+"']").remove();


                
                var duration = 4000; // 4 seconds
                

                $(className).parent().fadeOut('1000');
                $('.unit_dlt_msg').fadeIn(500).fadeOut(4000);
                
                

              }
           })
       select_menus_reforming_serial_no('.unit_remove_btn');
     }

  }else{
      false;
  }       
}

var change_the_padding = function(badge_span,badge_span_postfix){
  var width_of_badge = $(badge_span).outerWidth();
  var width_of_badge_post = $(badge_span_postfix).outerWidth();
  var new_badge_width = width_of_badge +2;
  var new_width = width_of_badge+ width_of_badge_post + 12;
  var width_for_hyphen = width_of_badge+ width_of_badge_post;


  $(badge_span).siblings('.serial_no_field').css('padding-left', ''+new_width+'px');
  $(badge_span).siblings('.serial_no_badge_postfix').css('left', ''+new_badge_width+'px');
  $(badge_span).siblings('.serial_no_badge_hyphen').css('left', ''+width_for_hyphen+'px');



  console.log($('.serial_no_field'))
  $('.serial_no_field').parent().addClass('is-filled');
  $('.serial_no_field').parent().addClass('is-focused');

}

var checking_the_input_field = function(){
  if($('.serial_no_field').length){
    if($('.serial_no_field').val().length>=1){
      $('.serial_name_wrapper').find('.serial_no_badge').show();
      $('.serial_name_wrapper').find('.serial_no_badge_postfix').show();
      $('.serial_name_wrapper').find('.serial_no_badge_hyphen').show();
    }
    else{
      $('.serial_name_wrapper').find('.serial_no_badge').hide();
      $('.serial_name_wrapper').find('.serial_no_badge_postfix').hide();
      $('.serial_name_wrapper').find('.serial_no_badge_hyphen').hide();
    }    
  }

}


$('.serial_selector_dw').on('change',function(){
  if($(this).val()){
    $(this).addClass('hasvalue');
  }
  else{
    
  }

  checking_the_input_field();
  change_the_padding('.serial_no_badge','.serial_no_badge_postfix');
})


String.prototype.replaceBetween = function(start, end, what) {
  return this.substring(0, start) + what + this.substring(end);
};

$(document).ready(function(){
  checking_the_input_field();
});

$('.add_unit_btn').on('click', function(){
  $(this).parents('.search-add-unit-card').siblings('.unit_info_card').fadeOut("1000", function(){

    $('.unit_details_param_section').find('input, textarea').not('.properties_key, .properties_type, .properties_unit_param_id, .unit_param_chk').val('');
    $('.unit_param_chk').prop("checked", false);

    $('input[name="inventory_unit_key"]').val(0);

    

    $('.serial_name_wrapper').find('.serial_no_badge').show().html($('.serial_for_new_unit').val());



    /*$('.unit_info_card #inventory_name')[0].value="";
    $('.unit_info_card #vin')[0].value="";
    $('.unit_info_card .form-group').each(function(){
      if($(this).hasClass('is-filled')){
        $(this).removeClass('is-filled');
      }
    })
    $('.unit_info_card #upc')[0].value="";
    $('.unit_info_card #unit_addition_info')[0].value="";*/

    $('.unit_info_card .input-images2 input[type=file]')[0].value = "";
    $('.unit_info_card .input-images2 .image-uploader').removeClass('has-files');

    $('.input-images2').find('.uploaded-image').each(function(){
      if($(this).hasClass('drag_drop_placeholder_wrapper2')){
        $(this).hide();
      }else{
        $(this).remove();
      }
      
    })

    // Making the serial no visible or not
    checking_the_input_field();

  }).fadeIn('2000');
  
  $(document).on('focus','.serial_no_field',function(){
  // Making the serial no visible or not
    change_the_padding('.serial_no_badge','.serial_no_badge_postfix')

  })

    // Reseting the select dropdown
    //$('#select_serial_name').select2('val', '');
    $('#select_serial_name').val('').trigger('change');
    //$('#select_vin_upc').val('').trigger('change');
    $('#select_serial_name').removeClass('hasvalue');
    //$('#select_vin_upc').removeClass('hasvalue');
})

$(document).on('focus','.serial_no_field',function(){
  // Making the serial no visible or not
    $('.serial_name_wrapper').find('.serial_no_badge').show();
    $('.serial_name_wrapper').find('.serial_no_badge_postfix').show();
})

$(document).on('blur','.serial_no_field',function(){
  // Making the serial no visible or not
  checking_the_input_field();
})

$('.unit_remove_btn').on('click', function(e){
  e.preventDefault();
  e.stopPropagation();
  deleteUnit($(this));


  // Reseting the select dropdown
  $(this).parents('.unit_info_card').siblings('.search-add-unit-card').find('.serial_selector_dw').each(function(){
    if($(this).val() == ""){
      $(this).removeClass('hasvalue')
    }
    
  })
})

$('.upc_holder_edit .random_btn').on('click', function(){
  $(this).parents('.upc_holder_edit').find('.upc_div .form-group').addClass('is-filled');
})

$('.upc_holder_edit .vin_btn').on('click', function(){
  $(this).parents('.upc_holder_edit').find('.vin_div .form-group').addClass('is-filled');
})

$('input.years_field').on('input',function(){
    this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    var len_of_val = $(this).val().length;
    if(len_of_val <= 2){
      $(this).siblings('.years_text').show();
      var sum_of_px = ( len_of_val * 10 );
      $(this).siblings('.years_text').css('left', ''+sum_of_px+'' + 'px')

    }
    else{
      alert("You Can't Write More Than 3 Characters")
      return false;
    }
    if(len_of_val <1 ){
      $(this).siblings('.years_text').hide();
    }
    else{
      $(this).siblings('.years_text').show();
    }
})

//report generate section

var reportFromValue = $('#insurance_report_from').val();
var reportToValue = $('#insurance_report_to').val();
var reportType = $('#insurace_type_report').val();
var reportTime = $('#insurace_time_interval').val();

var reportResetValue = function(){

  $('#insurace_time_interval').val('custom');
  $('#insurace_time_interval').trigger('change');

  $('#insurace_type_report').val('all');
  $('#insurace_type_report').trigger('change');

/*  $("#insurace_time_interval").select2().select2("val", 'custom');
  $("#insurace_time_interval").select2();*/

  $('#insurance_report_from').val(reportFromValue);
  $('#insurance_report_to').val(reportToValue);
  $('#insurace_type_report').val(reportType);
  $('#insurace_time_interval').val(reportTime);
  $('#insurace_type_report .select2-selection__rendered').text(reportType);
  $('#insurace_time_interval .select2-selection__rendered').text(reportTime);
}

$('.generate_report_btn').on('click', function(){
  $('.report_generate_section').fadeIn('4000');
  $(this).fadeOut('1000');
})

$('.insurance_report_cross').on('click', function(){
  $(this).parent().fadeOut('2000', function(){
    reportResetValue();
    $('.generate_report_btn').fadeIn('2000');
  });
})

$('.insurace_type_report').select2();


$('.print_insurace_btn').on('click', function(){

  $('.excelDownloadMsg').show();

  let type = $('#insurace_type_report').val();
  let from = $('#insurance_report_from').val();
  let to = $('#insurance_report_to').val();
  let _token = $('input[name="_token"]').val();

 setTimeout(function(){
  $.ajax({
          async: true,
          url:"/generateInsuranceReport",
          method:"POST",
          data:{type:type, from:from, to:to, _token:_token},
          //timeout: 10000,
          beforeSend: function() {
            //
          },
          success: function (response, textStatus, request) {
            var a = document.createElement("a");
            a.href = response.file; 
            a.download = response.name;
            document.body.appendChild(a);
            a.click();
            a.remove();
            $('.excelDownloadMsg').hide();
            $('.excelSuccessMsg').show();
            $('.generate_report_btn').fadeIn('1000');
          },
           error: function(xhr) { // if error occured
             //
          },
          complete: function() {
            $('.print_insurace_btn').parents('.report_generate_section').fadeOut('3000');
            reportResetValue();
          }
        })

  },3000);

})

var default_from_value = $('#insurance_report_from').val();
var default_to_value = $('#insurance_report_to').val();

$('#insurace_time_interval').on('change',function(){
  if($(this).val() != 'custom'){
    var today = new Date();
    var todaysDate = ((today.getDate()).toString().padStart(2, "0") + '/' + (today.getMonth() + 1).toString().padStart(2, "0") + '/' + (today.getFullYear()));

    $('#insurance_report_from').prop('disabled',true)
    $('#insurance_report_to').prop('disabled',true)
    if($(this).val() == 'daily'){

      $('#insurance_report_from').val(todaysDate);
      $('#insurance_report_to').val(todaysDate);
    }
    else if($(this).val() == 'weekly'){
      today.setDate(today.getDate() - 7);
      var oneWeeksAgoDate = ((today.getDate()).toString().padStart(2, "0") + '/' + (today.getMonth() + 1).toString().padStart(2, "0") + '/' + (today.getFullYear()));
      console.log(todaysDate);
      $('#insurance_report_from').val(oneWeeksAgoDate);
      $('#insurance_report_to').val(todaysDate);
    }
    else if($(this).val() == 'monthly'){
      today.setDate(today.getDate() - 30);
      var oneMonthsAgoDate = ((today.getDate()).toString().padStart(2, "0") + '/' + (today.getMonth() + 1).toString().padStart(2, "0") + '/' + (today.getFullYear()));
      // console.log(oneWeekAgo);
      $('#insurance_report_from').val(oneMonthsAgoDate);
      $('#insurance_report_to').val(todaysDate);
    }

  }
  else{
    $('#insurance_report_from').prop('disabled',false)
    $('#insurance_report_to').prop('disabled',false)

      $('#insurance_report_from').val(default_from_value);
      $('#insurance_report_to').val(default_to_value);
  }
})


// <------- Agreement report generate section ------->

var agreementFromValue = $('#agreement_report_from').val();
var agreementToValue = $('#agreement_report_to').val();
var agreementType = $('#agreement_type_report').val();
var agreementTime = $('#agreement_time_interval').val();

var agreementResetValue = function(){

  $('#agreement_time_interval').val('custom');
  $('#agreement_time_interval').trigger('change');

  $('#agreement_type_report').val('all');
  $('#agreement_type_report').trigger('change');

/*  $("#insurace_time_interval").select2().select2("val", 'custom');
  $("#insurace_time_interval").select2();*/

  $('#agreement_report_from').val(agreementFromValue);
  $('#agreement_report_to').val(agreementToValue);
  $('#agreement_type_report').val(agreementType);
  $('#agreement_time_interval').val(agreementTime);
  $('#agreement_type_report .select2-selection__rendered').text(agreementType);
  $('#agreement_time_interval .select2-selection__rendered').text(agreementTime);
}

$('.generate_agreement_btn').on('click', function(){
  $('.agreement_generate_section').fadeIn('4000');
  $(this).fadeOut('1000');
})

$('.insurance_agreement_cross').on('click', function(){
  $(this).parent().fadeOut('2000', function(){
    agreementResetValue();
    $('.generate_agreement_btn').fadeIn('2000');
  });
})

$('.agreement_type_report').select2();


$('.print_agreement_btn').on('click', function(){

  $('.excelAgreementMsg').show();

  let type = $('#agreement_type_report').val();
  let from = $('#agreement_report_from').val();
  let to = $('#agreement_report_to').val();
  let _token = $('input[name="_token"]').val();
//alert(to);
 setTimeout(function(){
  $.ajax({
          async: true,
          url:"/generateAgreementReport",
          method:"POST",
          data:{type:type, from:from, to:to, _token:_token},
          //timeout: 10000,
          beforeSend: function() {
            //
          },
          success: function (response, textStatus, request) {
            var a = document.createElement("a");
            a.href = response.file; 
            a.download = response.name;
            document.body.appendChild(a);
            a.click();
            a.remove();
            $('.excelAgreementMsg').hide();
            $('.excelSuccessMsg').show();
            $('.generate_agreement_btn').fadeIn('1000');
          },
           error: function(xhr) { // if error occured
             //
          },
          complete: function() {
            $('.print_agreement_btn').parents('.agreement_generate_section').fadeOut('3000');
            agreementResetValue();
          }
        })

  },3000);

})

var prev_from_agreement_val = $('#agreement_report_from').val();
var prev_to_agreement_val = $('#agreement_report_to').val();

$('#agreement_time_interval').on('change',function(){
  if($(this).val() != 'custom'){
    var today = new Date();
    var todaysDate = ((today.getDate()).toString().padStart(2, "0") + '/' + (today.getMonth() + 1).toString().padStart(2, "0") + '/' + (today.getFullYear()));

    $('#agreement_report_from').prop('disabled',true)
    $('#agreement_report_to').prop('disabled',true)
    if($(this).val() == 'daily'){

      $('#agreement_report_from').val(todaysDate);
      $('#agreement_report_to').val(todaysDate);
    }
    else if($(this).val() == 'weekly'){
      today.setDate(today.getDate() - 7);
      var oneWeeksAgoDate = ((today.getDate()).toString().padStart(2, "0") + '/' + (today.getMonth() + 1).toString().padStart(2, "0") + '/' + (today.getFullYear()));
      //console.log(todaysDate);
      $('#agreement_report_from').val(oneWeeksAgoDate);
      $('#agreement_report_to').val(todaysDate);
    }
    else if($(this).val() == 'monthly'){
      today.setDate(today.getDate() - 30);
      var oneMonthsAgoDate = ((today.getDate()).toString().padStart(2, "0") + '/' + (today.getMonth() + 1).toString().padStart(2, "0") + '/' + (today.getFullYear()));
      // console.log(oneWeekAgo);
      $('#agreement_report_from').val(oneMonthsAgoDate);
      $('#agreement_report_to').val(todaysDate);
    }

  }
  else{
    $('#agreement_report_from').prop('disabled',false)
    $('#agreement_report_to').prop('disabled',false)


    $('#agreement_report_from').val(prev_from_agreement_val);
    $('#agreement_report_to').val(prev_to_agreement_val);
  }
})

///////Agreement report generate section



// Customer Fields Validator

var phone_fax_validator = function(input_field,key){

    var s = String.fromCharCode(key.which);
    if(s >=0 || s<=9 ){
      input_field.val(input_field.val().replace(/^(\d{3})(\d{3})(\d)+$/, "($1) $2-$3"));
      input_field.attr('maxlength','14');
      input_field.prop('type','text');
    }
    else{
      key.preventDefault();
      alert("Please Type A Number")
    }
}

$('.mobile').on('keypress',function(e){
  phone_fax_validator($(this),e);
  if($(this).val().length == 14){
    if($(this).siblings('.mobile_error').length >=1){
      $(this).siblings('span.mobile_error').remove();
    }
  }
})
$('.mobile').on('blur',function(e){
  // console.log($(this).val().length)
  // console.log($(this).attr('max'))
  if($(this).val().length < 14){
    $(this).addClass('invalid');
    if($(this).siblings('.mobile_error').length < 1){
      $(this).after('<span class="mobile_error">Please Type A Valid Number</span>');
    }
  }
  else{
    $(this).removeClass('invalid');
    if($(this).siblings('.mobile_error').length >=1){
      $(this).siblings('span.mobile_error').remove();
    }
  }
})

$('.customer_form').validate();



// gps location 

var lattitudeValidator = function(element){
  var lattRegx =  /^(\-?([0]?[0-8]?[0-9](\.\d+)?|90((.[0]+)?)))$/;
  var lat = $(element).val();
  var invalid_msg = 'Latitude measurements range from –90° to +90°';

  if(!lattRegx.test(lat)) {
   $(element).siblings('.error_msg').html(invalid_msg);
   $(element).siblings('.error_msg').show();
   $(element).val('');
   $(element).parent().removeClass('is-filled');
    return false;
   }else{
    $(element).siblings('.error_msg').hide();
    return true;
   }  
}

var longitudeValidator = function(element){
  var longRegx =  /^(\-?([1]?[0-7]?[0-9](\.\d+)?|180((.[0]+)?)))$/;
  var long = $(element).val();
  var invalid_msg = 'Longitude measurements range from –180° to +180°';

  if(!longRegx.test(long)) {
  $(element).siblings('.error_msg').html(invalid_msg);
  $(element).siblings('.error_msg').show();
   $(element).val('');
   $(element).parent().removeClass('is-filled');
    return false;
   }else{
    $(element).siblings('.error_msg').hide();
     return true;
   }  
}

$(document).on('blur', '.dd_latitude', function(){
  console.log('hello');
  lattitudeValidator($(this));
})



$(document).on('blur', '.dd_longitude', function(){
  longitudeValidator($(this));
})


///Unit parameter for inventory resource

$('.unit_parameter_type').select2();

var count_click_unit_id = 0;

$('.add_parameter_btn').click(function(){

  var click_count = count_click_unit_id++;

  let unit_para_row = '<div class="row input_row unit_parameter_row">';
      
      unit_para_row +=  '<div class="col-md-5 input_wrapper first input_wrapper">';
      unit_para_row +=   '<div class="form-group bmd-form-group">';
      unit_para_row +=   '<label for="unit_parameter_name_'+click_count +'" class="bmd-label-floating input_label">Name of parameter *</label>';
      unit_para_row +=    '<input type="text" class="form-control form_input_field" id="unit_parameter_name_'+count_click_unit_id+'" name="unit_param_name[]" required="true" aria-required="true">';
      unit_para_row +=    '</div>';
      unit_para_row +=    '</div>';

      unit_para_row +=    '<div class="col-md-5 second input_wrapper custom_select2_col">';
      unit_para_row +=    '<div class="form-group custom-form-select">';
      unit_para_row +=     '<select class="custom-select unit_parameter_type select2" id="unit_data_type'+count_click_unit_id+'" name="unit_param_data_type[]" required="true" aria-required="true">';
      unit_para_row +=        '<option class="form-select-placeholder"></option>';
      unit_para_row +=          '<option value="string">String</option>';
      unit_para_row +=          '<option value="integer">Integer</option>';
      unit_para_row +=          '<option value="decimal">Decimal</option>';
      unit_para_row +=          '<option value="datetime">DateTime</option>';
      unit_para_row +=          '<option value="checkbox">Checkbox</option>';
      unit_para_row +=          '<option value="vin_number">VIN Number</option>';
      unit_para_row +=          '<option value="upc_number">UPC Number</option>';
      unit_para_row +=      '</select>';
      unit_para_row +=     '<div class="form-element-bar">';
      unit_para_row +=      '</div>';
      unit_para_row +=     '<label class="form-element-label" for="unit_data_type_'+click_count+' input_label">Data Type</label>';
      unit_para_row +=     '</div>';
      unit_para_row +=     '</div>';

      unit_para_row +=       '<div class="col-md-2 unit_parameter_cross text-right">';
      unit_para_row +=         '<a href="#" class="btn btn-link btn-danger btn-just-icon remove unit_param_dlt_btn"><i class="material-icons">close</i><div class="ripple-container"></div></a>';
      unit_para_row +=       '</div>';

      unit_para_row +=        '</div>';


  $('.add_parameter_btn_row').before(unit_para_row);

  $('.unit_parameter_type').select2();
  $(document).on('change','.unit_parameter_type', function(){
    $(this).addClass('hasvalue');
  })
  
});


$(document).on("click", ".unit_param_dlt_btn", function(){
    $(this).parent('.unit_parameter_cross').parent().remove();
});
//  Email Validator ON next BTN click

function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}
//  Multi Step Form JS

var currentTab = 0; // Current tab is set to be the first tab (0)

if($('.tab').length){
  showTab(currentTab); // Display the current tab
}


function showTab(n) {
  window.scrollTo({ top: 0, behavior: 'smooth' });
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");

  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    $('.submitBtn').show();
    $('#nextBtn').hide();
    // document.getElementById("nextBtn").innerHTML = "Submit";
    // document.getElementById("nextBtn").setAttribute('type','submit')
    // document.getElementById("nextBtn").setAttribute('onclick','')
  } else {
    $('.submitBtn').hide();
    $('#nextBtn').show();
    // document.getElementById("nextBtn").innerHTML = "Next";
    // document.getElementById("nextBtn").setAttribute('type','button')
    // document.getElementById("nextBtn").setAttribute('onclick','nextPrev(1)')
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    // document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true, error_type = 'normal';
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByClassName("required");
  // A loop that checks every input field in the current tab:
  
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      if($(y[i]).hasClass('custom-select')){
        $(y[i]).parent().addClass('invalid_parent')
      }
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  //  Email Validator in the tab
  if($(x[currentTab]).find('input[type="email"]').length>=1){
    $(x[currentTab]).find('input[type="email"]').each(function(){
     if(!validateEmail($(this).val())){
        // $(this).addClass("invalid");
        error_type = 'email'
        valid = false;
     }
    })
  }
    //  Mobile Validator in the tab
  if($(x[currentTabCustomer]).find('.mobile').length>=1){
    $(x[currentTabCustomer]).find('.mobile').each(function(){
      if($(this).val().length < 14 && $(this).val() != ""){
        valid = false;
        
      }
    })
  }


  
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  else{
    if(error_type != 'email'){
      alert("Please check and fill up all the required fields")  
    }
    else{
        alert("Please provide a valid email address")
    }
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className = x[n].className.replace(" finish", "");
  x[n].className += " active";
}

$('#regForm').validate();
//  Rental Agreement Multi Step Form

var currentTabRen = 0; // Current tab is set to be the first tab (0)
if($('.tabRen').length){
  showTabRen(currentTabRen); // Display the current tab
}



function showTabRen(n) {
  window.scrollTo({ top: 0, behavior: 'smooth' });
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tabRen");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtnRen").style.display = "none";
  } else {
    document.getElementById("prevBtnRen").style.display = "inline";
  }
  if (n == (x.length - 1)) {

    
    if($('.tabRen').parents('#multiForm2').hasClass('nli_form')){
      $('#nextBtnRen').hide();
      $('.checkListNextBtn#nextBtn').hide();
      $('.checklistSubmitBtn').show();
      $('.submitBtnRen').hide();

    }
    else{
      $('.submitBtnRen').show();
      $('#nextBtnRen').hide();
    }

    // document.getElementById("nextBtnRen").innerHTML = "Submit";
    // document.getElementById("nextBtnRen").setAttribute('type','submit')
    // document.getElementById("nextBtnRen").setAttribute('onclick','')
  } else {
      $('.submitBtnRen').hide();
      $('#nextBtnRen').show();
    // document.getElementById("nextBtnRen").innerHTML = "Next";
    // document.getElementById("nextBtnRen").setAttribute('type','button');
    // document.getElementById("nextBtnRen").setAttribute('onclick','nextPrevRen(1)')
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicatorRen(n)
}

function nextPrevRen(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tabRen");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateFormRen()) return false;
  // Hide the current tab:
  x[currentTabRen].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTabRen = currentTabRen + n;
  // if you have reached the end of the form...
  if (currentTabRen >= x.length) {
    // ... the form gets submitted:
    // document.getElementById("multiForm2").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTabRen(currentTabRen);
}

function validateFormRen() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tabRen");
  y = x[currentTabRen].getElementsByClassName("requiredRen");
  // A loop that checks every input field in the current tab:
  
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      alert("Please check and fill up all the required fields")
      // and set the current valid status to false
      valid = false;
    }
  }

  //  Email Validator in the tab
  if($(x[currentTab]).find('input[type="email"]').length>=1){
    $(x[currentTab]).find('input[type="email"]').each(function(){
     if(!validateEmail($(this).val())){
        alert("Please provide a valid email address")
        // $(this).addClass("invalid");
        input_type = 'email'
        valid = false;
     }
    })
  }
    //  Mobile Validator in the tab
  if($(x[currentTabCustomer]).find('.mobile').length>=1){
    $(x[currentTabCustomer]).find('.mobile').each(function(){
      if($(this).val().length < 14 && $(this).val() != ""){
        valid = false;
        
      }
    })
  }
  
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("stepRen")[currentTabRen].className += " finish";
  }
  else{
    alert("Please Check and fill up all the fields")
  }
  return valid; // return the valid status
}

function fixStepIndicatorRen(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("stepRen");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className = x[n].className.replace(" finish", "");
  x[n].className += " active";
}


$('#multiForm2').validate();

//  Rental MULTI STEP FORM JS ends


//  Owner FORM multistep JS

var currentTabOwner = 0; // Current tab is set to be the first tab (0)

if($('.tabOwner').length){
  showTabOwner(currentTabOwner); // Display the current tab
}


function showTabOwner(n) {
  window.scrollTo({ top: 0, behavior: 'smooth' });
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tabOwner");

  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtnOwner").style.display = "none";
  } else {
    document.getElementById("prevBtnOwner").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    $('.submitBtnOwner').show();
    $('#nextBtnOwner').hide();
    // document.getElementById("nextBtn").innerHTML = "Submit";
    // document.getElementById("nextBtn").setAttribute('type','submit')
    // document.getElementById("nextBtn").setAttribute('onclick','')
  } else {
    $('.submitBtnOwner').hide();
    $('#nextBtnOwner').show();
    // document.getElementById("nextBtn").innerHTML = "Next";
    // document.getElementById("nextBtn").setAttribute('type','button')
    // document.getElementById("nextBtn").setAttribute('onclick','nextPrev(1)')
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicatorOwner(n)
}

function nextPrevOwner(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tabOwner");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateFormOwner()) return false;
  // Hide the current tab:
  x[currentTabOwner].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTabOwner = currentTabOwner + n;
  // if you have reached the end of the form...
  if (currentTabOwner >= x.length) {
    // ... the form gets submitted:
    // document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTabOwner(currentTabOwner);
}

function validateFormOwner() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true,error_type="normal";
  x = document.getElementsByClassName("tabOwner");
  y = x[currentTabOwner].getElementsByClassName("requiredOwner");
  // A loop that checks every input field in the current tab:
  
  for (i = 0; i < y.length; i++) {

    //  Owner Image uploader Validation issue 
    if($(y[i]).hasClass('agreement_upload')){
      $(y[i]).find('input').each(function(){
        if($(this).val() == ""){
          $(this).parents('.agreement_upload').siblings('.upload_owner_file_error_msg').show();
          valid = false;
        }
      })
    }

    // If a field is empty...
    if (y[i].value == "") {
      if($(y[i]).hasClass('custom-select')){
        $(y[i]).parent().addClass('invalid_parent')
      }
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
    
  }

  //  Email Validator in the tab
  if($(x[currentTabOwner]).find('input[type="email"]').length>=1){
    $(x[currentTabOwner]).find('input[type="email"]').each(function(){
     if(!validateEmail($(this).val())){
        // $(this).addClass("invalid");
        error_type = 'email'
        valid = false;
     }
    })
  }

  //  Mobile Validator in the tab
  if($(x[currentTabCustomer]).find('.mobile').length>=1){
    $(x[currentTabCustomer]).find('.mobile').each(function(){
      if($(this).val().length < 14 && $(this).val() != ""){
        valid = false;
        
      }
    })
  }

  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("stepOwner")[currentTabOwner].className += " finish";
  }
  else{
    if(error_type != 'email'){
      alert("Please check and fill up all the required fields")  
    }
    else{
        alert("Please provide a valid email address")
    }
  }
  return valid; // return the valid status
}

function fixStepIndicatorOwner(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("stepOwner");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className = x[n].className.replace(" finish", "");
  x[n].className += " active";
}

$('#multiForm3').validate();

$('.month select').on('change',function(){
  if($(this).val()!== ""){
    $(this).siblings('label').text("")
  }
})

$('#multiForm3 select').on('change',function(){
  if($(this).val()!== ""){
    $(this).addClass('hasvalue');
  }
})


//  owner multistep js ends

//  JSA Multi step 
var currentTabJsa = 0; // Current tab is set to be the first tab (0)
if($('.tabJsa').length){
  showTabJsa(currentTabJsa); // Display the current tab
}



function showTabJsa(n) {

  window.scrollTo({ top: 0, behavior: 'smooth' });
  console.log("From JSA")
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tabJsa");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtnJsa").style.display = "none";
  } else {
    document.getElementById("prevBtnJsa").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    $('.submitBtnJsa').hide();
    $('#nextBtnJsa').hide();
    // document.getElementById("nextBtnRen").innerHTML = "Submit";
    // document.getElementById("nextBtnRen").setAttribute('type','submit')
    // document.getElementById("nextBtnRen").setAttribute('onclick','')
  } else {
      $('.submitBtnJsa').hide();
      $('#nextBtnJsa').show();
    // document.getElementById("nextBtnJsa").innerHTML = "Next";
    // document.getElementById("nextBtnJsa").setAttribute('type','button');
    // document.getElementById("nextBtnJsa").setAttribute('onclick','nextPrevJsa(1)')
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicatorJsa(n)
}

function nextPrevJsa(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tabJsa");
  // Exit the function if any field in the curJsat tab is invalid:
  if (n == 1 && !validateFormJsa()) return false;
  // Hide the current tab:
  x[currentTabJsa].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTabJsa = currentTabJsa + n;
  // if you have reached the end of the form...
  if (currentTabJsa >= x.length) {
    // ... the form gets submitted:
    // document.getElementById("multiForm2").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTabJsa(currentTabJsa);
}

function validateFormJsa() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tabJsa");
  y = x[currentTabJsa].getElementsByClassName("requiredJsa");
  // A loop that checks every input field in the current tab:
  
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      alert("Please check and fill up all the required fields")
      // and set the current valid status to false
      valid = false;
    }
  }

  //  Email Validator in the tab
  if($(x[currentTabJsa]).find('input[type="email"]').length>=1){
    $(x[currentTabJsa]).find('input[type="email"]').each(function(){
     if(!validateEmail($(this).val())){
        alert("Please provide a valid email address")
        // $(this).addClass("invalid");
        input_type = 'email'
        valid = false;
     }
    })
  }
  //  Mobile Validator in the tab
  if($(x[currentTabCustomer]).find('.mobile').length>=1){
    $(x[currentTabCustomer]).find('.mobile').each(function(){
      if($(this).val().length < 14 && $(this).val() != ""){
        valid = false;
        
      }
    })
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("stepJsa")[currentTabJsa].className += " finish";
  }
  else{
    alert("Please check and fill up all the required fields")
  }
  return valid; // return the valid status
}

function fixStepIndicatorJsa(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("stepJsa");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className = x[n].className.replace(" finish", "");
  x[n].className += " active";
}

//  Jsa ends

$('input[name="responsible"]').on('click', function(e) {
  $('input[name="responsible"]').prop('checked', false);
  $(this).prop('checked', true);
});

//read more
$('.btn_read_more').click(function() {
  $(this).siblings('.more').slideToggle(500,function(){
    $(this).siblings('.dots').slideToggle(100)
  });
  if ($(this).text() == "Read More") {
    $(this).text("Read Less")
  } else {
    $(this).text("Read More")
  }
});


$('.contractor_select').select2();
$('#contractor_select').on('change', function(){
  $(this).addClass('hasvalue');
})


$('#sub_agree_year').val('');
$('#sub_agree_year_2').val('');

//day validation
$('#sub_agree_day').on('blur', function(){
  var dayValue = $(this).val();
  var maxAttr = $(this).attr('max');
  maxAttr = parseInt(maxAttr);
  console.log(maxAttr);
  if($(this).val().length > 1){
    if (!(dayValue>=1 && dayValue<=maxAttr)){
      alert('Please put a date from 1 to '+ maxAttr +'');
      $(this).val('');
      $(this).siblings('.error').hide();
      $(this).addClass('invalid')
    }
    else{
      $(this).removeClass('invalid')
    }  
  }
})

$('#sub_agree_day_2').on('blur', function(){
  var dayValue = $(this).val();
  var maxAttr = $(this).attr('max');
  maxAttr = parseInt(maxAttr);
  console.log(maxAttr);
  if($(this).val().length > 1){
    if (!(dayValue>=1 && dayValue<=maxAttr)){
      alert('Please put a date from 1 to '+ maxAttr +'');
      $(this).val('');
      $(this).siblings('.error').hide();
      $(this).addClass('invalid')
    }
    else{
      $(this).removeClass('invalid')
    }  
  }
})

//  Owber day validation
$('#owner_agree_day').on('blur', function(){
  var dayValue = $(this).val();
  var maxAttr = $(this).attr('max');
  maxAttr = parseInt(maxAttr);
  if($(this).val().length > 1){
    if (!(dayValue>=1 && dayValue<=maxAttr)){
      alert('Please put a date from 1 to '+ maxAttr +'');
      $(this).val('');
      $(this).siblings('.error').hide();
      $(this).addClass('invalid')
    }
    else{
      $(this).removeClass('invalid')
    }  
  }
})

$('#owner_agree_day_2').on('blur', function(){
  var dayValue = $(this).val();
  var maxAttr = $(this).attr('max');
  maxAttr = parseInt(maxAttr);
  console.log(maxAttr);
  if($(this).val().length > 1){
    if (!(dayValue>=1 && dayValue<=maxAttr)){
      alert('Please put a date from 1 to '+ maxAttr +'');
      $(this).val('');
      $(this).siblings('.error').hide();
      $(this).addClass('invalid')
    }
    else{
      $(this).removeClass('invalid')
    }  
  }
})


//month label hide

$('#subContractoMonth').on('change', function(){
  $(this).parent().children('label').text('');
})
$('#subContractoMonth2').on('change', function(){
  $(this).parent().children('label').text('');
})

//month validation

$('#subContractoMonth').on('change', function(){
  var getMon = $(this).val();
  var setDateLength = $(this).parents('span').siblings('span').children('.day').children();
  if(getMon == 'january' || getMon == 'march' || getMon == 'may' || getMon == 'july' || getMon =='august' || getMon =='october' || getMon =='december' ){
    setDateLength.attr('max', '31');
  }
  else if(getMon == 'april' || getMon == 'june' || getMon == 'september' || getMon == 'november'){
    setDateLength.attr('max', '30');
  }
  else if (getMon == 'february') {
    setDateLength.attr('max', '28');
  }
  var setDateValue = setDateLength.val();
  var setdateMax = setDateLength.attr('max');
  if(setDateValue>setdateMax){
    setDateLength.val('');
  }

});

$('#subContractoMonth2').on('change', function(){
  var getMon = $(this).val();
  var setDateLength = $(this).parents('span').siblings('span').children('.day').children();
  if(getMon == 'january' || getMon == 'march' || getMon == 'may' || getMon == 'july' || getMon =='august' || getMon =='october' || getMon =='december' ){
    setDateLength.attr('max', '31');
  }
  else if(getMon == 'april' || getMon == 'june' || getMon == 'september' || getMon == 'november'){
    setDateLength.attr('max', '30');
  }
  else if (getMon == 'february') {
    setDateLength.attr('max', '28');
  }
  var setDateValue = setDateLength.val();
  var setdateMax = setDateLength.attr('max');
  if(setDateValue>setdateMax){
    setDateLength.val('');
  }

});

//  Owner MOnth validation 

$('#OwnerAgreementMonth').on('change', function(){
  var getMon = $(this).val();
  var setDateLength = $(this).parents('span').siblings('span').children('.day').children();
  if(getMon == 'january' || getMon == 'march' || getMon == 'may' || getMon == 'july' || getMon =='august' || getMon =='october' || getMon =='december' ){
    setDateLength.attr('max', '31');
  }
  else if(getMon == 'april' || getMon == 'june' || getMon == 'september' || getMon == 'november'){
    setDateLength.attr('max', '30');
  }
  else if (getMon == 'february') {
    setDateLength.attr('max', '28');
  }
  var setDateValue = setDateLength.val();
  var setdateMax = setDateLength.attr('max');
  if(setDateValue>setdateMax){
    setDateLength.val('');
  }

});

$('#OwnerAgreementMonth2').on('change', function(){
  var getMon = $(this).val();
  var setDateLength = $(this).parents('span').siblings('span').children('.day').children();
  if(getMon == 'january' || getMon == 'march' || getMon == 'may' || getMon == 'july' || getMon =='august' || getMon =='october' || getMon =='december' ){
    setDateLength.attr('max', '31');
  }
  else if(getMon == 'april' || getMon == 'june' || getMon == 'september' || getMon == 'november'){
    setDateLength.attr('max', '30');
  }
  else if (getMon == 'february') {
    setDateLength.attr('max', '28');
  }
  var setDateValue = setDateLength.val();
  var setdateMax = setDateLength.attr('max');
  if(setDateValue>setdateMax){
    setDateLength.val('');
  }

});

//  ends

// Year Validation 
$('.sub_contractor_year').on('click','.yearpicker-items',function(){
  console.log("Year Picker Value taken")
  // console.log($(this))
  var getYear, getMonth, getDate, getDateValue,setdateMax;
  // getYear = $('#sub_agree_year').val();
  getYear = $(this).html();
  

  getDate = $('#sub_agree_year').parents('span').siblings('span').children('.day').children();

  getDateValue = getDate.val();
  getMonth = $('#sub_agree_year').parents('span').siblings('span').children('.month').children().children('#subContractoMonth').val();

  setdateMax = getDate.attr('max');

  if(getYear%4 === 0){
    if(getMonth == 'february'){

       getDate.attr('max', '29');
      if(getDateValue>setdateMax){
        getDate.val('');
      }
    }
  }
  else if(getYear%4 != 0){
    if((getMonth == 'february')){
      getDate.attr('max','28');
      if(getDateValue>setdateMax){
        getDate.val('');
      }
    }
  }
})

$('.sub_contractor_year2').on('click','.yearpicker-items',function(){
  console.log("Year Picker Value taken")
  var getYear, getMonth, getDate, getDateValue,setdateMax;
  // getYear = $('#sub_agree_year').val();
  getYear = $(this).html();
  

  getDate = $('#sub_agree_year_2').parents('span').siblings('span').children('.day').children();

  getDateValue = getDate.val();
  getMonth = $('#sub_agree_year_2').parents('span').siblings('span').children('.month').children().children('#subContractoMonth2').val();

  setdateMax = getDate.attr('max');

  if(getYear%4 === 0){
    if(getMonth == 'february'){
       getDate.attr('max', '29');
      if(getDateValue>setdateMax){
        getDate.val('');
      }
    }
  }
  else if(getYear%4 != 0){
    if((getMonth == 'february')){
      getDate.attr('max','28');
      if(getDateValue>setdateMax){
        getDate.val('');
      }
    }
  }
})

// Owner Year Validation 
$('.owner_agreement_year').on('click','.yearpicker-items',function(){
  console.log("Year Picker Value taken")
  console.log($(this))
  var getYear, getMonth, getDate, getDateValue,setdateMax;
  // getYear = $('#sub_agree_year').val();
  getYear = $(this).html();
  

  getDate = $('#owner_agreement_year').parents('span').siblings('span').children('.day').children();

  getDateValue = getDate.val();
  getMonth = $('#owner_agreement_year').parents('span').siblings('span').children('.month').children().children('#OwnerAgreementMonth').val();

  setdateMax = getDate.attr('max');

  if(getYear%4 === 0){
    if(getMonth == 'february'){

       getDate.attr('max', '29');
      if(getDateValue>setdateMax){
        getDate.val('');
      }
    }
  }
  else if(getYear%4 != 0){
    if((getMonth == 'february')){
      getDate.attr('max','28');
      if(getDateValue>setdateMax){
        getDate.val('');
      }
    }
  }
})

$('.owner_agreement_year2').on('click','.yearpicker-items',function(){
  console.log("Year Picker Value taken")
  var getYear, getMonth, getDate, getDateValue,setdateMax;
  // getYear = $('#sub_agree_year').val();
  getYear = $(this).html();
  

  getDate = $('#owner_agreement_year2').parents('span').siblings('span').children('.day').children();

  getDateValue = getDate.val();
  getMonth = $('#owner_agreement_year2').parents('span').siblings('span').children('.month').children().children('#OwnerAgreementMonth2').val();

  setdateMax = getDate.attr('max');

  if(getYear%4 === 0){
    if(getMonth == 'february'){
       getDate.attr('max', '29');
      if(getDateValue>setdateMax){
        getDate.val('');
      }
    }
  }
  else if(getYear%4 != 0){
    if((getMonth == 'february')){
      getDate.attr('max','28');
      if(getDateValue>setdateMax){
        getDate.val('');
      }
    }
  }
})






//auto fillup generate for contractor

function autoFillUpContractor(autoInputField, firstInputField) {
  $(autoInputField).val($(firstInputField).val());
}
$("#subcontractor_name").keyup(function(){
    autoFillUpContractor('#subcontractor_name_2', '#subcontractor_name');
    var isFilledAdded = $('#subcontractor_name_2');
    if(isFilledAdded.val() !== ''){
      $(isFilledAdded).parent().addClass('is-filled');
    }
    else if(isFilledAdded.val() == '') {
      $(isFilledAdded).parent().removeClass('is-filled');
    }
});

$("#mailing_addr").keyup(function(){
    autoFillUpContractor('#subcontractor_address', '#mailing_addr');
    var isFilledAdded = $('#subcontractor_address');
    if(isFilledAdded.val() !== ''){
      $(isFilledAdded).parent().addClass('is-filled');
    }
    else if(isFilledAdded.val() == '') {
      $(isFilledAdded).parent().removeClass('is-filled');
    }
});

$("#contact_person_phone").keyup(function(){
    autoFillUpContractor('#subcontractor_phone', '#contact_person_phone');
    var isFilledAdded = $('#subcontractor_phone');
    if(isFilledAdded.val() !== ''){
      $(isFilledAdded).parent().addClass('is-filled');
    }
    else if(isFilledAdded.val() == '') {
      $(isFilledAdded).parent().removeClass('is-filled');
    }
});

$("#contact_person_email").keyup(function(){
    autoFillUpContractor('#subcontractor_email', '#contact_person_email');
    var isFilledAdded = $('#subcontractor_email');
    if(isFilledAdded.val() !== ''){
      $(isFilledAdded).parent().addClass('is-filled');
    }
    else if(isFilledAdded.val() == '') {
      $(isFilledAdded).parent().removeClass('is-filled');
    }
});

$("#contact_person").keyup(function(){
    autoFillUpContractor('#subcontractor_contact', '#contact_person');
    var isFilledAdded = $('#subcontractor_contact');
    if(isFilledAdded.val() !== ''){
      $(isFilledAdded).parent().addClass('is-filled');
    }
    else if(isFilledAdded.val() == '') {
      $(isFilledAdded).parent().removeClass('is-filled');
    }
});


//  auto fillup generate for owner

$("#owner_name").keyup(function(){
    autoFillUpContractor('#owner_operator_name_2', '#owner_name');
    var isFilledAdded = $('#owner_operator_name_2');
    if(isFilledAdded.val() !== ''){
      $(isFilledAdded).parent().addClass('is-filled');
    }
    else if(isFilledAdded.val() == '') {
      $(isFilledAdded).parent().removeClass('is-filled');
    }
});

$("#owner_mailing_addr").keyup(function(){
    autoFillUpContractor('#owner_operator_address', '#owner_mailing_addr');
    var isFilledAdded = $('#owner_operator_address');
    if(isFilledAdded.val() !== ''){
      $(isFilledAdded).parent().addClass('is-filled');
    }
    else if(isFilledAdded.val() == '') {
      $(isFilledAdded).parent().removeClass('is-filled');
    }
});

$("#owner_contact_person_phone").keyup(function(){
    autoFillUpContractor('#notice_owner_operator_phone', '#owner_contact_person_phone');
    var isFilledAdded = $('#notice_owner_operator_phone');
    if(isFilledAdded.val() !== ''){
      $(isFilledAdded).parent().addClass('is-filled');
    }
    else if(isFilledAdded.val() == '') {
      $(isFilledAdded).parent().removeClass('is-filled');
    }
});

$("#owner_contact_person_email").keyup(function(){
    autoFillUpContractor('#notice_owner_operator_email', '#owner_contact_person_email');
    var isFilledAdded = $('#notice_owner_operator_email');
    if(isFilledAdded.val() !== ''){
      $(isFilledAdded).parent().addClass('is-filled');
    }
    else if(isFilledAdded.val() == '') {
      $(isFilledAdded).parent().removeClass('is-filled');
    }
});

$("#owner_contact_person").keyup(function(){
    autoFillUpContractor('#notice_owner_operator_contact', '#owner_contact_person');
    var isFilledAdded = $('#notice_owner_operator_contact');
    if(isFilledAdded.val() !== ''){
      $(isFilledAdded).parent().addClass('is-filled');
    }
    else if(isFilledAdded.val() == '') {
      $(isFilledAdded).parent().removeClass('is-filled');
    }
});
//  ends autofillup

//  Currency INput Validation



$(".currency_input").on({
    keyup: function(e) {
      
      $(this).attr('type','text')
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "" + left_side + "." + right_side; // $ moved
  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "" + input_val; // $ moved
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}

// end of Currency INput Validation 


$('.tooltip_link').on('mouseover', function(){
  $(this).siblings().css({'opacity':'1', 'z-index':'99'});
})
$('.tooltip_link').on('mouseleave', function(){
  $(this).siblings().css({'opacity':'0', 'z-index':'-2'});
})



//new customer info form js

var currentTabCustomer = 0; // Current tab is set to be the first tab (0)
if($('.tab_customer_info').length){
  showTabCustomer(currentTabCustomer); // Display the current tab
}



function showTabCustomer(n) {

  window.scrollTo({ top: 0, behavior: 'smooth' });

  $("html, body").animate({scrollTop:0},"slow");
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab_customer_info");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtnCustomer").style.display = "none";
  } else {
    document.getElementById("prevBtnCustomer").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    $('.submitBtnCustomer').show();
    $('#nextBtnCustomer').hide();
    $('#customerRejecgBtn').show();
  } else {
      $('.submitBtnCustomer').hide();
      $('#nextBtnCustomer').show();
      $('#customerRejecgBtn').hide();
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicatorCustomer(n)
}

function nextPrevCustomer(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab_customer_info");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateFormCustomer()) return false;
  // Hide the current tab:
  x[currentTabCustomer].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTabCustomer = currentTabCustomer + n;
  // if you have reached the end of the form...
  if (currentTabCustomer >= x.length) {
    // ... the form gets submitted:
    return false;
  }
  // Otherwise, display the correct tab:
  showTabCustomer(currentTabCustomer);
}

function validateFormCustomer() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true, error_type = 'normal';
  x = document.getElementsByClassName("tab_customer_info");
  y = x[currentTabCustomer].getElementsByClassName("requiredCustomer");
  // A loop that checks every input field in the current tab:
  
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      if($(y[i]).hasClass('custom-select')){
        $(y[i]).parent().addClass('invalid_parent')
      }
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  //  Email Validator in the tab
  if($(x[currentTabCustomer]).find('input[type="email"]').length>=1){
    $(x[currentTabCustomer]).find('input[type="email"]').each(function(){
     if(!validateEmail($(this).val())){
        // $(this).addClass("invalid");
        error_type = 'email'
        valid = false;
     }
    })
  }
  //  Mobile Validator in the tab
  if($(x[currentTabCustomer]).find('.mobile').length>=1){
    $(x[currentTabCustomer]).find('.mobile').each(function(){
      if($(this).val().length < 14 && $(this).val() != ""){
        valid = false;
        
      }
    })
  }

  if($(x[currentTabCustomer]).find('.sign_input_validation.customer_mail_sign').val() == "0"){
    $('.sign_error_msg').show();
    valid = false;
  }
  else{
    $('.sign_error_msg').hide();
  }


  
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step_customer")[currentTabCustomer].className += " finish";
  }
  else{
    if(error_type != 'email'){
      alert("Please check and fill up all the required fields")  
    }
    else{
        alert("Please provide a valid email address")
    }
  }
  return valid; // return the valid status
}

function fixStepIndicatorCustomer(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step_customer");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className = x[n].className.replace(" finish", "");
  x[n].className += " active";
}


$('#customer_info_form').validate();


//supplier addition

var countSupplier = 0;

$('.add_suplier_refer_btn').click(function(){

  var click_count_supplier = countSupplier++;


  let supplier_para_row = '<fieldset class="customer_fieldset supplier_fieldset">';
      supplier_para_row +=  '<legend>Reference #</legend>';

      supplier_para_row +='<div class="row input_row">';
      
      supplier_para_row +=    '<div class="col-md-6 input_wrapper">';
      supplier_para_row +=     '<div class="form-group bmd-form-group">';
      supplier_para_row +=      '<label for="suplier_name_'+click_count_supplier +'" class="bmd-label-floating input_label">Name of Suplier</label>';
      supplier_para_row +=      '<input type="text" class="form-control form_input_field" id="suplier_name_'+click_count_supplier+'" name="suplier_name[]" required="true" aria-required="true">';
      supplier_para_row +=     '</div>';
      supplier_para_row +=    '</div>';

      supplier_para_row +=    '<div class="col-md-6 input_wrapper">';
      supplier_para_row +=     '<div class="form-group bmd-form-group">';
      supplier_para_row +=      '<label for="type_of_business_'+click_count_supplier +'" class="bmd-label-floating input_label">Type of Business</label>';
      supplier_para_row +=      '<input type="text" class="form-control form_input_field" id="type_of_business_'+click_count_supplier+'" name="type_of_business[]" required="true" aria-required="true">';
      supplier_para_row +=     '</div>';
      supplier_para_row +=    '</div>';

      supplier_para_row +=    '<div class="col-md-6 input_wrapper">';
      supplier_para_row +=     '<div class="form-group bmd-form-group">';
      supplier_para_row +=      '<label for="suplier_addr_'+click_count_supplier +'" class="bmd-label-floating input_label">Address</label>';
      supplier_para_row +=      '<input type="text" class="form-control form_input_field" id="suplier_addr_'+click_count_supplier+'" name="suplier_addr[]" required="true" aria-required="true">';
      supplier_para_row +=     '</div>';
      supplier_para_row +=    '</div>';

      supplier_para_row +=    '<div class="col-md-6 input_wrapper">';
      supplier_para_row +=     '<div class="form-group bmd-form-group">';
      supplier_para_row +=      '<label for="suplier_contact_'+click_count_supplier +'" class="bmd-label-floating input_label">Contact</label>';
      supplier_para_row +=      '<input type="text" class="form-control form_input_field" id="suplier_contact_'+click_count_supplier+'" name="suplier_contact[]" required="true" aria-required="true">';
      supplier_para_row +=     '</div>';
      supplier_para_row +=    '</div>';

      supplier_para_row +=    '<div class="col-md-6 input_wrapper">';
      supplier_para_row +=     '<div class="form-group bmd-form-group">';
      supplier_para_row +=      '<label for="suplier_telephone_'+click_count_supplier +'" class="bmd-label-floating input_label">Telephone No</label>';
      supplier_para_row +=      '<input type="text" class="form-control form_input_field" id="suplier_telephone_'+click_count_supplier+'" name="suplier_telephone[]" required="true" aria-required="true">';
      supplier_para_row +=     '</div>';
      supplier_para_row +=    '</div>';

      supplier_para_row +=    '<div class="col-md-6 input_wrapper">';
      supplier_para_row +=     '<div class="form-group bmd-form-group">';
      supplier_para_row +=      '<label for="suplier_fax_'+click_count_supplier +'" class="bmd-label-floating input_label">Fax No</label>';
      supplier_para_row +=      '<input type="text" class="form-control form_input_field" id="suplier_fax_'+click_count_supplier+'" name="suplier_fax[]" required="true" aria-required="true">';
      supplier_para_row +=     '</div>';
      supplier_para_row +=    '</div>';

      
      supplier_para_row +=       '<div class="supplier_parameter_cross text-right">';
      supplier_para_row +=         '<a href="#" class="btn btn-link btn-danger btn-just-icon remove supplier_dlt_btn"><i class="material-icons">close</i><div class="ripple-container"></div></a>';
      supplier_para_row +=       '</div>';

      supplier_para_row +=    '</div>';
      supplier_para_row +=   '</fieldset>';

      if($('.supplier_fieldset').length <3 ){
        $('.add_suplier_btn_row').before(supplier_para_row);
      }
  
});


$(document).on("click", ".supplier_dlt_btn", function(){
    $(this).parent('.supplier_parameter_cross').parents('.customer_fieldset').remove();
});

//Express Sevice added 

var countExpService = 0;

$('.add_exp_ser_refer_btn').click(function(){

  var click_count_exp_ser = countExpService++;


  let exp_serv_para_row = '<fieldset class="express_service">';
      exp_serv_para_row +=  '<legend>Service #</legend>';

      exp_serv_para_row +='<div class="row input_row">';
      
      exp_serv_para_row +=    '<div class="col-md-6 input_wrapper">';
      exp_serv_para_row +=     '<div class="form-group bmd-form-group currency_form_group">';
      exp_serv_para_row +=      '<label for="credit_limit_'+click_count_exp_ser +'" class="bmd-label-floating input_label">Credit Limit</label>';
      exp_serv_para_row +=      '<span class="currency_symbol">$</span>';
      exp_serv_para_row +=      '<input type="number" class="form-control form_input_field currency_field" id="credit_limit_'+click_count_exp_ser+'" name="credit_limit[]" required="true" aria-required="true">';
      exp_serv_para_row +=     '</div>';
      exp_serv_para_row +=    '</div>';

      exp_serv_para_row +=    '<div class="col-md-6 input_wrapper">';
      exp_serv_para_row +=     '<div class="form-group bmd-form-group currency_form_group">';
      exp_serv_para_row +=      '<label for="date_acct_opened_'+click_count_exp_ser +'" class="bmd-label-floating input_label">Date Account Open</label>';
      exp_serv_para_row +=      '<span class="currency_symbol">$</span>';
      exp_serv_para_row +=      '<input type="number" class="form-control form_input_field currency_field" id="date_acct_opened_'+click_count_exp_ser+'" name="date_acct_opened[]" required="true" aria-required="true">';
      exp_serv_para_row +=     '</div>';
      exp_serv_para_row +=    '</div>';

      exp_serv_para_row +=    '<div class="col-md-6 input_wrapper">';
      exp_serv_para_row +=     '<div class="form-group bmd-form-group currency_form_group">';
      exp_serv_para_row +=      '<label for="accout_average_'+click_count_exp_ser +'" class="bmd-label-floating input_label">Account Average</label>';
      exp_serv_para_row +=      '<span class="currency_symbol">$</span>';
      exp_serv_para_row +=      '<input type="number" class="form-control form_input_field currency_field" id="accout_average_'+click_count_exp_ser+'" name="accout_average[]" required="true" aria-required="true">';
      exp_serv_para_row +=     '</div>';
      exp_serv_para_row +=    '</div>';

      exp_serv_para_row +=    '<div class="col-md-6 input_wrapper">';
      exp_serv_para_row +=     '<div class="form-group bmd-form-group currency_form_group">';
      exp_serv_para_row +=      '<label for="account_high_'+click_count_exp_ser +'" class="bmd-label-floating input_label">Account High</label>';
      exp_serv_para_row +=      '<span class="currency_symbol">$</span>';
      exp_serv_para_row +=      '<input type="number" class="form-control form_input_field currency_field" id="account_high_'+click_count_exp_ser+'" name="account_high[]" required="true" aria-required="true">';
      exp_serv_para_row +=     '</div>';
      exp_serv_para_row +=    '</div>';

      exp_serv_para_row +=    '<div class="col-md-6 input_wrapper">';
      exp_serv_para_row +=     '<div class="form-group bmd-form-group currency_form_group">';
      exp_serv_para_row +=      '<label for="avg_day_pay_'+click_count_exp_ser +'" class="bmd-label-floating input_label">Average Day to Pay</label>';
      exp_serv_para_row +=      '<span class="currency_symbol">$</span>';
      exp_serv_para_row +=      '<input type="number" class="form-control form_input_field currency_field" id="avg_day_pay_'+click_count_exp_ser+'" name="avg_day_pay[]" required="true" aria-required="true">';
      exp_serv_para_row +=     '</div>';
      exp_serv_para_row +=    '</div>';

      exp_serv_para_row +=    '<div class="col-md-6 input_wrapper">';
      exp_serv_para_row +=     '<div class="form-group bmd-form-group">';
      exp_serv_para_row +=      '<label for="terms_'+click_count_exp_ser +'" class="bmd-label-floating input_label">Terms</label>';
      exp_serv_para_row +=      '<input type="text" class="form-control form_input_field" id="terms_'+click_count_exp_ser+'" name="terms[]" required="true" aria-required="true">';
      exp_serv_para_row +=     '</div>';
      exp_serv_para_row +=    '</div>';

      exp_serv_para_row +=    '<div class="col-md-6 second input_wrapper custom_select2_col">';
      exp_serv_para_row +=    '<div class="form-group custom-form-select">';
      exp_serv_para_row +=     '<select class="custom-select exp_service_rating select2" id="unit_data_type_'+click_count_exp_ser+'" name="rating[]" required="true" aria-required="true">';
      exp_serv_para_row +=        '<option class="form-select-placeholder"></option>';
      exp_serv_para_row +=          '<option value="excellent">Excellent</option>';
      exp_serv_para_row +=          '<option value="good">Good</option>';
      exp_serv_para_row +=          '<option value="fair">Fair</option>';
      exp_serv_para_row +=          '<option value="poor">Poor</option>';
      exp_serv_para_row +=      '</select>';
      exp_serv_para_row +=     '<div class="form-element-bar">';
      exp_serv_para_row +=      '</div>';
      exp_serv_para_row +=     '<label class="form-element-label" for="unit_data_type_'+click_count_exp_ser+' input_label">Rating</label>';
      exp_serv_para_row +=     '</div>';
      exp_serv_para_row +=     '</div>';

      exp_serv_para_row +=    '<div class="col-md-6 input_wrapper">';
      exp_serv_para_row +=     '<div class="form-group bmd-form-group">';
      exp_serv_para_row +=      '<label for="express_comments_'+click_count_exp_ser +'" class="bmd-label-floating input_label">Comments</label>';
      exp_serv_para_row +=      '<input type="text" class="form-control form_input_field" id="express_comments_'+click_count_exp_ser+'" name="express_comments[]" required="true" aria-required="true">';
      exp_serv_para_row +=     '</div>';
      exp_serv_para_row +=    '</div>';

      
      
      exp_serv_para_row +=       '<div class="exp_service_parameter_cross text-right">';
      exp_serv_para_row +=         '<a href="#" class="btn btn-link btn-danger btn-just-icon remove exp_serv_dlt_btn"><i class="material-icons">close</i><div class="ripple-container"></div></a>';
      exp_serv_para_row +=       '</div>';

      exp_serv_para_row +=    '</div>';
      exp_serv_para_row +=   '</fieldset>';
      
      $('.add_exp_ser_btn_row').before(exp_serv_para_row);

      $('.exp_service_rating').select2();
      $(document).on('change','.exp_service_rating', function(){
        $(this).addClass('hasvalue');
      })
  
});


$(document).on("click", ".exp_serv_dlt_btn", function(){
    $(this).parent('.exp_service_parameter_cross').parents('.express_service').remove();
});


$('.type_of_ownership').select2();
$('.type_of_ownership').on('change', function(){
  $(this).addClass('hasvalue');
})

$('.type_of_organization').select2();
$('.type_of_organization').on('change', function(){
  $(this).addClass('hasvalue');
});

$('.po_required').select2();
$('.po_required').on('change', function(){
  $(this).addClass('hasvalue');
});

$('.loan_outstanding').select2();
$('.loan_outstanding').on('change', function(){
  $(this).addClass('hasvalue');
});

$('.bank_credit_rating').select2();
$('.bank_credit_rating').on('change', function(){
  $(this).addClass('hasvalue');
});

$('.payment_history').select2();
$('.payment_history').on('change', function(){
  $(this).addClass('hasvalue');
});

//  On load drodpdown label issue for whole app

  $('.custom-select.select2').each(function(){
    if($(this).val() !== ""){
      $(this).addClass("hasvalue")
    }
  })

function sortDropDownListByText() {
    // Loop for each select element on the page.
    $("select.custom-select").each(function() {

      if(!$(this).parents('.custom_select2_col').hasClass('month')){
         
        // Keep track of the selected option.
        var selectedValue = $(this).val();
 
        // Sort all the options by text. I could easily sort these by val.
        $(this).html($("option", $(this)).sort(function(a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }));
 
        // Select one option.
        $(this).val(selectedValue);
      }
    });
}

sortDropDownListByText();

$(document).on('keydown','.searchTerm',function(e) {
  switch (e.which) {
    case 40:
      // $($(this).parents('.searchWrapper').find('.suggestionList ul.results li')[0]).children().addClass('suggestion_selected')
      // console.log($('.results li:not(:last-child) a.suggestion_selected'))
      e.preventDefault(); // prevent moving the cursor
      // console.log($('.results li:not(:last-child) a.suggestion_selected').parent());
      $(this).parents('.searchWrapper').find('.suggestionList .results li:not(:last-child) a.suggestion_selected').removeClass('suggestion_selected')
        .parent().next().children().addClass('suggestion_selected');
      //   var len_of_li = $('.results li').length
      //   var active_li = $('.results li a.suggestion_selected').index('.results a');
      // if(active_li == len_of_li-1){
      //   $($('.results li')[active_li]).children().removeClass('suggestion_selected');
      //   active_li = 0;
      //   $($('.results li')[active_li]).children().addClass('suggestion_selected');
      // }
      break;
    case 38:
    // console.log("Up Pressed")
      e.preventDefault(); // prevent moving the cursor
      $(this).parents('.searchWrapper').find('.suggestionList .results li:not(:first-child) a.suggestion_selected').removeClass('suggestion_selected')
        .parent().prev().children().addClass('suggestion_selected');
      break;
  }
});

//crdit application toggle

$('.social_tax_toggle input[type="checkbox"]').on('click', function(){

  var socialTaxt = $(this).parents('.social_tax_toggle').siblings('.tax_sec');
  if($(this).prop("checked") == true){
    socialTaxt.children('label').attr('for','social_security');
    socialTaxt.children('label').text('Social security(Last 4 digit)');
    socialTaxt.children('input').attr('id','social_security');
    socialTaxt.children('input').val('');
  }
  else if($(this).prop("checked") == false){
    socialTaxt.children('label').attr('for','tax_number');
    socialTaxt.children('label').text('Tax ID Number(9 digit)');
    socialTaxt.children('input').attr('id','tax_number');
    socialTaxt.children('input').val('');
  }
})

//credit application

var tinNumber = function(input_field,key){

    var s = String.fromCharCode(key.which);

    // var inputChar = input_field[0].value.length;
    // if(inputChar === 0){
    //   var keyChar = s;
    //   if(keyChar ==='9'){
    //   }else{
    //     key.preventDefault();
    //     alert('TINs always begin with the number 9');
    //   }
    // }

    if(s >=0 || s<=9 ){
      input_field.val(input_field.val().replace(/^(\d{3})(\d{2})(\d)+$/, "$1-$2-$3"));
      input_field.attr('maxlength','11');
      input_field.prop('type','text');
    }
    else{
      key.preventDefault();
      alert("Please Type A Number")
    }
}

$(document).on('keypress','#tax_number',function(e){
  tinNumber($(this),e);
})


var socialSecurity = function(input_field, key){
  var s = String.fromCharCode(key.which);
  if(s >=0 || s<=0){
    input_field.attr('maxlength','4');
    input_field.prop('type','text');
  }else{
    key.preventDefault();
    alert('Please type a number');
  }
}

$(document).on('keypress','#social_security',function(e){
  socialSecurity($(this),e);
})





//  Vacuum Truck Operator Checklist FORM multistep JS

var currentTabVehOpeCheck = 0; // Current tab is set to be the first tab (0)

if($('.tabVehOpeCheck').length){
  showTabVehOpeCheck(currentTabVehOpeCheck); // Display the current tab
}


function showTabVehOpeCheck(n) {
  window.scrollTo({ top: 0, behavior: 'smooth' });
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tabVehOpeCheck");

  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtnVehOpeCheck").style.display = "none";
  } else {
    document.getElementById("prevBtnVehOpeCheck").style.display = "inline";
  }
  console.log("current tab" + currentTabVehOpeCheck );
  if (n == (x.length - 1)) {
    $('.submitBtnVehOpeCheck').hide();
    $('#nextBtnVehOpeCheck').hide();
  }
  // else if(n != 0 && n != (x.length - 1)){
  //   $(".checkListPrevBtn").hide();
  //   $(".checkListNextBtn").hide();
  //   $('#nextBtnVehOpeCheck').show();
  // }
  else {
    $('.submitBtnVehOpeCheck').hide();
    $('#nextBtnVehOpeCheck').show();
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicatorVehOpeCheck(n)
}

function nextPrevVehOpeCheck(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tabVehOpeCheck");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateFormVehOpeCheck()) return false;
  // Hide the current tab:
  x[currentTabVehOpeCheck].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTabVehOpeCheck = currentTabVehOpeCheck + n;
  // if you have reached the end of the form...
  if (currentTabVehOpeCheck >= x.length) {
    // ... the form gets submitted:
    // document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTabVehOpeCheck(currentTabVehOpeCheck);
}

function validateFormVehOpeCheck() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true,error_type="normal";
  x = document.getElementsByClassName("tabVehOpeCheck");
  y = x[currentTabVehOpeCheck].getElementsByClassName("requiredVehOpeCheck");
  // A loop that checks every input field in the current tab:
  
  for (i = 0; i < y.length; i++) {

    // If a field is empty...
    if (y[i].value == "") {
      if($(y[i]).hasClass('custom-select')){
        $(y[i]).parent().addClass('invalid_parent')
      }
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
    
  }
    //  Mobile Validator in the tab
  if($(x[currentTabCustomer]).find('.mobile').length>=1){
    $(x[currentTabCustomer]).find('.mobile').each(function(){
      if($(this).val().length < 14 && $(this).val() != ""){
        valid = false;
        
      }
    })
  }


  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("stepVehOpeCheck")[currentTabVehOpeCheck].className += " finish";
  }
  else{
    if(error_type != 'email'){
      alert("Please check and fill up all the required fields")  
    }
    else{
        alert("Please provide a valid email address")
    }
  }
  return valid; // return the valid status
}

function fixStepIndicatorVehOpeCheck(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("stepVehOpeCheck");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className = x[n].className.replace(" finish", "");
  x[n].className += " active";
}



//  owner multistep js ends


$('#sub-category-custom-select').on('change',function(){

})


// inventory unit table scroll issure

$('.all_unit_list_view .table-responsive').css('overflow-x','auto');
var resizeDiv = function(){
  var widthofInventory = $('.all_unit_list_view').width();
  $('.unit_visibility_card').width(widthofInventory - 40);
}

$('.unit_list_edit').on('click', function(){
  $(this).parents('.table-responsive').scrollLeft(0);
  resizeDiv();
  $('.unit_visibility_card').css('margin-left','5px');
  $('.all_unit_list_view .table-responsive').css('overflow-x','hidden');
})

jQuery('.all_unit_list_view').on('resize', function() {
  $('.unit_visibility_card').width(widthofInventory - 40);
});


$(document).ready(resizeDiv);
$(window).on("load resize", resizeDiv);


$('.custom-btn-one').on('click', function(){
  console.log('clicked');
  $('.all_unit_list_view .table-responsive').css('overflow-x','auto');
  $('.unit_visibility_card').css({"width": "99%", "margin": "0 auto"});
})



//Job site hazard analysis FORM multistep JS

var currentTabJobSiteAnalysis = 0; // Current tab is set to be the first tab (0)

if($('.tabJobSiteAnalysis').length){
  showTabJobSiteAnalysis(currentTabJobSiteAnalysis); // Display the current tab
}


function showTabJobSiteAnalysis(n) {
  window.scrollTo({ top: 0, behavior: 'smooth' });
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tabJobSiteAnalysis");

  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtnJobSiteAnalysis").style.display = "none";
  } else {
    document.getElementById("prevBtnJobSiteAnalysis").style.display = "inline-block";
  }
  if (n == (x.length - 1)) {
    $('.submitBtnJobSiteAnalysis').hide();
    $('#nextBtnJobSiteAnalysis').hide();
  } else {
    $('.submitBtnJobSiteAnalysis').hide();
    $('#nextBtnJobSiteAnalysis').show();
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicatorJobSiteAnalysis(n)
}

function nextPrevJobSiteAnalysis(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tabJobSiteAnalysis");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateJobSiteAnalysis()) return false;
  // Hide the current tab:
  x[currentTabJobSiteAnalysis].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTabJobSiteAnalysis = currentTabJobSiteAnalysis + n;
  // if you have reached the end of the form...
  if (currentTabJobSiteAnalysis >= x.length) {
    // ... the form gets submitted:
    // document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTabJobSiteAnalysis(currentTabJobSiteAnalysis);
}

function validateJobSiteAnalysis() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true,error_type="normal";
  x = document.getElementsByClassName("tabJobSiteAnalysis");
  y = x[currentTabJobSiteAnalysis].getElementsByClassName("requiredJobSiteAnalysis");
  // A loop that checks every input field in the current tab:
  
  for (i = 0; i < y.length; i++) {

    // If a field is empty...
    if (y[i].value == "") {
      if($(y[i]).hasClass('custom-select')){
        $(y[i]).parent().addClass('invalid_parent')
      }
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
    
  }


  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("stepJobSiteAnalysis")[currentTabJobSiteAnalysis].className += " finish";
  }
  else{
    if(error_type != 'email'){
      alert("Please check and fill up all the required fields")  
    }
    else{
        alert("Please provide a valid email address")
    }
  }
  return valid; // return the valid status
}

function fixStepIndicatorJobSiteAnalysis(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("stepJobSiteAnalysis");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className = x[n].className.replace(" finish", "");
  x[n].className += " active";
}


$('.demoBtn').on('click',function(){
  console.log("HOLA")
  window.scrollTo(0, 0);

})

//  jobs site hazard anlysis js ends


